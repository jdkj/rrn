package com.eliteams.quick4j.core.threadpool;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 全局定量线程池
 * Created by Dsmart on 2016/11/23.
 */
@Component
@Scope
public class GlobalFixedThreadPool {

    /**
     * 线程池线程数数量
     */
    private final static int CORE_POOL_SIZE = 36;


    static ExecutorService fixedThreadPool;


    private static class GlobalFixedThreadPoolHandler{

        public static ExecutorService getFixedThreadPool(){
            if(fixedThreadPool==null){
                fixedThreadPool = Executors.newFixedThreadPool(CORE_POOL_SIZE);
            }
            return fixedThreadPool;
        }

    }


    /**
     * 获取全局线程池
     * @return
     */
    public static ExecutorService getInstance(){
        return GlobalFixedThreadPoolHandler.getFixedThreadPool();
    }

    /**
     * 初始化线程池
     */
    @PostConstruct
    public void init(){
        GlobalFixedThreadPool.getInstance();
    }

    /**
     * 销毁线程池
     */
    @PreDestroy
    public void destroy(){
        GlobalFixedThreadPool.getInstance().shutdown();
        try {
            GlobalFixedThreadPool.getInstance().awaitTermination(10000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


