package com.eliteams.quick4j.core.threadpool;



import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/**
 * 全局定时线程池
 * Created by Dsmart on 2016/11/23.
 */

@Component
@Scope
public class GlobalScheduleThreadPool {

    /**
     * 线程池线程数数量
     */
    private final static int CORE_POOL_SIZE = 6;


    static ScheduledExecutorService scheduleThreadPool;


    private static class GlobalScheduleThreadPoolHandler{

        public static ScheduledExecutorService getScheduleThreadPool(){
            if(scheduleThreadPool==null){
                scheduleThreadPool = Executors.newScheduledThreadPool(CORE_POOL_SIZE);
            }
            return scheduleThreadPool;
        }

    }


    /**
     * 获取全局线程池
     * @return
     */
    public static ScheduledExecutorService getInstance(){
        return GlobalScheduleThreadPoolHandler.getScheduleThreadPool();
    }


    @PostConstruct
    public void init(){
        GlobalScheduleThreadPool.getInstance();
    }

    @PreDestroy
    public void destroy(){
        GlobalScheduleThreadPool.getInstance().shutdown();
        try {
            GlobalScheduleThreadPool.getInstance().awaitTermination(10000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }








}
