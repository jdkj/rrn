package com.eliteams.quick4j.core.entity;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

/**
 * 请求参数基本类
 * Created by Dsmart on 2016/6/2.
 */
public class PageRestrict<T> implements Serializable{

    private static final long serialVersionUID = -5488783656752303796L;

    /**
     * 用户授权码
     */
    protected String tickets;

    /**
     * 数据类型
     */
    protected T model ;

    /**
     * 请求
     */
    protected HttpServletRequest request;

    /**
     * session对象
     */
    protected HttpSession session;

    /**
     * 页码
     */
    protected int pageno=1;

    /**
     * 每页显示多少数据
     */
    protected int pagesize=10;

    /**
     * ip地址
     */
    protected String ip;

    public T getModel() {
        return model;
    }

    public void setModel(T model) {
        this.model = model;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
        this.setSession(this.request.getSession());
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    public int getPageno() {
        return pageno;
    }

    public void setPageno(int pageno) {
        this.pageno = pageno;
    }

    public int getPagesize() {
        return pagesize;
    }

    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getTickets() {
        return tickets;
    }

    public void setTickets(String tickets) {
        this.tickets = tickets;
    }
}
