package com.eliteams.quick4j.core.entity;

import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;

/**
 * Result : 响应的结果对象
 *
 * @author StarZou
 * @since 2014-09-27 16:28
 */
public class Result implements Serializable {
    private static final long serialVersionUID = 6288374846131788743L;

    /**
     * 信息
     */
    private String resultMsg;

    /**
     * 状态码
     */
    private int statusCode;

    /**
     * 是否成功
     */
    private boolean resultFlag;

    @JsonIgnore
    private ResponseStatusEnums responseStatusEnums;


    public Result() {

    }

    public Result(String msg) {
        this.resultFlag = false;
        this.resultMsg = msg;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public boolean getResultFlag() {
        return resultFlag;
    }

    public void setResultFlag(boolean resultFlag) {
        this.resultFlag = resultFlag;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void setResponseStatusEnums(ResponseStatusEnums responseStatusEnums) {
        this.responseStatusEnums = responseStatusEnums;
        this.setResultFlag(responseStatusEnums.getStatusFlag());
        this.setStatusCode(responseStatusEnums.getStatusCode());
        this.setResultMsg(responseStatusEnums.getStatusMsg());
    }
}
