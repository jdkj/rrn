package com.eliteams.quick4j.core.feature.cache.redis;

import com.eliteams.quick4j.web.model.StockMarket;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.*;

/**
 * RedisCache : redis 缓存 插件
 *
 * @author StarZou
 * @since 2015-03-20 11:12
 */
@Component(value = "redisCache")
public class RedisCache {


    //Redis服务器IP
    private static String ADDR = "127.0.0.1";

    //Redis的端口号
    private static int PORT = 6379;

    //访问密码
    private static String AUTH = "admin";

    //可用连接实例的最大数目，默认值为8；
    //如果赋值为-1，则表示不限制；如果pool已经分配了maxActive个jedis实例，则此时pool的状态为exhausted(耗尽)。
    private static int MAX_ACTIVE = 1024;

    //控制一个pool最多有多少个状态为idle(空闲的)的jedis实例，默认值也是8。
    private static int MAX_IDLE = 200;

    //等待可用连接的最大时间，单位毫秒，默认值为-1，表示永不超时。如果超过等待时间，则直接抛出JedisConnectionException；
    private static int MAX_WAIT = 10000;

    private static int TIMEOUT = 10000;

    //在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    private static boolean TEST_ON_BORROW = true;

    private static JedisPool jedisPool = null;

    /**
     * 初始化Redis连接池
     */
    static {
        try {
            JedisPoolConfig config = new JedisPoolConfig();
            config.setMaxTotal(MAX_ACTIVE);
            config.setMaxIdle(MAX_IDLE);
            config.setMaxWaitMillis(MAX_WAIT);
            config.setTestOnBorrow(TEST_ON_BORROW);
            //使用Redis密码
            //jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT, AUTH);
            //不使用Redis密码
            jedisPool = new JedisPool(config, ADDR, PORT, TIMEOUT, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取Jedis实例
     *
     * @return
     */
    public synchronized static Jedis getJedis() {
        try {
            if (jedisPool != null) {
                Jedis resource = jedisPool.getResource();
                return resource;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 释放jedis资源
     *
     * @param jedis
     */
    public static void returnResource(final Jedis jedis) {
        if (jedis != null) {
            jedisPool.returnResource(jedis);
            jedis.close();
        }
    }


    //对象序列化为字符串
    public static String objectSerialiable(Object obj) {
        String serStr = null;
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
            objectOutputStream.writeObject(obj);
            serStr = byteArrayOutputStream.toString("ISO-8859-1");
            serStr = java.net.URLEncoder.encode(serStr, "UTF-8");

            objectOutputStream.close();
            byteArrayOutputStream.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return serStr;
    }

    //字符串反序列化为对象
    public static Object objectDeserialization(String serStr) {
        Object newObj = null;
        if (StringUtils.isNotEmpty(serStr)) {
            try {
                String redStr = java.net.URLDecoder.decode(serStr, "UTF-8");
                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(redStr.getBytes("ISO-8859-1"));
                ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
                newObj = objectInputStream.readObject();
                objectInputStream.close();
                byteArrayInputStream.close();
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return newObj;
    }


    public static void putObject(String key, Object value) {
        Jedis jedis = getJedis();
        jedis.set(key, objectSerialiable(value));
        returnResource(jedis);
    }


    public static Object getObject(String key) {
        Object obj = null;
        Jedis jedis = getJedis();
        String result = jedis.get(key);

        if (result != null) {
            obj = objectDeserialization(result);
        }
        returnResource(jedis);
        return obj;
    }


    public static void main(String[] as) {
//        Jedis jedis = getJedis();


//        StockBase stockBase   = new StockBase();
//        stockBase.setStockBaseId(1193l);
//        stockBase.setStockMarket("sz");
//        stockBase.setStockName("浪潮信息");
//        stockBase.setStockCode("000977");
//
//
//        String serStr = objectSerialiable(stockBase);
//        System.out.println(serStr);
//        jedis.set("000977",serStr);

//        String reStr = jedis.get("sh600978");

//        System.out.println(reStr);
        String stockCode = "sh600978";

//        System.out.println( get(stockCode));
        StockMarket reStockBase = (StockMarket) getObject(stockCode);

//        reStockBase.setStockNamePy("中国联通");
//
//        String setStr = objectSerialiable(reStockBase);
//
//        jedis.set("600050",setStr);
//            jedis.bgsave();
        System.out.println(reStockBase.toString());


//        Set<String> stockBaseList = jedis.smembers("stockBase");
//        for (String stockBaseCode : stockBaseList) {
//            System.err.println(stockBaseCode);
//        }


//        RedisCache.returnResource(jedis);
    }
}
