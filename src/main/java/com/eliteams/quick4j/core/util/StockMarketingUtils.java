package com.eliteams.quick4j.core.util;

/**
 * Created by Dsmart on 2016/11/24.
 */
public class StockMarketingUtils {

    //上午开盘时间
    private final static String MORNING_MARKETING_START_TIME = "09:30:00";


    //上午休市时间
    private final static String MORNING_MARKETING_END_TIME = "11:30:00";


    //下午开盘时间
    private final static String AFTERNOON_MARKETING_START_TIME = "13:00:00";


    //下午休市时间
    private final static String AFTERNOON_MARKETING_END_TIME = "15:00:00";

    //股票可委托开始时间
    private final static String STOCK_ENTRUST_START_TIME = "09:15:00";

    //股票可委托结束时间
    private final static String STOCK_ENTRUST_END_TIME = "15:00:00";

    //早市委托不可撤开始时间
    private final static String MORNING_UNCANCELABLE_START_TIME = "09:20:00";

    //早市委托不可撤结束时间
    private final static String MORNING_UNCANCELABLE_END_TIME = "09:25:00";

    //午市委托不可撤开始时间   针对深市
    private final static String AFTERNOON_UNCANCELABLE_START_TIME = "14:57:00";

    //午市委托不可撤结束时间  针对深市
    private final static String AFTERNOON_UNCANCELABLE_END_TIME = "15:00:00";

    //合约数据更新开始时间
    private final static String AGREEMENT_REFRESH_START_TIME = "09:00:00";

    //合约数据更新截止时间
    private final static String AGREEMENT_REFRESH_END_TIME = "15:05:00";

    //股票数据/持仓数据早市更新开始时间
    private final static String STOCK_REFRESH_MORNING_START_TIME = "09:25:15";
    //股票数据/持仓数据早市更新结束时间
    private final static String STOCK_REFRESH_MORNING_END_TIME = "11:32:00";
    //股票数据/持仓数据午市更新开始时间
    private final static String STOCK_REFRESH_AFTERNOON_START_TIME = "13:00:00";
    //股票数据/持仓数据午市更新结束时间
    private final static String STOCK_REFRESH_AFTERNOON_END_TIME = "15:03:00";


    /**
     * 判断当前是否为股票可委托时间段
     *
     * @return
     */
    public static boolean isDuringStockEntrustTime() {
        Long current = current();
        Long stockEntrustStartTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + STOCK_ENTRUST_START_TIME, "yyyy-MM-dd HH:mm:ss");
        Long stockEntrustEndTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + STOCK_ENTRUST_END_TIME, "yyyy-MM-dd HH:mm:ss");
        return current.compareTo(stockEntrustStartTime) >= 0 && current.compareTo(stockEntrustEndTime) <= 0;
    }


    /**
     * 判断当前是否为不可撤单时间段
     *
     * @param stockMarket 股票所属证券市场
     * @return
     */
    public static boolean isDuringUncancelableTime(String stockMarket) {
        Long current = current();
        Long morningUncancelableStartTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + MORNING_UNCANCELABLE_START_TIME, "yyyy-MM-dd HH:mm:ss");
        Long morningUncancelableEndTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + MORNING_UNCANCELABLE_END_TIME, "yyyy-MM-dd HH:mm:ss");

        Long afternoonUncancelableStartTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + AFTERNOON_UNCANCELABLE_START_TIME, "yyyy-MM-dd HH:mm:ss");
        Long afternoonUncancelableEndTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + AFTERNOON_UNCANCELABLE_END_TIME, "yyyy-MM-dd HH:mm:ss");

        if ("sz".equals(stockMarket)) {//午市集合竞价只针对深市    竞价期间不可撤单
            return (current >= morningUncancelableStartTime && current <= morningUncancelableEndTime) || (current >= afternoonUncancelableStartTime && current <= afternoonUncancelableEndTime);
        } else {
            return current >= morningUncancelableStartTime && current <= morningUncancelableEndTime;
        }

    }

    /**
     * 是否在合约更新的时间段内
     *
     * @return
     */
    public static boolean isDuringAgreementRefreshTime() {
        Long current = current();
        Long agreementRefreshStartTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + AGREEMENT_REFRESH_START_TIME, "yyyy-MM-dd HH:mm:ss");
        Long agreementRefreshEndTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + AGREEMENT_REFRESH_END_TIME, "yyyy-MM-dd HH:mm:ss");
        return (current >= agreementRefreshStartTime && current <= agreementRefreshEndTime);
    }


    /**
     * 是否在股票更新/持仓数据更新时间段内
     *
     * @return
     */
    public static boolean isDuringStockRefreshTime() {
        Long current = current();
        Long stockRefreshMorningStartTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + STOCK_REFRESH_MORNING_START_TIME, "yyyy-MM-dd HH:mm:ss");
        Long stockRefreshMorningEndTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + STOCK_REFRESH_MORNING_END_TIME, "yyyy-MM-dd HH:mm:ss");
        Long stockRefreshAfternoonStartTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + STOCK_REFRESH_AFTERNOON_START_TIME, "yyyy-MM-dd HH:mm:ss");
        Long stockRefreshAfternoonEndTime = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + STOCK_REFRESH_AFTERNOON_END_TIME, "yyyy-MM-dd HH:mm:ss");
        return (current >= stockRefreshMorningStartTime && current <= stockRefreshMorningEndTime) || (current >= stockRefreshAfternoonStartTime && current <= stockRefreshAfternoonEndTime);
    }


    /**
     * 是否处于连续报价时间段内
     *
     * @return
     */
    public static boolean isDuringStreamingPricesTime() {
        Long current = current();
        Long morningStartTime = morningStar();
        Long morningEndTime = morningEnd();
        Long afternoonStartTime = afternoonStar();
        Long afternoonEndTime = afternoonEnd();
        return (current >= morningStartTime && current <= morningEndTime) || (current >= afternoonStartTime && current <= afternoonEndTime);
    }


    /**
     * 当前是否为节假日
     * 用于系统数据更新 判断是否为开市日
     *
     * @return
     */
    public static boolean isCurrentDayFestivalDay() {
        return FestivalDayUtils.isCurrentDayFestivalDay();
    }

    /**
     * 当前时间是否在开市时间段内
     * 9:30-15:00
     *
     * @return
     */
    public static boolean isDuringMarketingTime() {
        Long current = current();
        Long morningStar = morningStar();
        Long afternoonEnd = afternoonEnd();
        return current.compareTo(morningStar) >= 0 && current.compareTo(afternoonEnd) <= 0;
    }

    /**
     * 是否在时间段内
     *
     * @param startTime HH:mm:ss
     * @param endTime   HH:mm:ss
     * @return
     */
    public static boolean isDuringTimeArea(String startTime, String endTime) {
        Long current = current();
        Long start = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + startTime, "yyyy-MM-dd HH:mm:ss");
        Long end = ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + endTime, "yyyy-MM-dd HH:mm:ss");
        return current >= start && current <= end;
    }


    /**
     * 当前时间是否是开市时间
     * 9:30-11:30  13:00-15:00
     *
     * @return
     */
    public static boolean isMarketingTime() {
//        return isMorningMarketingTime()||isAfternoonMarketingTime();
        return isMarketingTime(0l);
    }

    /**
     * 当前时间是否是开市修正时间
     *
     * @param fixMills
     * @return
     */
    public static boolean isMarketingTime(Long fixMills) {
        return isMorningMarketingTime(fixMills) || isAfternoonMarketingTime(fixMills);
    }

    /**
     * 是否处于早市时间内
     *
     * @param fixMills 修正时间
     * @return
     */
    public static boolean isMorningMarketingTime(Long fixMills) {
        Long current = current();
        Long morningStar = morningStar() - fixMills;
        Long morningEnd = morningEnd() + fixMills;
        return current.compareTo(morningStar) >= 0 && current.compareTo(morningEnd) <= 0;
    }

    /**
     * 是否处于早市时间内
     *
     * @return
     */
    public static boolean isMorningMarketingTime() {
//        Long current = current();
//        Long morningStar = morningStar();
//        Long morningEnd = morningEnd();
//        return current.compareTo(morningStar)>=0&&current.compareTo(morningEnd)<=0;
        return isMorningMarketingTime(0l);
    }

    /**
     * 是否处于 午市时间内
     *
     * @param fixMills 修正参数值
     * @return
     */
    public static boolean isAfternoonMarketingTime(Long fixMills) {
        Long current = current();
        Long afternoonStar = afternoonStar() - fixMills;
        Long afternoonEnd = afternoonEnd() + fixMills;
        return current.compareTo(afternoonStar) >= 0 && current.compareTo(afternoonEnd) <= 0;
    }

    /**
     * 是否处于 午市时间内
     *
     * @return
     */
    public static boolean isAfternoonMarketingTime() {
//        Long current = current();
//        Long afternoonStar = afternonnStar();
//        Long afternoonEnd = afternoonEnd();
//        return current.compareTo(afternoonStar)>=0&&current.compareTo(afternoonEnd)<=0;
        return isAfternoonMarketingTime(0l);
    }


    /**
     * 早市 起点
     *
     * @return
     */
    private static Long morningStar() {
        return ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + MORNING_MARKETING_START_TIME, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 早市 终点
     *
     * @return
     */
    private static Long morningEnd() {
        return ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + MORNING_MARKETING_END_TIME, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 午市 起点
     *
     * @return
     */
    private static Long afternoonStar() {
        return ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + AFTERNOON_MARKETING_START_TIME, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 午市 终点
     *
     * @return
     */
    private static Long afternoonEnd() {
        return ApplicationUtils.getMillsFromDate(getCurrentDate() + " " + AFTERNOON_MARKETING_END_TIME, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 当前系统时间
     *
     * @return
     */
    private static Long current() {
        return System.currentTimeMillis();
//        return  ApplicationUtils.getMillsFromDate( ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(System.currentTimeMillis()),"yyyy-MM-dd HH:mm:ss:SSS","HH:mm"),"HH:mm");
    }

    /**
     * 获取当前日期
     *
     * @return
     */
    private static String getCurrentDate() {
        return ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(System.currentTimeMillis()), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy-MM-dd");
    }


}
