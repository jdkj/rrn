package com.eliteams.quick4j.core.util;

/**
 * Created by Dsmart on 2016/11/24.
 */
public class FestivalDayUtils {

    //api请求地址
    private final static String API_URL = "http://tool.bitefu.net/jiari/";

    private final static String PARAM_PREFIX = "?d=";


    /**
     * 当天是否为节假日
     *
     * @return
     */
    public static Boolean isCurrentDayFestivalDay() {
        return isFestivalDay(System.currentTimeMillis());
    }


    /**
     * 指定日期是否为节假日
     * yyyyMMdd
     *
     * @param mills
     * @return true 是节日 false 不是节日
     */
    public static Boolean isFestivalDay(Long mills) {
//        String result = checkFromApi(date);
//        return !result.endsWith("0");
        return !CheckStockOpeningUtils.isStockOpeningDay(mills);
    }

    /**
     * 从api处获取信息
     *
     * @param param
     * @return
     */
    private static String checkFromApi(String param) {
        return HttpRequestClient.sendPost(API_URL + PARAM_PREFIX + param, "");
    }


    /**
     * 获取基于今天的几个工作日后的时间
     *
     * @param days
     * @return
     */
    public static String getSeveralWorkDayBaseOnToday(int days) {
        return getSeveralWorkDay(System.currentTimeMillis(), days);
    }

    public static String getSeveralWorkDay(Long mills, int days) {
        if (isFestivalDay(mills)) {
            mills += 1000 * 60 * 60 * 24;
            return getSeveralWorkDay(mills, days);
        } else {
            if (days > 0) {
                days--;
                mills += 1000 * 60 * 60 * 24;
                if (days > 0) {
                    return getSeveralWorkDay(mills, days);
                }
            }
        }

        return ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(mills), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy-MM-dd HH:mm:ss");
//        return ApplicationUtils.getDateWithMills(mills);
    }


    public static String getSeveralWorkDayBefore(Long mills, int days) {
        if (isFestivalDay(mills)) {
            mills += 1000 * 60 * 60 * 24;
            return getSeveralWorkDay(mills, days);
        } else {
            days++;
            mills += 1000 * 60 * 60 * 24;
            if (days < 0) {
                return getSeveralWorkDay(mills, days);
            }
        }

        return ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(mills), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy-MM-dd HH:mm:ss");
//        return ApplicationUtils.getDateWithMills(mills);
    }


    public static void main(String[] as) {
        Long mills = System.currentTimeMillis();
        System.out.println(getSeveralWorkDayBefore(mills, -5));
    }


}
