package com.eliteams.quick4j.core.util;

import com.eliteams.quick4j.web.model.StockMarket;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class SinaStockDataLoadUtils {

    private final static Logger logger = LoggerFactory.getLogger(SinaStockDataLoadUtils.class);

    /**
     * 请求地址
     */
    private final static String SINA_HOST = "http://hq.sinajs.cn/list=";


    private SinaStockDataLoadUtils() {
    }


    public static List<StockMarket> load(String stockCode) {

        List<StockMarket> stockMarketList = null;

        String result = HttpRequestClient.sendGet(SINA_HOST + stockCode, "", "GBK");

//        logger.error(SINA_HOST + stockCode);

        //返回结果非空
        if (StringUtils.isNotEmpty(result)) {

            stockMarketList = new ArrayList<>();

            StockMarket stockMarket = null;

            String arrs[] = result.split(";");

            for (String arr : arrs) {
                stockMarket = new StockMarket(arr);
                if (StringUtils.isNotEmpty(stockMarket.getCode())) {
                    stockMarketList.add(stockMarket);
                }
            }


        }


        return stockMarketList;

    }


    public static void main(String[] as) {
        String stockCode = "sh600050,sh600233,sz000886,sz300222";
        load(stockCode);
    }


}
