package com.eliteams.quick4j.core.util;

/**
 * 流水号修饰工具
 * //后期可用来扩展分表
 * Created by Dsmart on 2016/11/18.
 */
public class PrimaryKeyModifyUtils {

    /**
     * 根据流水号 流水记录创建时间(yyyy-MM-dd HH:mm:ss) 来合成用于传输中的流水号
     * @param primaryKey
     * @param createTime yyyy-MM-dd HH:mm:ss
     * @return
     */
    public static String encodePrimaryKey(Long primaryKey,String createTime){
        return encodePrimaryKey(primaryKey,createTime,"yyyy-MM-dd HH:mm:ss");
    }


    /**
     * 根据流水号 流水创建时间 来合成用于传输中的流水号
     * @param primaryKey
     * @param createTime
     * @param dateFormat
     * @return
     */
    public static String encodePrimaryKey(Long primaryKey,String createTime,String dateFormat){
        String mills = ApplicationUtils.getMillsFromDate(createTime,dateFormat)/1000+"";//10位
        mills = mills.substring(1);//9位
        return mills+primaryKey;
    }


    /**
     * 根据传输中的流水号获取真实流水号
     * @param primaryKey
     * @return
     */
    public static Long decodePrimaryKey(String primaryKey){
        try{
            return Long.parseLong(primaryKey.substring(9));
        }catch(Exception ex){

        }
        return null;
    }


}
