/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eliteams.quick4j.core.util;

import javax.servlet.http.HttpServletRequest;

/**
 * @description
 * @version 
 * @author Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-7-15, 11:27:13 
 */
public class IPUtils {
    
    
      /**
     * 获取真实的ip地址
     * @param request
     * @return 
     */
    public static String getIpAddr(HttpServletRequest request) {

        //117.26.6.30, 58.67.157.30
        String ip = request.getHeader("x-forwarded-for");
        
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            
            ip = request.getHeader("Proxy-Client-IP");
            
        }
        
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            
            ip = request.getHeader("WL-Proxy-Client-IP");
            
        }
        
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            
            ip = request.getRemoteAddr();
            
        }
        
        if (ip.indexOf(",") > 0) {
            
            ip = ip.substring(0, ip.indexOf(","));
            
            
        }
        
        return ip;
    }
    

}
