package com.eliteams.quick4j.core.util;

import com.eliteams.quick4j.web.model.StockMarket;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dsmart on 2016/11/1.
 */
public class StockDataLoadUtils {

    /**
     * appcode
     */
    private static final String APP_CODE = "c9079fe8926d4c2db20cd1216b9d4a42";


    /**
     * 请求地址
     */
    private static final String HOST = "http://ali-stock.showapi.com";

    /**
     * 请求路径
     */
    private static final String PATH = "/real-stockinfo";

    /**
     * 批量获取数据请求路径
     */
    private static final String BATCH_PARH = "/batch-real-stockinfo";

    /**
     * 请求方式
     */
    private static final String REQUEST_GET = "GET";


    private static String loadSingleStockData(String stockCode) {

        String result = null;

        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + APP_CODE);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("code", stockCode);
        querys.put("needIndex", "0");
        querys.put("need_k_pic", "0");
        try {
            HttpResponse response = HttpUtils.doGet(HOST, PATH, REQUEST_GET, headers, querys);
            result = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }


    private static String loadStocksData(String stockCodes) {

        String result = null;

        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + APP_CODE);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("stocks", stockCodes);
        querys.put("needIndex", "0");
        try {
            HttpResponse response = HttpUtils.doGet(HOST, BATCH_PARH, REQUEST_GET, headers, querys);
            result = EntityUtils.toString(response.getEntity());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }


    /**
     * 刷新股票信息
     *
     * @param stockCode
     * @return
     */
    public static StockMarket loadSingleStock(String stockCode) {
//        String stockString = StockDataLoadUtils.loadSingleStockData(stockCode);
//        StockMarket stockMarket = null;
//        try {
//            JSONObject json = JSONObject.parseObject(stockString);
//
//            JSONObject resBodyjson = json.getJSONObject("showapi_res_body");
//
//            JSONObject stockMarketJson = resBodyjson.getJSONObject("stockMarket");
//            stockMarket = stockMarketJson.toJavaObject(StockMarket.class);
//        } catch (Exception e) {
//            stockMarket = null;
//        }
//        return stockMarket;

        List<StockMarket> list = loadStocks(stockCode);

        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }

        return null;

    }


    /**
     * 批量获取股票信息
     *
     * @param codes
     * @return
     */
    public static List<StockMarket> loadStocks(String codes) {
//        List<StockMarket> stockMarketList = null;
//        if(StringUtils.isNotEmpty(codes)){
//            String results = loadStocksData(codes);
//            try {
//                JSONObject json = JSONObject.parseObject(results);
//                JSONObject resBodyjson = json.getJSONObject("showapi_res_body");
//                JSONArray jsonArray = resBodyjson.getJSONArray("list");
//                if(jsonArray!=null&&!jsonArray.isEmpty()){
//                    stockMarketList = new ArrayList<>();
//                    for(Object item:jsonArray){
//                        stockMarketList.add(((JSONObject)(item)).toJavaObject(StockMarket.class));
//                    }
//                }
//            } catch (Exception e) {
//                stockMarketList = null;
//            }
//        }
//        return stockMarketList;
        return SinaStockDataLoadUtils.load(codes);
    }


    public static void main(String[] as) {

        String code = "sh600050";
        System.err.println(loadSingleStock(code).toString());
    }

}
