package com.eliteams.quick4j.core.util;

import java.math.BigDecimal;

/**
 * Created by Dsmart on 2016/10/31.
 */
public class CurrencyUtils {

    /**
     * 4舍5入保留2位小数
     *
     * @param money
     * @return
     */
    public static Double parseMoenyRoundHalfUp(String money) {
        try {
            return Double.parseDouble(new BigDecimal(money).setScale(2, BigDecimal.ROUND_HALF_UP) + "");
        } catch (Exception ex) {
            return 0.00d;
        }
    }

    public static Double parseMoenyRoundHalfUpWithoutDigits(String money) {
        try {
            return Double.parseDouble(new BigDecimal(money).setScale(0, BigDecimal.ROUND_HALF_UP) + "");
        } catch (Exception ex) {
            return 0.00d;
        }
    }

    /**
     * 返回两数中较大的数值
     * @param baseMoney
     * @param caculateMoney
     * @return
     */
    public static Double max(Double baseMoney,Double caculateMoney){
        Double result = baseMoney.compareTo(caculateMoney)>0?baseMoney:caculateMoney;
        return parseMoenyRoundHalfUp(result+"");
    }


}
