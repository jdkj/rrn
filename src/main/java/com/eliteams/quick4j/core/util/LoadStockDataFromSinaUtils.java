package com.eliteams.quick4j.core.util;

import com.eliteams.quick4j.web.model.StockMarket;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * 从新浪财经获取股票时时数据信息
 * <p>
 * http://hq.sinajs.cn/list=sz000002,sz000651
 * Created by Dsmart on 2017/6/12.
 */
public class LoadStockDataFromSinaUtils {


    private final static String SERVER_HOST = "http://hq.sinajs.cn/list=";


    private LoadStockDataFromSinaUtils() {
    }

    public static List<StockMarket> loadStocks(String codes) {
        if (StringUtils.isNotEmpty(codes)) {
            String rsp = HttpRequestClient.sendGet(SERVER_HOST + codes, "");

            System.out.println(rsp);
        }
        return null;
    }


    public static void main(String as[]) {

        String codes = "sz000002,sz000001";
        System.out.println(loadStocks(codes));
    }


}
