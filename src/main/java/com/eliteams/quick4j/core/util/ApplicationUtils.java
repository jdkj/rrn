package com.eliteams.quick4j.core.util;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ApplicationUtils : 程序工具类，提供大量的便捷方法
 *
 * @author StarZou
 * @since 2014-09-28 22:31
 */
public class ApplicationUtils {

    /**
     * 产生一个36个字符的UUID
     *
     * @return UUID
     */
    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * md5加密
     *
     * @param value 要加密的值
     * @return md5加密后的值
     */
    public static String md5Hex(String value) {
        return DigestUtils.md5Hex(value);
    }

    /**
     * sha1加密
     *
     * @param value 要加密的值
     * @return sha1加密后的值
     */
    public static String sha1Hex(String value) {
        return DigestUtils.sha1Hex(value);
    }

    /**
     * sha256加密
     *
     * @param value 要加密的值
     * @return sha256加密后的值
     */
    public static String sha256Hex(String value) {
        return DigestUtils.sha256Hex(value);
    }


    /**
     * 验证手机号格式是否正确
     *
     * @param value
     * @return
     */
    public static Boolean validPhone(String value) {

        Boolean flag = false;

        try {

            String check = "^((13[0-9])|(14[5,7])|(15[^4,\\D])|(18[0-9]))\\d{8}$";

            Pattern regex = Pattern.compile(check);

            Matcher matcher = regex.matcher(value);

            flag = matcher.matches();

        } catch (Exception ex) {

            flag = false;

        }

        return flag;
    }


    /**
     * 从请求头中获取手机ua
     *
     * @param userAgent
     * @return
     */
    public static String getValue(String userAgent) {

        if (userAgent != null) {

            if (userAgent.indexOf(")") > 0) {

                userAgent = userAgent.substring(0, userAgent.indexOf(")"));

                if (userAgent.indexOf(";") > 0) {

                    String[] arrs = userAgent.split(";");

                    userAgent = arrs[arrs.length - 1];
                }

            }

        }

        if (!StringUtils.isEmpty(userAgent)) {

            userAgent = userAgent.trim().replaceAll(" +", "_");

        }

        return userAgent;

    }

    /**
     * 获取定长的数字字符串
     *
     * @param length 长度
     * @return
     */
    public static String limitedNumsStr(int length) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < length; i++) {
            sb.append(new Random().nextInt(10));
        }

        return sb.toString();
    }


    /**
     * 根据毫秒数获取
     *
     * @param time
     * @return
     */
    public static String getDateWithMills(Long time) {

        String result = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");

        result = simpleDateFormat.format(new Date(time));

        return result;

    }


    /**
     * 获取规定格式的日期
     * <p>
     * 需求：获取规定格式的日期
     * <p>
     * 适用范围：获取规定格式的日期
     * <p>
     * 作者:Dsmart
     * <p>
     * 时间:2013.08.31
     *
     * @param date
     * @param original
     * @param format
     * @return
     */
    public static String formatDate(String date, String original, String format) {

        String result = null;

        SimpleDateFormat originalDateFormat = new SimpleDateFormat(original);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);


        try {
            result = simpleDateFormat.format(originalDateFormat.parse(date).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return result;

    }

    /**
     * 根据日期字符串获取其毫秒数
     *
     * @param date
     * @param originaFormat
     * @return
     */
    public static Long getMillsFromDate(String date, String originaFormat) {
        Long mills = null;
        try {
            SimpleDateFormat originalDateFormat = new SimpleDateFormat(originaFormat);
            mills = originalDateFormat.parse(date).getTime();
        } catch (Exception ex) {

        }
        return mills;
    }


    public static String modifyBankCardNo(String cardNo) {
        String result = null;
        if (org.apache.commons.lang.StringUtils.isNotEmpty(cardNo)) {
            int length = cardNo.length();
            if (length > 4) {
                result = "************" + cardNo.substring(length - 4);
            } else {
                result = "****";
            }
        }
        return result;
    }

    /**
     * 判断今天是否是周末
     *
     * @return
     */
    public static Boolean isTodayWeekend() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        return week == 1 || week == 7;
    }

    /**
     * 判断该时间戳是否为周末
     *
     * @param mills
     * @return
     */
    public static Boolean isWeekend(Long mills) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mills);
        int week = calendar.get(Calendar.DAY_OF_WEEK);
        return week == 1 || week == 7;
    }


    /**
     * 获取定时更新需要延迟的时间
     *
     * @param scheduleTime 定时更新的时间点   HH:mm:ss
     * @return
     */
    public static Long getDelayFroScheduleTask(String scheduleTime) {

        Long oneDayMills = 86400000l;

        Long nowMills = System.currentTimeMillis();

        String nowDate = ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(nowMills), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy-MM-dd");

        String nowFullTime = nowDate + " " + scheduleTime;

        Long scheduleMills = ApplicationUtils.getMillsFromDate(nowFullTime, "yyyy-MM-dd HH:mm:ss");

        Long delay = 0l;

        delay = scheduleMills - nowMills;

        if (delay.compareTo(0l) < 0) {

            delay += oneDayMills;

        }

        return delay;

    }


    /**
     * 获取下个月的今天
     *
     * @param format 日期格式化标准
     * @return
     */
    public static String getTheDayAfterMonth(String format) {
        Calendar cal = Calendar.getInstance();
        cal.add(cal.MONTH, 1);
        SimpleDateFormat dft = new SimpleDateFormat(format);
        String preMonth = dft.format(cal.getTime());
        return preMonth;
    }

    /**
     * 过滤掉emoji表情
     * <p>
     * mysql中utf-8最多只能支持3个字节，但是emoji标签使用utf-8编码很多是3字节以上  会导致数据异常
     *
     * @param source
     * @return
     */
    public static String filterEmoji(String source) {
        if (source != null) {
            Pattern emoji = Pattern.compile("[\ud83c\udc00-\ud83c\udfff]|[\ud83d\udc00-\ud83d\udfff]|[\u2600-\u27ff]", Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
            Matcher emojiMatcher = emoji.matcher(source);
            if (emojiMatcher.find()) {
                source = emojiMatcher.replaceAll("*");
                return source;
            }
            return source;
        }
        return source;
    }


}
