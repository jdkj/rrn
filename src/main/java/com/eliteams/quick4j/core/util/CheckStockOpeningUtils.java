package com.eliteams.quick4j.core.util;

/**
 * Created by Dsmart on 2017/3/29.
 */
public class CheckStockOpeningUtils {

    /**
     * 节假日日期
     */
    private static final String FESTIVAL_DAYS = "20170101|20170102|" +//元旦
            "20170127|20170128|20170129|20170130|20170131|20170201|20170202|" +//春节
            "20170402|20170403|20170404|" +//清明节
            "20170429|20170430|20170501|" +//劳动节
            "20170528|20170529|20170530|" +//端午节
            "20171001|20171002|20171003|20171004|20171005|20171006|20171007|20171008|";//国庆节+中秋节


    private CheckStockOpeningUtils() {
    }


    /**
     * 判断当天是否为开市日
     *
     * @return
     */
    public static Boolean isStockOpeningDay() {
        String date = ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(System.currentTimeMillis()), "yyyy-MM-dd HH:mm:ss:SSS", "yyyyMMdd");
        return !ApplicationUtils.isTodayWeekend() && !FESTIVAL_DAYS.contains(date);
    }


    public static Boolean isStockOpeningDay(Long mills) {
        String date = ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(mills), "yyyy-MM-dd HH:mm:ss:SSS", "yyyyMMdd");
        return !ApplicationUtils.isWeekend(mills) && !FESTIVAL_DAYS.contains(date);
    }

    public static void main(String as[]) {

        System.out.println(isStockOpeningDay());

    }


}
