package com.eliteams.quick4j.core.util;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ConvertBootstrapJson implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2561175274753496191L;


    private ConvertBootstrapJson() {
    }

    /**
     * 特殊处理page数据,添加删除修改时默认page数据为空
     *
     * @param page
     * @return
     */
    public static Map<String, Object> processConvertJsonToMap(Page page) {

        return convertJsonToMap(page);

    }

    private static Map<String, Object> convertJsonToMap(Page page) {

        Map<String, Object> mapResult = new LinkedHashMap<String, Object>();

        boolean resultFlag = false;
        String resultMsg = "数据操作失败";
        List list = page.getResult();
        if (list.size() > 0) {

            resultFlag = true;

            resultMsg = "数据操作成功";

        }

        mapResult.put("resultMsg", resultMsg);

        mapResult.put("resultFlag", resultFlag);

        if (resultFlag) {

            Map<String, Object> pageData = new LinkedHashMap<String, Object>();

            if (page != null) {

                pageData.put("total", page.getTotalCount());

                pageData.put("pageNum", page.getPageNo());

                pageData.put("pageSize", page.getPageSize());
                //System.out.println("list:"+list.toString());
                pageData.put("rows", list);

                mapResult.put("pageData", pageData);

            }

        }

        return mapResult;

    }
}
