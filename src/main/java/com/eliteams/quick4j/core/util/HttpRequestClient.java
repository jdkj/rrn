/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eliteams.quick4j.core.util;


import javax.activation.MimetypesFileTypeMap;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2015-8-22, 17:54:25
 */
public class HttpRequestClient {

    /**
     * ��־��¼����
     */
    private final static Logger log = Logger.getLogger(HttpRequestClient.class.getName());

    /**
     * post����
     */
    private final static String METHOD_POST = "POST";

    /**
     * get����
     */
    private final static String METHOD_GET = "GET";

    /**
     * Ĭ���ַ����
     */
    private final static String CHARSET = "utf-8";

    /**
     * �����ض�������ʽ��������
     *
     * @param strUrl �����ַ
     * @param params ����
     * @param method ����ʽ
     * @return ����󷵻ص����
     */
    private static String request(String strUrl, String params, String method, String charSet) {

        StringBuilder resultBuffer = new StringBuilder();

        InputStream inputStream = null;

        BufferedReader reader = null;

        OutputStreamWriter out = null;

        URL url = null;

        HttpURLConnection connection = null;

        try {

            url = new URL(strUrl);

            connection = (HttpURLConnection) url.openConnection();

            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);

            //��������ʽ
            connection.setRequestMethod(method);

            connection.connect();

            out = new OutputStreamWriter(connection.getOutputStream(), charSet);

            if (params == null) {

                params = "";

            }

            //���Ͳ���
            out.append(params);

            out.flush();

            out.close();

            //������ݵĳ���
            int length = connection.getContentLength();

            inputStream = connection.getInputStream();

            //�з������
            if (length != -1) {

                reader = new BufferedReader(new InputStreamReader(inputStream, charSet));

                String line;

                while ((line = reader.readLine()) != null) {

                    resultBuffer.append(line);
                }

            }

        } catch (Exception ex) {

            log.log(Level.SEVERE, null, ex);

        } finally {

            if (reader != null) {

                try {

                    reader.close();

                } catch (Exception ex) {

                    log.log(Level.SEVERE, null, ex);

                }

            }

            if (inputStream != null) {

                try {

                    inputStream.close();

                } catch (Exception ex) {

                    log.log(Level.SEVERE, null, ex);

                }

            }

            if (connection != null) {

                connection.disconnect();

            }

        }

        return resultBuffer.length() == 0 ? null : resultBuffer.toString();

    }

    public static String postImage(String urlStr, Map<String, String> fileMap) {

        String res = "";
        HttpURLConnection conn = null;
        String BOUNDARY = "---------------------------123821742118716"; //boundary����requestͷ���ϴ��ļ����ݵķָ���  
        try {
            URL url = new URL(urlStr);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(30000);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn
                    .setRequestProperty("User-Agent",
                            "Mozilla/5.0 (Windows; U; Windows NT 6.1; zh-CN; rv:1.9.2.6)");
            conn.setRequestProperty("Content-Type",
                    "multipart/form-data; boundary=" + BOUNDARY);

            OutputStream out = new DataOutputStream(conn.getOutputStream());
            // text  

            // file  
            if (fileMap != null) {
                for (Map.Entry entry : fileMap.entrySet()) {
                    String inputName = (String) entry.getKey();
                    String inputValue = (String) entry.getValue();
                    if (inputValue == null) {
                        continue;
                    }
                    File file = new File(inputValue);
                    String filename = file.getName();
                    String contentType = new MimetypesFileTypeMap()
                            .getContentType(file);
                    if (filename.endsWith(".png")) {
                        contentType = "image/png";
                    } else if (filename.endsWith(".jpg")) {
                        contentType = "image/jpg";

                    }
                    if (contentType == null || contentType.equals("")) {
                        contentType = "application/octet-stream";
                    }

                    StringBuffer strBuf = new StringBuffer();
                    strBuf.append("\r\n").append("--").append(BOUNDARY).append(
                            "\r\n");
                    strBuf.append("Content-Disposition: form-data; name=\""
                            + inputName + "\"; filename=\"" + filename
                            + "\"\r\n");
                    strBuf.append("Content-Type:" + contentType + "\r\n\r\n");

                    out.write(strBuf.toString().getBytes());

                    DataInputStream in = new DataInputStream(
                            new FileInputStream(file));
                    int bytes = 0;
                    byte[] bufferOut = new byte[1024];
                    while ((bytes = in.read(bufferOut)) != -1) {
                        out.write(bufferOut, 0, bytes);
                    }
                    in.close();
                }
            }

            byte[] endData = ("\r\n--" + BOUNDARY + "--\r\n").getBytes();
            out.write(endData);
            out.flush();
            out.close();

            // ��ȡ�������  
            StringBuffer strBuf = new StringBuffer();
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                strBuf.append(line).append("\n");
            }
            res = strBuf.toString();
            reader.close();
            reader = null;
        } catch (Exception e) {
            System.out.println("����POST������?" + urlStr);
            e.printStackTrace();
        } finally {
            if (conn != null) {
                conn.disconnect();
                conn = null;
            }
        }
        return res;

    }

    /**
     * ����post�����ȡ���ص����
     *
     * @param url �����ַ
     * @param params �������
     * @return post���󷵻ص����
     */
    public static String sendPost(String url, String params) {

        return request(url, params, METHOD_POST, CHARSET);

    }

    /**
     * ����get�����ȡ���ص����
     *
     * @param url
     * @param params
     * @return get���󷵻ص����
     */
    public static String sendGet(String url, String params) {

        return request(url, params, METHOD_GET, CHARSET);

    }

    public static String sendPost(String url, String params, String charset) {

        return request(url, params, METHOD_POST, charset);

    }


    public static String sendGet(String url, String params, String charset) {

        return request(url, params, METHOD_GET, charset);

    }


   

}
