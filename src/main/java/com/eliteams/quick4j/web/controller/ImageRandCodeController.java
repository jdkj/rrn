package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.util.ImageRandCodeUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Dsmart on 2017/6/6.
 */
@Controller
@RequestMapping(value = "ImageRandCode/")
public class ImageRandCodeController {


    @RequestMapping
    public void createCode(HttpServletResponse response, HttpSession session) throws IOException {

        Object[] objs = ImageRandCodeUtils.createImage();
        //将验证码存入Session
        session.setAttribute("imageRandCodeValue", objs[0]);
        session.setAttribute("validImageRandCode", 0);
        //将图片输出给浏览器
        BufferedImage image = (BufferedImage) objs[1];
        response.setContentType("image/png");
        OutputStream os = response.getOutputStream();
        ImageIO.write(image, "png", os);

    }


    @RequestMapping(value = "validate/")
    @ResponseBody
    public Result validRandCode(String code, HttpSession session) {
        Result result = new Result("图形验证码错误");
        String imageRandCodeValue = null;
        if (session.getAttribute("imageRandCodeValue") != null) {
            imageRandCodeValue = session.getAttribute("imageRandCodeValue").toString();
        }
        if (imageRandCodeValue != null) {
            if (code.equalsIgnoreCase(imageRandCodeValue)) {
                session.setAttribute("validImageRandCode", 1);
                result.setResultFlag(true);
                result.setResultMsg("图形验证码正确");
            }
        }
        return result;
    }


}
