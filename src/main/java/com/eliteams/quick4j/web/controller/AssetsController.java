package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.model.UserAccount;
import com.eliteams.quick4j.web.model.UserAccountMx;
import com.eliteams.quick4j.web.model.pagerestrict.UserAccountMxRestrict;
import com.eliteams.quick4j.web.service.AgreementService;
import com.eliteams.quick4j.web.service.UserAccountMxService;
import com.eliteams.quick4j.web.service.UserAccountService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Dsmart on 2016/10/22.
 */
@Controller
@RequestMapping(value="/assets/")
public class AssetsController {

    @Resource
    private UserAccountService userAccountService;

    @Resource
    private AgreementService agreementService;

    @Resource
    private UserAccountMxService userAccountMxService;

    @RequestMapping(value="",method = RequestMethod.GET)
    public String index(HttpServletRequest request,Model model){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());
        //可用资金
        Double availableMoney = userAccount.getMoney();
        //合约资金
        List<Agreement> list =agreementService.listAllActivityAgreementByUserId(user.getUserId());
        Double agreementMoney = 0.0d;
        for(Agreement agreement:list){
            agreementMoney+=(agreement.getLockMoney()+agreement.getDiffMoney());
        }
        model.addAttribute("availableMoney",availableMoney);
        model.addAttribute("agreementMoney",agreementMoney);

        return "/web/1.0/dashboard/assets";
    }


    @RequestMapping(value="flowing/")
    public String flowing(HttpServletRequest request, Model model, UserAccountMxRestrict userAccountMxRestrict){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userAccountMxRestrict.setUserId(user.getUserId());
        model.addAttribute("pageResult",userAccountMxService.listByRestrict(userAccountMxRestrict));
        return "/web/1.0/dashboard/account.mx.all";
    }

    @RequestMapping(value="flowing/{pageNo}")
    public String flowing(HttpServletRequest request, Model model, UserAccountMxRestrict userAccountMxRestrict, @PathVariable int pageNo){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userAccountMxRestrict.setUserId(user.getUserId());
        userAccountMxRestrict.setPageno(pageNo);
        model.addAttribute("pageResult",userAccountMxService.listByRestrict(userAccountMxRestrict));
        return "/web/1.0/dashboard/account.mx.all";
    }

    @RequestMapping(value="flowing/c{changeType}/")
    public String flow(HttpServletRequest request, Model model, UserAccountMxRestrict userAccountMxRestrict,@PathVariable int changeType){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userAccountMxRestrict.setUserId(user.getUserId());
        userAccountMxRestrict.setChangeType(changeType);
        model.addAttribute("changeType",changeType);
        model.addAttribute("pageResult",userAccountMxService.listByRestrict(userAccountMxRestrict));
        return "/web/1.0/dashboard/account.mx.category";
    }

    @RequestMapping(value="flowing/c{changeType}/{pageNo}")
    public String flowing(HttpServletRequest request, Model model, UserAccountMxRestrict userAccountMxRestrict,@PathVariable int changeType, @PathVariable int pageNo){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userAccountMxRestrict.setUserId(user.getUserId());
        userAccountMxRestrict.setPageno(pageNo);
        userAccountMxRestrict.setChangeType(changeType);
        model.addAttribute("changeType",changeType);
        model.addAttribute("pageResult",userAccountMxService.listByRestrict(userAccountMxRestrict));
        return "/web/1.0/dashboard/account.mx.category";
    }


}
