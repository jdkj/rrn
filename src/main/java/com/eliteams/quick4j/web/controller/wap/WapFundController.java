package com.eliteams.quick4j.web.controller.wap;


import com.eliteams.quick4j.core.util.CheckMobile;
import com.eliteams.quick4j.core.util.IPUtils;
import com.eliteams.quick4j.web.model.BankAccount;
import com.eliteams.quick4j.web.model.Recharge;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.model.UserAccount;
import com.eliteams.quick4j.web.model.pagerestrict.WxPayRestrict;
import com.eliteams.quick4j.web.service.BankAccountService;
import com.eliteams.quick4j.web.service.RechargeService;
import com.eliteams.quick4j.web.service.UserAccountService;
import com.eliteams.quick4j.web.service.WxPayService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * Created by Dsmart on 2016/11/17.
 */
@RequestMapping(value = "/wap/fund/")
@Controller
public class WapFundController {

    @Resource
    private UserAccountService userAccountService;

    @Resource
    private RechargeService rechargeService;

    @Resource
    private WxPayService wxPayService;


    @Resource
    private BankAccountService bankAccountService;


    @RequestMapping(value = "recharge")
    public String rechargePage(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());
        model.addAttribute("userAccount", userAccount);
        return "/wap/1.0/dashboard/fund.recharge";
    }


    /**
     * 支付订单确认页面
     *
     * @param request
     * @param model
     * @param recharge
     * @return
     */
    @RequestMapping(value = "recharge/confirm/")
    public String rechargeOrderConfirmPage(HttpServletRequest request, Model model, Recharge recharge) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        recharge.setUserId(user.getUserId());
        recharge.setIp(IPUtils.getIpAddr(request));
        if (rechargeService.applyRecharge(recharge)) {//订单生成成功
            recharge = rechargeService.selectById(recharge.getRechargeId());
            UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());
            model.addAttribute("userAccount", userAccount);
            model.addAttribute("recharge", recharge);
            //微信内置浏览器
            if (CheckMobile.isFromMobileDeviceWeChat(request.getHeader("user-agent"))) {
                //根据用户授权信息获取到相应的调起支付数据
                WxPayRestrict wxPayRestrict = new WxPayRestrict();
                wxPayRestrict.setRequest(request);
                wxPayRestrict.setUser(user);
                wxPayRestrict.setOrderId(recharge.getShowRechargeId());

                Map map = (Map) wxPayService.jsAPIWxPay(wxPayRestrict).getData();
                model.addAttribute("appId", map.get("appId"));
                model.addAttribute("timeStamp", map.get("timeStamp"));
                model.addAttribute("nonceStr", map.get("nonceStr"));
                model.addAttribute("package", map.get("package"));
                model.addAttribute("paySign", map.get("paySign"));
                return "/wap/1.0/dashboard/fund.recharge.wx.jsapi";


            }
            return "/wap/1.0/dashboard/fund.recharge.wx.scan";
        } else {
            return "redirect:/error";
        }
    }


    @RequestMapping(value = "withdrawals/")
    public String withdrawalPage(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());
        model.addAttribute("userAccount", userAccount);
        BankAccount bankAccount = bankAccountService.selectByUserId(user.getUserId());
        model.addAttribute("bankAccount", bankAccount);
        return "/wap/1.0/dashboard/fund.withdrawal";
    }



}
