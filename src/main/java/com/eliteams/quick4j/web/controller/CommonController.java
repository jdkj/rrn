package com.eliteams.quick4j.web.controller;


import com.eliteams.quick4j.core.util.CheckMobile;
import com.eliteams.quick4j.web.model.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;


/**
 * 公共视图控制器
 *
 * @author starzou
 * @since 2014年4月15日 下午4:16:34
 **/
@Controller
public class CommonController {
    /**
     * 首页
     *
     * @param request
     * @return
     */
    @RequestMapping("index")
    public String index(HttpServletRequest request) {

        if (CheckMobile.isFromMobileDeviceWeChat(request.getHeader("user-agent"))) {//移动微信端访问
            Subject subject = SecurityUtils.getSubject();
            User user = (User) subject.getSession().getAttribute("userInfo");
            if (user == null) {
                return "redirect:/rest/wap/page/login";
            }
            return "redirect:/rest/wap/page/index";
        } else if (CheckMobile.check(request.getHeader("user-agent"))) {//移动非微信端访问
            return "redirect:/rest/wap/page/index";
        }
        return "/web/1.0/index";
    }


    @RequestMapping("error")
    public String error(HttpServletRequest request) {
        if (request.getMethod().equals("post")) {
            return "redirect:/rest/err";
        }
        if (CheckMobile.check(request.getHeader("user-agent"))) {//移动端访问
            return "/wap/1.0/error/error";
        }
        return "/web/1.0/error/error";
    }


    @RequestMapping("/s/{shareNo}")
    public String share(HttpServletRequest request, @PathVariable Long shareNo) {
        request.getSession().setAttribute("shareId", shareNo);
        return "redirect:/";
    }


}