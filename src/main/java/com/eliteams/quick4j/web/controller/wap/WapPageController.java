package com.eliteams.quick4j.web.controller.wap;

import com.eliteams.quick4j.core.util.CheckMobile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Dsmart on 2016/11/12.
 */
@Controller
@RequestMapping(value = "/wap/page/")
public class WapPageController {

    @RequestMapping(value = "index")
    public String indexPage(HttpServletRequest request, Model model) {
        if (CheckMobile.isFromMobileDeviceWeChat(request.getHeader("user-agent"))) {
            model.addAttribute("isWeChat", true);
        }
        return "/wap/1.0/index";
    }

    @RequestMapping(value = "login")
    public String loginPage(HttpServletRequest request) {
        if (CheckMobile.isFromMobileDeviceWeChat(request.getHeader("user-agent"))) {
            return "/wap/1.0/login.wx";
        }
        return "/wap/1.0/login";
    }

    /**
     * 关于首页
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "about/")
    public String aboutPage(HttpServletRequest request) {
        return "/wap/1.0/about/index";
    }

    @RequestMapping(value = "share/")
    public String sharePage(HttpServletRequest request) {
        return "/wap/1.0/distribution/index";
    }

    @RequestMapping(value = "about/introduction/")
    public String companyIntroductionPage(HttpServletRequest request) {
        return "/wap/1.0/about/company.introduction";
    }



}
