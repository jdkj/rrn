package com.eliteams.quick4j.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "ai/")
public class AiController {

    @RequestMapping(value = "{showAgreementId}")
    public String index(HttpServletRequest request, Model model, @PathVariable String showAgreementId) {
        model.addAttribute("showAgreementId", showAgreementId);
        return "wap/1.0/ai/index";
    }


    @RequestMapping(value = "{showAgreementId}/filterStock/")
    public String filterStockPage(HttpServletRequest request, Model model, @PathVariable String showAgreementId) {
        model.addAttribute("showAgreementId", showAgreementId);
        return "wap/1.0/ai/filter.stock";
    }

    @RequestMapping(value = "stockResults/")
    @ResponseBody
    public List<Map> listStockResults(HttpServletRequest request) {
        List<Map> stockBaseList = new ArrayList<>();
        Map map = new HashMap();

        map.put("name", "中国联通");
        map.put("ticketno", "600050");
        map.put("price", 7.32);
        map.put("status", 1);
        stockBaseList.add(map);
//        map = new HashMap();
//
//        map.put("name", "中国天天");
//        map.put("ticketno", "600051");
//        map.put("price", 37.32);
//        map.put("status", 0);
//        stockBaseList.add(map);
        return stockBaseList;
    }


}
