package com.eliteams.quick4j.web.controller.wap;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.util.PrimaryKeyModifyUtils;
import com.eliteams.quick4j.web.model.*;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateTransactionLogRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockPositionRestrict;
import com.eliteams.quick4j.web.service.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Dsmart on 2016/11/15.
 */
@Controller
@RequestMapping(value = "/wap/trade/")
public class WapTradeController {


    @Resource
    private AgreementService agreementService;

    @Resource
    private BorrowAgreementService borrowAgreementService;


    @Resource
    private ForbidStockService forbidStockService;

    @Resource
    private UserAccountService userAccountService;

    @Resource
    private StockPositionService stockPositionService;

    @Resource
    private StockOperateService stockOperateService;


    @Resource
    private StockOperateTransactionLogService stockOperateTransactionLogService;

    /**
     * 我的操盘
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "")
    public String myTradePage(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        AgreementRestrict agreementRestrict = new AgreementRestrict();
        agreementRestrict.setUserId(user.getUserId());
        Page<Agreement> page = agreementService.listAgreement(agreementRestrict);
        model.addAttribute("pageResult", page);
        return "/wap/1.0/dashboard/mytrade";
    }


    /**
     * 我的操盘
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "{pageNo}")
    public String myTradePage(HttpServletRequest request, Model model,@PathVariable int pageNo) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        AgreementRestrict agreementRestrict = new AgreementRestrict();
        agreementRestrict.setUserId(user.getUserId());
        agreementRestrict.setPageno(pageNo);
        Page<Agreement> page = agreementService.listAgreement(agreementRestrict);
        model.addAttribute("pageResult", page);
        return "/wap/1.0/dashboard/mytrade";
    }



    /**
     * 申请操盘页面
     *
     * @param request
     * @param model
     * @param agreementType 合约类型
     * @return
     */
    @RequestMapping(value = "type/{agreementType}/")
    public String applyTradePage(HttpServletRequest request, Model model, @PathVariable int agreementType) {
        //该类型下的合约列表
        List<BorrowAgreement> borrowAgreementList = borrowAgreementService.listBorrowAgreement(agreementType);
        //禁买股票
        List<ForbidStock> forbidStocks = forbidStockService.getForbidStocks();
        //当前可申请的合约类别
        List<BorrowAgreement> borrowAgreementTypeList = borrowAgreementService.listActivityBorrowType();

        BorrowAgreement currnetBorrowType = borrowAgreementService.getBorrowTypeByType(agreementType);
        model.addAttribute("borrowAgreementList", borrowAgreementList);
        model.addAttribute("tradeType", agreementType);
        model.addAttribute("forbidStockList", forbidStocks);
        model.addAttribute("borrowAgreementTypeList", borrowAgreementTypeList);
        model.addAttribute("currnetBorrowType", currnetBorrowType);
        return "/wap/1.0/dashboard/trade.apply";
    }

    /**
     * 正在进行中的合约详情
     *
     * @param request
     * @param model
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value = "tradeDetail/{showAgreementId}")
    public String tradeDetail(HttpServletRequest request, Model model, @PathVariable String showAgreementId) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.detail";
        } else {
            return "redirect:/error";
        }
    }

    /**
     * 追加保证金页面
     *
     * @param request
     * @param model
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/additional/")
    public String tradeAdditionalPage(HttpServletRequest request, Model model, @PathVariable String showAgreementId) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            model.addAttribute("agreement", agreement);
            model.addAttribute("userAccount", userAccount);
            return "/wap/1.0/dashboard/trade.additional";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 交易委托---买入
     * @param request
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value = "tradeClient/{showAgreementId}/buy/")
    public String tradeClientBuyPage(HttpServletRequest request, @PathVariable String showAgreementId,Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            List<StockPosition> stockPositionList = stockPositionService.listAllByAgreementId(agreementId);
            model.addAttribute("agreement", agreement);
            model.addAttribute("stockPositionList",stockPositionList);
            return "/wap/1.0/dashboard/trade.tradeclient.buy";
        } else {
            return "redirect:/error";
        }
    }

    /**
     * 交易委托买入
     * @param request
     * @param showAgreementId
     * @param stockCode
     * @param model
     * @return
     */
    @RequestMapping(value="tradeClient/{showAgreementId}/buy/{stockCode}")
    public String tradeClientBuyPageWithStockCode(HttpServletRequest request,@PathVariable String showAgreementId,@PathVariable String stockCode,Model model){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            List<StockPosition> stockPositionList = stockPositionService.listAllByAgreementId(agreementId);
            model.addAttribute("agreement", agreement);
            model.addAttribute("stockPositionList",stockPositionList);
            model.addAttribute("stockCode",stockCode);
            return "/wap/1.0/dashboard/trade.tradeclient.buy";
        } else {
            return "redirect:/error";
        }
    }

    /**
     * 交易委托---卖出
     * @param request
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value = "tradeClient/{showAgreementId}/sell/")
    public String tradeClientSellPage(HttpServletRequest request, @PathVariable String showAgreementId,Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            List<StockPosition> stockPositionList = stockPositionService.listAllByAgreementId(agreementId);
            model.addAttribute("agreement", agreement);
            model.addAttribute("stockPositionList",stockPositionList);
            return "/wap/1.0/dashboard/trade.tradeclient.sell";
        } else {
            return "redirect:/error";
        }
    }

    /**
     * 交易委托买入
     * @param request
     * @param showAgreementId
     * @param stockCode
     * @param model
     * @return
     */
    @RequestMapping(value="tradeClient/{showAgreementId}/sell/{stockCode}")
    public String tradeClientSellPageWithStockCode(HttpServletRequest request,@PathVariable String showAgreementId,@PathVariable String stockCode,Model model){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            List<StockPosition> stockPositionList = stockPositionService.listAllByAgreementId(agreementId);
            model.addAttribute("agreement", agreement);
            model.addAttribute("stockPositionList",stockPositionList);
            model.addAttribute("stockCode",stockCode);
            return "/wap/1.0/dashboard/trade.tradeclient.sell";
        } else {
            return "redirect:/error";
        }
    }

    /**
     * 合约持仓页面
      * @param request
     * @param model
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value="{showAgreementId}/position/")
    public String agreementStockPositionPage(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockPositionRestrict stockPositionRestrict){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockPositionRestrict.setAgreementId(agreementId);
        stockPositionRestrict.setUserId(user.getUserId());
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if (agreement != null) {
            model.addAttribute("pageResult", stockPositionService.listByAgreementId(stockPositionRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.position";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 合约持仓页面-分页数据
     * @param request
     * @param model
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value="{showAgreementId}/position/{pageNo}")
    public String agreementStockPositionPageByPageNo(HttpServletRequest request,Model model,@PathVariable String showAgreementId,@PathVariable int pageNo,StockPositionRestrict stockPositionRestrict){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if (agreement != null) {
            stockPositionRestrict.setAgreementId(agreementId);
            stockPositionRestrict.setUserId(user.getUserId());
            stockPositionRestrict.setPageno(pageNo);
            model.addAttribute("pageResult", stockPositionService.listByAgreementId(stockPositionRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.position";
        } else {
            return "redirect:/error";
        }
    }

    /**
     * 撤单页面
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/waiting/")
    public String stockOperateWaitingPage(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockOperateRestrict stockOperateRestrict){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockOperateRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult", stockOperateService.listAllWaitingStockOperateOfAgreement(stockOperateRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.waiting";
        }else{
            return "redirect:/error";
        }
    }


    /**
     * 撤单页面
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/waiting/{pageNo}")
    public String stockOperateWaitingPageByPageNo(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockOperateRestrict stockOperateRestrict,@PathVariable int pageNo){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockOperateRestrict.setAgreementId(agreementId);
            stockOperateRestrict.setPageno(pageNo);
            model.addAttribute("pageResult", stockOperateService.listAllWaitingStockOperateOfAgreement(stockOperateRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.waiting";
        }else{
            return "redirect:/error";
        }
    }

    /**
     * 查询页面
     * @param request
     * @param model
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/")
    public String stockOperateQueryPage(HttpServletRequest request,Model model,@PathVariable String showAgreementId){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query";
        }else{
            return "redirect:/error";
        }
    }

    /**
     * 当日成交页面数据
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateTransactionLogRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/current/transaction/")
    public String queryCurrentTransactionPage(HttpServletRequest request, Model model, @PathVariable String showAgreementId, StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
//            stockOperateRestrict.setAgreementId(agreementId);
//            model.addAttribute("pageResult", stockOperateService.listCurrentDayDeals(stockOperateRestrict));
            stockOperateTransactionLogRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult", stockOperateTransactionLogService.listCurrentDayDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query.current.transaction";
        }else{
            return "redirect:/error";
        }
    }


    /**
     * 当日成交页面数据
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateTransactionLogRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/current/transaction/{pageNo}")
    public String queryCurrentTransactionPageByPageNo(HttpServletRequest request, Model model, @PathVariable String showAgreementId, StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict, @PathVariable int pageNo) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
//            stockOperateRestrict.setAgreementId(agreementId);
//            stockOperateRestrict.setPageno(pageNo);
//            model.addAttribute("pageResult", stockOperateService.listCurrentDayDeals(stockOperateRestrict));
            stockOperateTransactionLogRestrict.setPageno(pageNo);
            stockOperateTransactionLogRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult", stockOperateTransactionLogService.listCurrentDayDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query.current.transaction";
        }else{
            return "redirect:/error";
        }
    }


    /**
     * 当日委托页面数据
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/current/commission/")
    public String queryCurrentCommissionPage(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockOperateRestrict stockOperateRestrict){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockOperateRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult", stockOperateService.listCurrentDayCommissions(stockOperateRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query.current.commission";
        }else{
            return "redirect:/error";
        }
    }


    /**
     * 当日委托页面数据
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/current/commission/{pageNo}")
    public String queryCurrentCommissionPageByPageNo(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockOperateRestrict stockOperateRestrict,@PathVariable int pageNo){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockOperateRestrict.setAgreementId(agreementId);
            stockOperateRestrict.setPageno(pageNo);
            model.addAttribute("pageResult", stockOperateService.listCurrentDayCommissions(stockOperateRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query.current.commission";
        }else{
            return "redirect:/error";
        }
    }



    /**
     * 历史成交页面数据
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateTransactionLogRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/history/transaction/")
    public String queryHistoryTransactionPage(HttpServletRequest request, Model model, @PathVariable String showAgreementId, StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
//            stockOperateRestrict.setAgreementId(agreementId);
//            model.addAttribute("pageResult", stockOperateService.listHistoryDeals(stockOperateRestrict));
            stockOperateTransactionLogRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult", stockOperateTransactionLogService.listHistoryDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query.history.transaction";
        }else{
            return "redirect:/error";
        }
    }


    /**
     * 历史成交页面数据
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateTransactionLogRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/history/transaction/{pageNo}")
    public String queryHistoryTransactionPageByPageNo(HttpServletRequest request, Model model, @PathVariable String showAgreementId, StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict, @PathVariable int pageNo) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
//            stockOperateRestrict.setAgreementId(agreementId);
//            stockOperateRestrict.setPageno(pageNo);
//            model.addAttribute("pageResult", stockOperateService.listHistoryDeals(stockOperateRestrict));
            stockOperateTransactionLogRestrict.setPageno(pageNo);
            stockOperateTransactionLogRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult", stockOperateTransactionLogService.listHistoryDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query.history.transaction";
        }else{
            return "redirect:/error";
        }
    }


    /**
     * 历史委托页面数据
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/history/commission/")
    public String queryHistoryCommissionPage(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockOperateRestrict stockOperateRestrict){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockOperateRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult", stockOperateService.listHistoryCommissions(stockOperateRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query.history.commission";
        }else{
            return "redirect:/error";
        }
    }


    /**
     * 历史委托页面数据
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value="{showAgreementId}/query/history/commission/{pageNo}")
    public String queryHistoryCommissionPageByPageNo(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockOperateRestrict stockOperateRestrict,@PathVariable int pageNo){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockOperateRestrict.setAgreementId(agreementId);
            stockOperateRestrict.setPageno(pageNo);
            model.addAttribute("pageResult", stockOperateService.listHistoryCommissions(stockOperateRestrict));
            model.addAttribute("agreementId", agreementId);
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.query.history.commission";
        }else{
            return "redirect:/error";
        }
    }

    @RequestMapping(value="tradeHistory/{showAgreementId}")
    public String tradeHistoryPage(HttpServletRequest request,Model model,@PathVariable String showAgreementId){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.history";
        }else{
            return "redirect:/error";
        }
    }

    @RequestMapping(value="tradeHistory/detail/{showAgreementId}")
    public String tradeHistoryDetailPage(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockOperateRestrict stockOperateRestrict){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockOperateRestrict.setUserId(user.getUserId());
            stockOperateRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult",stockOperateService.listAllStockOperatesOfAgreement(stockOperateRestrict));
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.history.detail";
        }else{
            return "redirect:/error";
        }
    }



    @RequestMapping(value="tradeHistory/detail/{showAgreementId}/{pageNo}")
    public String tradeHistoryDetailPageByPageNo(HttpServletRequest request,Model model,@PathVariable String showAgreementId,StockOperateRestrict stockOperateRestrict,@PathVariable int pageNo){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockOperateRestrict.setPageno(pageNo);
            stockOperateRestrict.setUserId(user.getUserId());
            stockOperateRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult",stockOperateService.listAllStockOperatesOfAgreement(stockOperateRestrict));
            model.addAttribute("agreement", agreement);
            return "/wap/1.0/dashboard/trade.history.detail";
        }else{
            return "redirect:/error";
        }
    }


}
