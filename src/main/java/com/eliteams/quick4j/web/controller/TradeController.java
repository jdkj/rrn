package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.core.entity.JSONResult;
import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.util.PrimaryKeyModifyUtils;
import com.eliteams.quick4j.web.container.ForbidStockContainer;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.*;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockPositionRestrict;
import com.eliteams.quick4j.web.service.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 交易类控制器
 * Created by Dsmart on 2016/10/24.
 */
@Controller
@RequestMapping("/trade")
public class TradeController {

    @Resource
    private AgreementService agreementService;

    @Resource
    private BorrowAgreementService borrowAgreementService;


    @Resource
    private ForbidStockService forbidStockService;

    @Resource
    private StockPositionService stockPositionService;

    @Resource
    private StockOperateService stockOperateService;

    @Resource
    private UserAccountService userAccountService;

    @Resource
    private StockMarketService stockMarketService;

    @Resource
    private UserCouponService userCouponService;

    /**
     * 我的操盘
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/")
    public String index(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        AgreementRestrict agreementRestrict = new AgreementRestrict();
        agreementRestrict.setUserId(user.getUserId());
        Page<Agreement> page = agreementService.listAgreement(agreementRestrict);
        model.addAttribute("pageResult", page);
        return "/web/1.0/dashboard/mytrade";
    }


    /**
     * 根据页码获取操盘数据
     *
     * @param request
     * @param model
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "/{pageNo}")
    public String index(HttpServletRequest request, Model model, @PathVariable int pageNo) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        AgreementRestrict agreementRestrict = new AgreementRestrict();
        agreementRestrict.setPageno(pageNo);
        agreementRestrict.setUserId(user.getUserId());
        Page<Agreement> page = agreementService.listAgreement(agreementRestrict);
        model.addAttribute("pageResult", page);
        return "/web/1.0/dashboard/mytrade";
    }

    /**
     * 操盘 短期策略
     *
     * @param request
     * @param model
     * @param agreementType 合约类型
     * @return
     */
    @RequestMapping(value = "/type/{agreementType}")
    public String applyTradePage(HttpServletRequest request, Model model, @PathVariable int agreementType) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        //该类型下的合约列表
        List<BorrowAgreement> borrowAgreementList = borrowAgreementService.listBorrowAgreement(agreementType);
        //禁买股票
        List<ForbidStock> forbidStocks = forbidStockService.getForbidStocks();
        //当前可申请的合约类别
        List<BorrowAgreement> borrowAgreementTypeList = borrowAgreementService.listActivityBorrowType();

        BorrowAgreement currentBorrowType = borrowAgreementService.getBorrowTypeByType(agreementType);
        List<UserCoupon> userCouponList = userCouponService.listAvailableUserCouponByUserId(user.getUserId());
        model.addAttribute("borrowAgreementList", borrowAgreementList);
        model.addAttribute("tradeType", agreementType);
        model.addAttribute("forbidStockList", forbidStocks);
        model.addAttribute("borrowAgreementTypeList", borrowAgreementTypeList);
        model.addAttribute("currentBorrowType", currentBorrowType);
        model.addAttribute("userCouponList", userCouponList);
        return "/web/1.0/dashboard/trade";
    }


    @RequestMapping(value = "/sign")
    @ResponseBody
    public Result signAgreement(HttpServletRequest request, AgreementRestrict agreementRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        agreementRestrict.setUserId(user.getUserId());
        return agreementService.signAgreement(agreementRestrict);
    }

    /**
     * 合约详细信息页面
     *
     * @param request
     * @param showAgreementId
     * @param model
     * @param stockPositionRestrict
     * @return
     */
    @RequestMapping(value = "/tradeDetail/{showAgreementId}")
    public String tradeDetail(HttpServletRequest request, @PathVariable String showAgreementId, Model model, StockPositionRestrict stockPositionRestrict) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            //合约内容
            model.addAttribute("agreement", agreement);
            UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());
            stockPositionRestrict.setAgreementId(agreementId);
            stockPositionRestrict.setUserId(user.getUserId());
            model.addAttribute("pageResult", stockPositionService.listByAgreementId(stockPositionRestrict));
            model.addAttribute("userAccount", userAccount);
            return "/web/1.0/dashboard/tradedetail";
        } else {
            return "redirect:/error";
        }

    }

    @RequestMapping(value = "/tradeDetail/{showAgreementId}/{pageNo}")
    public String tradeDetailByPageNo(HttpServletRequest request, @PathVariable String showAgreementId, @PathVariable int pageNo, Model model, StockPositionRestrict stockPositionRestrict) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            //合约内容
            model.addAttribute("agreement", agreement);
            UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());
            stockPositionRestrict.setAgreementId(agreementId);
            stockPositionRestrict.setUserId(user.getUserId());
            stockPositionRestrict.setPageno(pageNo);
            model.addAttribute("pageResult", stockPositionService.listByAgreementId(stockPositionRestrict));
            model.addAttribute("userAccount", userAccount);
            return "/web/1.0/dashboard/tradedetail";
        } else {
            return "redirect:/error";
        }

    }

    /**
     * 结算合约列表详细页面
     *
     * @param request
     * @param showAgreementId
     * @param model
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value = "/tradeHistory/{showAgreementId}")
    public String tradeHistoryDetail(HttpServletRequest request, @PathVariable String showAgreementId, Model model, StockOperateRestrict stockOperateRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            stockOperateRestrict.setUserId(user.getUserId());
            stockOperateRestrict.setAgreementId(agreementId);
            model.addAttribute("pageResult", stockOperateService.listAllStockOperatesOfAgreement(stockOperateRestrict));
            model.addAttribute("agreement", agreement);
            return "/web/1.0/dashboard/tradehistory";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 结算合约列表详细页面
     *
     * @param request
     * @param showAgreementId
     * @param model
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value = "/tradeHistory/{showAgreementId}/{pageNo}")
    public String tradeHistoryDetailByPage(HttpServletRequest request, @PathVariable String showAgreementId, @PathVariable int pageNo, Model model, StockOperateRestrict stockOperateRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            stockOperateRestrict.setUserId(user.getUserId());
            stockOperateRestrict.setAgreementId(agreementId);
            stockOperateRestrict.setPageno(pageNo);
            model.addAttribute("pageResult", stockOperateService.listAllStockOperatesOfAgreement(stockOperateRestrict));
            model.addAttribute("agreement", agreement);
            return "/web/1.0/dashboard/tradehistory";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 委托交易---买入
     *
     * @param request
     * @param model
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value = "/tradeClient/{showAgreementId}/buy")
    public String tradeClientBuy(HttpServletRequest request, Model model, @PathVariable String showAgreementId) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            model.addAttribute("agreement", agreement);//合约类型
            model.addAttribute("agreementId", showAgreementId);
            return "/web/1.0/dashboard/tradeclient.buy";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 委托交易-----买入
     *
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockCode
     * @return
     */
    @RequestMapping(value = "/tradeClient/{showAgreementId}/buy/{stockCode}")
    public String tradeClientBuyStock(HttpServletRequest request, Model model, @PathVariable String showAgreementId, @PathVariable String stockCode) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            model.addAttribute("agreement", agreement);//合约类型
            model.addAttribute("stockCode", stockCode);
            model.addAttribute("agreementId", showAgreementId);
            return "/web/1.0/dashboard/tradeclient.buy";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 委托交易---卖出
     *
     * @param request
     * @param model
     * @param showAgreementId
     * @return
     */
    @RequestMapping(value = "/tradeClient/{showAgreementId}/sell")
    public String tradeClientSell(HttpServletRequest request, Model model, @PathVariable String showAgreementId) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            model.addAttribute("agreement", agreement);//合约类型
            model.addAttribute("agreementId", showAgreementId);
            return "/web/1.0/dashboard/tradeclient.sell";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 委托交易-----卖出
     *
     * @param request
     * @param model
     * @param showAgreementId
     * @param stockCode
     * @return
     */
    @RequestMapping(value = "/tradeClient/{showAgreementId}/sell/{stockCode}")
    public String tradeClientSellStock(HttpServletRequest request, Model model, @PathVariable String showAgreementId, @PathVariable String stockCode) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(), agreementId);
        if (agreement != null) {
            model.addAttribute("agreement", agreement);//合约类型
            model.addAttribute("stockCode", stockCode);
            model.addAttribute("agreementId", showAgreementId);
            return "/web/1.0/dashboard/tradeclient.sell";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 查询股票数据信息
     *
     * @param request
     * @param stockPosition
     * @return
     */
    @RequestMapping(value = "/queryStock")
    @ResponseBody
    public JSONResult<Map> queryStockInfo(HttpServletRequest request, StockPosition stockPosition) {
        JSONResult<Map> jsonResult = new JSONResult<>();
        String stockCode = stockPosition.getStockCode();
        if (ForbidStockContainer.isForbiden(stockCode)) {
            jsonResult.setResponseStatusEnums(ResponseStatusEnums.STOCK_IS_FORBID);
            return jsonResult;
        }
        StockMarket stockMarket = stockMarketService.stockInfo(stockCode);
        if (stockMarket != null) {
            if (stockMarket.getOpenPrice().compareTo(0d) == 0) {
                 jsonResult.setResultFlag(false);
                 jsonResult.setResultMsg("该股票已停牌");
                return jsonResult;
            } else {
                jsonResult.setResultFlag(true);
                jsonResult.setResultMsg("股票信息获取成功");
            }
        } else {
            jsonResult.setResultFlag(false);
            jsonResult.setResultMsg("股票信息获取失败");
            return jsonResult;
        }
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");


        Long agreementId = stockPosition.getAgreementId();
        Agreement agreement = agreementService.selectById(agreementId);

        StockPosition orginalStockPositon = stockPositionService.getStockPositionByAgreementIdAndUserIdAndStockCode(agreementId, user.getUserId(), stockCode);
        Long stockAvailableNum = 0l;
        if (orginalStockPositon != null) {
            stockAvailableNum = orginalStockPositon.getStockAvailableNum();
        }
        Map map = new HashMap();
        Map data = new HashMap();
        data.put("stockAvailableNum", stockAvailableNum);
        data.put("avaiableCredit", agreement.getAvailableCredit());
        map.put("stockMarket", stockMarket);
        map.put("agreementInfo", data);
        jsonResult.setData(map);
        return jsonResult;
    }


    @RequestMapping(value = "/operate/buy")
    @ResponseBody
    public Result stockOperateBuy(HttpServletRequest request, StockOperateRestrict stockOperateRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        stockOperateRestrict.setUserId(user.getUserId());
        return stockOperateService.operateBuy(stockOperateRestrict);
    }

    @RequestMapping(value = "/operate/sell")
    @ResponseBody
    public Result stockOperateSell(HttpServletRequest request, StockOperateRestrict stockOperateRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        stockOperateRestrict.setUserId(user.getUserId());
        return stockOperateService.operateSell(stockOperateRestrict);
    }


    /**
     * 撤单
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/operate/cancel", method = RequestMethod.POST)
    @ResponseBody
    public Result cancelOperate(HttpServletRequest request, Long operateId) {
        return stockOperateService.cancelStockOperateApply(operateId);
    }


}
