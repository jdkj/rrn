package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.core.entity.JSONResult;
import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.core.util.IPUtils;
import com.eliteams.quick4j.core.util.IdcardValidatorUtil;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.*;
import com.eliteams.quick4j.web.model.pagerestrict.UserRestrict;
import com.eliteams.quick4j.web.service.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户控制器
 *
 * @author StarZou
 * @since 2014年5月28日 下午3:54:00
 **/
@Controller
@RequestMapping(value = "/user")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private RandCodeService randCodeService;

    @Resource
    private UserIdAuthService userIdAuthService;

    @Resource
    private BankService bankService;

    @Resource
    private BankAccountService bankAccountService;

    @Resource
    private DistributionApi distributionApi;


    /**
     * 用户登录
     *
     * @param user
     * @param result
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(@Valid User user, BindingResult result, Model model, HttpServletRequest request) {
        try {
            Subject subject = SecurityUtils.getSubject();
            // 已登陆则 跳到首页
            if (subject.isAuthenticated()) {
                return "redirect:/";
            }
            if (result.hasErrors()) {
                model.addAttribute("error", "参数错误！");
                return "/web/1.0/login";
            }
            // 身份验证
            subject.login(new UsernamePasswordToken(user.getPhone(), user.getUserPassword()));
            // 验证成功在Session中保存用户信息
            final User authUserInfo = userService.selectByPhone(user.getPhone());
            request.getSession().setAttribute("userInfo", authUserInfo);
            DistributionUser distributionUser = distributionApi.getByUserId(authUserInfo.getUserId());
            request.getSession().setAttribute("distributionUser", distributionUser);

            UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();

            if (userParamInfoTag.isBankTag()) {
                BankAccount bankAccount = bankAccountService.selectByUserId(user.getUserId());
                request.getSession().setAttribute("bankAccount", bankAccount);
            }
        } catch (AuthenticationException e) {
            // 身份验证失败
            model.addAttribute("error", "用户名或密码错误 ！");
            return "/web/1.0/login";
        }
        return "redirect:/";
    }


    /**
     * 用户帐号密码登录 ajax
     *
     * @param user
     * @param request
     * @return
     */
    @RequestMapping(value = "/login/asyc", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult login(User user, HttpServletRequest request) {
        JSONResult json = new JSONResult();
        try {
            Subject subject = SecurityUtils.getSubject();
            // 身份验证
            subject.login(new UsernamePasswordToken(user.getPhone(), user.getUserPassword()));
            // 验证成功在Session中保存用户信息
            user = userService.authenticationByPhoneAndPassword(user);
        } catch (AuthenticationException e) {
            json.setResultFlag(false);
            json.setResultMsg("密码错误!");
        }
        if (user != null && user.getUserId() != null) {
            json.setResultFlag(true);
            json.setResultMsg("登录成功");
            request.getSession().setAttribute("userInfo", user);
            DistributionUser distributionUser = distributionApi.getByUserId(user.getUserId());
            request.getSession().setAttribute("distributionUser", distributionUser);
            UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();

            if (userParamInfoTag.isBankTag()) {
                BankAccount bankAccount = bankAccountService.selectByUserId(user.getUserId());
                request.getSession().setAttribute("bankAccount", bankAccount);
            }
        } else {
            json.setResultFlag(false);
            json.setResultMsg("登录失败");
        }
        return json;
    }

    /**
     * 用户登出
     *
     * @param session
     * @return
     */
    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {
        session.removeAttribute("userInfo");
        // 登出操作
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return "/web/1.0/login";
    }


    /**
     * 用户注册
     *
     * @param user
     * @param randCode
     * @param request
     * @return
     */
    @RequestMapping(value = "/registe", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult<Map> registe(User user, RandCode randCode, HttpServletRequest request) {
        JSONResult<Map> json = new JSONResult<>();
        if (userService.validatePhone(randCode.getPhone())) {
            if (randCodeService.validPhoneCode(randCode)) {
                //当前手机号没有注册
                if (!userService.isPhoneRegisted(user.getPhone())) {
                    if (userService.registeByPhone(user)) {
                        json.setResultFlag(true);
                        json.setResultMsg("注册成功");
                        Long shareId = 0l;
                        try {
                            shareId = Long.parseLong(request.getSession().getAttribute("shareId").toString());
                        } catch (Exception ex) {

                        }
                        distributionApi.bindDistributionRelation(user.getUserId(), shareId);
                        distributionApi.updateByUser(user);

                    } else {
                        json.setResultFlag(false);
                        json.setResultMsg("注册失败");
                    }

                } else {//当前手机号已经注册
                    user = userService.selectByPhone(user.getPhone());
                    json.setResultFlag(true);
                    json.setResultMsg("登录成功");
                }

                //用户注册成功(登录成功)
                if (json.getResultFlag()) {
                    Subject subject = SecurityUtils.getSubject();
                    user = userService.selectById(user.getUserId());
                    subject.login(new UsernamePasswordToken(user.getPhone(), "六六好帅"));
                    //将用户信息保存到session中
                    request.getSession().setAttribute("userInfo", user);
                    DistributionUser distributionUser = distributionApi.getByUserId(user.getUserId());
                    request.getSession().setAttribute("distributionUser", distributionUser);
                    UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();

                    if (userParamInfoTag.isBankTag()) {
                        BankAccount bankAccount = bankAccountService.selectByUserId(user.getUserId());
                        request.getSession().setAttribute("bankAccount", bankAccount);
                    }
                    Map map = new HashMap();
                    map.put("user_info", user);
                    json.setData(map);
                }
            } else {
                json.setResultFlag(false);
                json.setResultMsg("短信验证码错误");
            }
        } else {
            json.setResponseStatusEnums(ResponseStatusEnums.PHONE_FORMAT_ERROR);
        }
        return json;
    }


    /**
     * 验证手机号格式
     *
     * @param phone
     * @return
     */
    @RequestMapping(value = "/validPhone", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult validPhoneForrmat(String phone) {

        JSONResult json = new JSONResult();

        if (phone != null) {
            Boolean flag = userService.validatePhone(phone);
            json.setResultFlag(flag);
            if (flag) {
                json.setResponseStatusEnums(ResponseStatusEnums.PHONE_FORMAT_SUC);
            } else {
                json.setResponseStatusEnums(ResponseStatusEnums.PHONE_FORMAT_ERROR);
            }
        } else {
            json.setResultFlag(false);
            json.setResponseStatusEnums(ResponseStatusEnums.REQUEST_PARAMS_ERROR);
        }

        return json;
    }

    /**
     * 请求获取短信验证码
     *
     * @param user
     * @param request
     * @return
     */
    @RequestMapping(value = "/reqCode")
    @ResponseBody
    public JSONResult reqPhoneCode(User user, HttpServletRequest request, Integer reqType) {
        HttpSession session = request.getSession();

        Boolean isImageCodeValid = false;
        Boolean isLogin = false;

        if (session.getAttribute("userInfo") != null) {
            isLogin = true;
        }

        if (session.getAttribute("validImageRandCode") != null && "1".equals(session.getAttribute("validImageRandCode").toString())) {
            isImageCodeValid = true;
        }

        JSONResult json = validPhoneForrmat(user.getPhone());
        if (isLogin || isImageCodeValid) {
            if (json.getResultFlag()) {
                RandCode randCode = new RandCode();
                randCode.setType(reqType);
                randCode.setPhone(user.getPhone());
                randCode.setClient_info(ApplicationUtils.getValue(request.getHeader("user-agent")));
                randCode.setIp(IPUtils.getIpAddr(request));
                ResponseStatusEnums rsp = randCodeService.validRequest(randCode);
                //没有被限制
                if (rsp == ResponseStatusEnums.OPERATE_SUCCESS) {
                    randCode = randCodeService.reqPhoneCode(randCode);
                    json.setResultFlag(true);
                    json.setData(randCode);
                } else {
                    json.setResultFlag(false);
                    json.setResponseStatusEnums(rsp);
                }
            }
        } else {
            json.setResultFlag(false);
            json.setResultMsg("图形验证码错误");
        }
        return json;
    }


    /**
     * 个人设置页面
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "/setting", method = RequestMethod.GET)
    public String setting(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();
        if (userParamInfoTag.isIdTag()) {
            UserIdAuth userIdAuth = userIdAuthService.getAuthInfoByUserId(user.getUserId());
            request.getSession().setAttribute("userIdAuth", userIdAuth);//好多页面都有提现  提现需要绑定银行卡  绑定银行卡前需要认证
        }

        if (userParamInfoTag.isBankTag()) {
            BankAccount bankAccount = bankAccountService.selectByUserId(user.getUserId());
            request.getSession().setAttribute("bankAccount", bankAccount);
        }
        return "/web/1.0/dashboard/setting";
    }

    /**
     * 用户身份证信息认证
     *
     * @param userIdAuth
     * @param request
     * @return
     */
    @RequestMapping(value = "/authId", method = RequestMethod.POST)
    @ResponseBody
    public Result authId(UserIdAuth userIdAuth, HttpServletRequest request) {
        Result result = new Result("认证失败");
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userIdAuth.setUserId(user.getUserId());
        if (IdcardValidatorUtil.isValidatedAllIdcard(userIdAuth.getIdNo())) {
            if (userIdAuthService.addItem(userIdAuth)) {
                user = userService.selectById(user.getUserId());
                request.getSession().setAttribute("userInfo", user);
                result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
                result.setResultMsg("身份信息认证成功!");
                request.getSession().setAttribute("userIdAuth", userIdAuth);
            } else {
                result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_FAIL);
                result.setResultMsg("该身份信息已被认证!");
            }
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.ID_CARD_NO_ILLEGAL);
        }
        return result;
    }

    /**
     * 修改手机号前 验证原有手机号
     *
     * @param randCode
     * @param request
     * @return
     */
    @RequestMapping(value = "/modify/phone/codeValidate")
    @ResponseBody
    public Result validPhoneAndCode(RandCode randCode, HttpServletRequest request) {
        Result result = new Result("验证失败");
        if (userService.validatePhone(randCode.getPhone())) {
            if (randCodeService.validPhoneCode(randCode)) {
                result.setResponseStatusEnums(ResponseStatusEnums.PHONE_CODE_VALIDATE_SUC);
            } else {
                result.setResponseStatusEnums(ResponseStatusEnums.PHONE_CODE_VALIDATE_FAIL);
            }
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.PHONE_FORMAT_ERROR);
        }
        return result;
    }

    /**
     * 设置或修改手机号
     *
     * @param randCode
     * @param request
     * @return
     */
    @RequestMapping(value = "/modify/phone", method = RequestMethod.POST)
    @ResponseBody
    public Result modifyPhone(RandCode randCode, HttpServletRequest request) {
        Result result = new Result("手机号绑定失败");
        //手机号格式验证通过
        if (userService.validatePhone(randCode.getPhone())) {
            if (randCodeService.validPhoneCode(randCode)) {
                Subject subject = SecurityUtils.getSubject();
                User user = (User) subject.getSession().getAttribute("userInfo");
                UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();
                boolean phoneTag = userParamInfoTag.isPhoneTag();
                if (phoneTag) {//修改手机号
                    //当前手机号未被注册
                    if (!userService.isPhoneRegisted(randCode.getPhone())) {
                        //这里是否还需要进行服务器校验？

                        user.setPhone(randCode.getPhone());
                        userService.updateUserPhone(user);
                        distributionApi.updateByUser(user);
                        result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
                        result.setResultMsg("手机号修改成功");

                    } else {
                        result.setResponseStatusEnums(ResponseStatusEnums.PHONE_REGISTED_ERROR);//手机号已被注册 不可使用
                    }
                } else {//绑定手机号
                    User orginalUser = userService.selectByPhone(randCode.getPhone());
                    if (orginalUser != null) {//欲绑定的手机已经注册了，则将通过第三方平台注册的用户信息与原用户信息合并
                        userService.mergeWxUserToOrginalUser(user, orginalUser);
                        orginalUser.setUserImage(user.getUserImage());
                        request.getSession().setAttribute("userInfo", orginalUser);
                        distributionApi.updateByUser(orginalUser);
                    } else {//需要绑定的手机号是全新的
                        user.setPhone(randCode.getPhone());
                        userService.updateUserPhone(user);
                        distributionApi.updateByUser(user);
                    }
                    result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
                    result.setResultMsg("手机号绑定成功");
                }

            } else {
                result.setResponseStatusEnums(ResponseStatusEnums.PHONE_CODE_VALIDATE_FAIL);//短信验证失败
            }
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.PHONE_FORMAT_ERROR);//手机号格式验证不通过
        }
        return result;
    }


    /**
     * 用户修改或设置登录密码
     *
     * @param userRestrict
     * @param request
     * @return
     */
    @RequestMapping(value = "/modify/password", method = RequestMethod.POST)
    @ResponseBody
    public Result modifyPassword(UserRestrict userRestrict, HttpServletRequest request) {
        Result result = new Result("密码修改成功");
        Subject subject = SecurityUtils.getSubject();
        User orginalUser = (User) subject.getSession().getAttribute("userInfo");
        UserParamInfoTag userParamInfoTag = orginalUser.getUserParamInfoTags();
        boolean passwordTag = userParamInfoTag.isPasswordTag();

        //修改密码
        if (passwordTag) {

            //校验用户原密码是否输入正确
            User user = new User();
            user.setUserId(orginalUser.getUserId());
            user.setUserPassword(userRestrict.getOrginalPassword());
            if (userService.validateUserIdAndPassword(user)) {//校验通过
                userParamInfoTag.setPasswordTag(true);
                orginalUser.setUserParamInfoTags(userParamInfoTag);
            } else {
                result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_FAIL);
                result.setResultMsg("原密码错误!");
                return result;
            }
        } else {//设置密码

        }

        orginalUser.setUserPassword(userRestrict.getPassword());
        if (userService.updatePassword(orginalUser)) {
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
            result.setResultMsg("密码" + (passwordTag ? "修改" : "设置") + "成功");
            request.getSession().setAttribute("userInfo", orginalUser);
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_FAIL);
            result.setResultMsg("密码" + (passwordTag ? "修改" : "设置") + "失败");
        }

        return result;
    }

    /**
     * 修改或设置交易密码
     *
     * @param userRestrict
     * @param request
     * @return
     */
    @RequestMapping(value = "/modify/accountpassword", method = RequestMethod.POST)
    @ResponseBody
    public Result modifyAccountPassword(UserRestrict userRestrict, HttpServletRequest request) {
        Result result = new Result("交易密码修改成功");
        Subject subject = SecurityUtils.getSubject();
        User orginalUser = (User) subject.getSession().getAttribute("userInfo");
        UserParamInfoTag userParamInfoTag = orginalUser.getUserParamInfoTags();
        boolean accountPasswordTag = userParamInfoTag.isAccountPasswordTag();

        //修改密码
        if (accountPasswordTag) {

            //校验用户原交易密码是否输入正确
            User user = new User();
            user.setUserId(orginalUser.getUserId());
            user.setAccountPassword(userRestrict.getOrginalAccountPassword());
            if (userService.validateUserIdAndAccountPassword(user)) {//校验通过
                userParamInfoTag.setAccountPasswordTag(true);
                orginalUser.setUserParamInfoTags(userParamInfoTag);
            } else {
                result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_FAIL);
                result.setResultMsg("原交易密码错误!");
                return result;
            }
        } else {//设置交易密码

        }

        orginalUser.setAccountPassword(userRestrict.getAccountPassword());
        if (userService.updateAccountPassword(orginalUser)) {
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
            result.setResultMsg("交易密码" + (accountPasswordTag ? "修改" : "设置") + "成功");
            request.getSession().setAttribute("userInfo", orginalUser);
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_FAIL);
            result.setResultMsg("交易密码" + (accountPasswordTag ? "修改" : "设置") + "失败");
        }

        return result;
    }

    /***
     * 添加银行账户页面
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/addbank", method = RequestMethod.GET)
    public String addBankPage(HttpServletRequest request, Model model) {
        model.addAttribute("bankList", bankService.listActivity());
        return "/web/1.0/dashboard/addbank";
    }


    /**
     * 添加银行账户信息
     *
     * @param request
     * @param bank
     * @param bankAccount
     * @return
     */
    @RequestMapping(value = "addBank", method = RequestMethod.POST)
    @ResponseBody
    public Result addBank(HttpServletRequest request, Bank bank, BankAccount bankAccount) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        bankAccount.setUserId(user.getUserId());
        Result result = userService.updateUserBankAccount(bank.getBankId(), bankAccount);
        if (result.getResultFlag()) {
            user = userService.selectById(user.getUserId());
            request.getSession().setAttribute("userInfo", user);
        }
        return result;
    }


    /**
     * 检测当前手机号是否注册 是否已经设置密码
     *
     * @param request
     * @param phone
     * @return
     */
    @RequestMapping(value = "/switch", method = RequestMethod.POST)
    @ResponseBody
    public Result switchLoginType(HttpServletRequest request, String phone) {
        Result jsonResult = new Result("未注册");
        User user = userService.selectByPhone(phone);
        if (user != null) {
            if (user.getUserParamInfoTags().isPasswordTag()) {
                jsonResult.setResultFlag(true);
                jsonResult.setResultMsg("可用密码登录");
            } else {
                jsonResult.setResultMsg("未设置密码");
            }
        }
        return jsonResult;
    }


    /**
     * 通过wx js渠道注册用户信息
     *
     * @param request
     * @param code
     * @return
     */
    @RequestMapping(value = "/registed/wx", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult<Map> registedByWxJsChannel(HttpServletRequest request, String code) {
        JSONResult<Map> json = new JSONResult<>();

        JSONResult<User> result = userService.registedByWxJsChannel(code);

        if (result.getResultFlag()) {
            Subject subject = SecurityUtils.getSubject();
            subject.login(new UsernamePasswordToken(result.getData().getUserId() + "", "六六好帅"));
            request.getSession().setAttribute("userInfo", result.getData());
            DistributionUser distributionUser = distributionApi.getByUserId(result.getData().getUserId());
            request.getSession().setAttribute("distributionUser", distributionUser);
            Long shareId = 0l;
            try {
                shareId = Long.parseLong(request.getSession().getAttribute("shareId").toString());
            } catch (Exception ex) {

            }
            distributionApi.bindDistributionRelation(result.getData().getUserId(), shareId);
            distributionApi.updateByUser(result.getData());

            json.setResultFlag(true);
            json.setResultMsg(result.getResultMsg());

        } else {
            json.setResultFlag(false);
            json.setResultMsg("信息拉取失败");
        }
        return json;
    }

    /**
     * 通过wx渠道登录
     *
     * @param request
     * @param code
     * @return
     */
    @RequestMapping(value = "/login/wx", method = RequestMethod.POST)
    @ResponseBody
    public JSONResult<Map> loginByWxJsChannel(HttpServletRequest request, String code) {
        JSONResult<Map> json = new JSONResult<>();
        User user = userService.authenticationByWx(code);
        if (user != null) {
            json.setResultFlag(true);
            json.setResultMsg("登录成功");
            Subject subject = SecurityUtils.getSubject();
            subject.login(new UsernamePasswordToken(user.getUserId() + "", "六六好帅"));
            request.getSession().setAttribute("userInfo", user);
            DistributionUser distributionUser = distributionApi.getByUserId(user.getUserId());
            request.getSession().setAttribute("distributionUser", distributionUser);
//            Map map = new HashMap();
//            map.put("userInfo", user);
//            json.setData(map);
        } else {
            json.setResultFlag(false);
            json.setResultMsg("该用户还未注册");
        }
        return json;
    }


}
