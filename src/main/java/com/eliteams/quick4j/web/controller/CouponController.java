package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.web.model.CouponStore;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.model.UserAccount;
import com.eliteams.quick4j.web.model.UserCoupon;
import com.eliteams.quick4j.web.service.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Dsmart on 2016/12/9.
 */
@Controller
@RequestMapping(value = "coupon/")
public class CouponController {


    @Resource
    private CouponStoreService couponStoreService;

    @Resource
    private CouponTicketsService couponTicketsService;

    @Resource
    private CouponApiService couponApiService;

    @Resource
    private UserCouponService userCouponService;

    @Resource
    private UserAccountService userAccountService;

    /**
     * 通过优惠券激活码兑换优惠券
     *
     * @param request
     * @param code    eg:A40069086009
     * @return
     */
    @RequestMapping(value = "tickets/exchange")
    @ResponseBody
    public Result activityCouponTickets(HttpServletRequest request, String code) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        return couponTicketsService.activiteCouponTickets(code, user.getUserId());
    }

    /**
     * 积分兑换优惠券
     * @param request
     * @param couponId
     * @return
     */
    @RequestMapping(value = "integral/exchange")
    @ResponseBody
    public Result integralExchangeCoupon(HttpServletRequest request, Long couponId) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        return couponApiService.addIntegralExchangeCoupon(couponId, user.getUserId());

    }

    /**
     * 优惠券页面
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value="")
    public String couponPage(HttpServletRequest request,Model model){
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        List<CouponStore> couponStoreList = couponStoreService.listAllExchangeableCouponStore();
        List<UserCoupon> userCouponList = userCouponService.listAvailableUserCouponByUserId(user.getUserId());
        UserAccount userAccount =userAccountService.getUserAccountByUserId(user.getUserId());
        model.addAttribute("couponStoreList",couponStoreList);
        model.addAttribute("userCouponList",userCouponList);
        model.addAttribute("userAccount",userAccount);
        return "/web/1.0/dashboard/coupon";
    }



}
