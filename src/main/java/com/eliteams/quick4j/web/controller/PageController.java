package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.core.util.CheckMobile;
import com.eliteams.quick4j.core.util.NumberToCN;
import com.eliteams.quick4j.core.util.QrcodeUtils;
import com.eliteams.quick4j.web.model.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 视图控制器,返回jsp视图给前端
 *
 * @author StarZou
 * @since 2014年5月28日 下午4:00:49
 **/
@Controller
@RequestMapping("/page")
public class PageController {

    /**
     * 登录页
     */
    @RequestMapping("/login")
    public String login(HttpServletRequest request) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");

        //移动设备微信客户端
        if (CheckMobile.isFromMobileDeviceWeChat(request.getHeader("user-agent"))) {
            if (user == null) {
                return "redirect:/rest/wap/page/login";
            } else {
                return "redirect:/rest/wap/page/index";
            }
        } else if (CheckMobile.check(request.getHeader("user-agent"))) {//移动端网页
            if (user == null) {
                return "redirect:/rest/wap/page/login";
            } else {
                return "redirect:/rest/wap/page/index";
            }
        }
        if (user == null) {
            return "/web/1.0/login";
        } else {
            return "/web/1.0/index";
        }

    }

    /**
     * 注册页
     *
     * @return
     */
    @RequestMapping("/registe")
    public String registe(Model model) {
        model.addAttribute("isRegiste", true);
        return "/web/1.0/login";
    }

    /**
     * dashboard页
     */
    @RequestMapping("/dashboard")
    public String dashboard() {
        return "dashboard";
    }

    /**
     * 404页
     */
    @RequestMapping("/404")
    public String error404() {
        return "redirect:/rest/error";
    }


    /**
     * 401页
     */
    @RequestMapping("/401")
    public String error401() {
        return "redirect:/rest/error";
    }

    /**
     * 500页
     */
    @RequestMapping("/500")
    public String error500() {
        return "redirect:/rest/error";
    }

    /**
     * 合格投资人申明
     *
     * @return
     */
    @RequestMapping("/protocol/invest")
    public String protocolInvest() {
        return "/web/1.0/protocol/protocolInvest";
    }

    /**
     * 风险揭示书
     *
     * @return
     */
    @RequestMapping("/protocol/risk")
    public String protocolRisk() {
        return "/web/1.0/protocol/protocolRisk";
    }

    /**
     * 服务协议
     *
     * @return
     */
    @RequestMapping("/protocol/sign")
    public String protocolSign() {
        return "/web/1.0/protocol/protocolSign";
    }

    /**
     * MOM操盘投资顾问协议
     *
     * @param request
     * @param model
     * @param borrowMoney
     * @param lockMoney
     * @return
     */
    @RequestMapping("/protocol/trade")
    public String protocolTrade(HttpServletRequest request, Model model, Double borrowMoney, Double lockMoney, Integer agreementType) {
        model.addAttribute("borrowMoney", borrowMoney);
        model.addAttribute("borrowMoneyCN", NumberToCN.number2CNMontrayUnit(borrowMoney));
        model.addAttribute("lockMoney", lockMoney);
        model.addAttribute("lockMoneyCN", NumberToCN.number2CNMontrayUnit(lockMoney));
        Long nowMills = System.currentTimeMillis();
        String startTime = ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(nowMills), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy年MM月dd");
        String endTime = "";
        switch (agreementType) {
            case 0://短期   两天
                endTime = ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(nowMills + 1000 * 60 * 60 * 24 * 2), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy年MM月dd");
                break;
            case 1://
            default:
                endTime = ApplicationUtils.getTheDayAfterMonth("yyyy年MM月dd");
                break;
        }
        model.addAttribute("startTime", startTime);
        model.addAttribute("endTime", endTime);

        if (CheckMobile.check(request.getHeader("user-agent"))) {
            return "/wap/1.0/protocol/protocolTrade";
        }

        return "/web/1.0/protocol/protocolTrade";
    }


    @RequestMapping("/s/{shareNo}")
    public String share(HttpServletRequest request, @PathVariable Long shareNo) {
        request.getSession().setAttribute("shareId", shareNo);
        return "redirect:/";
    }

    @RequestMapping(value = "share/")
    public String sharePage(HttpServletRequest request) {
        return "/web/1.0/dashboard/distribution";
    }


    @RequestMapping(value = "/qrcode/")
    public void showQrcodeImg(HttpServletRequest request, HttpServletResponse response, String content) {

        QrcodeUtils.qrcodeStream(response, content);
    }


}