package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.util.IPUtils;
import com.eliteams.quick4j.core.util.QrcodeUtils;
import com.eliteams.quick4j.web.model.Recharge;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.model.UserAccount;
import com.eliteams.quick4j.web.model.Withdrawal;
import com.eliteams.quick4j.web.model.pagerestrict.WxPayRestrict;
import com.eliteams.quick4j.web.service.RechargeService;
import com.eliteams.quick4j.web.service.UserAccountService;
import com.eliteams.quick4j.web.service.WithdrawalService;
import com.eliteams.quick4j.web.service.WxPayService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 账户充值与提现
 * Created by Dsmart on 2016/11/12.
 */
@Controller
@RequestMapping(value = "/fund/")
public class FundController {

    @Resource
    private UserAccountService accountService;

    @Resource
    private WxPayService wxPayService;

    @Resource
    private RechargeService rechargeService;

    @Resource
    private WithdrawalService withdrawalService;

    /**
     * 充值页面
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "recharge/")
    public String rechargePage(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserAccount userAccount = accountService.getUserAccountByUserId(user.getUserId());
        model.addAttribute("userAccount", userAccount);
        return "/web/1.0/dashboard/recharge";
    }


    /**
     * 支付订单确认页面
     *
     * @param request
     * @param model
     * @param recharge
     * @return
     */
    @RequestMapping(value = "recharge/confirm/")
    public String rechargeOrderConfirmPage(HttpServletRequest request, Model model, Recharge recharge) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        recharge.setUserId(user.getUserId());
        recharge.setIp(IPUtils.getIpAddr(request));
        if (rechargeService.applyRecharge(recharge)) {//订单生成成功
            recharge = rechargeService.selectById(recharge.getRechargeId());
            UserAccount userAccount = accountService.getUserAccountByUserId(user.getUserId());
            model.addAttribute("userAccount", userAccount);
            model.addAttribute("recharge", recharge);

            return "/web/1.0/dashboard/recharge.wx.scan";
        } else {
            return "redirect:/error";
        }
    }

    /**
     * 申请提款页面
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "withdrawals/")
    public String withdrawalsPage(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserAccount userAccount = accountService.getUserAccountByUserId(user.getUserId());
        model.addAttribute("userAccount", userAccount);
        return "/web/1.0/dashboard/withdrawals";
    }

    /**
     * 用户提现申请
     *
     * @param request
     * @param withdrawal
     * @return
     */
    @RequestMapping(value = "withdrawals/apply/")
    @ResponseBody
    public Result withdrawalsApply(HttpServletRequest request, Withdrawal withdrawal) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        withdrawal.setUserId(user.getUserId());
        return withdrawalService.addItem(withdrawal);
    }


    /**
     * 申请提款记录
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "withdrawals/record/")
    public String withdrawalsRecord(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserAccount userAccount = accountService.getUserAccountByUserId(user.getUserId());
        model.addAttribute("userAccount", userAccount);
        return "/web/1.0/dashboard/withdrawals.record";
    }

    @RequestMapping(value = "withdrawals/record/{pageNo}")
    public String withdrawalsRecordByPageNo(HttpServletRequest recordByPageNo, @PathVariable int pageNo, Model model) {
        Subject subject = SecurityUtils.getSubject();
        pageNo = 0;
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserAccount userAccount = accountService.getUserAccountByUserId(user.getUserId());
        model.addAttribute("userAccount", userAccount);
        return "/web/1.0/dashboard/withdrawals.record";
    }

    /**
     * 获取扫码支付的二维码
     *
     * @param request
     * @param response
     * @param showOrderId
     */
    @RequestMapping(value = "{showOrderId}")
    public void wxScanQrcodeImg(HttpServletRequest request, HttpServletResponse response, @PathVariable String showOrderId) {
        WxPayRestrict wxPayRestrict = new WxPayRestrict();
        wxPayRestrict.setOrderId(showOrderId);
        wxPayRestrict.setRequest(request);
        QrcodeUtils.qrcodeStream(response, wxPayService.scanWxPay(wxPayRestrict).getResultMsg());
    }

    @RequestMapping(value = "recharge/query/wx/")
    @ResponseBody
    public Result queryOrder(HttpServletRequest request, WxPayRestrict wxPayRestrict) {
        return wxPayService.orderQuery(wxPayRestrict);
    }


}
