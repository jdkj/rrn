package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.util.PrimaryKeyModifyUtils;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateTransactionLogRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockPositionRestrict;
import com.eliteams.quick4j.web.service.AgreementService;
import com.eliteams.quick4j.web.service.StockOperateService;
import com.eliteams.quick4j.web.service.StockOperateTransactionLogService;
import com.eliteams.quick4j.web.service.StockPositionService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by Dsmart on 2016/11/3.
 */
@RequestMapping(value = "/agreement/")
@Controller
public class AgreementController {

    @Resource
    private StockPositionService stockPositionService;


    @Resource
    private StockOperateService stockOperateService;


    @Resource
    private AgreementService agreementService;

    @Resource
    private StockOperateTransactionLogService stockOperateTransactionLogService;

    /**
     * 结算合约
     *
     * @param request
     * @param agreementId
     * @return
     */
    @RequestMapping(value = "settlement", method = RequestMethod.POST)
    @ResponseBody
    public Result settlementAgreement(HttpServletRequest request, Long agreementId) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        return agreementService.settlementAgreement(agreementId, user.getUserId());
    }

    @RequestMapping(value = "additional", method = RequestMethod.POST)
    @ResponseBody
    public Result addLockMoney(HttpServletRequest request, AgreementRestrict agreementRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        agreementRestrict.setUserId(user.getUserId());
        return agreementService.additionalLockMoney(agreementRestrict);
    }


    /**
     * 委托交易----持仓
     *
     * @param request
     * @param model
     * @param stockPositionRestrict
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/position")
    public String agreementPosition(HttpServletRequest request, Model model, StockPositionRestrict stockPositionRestrict, @PathVariable String showAgreementId) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockPositionRestrict.setAgreementId(agreementId);
        stockPositionRestrict.setUserId(user.getUserId());
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if (agreement != null) {
            model.addAttribute("pageResult", stockPositionService.listByAgreementId(stockPositionRestrict));
            model.addAttribute("agreementId", showAgreementId);
            model.addAttribute("agreement", agreement);
            return "/web/1.0/dashboard/agreement.position";
        } else {
            return "redirect:/error";
        }
    }


    /**
     * 委托交易---持仓
     *
     * @param request
     * @param model
     * @param stockPositionRestrict
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/position/{pageNo}")
    public String agreementPositionByPageNo(HttpServletRequest request, Model model, StockPositionRestrict stockPositionRestrict, @PathVariable int pageNo, @PathVariable String showAgreementId) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(user.getUserId(),agreementId);
        if(agreement!=null) {
            stockPositionRestrict.setAgreementId(agreementId);
            stockPositionRestrict.setPageno(pageNo);
            stockPositionRestrict.setUserId(user.getUserId());
            model.addAttribute("pageResult", stockPositionService.listByAgreementId(stockPositionRestrict));
            model.addAttribute("agreementId", showAgreementId);
            model.addAttribute("agreement", agreement);
            return "/web/1.0/dashboard/agreement.position";
        }else{
            return "redirect:/error";
        }
    }

    /**
     * 委托交易--撤单
     *
     * @param request
     * @param model
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/operate/waiting")
    public String agreementWaiting(HttpServletRequest request, Model model, StockOperateRestrict stockOperateRestrict, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockOperateRestrict.setAgreementId(agreementId);
        model.addAttribute("pageResult", stockOperateService.listAllWaitingStockOperateOfAgreement(stockOperateRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.waiting";
    }

    /**
     * 委托交易----撤单 根据页码获取数据
     *
     * @param request
     * @param model
     * @param stockOperateRestrict
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/operate/waiting/{pageNo}")
    public String agreementWaitingByPageNo(HttpServletRequest request, Model model, StockOperateRestrict stockOperateRestrict, @PathVariable int pageNo, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockOperateRestrict.setAgreementId(agreementId);
        stockOperateRestrict.setPageno(pageNo);
        model.addAttribute("pageResult", stockOperateService.listAllWaitingStockOperateOfAgreement(stockOperateRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.waiting";
    }

    /**
     * 委托交易----查询---当日成交
     *
     * @param request
     * @param model
     * @param stockOperateTransactionLogRestrict
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/query/currentdeals")
    public String agreementQueryCurrentDayDeals(HttpServletRequest request, Model model, StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockOperateTransactionLogRestrict.setAgreementId(agreementId);
//        stockOperateRestrict.setAgreementId(agreementId);
//        model.addAttribute("pageResult", stockOperateService.listCurrentDayDeals(stockOperateRestrict));
        model.addAttribute("pageResult", stockOperateTransactionLogService.listCurrentDayDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.query.currentdeals";
    }

    /**
     * 委托交易---查询---当日成交  根据页码获取数据
     *
     * @param request
     * @param model
     * @param stockOperateTransactionLogRestrict
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/query/currentdeals/{pageNo}")
    public String agreementQueryCurrentDayDealsByPageNo(HttpServletRequest request, Model model, StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict, @PathVariable int pageNo, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
//        stockOperateRestrict.setAgreementId(agreementId);
//        stockOperateRestrict.setPageno(pageNo);
//        model.addAttribute("pageResult", stockOperateService.listCurrentDayDeals(stockOperateRestrict));
        stockOperateTransactionLogRestrict.setAgreementId(agreementId);
        stockOperateTransactionLogRestrict.setPageno(pageNo);
        model.addAttribute("pageResult", stockOperateTransactionLogService.listCurrentDayDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.query.currentdeals";
    }

    /**
     * 委托交易---查询---当日委托
     *
     * @param request
     * @param model
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/query/currentcommission")
    public String agreementQueryCurrentDayCommissions(HttpServletRequest request, Model model, StockOperateRestrict stockOperateRestrict, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockOperateRestrict.setAgreementId(agreementId);
        model.addAttribute("pageResult", stockOperateService.listCurrentDayCommissions(stockOperateRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.query.currentcommission";
    }

    /**
     * 委托交易----查询---当日委托 根据页码获取数据
     *
     * @param request
     * @param model
     * @param stockOperateRestrict
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/query/currentcommission/{pageNo}")
    public String agreementQueryCurrentDayCommissionsByPageNo(HttpServletRequest request, Model model, StockOperateRestrict stockOperateRestrict, @PathVariable int pageNo, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockOperateRestrict.setAgreementId(agreementId);
        stockOperateRestrict.setPageno(pageNo);
        model.addAttribute("pageResult", stockOperateService.listCurrentDayCommissions(stockOperateRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.query.currentcommission";
    }

    /**
     * 委托交易---查询---历史成交
     *
     * @param request
     * @param model
     * @param stockOperateTransactionLogRestrict
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/query/historydeals")
    public String agreementQueryHistoryDeals(HttpServletRequest request, Model model, StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockOperateTransactionLogRestrict.setAgreementId(agreementId);
        model.addAttribute("pageResult", stockOperateTransactionLogService.listHistoryDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict));
//        stockOperateRestrict.setAgreementId(agreementId);
//        model.addAttribute("pageResult", stockOperateService.listHistoryDeals(stockOperateRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.query.historydeals";
    }

    /**
     * 委托交易---查询---历史成交  根据页码获取数据
     *
     * @param request
     * @param model
     * @param stockOperateTransactionLogRestrict
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/query/historydeals/{pageNo}")
    public String agreementQueryHistoryDealsByPageNo(HttpServletRequest request, Model model, StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict, @PathVariable int pageNo, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
//        stockOperateRestrict.setAgreementId(agreementId);
//        stockOperateRestrict.setPageno(pageNo);
//        model.addAttribute("pageResult", stockOperateService.listHistoryDeals(stockOperateRestrict));
        stockOperateTransactionLogRestrict.setAgreementId(agreementId);
        stockOperateTransactionLogRestrict.setPageno(pageNo);
        model.addAttribute("pageResult", stockOperateTransactionLogService.listHistoryDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.query.historydeals";
    }


    /**
     * 委托交易---查询---历史委托
     *
     * @param request
     * @param model
     * @param stockOperateRestrict
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/query/historycommission")
    public String agreementQueryHistoryCommissions(HttpServletRequest request, Model model, StockOperateRestrict stockOperateRestrict, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockOperateRestrict.setAgreementId(agreementId);
        model.addAttribute("pageResult", stockOperateService.listHistoryCommissions(stockOperateRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.query.historycommission";
    }

    /**
     * 委托交易---查询---历史委托 根据页码获取数据
     *
     * @param request
     * @param model
     * @param stockOperateRestrict
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "{showAgreementId}/query/historycommission/{pageNo}")
    public String agreementQueryHistoryyCommissionsByPageNo(HttpServletRequest request, Model model, StockOperateRestrict stockOperateRestrict, @PathVariable int pageNo, @PathVariable String showAgreementId) {
        Long agreementId = PrimaryKeyModifyUtils.decodePrimaryKey(showAgreementId);
        stockOperateRestrict.setAgreementId(agreementId);
        stockOperateRestrict.setPageno(pageNo);
        model.addAttribute("pageResult", stockOperateService.listHistoryCommissions(stockOperateRestrict));
        model.addAttribute("agreementId", showAgreementId);
        return "/web/1.0/dashboard/agreement.query.historycommission";
    }


}
