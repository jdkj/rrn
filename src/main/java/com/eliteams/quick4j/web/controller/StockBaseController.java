package com.eliteams.quick4j.web.controller;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.util.ConvertBootstrapJson;
import com.eliteams.quick4j.web.model.StockBase;
import com.eliteams.quick4j.web.model.pagerestrict.StockBaseRestrict;
import com.eliteams.quick4j.web.service.StockBaseService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping(value = "/stock/")
public class StockBaseController {

    @Resource
    private StockBaseService stockBaseService;


    /**
     * 模糊查询
     *
     * @param request
     * @param stockBaseRestrict
     * @return
     */
    @RequestMapping(value = "fuzzyQuery/")
    @ResponseBody
    public Map<String, Object> queryStockInfo(HttpServletRequest request, StockBaseRestrict stockBaseRestrict) {
        Page<StockBase> page = stockBaseService.fuzzyQuery(stockBaseRestrict);
        return ConvertBootstrapJson.processConvertJsonToMap(page);
    }


}
