package com.eliteams.quick4j.web.controller.wap;

import com.eliteams.quick4j.web.model.*;
import com.eliteams.quick4j.web.model.pagerestrict.UserAccountMxRestrict;
import com.eliteams.quick4j.web.service.*;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Dsmart on 2016/11/14.
 */
@RequestMapping(value = "/wap/personalcenter/")
@Controller
public class PersonalCenterController {

    @Resource
    private UserAccountService userAccountService;


    @Resource
    private AgreementService agreementService;


    @Resource
    private UserAccountMxService userAccountMxService;

    @Resource
    private UserIdAuthService userIdAuthService;

    @Resource
    private BankAccountService bankAccountService;


    @Resource
    private BankService bankService;

    @Resource
    private UserCouponService userCouponService;


    @RequestMapping(value = "")
    public String index(HttpServletRequest request, Model model) {
//        Subject subject = SecurityUtils.getSubject();
//        User user = (User) subject.getSession().getAttribute("userInfo");
//        UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());
//        //可用资金
//        Double availableMoney = userAccount.getMoney();
//        //合约资金
//        List<Agreement> list = agreementService.listAllActivityAgreementByUserId(user.getUserId());
//        Double agreementMoney = 0.0d;
//        for (Agreement agreement : list) {
//            agreementMoney += (agreement.getLockMoney() + agreement.getDiffMoney());
//        }
//        model.addAttribute("availableMoney", availableMoney);
//        model.addAttribute("agreementMoney", agreementMoney);
//        return "wap/1.0/dashboard/personal.center";

        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");


        UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());

        //用户可用优惠券数量
        int userCouponTicketsNo = 0;

        List<UserCoupon> userCouponList = userCouponService.listAvailableUserCouponByUserId(user.getUserId());

        if (userCouponList != null && !userCouponList.isEmpty()) {
            userCouponTicketsNo = userCouponList.size();
        }

        List<Agreement> agreementList = agreementService.listAllActivityAgreementByUserId(user.getUserId());

        Double agreementMoney = 0.0d;
        for(Agreement agreement:agreementList){
            agreementMoney+=(agreement.getLockMoney()+agreement.getDiffMoney());
        }

        model.addAttribute("userAccount", userAccount);

        model.addAttribute("userCouponTicketsNo", userCouponTicketsNo);

        model.addAttribute("agreementList", agreementList);

        model.addAttribute("agreementMoney",agreementMoney);

        return "wap/1.0/dashboard/personal.center.v2";
    }

    /**
     * 资金明细----全部
     *
     * @param request
     * @param model
     * @param userAccountMxRestrict
     * @return
     */
    @RequestMapping(value = "flowing/")
    public String flowing(HttpServletRequest request, Model model, UserAccountMxRestrict userAccountMxRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userAccountMxRestrict.setUserId(user.getUserId());
        model.addAttribute("pageResult", userAccountMxService.listByRestrict(userAccountMxRestrict));
        return "/wap/1.0/dashboard/account.mx.all";
    }

    /**
     * 资金明细----全部---分页展示
     *
     * @param request
     * @param model
     * @param pageNo
     * @param userAccountMxRestrict
     * @return
     */
    @RequestMapping(value = "flowing/{pageNo}")
    public String flowingByPageNo(HttpServletRequest request, Model model, @PathVariable int pageNo, UserAccountMxRestrict userAccountMxRestrict) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userAccountMxRestrict.setUserId(user.getUserId());
        userAccountMxRestrict.setPageno(pageNo);
        model.addAttribute("pageResult", userAccountMxService.listByRestrict(userAccountMxRestrict));
        return "/wap/1.0/dashboard/account.mx.all";
    }

    /**
     * 资金明细----名目
     *
     * @param request
     * @param model
     * @param userAccountMxRestrict
     * @param changeType
     * @return
     */
    @RequestMapping(value = "flowing/c{changeType}/")
    public String flow(HttpServletRequest request, Model model, UserAccountMxRestrict userAccountMxRestrict, @PathVariable int changeType) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userAccountMxRestrict.setUserId(user.getUserId());
        userAccountMxRestrict.setChangeType(changeType);
        model.addAttribute("changeType", changeType);
        model.addAttribute("pageResult", userAccountMxService.listByRestrict(userAccountMxRestrict));
        return "/wap/1.0/dashboard/account.mx.category";
    }

    /**
     * 资金明细---名目----分页
     *
     * @param request
     * @param model
     * @param userAccountMxRestrict
     * @param changeType
     * @param pageNo
     * @return
     */
    @RequestMapping(value = "flowing/c{changeType}/{pageNo}")
    public String flowing(HttpServletRequest request, Model model, UserAccountMxRestrict userAccountMxRestrict, @PathVariable int changeType, @PathVariable int pageNo) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        userAccountMxRestrict.setUserId(user.getUserId());
        userAccountMxRestrict.setPageno(pageNo);
        userAccountMxRestrict.setChangeType(changeType);
        model.addAttribute("changeType", changeType);
        model.addAttribute("pageResult", userAccountMxService.listByRestrict(userAccountMxRestrict));
        return "/wap/1.0/dashboard/account.mx.category";
    }

    /**
     * 用户信息设置中心
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "setting/")
    public String personalSettingPage(HttpServletRequest request, Model model) {
        Subject subject = SecurityUtils.getSubject();
        User user = (User) subject.getSession().getAttribute("userInfo");
        UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();
        if (userParamInfoTag.isIdTag()) {
            UserIdAuth userIdAuth = (UserIdAuth) request.getSession().getAttribute("userIdAuth");

            if (userIdAuth == null) {
                userIdAuth = userIdAuthService.getAuthInfoByUserId(user.getUserId());
            }

            request.getSession().setAttribute("userIdAuth", userIdAuth);//好多页面都有提现  提现需要绑定银行卡  绑定银行卡前需要认证
        }

        if (userParamInfoTag.isBankTag()) {
            BankAccount bankAccount = (BankAccount) request.getSession().getAttribute("bankAccount");
            if (bankAccount == null) {
                bankAccount = bankAccountService.selectByUserId(user.getUserId());
            }
            request.getSession().setAttribute("bankAccount", bankAccount);
        }

        return "/wap/1.0/dashboard/personal.setting";
    }

    /**
     * 用户银行卡信息展示页面
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/setting/userbank/")
    public String userBankInfoPage(HttpServletRequest request, Model model) {
        BankAccount bankAccount = (BankAccount) request.getSession().getAttribute("bankAccount");
        if (bankAccount == null) {
            Subject subject = SecurityUtils.getSubject();
            User user = (User) subject.getSession().getAttribute("userInfo");
            bankAccount = bankAccountService.selectByUserId(user.getUserId());
        }
        request.getSession().setAttribute("bankAccount", bankAccount);

        return "/wap/1.0/dashboard/personal.setting.userbank";
    }

    /**
     * 添加银行账户信息
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "setting/addbank/")
    public String addBankPage(HttpServletRequest request, Model model) {
        UserIdAuth userIdAuth = (UserIdAuth) request.getSession().getAttribute("userIdAuth");
        if (userIdAuth == null) {
            Subject subject = SecurityUtils.getSubject();
            User user = (User) subject.getSession().getAttribute("userInfo");
            userIdAuth = userIdAuthService.getAuthInfoByUserId(user.getUserId());
        }
        request.getSession().setAttribute("userIdAuth", userIdAuth);
        model.addAttribute("bankList", bankService.listActivity());
        return "/wap/1.0/dashboard/personal.setting.addbank";
    }

    /**
     * 用户身份认证页面
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "setting/idauth/")
    public String idAuthPage(HttpServletRequest request, Model model) {
        return "/wap/1.0/dashboard/personal.setting.idauth";
    }

    @RequestMapping(value="setting/phone/")
    public String phoneSetPage(HttpServletRequest request){
        return "/wap/1.0/dashboard/personal.setting.phone";
    }

    @RequestMapping(value="setting/phone/modify/")
    public String phoneModifyPage(HttpServletRequest request,Model model){
        model.addAttribute("isModify",true);
        return "/wap/1.0/dashboard/personal.setting.phone";
    }


    /**
     * 设置密码页面
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "setting/password/")
    public String passwordSetPage(HttpServletRequest request) {
        return "/wap/1.0/dashboard/personal.setting.password";
    }

    /**
     * 修改密码页面
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "setting/password/modify/")
    public String passwordModifyPage(HttpServletRequest request, Model model) {
        model.addAttribute("isModify", true);
        return "/wap/1.0/dashboard/personal.setting.password";
    }

    /**
     * 交易密码设置
     *
     * @param request
     * @return
     */
    @RequestMapping(value = "setting/accountpassword/")
    public String accountPasswordSetPage(HttpServletRequest request) {
        return "/wap/1.0/dashboard/personal.setting.accountpassword";
    }

    /**
     * 交易密码修改
     *
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "setting/accountpassword/modify/")
    public String accountPasswordModifyPage(HttpServletRequest request, Model model) {
        model.addAttribute("isModify", true);
        return "/wap/1.0/dashboard/personal.setting.accountpassword";
    }


}
