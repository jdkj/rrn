package com.eliteams.quick4j.web.enums;

import com.eliteams.quick4j.core.util.ApplicationUtils;

/**
 * 系统参数枚举定义
 * Created by Dsmart on 2016/11/29.
 */
public enum SystemParamEnums {
    SYSTEM_ON("system_on","1","系统是否开启"),
    STOCK_MARKET_ON("stock_market_on","1","股市是否开市"),
    AGREEMENT_INTEREST_UPDATE_TIME("agreement_interest_update_time", ApplicationUtils.getDateWithMills(System.currentTimeMillis()),"合约利息结算更新时间"),
    STOCK_OPERATE_BASE_FEE("stock_operate_base_fee", "6.00", "操盘手续费最低收费"),
    STOCK_OPERATE_BASE_RATE("stock_operate_base_rate", "0.10", "操盘手续费最低费率"),
    STOCK_OPERATE_COST_BASE_FEE("stock_operate_cost_base_fee", "5.00", "操盘手续费最低收费"),
    STOCK_OPERATE_COST_BASE_RATE("stock_operate_cost_base_rate", "0.03", "操盘手续费最低费率"),
    STOCK_STAMP_DUTY_PARAM("stock_stamp_duty_param","0.10","印花税最低费率"),
    STOCK_TRANSFER_BASE_FEE("stock_transfer_base_fee","1.00","过户费最低收费"),
    STOCK_TRANSFER_RATE("stock_transfer_rate","0.06","过户费最低费率"),
    INTEGRAL_MONEY_RATE("integral_money_rate", "1000", "积分与现金汇率"),
    ADMIN_STOCK_OPERATE_TYPE("admin_stock_operate_type", "1", "管理员处理委托类型"),
    AGREEMENT_BORROW_MONEY_COST_RATE("agreement_stock_money_cost_rate", "14.4", "资金成本年利率"),
    SECURITIES_ACCOUNT_CATEGORY_TYPE("securities_account_category_type", "0", "资金账户策略类型");

    /**
     * 参数
     */
    private String param;

    /**
     * 参数默认值
     */
    private String paramDefaultValue;

    /**
     * 参数描述
     */
    private String paramDesc;


    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    public String getParamDefaultValue() {
        return paramDefaultValue;
    }

    public void setParamDefaultValue(String paramDefaultValue) {
        this.paramDefaultValue = paramDefaultValue;
    }

    public String getParamDesc() {
        return paramDesc;
    }

    public void setParamDesc(String paramDesc) {
        this.paramDesc = paramDesc;
    }


    SystemParamEnums(String param, String paramDefaultValue, String paramDesc) {
        this.param = param;
        this.paramDefaultValue = paramDefaultValue;
        this.paramDesc = paramDesc;
    }
}
