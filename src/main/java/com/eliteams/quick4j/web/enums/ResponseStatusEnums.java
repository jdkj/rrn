package com.eliteams.quick4j.web.enums;

/**
 * Created by Dsmart on 2016/10/10.
 */
public enum ResponseStatusEnums {


    WITHOUT_LOGON(false, 1000, "请先登录"),
    PHONE_FORMAT_ERROR(false,1003, "手机号格式错误"),
    PHONE_FORMAT_SUC(true,1004, "手机号格式正确"),
    PHNOE_CODE_LIMITTED(false,1005, "该手机号请求验证码过频,请稍后再试!"),
    CLIENT_CODE_LIMITTED(false,1006, "该客户端请求验证码过频,请稍后再试!"),
    IP_CODE_LIMITTED(false,1007, "该ip地址请求验证码过频,请稍后再试!"),
    PHONE_REGISTED_ERROR(false,1008,"该手机号已经被注册"),
    PHONE_CODE_VALIDATE_FAIL(false,1009,"短信验证码验证失败"),
    PHONE_CODE_VALIDATE_SUC(true,1010,"短信验证码验证成功"),
    USER_PHONE_IS_REQUIRED(false, 1011, "请先完善手机号信息"),
    ID_CARD_NO_ILLEGAL(false,2000,"身份证号码异常!"),
    COUPON_TICKETS_IS_NOT_EXIST(false,3000,"优惠券激活码不存在"),
    COUPON_TICKETS_IS_NOT_CORRECT(false,3001,"优惠券激活码不正确"),
    COUPON_TICKETS_IS_NOT_AVAILABLE(false,3002,"优惠券激活码不可用"),
    COUPON_TICKETS_IS_LIMITED(false,3003,"每位用户通过该激活码只可兑换%s张优惠券"),
    COUPON_TICKETS_EXCHANGE_SUCCESS(true,3004,"优惠券兑换成功，请到用户中心查看!"),
    COUPON_TICKETS_EXCHANGE_FAILED(false,3005,"优惠券兑换失败!"),
    COUPON_TICKETS_IS_USED(false,3006,"优惠券激活码已被使用"),
    USER_COUPON_IS_NOT_EXIST(false,3100,"优惠券不存在"),
    AGREEMENT_ATTACHED_COUPON(false,3101,"单笔操盘只可使用1张优惠券"),
    COUPON_STORE_IS_NOT_EXIST(false,3201,"优惠券不存在"),
    COUPON_STORE_IS_NOT_EXCHANGEABLE(false,3202,"当前优惠券不可兑换"),
    BORROW_NOT_ENOUGH_MONEY(false,8000,"申请资金过少"),
    BORROW_TOO_MUCH_MONEY(false,8001,"申请资金过多"),
    ACCOUNT_MONEY_NOT_ENOUGH(false,8002,"账户资金不足"),
    ORDER_STATUS_ILLEGAL(false,8003,"订单状态异常"),
    ORDER_IS_NOT_EXIST(false,8004,"当前订单不存在"),
    ACCOUNT_INTEGRAL_NOT_ENOUGH(false,8005,"可用积分不足"),
    AVAILABLE_STOCK_NUM_NOT_ENOUGH(false,8100,"可售股票数量不足"),
    REDUCE_STOCK_POSITION_SUCCESS(true,8101,"减仓成功"),
    REDUCE_STOCK_POSITION_FAIL(false,8102,"减仓失败"),
    CLEAR_STOCK_POSITION(true,8103,"清仓成功"),
    STOCK_IS_FORBID(false,8104,"很抱歉，您输入的股票是当前系统限买股，暂不支持交易！"),
    STOCK_MARKET_IS_NOT_OPENING(false,8105,"当前已休市，暂不支持交易！"),
    AGREEMENT_IS_FORBID_BUY(false,8200,"很抱歉，您当前合约已触发预警，无法买入！"),
    DATA_ILLEGAL_ERROR(false,9001,"数据异常"),
    OPERATE_FAIL(false,5000, "操作失败"),
    OPERATE_SUCCESS(true,5001, "操作成功"),
    REQUEST_PARAMS_ERROR(false,9001, "请求参数错误"),
    UNSUPPORED_METHOD(false,401, "不支持该方式访问"),
    ERROR_REQUEST(false,404, "错误的请求"),
    SYSTEM_ERROR(false,500, "系统错误");

    private boolean statusFlag;

    private int statusCode;

    private String statusMsg;


    ResponseStatusEnums(boolean statusFlag, int statusCode, String statusMsg) {
        this.statusFlag = statusFlag;
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
    }


    public boolean getStatusFlag() {
        return statusFlag;
    }

    public void setStatusFlag(boolean statusFlag) {
        this.statusFlag = statusFlag;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }
}
