package com.eliteams.quick4j.web.enums;

/**
 * Created by Dsmart on 2016/11/20.
 */
public enum PhoneCodeReqTypeEnums {
    USER_REGISTER(0, "用户注册"),
    USER_LOGIN(1, "用户登录"),
    USER_PHONE_BINDING(2, "用户手机号绑定"),
    USER_PERMISSION_VALIDATE(3, "用户权限验证");


    /**
     * 请求类型
     */
    private Integer type;

    /**
     * 请求描述
     */
    private String remark;

   private  PhoneCodeReqTypeEnums(Integer type, String remark) {
        this.type = type;
        this.remark = remark;
    }


    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    public static PhoneCodeReqTypeEnums getPhoneCodeReqTypeEnums(Integer type) {
        PhoneCodeReqTypeEnums phoneCodeReqTypeEnums = null;
        switch (type) {
            default:
            case 0:
                phoneCodeReqTypeEnums = USER_REGISTER;
                break;
            case 1:
                phoneCodeReqTypeEnums = USER_LOGIN;
                break;
            case 2:
                phoneCodeReqTypeEnums = USER_PHONE_BINDING;
                break;
            case 3:
                phoneCodeReqTypeEnums = USER_PERMISSION_VALIDATE;
                break;
        }
        return phoneCodeReqTypeEnums;
    }
}
