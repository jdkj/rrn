package com.eliteams.quick4j.web.enums;

/**
 * 短信通道枚举常量
 * Created by Dsmart on 2016/10/4.
 */
public enum MobileMessageEnums {


    SMS_253(0, "url", "account", "password", true);

    /**
     * 短信通道编号
     */
    private int channelType;

    /**
     * 短信通道地址
     */
    private String url;

    /**
     * 短信通道帐号
     */
    private String account;

    /**
     * 短信通道密码
     */
    private String password;


    /**
     * 是否需要返回码
     */
    private Boolean needStatus;


    MobileMessageEnums(int channelType, String url, String account, String password, Boolean needStatus) {
        this.channelType = channelType;
        this.url = url;
        this.account = account;
        this.password = password;
        this.needStatus = needStatus;
    }

    public int getChannelType() {
        return channelType;
    }

    public void setChannelType(int channelType) {
        this.channelType = channelType;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getNeedStatus() {
        return needStatus;
    }

    public void setNeedStatus(Boolean needStatus) {
        this.needStatus = needStatus;
    }
}
