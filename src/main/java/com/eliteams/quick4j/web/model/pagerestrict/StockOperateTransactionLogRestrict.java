package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2017/5/30.
 */
public class StockOperateTransactionLogRestrict extends PageRestrict {


    /**
     * 合约编号
     */
    private Long agreementId;


    /**
     * 券商账户编号
     */
    private Long securitiesAccountId;


    /**
     * 交易软件合约编号
     */
    private String agreementNo;


    /**
     * 交易软件成交编号
     */
    private String transactionNo;


    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }
}
