package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.web.enums.PhoneCodeReqTypeEnums;
import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;

/**
 * Created by Dsmart on 2016/6/27.
 */
public class RandCode implements Serializable {
    private static final long serialVersionUID = 4140916531117980686L;

    @JsonIgnore
    private Long id;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 随机码
     */
    private String codes;

    /**
     * 请求ip地址
     */
    @JsonIgnore
    private String ip;

    /**
     * 描述
     */
    @JsonIgnore
    private String remark="用户注册";

    /**
     * 客户端信息
     */
    @JsonIgnore
    private String client_info;

    /**
     * 状态
     */
    @JsonIgnore
    private int status;

    /**
     * 创建时间
     */
    @JsonIgnore
    private String create_time;

    /**
     * 更新时间
     */
    @JsonIgnore
    private String update_time;


    private Integer type;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCodes() {
        return codes;
    }

    public void setCodes(String codes) {
        this.codes = codes;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getClient_info() {
        return client_info;
    }

    public void setClient_info(String client_info) {
        this.client_info = client_info;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
        this.remark = PhoneCodeReqTypeEnums.getPhoneCodeReqTypeEnums(type).getRemark();
    }
}
