package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * 股票查询接口返回数据模型
 * Created by Dsmart on 2016/10/19.
 */
public class StockMarket implements Serializable {

    private static final long serialVersionUID = 4866276738832754518L;
    /**
     * 股票市场过
     */
    private Long stockMarketId;


    /**
     * 股票名称
     */
    private String name;

    /**
     * 今日开盘价格
     * ##.##
     */
    private Double openPrice;


    /**
     * 昨日收盘价格
     * ##.##
     */
    private Double closePrice;


    /**
     * 当前价格
     * ##.##
     */
    private Double nowPrice;


    /**
     * 今日最高价
     * ##.##
     */
    private Double todayMax;


    /**
     * 今日最低价
     * ##.##
     */
    private Double todayMin;


    /**
     * 竞买价
     */
    private Double competBuyPrice;


    /**
     * 竞卖价
     */
    private Double competSellPrice;

    /**
     * 成交量
     */
    private Long tradeNum;


    /**
     * 成交金额
     * ##.##
     */
    private Double tradeAmount;

    /**
     * 买一数量
     */
    private Long buy1_n;


    /**
     * 买一报价
     * ##.##
     */
    private Double buy1_m;


    /**
     * 买二数量
     */
    private Long buy2_n;


    /**
     * 买二报价
     */
    private Double buy2_m;


    /**
     * 买三数量
     */
    private Long buy3_n;

    /**
     * 买三价格
     */
    private Double buy3_m;


    /**
     * 买四数量
     */
    private Long buy4_n;


    /**
     * 买四价格
     */
    private Double buy4_m;


    /**
     * 买五数量
     */
    private Long buy5_n;


    /**
     * 买五价格
     */
    private Double buy5_m;


    /**
     * 卖一数量
     */
    private Long sell1_n;


    /**
     * 卖一报价
     * ##.##
     */
    private Double sell1_m;


    /**
     * 卖二数量
     */
    private Long sell2_n;


    /**
     * 卖二价格
     */
    private Double sell2_m;


    /**
     * 卖三数量
     */
    private Long sell3_n;


    /**
     * 卖三价格
     */
    private Double sell3_m;


    /**
     * 卖四数量
     */
    private Long sell4_n;


    /**
     * 卖四报价
     */
    private Double sell4_m;


    /**
     * 卖五数量
     */
    private Long sell5_n;


    /**
     * 卖五报价
     */
    private Double sell5_m;


    /**
     * 日期
     * yyyy-MM-dd
     */
    private String date;

    /**
     * 时间
     * HH:mm:ss
     */
    private String time;


    /**
     * 涨跌金额
     * 计算公式：(当前价格-昨日收盘价格)
     */
    private Double diff_money;


    /**
     * 涨跌幅度
     * 计算公式: (当前价格-昨日收盘价格)/昨日收盘价格
     */
    private Double diff_rate;


    /**
     * 振幅
     * <p>
     * 计算公式: (当日最高-当日最低)/昨日收盘*100%
     */
    private Double swing;


    /**
     * 股票代码
     */
    private String code;

    /**
     * 股票所属市场
     * sh:上海
     * sz:深圳
     * hk:香港
     */
    private String market;

    /**
     * 涨停板价格
     */
    private Double maxPrice=0.0d;

    /**
     * 跌停板价格
     */
    private Double minPrice=0.0d;


    /**
     * 创建时间
     */
    private String createTime;


    /**
     * 更新时间
     */
    private String updateTime;


    /**
     * 股票状态
     * 0:正常
     * 3:停牌
     */
    private Integer stockStatus;


    private String stockStatusDesc;


    public StockMarket() {
    }

    public StockMarket(String sinaResult) {

        if (StringUtils.isNotEmpty(sinaResult)) {

            if (sinaResult.contains("=")) {

                String stockCode = sinaResult.substring(sinaResult.indexOf("str_") + 4, sinaResult.indexOf("="));

                String stockMarket = stockCode.substring(0, 2);
                stockCode = stockCode.substring(2);


                String stockMsg = sinaResult.substring(sinaResult.indexOf("=") + 2, sinaResult.lastIndexOf("\""));

                if (stockMsg.contains(",")) {

                    this.code = stockCode;
                    this.market = stockMarket;

                    String params[] = stockMsg.split(",");

                    this.name = params[0];
                    this.openPrice = CurrencyUtils.parseMoenyRoundHalfUp(params[1]);
                    this.closePrice = CurrencyUtils.parseMoenyRoundHalfUp(params[2]);
                    this.setClosePrice(closePrice);
                    this.nowPrice = CurrencyUtils.parseMoenyRoundHalfUp(params[3]);
                    this.todayMax = CurrencyUtils.parseMoenyRoundHalfUp(params[4]);
                    this.todayMin = CurrencyUtils.parseMoenyRoundHalfUp(params[5]);
                    this.competBuyPrice = CurrencyUtils.parseMoenyRoundHalfUp(params[6]);
                    this.competSellPrice = CurrencyUtils.parseMoenyRoundHalfUp(params[7]);
                    this.tradeNum = Long.parseLong(params[8]);
                    this.tradeAmount = CurrencyUtils.parseMoenyRoundHalfUp(params[9]);
                    this.buy1_n = Long.parseLong(params[10]);
                    this.buy1_m = CurrencyUtils.parseMoenyRoundHalfUp(params[11]);

                    this.buy2_n = Long.parseLong(params[12]);
                    this.buy2_m = CurrencyUtils.parseMoenyRoundHalfUp(params[13]);

                    this.buy3_n = Long.parseLong(params[14]);
                    this.buy3_m = CurrencyUtils.parseMoenyRoundHalfUp(params[15]);

                    this.buy4_n = Long.parseLong(params[16]);
                    this.buy4_m = CurrencyUtils.parseMoenyRoundHalfUp(params[17]);

                    this.buy5_n = Long.parseLong(params[18]);
                    this.buy5_m = CurrencyUtils.parseMoenyRoundHalfUp(params[19]);


                    this.sell1_n = Long.parseLong(params[20]);
                    this.sell1_m = CurrencyUtils.parseMoenyRoundHalfUp(params[21]);

                    this.sell2_n = Long.parseLong(params[22]);
                    this.sell2_m = CurrencyUtils.parseMoenyRoundHalfUp(params[23]);

                    this.sell3_n = Long.parseLong(params[24]);
                    this.sell3_m = CurrencyUtils.parseMoenyRoundHalfUp(params[25]);

                    this.sell4_n = Long.parseLong(params[26]);
                    this.sell4_m = CurrencyUtils.parseMoenyRoundHalfUp(params[27]);

                    this.sell5_n = Long.parseLong(params[28]);
                    this.sell5_m = CurrencyUtils.parseMoenyRoundHalfUp(params[29]);

                    this.date = params[30];
                    this.time = params[31];

                    String stockStatus = params[32];

                    if (stockStatus.equalsIgnoreCase("00")) {
                        this.setStockStatus(0);
                    } else if (stockStatus.equalsIgnoreCase("03")) {
                        this.setStockStatus(3);
                    }


                    this.diff_money = CurrencyUtils.parseMoenyRoundHalfUp((nowPrice - closePrice) + "");
                    this.diff_rate = CurrencyUtils.parseMoenyRoundHalfUp((diff_money * 100 / closePrice) + "");
                    this.swing = CurrencyUtils.parseMoenyRoundHalfUp(((todayMax - todayMin) * 100 / closePrice) + "");


                }


            }


        }

    }


    public Long getStockMarketId() {
        return stockMarketId;
    }

    public void setStockMarketId(Long stockMarketId) {
        this.stockMarketId = stockMarketId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(Double openPrice) {
        this.openPrice = openPrice;
    }

    public Double getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(Double closePrice) {
        this.closePrice = closePrice;
        this.maxPrice = CurrencyUtils.parseMoenyRoundHalfUp(closePrice*1.1+"");
        this.minPrice =  CurrencyUtils.parseMoenyRoundHalfUp(closePrice*0.9+"");
    }

    public Double getNowPrice() {
        return nowPrice;
    }

    public void setNowPrice(Double nowPrice) {
        this.nowPrice = nowPrice;
    }

    public Double getTodayMax() {
        return todayMax;
    }

    public void setTodayMax(Double todayMax) {
        this.todayMax = todayMax;
    }

    public Double getTodayMin() {
        return todayMin;
    }

    public void setTodayMin(Double todayMin) {
        this.todayMin = todayMin;
    }

    public Double getCompetBuyPrice() {
        return competBuyPrice;
    }

    public void setCompetBuyPrice(Double competBuyPrice) {
        this.competBuyPrice = competBuyPrice;
    }

    public Double getCompetSellPrice() {
        return competSellPrice;
    }

    public void setCompetSellPrice(Double competSellPrice) {
        this.competSellPrice = competSellPrice;
    }

    public Long getTradeNum() {
        return tradeNum;
    }

    public void setTradeNum(Long tradeNum) {
        this.tradeNum = tradeNum;
    }

    public Double getTradeAmount() {
        return tradeAmount;
    }

    public void setTradeAmount(Double tradeAmount) {
        this.tradeAmount = tradeAmount;
    }

    public Long getBuy1_n() {
        return buy1_n;
    }

    public void setBuy1_n(Long buy1_n) {
        this.buy1_n = buy1_n;
    }

    public Double getBuy1_m() {
        return buy1_m;
    }

    public void setBuy1_m(Double buy1_m) {
        this.buy1_m = buy1_m;
    }

    public Long getBuy2_n() {
        return buy2_n;
    }

    public void setBuy2_n(Long buy2_n) {
        this.buy2_n = buy2_n;
    }

    public Double getBuy2_m() {
        return buy2_m;
    }

    public void setBuy2_m(Double buy2_m) {
        this.buy2_m = buy2_m;
    }

    public Long getBuy3_n() {
        return buy3_n;
    }

    public void setBuy3_n(Long buy3_n) {
        this.buy3_n = buy3_n;
    }

    public Double getBuy3_m() {
        return buy3_m;
    }

    public void setBuy3_m(Double buy3_m) {
        this.buy3_m = buy3_m;
    }

    public Long getBuy4_n() {
        return buy4_n;
    }

    public void setBuy4_n(Long buy4_n) {
        this.buy4_n = buy4_n;
    }

    public Double getBuy4_m() {
        return buy4_m;
    }

    public void setBuy4_m(Double buy4_m) {
        this.buy4_m = buy4_m;
    }

    public Long getBuy5_n() {
        return buy5_n;
    }

    public void setBuy5_n(Long buy5_n) {
        this.buy5_n = buy5_n;
    }

    public Double getBuy5_m() {
        return buy5_m;
    }

    public void setBuy5_m(Double buy5_m) {
        this.buy5_m = buy5_m;
    }

    public Long getSell1_n() {
        return sell1_n;
    }

    public void setSell1_n(Long sell1_n) {
        this.sell1_n = sell1_n;
    }

    public Double getSell1_m() {
        return sell1_m;
    }

    public void setSell1_m(Double sell1_m) {
        this.sell1_m = sell1_m;
    }

    public Long getSell2_n() {
        return sell2_n;
    }

    public void setSell2_n(Long sell2_n) {
        this.sell2_n = sell2_n;
    }

    public Double getSell2_m() {
        return sell2_m;
    }

    public void setSell2_m(Double sell2_m) {
        this.sell2_m = sell2_m;
    }

    public Long getSell3_n() {
        return sell3_n;
    }

    public void setSell3_n(Long sell3_n) {
        this.sell3_n = sell3_n;
    }

    public Double getSell3_m() {
        return sell3_m;
    }

    public void setSell3_m(Double sell3_m) {
        this.sell3_m = sell3_m;
    }

    public Long getSell4_n() {
        return sell4_n;
    }

    public void setSell4_n(Long sell4_n) {
        this.sell4_n = sell4_n;
    }

    public Double getSell4_m() {
        return sell4_m;
    }

    public void setSell4_m(Double sell4_m) {
        this.sell4_m = sell4_m;
    }

    public Long getSell5_n() {
        return sell5_n;
    }

    public void setSell5_n(Long sell5_n) {
        this.sell5_n = sell5_n;
    }

    public Double getSell5_m() {
        return sell5_m;
    }

    public void setSell5_m(Double sell5_m) {
        this.sell5_m = sell5_m;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Double getDiff_money() {
        return diff_money;
    }

    public void setDiff_money(Double diff_money) {
        this.diff_money = diff_money;
    }

    public Double getDiff_rate() {
        return diff_rate;
    }

    public void setDiff_rate(Double diff_rate) {
        this.diff_rate = diff_rate;
    }

    public Double getSwing() {
        return swing;
    }

    public void setSwing(Double swing) {
        this.swing = swing;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public Double getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(Double maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(Double minPrice) {
        this.minPrice = minPrice;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public Integer getStockStatus() {
        return stockStatus;
    }

    public void setStockStatus(Integer stockStatus) {
        this.stockStatus = stockStatus;
        switch (stockStatus) {
            case 0:
                this.stockStatusDesc = "正常";
                break;
            case 3:
                this.stockStatusDesc = "停牌";
                break;
        }
    }

    public String getStockStatusDesc() {
        return stockStatusDesc;
    }

    public void setStockStatusDesc(String stockStatusDesc) {
        this.stockStatusDesc = stockStatusDesc;
    }


    @Override
    public String toString() {
        return "StockMarket{" +
                "stockMarketId=" + stockMarketId +
                ", name='" + name + '\'' +
                ", openPrice=" + openPrice +
                ", closePrice=" + closePrice +
                ", nowPrice=" + nowPrice +
                ", todayMax=" + todayMax +
                ", todayMin=" + todayMin +
                ", competBuyPrice=" + competBuyPrice +
                ", competSellPrice=" + competSellPrice +
                ", tradeNum=" + tradeNum +
                ", tradeAmount=" + tradeAmount +
                ", buy1_n=" + buy1_n +
                ", buy1_m=" + buy1_m +
                ", buy2_n=" + buy2_n +
                ", buy2_m=" + buy2_m +
                ", buy3_n=" + buy3_n +
                ", buy3_m=" + buy3_m +
                ", buy4_n=" + buy4_n +
                ", buy4_m=" + buy4_m +
                ", buy5_n=" + buy5_n +
                ", buy5_m=" + buy5_m +
                ", sell1_n=" + sell1_n +
                ", sell1_m=" + sell1_m +
                ", sell2_n=" + sell2_n +
                ", sell2_m=" + sell2_m +
                ", sell3_n=" + sell3_n +
                ", sell3_m=" + sell3_m +
                ", sell4_n=" + sell4_n +
                ", sell4_m=" + sell4_m +
                ", sell5_n=" + sell5_n +
                ", sell5_m=" + sell5_m +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", diff_money=" + diff_money +
                ", diff_rate=" + diff_rate +
                ", swing=" + swing +
                ", code='" + code + '\'' +
                ", market='" + market + '\'' +
                ", maxPrice=" + maxPrice +
                ", minPrice=" + minPrice +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", stockStatus=" + stockStatus +
                ", stockStatusDesc='" + stockStatusDesc + '\'' +
                '}';
    }
}

