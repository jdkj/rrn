package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * 某券商户头下的股票交易帐号
 * Created by Dsmart on 2017/3/19.
 */
public class SecuritiesOperateAccount implements Serializable {
    private static final long serialVersionUID = -2677689873320752405L;

    private Long securitiesOperateAccountId;

    private Long securitiesAccountId;

    private String accountName;

    private String accountPassword;

    private Integer status;

    private String createTime;

    private String updateTime;


    public Long getSecuritiesOperateAccountId() {
        return securitiesOperateAccountId;
    }

    public void setSecuritiesOperateAccountId(Long securitiesOperateAccountId) {
        this.securitiesOperateAccountId = securitiesOperateAccountId;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
