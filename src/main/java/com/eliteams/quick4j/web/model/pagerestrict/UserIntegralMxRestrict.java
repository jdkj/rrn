package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;
import com.eliteams.quick4j.web.model.UserIntegralMx;

/**
 * Created by Dsmart on 2016/12/7.
 */
public class UserIntegralMxRestrict extends PageRestrict<UserIntegralMx> {

    private Long userId;


    private Integer changeType;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getChangeType() {
        return changeType;
    }

    public void setChangeType(Integer changeType) {
        this.changeType = changeType;
    }
}
