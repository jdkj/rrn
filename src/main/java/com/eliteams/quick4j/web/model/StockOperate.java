package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.container.StockOperateParamContainer;
import com.eliteams.quick4j.web.container.StockStampDutyParamContainer;
import com.eliteams.quick4j.web.container.StockTransferParamContainer;

/**
 * 股票操盘记录模型
 * Created by Dsmart on 2016/11/1.
 */
public class StockOperate {


    private Long operateId;

    /**
     * 合约单号
     */
    private Long agreementId;

    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 股票编码
     */
    private String stockCode;


    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 股票所属市场
     */
    private String stockMarket;

    /**
     * 委托价格
     */
    private Double commissionPrice;


    private Integer commissionPriceType=1;

    /**
     * 委托数量
     */
    private Long commissionNum;


    /**
     * 成交价格
     */
    private Double transactionPrice = 0.00d;


    /**
     * 成交数量
     */
    private Long transactionNum = 0l;


    /**
     * 操作类型
     * 0:卖出
     * 1：买入
     */
    private Integer operateType = 1;


    /**
     * 操盘状态
     */
    private Integer operateStatus = 0;

    /**
     * 操盘手续费最低收费
     */
    private Double operateBaseFee;

    /**
     * 操盘手续费 最低费率
     */
    private Double operateBaseRate;

    /**
     * 操盘手续费 可调控费率
     */
    private Double operateControlRate;

    /**
     * 操盘最总收取的手续费
     * 计算公式:max(成交价值*(基础费率+可调费率),最低收费)
     * max(operateBaseFee,(transactionPrice*transactionNum*(operateBaseRate+operateContorlRate)))
     */
    private Double operateFinalFee;

    /**
     * 操盘成本
     */
    private Double operateCostFee;


    /***
     *过户费只是针对沪市股票即stockMarket='sh'
     */

    /**
     * 过户税费率
     */
    private Double stockTransferRate;

    /**
     * 最低收取的过户费 1元
     */
    private Double stockTransferBaseFee;

    /**
     * 实际过户费
     * 计算公式:max(过户税最低收费,成交价值*过户费税率)
     * max(stockTransferBaseFee,transactionPrice*transactionNum*stockTransferRate)
     */
    private Double stockTransferFinalFee;

    /**
     * **********************************
     */


    /***
     *印花税只针对卖出
     */

    /**
     * 印花税费率
     */
    private Double stockStampDutyRate;

    /**
     * 印花税费用
     */
    private Double stockStampDutyFinalFee;


    /**
     * 锁定资金
     */
    private Double lockMoney;


    private String createTime;


    private String updateTime;


    private String operateTypeDesc;

    private String operateStatusDesc;

    /**
     * 券商账户编号
     */
    private Long securitiesAccountId;


    /**
     * 处理状态
     */
    private Integer progressStatus;

    /**
     * 处理的股数
     */
    private Long progressNum;


    /**
     * 交易券商账户类型
     */
    private Integer accountType;


    public StockOperate() {
    }

    public StockOperate(Long agreementId, Long userId, String stockCode, String stockName, String stockMarket, Double commissionPrice, Long commissionNum, Integer operateType, Double operateContorlRate) {
        this.agreementId = agreementId;
        this.userId = userId;
        this.stockCode = stockCode;
        this.stockName = stockName;
        this.stockMarket = stockMarket;
        this.commissionPrice = commissionPrice;
        this.commissionNum = commissionNum;
        this.operateType = operateType;
        this.operateControlRate = operateContorlRate;
        this.operateBaseFee = StockOperateParamContainer.getOperateBaseFee();
        this.operateBaseRate = StockOperateParamContainer.getOperateBaseRate();
        this.stockStampDutyRate = StockStampDutyParamContainer.getStockStampDutyRate();
        this.stockTransferBaseFee = StockTransferParamContainer.getStockTransferBaseFee();
        this.stockTransferRate = StockTransferParamContainer.getStockTransferRate();

        //操盘手续费
        this.operateFinalFee = CurrencyUtils.max(operateBaseFee, commissionPrice * commissionNum * (operateBaseRate + operateContorlRate) / 100);

        //过户费
        if (stockMarket.equals("sh")) {
            this.stockTransferFinalFee = CurrencyUtils.max(stockTransferBaseFee, commissionPrice * commissionNum * stockTransferRate / 100);
        } else {
            this.stockTransferFinalFee = 0.00d;
        }

        //印花税
        //卖出的时候才有
        if (operateType == 0) {
            this.stockStampDutyFinalFee = CurrencyUtils.parseMoenyRoundHalfUp(commissionPrice * commissionNum * stockStampDutyRate / 100 + "");
            this.lockMoney = 0.00d;
        } else {//买入
            //锁定资金
            this.lockMoney = commissionPrice * commissionNum + this.operateFinalFee + this.stockTransferFinalFee;
        }


    }

    public Long getOperateId() {
        return operateId;
    }

    public void setOperateId(Long operateId) {
        this.operateId = operateId;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Double getCommissionPrice() {
        return commissionPrice;
    }

    public void setCommissionPrice(Double commissionPrice) {
        this.commissionPrice = commissionPrice;
    }

    public Integer getCommissionPriceType() {
        return commissionPriceType;
    }

    public void setCommissionPriceType(Integer commissionPriceType) {
        this.commissionPriceType = commissionPriceType;
    }

    public Long getCommissionNum() {
        return commissionNum;
    }

    public void setCommissionNum(Long commissionNum) {
        this.commissionNum = commissionNum;
    }

    public Double getTransactionPrice() {
        return transactionPrice;
    }

    public void setTransactionPrice(Double transactionPrice) {
        this.transactionPrice = transactionPrice;
    }

    public Long getTransactionNum() {
        return transactionNum;
    }

    public void setTransactionNum(Long transactionNum) {
        this.transactionNum = transactionNum;
    }

    public Integer getOperateType() {
        return operateType;
    }

    public void setOperateType(Integer operateType) {
        this.operateType = operateType;
    }

    public Integer getOperateStatus() {
        return operateStatus;
    }

    public void setOperateStatus(Integer operateStatus) {
        this.operateStatus = operateStatus;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getStockMarket() {
        return stockMarket;
    }

    public void setStockMarket(String stockMarket) {
        this.stockMarket = stockMarket;
    }

    public Double getOperateBaseFee() {
        return operateBaseFee;
    }

    public void setOperateBaseFee(Double operateBaseFee) {
        this.operateBaseFee = operateBaseFee;
    }

    public Double getOperateBaseRate() {
        return operateBaseRate;
    }

    public void setOperateBaseRate(Double operateBaseRate) {
        this.operateBaseRate = operateBaseRate;
    }

    public Double getOperateCostFee() {
        return operateCostFee;
    }

    public void setOperateCostFee(Double operateCostFee) {
        this.operateCostFee = operateCostFee;
    }

    public Double getOperateControlRate() {
        return operateControlRate;
    }

    public void setOperateControlRate(Double operateControlRate) {
        this.operateControlRate = operateControlRate;
    }

    public Double getOperateFinalFee() {
        return operateFinalFee;
    }

    public void setOperateFinalFee(Double operateFinalFee) {
        this.operateFinalFee = operateFinalFee;
    }

    public Double getStockTransferRate() {
        return stockTransferRate;
    }

    public void setStockTransferRate(Double stockTransferRate) {
        this.stockTransferRate = stockTransferRate;
    }

    public Double getStockTransferBaseFee() {
        return stockTransferBaseFee;
    }

    public void setStockTransferBaseFee(Double stockTransferBaseFee) {
        this.stockTransferBaseFee = stockTransferBaseFee;
    }

    public Double getStockTransferFinalFee() {
        return stockTransferFinalFee;
    }

    public void setStockTransferFinalFee(Double stockTransferFinalFee) {
        this.stockTransferFinalFee = stockTransferFinalFee;
    }

    public Double getStockStampDutyRate() {
        return stockStampDutyRate;
    }

    public void setStockStampDutyRate(Double stockStampDutyRate) {
        this.stockStampDutyRate = stockStampDutyRate;
    }

    public Double getStockStampDutyFinalFee() {
        return stockStampDutyFinalFee;
    }

    public void setStockStampDutyFinalFee(Double stockStampDutyFinalFee) {
        this.stockStampDutyFinalFee = stockStampDutyFinalFee;
    }

    public Double getLockMoney() {
        return lockMoney;
    }

    public void setLockMoney(Double lockMoney) {
        this.lockMoney = lockMoney;
    }

    public String getOperateTypeDesc() {
        String desc = "卖出";
        switch (this.getOperateType()) {
            case 0:
                break;
            case 1:
            default:
                desc = "买入";
                break;
        }
        return desc;
    }

    public String getOperateStatusDesc() {
        String desc = "未报";
        switch (this.getOperateStatus()) {
            case 0:
            default:
                break;
            case 1:
                desc = "全成交";
                break;
            case 2:
                desc = "部分成交";
                break;
            case 3:
                desc = "挂单";
                break;
            case 4:
                desc = "撤单";
                break;
            case 5:
                desc = "过期";
                break;
            case 6:
                desc="撤单中";
                break;
        }
        return desc;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }


    public Integer getProgressStatus() {
        return progressStatus;
    }

    public void setProgressStatus(Integer progressStatus) {
        this.progressStatus = progressStatus;
    }

    public Long getProgressNum() {
        return progressNum;
    }

    public void setProgressNum(Long progressNum) {
        this.progressNum = progressNum;
    }


    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    /**
     * 操盘成功后  重新计算
     * @return
     */
    public Double caculatedByTransaction() {

        Double caculateValue = 0.0d;

        Double transactionValue = this.getTransactionPrice() * this.getTransactionNum();

        //操盘手续费
        this.operateFinalFee = CurrencyUtils.max(operateBaseFee, transactionValue * (operateBaseRate + operateControlRate) / 100);

        //操盘成本费用
        this.operateCostFee = CurrencyUtils.max(StockOperateParamContainer.getOperateCostBaseFee(), transactionValue * (StockOperateParamContainer.getOperateCostBaseRate()) / 100);

        //过户费
        if (stockMarket.equals("sh")) {
            this.stockTransferFinalFee = CurrencyUtils.max(stockTransferBaseFee, transactionValue * stockTransferRate / 100);
        } else {
            this.stockTransferFinalFee = 0.00d;
        }

        //印花税
        //卖出的时候才有
        //印花税
        //卖出的时候才有
        if (operateType == 0) {
            this.stockStampDutyFinalFee = CurrencyUtils.parseMoenyRoundHalfUp(transactionValue* stockStampDutyRate / 100 + "");
            caculateValue = transactionValue-this.operateFinalFee -this.stockTransferFinalFee-this.stockStampDutyFinalFee;

        } else {//买入
            //锁定资金
            caculateValue = transactionValue+ this.operateFinalFee + this.stockTransferFinalFee;
        }

        return caculateValue;
    }

}
