/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eliteams.quick4j.web.model;

import com.thinkersoft.wechat.pay.common.RandomStringGenerator;
import com.thinkersoft.wechat.pay.configure.WxPayCommonConfigure;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-7-15, 15:21:55
 */
public class WxPayPo {

    /**
     * 用户编号
     */
    private String openId;

    /**
     * 公众号id
     */
    private String appId;

    /**
     * 商户编号
     */
    private String mch_id;

    /**
     * 微信支付附加校验数据
     */
    private String privateKey;

    /**
     * 附加数据
     */
    private String attach;

    /**
     * 商品描述
     */
    private String body;

    /**
     * 随机数据
     */
    private String nonce_str = RandomStringGenerator.getRandomStringByLength(32);

    /**
     * 异步通知获取地址
     */
    private String notify_url = WxPayCommonConfigure.NOTIFY_URL;

    /**
     * 商户订单号
     */
    private String out_trade_no;

    /**
     * 订单创建ip地址
     */
    private String spbill_create_ip;

    /**
     * 总共金额 单位分
     */
    private int total_fee;

    /**
     * 交易方式
     */
    private String trade_type = "JSAPI";

    /**
     * 指定支付方式
     */
    private String limit_pay="no_credit";


    /**
     * 签名
     */
    private String sign;

    /**
     * 微信支付 预支付交易会话标识
     */
    private String prepayId;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getNotify_url() {
        return notify_url;
    }

    public void setNotify_url(String notify_url) {
        this.notify_url = notify_url;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getSpbill_create_ip() {
        return spbill_create_ip;
    }

    public void setSpbill_create_ip(String spbill_create_ip) {
        this.spbill_create_ip = spbill_create_ip;
    }

    public int getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(int total_fee) {
        this.total_fee = total_fee;
    }

    public String getTrade_type() {
        return trade_type;
    }

    public void setTrade_type(String trade_type) {
        this.trade_type = trade_type;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getPrepayId() {
        return prepayId;
    }

    public void setPrepayId(String prepayId) {
        this.prepayId = prepayId;
    }

    public String getLimit_pay() {
        return limit_pay;
    }

    public void setLimit_pay(String limit_pay) {
        this.limit_pay = limit_pay;
    }

    /**
     * 创建统一下单提交前需要加密的数据的数据
     *
     * @return
     */
    public Map<String, Object> createUnifiedOrderReqOriginalDataForSign() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("appid", this.getAppId());

        map.put("attach", this.getAttach());

        map.put("body", this.getBody());

        map.put("mch_id", this.getMch_id());

        map.put("nonce_str", this.getNonce_str());

        map.put("notify_url", this.getNotify_url());

        if(StringUtils.isNotEmpty(this.getOpenId())) {

            map.put("openid", this.getOpenId());
        }

        map.put("out_trade_no", this.getOut_trade_no());

        map.put("spbill_create_ip", this.getSpbill_create_ip());

        map.put("total_fee", this.getTotal_fee());

        map.put("trade_type",this.getTrade_type());

        map.put("limit_pay",this.getLimit_pay());

        return map;

    }

    /**
     * 获取统一下单提交数据
     *
     * @return 统一下单提交数据
     * <xml>
     * <appid>wx3ad7ae3de3bc10fc</appid>
     * <attach>支付测试</attach>
     * <body>我趣支付测试</body>
     * <mch_id>1265942501</mch_id>
     * <nonce_str>k4x4iiesvi1r7xn0dwsbig1rfov3df6s</nonce_str>
     * <notify_url>http://w.30buy.com/weixin.jsp</notify_url>
     * <openid>oeoPtswbQCsrN_jtAqoCjzpsGX5E</openid>
     * <out_trade_no>59466</out_trade_no>
     * <product_id>59466</product_id>
     * <spbill_create_ip>127.0.0.1</spbill_create_ip>
     * <total_fee>1</total_fee>
     * <trade_type>JSAPI</trade_type>
     * <sign>12C5F776491CB3D540F5BB2EDD831BAD</sign>
     * </xml>";
     *
     */
    public String createUnifiedOrderReqData() {

        String result = "<xml>"
                + "<appid>" + this.getAppId() + "</appid>"
                + "<attach>" + this.getAttach() + "</attach>"
                + "<body>" + this.getBody() + "</body>"
                + "<mch_id>" + this.getMch_id() + "</mch_id>"
                + "<nonce_str>" + this.getNonce_str() + "</nonce_str>"
                + "<notify_url>" + this.getNotify_url() + "</notify_url>";
        if(StringUtils.isNotEmpty(this.getOpenId())){
            result += "<openid>" + this.getOpenId() + "</openid>";
        }

        result +=  "<out_trade_no>" + this.getOut_trade_no() + "</out_trade_no>"
                + "<spbill_create_ip>" + this.getSpbill_create_ip() + "</spbill_create_ip>"
                + "<total_fee>" + this.getTotal_fee() + "</total_fee>"
                + "<trade_type>" + this.getTrade_type() + "</trade_type>"
                + "<limit_pay>"+this.getLimit_pay()+"</limit_pay>"
                + "<sign>" + this.getSign() + "</sign></xml>";


        return result;



    }

    /**
     * 获取订单支付结果查询需要加密的数据map集合
     *
     * @return map
     */
    public Map<String, Object> createOrderQueryReqOrginalDataForSign() {

        Map<String, Object> map = new HashMap<String, Object>();

        map.put("appid", this.getAppId());

        map.put("mch_id", this.getMch_id());

        map.put("nonce_str", this.getNonce_str());

        map.put("out_trade_no", this.getOut_trade_no());

        return map;

    }

    /**
     * 获取订单支付结果查询提交的数据
     *
     * @return 订单支付结果查询提交的xml数据
     * <xml>
     * <appid>wx2421b1c4370ec43b</appid>
     * <mch_id>10000100</mch_id>
     * <nonce_str>ec2316275641faa3aacf3cc599e8730f</nonce_str>
     * <out_trade_no>111</out_trade_no>
     * <sign>FDD167FAA73459FD921B144BAF4F4CA2</sign>
     * </xml>
     *
     */
    public String createOrderQueryReqData() {

        return "<xml>"
                + "<appid>" + this.getAppId() + "</appid>"
                + "<mch_id>" + this.getMch_id() + "</mch_id>"
                + "<nonce_str>" + this.getNonce_str() + "</nonce_str>"
                + "<out_trade_no>" + this.getOut_trade_no() + "</out_trade_no>"
                + "<sign>" + this.getSign() + "</sign>"
                + "</xml>";

    }
}
