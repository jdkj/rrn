package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * Created by Dsmart on 2017/3/10.
 */
public class DistributionAccountMx implements Serializable {

    private static final long serialVersionUID = 9159282676660691296L;
    private Long distributionUserAccountMxId;

    private Long distributionUserId;

    private Long orderId;

    /**
     * 1：佣金分成
     * 0:提款去除
     */
    private Integer orderType;

    private Double totalMoney;

    private Double changeMoney;

    private String createTime;

    private String updateTime;

    public Long getDistributionUserAccountMxId() {
        return distributionUserAccountMxId;
    }

    public void setDistributionUserAccountMxId(Long distributionUserAccountMxId) {
        this.distributionUserAccountMxId = distributionUserAccountMxId;
    }

    public Long getDistributionUserId() {
        return distributionUserId;
    }

    public void setDistributionUserId(Long distributionUserId) {
        this.distributionUserId = distributionUserId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Double getChangeMoney() {
        return changeMoney;
    }

    public void setChangeMoney(Double changeMoney) {
        this.changeMoney = changeMoney;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "DistributionAccountMx{" +
                "distributionUserAccountMxId=" + distributionUserAccountMxId +
                ", distributionUserId=" + distributionUserId +
                ", orderId=" + orderId +
                ", orderType=" + orderType +
                ", totalMoney=" + totalMoney +
                ", changeMoney=" + changeMoney +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
