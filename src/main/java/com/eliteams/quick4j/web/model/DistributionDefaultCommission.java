package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * Created by Dsmart on 2017/3/11.
 */
public class DistributionDefaultCommission implements Serializable {
    private static final long serialVersionUID = -2565463284935195016L;

    private Long id;

    private String commissionType;

    private Double commissionProportion;

    private Integer commissionLevel;

    private Integer status;

    private String createTime;

    private String updateTime;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(String commissionType) {
        this.commissionType = commissionType;
    }

    public Double getCommissionProportion() {
        return commissionProportion;
    }

    public void setCommissionProportion(Double commissionProportion) {
        this.commissionProportion = commissionProportion;
    }

    public Integer getCommissionLevel() {
        return commissionLevel;
    }

    public void setCommissionLevel(Integer commissionLevel) {
        this.commissionLevel = commissionLevel;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "DistributionDefaultCommission{" +
                "id=" + id +
                ", commissionType='" + commissionType + '\'' +
                ", commissionProportion=" + commissionProportion +
                ", commissionLevel=" + commissionLevel +
                ", status=" + status +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
