package com.eliteams.quick4j.web.model;

/**
 * Created by Dsmart on 2016/11/1.
 */
public class StockLoadData {


    private Integer showapi_res_code;

    private String showapi_res_error;

    private ShowApiResBody showapi_res_body;

    private StockMarket stockMarket;


    public Integer getShowapi_res_code() {
        return showapi_res_code;
    }

    public void setShowapi_res_code(Integer showapi_res_code) {
        this.showapi_res_code = showapi_res_code;
    }

    public String getShowapi_res_error() {
        return showapi_res_error;
    }

    public void setShowapi_res_error(String showapi_res_error) {
        this.showapi_res_error = showapi_res_error;
    }

    public ShowApiResBody getShowapi_res_body() {
        return showapi_res_body;
    }

    public void setShowapi_res_body(ShowApiResBody showapi_res_body) {
        this.showapi_res_body = showapi_res_body;
        this.stockMarket = showapi_res_body.getStockMarket();
    }

    public StockMarket getStockMarket() {
        return stockMarket;
    }

    public void setStockMarket(StockMarket stockMarket) {
        this.stockMarket = stockMarket;
    }

    class ShowApiResBody {

        private Integer ret_code;

        private StockMarket stockMarket;

        public ShowApiResBody() {
        }

        public Integer getRet_code() {
            return ret_code;
        }

        public void setRet_code(Integer ret_code) {
            this.ret_code = ret_code;
        }

        public StockMarket getStockMarket() {
            return stockMarket;
        }

        public void setStockMarket(StockMarket stockMarket) {
            this.stockMarket = stockMarket;
        }
    }

}
