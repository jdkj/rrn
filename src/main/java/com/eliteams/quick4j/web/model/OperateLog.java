package com.eliteams.quick4j.web.model;

/**
 * Created by Dsmart on 2017/1/7.
 */
public class OperateLog {

    /**
     * 流水编号
     */
    private Long logId;

    /**
     * 主操盘员编号
     */
    private Long adminId;

    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 股票所属交易市场
     * sh:上海
     * sz:深圳
     */
    private String stockMarket;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 用户委托价格
     */
    private Double commissionPrice;

    /**
     * 委托价格类型
     * 1：指定价
     * 2:市价
     */
    private Integer commissionPriceType = 1;

    /**
     * 用户委托数量
     */
    private Long commissionNum;

    /**
     * 委托类型
     */
    private Integer commissionType;


    /**
     * 回单号
     */
    private String stockOperateId;


    /**
     * 操作交易价格
     */
    private Double stockOperatePrice;

    /**
     * 操作交易数量
     */
    private Long stockOperateNum;

    /**
     * 操作交易状态
     * 0:未报
     * 1:已报
     */
    private Integer stockOperateStatus;

    /**
     * 状态
     * 0:未报
     * 1:已报
     * 2:已确认
     * 3:用户撤销
     * 4:已撤销
     */
    private Integer status;


    private String createTime;

    private String updateTime;

    /**
     * 用户操作编号
     */
    private Long operateId;


    /**
     * 审查状态
     * 0:未审查
     * 1:审查通过
     * 2:审查驳回
     */
    private Integer reviewStatus;

    /**
     * 审查时间
     */
    private String reviewTime;

    /**
     * 成功交易价格
     */
    private Double transactionPrice;

    /**
     * 成功交易股数
     */
    private Long transactionNum;


    /**
     * 券商账户编号
     */
    private Long securitiesAccountId;

    /**
     * 交易券商账户类型
     */
    private Integer accountType;


    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStockMarket() {
        return stockMarket;
    }

    public void setStockMarket(String stockMarket) {
        this.stockMarket = stockMarket;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Double getCommissionPrice() {
        return commissionPrice;
    }

    public void setCommissionPrice(Double commissionPrice) {
        this.commissionPrice = commissionPrice;
    }

    public Integer getCommissionPriceType() {
        return commissionPriceType;
    }

    public void setCommissionPriceType(Integer commissionPriceType) {
        this.commissionPriceType = commissionPriceType;
    }

    public Long getCommissionNum() {
        return commissionNum;
    }

    public void setCommissionNum(Long commissionNum) {
        this.commissionNum = commissionNum;
    }

    public Integer getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(Integer commissionType) {
        this.commissionType = commissionType;
    }

    public String getStockOperateId() {
        return stockOperateId;
    }

    public void setStockOperateId(String stockOperateId) {
        this.stockOperateId = stockOperateId;
    }

    public Double getStockOperatePrice() {
        return stockOperatePrice;
    }

    public void setStockOperatePrice(Double stockOperatePrice) {
        this.stockOperatePrice = stockOperatePrice;
    }

    public Long getStockOperateNum() {
        return stockOperateNum;
    }

    public void setStockOperateNum(Long stockOperateNum) {
        this.stockOperateNum = stockOperateNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getOperateId() {
        return operateId;
    }

    public void setOperateId(Long operateId) {
        this.operateId = operateId;
    }

    public Integer getReviewStatus() {
        return reviewStatus;
    }

    public void setReviewStatus(Integer reviewStatus) {
        this.reviewStatus = reviewStatus;
    }

    public String getReviewTime() {
        return reviewTime;
    }

    public void setReviewTime(String reviewTime) {
        this.reviewTime = reviewTime;
    }


    public Double getTransactionPrice() {
        return transactionPrice;
    }

    public void setTransactionPrice(Double transactionPrice) {
        this.transactionPrice = transactionPrice;
    }

    public Long getTransactionNum() {
        return transactionNum;
    }

    public void setTransactionNum(Long transactionNum) {
        this.transactionNum = transactionNum;
    }

    public Integer getStockOperateStatus() {
        return stockOperateStatus;
    }

    public void setStockOperateStatus(Integer stockOperateStatus) {
        this.stockOperateStatus = stockOperateStatus;
    }


    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }


    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }
}


