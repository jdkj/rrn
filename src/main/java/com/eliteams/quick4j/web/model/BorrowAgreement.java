package com.eliteams.quick4j.web.model;

/**
 * 短期借款费率对象
 * Created by Dsmart on 2016/10/17.
 */
public class BorrowAgreement {

    /**
     * 编号
     */
    private Long borrowAgreementId;


    /**
     * 借款合约类型
     * 0:短期
     * 1:中期
     */
    private Integer borrowType;


    /**
     * 借款合约类型描述
     */
    private String borrowTypeDesc;


    /**
     * 倍数
     */
    private Integer multiple;


    /**
     * 倍数费率
     *
     * 单位百分比 年息
     *
     * 计算日息采取  金额*费率/365的方式
     *
     */
    private Double multipleRate;


    /**
     * 状态
     * 0:停用
     * 1:启用
     */
    private int status;

    /**
     * 最少借款金额
     */
    private Double borrowMinMoney=2000.00d;


    /**
     * 最大借款金额
     */
    private Double borrowMaxMoney=1000000.00d;


    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 最长操盘周期
     */
    private Integer maxPeriod;

    /**
     * 同类型合约最大拥有数量
     * 0:不限制
     * 非0则表示最大拥有数量
     */
    private Integer maxOwnNumber;

    /**
     * 主板单票持仓占总仓位最大比率
     */
    private Double mainBoard;

    /**
     * 创业板单票持仓占总仓位最大比率
     */
    private Double gem;


    public Long getBorrowAgreementId() {
        return borrowAgreementId;
    }

    public void setBorrowAgreementId(Long borrowAgreementId) {
        this.borrowAgreementId = borrowAgreementId;
    }

    public Integer getBorrowType() {
        return borrowType;
    }

    public void setBorrowType(Integer borrowType) {
        this.borrowType = borrowType;
    }

    public String getBorrowTypeDesc() {
        return borrowTypeDesc;
    }

    public void setBorrowTypeDesc(String borrowTypeDesc) {
        this.borrowTypeDesc = borrowTypeDesc;
    }

    public Integer getMultiple() {
        return multiple;
    }

    public void setMultiple(Integer multiple) {
        this.multiple = multiple;
    }

    public Double getMultipleRate() {
        return multipleRate;
    }

    public void setMultipleRate(Double multipleRate) {
        this.multipleRate = multipleRate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public Double getBorrowMinMoney() {
        return borrowMinMoney;
    }

    public void setBorrowMinMoney(Double borrowMinMoney) {
        this.borrowMinMoney = borrowMinMoney;
    }

    public Double getBorrowMaxMoney() {
        return borrowMaxMoney;
    }

    public void setBorrowMaxMoney(Double borrowMaxMoney) {
        this.borrowMaxMoney = borrowMaxMoney;
    }

    public Integer getMaxPeriod() {
        return maxPeriod;
    }

    public void setMaxPeriod(Integer maxPeriod) {
        this.maxPeriod = maxPeriod;
    }


    public Integer getMaxOwnNumber() {
        return maxOwnNumber;
    }

    public void setMaxOwnNumber(Integer maxOwnNumber) {
        this.maxOwnNumber = maxOwnNumber;
    }

    public Double getMainBoard() {
        return mainBoard;
    }

    public void setMainBoard(Double mainBoard) {
        this.mainBoard = mainBoard;
    }

    public Double getGem() {
        return gem;
    }

    public void setGem(Double gem) {
        this.gem = gem;
    }
}
