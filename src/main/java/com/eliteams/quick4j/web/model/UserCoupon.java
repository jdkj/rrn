package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.web.model.enums.couponstore.CouponStoreAvailablePeriodUnitEnums;
import com.eliteams.quick4j.web.model.enums.usercoupon.UserCouponStatusEnums;
import com.eliteams.quick4j.web.model.enums.usercoupon.UserCouponTypeAndDescEnums;

/**
 * Created by Dsmart on 2016/12/5.
 */
public class UserCoupon {

    /**
     * 用户优惠券编号
     */
    private Long userCouponId;


    /**
     * 优惠券编号
     */
    private Long couponId;


    /**
     * 用户编号
     */
    private Long userId;


    /**
     * 优惠券市值
     */
    private Double couponMarketValue;


    /**
     * 优惠券可用余额
     */
    private Double couponAvailableValue;


    /**
     * 优惠券类型
     */
    private Integer userCouponType;


    /**
     * 优惠券来源描述
     */
    private String userCouponDesc;

    /**
     * 优惠券状态
     */
    private Integer userCouponStatus=0;


    /**
     * 合约编号
     */
    private Long agreementId;


    /**
     * 获得时间
     */
    private String createTime;


    /**
     *信息更新时间
     */
    private String updateTime;


    /**
     * 过期时间
     */
    private String expiredTime;


    /**
     * 优惠券激活时间
     */
    private String activiteTime;


    private Long couponTicketId=0l;


    private String expiredTimeSql;


    private CouponStore couponStore;

    /**
     * 用户优惠券类型以及描述枚举
     */
    private UserCouponTypeAndDescEnums userCouponTypeAndDescEnums;

    /**
     * 用户优惠券状态枚举
     */
    private UserCouponStatusEnums userCouponStatusEnums;

    public Long getUserCouponId() {
        return userCouponId;
    }

    public void setUserCouponId(Long userCouponId) {
        this.userCouponId = userCouponId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getCouponMarketValue() {
        return couponMarketValue;
    }

    public void setCouponMarketValue(Double couponMarketValue) {
        this.couponMarketValue = couponMarketValue;
    }

    public Double getCouponAvailableValue() {
        return couponAvailableValue;
    }

    public void setCouponAvailableValue(Double couponAvailableValue) {
        this.couponAvailableValue = couponAvailableValue;
    }

    public Integer getUserCouponType() {
        return userCouponType;
    }

    public void setUserCouponType(Integer userCouponType) {
        this.userCouponType = userCouponType;
    }

    public String getUserCouponDesc() {
        return userCouponDesc;
    }

    public void setUserCouponDesc(String userCouponDesc) {
        this.userCouponDesc = userCouponDesc;
    }

    public Integer getUserCouponStatus() {
        return userCouponStatus;
    }

    public void setUserCouponStatus(Integer userCouponStatus) {
        this.userCouponStatus = userCouponStatus;
        this.userCouponStatusEnums = UserCouponStatusEnums.getUserCouponStatusEnums(userCouponStatus);
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }

    public String getActiviteTime() {
        return activiteTime;
    }

    public void setActiviteTime(String activiteTime) {
        this.activiteTime = activiteTime;
    }

    public String getExpiredTimeSql() {
        return expiredTimeSql;
    }

    public void setExpiredTimeSql(String expiredTimeSql) {
        this.expiredTimeSql = expiredTimeSql;
    }

    public void setExpiredTimeSql(int availablePeriod, CouponStoreAvailablePeriodUnitEnums couponStoreAvailablePeriodUnitEnums) {
        this.expiredTimeSql = "date_add(now(),interval "+availablePeriod+" "+ couponStoreAvailablePeriodUnitEnums.getAvailablePeriodUnit4Sql()+" )";
    }

    public CouponStore getCouponStore() {
        return couponStore;
    }

    public void setCouponStore(CouponStore couponStore) {
        this.couponStore = couponStore;
    }


    public Long getCouponTicketId() {
        return couponTicketId;
    }

    public void setCouponTicketId(Long couponTicketId) {
        this.couponTicketId = couponTicketId;
    }

    public UserCouponTypeAndDescEnums getUserCouponTypeAndDescEnums() {
        return userCouponTypeAndDescEnums;
    }

    public void setUserCouponTypeAndDescEnums(UserCouponTypeAndDescEnums userCouponTypeAndDescEnums) {
        this.userCouponTypeAndDescEnums = userCouponTypeAndDescEnums;
        this.setUserCouponType(userCouponTypeAndDescEnums.getUserCouponType());
        this.setUserCouponDesc(userCouponTypeAndDescEnums.getUserCouponDesc());
    }

    public UserCouponStatusEnums getUserCouponStatusEnums() {
        return userCouponStatusEnums;
    }

    public void setUserCouponStatusEnums(UserCouponStatusEnums userCouponStatusEnums) {
        this.userCouponStatusEnums = userCouponStatusEnums;
    }
}
