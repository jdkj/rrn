package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.ApplicationUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 用户资金明细
 * Created by Dsmart on 2016/10/25.
 */
public class UserAccountMx {

    private Long userAccountMxId;

    private Long userId;

    //订单编号
    @JsonIgnore
    private Long orderId;

    //订单类型
    //0:资金管理费用
    //1:充值
    //2:提现
    //3:利润提取
    //4:退保证金
    private Integer orderType;

    //总可用资金
    private Double totalMoney;

    //变动资金
    private Double changeMoney;

    //变动方式
    //0:充值提款
    //1:借款
    //2:服务费
    //3:理财
    //4:利润提取
    private Integer changeType;

    //变动描述
    private String changeDesc;


    private String createTime;


    private String updateTime;


    private String showOrderId;

    public Long getUserAccountMxId() {
        return userAccountMxId;
    }

    public void setUserAccountMxId(Long userAccountMxId) {
        this.userAccountMxId = userAccountMxId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Double getChangeMoney() {
        return changeMoney;
    }

    public void setChangeMoney(Double changeMoney) {
        this.changeMoney = changeMoney;
    }

    public Integer getChangeType() {
        return changeType;
    }

    public void setChangeType(Integer changeType) {
        this.changeType = changeType;
    }

    public String getChangeDesc() {
        return changeDesc;
    }

    public void setChangeDesc(String changeDesc) {
        this.changeDesc = changeDesc;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public String getShowOrderId() {
        if(this.getCreateTime()!=null){
            return ApplicationUtils.formatDate(this.getCreateTime(),"yyyy:MM:dd HH:mm:ss","yyyyMMddHHmmss")+this.getOrderId();
        }
        return showOrderId;
    }

    public void setShowOrderId(String showOrderId) {
        this.showOrderId = showOrderId;
        Long orderId = 0l;
        try{
            orderId = Long.parseLong(showOrderId.substring(12));
        }catch(Exception ex){}
        this.setOrderId(orderId);
    }
}
