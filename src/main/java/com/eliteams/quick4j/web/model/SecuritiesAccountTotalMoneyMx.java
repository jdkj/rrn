package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * 券商账户总资金明细
 */
public class SecuritiesAccountTotalMoneyMx implements Serializable {

    private static final long serialVersionUID = -924059149796321699L;
    /**
     * 流水编号
     */
    private Long mxId;

    /**
     * 券商账户编号
     */
    private Long securitiesAccountId;

    /**
     * 变动资金
     */
    private Double changeMoney;

    /**
     * 总资金
     */
    private Double totalMoney;

    /**
     * 变动类型
     */
    private Integer orderType;


    private String createTime;

    private String updateTime;


    public Long getMxId() {
        return mxId;
    }

    public void setMxId(Long mxId) {
        this.mxId = mxId;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Double getChangeMoney() {
        return changeMoney;
    }

    public void setChangeMoney(Double changeMoney) {
        this.changeMoney = changeMoney;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "SecuritiesAccountTotalMoneyMx{" +
                "mxId=" + mxId +
                ", securitiesAccountId=" + securitiesAccountId +
                ", changeMoney=" + changeMoney +
                ", totalMoney=" + totalMoney +
                ", orderType=" + orderType +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
