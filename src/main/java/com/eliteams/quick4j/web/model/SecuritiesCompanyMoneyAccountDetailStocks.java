package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * 券商账户持有股票明细模型
 * Created by Dsmart on 2017/4/10.
 */
public class SecuritiesCompanyMoneyAccountDetailStocks implements Serializable {

    /**
     * 流水号
     */
    private Long stockDetailId;


    /**
     * 券商资金账户编号
     */
    private Long securitiesAccountId;

    /**
     * 股票所属证券市场
     */
    private String stockMarket;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 持有股数
     */
    private Long stockNum;

    /**
     * 股票可售股数
     */
    private Long stockAvailableNum;

    /**
     * 计算后的买入价格
     */
    private Double buyPrice;

    /**
     * 股票当前价格
     */
    private Double currentPrice;

    /**
     * 当前持股市值
     */
    private Double currentMarketValue;
    /**
     * 购买该股票总消耗资金
     * 买入股票所消耗资金+买入股票所需手续费
     */
    private Double buyTotalExpenses;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;


    public Long getStockDetailId() {
        return stockDetailId;
    }

    public void setStockDetailId(Long stockDetailId) {
        this.stockDetailId = stockDetailId;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public String getStockMarket() {
        return stockMarket;
    }

    public void setStockMarket(String stockMarket) {
        this.stockMarket = stockMarket;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Long getStockNum() {
        return stockNum;
    }

    public void setStockNum(Long stockNum) {
        this.stockNum = stockNum;
    }

    public Long getStockAvailableNum() {
        return stockAvailableNum;
    }

    public void setStockAvailableNum(Long stockAvailableNum) {
        this.stockAvailableNum = stockAvailableNum;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Double getCurrentMarketValue() {
        return currentMarketValue;
    }

    public void setCurrentMarketValue(Double currentMarketValue) {
        this.currentMarketValue = currentMarketValue;
    }

    public Double getBuyTotalExpenses() {
        return buyTotalExpenses;
    }

    public void setBuyTotalExpenses(Double buyTotalExpenses) {
        this.buyTotalExpenses = buyTotalExpenses;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "SecuritiesCompanyMoneyAccountDetailStocks{" +
                "stockDetailId=" + stockDetailId +
                ", securitiesAccountId=" + securitiesAccountId +
                ", stockMarket='" + stockMarket + '\'' +
                ", stockCode='" + stockCode + '\'' +
                ", stockName='" + stockName + '\'' +
                ", stockNum=" + stockNum +
                ", stockAvailableNum=" + stockAvailableNum +
                ", buyPrice=" + buyPrice +
                ", currentPrice=" + currentPrice +
                ", currentMarketValue=" + currentMarketValue +
                ", buyTotalExpenses=" + buyTotalExpenses +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
