package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;
import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.model.StockOperate;

/**
 * Created by Dsmart on 2016/11/2.
 */
public class StockOperateRestrict extends PageRestrict<StockOperate> {

    private Long userId;

    private Long agreementId;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 委托数量
     */
    private Long commissionNum;

    /**
     * 委托价格
     */
    private Double commissionPrice;


    /**
     * 委托类型
     * 0:限价委托
     * 1:市价委托
     */
    private Integer commissionType=0;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public Long getCommissionNum() {
        return commissionNum;
    }

    public void setCommissionNum(Long commissionNum) {
        commissionNum = commissionNum/100*100;
        this.commissionNum = commissionNum;
    }

    public Double getCommissionPrice() {
        return commissionPrice;
    }

    public void setCommissionPrice(Double commissionPrice) {
        this.commissionPrice = CurrencyUtils.parseMoenyRoundHalfUp(commissionPrice+"");
    }

    public Integer getCommissionType() {
        return commissionType;
    }

    public void setCommissionType(Integer commissionType) {
        this.commissionType = commissionType;
    }
}
