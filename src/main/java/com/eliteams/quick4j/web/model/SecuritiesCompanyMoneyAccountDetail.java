package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * 券商户头资金表
 * Created by Dsmart on 2017/3/19.
 */
public class SecuritiesCompanyMoneyAccountDetail implements Serializable {
    private static final long serialVersionUID = 7739077151264586322L;

    private Long securitiesAccountId;


    private Double totalMoney;

    private Double availableMoney;

    private Double rate;

    private Integer status;

    private String securitiesCompany;

    private String securitiesCompanyAccountHost;

    private String createTime;

    private String updateTime;

    /**
     * 当前持仓股票市值
     */
    private Double stockValue;

    /**
     * 当前盈利
     */
    private Double profitMoney;

    /**
     * 历史总盈利
     */
    private Double historyProfitMoney;


    /**
     * 账户类型
     * 0:虚拟账户
     * 1:实盘账户
     */
    private Integer accountType;


    /**
     * 合约申请可用余额
     */
    private Double agreementAvailableMoney;


    /**
     * 购买股票预锁定资金
     */
    private Double preBuyLockMoney;


    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Double getAvailableMoney() {
        return availableMoney;
    }

    public void setAvailableMoney(Double availableMoney) {
        this.availableMoney = availableMoney;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }


    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSecuritiesCompany() {
        return securitiesCompany;
    }

    public void setSecuritiesCompany(String securitiesCompany) {
        this.securitiesCompany = securitiesCompany;
    }

    public String getSecuritiesCompanyAccountHost() {
        return securitiesCompanyAccountHost;
    }

    public void setSecuritiesCompanyAccountHost(String securitiesCompanyAccountHost) {
        this.securitiesCompanyAccountHost = securitiesCompanyAccountHost;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public Double getStockValue() {
        return stockValue;
    }

    public void setStockValue(Double stockValue) {
        this.stockValue = stockValue;
    }

    public Double getProfitMoney() {
        return profitMoney;
    }

    public void setProfitMoney(Double profitMoney) {
        this.profitMoney = profitMoney;
    }

    public Double getHistoryProfitMoney() {
        return historyProfitMoney;
    }

    public void setHistoryProfitMoney(Double historyProfitMoney) {
        this.historyProfitMoney = historyProfitMoney;
    }


    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }

    public Double getAgreementAvailableMoney() {
        return agreementAvailableMoney;
    }

    public void setAgreementAvailableMoney(Double agreementAvailableMoney) {
        this.agreementAvailableMoney = agreementAvailableMoney;
    }


    public Double getPreBuyLockMoney() {
        return preBuyLockMoney;
    }

    public void setPreBuyLockMoney(Double preBuyLockMoney) {
        this.preBuyLockMoney = preBuyLockMoney;
    }

    @Override
    public String toString() {
        return "SecuritiesCompanyMoneyAccountDetail{" +
                "securitiesAccountId=" + securitiesAccountId +
                ", totalMoney=" + totalMoney +
                ", availableMoney=" + availableMoney +
                ", rate=" + rate +
                ", status=" + status +
                ", securitiesCompany='" + securitiesCompany + '\'' +
                ", securitiesCompanyAccountHost='" + securitiesCompanyAccountHost + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", stockValue=" + stockValue +
                ", profitMoney=" + profitMoney +
                ", historyProfitMoney=" + historyProfitMoney +
                ", accountType=" + accountType +
                ", agreementAvailableMoney=" + agreementAvailableMoney +
                ", preBuyLockMoney=" + preBuyLockMoney +
                '}';
    }
}
