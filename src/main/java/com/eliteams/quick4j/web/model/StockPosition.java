package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.CurrencyUtils;

/**
 * 用户持仓统计数据模型
 * Created by Dsmart on 2016/10/19.
 */
public class StockPosition {

    /**
     * 持仓编号
     */
    private Long positionId;


    /**
     * 合约编号
     */
    private Long agreementId;


    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 股票编码
     */
    private String stockCode;


    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 持仓股数
     */
    private Long stockNum;

    /**
     * 可售股数
     */
    private Long stockAvailableNum = 0l;


    /**
     * 买入价格
     * 计算公式（买一价格*买一股数+买二价格*买二股数+....+买n价格*买n股数）/(买一股数+买二股数+....+买n股数)
     * ##.##
     */
    private Double buyPrice;


    /**
     * 当前价格
     */
    private Double currentPrice = 0d;

    /**
     * 当前市值
     * 计算公式:持仓股数*当前价格
     */
    private Double currentMarketValue = 0d;

    /**
     * 购买股票总花费
     * （买一价格*买一股数+买二价格*买二股数+....+买n价格*买n股数）
     */
    private Double buyTotalExpenses;

    /**
     * 浮动收益
     */
    private Double diffMoney;

    /**
     * 浮动收益比
     */
    private Double diffRate;


    private String createTime;

    private String updateTime;

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Long getStockNum() {
        return stockNum;
    }

    public void setStockNum(Long stockNum) {
        this.stockNum = stockNum;
    }

    public Long getStockAvailableNum() {
        return stockAvailableNum;
    }

    public void setStockAvailableNum(Long stockAvailableNum) {
        this.stockAvailableNum = stockAvailableNum;
    }

    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = CurrencyUtils.parseMoenyRoundHalfUp(buyPrice + "");
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = CurrencyUtils.parseMoenyRoundHalfUp(currentPrice + "");
    }

    public Double getCurrentMarketValue() {
        return CurrencyUtils.parseMoenyRoundHalfUp(""+currentMarketValue);
    }

    public void setCurrentMarketValue(Double currentMarketValue) {
        this.currentMarketValue = CurrencyUtils.parseMoenyRoundHalfUp("" + currentMarketValue);
    }

    public Double getBuyTotalExpenses() {
        return buyTotalExpenses;
    }

    public void setBuyTotalExpenses(Double buyTotalExpenses) {
        this.buyTotalExpenses = CurrencyUtils.parseMoenyRoundHalfUp("" + buyTotalExpenses);
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public Double getDiffMoney() {
        return CurrencyUtils.parseMoenyRoundHalfUp("" +(this.getCurrentMarketValue()-this.getBuyTotalExpenses()));
    }

    public void setDiffMoney(Double diffMoney) {
        this.diffMoney = diffMoney;
    }

    public Double getDiffRate() {
        return CurrencyUtils.parseMoenyRoundHalfUp("" +(this.getCurrentMarketValue()-this.getBuyTotalExpenses())*100/this.buyTotalExpenses);
    }

    public void setDiffRate(Double diffRate) {
        this.diffRate = diffRate;
    }
}
