package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.PrimaryKeyModifyUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 充值记录数据模型
 * Created by Dsmart on 2016/10/26.
 */
public class Recharge {
    @JsonIgnore
    private Long rechargeId;
    @JsonIgnore
    private Long userId;

    /**
     * 状态
     * 0:新建
     * 1:完成
     * 2:支付失败
     * 3：过期
     * 4:支付校验异常
     */
    private Integer status;

    /**
     * 支付通道
     * 0:微信支付
     *
     */
    private Integer payChannel=0;

    private Double money;

    /**
     * 支付描述
     */
    private String rechargeDesc="账户充值";

    /**
     * 支付成功采用的通道
     * 0:微信公众号h5支付
     * 1:微信扫码支付
     */
    private Integer paySuccessChannel;

    /**
     * ip地址
     */
    private String ip;

    /**
     * 订单创建时间
     */
    private String createTime;

    /**
     * 订单更新时间
     */
    private String updateTime;

    /**
     * 订单过期时间
     */
    private String expiredTime;

    private String showRechargeId;

    /**
     * 是否通过异步推送检测
     */
    private Integer asyncCheck;



    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }


    public Long getRechargeId() {
        return rechargeId;
    }

    public void setRechargeId(Long rechargeId) {
        this.rechargeId = rechargeId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPayChannel() {
        return payChannel;
    }

    public void setPayChannel(Integer payChannel) {
        this.payChannel = payChannel;
    }

    public String getRechargeDesc() {
        return rechargeDesc;
    }

    public void setRechargeDesc(String rechargeDesc) {
        this.rechargeDesc = rechargeDesc;
    }

    public Integer getPaySuccessChannel() {
        return paySuccessChannel;
    }

    public void setPaySuccessChannel(Integer paySuccessChannel) {
        this.paySuccessChannel = paySuccessChannel;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getExpiredTime() {
        return expiredTime;
    }

    public void setExpiredTime(String expiredTime) {
        this.expiredTime = expiredTime;
    }

    public String getShowRechargeId() {
        return PrimaryKeyModifyUtils.encodePrimaryKey(this.getRechargeId(),this.getCreateTime());
    }

    public void setShowRechargeId(String showRechargeId) {
        this.showRechargeId = showRechargeId;
    }

    public Integer getAsyncCheck() {
        return asyncCheck;
    }

    public void setAsyncCheck(Integer asyncCheck) {
        this.asyncCheck = asyncCheck;
    }
}
