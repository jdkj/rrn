package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * 券商户头总资金表
 * Created by Dsmart on 2017/3/19.
 */
public class SecuritiesCompanyMoneyAccount implements Serializable {
    private static final long serialVersionUID = 7578854943296310646L;

    private Long accountId;

    private Double totalMoney;

    private Double availableMoney;

    private String createTime;

    private String updateTime;


    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Double getAvailableMoney() {
        return availableMoney;
    }

    public void setAvailableMoney(Double availableMoney) {
        this.availableMoney = availableMoney;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
