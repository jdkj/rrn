package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2017/1/8.
 */
public class OperateLogRestrict extends PageRestrict {

    /**
     * 操作员编号
     */
    private Long adminId;

    /**
     * 回单号
     */
    private String stockOperateId;


    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public String getStockOperateId() {
        return stockOperateId;
    }

    public void setStockOperateId(String stockOperateId) {
        this.stockOperateId = stockOperateId;
    }
}
