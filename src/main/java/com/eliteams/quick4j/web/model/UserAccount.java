package com.eliteams.quick4j.web.model;

/**
 * 用户账户模型
 * Created by Dsmart on 2016/10/25.
 */
public class UserAccount {

    private Long userAccountId;

    private Long userId;

    private Double money=0.00d;

    private Double integral=0.0d;

    private String createTime;

    private String updateTime;

    /**
     * 贡献的总操盘手续费(剔除成本的)
     */
    private Double totalOperateFee;

    /**
     * 贡献的总合约管理费用(剔除成本的)
     */
    private Double totalManagementFee;

    /**
     * 用户贡献的总费用(剔除成本的)
     */
    private Double totalContributionFee;


    /**
     * 用户操盘以来获得的总利润(剔除成本的)
     */
    private Double totalProfit;


    /**
     * 用户申请的总操盘资金(历史合计数据)
     */
    private Double totalAgreementFund;


    public Long getUserAccountId() {
        return userAccountId;
    }

    public void setUserAccountId(Long userAccountId) {
        this.userAccountId = userAccountId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Double getIntegral() {
        return integral;
    }

    public void setIntegral(Double integral) {
        this.integral = integral;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public Double getTotalOperateFee() {
        return totalOperateFee;
    }

    public void setTotalOperateFee(Double totalOperateFee) {
        this.totalOperateFee = totalOperateFee;
    }

    public Double getTotalManagementFee() {
        return totalManagementFee;
    }

    public void setTotalManagementFee(Double totalManagementFee) {
        this.totalManagementFee = totalManagementFee;
    }

    public Double getTotalContributionFee() {
        return totalContributionFee;
    }

    public void setTotalContributionFee(Double totalContributionFee) {
        this.totalContributionFee = totalContributionFee;
    }

    public Double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Double getTotalAgreementFund() {
        return totalAgreementFund;
    }

    public void setTotalAgreementFund(Double totalAgreementFund) {
        this.totalAgreementFund = totalAgreementFund;
    }
}
