package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2017/3/22.
 */
public class SecuritiesCompanyMoneyAccountDetailMxRestrict extends PageRestrict {

    /**
     * 资金账户编号
     */
    private Long securitiesAccountId;

    /**
     * 订单类型
     * 0:生成合约锁定
     * 1:合约释放
     * 3:账户提款
     * 4:账户追加
     */
    private Integer orderType;


    /**
     * 查询开始日期
     */
    private String startDate;

    /**
     * 查询截止日期
     */
    private String endDate;


    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }


    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
