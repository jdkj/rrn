package com.eliteams.quick4j.web.model.enums.usercoupon;

/**
 * Created by Dsmart on 2016/12/8.
 */
public enum UserCouponTypeAndDescEnums {
    USER_REGISTER_GIFT(0,"用户注册赠送"),
    COUPON_TICKETS_EXCHANGE(1,"通过激活码兑换"),
    ACTIVITY_SYSTEM_GIFT(2,"活动赠送"),
    USER_TASK_REWARD(3,"任务奖励"),
    USER_INTEGRAL_EXCHANGE(4,"用户积分兑换");


    private Integer userCouponType;

    private String userCouponDesc;


    UserCouponTypeAndDescEnums(Integer userCouponType, String userCouponDesc) {
        this.userCouponType = userCouponType;
        this.userCouponDesc = userCouponDesc;
    }


    public Integer getUserCouponType() {
        return userCouponType;
    }

    public void setUserCouponType(Integer userCouponType) {
        this.userCouponType = userCouponType;
    }

    public String getUserCouponDesc() {
        return userCouponDesc;
    }

    public void setUserCouponDesc(String userCouponDesc) {
        this.userCouponDesc = userCouponDesc;
    }
}
