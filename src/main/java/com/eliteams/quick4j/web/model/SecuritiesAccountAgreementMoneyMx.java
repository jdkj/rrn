package com.eliteams.quick4j.web.model;

import java.io.Serializable;

public class SecuritiesAccountAgreementMoneyMx implements Serializable {
    private static final long serialVersionUID = 5071826985143461749L;


    private Long mxId;


    private Long securitiesAccountId;

    private Double totalMoney;

    private Double changeMoney;

    private Integer orderType;

    private String createTime;

    private String updateTime;

    /**
     * 合约编号
     */
    private Long agreementId;


    public Long getMxId() {
        return mxId;
    }

    public void setMxId(Long mxId) {
        this.mxId = mxId;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Double getChangeMoney() {
        return changeMoney;
    }

    public void setChangeMoney(Double changeMoney) {
        this.changeMoney = changeMoney;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }
}
