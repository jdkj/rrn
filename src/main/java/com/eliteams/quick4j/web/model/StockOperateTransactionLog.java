package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.container.StockOperateParamContainer;

import java.io.Serializable;

/**
 * Created by Dsmart on 2017/5/30.
 */
public class StockOperateTransactionLog implements Serializable {
    private static final long serialVersionUID = -5303378453326588710L;


    /**
     * 交易处理流水编号
     */
    private Long transactionLogId;


    /**
     * 用户申请操盘处理编号
     */
    private Long operateId;


    /**
     * 交易员操盘处理记录编号
     */
    private Long dealerOperateId;


    /**
     * 交易客户端合约编号
     */
    private String agreementNo;

    /**
     * 交易客户端成交编号
     * 用于检测当前这笔成交是否已经记录在案
     */
    private String transactionNo;


    /**
     * 成交类型
     * 0:卖出
     * 1:买入
     * 2:撤单
     */
    private Integer transactionType;


    /**
     * 成交类型描述
     */
    private String transactionTypeDesc;


    /**
     * 成交价格
     */
    private Double transactionPrice;


    /**
     * 成交数量
     */
    private Long transactionNum;

    /**
     * 股票编码
     */
    private String stockCode;

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 股票所属交易市场
     */
    private String stockMarket;

    /**
     * 交易委托时间
     */
    private String commissionTime;

    /**
     * 交易完成时间
     */
    private String transactionTime;


    /**
     * 券商账户编号
     */
    private Long securitiesAccountId;

    /**
     * 用户合约编号
     */
    private Long agreementId;

    /**
     * 操盘交易最低手续费
     */
    private Double operateBaseFee;

    /**
     * 操盘交易最低手续费率
     */
    private Double operateBaseRate;

    /**
     * 操盘交易调控费率(基于最低)
     */
    private Double operateControlRate;


    private Double operateFinalFee;

    /**
     * 操盘成本
     */
    private Double operateCostFee;

    /**
     * 过户税费率
     */
    private Double stockTransferRate;


    /**
     * 过户税最低收费
     */
    private Double stockTransferBaseFee;


    /**
     * 过户税最终收费
     */
    private Double stockTransferFinalFee;

    /**
     * 印花税费率
     */
    private Double stockStampDutyRate;


    /**
     * 印花税最终收费
     */
    private Double stockStampDutyFinalFee;


    /**
     * 查询状态
     * 0:不可查
     * 1:可查
     */
    private Integer queryStatus;


    /**
     * 创建时间
     */
    private String createTime;


    /**
     * 更新时间
     */
    private String updateTime;


    public StockOperateTransactionLog() {
    }

    /**
     * 撤单数据
     *
     * @param operateId
     * @param dealerOperateId
     * @param agreementNo
     * @param stockCode
     * @param stockName
     * @param stockMarket
     * @param commissionTime
     * @param securitiesAccountId
     * @param agreementId
     */
    public StockOperateTransactionLog(Long operateId, Long dealerOperateId, String agreementNo, String stockCode, String stockName, String stockMarket, String commissionTime, Long securitiesAccountId, Long agreementId, Long transactionNum) {
        this.operateId = operateId;
        this.dealerOperateId = dealerOperateId;
        this.agreementNo = agreementNo;
        this.stockCode = stockCode;
        this.stockName = stockName;
        this.stockMarket = stockMarket;
        this.commissionTime = commissionTime;
        this.securitiesAccountId = securitiesAccountId;
        this.agreementId = agreementId;
        this.transactionNum = transactionNum;
    }

    /**
     * 交易成功数据
     *
     * @param operateId
     * @param dealerOperateId
     * @param agreementNo
     * @param transactionNo
     * @param transactionType
     * @param transactionPrice
     * @param transactionNum
     * @param stockCode
     * @param stockName
     * @param stockMarket
     * @param commissionTime
     * @param securitiesAccountId
     * @param agreementId
     * @param operateBaseFee
     * @param operateBaseRate
     * @param operateControlRate
     * @param stockTransferRate
     * @param stockTransferBaseFee
     * @param stockStampDutyRate
     */
    public StockOperateTransactionLog(Long operateId, Long dealerOperateId, String agreementNo, String transactionNo, Integer transactionType, Double transactionPrice, Long transactionNum, String stockCode, String stockName, String stockMarket, String commissionTime, Long securitiesAccountId, Long agreementId, Double operateBaseFee, Double operateBaseRate, Double operateControlRate, Double stockTransferRate, Double stockTransferBaseFee, Double stockStampDutyRate) {
        this.operateId = operateId;
        this.dealerOperateId = dealerOperateId;
        this.agreementNo = agreementNo;
        this.transactionNo = transactionNo;
        this.transactionType = transactionType;
        this.transactionPrice = transactionPrice;
        this.transactionNum = transactionNum;
        this.stockCode = stockCode;
        this.stockName = stockName;
        this.stockMarket = stockMarket;
        this.commissionTime = commissionTime;
        this.securitiesAccountId = securitiesAccountId;
        this.agreementId = agreementId;
        this.operateBaseFee = operateBaseFee;
        this.operateBaseRate = operateBaseRate;
        this.operateControlRate = operateControlRate;
        this.stockTransferRate = stockTransferRate;
        this.stockTransferBaseFee = stockTransferBaseFee;
        this.stockStampDutyRate = stockStampDutyRate;
        calculateFees();

    }

    public Long getTransactionLogId() {
        return transactionLogId;
    }

    public void setTransactionLogId(Long transactionLogId) {
        this.transactionLogId = transactionLogId;
    }

    public Long getOperateId() {
        return operateId;
    }

    public void setOperateId(Long operateId) {
        this.operateId = operateId;
    }

    public Long getDealerOperateId() {
        return dealerOperateId;
    }

    public void setDealerOperateId(Long dealerOperateId) {
        this.dealerOperateId = dealerOperateId;
    }

    public String getAgreementNo() {
        return agreementNo;
    }

    public void setAgreementNo(String agreementNo) {
        this.agreementNo = agreementNo;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Integer getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(Integer transactionType) {
        this.transactionType = transactionType;
        switch (transactionType) {
            case 0:
            default:
                this.transactionTypeDesc = "卖出";
                break;
            case 1:
                this.transactionTypeDesc = "买入";
                break;
            case 2:
                this.transactionTypeDesc = "撤单";
                break;
        }
    }

    public String getTransactionTypeDesc() {
        return transactionTypeDesc;
    }

    public void setTransactionTypeDesc(String transactionTypeDesc) {
        this.transactionTypeDesc = transactionTypeDesc;
    }

    public Double getTransactionPrice() {
        return transactionPrice;
    }

    public void setTransactionPrice(Double transactionPrice) {
        this.transactionPrice = transactionPrice;
    }

    public Long getTransactionNum() {
        return transactionNum;
    }

    public void setTransactionNum(Long transactionNum) {
        this.transactionNum = transactionNum;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockMarket() {
        return stockMarket;
    }

    public void setStockMarket(String stockMarket) {
        this.stockMarket = stockMarket;
    }

    public String getCommissionTime() {
        return commissionTime;
    }

    public void setCommissionTime(String commisiionTime) {
        this.commissionTime = commisiionTime;
    }

    public String getTransactionTime() {
        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {
        this.transactionTime = transactionTime;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Double getOperateBaseFee() {
        return operateBaseFee;
    }

    public void setOperateBaseFee(Double operateBaseFee) {
        this.operateBaseFee = operateBaseFee;
    }

    public Double getOperateBaseRate() {
        return operateBaseRate;
    }

    public void setOperateBaseRate(Double operateBaseRate) {
        this.operateBaseRate = operateBaseRate;
    }

    public Double getOperateControlRate() {
        return operateControlRate;
    }

    public void setOperateControlRate(Double operateControlRate) {
        this.operateControlRate = operateControlRate;
    }

    public Double getStockTransferRate() {
        return stockTransferRate;
    }

    public void setStockTransferRate(Double stockTransferRate) {
        this.stockTransferRate = stockTransferRate;
    }

    public Double getStockTransferBaseFee() {
        return stockTransferBaseFee;
    }

    public void setStockTransferBaseFee(Double stockTransferBaseFee) {
        this.stockTransferBaseFee = stockTransferBaseFee;
    }

    public Double getStockTransferFinalFee() {
        return stockTransferFinalFee;
    }

    public void setStockTransferFinalFee(Double stockTransferFinalFee) {
        this.stockTransferFinalFee = stockTransferFinalFee;
    }

    public Double getStockStampDutyRate() {
        return stockStampDutyRate;
    }

    public void setStockStampDutyRate(Double stockStampDutyRate) {
        this.stockStampDutyRate = stockStampDutyRate;
    }

    public Double getStockStampDutyFinalFee() {
        return stockStampDutyFinalFee;
    }

    public void setStockStampDutyFinalFee(Double stockStampDutyFinalFee) {
        this.stockStampDutyFinalFee = stockStampDutyFinalFee;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getQueryStatus() {
        return queryStatus;
    }

    public void setQueryStatus(Integer queryStatus) {
        this.queryStatus = queryStatus;
    }

    public Double getOperateCostFee() {
        return operateCostFee;
    }

    public void setOperateCostFee(Double operateCostFee) {
        this.operateCostFee = operateCostFee;
    }

    @Override
    public String toString() {
        return "StockOperateTransactionLog{" +
                "transactionLogId=" + transactionLogId +
                ", operateId=" + operateId +
                ", dealerOperateId=" + dealerOperateId +
                ", agreementNo='" + agreementNo + '\'' +
                ", transactionNo='" + transactionNo + '\'' +
                ", transactionType=" + transactionType +
                ", transactionPrice=" + transactionPrice +
                ", transactionNum=" + transactionNum +
                ", stockCode='" + stockCode + '\'' +
                ", stockName='" + stockName + '\'' +
                ", stockMarket='" + stockMarket + '\'' +
                ", commissionTime='" + commissionTime + '\'' +
                ", transactionTime='" + transactionTime + '\'' +
                ", securitiesAccountId=" + securitiesAccountId +
                ", agreementId=" + agreementId +
                ", operateBaseFee=" + operateBaseFee +
                ", operateBaseRate=" + operateBaseRate +
                ", operateControlRate=" + operateControlRate +
                ", operateFinalFee=" + operateFinalFee +
                ", operateCostFee=" + operateCostFee +
                ", stockTransferRate=" + stockTransferRate +
                ", stockTransferBaseFee=" + stockTransferBaseFee +
                ", stockTransferFinalFee=" + stockTransferFinalFee +
                ", stockStampDutyRate=" + stockStampDutyRate +
                ", stockStampDutyFinalFee=" + stockStampDutyFinalFee +
                ", queryStatus=" + queryStatus +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }

    public Double getOperateFinalFee() {
        return operateFinalFee;
    }

    public void setOperateFinalFee(Double operateFinalFee) {
        this.operateFinalFee = operateFinalFee;
    }


    private void calculateFees() {
        Double transactionValue = this.getTransactionPrice() * this.getTransactionNum();
        //操盘手续费
        this.operateFinalFee = CurrencyUtils.max(operateBaseFee, transactionValue * (operateBaseRate + operateControlRate) / 100);

        //操盘成本费用
        this.operateCostFee = CurrencyUtils.max(StockOperateParamContainer.getOperateCostBaseFee(), transactionValue * (StockOperateParamContainer.getOperateCostBaseRate()) / 100);
        //过户费
        if (stockMarket.equals("sh")) {
            this.stockTransferFinalFee = CurrencyUtils.max(stockTransferBaseFee, transactionValue * stockTransferRate / 100);
        } else {
            this.stockTransferFinalFee = 0.00d;
        }
        //印花税
        //卖出的时候才有
        if (transactionType == 0) {
            this.stockStampDutyFinalFee = CurrencyUtils.parseMoenyRoundHalfUp(transactionValue * stockStampDutyRate / 100 + "");
        } else {
            this.stockStampDutyFinalFee = 0.00d;
        }
    }

    /**
     * 计算交易总消耗
     *
     * @return
     */
    public Double calculateByTransaction() {
        Double calculateValue = 0.0d;

        Double transactionValue = this.getTransactionPrice() * this.getTransactionNum();
        //印花税
        //卖出的时候才有
        if (transactionType == 0) {

            calculateValue = transactionValue - this.operateFinalFee - this.stockTransferFinalFee - this.stockStampDutyFinalFee;

        } else {//买入
            //锁定资金
            calculateValue = transactionValue + this.operateFinalFee + this.stockTransferFinalFee;
        }

        return calculateValue;
    }
}
