package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2016/10/23.
 */
public class UserRestrict extends PageRestrict {

    /**
     * 原始密码
     */
    private String orginalPassword;

    /**
     * 新密码
     */
    private String password;


    /**
     * 原交易密码
     */
    private String orginalAccountPassword;


    /**
     * 新交易密码
     */
    private String accountPassword;


    public String getOrginalPassword() {
        return orginalPassword;
    }

    public void setOrginalPassword(String orginalPassword) {
        this.orginalPassword = orginalPassword;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getOrginalAccountPassword() {
        return orginalAccountPassword;
    }

    public void setOrginalAccountPassword(String orginalAccountPassword) {
        this.orginalAccountPassword = orginalAccountPassword;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }
}
