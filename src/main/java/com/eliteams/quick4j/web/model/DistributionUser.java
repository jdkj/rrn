package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/*
 * 账户登陆用
 */
public class DistributionUser implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 2440741646642170328L;

    /**
     * 分销体系下用户编号
     */
    private Long distributionUserId;

    /**
     * 账户编号
     */
    private Long accountNum;

    /**
     * 密码
     */
    private String accountPassword;

    /**
     * 业务员姓名
     */
    private String accountName;

    /**
     * 业务员联系方式
     */
    private String accountPhone;

    /**
     * 所属级别
     * -1:客户
     * 0:总公司
     * 1:代理商
     * 2:业务员
     */
    private Integer accountLevel;

    /**
     * 父节点编号
     */
    private Long parentId = 1l;

    /**
     * 抽佣比率
     */
    private Double commissionProportion = 0.0d;

    /**
     * 客户编号
     */
    private Long userId;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 关系路径
     * 1_2_3
     */
    private String relationPath;


    private String createTime;

    private String updateTime;


    private DistributionDefaultCommission distributionDefaultCommission;


    private DistributionAccount distributionAccount;


    public DistributionUser() {
    }

    public DistributionUser(Long accountNum, String accountPassword) {
        this.accountNum = accountNum;
        this.accountPassword = accountPassword;
    }

    public Long getDistributionUserId() {
        return distributionUserId;
    }

    public void setDistributionUserId(Long distributionUserId) {
        this.distributionUserId = distributionUserId;
    }

    public Long getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(Long accountNum) {
        this.accountNum = accountNum;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getAccountPhone() {
        return accountPhone;
    }

    public void setAccountPhone(String accountPhone) {
        this.accountPhone = accountPhone;
    }

    public Integer getAccountLevel() {
        return accountLevel;
    }

    public void setAccountLevel(Integer accountLevel) {
        this.accountLevel = accountLevel;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Double getCommissionProportion() {
        return commissionProportion;
    }

    public void setCommissionProportion(Double commissionProportion) {
        this.commissionProportion = commissionProportion;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRelationPath() {
        return relationPath;
    }

    public void setRelationPath(String relationPath) {
        this.relationPath = relationPath;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public DistributionDefaultCommission getDistributionDefaultCommission() {
        return distributionDefaultCommission;
    }

    public void setDistributionDefaultCommission(DistributionDefaultCommission distributionDefaultCommission) {
        this.distributionDefaultCommission = distributionDefaultCommission;
    }

    public DistributionAccount getDistributionAccount() {
        return distributionAccount;
    }

    public void setDistributionAccount(DistributionAccount distributionAccount) {
        this.distributionAccount = distributionAccount;
    }

    @Override
    public String toString() {
        return "DistributionUser{" +
                "distributionUserId=" + distributionUserId +
                ", accountNum=" + accountNum +
                ", accountPassword='" + accountPassword + '\'' +
                ", accountName='" + accountName + '\'' +
                ", accountPhone='" + accountPhone + '\'' +
                ", accountLevel=" + accountLevel +
                ", parentId=" + parentId +
                ", commissionProportion=" + commissionProportion +
                ", userId=" + userId +
                ", status=" + status +
                ", relationPath='" + relationPath + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
