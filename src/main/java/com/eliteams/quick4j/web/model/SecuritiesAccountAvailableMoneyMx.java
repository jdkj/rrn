package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * 券商账户可用于购买股票额度明细
 */
public class SecuritiesAccountAvailableMoneyMx implements Serializable {


    private static final long serialVersionUID = -4651556995048352506L;

    private Long mxId;

    private Long securitiesAccountId;

    private Double totalMoney;

    private Double changeMoney;

    private Integer orderType;


    private String createTime;


    private String updateTime;

    /**
     * 股票交易编号
     */
    private Long operateId;


    /**
     * 用户编号
     */
    private Long userId;


    public Long getMxId() {
        return mxId;
    }

    public void setMxId(Long mxId) {
        this.mxId = mxId;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Double getChangeMoney() {
        return changeMoney;
    }

    public void setChangeMoney(Double changeMoney) {
        this.changeMoney = changeMoney;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getOperateId() {
        return operateId;
    }

    public void setOperateId(Long operateId) {
        this.operateId = operateId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "SecuritiesAccountAvailableMoneyMx{" +
                "mxId=" + mxId +
                ", securitiesAccountId=" + securitiesAccountId +
                ", totalMoney=" + totalMoney +
                ", changeMoney=" + changeMoney +
                ", orderType=" + orderType +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", operateId=" + operateId +
                ", userId=" + userId +
                '}';
    }
}
