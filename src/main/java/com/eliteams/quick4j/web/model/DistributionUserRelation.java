package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * 分销关系模型
 * Created by Dsmart on 2017/3/10.
 */
public class DistributionUserRelation implements Serializable {

    private static final long serialVersionUID = 7722602413513121927L;
    /**
     * 关系编号
     */
    private Long relationId;
    /**
     * 用户编号
     */
    private Long customerId;
    /**
     * 分销业务员编号
     */
    private Long distributionUserId;
    /**
     * 业务员抽佣比率
     */
    private Double commissionProportion;

    /**
     * 状态
     */
    private Integer status;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 更新时间
     */
    private String updateTime;


    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getDistributionUserId() {
        return distributionUserId;
    }

    public void setDistributionUserId(Long distributionUserId) {
        this.distributionUserId = distributionUserId;
    }

    public Double getCommissionProportion() {
        return commissionProportion;
    }

    public void setCommissionProportion(Double commissionProportion) {
        this.commissionProportion = commissionProportion;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "DistributionUserRelation{" +
                "relationId=" + relationId +
                ", customerId=" + customerId +
                ", distributionUserId=" + distributionUserId +
                ", commissionProportion=" + commissionProportion +
                ", status=" + status +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
