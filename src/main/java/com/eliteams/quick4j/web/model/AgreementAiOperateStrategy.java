package com.eliteams.quick4j.web.model;

import java.io.Serializable;

public class AgreementAiOperateStrategy implements Serializable {

    private static final long serialVersionUID = 248730703879942733L;
    private Long logId;

    /**
     * 合约编号
     */
    private Long agreementId;

    /**
     * 策略编号
     */
    private Long strategyId;

    /**
     * 状态
     * 0：停用
     * 1:启用
     */
    private Integer status;

    private String createTime;

    private String updateTime;


    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Long getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Long strategyId) {
        this.strategyId = strategyId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "AgreementAiOperateStrategy{" +
                "logId=" + logId +
                ", agreementId=" + agreementId +
                ", strategyId=" + strategyId +
                ", status=" + status +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
