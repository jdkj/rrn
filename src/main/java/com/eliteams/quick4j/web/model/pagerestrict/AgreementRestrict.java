package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;
import com.eliteams.quick4j.core.util.CurrencyUtils;

/**
 * Created by Dsmart on 2016/10/18.
 */
public class AgreementRestrict<Agreement> extends PageRestrict<Agreement> {

    /**
     * 每页显示多少数据
     */
    protected int pagesize=3;


    /**
     * 用户编号
     */
    private Long userId;


    /**
     * 合约类型
     * 0：短期
     * 1：中期
     */
    private Integer signAgreementType;

    /**
     * 借款合约类型编号
     */
    private Long borrowAgreementId;

    /**
     * 借款金额
     */
    private Double borrowMoney;

    /**
     * 合约编号
     */
    private Long agreementId;


    /**
     * 追加的保证金金额
     */
    private Double additionalLockMoney;


    private Long userCouponId=0l;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getSignAgreementType() {
        return signAgreementType;
    }

    public void setSignAgreementType(Integer signAgreementType) {
        this.signAgreementType = signAgreementType;
    }


    @Override
    public int getPagesize() {
        return pagesize;
    }

    @Override
    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }

    public Long getBorrowAgreementId() {
        return borrowAgreementId;
    }

    public void setBorrowAgreementId(Long borrowAgreementId) {
        this.borrowAgreementId = borrowAgreementId;
    }


    public Double getBorrowMoney() {
        return borrowMoney;
    }

    public void setBorrowMoney(Double borrowMoney) {
        this.borrowMoney = borrowMoney;
    }


    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Double getAdditionalLockMoney() {
        return additionalLockMoney;
    }

    public void setAdditionalLockMoney(Double additionalLockMoney) {
        this.additionalLockMoney = CurrencyUtils.parseMoenyRoundHalfUp(additionalLockMoney+"");
    }

    public Long getUserCouponId() {
        return userCouponId;
    }

    public void setUserCouponId(Long userCouponId) {
        this.userCouponId = userCouponId;
    }
}
