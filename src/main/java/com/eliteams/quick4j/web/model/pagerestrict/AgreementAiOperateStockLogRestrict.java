package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;
import org.apache.commons.lang.StringUtils;


/**
 * 合约只能操作股票请求参数集合
 */
public class AgreementAiOperateStockLogRestrict extends PageRestrict {

    private Long agreementId;

    private String stockCode;

    private String stockCodes;

    private String stockCodeArr[];


    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockCodes() {
        return stockCodes;
    }

    public void setStockCodes(String stockCodes) {
        this.stockCodes = stockCodes;
        if (StringUtils.isNotEmpty(stockCodes)) {
            this.stockCodeArr = stockCodes.split(",");
        }
    }

    public String[] getStockCodeArr() {
        return stockCodeArr;
    }

    public void setStockCodeArr(String[] stockCodeArr) {
        this.stockCodeArr = stockCodeArr;
    }
}
