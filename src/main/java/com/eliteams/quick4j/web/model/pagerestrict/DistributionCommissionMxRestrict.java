package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

public class DistributionCommissionMxRestrict extends PageRestrict {

    private Long distributionUserId;

    /**
     * 介绍人编号
     */
    private Long introducerId;

    /**
     * 介绍人所属分销层级
     */
    private Integer introducerLevel;


    /**
     * 0:合约利息
     * 1:操盘手续费
     */
    private Integer orderType;


    /**
     * 贡献者编号
     */
    private Long customerId;


    /**
     * 查询开始日期
     */
    private String startDate;

    /**
     * 查询截止日期
     */
    private String endDate;


    public Long getDistributionUserId() {
        return distributionUserId;
    }

    public void setDistributionUserId(Long distributionUserId) {
        this.distributionUserId = distributionUserId;
    }

    public Long getIntroducerId() {
        return introducerId;
    }

    public void setIntroducerId(Long introducerId) {
        this.introducerId = introducerId;
    }

    public Integer getIntroducerLevel() {
        return introducerLevel;
    }

    public void setIntroducerLevel(Integer introducerLevel) {
        this.introducerLevel = introducerLevel;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
