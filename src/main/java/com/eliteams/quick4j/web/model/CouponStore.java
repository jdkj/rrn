package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.web.model.enums.couponstore.CouponStoreAvailablePeriodUnitEnums;

/**
 * 优惠券仓库数据模型
 * Created by Dsmart on 2016/12/5.
 */
public class CouponStore {

    /**
     * 编号
     */
    private Long couponId;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 优惠券市值
     */
    private Double couponMarketValue;


    /**
     * 优惠券类型
     */
    private Integer couponType;


    /**
     * 有效周期
     */
    private Integer availablePeriod;


    /**
     * 有效周期单位
     */
    private Integer availablePeriodUnit;

    private CouponStoreAvailablePeriodUnitEnums couponStoreAvailablePeriodUnitEnums;


    /**
     * 是否是活动促销
     */
    private Integer isPromotion;

    /**
     * 是否可通过积分兑换
     */
    private Integer isExchangeable;


    /**
     * 优惠券状态
     */
    private Integer couponStatus;


    /**
     *创建时间
     */
    private String createTime;


    /**
     * 更新时间
     */
    private String updateTime;


    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public Double getCouponMarketValue() {
        return couponMarketValue;
    }

    public void setCouponMarketValue(Double couponMarketValue) {
        this.couponMarketValue = couponMarketValue;
    }

    public Integer getCouponType() {
        return couponType;
    }

    public void setCouponType(Integer couponType) {
        this.couponType = couponType;
    }

    public Integer getAvailablePeriod() {
        return availablePeriod;
    }

    public void setAvailablePeriod(Integer availablePeriod) {
        this.availablePeriod = availablePeriod;
    }

    public Integer getAvailablePeriodUnit() {
        return availablePeriodUnit;
    }

    public void setAvailablePeriodUnit(Integer availablePeriodUnit) {
        this.availablePeriodUnit = availablePeriodUnit;
        this.couponStoreAvailablePeriodUnitEnums = CouponStoreAvailablePeriodUnitEnums.getCouponStoreAvailablePeriodUnitEnums(availablePeriodUnit);
    }

    public Integer getIsPromotion() {
        return isPromotion;
    }

    public void setIsPromotion(Integer isPromotion) {
        this.isPromotion = isPromotion;
    }

    public Integer getIsExchangeable() {
        return isExchangeable;
    }

    public void setIsExchangeable(Integer isExchangeable) {
        this.isExchangeable = isExchangeable;
    }

    public Integer getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(Integer couponStatus) {
        this.couponStatus = couponStatus;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public CouponStoreAvailablePeriodUnitEnums getCouponStoreAvailablePeriodUnitEnums() {
        return couponStoreAvailablePeriodUnitEnums;
    }

    public void setCouponStoreAvailablePeriodUnitEnums(CouponStoreAvailablePeriodUnitEnums couponStoreAvailablePeriodUnitEnums) {
        this.couponStoreAvailablePeriodUnitEnums = couponStoreAvailablePeriodUnitEnums;
    }
}
