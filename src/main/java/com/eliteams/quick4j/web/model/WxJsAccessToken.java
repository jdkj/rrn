/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eliteams.quick4j.web.model;

/**
 * @description
 * @version 
 * @author Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-9-3, 11:38:40 
 */
public class WxJsAccessToken {
    
    /**
     * 微信公众平台js AccessToken
     */
    private  String accessToken;
    
    /**
     * 凭证有效时间  单位 秒   目前最长时间为7200秒
     */
    private int expiresIn;
    
    /**
     * 凭证创建时间
     */
    private Long createTime;
    
    /**
     * 用户授权后所获取到的 用于兑换accessToken
     */
    private String code;
    
    /**
     * 用户针对当前平台的openId
     */
    private String openId;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }
    

    
    
}
