package com.eliteams.quick4j.web.model.enums.couponstore;

/**
 * Created by Dsmart on 2016/12/9.
 */
public enum CouponStoreAvailablePeriodUnitEnums {
    HOUR(0, " hour "),
    DAY(1, " day "),
    WEEK(2, " week "),
    MONTH(3, " month "),
    YEAR(4, " year ");


    private Integer availablePeriodUnit;

    private String availablePeriodUnit4Sql;


    CouponStoreAvailablePeriodUnitEnums(Integer availablePeriodUnit, String availablePeriodUnit4Sql) {
        this.availablePeriodUnit = availablePeriodUnit;
        this.availablePeriodUnit4Sql = availablePeriodUnit4Sql;
    }

    /**
     * 根据可用周期类型获取
     * @param availablePeriodUnit
     * @return
     */
    public static CouponStoreAvailablePeriodUnitEnums getCouponStoreAvailablePeriodUnitEnums(Integer availablePeriodUnit) {
        switch (availablePeriodUnit) {
            case 0:
            default:
                return HOUR;
            case 1:
                return DAY;
            case 2:
                return WEEK;
            case 3:
                return MONTH;
            case 4:
                return YEAR;
        }
    }

    public Integer getAvailablePeriodUnit() {
        return availablePeriodUnit;
    }

    public void setAvailablePeriodUnit(Integer availablePeriodUnit) {
        this.availablePeriodUnit = availablePeriodUnit;
    }

    public String getAvailablePeriodUnit4Sql() {
        return availablePeriodUnit4Sql;
    }

    public void setAvailablePeriodUnit4Sql(String availablePeriodUnit4Sql) {
        this.availablePeriodUnit4Sql = availablePeriodUnit4Sql;
    }
}
