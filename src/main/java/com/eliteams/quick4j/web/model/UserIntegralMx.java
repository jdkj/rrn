package com.eliteams.quick4j.web.model;

/**
 * 用户积分数据模型
 * Created by Dsmart on 2016/12/7.
 */
public class UserIntegralMx {

    private Long userIntegralMxId;

    private Long userId;

    private Long orderId;

    private Double totalIntegral;

    private Double changeIntegral;

    private Integer changeType;

    private String changeDesc;

    private String createTime;

    private String updateTime;


    public Long getUserIntegralMxId() {
        return userIntegralMxId;
    }

    public void setUserIntegralMxId(Long userIntegralMxId) {
        this.userIntegralMxId = userIntegralMxId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Double getTotalIntegral() {
        return totalIntegral;
    }

    public void setTotalIntegral(Double totalIntegral) {
        this.totalIntegral = totalIntegral;
    }

    public Double getChangeIntegral() {
        return changeIntegral;
    }

    public void setChangeIntegral(Double changeIntegral) {
        this.changeIntegral = changeIntegral;
    }

    public Integer getChangeType() {
        return changeType;
    }

    public void setChangeType(Integer changeType) {
        this.changeType = changeType;
    }

    public String getChangeDesc() {
        return changeDesc;
    }

    public void setChangeDesc(String changeDesc) {
        this.changeDesc = changeDesc;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
