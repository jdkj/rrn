package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.core.util.PrimaryKeyModifyUtils;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * 合约模型
 * Created by Dsmart on 2016/10/18.
 */
public class Agreement {

    private Long agreementId;

    private Long userId;

    /**
     * 借款总金额
     */
    private Double borrowMoney;

    /**
     * 锁定金额(保证金)
     * #0
     */
    private Double lockMoney;


    /**
     * 警戒线
     */
    private Double warningLineMoney;


    /**
     * 平仓线
     */
    private Double closeLineMoney;


    /**
     * 购买股票未成交前锁定的资金
     */
    private Double prebuyLockMoney;





    /**
     * 持仓当前市值
     */
    private Double stockCurrentPrice=0.0d;


    /**
     * 当前可用额度
     */
    private Double availableCredit;


    /**
     * 借款倍数与比率
     */
    private Long signAgreementMultipleId;


    /**
     * 借款类型
     * 0:短期借款(利息日结)
     *
     */
    private Integer signAgreementType;


    /**
     * 资金管理费用(利息)
     */
    private Double accountManagementFee;

    private Double managementCostFee;

    /**
     * 操盘手续费基数
     */
    private Double operateFee;

    /**
     * 操盘手续费费率
     */
    private Double operateRate;

    /**
     * 合约签订日期
     */
    private String createTime;

    /**
     * 信息更新日期
     */
    private String updateTime;

    /**
     * 合约状态
     * 0:合约正常
     * 1:合约结算中
     * 2：合约结束
     */
    private Integer status;

    /**
     * 持续操盘周期
     * 短期:最多20
     * 中期:最多3
     */
    private Integer continueOperatePeriod=0;


    /**
     * 操盘最大周期
     */
    private Integer operateMaxPeriod=0;

    /**
     * 是否禁止买入
     * 当进入警告线及以下的时候
     * 0:否
     * 1:是
     */
    private Integer forbidBuy=0;

    /**
     * 是否强制平仓
     * 当进入止损线的时候
     * 0:否
     * 1:是
     */
    private Integer forceClose=0;


    /**
     * 操盘状态
     * 0:正常
     * 1:用户主动结束
     * 2：操盘资金管理费不足关闭
     * 3:操盘保证金不足关闭
     * 4：操盘最大期限关闭
     */
    @JsonIgnore
    private Integer operateStatus;

    /**
     * 操盘状态描述
     */
    @JsonIgnore
    private String operateDesc;


    /**
     * 操盘截止日期
     */
    private String operateDeadline;


    /**
     * 当前盈亏   持仓市值+可用余额-借款资金-锁定资金
     */
    private Double diffMoney=0.00d;

    /**
     * 持续操盘天数
     */
    private Integer continueOperateDay;

    /**
     * 申请合约类型描述
     */
    private String signAgreementTypeDesc;

    private String showAgreementId;


    private Long userCouponId=0l;

    private Double couponAvailableValue=0.0d;

    /**
     * 所属券商帐号
     */
    private Long securitiesAccountId;

    /**
     * 合约结算类型
     * 0:用户主动结算
     * 1:操盘资金管理非不足结算
     * 2:操盘最大期限结算
     */
    private Integer settlementType;


    /**
     * 总管理费用
     */
    private Double totalManagementFee;


    /**
     * 总操盘手续费
     */
    private Double totalOperateFee;


    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getBorrowMoney() {
        return borrowMoney;
    }

    public void setBorrowMoney(Double borrowMoney) {
        this.borrowMoney = CurrencyUtils.parseMoenyRoundHalfUp(borrowMoney+"");
    }

    public Double getLockMoney() {
        return lockMoney;
    }

    public void setLockMoney(Double lockMoney) {
        this.lockMoney = CurrencyUtils.parseMoenyRoundHalfUp(lockMoney+"");
    }

    public Double getStockCurrentPrice() {
        return stockCurrentPrice;
    }

    public void setStockCurrentPrice(Double stockCurrentPrice) {
        this.stockCurrentPrice = CurrencyUtils.parseMoenyRoundHalfUp(stockCurrentPrice+"");
    }

    public Double getAvailableCredit() {
        return availableCredit;
    }

    public void setAvailableCredit(Double availableCredit) {
        this.availableCredit = CurrencyUtils.parseMoenyRoundHalfUp(availableCredit+"");
    }

    public Long getSignAgreementMultipleId() {
        return signAgreementMultipleId;
    }

    public void setSignAgreementMultipleId(Long signAgreementMultipleId) {
        this.signAgreementMultipleId = signAgreementMultipleId;
    }

    public Integer getSignAgreementType() {
        return signAgreementType;
    }

    public void setSignAgreementType(Integer signAgreementType) {
        this.signAgreementType = signAgreementType;
    }

    public Double getAccountManagementFee() {
        return accountManagementFee;
    }

    public void setAccountManagementFee(Double accountManagementFee) {
        this.accountManagementFee = CurrencyUtils.parseMoenyRoundHalfUp(accountManagementFee+"");
    }

    public Double getOperateFee() {
        return operateFee;
    }

    public void setOperateFee(Double operateFee) {
        this.operateFee = CurrencyUtils.parseMoenyRoundHalfUp(operateFee+"");
    }

    public Double getOperateRate() {
        return operateRate;
    }

    public void setOperateRate(Double operateRate) {
        this.operateRate = operateRate;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getContinueOperatePeriod() {
        return continueOperatePeriod;
    }

    public void setContinueOperatePeriod(Integer continueOperatePeriod) {
        this.continueOperatePeriod = continueOperatePeriod;
    }

    public Integer getOperateMaxPeriod() {
        return operateMaxPeriod;
    }

    public void setOperateMaxPeriod(Integer operateMaxPeriod) {
        this.operateMaxPeriod = operateMaxPeriod;
    }

    public Integer getForbidBuy() {
        return forbidBuy;
    }

    public void setForbidBuy(Integer forbidBuy) {
        this.forbidBuy = forbidBuy;
    }

    public Integer getForceClose() {
        return forceClose;
    }

    public void setForceClose(Integer forceClose) {
        this.forceClose = forceClose;
    }


    public Integer getOperateStatus() {
        return operateStatus;
    }

    public void setOperateStatus(Integer operateStatus) {
        this.operateStatus = operateStatus;
    }

    public String getOperateDesc() {
        return operateDesc;
    }

    public void setOperateDesc(String operateDesc) {
        this.operateDesc = operateDesc;
    }

    public String getOperateDeadline() {
        return operateDeadline;
    }

    public void setOperateDeadline(String operateDeadline) {
        this.operateDeadline = operateDeadline;
    }


    public Double getDiffMoney() {
        return diffMoney;
    }

    public void setDiffMoney(Double diffMoney) {
        this.diffMoney = CurrencyUtils.parseMoenyRoundHalfUp(diffMoney+"");
    }


    public Integer getContinueOperateDay() {
        return continueOperateDay;
    }

    public void setContinueOperateDay(Integer continueOperateDay) {
        this.continueOperateDay = continueOperateDay;
    }

    public String getSignAgreementTypeDesc() {
        return signAgreementTypeDesc;
    }

    public void setSignAgreementTypeDesc(String signAgreementTypeDesc) {
        this.signAgreementTypeDesc = signAgreementTypeDesc;
    }


    public Double getWarningLineMoney() {
        return warningLineMoney;
    }

    public void setWarningLineMoney(Double warningLineMoney) {
        this.warningLineMoney = warningLineMoney;
    }

    public Double getCloseLineMoney() {
        return closeLineMoney;
    }

    public void setCloseLineMoney(Double closeLineMoney) {
        this.closeLineMoney = closeLineMoney;
    }

    public Double getPrebuyLockMoney() {
        return prebuyLockMoney;
    }

    public void setPrebuyLockMoney(Double prebuyLockMoney) {
        this.prebuyLockMoney = prebuyLockMoney;
    }

    public String getShowAgreementId() {
        return PrimaryKeyModifyUtils.encodePrimaryKey(this.getAgreementId(),this.getCreateTime());
    }

    public void setShowAgreementId(String showAgreementId) {
        this.showAgreementId = showAgreementId;
    }


    public Long getUserCouponId() {
        return userCouponId;
    }

    public void setUserCouponId(Long userCouponId) {
        this.userCouponId = userCouponId;
    }

    public Double getCouponAvailableValue() {
        return couponAvailableValue;
    }

    public void setCouponAvailableValue(Double couponAvailableValue) {
        this.couponAvailableValue = couponAvailableValue;
    }

    public Double getManagementCostFee() {
        return managementCostFee;
    }

    public void setManagementCostFee(Double managementCostFee) {
        this.managementCostFee = managementCostFee;
    }


    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Integer getSettlementType() {
        return settlementType;
    }

    public void setSettlementType(Integer settlementType) {
        this.settlementType = settlementType;
    }


    public Double getTotalManagementFee() {
        return totalManagementFee;
    }

    public void setTotalManagementFee(Double totalManagementFee) {
        this.totalManagementFee = totalManagementFee;
    }

    public Double getTotalOperateFee() {
        return totalOperateFee;
    }

    public void setTotalOperateFee(Double totalOperateFee) {
        this.totalOperateFee = totalOperateFee;
    }
}
