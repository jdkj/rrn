package com.eliteams.quick4j.web.model;

/**
 * Created by Dsmart on 2017/3/6.
 */
public class Withdrawal {

    /**
     * 提现申请流水编号
     */
    private Long withdrawalId;

    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 银行账户信息
     */
    private Long bankAccountId;

    /**
     * 提现金额
     */
    private Double money;

    /**
     * 状态
     * 0:申请
     * 1:转账完成
     * 2:转账失败
     */
    private Integer state;

    /**
     * 转账编号
     */
    private String transferId;

    /**
     * 转账描述
     */
    private String transferDesc;


    /**
     * 操作员编号
     */
    private Long operatorId;


    public Long getWithdrawalId() {
        return withdrawalId;
    }

    public void setWithdrawalId(Long withdrawalId) {
        this.withdrawalId = withdrawalId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getTransferDesc() {
        return transferDesc;
    }

    public void setTransferDesc(String transferDesc) {
        this.transferDesc = transferDesc;
    }

    public Long getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Long operatorId) {
        this.operatorId = operatorId;
    }
}
