package com.eliteams.quick4j.web.model.enums.usercoupon;

/**
 * Created by Dsmart on 2016/12/9.
 */
public enum UserCouponStatusEnums {
    USER_COUPON_AVAILABLE(0,"未使用"),
    USER_COUPON_USED(1,"已使用"),
    USER_COUPON_EXPIRED(2,"已过期");

    UserCouponStatusEnums(Integer userCouponStatus, String userCouponStatusDesc) {
        this.userCouponStatus = userCouponStatus;
        this.userCouponStatusDesc = userCouponStatusDesc;
    }

    /**
     * 根据状态值获取枚举类型
     * @param userCouponStatus
     * @return
     */
    public static UserCouponStatusEnums getUserCouponStatusEnums(Integer userCouponStatus){
        switch(userCouponStatus){
            case 0:
            default:
                    return USER_COUPON_AVAILABLE;
            case 1:
                return USER_COUPON_USED;
            case 2:
                return USER_COUPON_EXPIRED;
        }
    }

    private Integer userCouponStatus;

    private String userCouponStatusDesc;


    public Integer getUserCouponStatus() {
        return userCouponStatus;
    }

    public void setUserCouponStatus(Integer userCouponStatus) {
        this.userCouponStatus = userCouponStatus;
    }

    public String getUserCouponStatusDesc() {
        return userCouponStatusDesc;
    }

    public void setUserCouponStatusDesc(String userCouponStatusDesc) {
        this.userCouponStatusDesc = userCouponStatusDesc;
    }
}
