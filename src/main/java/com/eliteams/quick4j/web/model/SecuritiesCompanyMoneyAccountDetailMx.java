package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * Created by Dsmart on 2017/3/19.
 */
public class SecuritiesCompanyMoneyAccountDetailMx implements Serializable {
    private static final long serialVersionUID = 1294344668408859773L;


    private Long detailMxId;

    private Long securitiesAccountId;


    private SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail;

    private Double totalMoney;

    private Double changeMoney;


    private Long orderId;

    private Integer orderType;

    private String createTime;

    private String updateTime;


    public Long getDetailMxId() {
        return detailMxId;
    }

    public void setDetailMxId(Long detailMxId) {
        this.detailMxId = detailMxId;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public SecuritiesCompanyMoneyAccountDetail getSecuritiesCompanyMoneyAccountDetail() {
        return securitiesCompanyMoneyAccountDetail;
    }

    public void setSecuritiesCompanyMoneyAccountDetail(SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail) {
        this.securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetail;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Double getChangeMoney() {
        return changeMoney;
    }

    public void setChangeMoney(Double changeMoney) {
        this.changeMoney = changeMoney;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
