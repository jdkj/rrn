package com.eliteams.quick4j.web.model;

import org.codehaus.jackson.map.ObjectMapper;

/**
 * Created by Dsmart on 2016/10/10.
 */
public class UserParamInfoTag {

    /**
     * 是否绑定手机号
     */
    private boolean phoneTag = false;

    /**
     * 登录密码是否设置
     */
    private boolean passwordTag = false;

    /**
     * 账户操作密码是否设置
     */
    private boolean accountPasswordTag = false;


    /**
     * 是否通过身份认证
     */
    private boolean idTag = false;

    /**
     * 是否绑定银行账户信息
     */
    private boolean bankTag = false;


    public UserParamInfoTag() {
    }

    public UserParamInfoTag(String paramInfo) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            UserParamInfoTag userParamInfoTag = objectMapper.readValue(paramInfo, UserParamInfoTag.class);
            this.setAccountPasswordTag(userParamInfoTag.isAccountPasswordTag());
            this.setPasswordTag(userParamInfoTag.isPasswordTag());
            this.setPhoneTag(userParamInfoTag.isPhoneTag());
            this.setIdTag(userParamInfoTag.isIdTag());
            this.setBankTag(userParamInfoTag.isBankTag());
        } catch (Exception ex) {

        }

    }

    public boolean isPhoneTag() {
        return phoneTag;
    }

    public void setPhoneTag(boolean phoneTag) {
        this.phoneTag = phoneTag;
    }

    public boolean isPasswordTag() {
        return passwordTag;
    }

    public void setPasswordTag(boolean passwordTag) {
        this.passwordTag = passwordTag;
    }

    public boolean isAccountPasswordTag() {
        return accountPasswordTag;
    }

    public void setAccountPasswordTag(boolean accountPasswordTag) {
        this.accountPasswordTag = accountPasswordTag;
    }

    public boolean isIdTag() {
        return idTag;
    }

    public void setIdTag(boolean idTag) {
        this.idTag = idTag;
    }

    public boolean isBankTag() {
        return bankTag;
    }

    public void setBankTag(boolean bankTag) {
        this.bankTag = bankTag;
    }

    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (Exception ex) {
            return "{\"phoneTag\":false,\"passwordTag\":false,\"accountTag\":false,\"idTag\":false,\"bankTag\":false}";
        }
    }


}
