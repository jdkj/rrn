package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;
import com.eliteams.quick4j.web.model.SecuritiesAccountTotalMoneyMx;

public class SecuritiesAccountTotalMoneyMxRestrict extends PageRestrict<SecuritiesAccountTotalMoneyMx> {


    /**
     * 券商编号
     */
    private Long securitiesAccountId;


    /**
     * 资金变化类型与方向
     */
    private Integer orderType;


    private String startDate;


    private String endDate;


    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }


    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
