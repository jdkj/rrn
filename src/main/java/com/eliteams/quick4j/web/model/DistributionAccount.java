package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * Created by Dsmart on 2017/3/10.
 */
public class DistributionAccount implements Serializable {

    /**
     * 佣金账户编号
     */
    private Long distributionUserAccountId;

    /**
     * 销售员编号
     */
    private Long distributionUserId;

    /**
     * 佣金总数
     */
    private Double commissionMoney;


    private Double totalCommissionMoney;

    private Double contributionCommissionMoney;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;


    public Long getDistributionUserAccountId() {
        return distributionUserAccountId;
    }

    public void setDistributionUserAccountId(Long distributionUserAccountId) {
        this.distributionUserAccountId = distributionUserAccountId;
    }

    public Long getDistributionUserId() {
        return distributionUserId;
    }

    public void setDistributionUserId(Long distributionUserId) {
        this.distributionUserId = distributionUserId;
    }

    public Double getCommissionMoney() {
        return commissionMoney;
    }

    public void setCommissionMoney(Double commissionMoney) {
        this.commissionMoney = commissionMoney;
    }

    public Double getTotalCommissionMoney() {
        return totalCommissionMoney;
    }

    public void setTotalCommissionMoney(Double totalCommissionMoney) {
        this.totalCommissionMoney = totalCommissionMoney;
    }

    public Double getContributionCommissionMoney() {
        return contributionCommissionMoney;
    }

    public void setContributionCommissionMoney(Double contributionCommissionMoney) {
        this.contributionCommissionMoney = contributionCommissionMoney;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "DistributionAccount{" +
                "distributionUserAccountId=" + distributionUserAccountId +
                ", distributionUserId=" + distributionUserId +
                ", commissionMoney=" + commissionMoney +
                ", status=" + status +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
