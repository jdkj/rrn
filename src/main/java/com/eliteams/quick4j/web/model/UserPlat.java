/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.ApplicationUtils;

/**
 * @description 用户第三方平台数据
 * @version 1.0
 * @author Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-9-3, 21:04:25
 */
public class UserPlat {

    /**
     * 第三方平台用户信息编号
     */
    private Long userPlatId;

    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 平台类型 0:微信公众平台
     */
    private int platId=0;

    /**
     * 用户openid
     */
    private String openId;

    /**
     * 用户unionId
     */
    private String unionId;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像地址
     */
    private String imgUrl;
    
    /**
     * 创建时间
     */
    private String createTime;
    
    
    /**
     * 更新时间
     */
    private String updateTime;
    

    public Long getUserPlatId() {
        return userPlatId;
    }

    public void setUserPlatId(Long userPlatId) {
        this.userPlatId = userPlatId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public int getPlatId() {
        return platId;
    }

    public void setPlatId(int platId) {
        this.platId = platId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getUnionId() {
        return unionId;
    }

    public void setUnionId(String unionId) {
        this.unionId = unionId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = ApplicationUtils.filterEmoji(nickName);
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    
    
    

}
