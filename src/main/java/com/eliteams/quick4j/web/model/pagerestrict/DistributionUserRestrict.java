package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2017/3/10.
 */
public class DistributionUserRestrict extends PageRestrict {


    /**
     * 分销员编号
     */
    private Long distributionUserId;


    //账户编号
    private Long accountNum;

    //密码
    private String accountPassword;

    //新密码
    private String newAccountPassword;

    //账户名
    private String accountName;

    //账号级别
    private Integer accountLevel;

    //代理编号
    private Long parentId;


    //佣金比例
    private Double commissionProportion;

    //状态
    private Integer status;


    private String startDate;


    private String endDate;


    public Long getDistributionUserId() {
        return distributionUserId;
    }

    public void setDistributionUserId(Long distributionUserId) {
        this.distributionUserId = distributionUserId;
    }

    public Long getAccountNum() {
        return accountNum;
    }

    public void setAccountNum(Long accountNum) {
        this.accountNum = accountNum;
    }

    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }

    public String getNewAccountPassword() {
        return newAccountPassword;
    }

    public void setNewAccountPassword(String newAccountPassword) {
        this.newAccountPassword = newAccountPassword;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getAccountLevel() {
        return accountLevel;
    }

    public void setAccountLevel(Integer accountLevel) {
        this.accountLevel = accountLevel;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Double getCommissionProportion() {
        return commissionProportion;
    }

    public void setCommissionProportion(Double commissionProportion) {
        this.commissionProportion = commissionProportion;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "DistributionUserRestrict{" +
                "accountNum=" + accountNum +
                ", accountPassword='" + accountPassword + '\'' +
                ", newAccountPassword='" + newAccountPassword + '\'' +
                ", accountName='" + accountName + '\'' +
                ", accountLevel=" + accountLevel +
                ", parentId=" + parentId +
                ", commissionProportion=" + commissionProportion +
                ", status=" + status +
                '}';
    }
}
