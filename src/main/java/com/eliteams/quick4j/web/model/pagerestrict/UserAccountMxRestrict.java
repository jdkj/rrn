package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;
import com.eliteams.quick4j.web.model.UserAccountMx;

/**
 * Created by Dsmart on 2016/10/25.
 */
public class UserAccountMxRestrict extends PageRestrict<UserAccountMx> {


    private Long userId;


    private Integer changeType;


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getChangeType() {
        return changeType;
    }

    public void setChangeType(Integer changeType) {
        this.changeType = changeType;
    }
}
