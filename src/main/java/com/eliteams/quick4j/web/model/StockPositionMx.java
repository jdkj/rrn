package com.eliteams.quick4j.web.model;

/**
 * 用户持仓明细表
 * Created by Dsmart on 2016/10/19.
 */
public class StockPositionMx {

    private Long stockPositionMxId;


    private Long positionId;

    private Long userId;

    private String stockCode;

    private String stockName;


    private Long stockNum;


    private Double buyPrice;

    /**
     * 锁定状态
     */
    private Integer lockStatus = 0;

    private String createTime;


    private String updateTime;


    public Long getStockPositionMxId() {
        return stockPositionMxId;
    }

    public void setStockPositionMxId(Long stockPositionMxId) {
        this.stockPositionMxId = stockPositionMxId;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }


    public Double getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Double buyPrice) {
        this.buyPrice = buyPrice;
    }


    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public Long getStockNum() {
        return stockNum;
    }

    public void setStockNum(Long stockNum) {
        this.stockNum = stockNum;
    }

    public Integer getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(Integer lockStatus) {
        this.lockStatus = lockStatus;
    }
}
