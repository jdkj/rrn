package com.eliteams.quick4j.web.model;

/**
 * Created by Dsmart on 2016/12/5.
 */
public class CouponTickets {

    /**
     * 优惠券激活码兑换编号
     */
    private Long couponTicketId;


    /**
     * 优惠券编号
     */
    private Long couponId;


    /**
     * 优惠券类型
     * 0：一次性
     * 1：可重复使用
     */
    private Integer couponTicketType;


    /**
     * 优惠券状态
     * 0:创建
     * 1:已兑换
     * 2:停用
     */
    private Integer couponTicketStatus;


    /**
     * 用户编号
     */
    private Long userId;


    /**
     * 创建时间
     */
    private String createTime;


    /**
     * 更新时间
     */
    private String updateTime;


    /**
     * 激活时间
     */
    private String activiteTime;


    /**
     * 每人限制可领取次数
     */
    private Integer limitNo=0;



    public Long getCouponTicketId() {
        return couponTicketId;
    }

    public void setCouponTicketId(Long couponTicketId) {
        this.couponTicketId = couponTicketId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Integer getCouponTicketType() {
        return couponTicketType;
    }

    public void setCouponTicketType(Integer couponTicketType) {
        this.couponTicketType = couponTicketType;
    }

    public Integer getCouponTicketStatus() {
        return couponTicketStatus;
    }

    public void setCouponTicketStatus(Integer couponTicketStatus) {
        this.couponTicketStatus = couponTicketStatus;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getActiviteTime() {
        return activiteTime;
    }

    public void setActiviteTime(String activiteTime) {
        this.activiteTime = activiteTime;
    }


    public Integer getLimitNo() {
        return limitNo;
    }

    public void setLimitNo(Integer limitNo) {
        this.limitNo = limitNo;
    }


}
