package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

public class StockBaseRestrict extends PageRestrict {

    /**
     * 流通市值
     */
    private Double flowValue;


    /**
     * 市盈率
     */
    private Double pe;


    /**
     * 模糊查询的关键字
     */
    private String fuzzyKeys;


    public Double getFlowValue() {
        return flowValue;
    }

    public void setFlowValue(Double flowValue) {
        this.flowValue = flowValue;
    }

    public Double getPe() {
        return pe;
    }

    public void setPe(Double pe) {
        this.pe = pe;
    }

    public String getFuzzyKeys() {
        return fuzzyKeys;
    }

    public void setFuzzyKeys(String fuzzyKeys) {
        this.fuzzyKeys = fuzzyKeys;
    }
}
