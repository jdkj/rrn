package com.eliteams.quick4j.web.model;

/**
 * 用户身份认证模型
 * Created by Dsmart on 2016/10/23.
 */
public class UserIdAuth {

    /**
     * 认证编号
     */
    private Long authId;

    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 身份证编号
     */
    private String idNo;

    /**
     * 用户真实姓名
     */
    private String idName;

    /**
     * 状态
     */
    private Integer status;


    private String createTime;

    private String updateTime;


    public Long getAuthId() {
        return authId;
    }

    public void setAuthId(Long authId) {
        this.authId = authId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
