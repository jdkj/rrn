package com.eliteams.quick4j.web.model;

/**
 * 禁止操作股票模型
 * Created by Dsmart on 2016/10/17.
 */
public class ForbidStock {

    private Long forbidId;

    private String stockCode;

    private String stockName;

    private String createTime;

    private String updateTime;


    public Long getForbidId() {
        return forbidId;
    }

    public void setForbidId(Long forbidId) {
        this.forbidId = forbidId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
