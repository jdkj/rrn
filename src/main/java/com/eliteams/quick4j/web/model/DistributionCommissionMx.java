package com.eliteams.quick4j.web.model;

import java.io.Serializable;

public class DistributionCommissionMx implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 6826856339581519075L;

    private Long mxId;

    private Long distributionUserId;

    private Long orderId;

    private Integer orderType;

    private Double totalCommission;

    private Double gainCommission;

    private Long customerId;

    private DistributionUser customer;

    private Double commissionProportion;

    /**
     * 介绍人编号
     */
    private Long introducerId;

    /**
     * 介绍人等级
     */
    private Integer introducerLevel;


    private DistributionUser introducer;

    private DistributionDefaultCommission distributionDefaultCommission;


    private String createTime;

    private String updateTime;


    public Long getMxId() {
        return mxId;
    }

    public void setMxId(Long mxId) {
        this.mxId = mxId;
    }

    public Long getDistributionUserId() {
        return distributionUserId;
    }

    public void setDistributionUserId(Long distributionUserId) {
        this.distributionUserId = distributionUserId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public Double getTotalCommission() {
        return totalCommission;
    }

    public void setTotalCommission(Double totalCommission) {
        this.totalCommission = totalCommission;
    }

    public Double getGainCommission() {
        return gainCommission;
    }

    public void setGainCommission(Double gainCommission) {
        this.gainCommission = gainCommission;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public DistributionUser getCustomer() {
        return customer;
    }

    public void setCustomer(DistributionUser customer) {
        this.customer = customer;
    }

    public Double getCommissionProportion() {
        return commissionProportion;
    }

    public void setCommissionProportion(Double commissionProportion) {
        this.commissionProportion = commissionProportion;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Long getIntroducerId() {
        return introducerId;
    }

    public void setIntroducerId(Long introducerId) {
        this.introducerId = introducerId;
    }

    public Integer getIntroducerLevel() {
        return introducerLevel;
    }

    public void setIntroducerLevel(Integer introducerLevel) {
        this.introducerLevel = introducerLevel;
    }

    public DistributionUser getIntroducer() {
        return introducer;
    }

    public void setIntroducer(DistributionUser introducer) {
        this.introducer = introducer;
    }

    public DistributionDefaultCommission getDistributionDefaultCommission() {
        return distributionDefaultCommission;
    }

    public void setDistributionDefaultCommission(DistributionDefaultCommission distributionDefaultCommission) {
        this.distributionDefaultCommission = distributionDefaultCommission;
    }

    @Override
    public String toString() {
        return "DistributionCommissionMx{" +
                "mxId=" + mxId +
                ", distributionUserId=" + distributionUserId +
                ", orderId=" + orderId +
                ", orderType=" + orderType +
                ", totalCommission=" + totalCommission +
                ", gainCommission=" + gainCommission +
                ", customerId=" + customerId +
                ", commissionProportion=" + commissionProportion +
                ", introducerId=" + introducerId +
                ", introducerLevel=" + introducerLevel +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
