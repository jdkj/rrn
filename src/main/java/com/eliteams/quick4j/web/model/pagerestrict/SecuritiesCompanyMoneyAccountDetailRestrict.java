package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2017/3/22.
 */
public class SecuritiesCompanyMoneyAccountDetailRestrict extends PageRestrict {

    /**
     * 券商账户编号
     */
    private Long securitiesAccountId;


    /**
     * 资金额度
     */
    private Double changeMoney;


    /**
     * 券商名称
     */
    private String securitiesCompany;

    /**
     * 开户人
     */
    private String securitiesCompanyAccountHost;


    private String startDate;

    private String endDate;


    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Double getChangeMoney() {
        return changeMoney;
    }

    public void setChangeMoney(Double changeMoney) {
        this.changeMoney = changeMoney;
    }

    public String getSecuritiesCompany() {
        return securitiesCompany;
    }

    public void setSecuritiesCompany(String securitiesCompany) {
        this.securitiesCompany = securitiesCompany;
    }

    public String getSecuritiesCompanyAccountHost() {
        return securitiesCompanyAccountHost;
    }

    public void setSecuritiesCompanyAccountHost(String securitiesCompanyAccountHost) {
        this.securitiesCompanyAccountHost = securitiesCompanyAccountHost;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
