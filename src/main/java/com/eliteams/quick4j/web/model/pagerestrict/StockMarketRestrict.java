package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2016/11/22.
 */
public class StockMarketRestrict extends PageRestrict {

    /**
     * 批量更新数据最大值为200
     */
    private int pagesize = 200;

    @Override
    public int getPagesize() {
        return pagesize;
    }

    @Override
    public void setPagesize(int pagesize) {
        this.pagesize = pagesize;
    }
}
