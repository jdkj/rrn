package com.eliteams.quick4j.web.model;

import java.io.Serializable;

public class AgreementAiOperateStockLog implements Serializable {

    private static final long serialVersionUID = -1513740921770278781L;
    private Long logId;

    /**
     * 合约编号
     */
    private Long agreementId;

    /**
     * 股票所属券商市场
     * sh：上海
     * sz:深圳
     */
    private String stockMarket;

    /**
     * 股票编码
     * 600050
     */
    private String stockCode;

    /**
     * 股票名称
     * 中国联通
     */
    private String stockName;

    /**
     * 选入价格
     */
    private Double stockPrice;

    /**
     * 当前价格
     */
    private Double currentPrice;

    private String createTime;

    private String updateTime;


    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public String getStockMarket() {
        return stockMarket;
    }

    public void setStockMarket(String stockMarket) {
        this.stockMarket = stockMarket;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public Double getStockPrice() {
        return stockPrice;
    }

    public void setStockPrice(Double stockPrice) {
        this.stockPrice = stockPrice;
    }

    public Double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "AgreementAiOperateStockLog{" +
                "logId=" + logId +
                ", agreementId=" + agreementId +
                ", stockMarket='" + stockMarket + '\'' +
                ", stockCode='" + stockCode + '\'' +
                ", stockName='" + stockName + '\'' +
                ", stockPrice=" + stockPrice +
                ", currentPrice=" + currentPrice +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
