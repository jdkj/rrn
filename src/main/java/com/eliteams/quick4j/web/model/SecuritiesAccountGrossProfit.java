package com.eliteams.quick4j.web.model;

import java.io.Serializable;

public class SecuritiesAccountGrossProfit implements Serializable {


    private static final long serialVersionUID = 2755751829153351628L;

    /**
     * 流水号
     */
    private Long grossProfitId;


    /**
     * 券商资金帐号编号
     */
    private Long securitiesAccountId;


    /**
     * 用户编号
     */
    private Long userId;


    /**
     * 产生利润
     */
    private Double profitMoney;


    /**
     * 总利润
     */
    private Double totalProfitMoney;

    /**
     * 产生利润类型
     * 0:操盘管理费用
     * 1:操盘手续费
     */
    private Integer profitType;


    /**
     * 盈利变更类型
     */
    private Integer profitChangeType;


    /**
     * 产生利润触发编号
     */
    private Long profitIssueId;

    /**
     * 创建时间
     */
    private String createTime;


    /**
     * 更新时间
     */
    private String updateTime;


    public Long getGrossProfitId() {
        return grossProfitId;
    }

    public void setGrossProfitId(Long grossProfitId) {
        this.grossProfitId = grossProfitId;
    }

    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Double getProfitMoney() {
        return profitMoney;
    }

    public void setProfitMoney(Double profitMoney) {
        this.profitMoney = profitMoney;
    }

    public Double getTotalProfitMoney() {
        return totalProfitMoney;
    }

    public void setTotalProfitMoney(Double totalProfitMoney) {
        this.totalProfitMoney = totalProfitMoney;
    }

    public Integer getProfitType() {
        return profitType;
    }

    public void setProfitType(Integer profitType) {
        this.profitType = profitType;
    }

    public Long getProfitIssueId() {
        return profitIssueId;
    }

    public void setProfitIssueId(Long profitIssueId) {
        this.profitIssueId = profitIssueId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public Integer getProfitChangeType() {
        return profitChangeType;
    }

    public void setProfitChangeType(Integer profitChangeType) {
        this.profitChangeType = profitChangeType;
    }

    @Override
    public String toString() {
        return "SecuritiesAccountGrossProfit{" +
                "grossProfitId=" + grossProfitId +
                ", securitiesAccountId=" + securitiesAccountId +
                ", userId=" + userId +
                ", profitMoney=" + profitMoney +
                ", profitType=" + profitType +
                ", profitChangeType=" + profitChangeType +
                ", profitIssueId=" + profitIssueId +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
