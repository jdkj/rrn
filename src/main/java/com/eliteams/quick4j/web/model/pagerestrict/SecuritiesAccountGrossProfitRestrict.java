package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;
import com.eliteams.quick4j.web.model.SecuritiesAccountGrossProfit;

public class SecuritiesAccountGrossProfitRestrict extends PageRestrict<SecuritiesAccountGrossProfit> {

    /**
     * 券商账户编号
     */
    private Long securitiesAccountId;

    /**
     * 盈利变化
     * 0:提取
     * 1:增加
     */
    private Integer profitChangeType;


    /**
     * 盈利来源
     * 0:操盘管理费用
     * 1:操盘手续费
     */
    private Integer profitType;


    /**
     * 开始日期
     */
    private String startDate;

    /**
     * 结束日期
     */
    private String endDate;


    public Long getSecuritiesAccountId() {
        return securitiesAccountId;
    }

    public void setSecuritiesAccountId(Long securitiesAccountId) {
        this.securitiesAccountId = securitiesAccountId;
    }

    public Integer getProfitChangeType() {
        return profitChangeType;
    }

    public void setProfitChangeType(Integer profitChangeType) {
        this.profitChangeType = profitChangeType;
    }

    public Integer getProfitType() {
        return profitType;
    }

    public void setProfitType(Integer profitType) {
        this.profitType = profitType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
