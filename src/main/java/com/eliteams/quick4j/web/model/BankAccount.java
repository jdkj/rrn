package com.eliteams.quick4j.web.model;

import com.eliteams.quick4j.core.util.ApplicationUtils;

/**
 * Created by Dsmart on 2016/11/5.
 */
public class BankAccount {

    private Long bankAccountId;

    private Long userId;

    private String bankName;

    private String cardNo;

    private String modifiedCardNo;

    private String createTime;

    private String updateTime;

    public Long getBankAccountId() {
        return bankAccountId;
    }

    public void setBankAccountId(Long bankAccountId) {
        this.bankAccountId = bankAccountId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getModifiedCardNo() {
        return ApplicationUtils.modifyBankCardNo(this.getCardNo());
    }
}
