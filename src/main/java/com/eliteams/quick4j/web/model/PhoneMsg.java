package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * Created by Dsmart on 2017/3/30.
 */
public class PhoneMsg implements Serializable {
    private static final long serialVersionUID = 2555832245983260943L;

    private Long msgId;

    private Long userId;

    /**
     * 手机号
     */
    private String phone;

    private Long agreementId;

    /**
     * 消息类型
     * 0：触发警戒线
     * 1:合约快到期
     * 2:操盘提交延时
     * 3:操盘确认延时
     * 4:账户余额不够支付利息
     * 5:合约达到最大操盘期限
     * 6:提现申请
     * 7:确认提现成功
     */
    private Integer msgType;

    private Integer msgLimit;

    private String createTime;

    private String updateTime;

    private String msgContent;


    public Long getMsgId() {
        return msgId;
    }

    public void setMsgId(Long msgId) {
        this.msgId = msgId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Long getAgreementId() {
        return agreementId;
    }

    public void setAgreementId(Long agreementId) {
        this.agreementId = agreementId;
    }

    public Integer getMsgType() {
        return msgType;
    }

    public void setMsgType(Integer msgType) {
        this.msgType = msgType;
    }

    public Integer getMsgLimit() {
        return msgLimit;
    }

    public void setMsgLimit(Integer msgLimit) {
        this.msgLimit = msgLimit;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getMsgContent() {
        return msgContent;
    }

    public void setMsgContent(String msgContent) {
        this.msgContent = msgContent;
    }

    @Override
    public String toString() {
        return "PhoneMsg{" +
                "msgId=" + msgId +
                ", userId=" + userId +
                ", phone='" + phone + '\'' +
                ", agreementId=" + agreementId +
                ", msgType=" + msgType +
                ", msgLimit=" + msgLimit +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", msgContent='" + msgContent + '\'' +
                '}';
    }

}
