package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2017/3/11.
 */
public class DistributionAccountMxRestrict extends PageRestrict {

    /**
     * 用户编号
     */
    private Long distributionUserId;

    /**
     * 开始日期
     */
    private String startTime;

    /**
     * 截止日期
     */
    private String endTime;


    public Long getDistributionUserId() {
        return distributionUserId;
    }

    public void setDistributionUserId(Long distributionUserId) {
        this.distributionUserId = distributionUserId;
    }


    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
