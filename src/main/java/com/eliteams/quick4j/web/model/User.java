package com.eliteams.quick4j.web.model;

import org.codehaus.jackson.annotate.JsonIgnore;

/**
 * 用户模型
 *
 * @author StarZou
 * @since 2014年7月5日 下午12:07:20
 **/
public class User {
    private Long userId;

    private String nickName;

    private String phone;

    private String userImage="";

    @JsonIgnore
    private String userPassword;

    private int userStatus;

    private String createTime;

    private String updateTime;

    /**
     * 股票操作手续费率
     */
    private Double operateRate;

    /**
     * 股票操作手续费
     */
    private Double operateFee;


    /**
     * 股票操作手续费收取方式
     */
    private int operateType;


    /**
     * 用户标识记录对象
     */
    private UserParamInfoTag userParamInfoTags;


    /**
     * 用户标识记录
     */
    @JsonIgnore
    private String userParamInfoTag;


    /**
     * 资金账户操作密码
     */
    @JsonIgnore
    private String accountPassword;


    public User() {
        this.setUserParamInfoTags(new UserParamInfoTag());
    }

    public User(String phone, String userPassword) {
        this.phone = phone;
        this.userPassword = userPassword;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Double getOperateRate() {
        return operateRate;
    }

    public void setOperateRate(Double operateRate) {
        this.operateRate = operateRate;
    }

    public Double getOperateFee() {
        return operateFee;
    }

    public void setOperateFee(Double operateFee) {
        this.operateFee = operateFee;
    }

    public int getOperateType() {
        return operateType;
    }

    public void setOperateType(int operateType) {
        this.operateType = operateType;
    }


    public String getAccountPassword() {
        return accountPassword;
    }

    public void setAccountPassword(String accountPassword) {
        this.accountPassword = accountPassword;
    }


    public UserParamInfoTag getUserParamInfoTags() {
        return userParamInfoTags;
    }

    public void setUserParamInfoTags(UserParamInfoTag userParamInfoTags) {
        this.userParamInfoTags = userParamInfoTags;
        this.userParamInfoTag = userParamInfoTags.toString();
    }

    public String getUserParamInfoTag() {
        return userParamInfoTag;
    }

    public void setUserParamInfoTag(String userParamInfoTag) {
        this.userParamInfoTag = userParamInfoTag;
        this.userParamInfoTags = new UserParamInfoTag(userParamInfoTag);
    }


}