package com.eliteams.quick4j.web.model;

import java.io.Serializable;

/**
 * Created by Dsmart on 2017/2/7.
 */
public class StockBase implements Serializable {

    private static final long serialVersionUID = 2002693275330880286L;

    private Long stockBaseId;

    /**
     * 股票所属市场
     */
    private String stockMarket = "sh";

    /**
     * 股票名称
     */
    private String stockName;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 股票名称拼音
     */
    private String stockNamePy;


    /**
     * 总市值
     */
    private Double totalValue;

    /**
     * 流通市值
     */
    private Double flowValue;

    /**
     * 市净率
     */
    private Double pb;

    /**
     * 市盈率
     */
    private Double pe;


    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 更新时间
     */
    private String updateTime;


    public Long getStockBaseId() {
        return stockBaseId;
    }

    public void setStockBaseId(Long stockBaseId) {
        this.stockBaseId = stockBaseId;
    }

    public String getStockMarket() {
        return stockMarket;
    }

    public void setStockMarket(String stockMarket) {
        this.stockMarket = stockMarket;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public String getStockNamePy() {
        return stockNamePy;
    }

    public void setStockNamePy(String stockNamePy) {
        this.stockNamePy = stockNamePy;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }


    public Double getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(Double totalValue) {
        this.totalValue = totalValue;
    }

    public Double getFlowValue() {
        return flowValue;
    }

    public void setFlowValue(Double flowValue) {
        this.flowValue = flowValue;
    }

    public Double getPb() {
        return pb;
    }

    public void setPb(Double pb) {
        this.pb = pb;
    }

    public Double getPe() {
        return pe;
    }

    public void setPe(Double pe) {
        this.pe = pe;
    }


    @Override
    public String toString() {
        return "StockBase{" +
                "stockBaseId=" + stockBaseId +
                ", stockMarket='" + stockMarket + '\'' +
                ", stockName='" + stockName + '\'' +
                ", stockCode='" + stockCode + '\'' +
                ", stockNamePy='" + stockNamePy + '\'' +
                ", totalValue=" + totalValue +
                ", flowValue=" + flowValue +
                ", pb=" + pb +
                ", pe=" + pe +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }


}
