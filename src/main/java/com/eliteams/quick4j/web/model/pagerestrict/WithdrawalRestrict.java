package com.eliteams.quick4j.web.model.pagerestrict;

import com.eliteams.quick4j.core.entity.PageRestrict;

/**
 * Created by Dsmart on 2017/3/6.
 */
public class WithdrawalRestrict extends PageRestrict {

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
