package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.service.OperateLogService;
import com.eliteams.quick4j.web.service.PhoneMsgService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * 自动检测操盘提交延时报警
 * 当用户提交操盘申请后，如若一分钟内还未报单，则向管理员发送短信警报
 * <p>
 * Created by Dsmart on 2017/6/22.
 */
@Component
public class ScheduleCheckStockOperateApplyDelayWarning {

    @Resource
    private OperateLogService operateLogService;

    @Resource
    private PhoneMsgService phoneMsgService;


    @PostConstruct
    public void init() {
        //每隔一分钟检测一次
//        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleCheckStockOperateApplyDelayWarningTask(), SystemGlobalConfigure.ONE_MINUTE_MILLS, SystemGlobalConfigure.ONE_MINUTE_MILLS, TimeUnit.MILLISECONDS);//每隔一天执行一次

    }


    class ScheduleCheckStockOperateApplyDelayWarningTask implements Runnable {

        @Override
        public void run() {

            //查询出超过一定时间是否还有未报单操盘请求
            Long maxDelaySeconds = 60l;

            if (StockMarketingUtils.isDuringStockEntrustTime()) {

                if (operateLogService.isExistsOverTimeStockOperateApply(maxDelaySeconds)) {
                    //发送短信
//                    SMSUtils.post("18659032066","自动报单挂了，快去看看什么情况!");
                    phoneMsgService.addStockOperateApplyDelayWarningMsg("15559173999", "自动报单挂啦，快去看看什么情况!");
                    phoneMsgService.addStockOperateApplyDelayWarningMsg("18659032066", "自动报单挂啦，快去看看什么情况!");
                }

            }


        }
    }


}
