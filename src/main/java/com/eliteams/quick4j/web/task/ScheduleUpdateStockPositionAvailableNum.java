package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.web.config.SystemGlobalConfigure;
import com.eliteams.quick4j.web.model.StockPositionMx;
import com.eliteams.quick4j.web.service.StockPositionMxService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by Dsmart on 2017/3/30.
 */
@Component
public class ScheduleUpdateStockPositionAvailableNum {

    @Resource
    private StockPositionMxService stockPositionMxService;

    @PostConstruct
    public void init() {
        new ScheduleUpdateStockPositionAvailableNumTask().run();
        Long delay = ApplicationUtils.getDelayFroScheduleTask(SystemGlobalConfigure.STOCK_POSITION_AVAILABLE_NUM_UPDATE_TIME);
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleUpdateStockPositionAvailableNumTask(), delay, SystemGlobalConfigure.ONE_DAY_MILLS, TimeUnit.MILLISECONDS);//每隔一天执行一次
    }


    class ScheduleUpdateStockPositionAvailableNumTask implements Runnable {
        @Override
        public void run() {
            //获取未解除锁定的股票列表
            List<StockPositionMx> stockPositionMxList = stockPositionMxService.listFreshStocks();
            if (stockPositionMxList != null && !stockPositionMxList.isEmpty()) {
                for (StockPositionMx stockPositionMx : stockPositionMxList) {
                    stockPositionMxService.updateStockPositionMxByUnlock(stockPositionMx);//执行解锁操作
                }
            }
        }
    }


}
