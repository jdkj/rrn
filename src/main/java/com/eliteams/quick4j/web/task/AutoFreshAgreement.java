package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.service.AgreementService;
import com.eliteams.quick4j.web.service.StockPositionService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 自动刷新合约数据
 * Created by Dsmart on 2016/11/4.
 */
@Component("AutoFreshAgreement")
public class AutoFreshAgreement {

    @Resource
    private AgreementService agreementService;

    @Resource
    private StockPositionService stockPositionService;

    private Timer timer = new Timer();

    private Timer getInstance() {
        if (timer == null) {
            timer = new Timer();
        }
        return timer;
    }

    @PostConstruct
    public void init() {
        this.timer = this.getInstance();
//       timer.schedule(new AutoFreshAgreementTimerTask(), 10 * 1000, 10 * 1000l);//每隔1秒执行一次
    }

    class AutoFreshAgreementTimerTask extends TimerTask {

        public void run() {

            List<Agreement> list = agreementService.listAll();
            for (Agreement agreement : list) {
                agreementService.refreshAgreementInfo(agreement.getAgreementId());
            }
        }
    }

}
