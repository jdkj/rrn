package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalFixedThreadPool;
import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.web.config.SystemGlobalConfigure;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.service.AgreementService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 定时结算待结算合约
 * Created by Dsmart on 2016/11/24.
 */
@Component
public class ScheduleSettlementAgreement {

    @Resource
    private AgreementService agreementService;

    @PostConstruct
    public void init(){
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleSettlementAgreementTask(),1,20, TimeUnit.MINUTES);//每隔20分钟执行一次
    }


    class ScheduleSettlementAgreementTask implements Runnable{
        @Override
        public void run() {

            if(MarketingContainer.isMarketingOnTime(20* SystemGlobalConfigure.ONE_MINUTE_MILLS)){
                List<Agreement> agreementList = agreementService.listCaculateAgreements();
                if(agreementList!=null&&!agreementList.isEmpty()){
                    for(Agreement agreement:agreementList){
                        final Long agreementId = agreement.getAgreementId();
                        final Long userId = agreement.getUserId();
                        GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                agreementService.settlementAgreement(agreementId,userId);
                            }
                        });

                    }
                }
            }
        }
    }




}
