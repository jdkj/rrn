package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalFixedThreadPool;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.container.StockMarketContainer;
import com.eliteams.quick4j.web.model.StockMarket;
import com.eliteams.quick4j.web.service.StockMarketService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Iterator;
import java.util.Map;

/**
 * 定期将股票信息写入本地
 * Created by Dsmart on 2016/11/30.
 */
@Component
public class ScheduleWriteStockMarketInfoLocal {

    @Resource
    private StockMarketService stockMarketService;

    @PostConstruct
    public void init() {
//        Long delay = ApplicationUtils.getDelayFroScheduleTask(SystemGlobalConfigure.WRITE_STOCK_MARKET_INFO_LOCAL_TIME);
//        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleWriteStockMarketInfoLocalTask(),delay, SystemGlobalConfigure.ONE_DAY_MILLS, TimeUnit.MILLISECONDS);
//        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleWriteStockMarketInfoLocalTask(), 60000l, 2 * SystemGlobalConfigure.ONE_MINUTE_MILLS, TimeUnit.MILLISECONDS);

    }


    class ScheduleWriteStockMarketInfoLocalTask implements Runnable {
        @Override
        public void run() {
            if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringStockRefreshTime()) {
                Iterator iterator = StockMarketContainer.getInstance().entrySet().iterator();
                while (iterator.hasNext()) {
                    Map.Entry<String, StockMarket> map = (Map.Entry) iterator.next();
                    final StockMarket stockMarket = map.getValue();
                    GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                        @Override
                        public void run() {
                            stockMarketService.updateStockInfo(stockMarket);
                        }
                    });
                }
            }

        }
    }
}
