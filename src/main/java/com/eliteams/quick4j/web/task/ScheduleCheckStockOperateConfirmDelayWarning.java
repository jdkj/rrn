package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.service.OperateLogService;
import com.eliteams.quick4j.web.service.PhoneMsgService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Created by Dsmart on 2017/6/22.
 */
@Component
public class ScheduleCheckStockOperateConfirmDelayWarning {


    @Resource
    private OperateLogService operateLogService;

    @Resource
    private PhoneMsgService phoneMsgService;


    @PostConstruct
    public void init() {
        //每隔一分钟检测一次
//        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleCheckStockOperateConfirmDelayWarningTask(), SystemGlobalConfigure.ONE_MINUTE_MILLS, SystemGlobalConfigure.ONE_MINUTE_MILLS, TimeUnit.MILLISECONDS);//每隔一天执行一次

    }


    class ScheduleCheckStockOperateConfirmDelayWarningTask implements Runnable {

        @Override
        public void run() {

            System.err.println("回单信息确认");

            //查询出超过一定时间是否还有未报单操盘请求
            Long maxDelaySeconds = 150l;

            if (StockMarketingUtils.isDuringStockEntrustTime()) {

                if (operateLogService.isExistsOverTimeStockConfirm(maxDelaySeconds)) {
                    //发送短信
//                    SMSUtils.post("18659032066","回单信息确认挂掉了，快去看看什么情况!");
                    phoneMsgService.addStockConfirmDelayWarningMsg("15559173999", "回单信息确认挂掉了，快去看看什么情况!");
                    phoneMsgService.addStockConfirmDelayWarningMsg("18659032066", "回单信息确认挂掉了，快去看看什么情况!");
                }

            }


        }
    }

}
