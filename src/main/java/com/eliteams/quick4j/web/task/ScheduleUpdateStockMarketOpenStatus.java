package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.config.SystemGlobalConfigure;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.container.SystemParamContainer;
import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.SystemParam;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 *  周期性更新当天是否为开市日
 * Created by Dsmart on 2016/11/30.
 */
@Component
public class ScheduleUpdateStockMarketOpenStatus {

    @PostConstruct
    public void init(){
        Long delay = ApplicationUtils.getDelayFroScheduleTask(SystemGlobalConfigure.STOCK_MARKET_OPEN_STATUS_UPDATE_TIME);
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleUpdateStockMarketOpenStatusTask(),delay, SystemGlobalConfigure.ONE_DAY_MILLS, TimeUnit.MILLISECONDS);
    }



    class ScheduleUpdateStockMarketOpenStatusTask implements Runnable{
        @Override
        public void run() {
            SystemParam systemParam = SystemParamContainer.get(SystemParamEnums.STOCK_MARKET_ON.getParam());
            if(systemParam!=null){
                if(StockMarketingUtils.isCurrentDayFestivalDay()){
                    systemParam.setParamValue("0");
                }else{
                    systemParam.setParamValue("1");
                }
                SystemParamContainer.put(systemParam,true);
            }
        }
    }

}
