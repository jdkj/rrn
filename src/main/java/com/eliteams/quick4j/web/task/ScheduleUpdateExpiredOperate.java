package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.web.config.SystemGlobalConfigure;
import com.eliteams.quick4j.web.model.StockOperate;
import com.eliteams.quick4j.web.service.StockOperateService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 定时更新过期交易委托
 * Created by Dsmart on 2016/11/28.
 */
@Component
public class ScheduleUpdateExpiredOperate {

    @Resource
    private StockOperateService stockOperateService;

    @PostConstruct
    public void init() {
        new ScheduleUpdateExpiredOperateTask().run();//执行一次
        Long delay = ApplicationUtils.getDelayFroScheduleTask(SystemGlobalConfigure.EXPIRED_STOCK_OPERATE_COMMISSION_SCHEDULE_UPDATE_TIME);
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleUpdateExpiredOperateTask(), delay, SystemGlobalConfigure.ONE_DAY_MILLS, TimeUnit.MILLISECONDS);//每隔一天执行一次
    }


    class ScheduleUpdateExpiredOperateTask implements Runnable {
        @Override
        public void run() {
            //所有过期的未完结交易委托
            List<StockOperate> stockOperateList = stockOperateService.listAllExpiredUnFinished();
            if(stockOperateList!=null&&!stockOperateList.isEmpty()){
                for(StockOperate stockOperate:stockOperateList){
                    stockOperateService.updateExpiredOperate(stockOperate);//执行过期设置操作
                }
            }
        }
    }


}
