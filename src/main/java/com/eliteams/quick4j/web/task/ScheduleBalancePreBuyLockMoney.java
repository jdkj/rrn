package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.config.SystemGlobalConfigure;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.service.AgreementService;
import com.eliteams.quick4j.web.service.StockOperateService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 这个定时线程是因为在合约交易过程中会出现预购金额与实际金额不一致  导致预购金额最后不能清零
 * <p>
 * 系统定时自动给予可用余额与预购金额的结账持平
 * <p>
 * <p>
 * 处理逻辑
 * 当前合约下没有未完结的操盘申请 progressStatus <2
 * 预购锁定资金不为0的时候
 * 合约可用余额 = 合约可用余额+预购锁定资金
 * <p>
 * <p>
 * Created by Dsmart on 2017/6/19.
 */
@Component
public class ScheduleBalancePreBuyLockMoney {

    @Resource
    private AgreementService agreementService;

    @Resource
    private StockOperateService stockOperateService;


    @PostConstruct
    public void init() {

        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleBalancePreBuyLockMoneyTask(), 60000l, SystemGlobalConfigure.ONE_MINUTE_MILLS, TimeUnit.MILLISECONDS);//每隔一分钟执行一次
    }


    class ScheduleBalancePreBuyLockMoneyTask implements Runnable {

        @Override
        public void run() {

            //执行时间段为9:00 -15:35
            //15:30处理未完成交易
            if (StockMarketingUtils.isDuringTimeArea("09:00:00", "15:35:00")) {
                //所有合约
                List<Agreement> agreementList = agreementService.listAll();
                //遍历所有合约
                for (Agreement agreement : agreementList) {
                    //操盘预购锁定资金不为0
                    if (agreement.getPrebuyLockMoney().compareTo(0d) != 0) {
                        //没有未完成的委托
                        if (stockOperateService.isAllOperateFinishedByAgreementId(agreement.getAgreementId())) {
                            agreement.setAvailableCredit(agreement.getAvailableCredit() + agreement.getPrebuyLockMoney());
                            agreement.setPrebuyLockMoney(0d);
                            agreementService.updateAgreementByOperateStock(agreement);
                        }
                    }

                    //如果预购锁定资金小于0  强制结账清平
                    if (agreement.getPrebuyLockMoney().compareTo(0d) < 0) {
                        agreement.setAvailableCredit(agreement.getAvailableCredit() + agreement.getPrebuyLockMoney());
                        agreement.setPrebuyLockMoney(0d);
                        agreementService.updateAgreementByOperateStock(agreement);
                    }
                }
            }
        }
    }


}
