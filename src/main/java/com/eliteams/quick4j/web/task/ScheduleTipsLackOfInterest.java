package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.web.config.SystemGlobalConfigure;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.model.UserAccount;
import com.eliteams.quick4j.web.service.AgreementService;
import com.eliteams.quick4j.web.service.PhoneMsgService;
import com.eliteams.quick4j.web.service.UserAccountService;
import com.eliteams.quick4j.web.service.UserService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 利息缺少提示
 * Created by Dsmart on 2017/7/10.
 */
@Component
public class ScheduleTipsLackOfInterest {

    @Resource
    private AgreementService agreementService;

    @Resource
    private UserService userService;

    @Resource
    private UserAccountService userAccountService;

    @Resource
    private PhoneMsgService phoneMsgService;


    /**
     * 构建的时候初始化
     */
    @PostConstruct
    public void init() {

        new ScheduleTipsLackOfInterestTask().run();
        Long delay = ApplicationUtils.getDelayFroScheduleTask(SystemGlobalConfigure.EXPIRED_STOCK_OPERATE_COMMISSION_SCHEDULE_UPDATE_TIME);//每日收盘后 进行检测
        // Long delay = ApplicationUtils.getDelayFroScheduleTask("15:30:00");//每日收盘后 进行检测
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleTipsLackOfInterestTask(), delay, SystemGlobalConfigure.ONE_DAY_MILLS, TimeUnit.MILLISECONDS);
    }


    /**
     *
     */
    class ScheduleTipsLackOfInterestTask implements Runnable {


        @Override
        public void run() {

            //获取用户快要到期的合约
            //计算用户这些合约总共需要支付的利息
            //查看用户可用余额是否能够足额支付
            //相应进行短息通知

            Map<Long, List<Agreement>> map = agreementService.listAlmostOverdueAgreements();

            if (map != null && map.size() > 0) {
                for (Map.Entry<Long, List<Agreement>> entry : map.entrySet()) {
                    List<Agreement> agreementList = entry.getValue();
                    Long userId = entry.getKey();

                    User user = userService.selectById(userId);
                    UserAccount userAccount = userAccountService.getUserAccountByUserId(userId);

                    //用户当前可用余额
                    Double availableMoney = userAccount.getMoney();

                    Double totalInterest = 0.0d;

                    for (Agreement agreement : agreementList) {

                        totalInterest += agreement.getManagementCostFee();

                    }

                    //余额够支付
                    if (availableMoney.compareTo(totalInterest) >= 0) {
                        //告知用户下一交易日如若不自动结算合约  合约将自动续期？

                    } else {//余额不够支付
                        //告知用户如果下一交易日不补齐管理费用,系统将在9.05自动卖出欠费合约下的股票 并进行结算
                        //欠费合约  当日是禁止买入的
                        phoneMsgService.addLackOfInterestMsg(userId, user.getPhone(), "尊敬的用户您好，请您于下一交易日前,向账户中充值至少" + (totalInterest - availableMoney) + "元,以确保您的合约能自动续期成功!");

                    }


                }


            }


        }
    }


}
