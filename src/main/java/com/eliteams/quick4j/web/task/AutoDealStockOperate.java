package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.web.model.StockOperate;
import com.eliteams.quick4j.web.service.StockOperateService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 自动成交股票交易
 *
 * Created by Dsmart on 2016/11/4.
 */
@Component("AutoDealStockOperate ")
public class AutoDealStockOperate {

    @Resource
    private StockOperateService stockOperateService;

    private Timer timer = new Timer();

    private Timer getInstance(){
        if(timer==null){
            timer = new Timer();
        }
        return timer;
    }

    @PostConstruct
    public void init(){
        this.timer = this.getInstance();
//        timer.schedule(new AutoDealStockOperateTimerTask(),0,30*1000l);//每隔30秒执行一次
    }


    class AutoDealStockOperateTimerTask extends TimerTask {

        public  void run(){

            List<StockOperate> list = stockOperateService.listFresh();
            for(StockOperate stockOperate:list){
                stockOperate.setTransactionNum(stockOperate.getCommissionNum());
                stockOperate.setTransactionPrice(stockOperate.getCommissionPrice());
                stockOperate.setOperateStatus(1);
                switch(stockOperate.getOperateType()){
                    case 0:
                        stockOperateService.sellSuccess(stockOperate);
                        break;
                    case 1:
                        stockOperateService.buySuccess(stockOperate);
                        break;
                }
            }
        }
    }
}
