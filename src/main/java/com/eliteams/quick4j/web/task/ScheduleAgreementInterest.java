package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.web.config.SystemGlobalConfigure;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.container.SystemParamContainer;
import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.model.SystemParam;
import com.eliteams.quick4j.web.service.AgreementService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 定时收取合约利息
 * Created by Dsmart on 2016/11/29.
 */
@Component
public class ScheduleAgreementInterest {

    @Resource
    private AgreementService agreementService;


    @PostConstruct
    public void init() {
        Long delay = ApplicationUtils.getDelayFroScheduleTask(SystemGlobalConfigure.AGREEMENT_INTEREST_SETTLEMENT_TIME);
        new ScheduleAgreementInterestTask().run();
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleAgreementInterestTask(), delay, SystemGlobalConfigure.ONE_DAY_MILLS, TimeUnit.MILLISECONDS);//每隔一天执行一次
    }


    class ScheduleAgreementInterestTask implements Runnable {
        @Override
        public void run() {
            //是否需要更新
            Boolean isNeedUpdate = false;

            SystemParam systemParam = SystemParamContainer.get(SystemParamEnums.AGREEMENT_INTEREST_UPDATE_TIME.getParam());

            //今天是开市日
            if (MarketingContainer.isMarketingOn()) {

                if (systemParam != null) {
                    //最后更新时间
                    String latestUpdateTime = systemParam.getParamValue();

                    Long updateTimeMills = ApplicationUtils.getMillsFromDate(ApplicationUtils.formatDate(latestUpdateTime, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd"), "yyyy-MM-dd");
                    Long todayMills = ApplicationUtils.getMillsFromDate(ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(System.currentTimeMillis()), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy-MM-dd"), "yyyy-MM-dd");

                    //最后更新时间是今天之前
                    if (updateTimeMills.compareTo(todayMills) < 0) {
                        isNeedUpdate = true;
                    }


                }

            }

            //需要更新
            if (isNeedUpdate) {
                //当前正常合约
                List<Agreement> agreementList = agreementService.listAll();

                if (agreementList != null && !agreementList.isEmpty()) {

                    for (Agreement agreement : agreementList) {

                        //是否需要缴纳利息
                        Boolean isNeedPayInterest = false;

                        Long deadLineMills = ApplicationUtils.getMillsFromDate(ApplicationUtils.formatDate(agreement.getOperateDeadline(), "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd"), "yyyy-MM-dd");


                        Long todayMills = ApplicationUtils.getMillsFromDate(ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(System.currentTimeMillis()), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy-MM-dd"), "yyyy-MM-dd");

                        //需要缴纳利息
                        if (todayMills.compareTo(deadLineMills) > 0) {
                            isNeedPayInterest = true;
                        }

                        //需要支付利息
                        if (isNeedPayInterest) {
                            //支付合约利息
                            agreementService.agreementInterestPayment(agreement.getAgreementId());
                        } else {//不需要支付利息
                            switch (agreement.getSignAgreementType()) {
                                //日结
                                case 0:

                                    break;
                                //月结
                                case 1://更新持续操盘天数
                                default:
                                    agreementService.updateAgreementContinueOperateDay(agreement.getAgreementId());
                                    break;
                            }
                        }
                    }
                }
            }
            //更新利息结算时间
            systemParam.setParamValue(ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(System.currentTimeMillis()), "yyyy-MM-dd HH:mm:ss:SSS", "yyyy-MM-dd HH:mm:ss"));
            SystemParamContainer.put(systemParam,true);

        }
    }

}
