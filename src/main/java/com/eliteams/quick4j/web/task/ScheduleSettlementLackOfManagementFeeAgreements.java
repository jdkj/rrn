package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.service.AgreementService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 定时结算由于管理费未缴而需要结算的合约
 * Created by Dsmart on 2017/7/13.
 */
@Component
public class ScheduleSettlementLackOfManagementFeeAgreements {

    @Resource
    private AgreementService agreementService;


    @PostConstruct
    public void init() {

        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleSettlementLackOfManagementFeeAgreementsTask(), 0, 2, TimeUnit.MINUTES);

    }


    /**
     * 合约缺少管理费而需要进行结算
     */
    class ScheduleSettlementLackOfManagementFeeAgreementsTask implements Runnable {


        @Override
        public void run() {

            //开盘日 操盘阶段
            //获取所有因缺少管理费待结算合约
            //执行卖出与结算操作
            if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringTimeArea("09:30:00", "15:00:00")) {
                List<Agreement> lackAgreements = agreementService.listLackOfManagementFeeAgreements();
                for (Agreement agreement : lackAgreements) {

                    agreementService.forceSettlementAgreement(agreement.getAgreementId());

                }

            }

        }
    }


}
