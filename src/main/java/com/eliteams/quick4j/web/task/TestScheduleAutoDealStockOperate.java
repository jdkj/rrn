package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalFixedThreadPool;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.model.StockMarket;
import com.eliteams.quick4j.web.model.StockOperate;
import com.eliteams.quick4j.web.service.StockMarketService;
import com.eliteams.quick4j.web.service.StockOperateService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;

/**
 * 测试根据当前股价更新交易委托
 * Created by Dsmart on 2016/11/24.
 */
@Component
public class TestScheduleAutoDealStockOperate {

    @Resource
    private StockOperateService stockOperateService;

    @Resource
    private StockMarketService stockMarketService;


    @PostConstruct
    public void init() {
//        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new TestScheduleAutoDealStockOperateTask(), 10l, 5l, TimeUnit.SECONDS);//每30s检测一次
    }




    class TestScheduleAutoDealStockOperateTask implements Runnable{
        @Override
        public void run() {

            //开市日 且在开市期内
            if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringStockRefreshTime()) {
                //当前所有未完结委托交易
                List<StockOperate> allUnFinishedList = stockOperateService.listAllUnFinishedGroupByStockCode();
                if(allUnFinishedList!=null&&!allUnFinishedList.isEmpty()){
                    for(StockOperate groupStockOperate:allUnFinishedList){
                        //某股票下所有未完结股票信息
                        List<StockOperate> unFinishedList = stockOperateService.listAllUnFinishedByStockCode(groupStockOperate.getStockCode());
                        if(unFinishedList!=null&&!unFinishedList.isEmpty()){
                            //当前该股票信息
                            StockMarket stockMarket = stockMarketService.stockInfo(groupStockOperate.getStockCode());// StockMarketContainer.get(groupStockOperate.getStockCode());
                            for(StockOperate stockOperate:unFinishedList){
                                Double currentPrice = stockMarket.getNowPrice();//股票当前价格
                                Double commissionPrice = stockOperate.getCommissionPrice();//委托价格
                                int operateStatus = stockOperate.getOperateStatus();//委托状态


                                /**
                                 * 委托交易处理规则：一切以用户利益最大化
                                 *  卖高:当前市场价格不低于用户委托价格的时候，按照当前市场价格成交
                                 *  买低：当前市场价格不高于用户委托价格的时候，按照当前市场价格成交
                                 */
                                switch(stockOperate.getOperateType()){
                                    //卖高
                                    case 0://卖
                                        if(currentPrice.compareTo(commissionPrice)>=0){//交易成功
                                            stockOperate.setTransactionPrice(currentPrice);
                                            stockOperate.setTransactionNum(stockOperate.getCommissionNum());
                                            stockOperate.setOperateStatus(1);
                                            final StockOperate finalStockOperate =stockOperate;
//                                            GlobalFixedThreadPool.getInstance().execute(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    stockOperateService.sellSuccess(finalStockOperate);
//                                                }
//                                            });

                                            stockOperateService.sellSuccess(finalStockOperate);

                                        }else{//挂单
                                            operateStatus = operateStatus>0?operateStatus:3;
                                            stockOperate.setOperateStatus(operateStatus);
                                            final StockOperate finalStockOperate =stockOperate;
                                            GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                    stockOperateService.updateStockOperate(finalStockOperate);
                                                }
                                            });
                                        }

                                        break;
                                    //买低
                                    case 1://买
                                        if(currentPrice.compareTo(commissionPrice)<=0){//交易成功
                                            stockOperate.setTransactionPrice(currentPrice);
                                            stockOperate.setTransactionNum(stockOperate.getCommissionNum());
                                            stockOperate.setOperateStatus(1);
                                            final StockOperate finalStockOperate =stockOperate;
//                                            GlobalFixedThreadPool.getInstance().execute(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    stockOperateService.buySuccess(finalStockOperate);
//                                                }
//                                            });
                                            stockOperateService.buySuccess(finalStockOperate);
                                        }else{//挂单
                                            operateStatus = operateStatus>0?operateStatus:3;
                                            stockOperate.setOperateStatus(operateStatus);
                                            final StockOperate finalStockOperate =stockOperate;
                                            GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                                                @Override
                                                public void run() {
                                                    stockOperateService.updateStockOperate(finalStockOperate);
                                                }
                                            });
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
