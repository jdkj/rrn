package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalFixedThreadPool;
import com.eliteams.quick4j.core.util.StockDataLoadUtils;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.container.StockMarketContainer;
import com.eliteams.quick4j.web.model.StockMarket;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.Map;

/**
 * 周期新刷新股票信息
 * Created by Dsmart on 2016/11/23.
 */
@Component
public class ScheduleRefreshStockMarketInfo {

    @PostConstruct
    public void init() {
//        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleRefreshStockMarketInfoTimerTask(), 20l, 20l, TimeUnit.SECONDS);//秒执行一次
    }

    class ScheduleRefreshStockMarketInfoTimerTask implements Runnable {

        @Override
        public void run() {


            //是否开市
                //判断当前时间段是否开市
            if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringStockRefreshTime()) {

                    Iterator iter = StockMarketContainer.getInstance().entrySet().iterator();
                    int count = 0;
                    StringBuffer stringBuffer = new StringBuffer();
                    while (iter.hasNext()) {
                        Map.Entry<String, StockMarket> map = (Map.Entry) iter.next();
                        StockMarket stockMarket = map.getValue();
                        stringBuffer.append(stockMarket.getMarket() + stockMarket.getCode());
                        stringBuffer.append(",");
                        count++;
                        if (count == 200) {
                            final String codes = stringBuffer.deleteCharAt(stringBuffer.length()).toString();
                            //去查询
                            GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                                @Override
                                public void run() {
                                    //植入缓存
                                    StockMarketContainer.putList(StockDataLoadUtils.loadStocks(codes));
                                }
                            });
                            count = 1;
                            stringBuffer = new StringBuffer();
                        }
                    }

                    if (count > 1) {
                        final String codes = stringBuffer.deleteCharAt(stringBuffer.length() - 1).toString();
                        //去查询
                        GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                //植入缓存
                                StockMarketContainer.putList(StockDataLoadUtils.loadStocks(codes));
                            }
                        });
                    }

                }
        }
    }
}
