package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.web.model.StockOperate;
import com.eliteams.quick4j.web.model.StockPosition;
import com.eliteams.quick4j.web.service.StockPositionService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * 自动刷新股票价值
 * Created by Dsmart on 2016/11/4.
 */
@Component("AutoFreshStockPosition")
public class AutoFreshStockPosition {

    @Resource
    private StockPositionService stockPositionService;

    private Timer timer = new Timer();

    private Timer getInstance(){
        if(timer==null){
            timer = new Timer();
        }
        return timer;
    }

    @PostConstruct
    public void init(){
        this.timer = this.getInstance();
//        timer.schedule(new AutoFreshStockPositionTimerTask(),0,10*1000l);//每隔1秒执行一次
    }


    class AutoFreshStockPositionTimerTask extends TimerTask {

        public  void run(){

            List<StockPosition>  list = stockPositionService.listAll();
            Double rate = 0.01;
            for(StockPosition stockPosition:list){
                if(stockPosition.getCurrentPrice()==0){
                    stockPosition.setCurrentPrice(stockPosition.getBuyPrice());
                }
                Double maxPrice =stockPosition.getBuyPrice()*1.1;
                Double minPrice = stockPosition.getBuyPrice()*0.9;
                int rand = new Random().nextInt(66);
                int seed =rand %2;


                if(seed>0){
                    Double cp = stockPosition.getCurrentPrice()+rate;
                    if(cp.compareTo(maxPrice)>0){
                        cp = cp-rate*2;
                    }
                    stockPosition.setCurrentPrice(cp);

                }else{
                    Double cp = stockPosition.getCurrentPrice()-rate;
                    if(cp.compareTo(minPrice)<0){
                        cp = cp+rate*2;
                    }
                    stockPosition.setCurrentPrice(cp);
                }
                stockPosition.setCurrentMarketValue(stockPosition.getStockNum()*stockPosition.getCurrentPrice());
                stockPositionService.updateStockPositionAvailableNum(stockPosition);
            }
        }
    }

}
