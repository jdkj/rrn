package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalFixedThreadPool;
import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.service.AgreementService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 定时更新合约数据
 * Created by Dsmart on 2016/11/24.
 */
@Component
public class ScheduleRefreshAgreement {

    @Resource
    private AgreementService agreementService;

    private Boolean forceProgress = true;


    @PostConstruct
    public void init() {
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleRefreshAgreementTask(), 1, 1, TimeUnit.MINUTES);//每隔3分钟执行一次
    }


    class ScheduleRefreshAgreementTask implements Runnable {
        @Override
        public void run() {
            if (forceProgress || MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringAgreementRefreshTime()) {
                List<Agreement> agreementList = agreementService.listAll();
                if (agreementList != null && !agreementList.isEmpty()) {
                    for (Agreement agreement : agreementList) {
                        final Long agreementId = agreement.getAgreementId();
                        GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                            @Override
                            public void run() {
                                agreementService.refreshAgreementInfo(agreementId);//更新合约价值
                            }
                        });
                    }
                }
                forceProgress = false;
            }
        }
    }

}
