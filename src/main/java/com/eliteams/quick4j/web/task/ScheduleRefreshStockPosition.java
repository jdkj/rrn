package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalFixedThreadPool;
import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.model.StockMarket;
import com.eliteams.quick4j.web.model.StockPosition;
import com.eliteams.quick4j.web.service.StockMarketService;
import com.eliteams.quick4j.web.service.StockPositionService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 根据当前股票价格更新用户持仓股票价值
 * Created by Dsmart on 2016/11/24.
 */
@Component
public class ScheduleRefreshStockPosition {

    @Resource
    private StockPositionService stockPositionService;

    @Resource
    private StockMarketService stockMarketService;

    private Boolean forceProgress = true;

    @PostConstruct
    public void init() {
        //每隔20秒执行一次
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleRefreshStockPositionTask(), 10l, 20l, TimeUnit.SECONDS);
    }


    class ScheduleRefreshStockPositionTask implements Runnable {
        @Override
        public void run() {
            //当前是开市日
            //是开市时间段内
            if (forceProgress || MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringStockRefreshTime()) {
                List<StockPosition> groupStockPositionList = stockPositionService.listAllGroupByStockCode();
                if (groupStockPositionList != null && !groupStockPositionList.isEmpty()) {
                    for (StockPosition groupStockPosition : groupStockPositionList) {
                        String stockCode = groupStockPosition.getStockCode();
                        List<StockPosition> stockPositionList = stockPositionService.listAllByStockCode(stockCode);
                        if (stockPositionList != null && !stockPositionList.isEmpty()) {
                            //当前股票数据信息
                            StockMarket stockMarket = stockMarketService.stockInfo(stockCode);
                            //股票当前价格大于0
                            if (stockMarket != null && stockMarket.getOpenPrice().compareTo(0d) > 0) {
                                for (StockPosition stockPosition : stockPositionList) {
                                    stockPosition.setCurrentPrice(stockMarket.getNowPrice());
                                    stockPosition.setCurrentMarketValue(stockPosition.getStockNum() * stockPosition.getCurrentPrice());

                                    final StockPosition finalStockPosition = stockPosition;
                                    GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                                        @Override
                                        public void run() {
                                            stockPositionService.updateStockPositionAvailableNum(finalStockPosition);
                                        }
                                    });
                                }
                            }
                        }
                    }
                }
                forceProgress = false;
            }

        }
    }


}
