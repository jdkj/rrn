package com.eliteams.quick4j.web.task;

import com.eliteams.quick4j.core.threadpool.GlobalScheduleThreadPool;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.service.AgreementService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 结算超过最大操盘周期的合约
 * Created by Dsmart on 2017/7/13.
 */
@Component
public class ScheduleSettlementOverMaxPeriodAgreements {

    @Resource
    private AgreementService agreementService;


    @PostConstruct
    public void init() {
        GlobalScheduleThreadPool.getInstance().scheduleAtFixedRate(new ScheduleSettlementOverMaxPeriodAgreementsTask(), 0, 1, TimeUnit.MINUTES);//每隔一分钟执行一次

    }


    class ScheduleSettlementOverMaxPeriodAgreementsTask implements Runnable {
        @Override
        public void run() {
            if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringTimeArea("14:30:00", "15:00:00")) {
                List<Agreement> agreementList = agreementService.listOverMaxPeriodAgreements();
                for (Agreement agreement : agreementList) {
                    agreementService.forceSettlementAgreement(agreement.getAgreementId());
                }
            }
        }
    }


}
