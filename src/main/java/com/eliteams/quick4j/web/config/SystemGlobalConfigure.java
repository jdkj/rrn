package com.eliteams.quick4j.web.config;

/**
 * 系统全局配置参数
 * Created by Dsmart on 2016/11/28.
 */
public class SystemGlobalConfigure {

    /**
     * 一天的毫秒数
     */
    public static final Long ONE_DAY_MILLS =86400000l;

    /**
     * 一小时的毫秒数
     */
    public static final Long ONE_HOUR_MILLS = 3600000l;

    /**
     * 一分钟的毫秒数
     */
    public static final Long ONE_MINUTE_MILLS = 60000l;


    /**
     *过期股票交易委托更新时间 下午15:30点
     */
    public static final String EXPIRED_STOCK_OPERATE_COMMISSION_SCHEDULE_UPDATE_TIME = "15:30:00";


    /**
     * 持仓股票可用股数更新时间  凌晨  1:30
     */
    public static final String STOCK_POSITION_AVAILABLE_NUM_UPDATE_TIME = "01:30:00";


    /**
     * 合约利息结算时间 早上8:50
     */
    public static final String AGREEMENT_INTEREST_SETTLEMENT_TIME="08:50:00";


    /**
     * 股市是否开市更新时间
     */
    public static final String STOCK_MARKET_OPEN_STATUS_UPDATE_TIME = "01:00:00";


    /**
     * 股票信息固态化时间 18:00:00
     */
    public static final String  WRITE_STOCK_MARKET_INFO_LOCAL_TIME = "18:00:00";




}
