package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.StockPosition;
import com.eliteams.quick4j.web.model.pagerestrict.StockPositionRestrict;

import java.util.List;


/**
 * 用户Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface StockPositionMapper extends GenericDao<StockPosition, Long> {


    int insert(StockPosition stockPosition);

    /**
     * 分页获取持仓列表数据
     * @param stockPositionRestrict
     * @param page
     * @return
     */
    List<StockPosition> listByRestrict(StockPositionRestrict stockPositionRestrict, Page<StockPosition> page);


    /**
     * 根据股票编码获取持仓列表数据
     * @param stockCode
     * @return
     */
    List<StockPosition> listByStockCode(String stockCode);


    /**
     * 根据用户编号与股票编码获取用户持仓数据
     * @param stockPosition
     * @return
     */
    StockPosition getStockPositionByAgreementIdAndUserIdAndStockCode(StockPosition stockPosition);


    /**
     * 根据持仓编号更新持仓数据信息
     * @param stockPosition
     * @return
     */
    int updateByPositionId(StockPosition stockPosition);


    /**
     * 根据用户编号与股票编码更新持仓数据
     * @param stockPosition
     * @return
     */
    int updateByUserIdAndStockCode(StockPosition stockPosition);


    /**
     * 获取所有的持仓股票
     * @return
     */
    List<StockPosition> listAll();



    List<StockPosition> listAllByAgreementId(Long agreementId);

    /**
     * 根据股票编码分组获取所有的持仓数据
     * @return
     */
    List<StockPosition> listAllGroupByStockCode();


    /**
     * 根据股票编码获取所有的持仓数据
     * @param stockCode
     * @return
     */
    List<StockPosition> listAllByStockCode(String stockCode);


}