package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.StockOperate;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateRestrict;

import java.util.List;


/**
 * 用户Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface StockOperateMapper extends GenericDao<StockOperate, Long> {

    /**
     * 操盘记录插入
     * @param stockOperate
     * @return
     */
     int insert(StockOperate stockOperate);


    /**
     * 获取合约下所有完结的操盘记录
     * @param stockOperateRestrict
     * @param page
     * @return
     */
     List<StockOperate> listAllStockOperatesOfAgreement(StockOperateRestrict stockOperateRestrict, Page<StockOperate> page);

    /**
     * 获取合约下所有挂单记录
     * @param stockOperateRestrict
     * @param page
     * @return
     */
    List<StockOperate> listAllWaitingStockOperateOfAgreement(StockOperateRestrict stockOperateRestrict, Page<StockOperate> page);

    /**
     * 获取当前合约下未完成的交易委托
     * @param agreementId
     * @return
     */
    int countWaitingStockOperateOfAgreement(Long agreementId);


    /**
     * 撤单中
     * @param operateId
     * @return
     */
    int cancelStockOperateApply(Long operateId);

    /**
     * 撤单
     * @param operateId
     * @return
     */
    int cancelStockOperate(Long operateId);


    /**
     * 获取当日成交记录
     * @param stockOperateRestrict
     * @param page
     * @return
     */
    List<StockOperate> listCurrentDayDeals(StockOperateRestrict stockOperateRestrict, Page<StockOperate> page);


    /**
     * 获取当日委托记录
     * @param stockOperateRestrict
     * @param page
     * @return
     */
    List<StockOperate> listCurrentDayCommissions(StockOperateRestrict stockOperateRestrict, Page<StockOperate> page);


    /**
     * 获取历史成交记录
     * @param stockOperateRestrict
     * @param page
     * @return
     */
    List<StockOperate> listHistoryDeals(StockOperateRestrict stockOperateRestrict, Page<StockOperate> page);


    /**
     * 获取历史委托记录
     * @param stockOperateRestrict
     * @param page
     * @return
     */
    List<StockOperate> listHistoryCommissions(StockOperateRestrict stockOperateRestrict, Page<StockOperate> page);


    /**
     * 更新股票信息
     * @param stockOperate
     * @return
     */
    int updateStockOperate(StockOperate stockOperate);


    /**
     * 获取当前未报的委托
     * @return
     */
    List<StockOperate> listFresh();


    /**
     * 获取某用户下所有未报的委托
     * @param userId
     * @return
     */
    List<StockOperate> listFreshWithUserId(Long userId);


    /**
     * 根据股票编码分组获取当前所有未完结委托
     * @return
     */
    List<StockOperate> listAllUnFinishedGroupByStockCode();

    /**
     * 根据股票编码获取当前所有未完结委托
     * @param stockCode
     * @return
     */
    List<StockOperate> listAllUnFinishedByStockCode(String stockCode);

    /**
     * 获取某合约下所有未完结的买入交易委托
     * @param agreementId
     * @return
     */
    List<StockOperate> listAllWaitingStockBuyOperateByAgreementId(Long agreementId);

    /**
     * 更新已经过期的交易委托
     * @return
     */
    int updateExpiredOperateById(Long operateId);


    /**
     * 获取所有过期的未完结交易委托
     * @return
     */
    List<StockOperate> listAllExpiredUnFinished();



    /**
     * 获取一个未绑定操作员的用户编号
     * @return
     */
    Long getUnBindOperatorUserId();


    /**
     * 根据合约编号统计未完成的操盘申请数量
     *
     * @param agreementId
     * @return
     */
    int countUnfinishedOperateByAgreementId(Long agreementId);


    /**
     * 获取某合约下当前还未完成的某股票申购
     *
     * @param stockOperateRestrict
     * @return
     */
    List<StockOperate> getUnFinishedStockOperateByAgreementIdAndStockCode(StockOperateRestrict stockOperateRestrict);

}