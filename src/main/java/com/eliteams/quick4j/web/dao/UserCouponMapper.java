package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.UserCoupon;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface UserCouponMapper extends GenericDao<UserCoupon, Long> {


    /**
     * 添加
     * @param userCoupon
     * @return
     */
   int insert(UserCoupon userCoupon);


    /**
     * 更新用户优惠券信息
     * @param userCoupon
     * @return
     */
    int update(UserCoupon userCoupon);


    /**
     * 更新所有过期优惠券
     * @return
     */
    int updateAllExpired();


    /**
     * 获取用户可用的所有优惠券
     * @param userId
     * @return
     */
    List<UserCoupon> listAvailableUserCouponByUserId(Long userId);


    /**
     * 根据用户编号与合约编号获取当前使用的优惠券信息
     * @param userCoupon
     * @return
     */
    UserCoupon selectByUserIdAndAgreementId(UserCoupon userCoupon);

    /**
     * 查询某一用户某优惠券激活码兑换的激活码数量
     * @param couponTicketId
     * @return
     */
    int countUserCouponByCouponTicketId(Long couponTicketId);

    /**
     * 通过用户编号与优惠券编号获取优惠券信息
     * @param userCoupon
     * @return
     */
    UserCoupon selectByUserIdAndUserCouponId(UserCoupon userCoupon);


    /**
     * 当前合约是否已绑定优惠券
     * @param agreementId
     * @return
     */
    int isAgreementAttached(Long agreementId);











}