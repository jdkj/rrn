package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.Withdrawal;
import com.eliteams.quick4j.web.model.pagerestrict.WithdrawalRestrict;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface WithdrawalMapper extends GenericDao<Withdrawal, Long> {


    /**
     * 添加
     *
     * @param withdrawal
     * @return
     */
    int insert(Withdrawal withdrawal);


    /**
     * 根据用户编号获取申请列表
     *
     * @param withdrawalRestrict
     * @param page
     * @return
     */
    List<Withdrawal> listByUserId(WithdrawalRestrict withdrawalRestrict, Page<Withdrawal> page);


}