package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.PhoneMsg;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface PhoneMsgMapper extends GenericDao<PhoneMsg, Long> {


    /**
     * 添加
     *
     * @param phoneMsg
     * @return
     */
    int insert(PhoneMsg phoneMsg);


    int countBeforeInsert(PhoneMsg phoneMsg);

}