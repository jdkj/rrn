package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementRestrict;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface AgreementMapper extends GenericDao<Agreement, Long> {


    /**
     * 获取用户当前活跃的合约
     * @param userId
     * @return
     */
    List<Agreement> listAllActivityAgreementByUserId(Long userId);


    /**
     * 添加
     *
     * @param agreement
     * @return
     */
    int insert(Agreement agreement);

    /**
     * 获取当前活动的合约记录
     *
     * @param agreementRestrict
     * @param page
     * @return
     */
    List<Agreement> listActiveAgreementsByUserIdAndSignType(AgreementRestrict<Agreement> agreementRestrict, Page<Agreement> page);


    /**
     * 根据用户编号获取合约记录
     *
     * @param agreementAgreementRestrict
     * @param page
     * @return
     */
    List<Agreement> listActiveAgreementsByUserId(AgreementRestrict<Agreement> agreementAgreementRestrict, Page<Agreement> page);


    /**
     * 获取需要结算的合约订单
     *
     * @return
     */
    List<Agreement> listCaculateAgreements();

    /**
     * 获取需要缴纳资金管理费的合约列表
     *
     * @return
     */
    List<Agreement> listNeedPayInterestAgreements();


    int updateDayAgreementByPayInterestWhenSigned(Agreement agreement);

    /**
     * 日结类型
     * 缴纳资金管理费后   更新合约状态
     *
     * @param agreementId
     * @return
     */
    int updateDayAgreementByPayInterest(Long agreementId);


    /**
     * 月结类型
     * 缴纳资金管理费后  更新合约状态
     *
     * @param agreementId
     * @return
     */
    int updateMonthAgreementByPayInterest(Long agreementId);


    /**
     * 月结类型
     * 更新持续操盘多少天记录
     *
     * @param agreementId
     * @return
     */
    int updateAgreementContinueOperateDay(Long agreementId);


    /**
     * 因未缴纳资金管理费 而进入结算状态
     * 未缴管理费从合约可用资金中扣除
     *
     * @param agreement
     * @return
     */
    int updateAgreementByLackOfInterest(Agreement agreement);

    /**
     * 合约超过最大操盘期限 更新合约状态
     *
     * @param agreement
     * @return
     */
    int updateAgreementByOverMaxPeriod(Agreement agreement);

    /**
     * 因操作股票引起的合约余额变化
     *
     * @param agreement
     * @return
     */
    int updateAgreementByOperateStock(Agreement agreement);


    /**
     * 更新合约状态
     * @param agreement
     * @return
     */
    int updateAgreementStatus(Agreement agreement);


    List<Agreement> listAll();


    int updateByFreshAgreement(Agreement agreement);


    /**
     * 更新合约数据
     * @param agreement
     * @return
     */
    int updateAgreement(Agreement agreement);

    /**
     * 根据用户编号与合约编号获取合约数据
     * @param agreementRestrict
     * @return
     */
    Agreement selectByUserIdAndId(AgreementRestrict agreementRestrict);


    /**
     * 获取快要逾期的合约列表
     *
     * @return
     */
    List<Agreement> listAlmostOverdueAgreements();


    /**
     * 获取因缺少管理费而需要结算的合约列表
     *
     * @return
     */
    List<Agreement> listLackOfManagementFeeAgreements();


    /**
     * 获取达到最大操盘期限的合约列表集合
     *
     * @return
     */
    List<Agreement> listOverMaxPeriodAgreements();


    /**
     * 合约结算失败更新
     *
     * @param agreement
     * @return
     */
    int updateBySettlementFailed(Agreement agreement);


    /**
     * 统计某用户当前持有的某类型合约数量
     *
     * @param agreementRestrict
     * @return
     */
    int countActiveAgreementByTypeAndUserId(AgreementRestrict agreementRestrict);

}