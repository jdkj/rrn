package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.model.UserIdAuth;


/**
 * 用户Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface UserIdAuthMapper extends GenericDao<UserIdAuth, Long> {


    /**
     * 写入身份认证信息
     * @param userIdAuth
     * @return
     */
    int insert(UserIdAuth userIdAuth);


    /**
     * 根据用户编号获取身份认证信息
     * @param userId
     * @return
     */
    UserIdAuth selectByUserId(Long userId);


}