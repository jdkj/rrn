package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.SecuritiesAccountGrossProfit;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountGrossProfitRestrict;

import java.util.List;


/**
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface SecuritiesAccountGrossProfitMapper extends GenericDao<SecuritiesAccountGrossProfit, Long> {


    /**
     * 添加
     *
     * @param securitiesAccountGrossProfit
     * @return
     */
    int insert(SecuritiesAccountGrossProfit securitiesAccountGrossProfit);


    /**
     * 获取利润明细列表
     *
     * @return
     */
    List<SecuritiesAccountGrossProfit> listByRestrict(SecuritiesAccountGrossProfitRestrict securitiesAccountGrossProfitRestrict, Page<SecuritiesAccountGrossProfit> page);


}