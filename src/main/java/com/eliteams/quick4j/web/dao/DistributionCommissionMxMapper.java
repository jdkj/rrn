package com.eliteams.quick4j.web.dao;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.DistributionCommissionMx;
import com.eliteams.quick4j.web.model.pagerestrict.DistributionCommissionMxRestrict;

import java.util.List;

public interface DistributionCommissionMxMapper extends GenericDao<DistributionCommissionMx, Long> {

    /**
     * 添加
     *
     * @param distributionCommissionMx
     * @return
     */
    int insert(DistributionCommissionMx distributionCommissionMx);


    /**
     * 根据请求参数获取列表数据
     *
     * @param distributionCommissionMxRestrict
     * @param page
     * @return
     */
    List<DistributionCommissionMx> listByRestrict(DistributionCommissionMxRestrict distributionCommissionMxRestrict, Page<DistributionCommissionMx> page);

}
