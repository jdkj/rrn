package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.BorrowAgreement;

import java.util.List;


/**
 * 短期合同Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface BorrowAgreementMapper extends GenericDao<BorrowAgreement, Long> {


    /**
     * 添加
     *
     * @param borrowAgreement
     * @return
     */
    int insert(BorrowAgreement borrowAgreement);


    /**
     * 根据合约类型获取合同倍数以及费率
     * @param borrowType 借款合约类型
     * @return
     */
    List<BorrowAgreement> list(Integer borrowType);


    /**
     * 获取当前存在的合约类别
     * @return
     */
    List<BorrowAgreement> listActivityBorrowType();

    /**
     * 根据合约类别编号获取合约类别信息
     * @param borrowType
     * @return
     */
    BorrowAgreement getBorrowTypeByType(Integer borrowType);

}