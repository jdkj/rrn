package com.eliteams.quick4j.web.dao;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.RandCode;

/**
 * Created by Dsmart on 2016/6/27.
 */
public interface RandCodeMapper extends GenericDao<RandCode,Long> {

    /**
     * 插入记录
     * @param randCode
     * @return
     */
    int insert(RandCode randCode);

    /**
     * 同一手机号请求次数
     * 限制5分钟内最多3次
     * @param randCode
     * @return
     */
    int countByPhone(RandCode randCode);

    /**
     * 同一客户端请求次数
     * 限制15分钟内 最多6次
     * @param randCode
     * @return
     */
    int countByClient(RandCode randCode);

    /**
     * 同一ip地址请求次数
     * 限制30分钟内最多 20次
     * @param randCode
     * @return
     */
    int countByIp(RandCode randCode);

    /**
     * 验证手机号与短信码是否正确
     * @param randCode
     * @return
     */
    RandCode authPhoneCodes(RandCode randCode);

    /**
     * 更新验证过后的数据
     * @param randCode
     * @return
     */
    int updateByPrimaryKey(RandCode randCode);

}
