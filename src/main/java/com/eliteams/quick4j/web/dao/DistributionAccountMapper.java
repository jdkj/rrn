package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.DistributionAccount;

/*
 * 分销体系账户Dao
 */
public interface DistributionAccountMapper extends GenericDao<DistributionAccount, Long> {


    /**
     * 添加
     *
     * @param distributionAccount
     * @return
     */
    int insert(DistributionAccount distributionAccount);


    /**
     * 根据分销体系用户编号获取
     *
     * @param distributionUserId
     * @return
     */
    DistributionAccount selectByDistributionUserId(Long distributionUserId);


}
