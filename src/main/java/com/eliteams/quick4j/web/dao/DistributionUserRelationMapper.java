package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.DistributionUserRelation;

import java.util.List;

/*
 * 登陆数据处理
 */
public interface DistributionUserRelationMapper extends GenericDao<DistributionUserRelation, Long> {


    /**
     * 添加
     *
     * @param distributionUser
     * @return
     */
    int insert(DistributionUserRelation distributionUser);


    /**
     * 根据提供佣金的用户编号获取参与分佣的销售员
     *
     * @param customerId
     * @return
     */
    List<DistributionUserRelation> listAllByCustomerId(Long customerId);

}
