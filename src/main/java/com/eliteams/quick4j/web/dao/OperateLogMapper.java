package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.OperateLog;
import com.eliteams.quick4j.web.model.pagerestrict.OperateLogRestrict;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface OperateLogMapper extends GenericDao<OperateLog, Long> {

    /**
     * 添加钱查询是否已存在该数据
     * @param operateLog
     * @return
     */
    int countBeforeInsert(OperateLog operateLog);

    /**
     * 添加
     *
     * @param operateLog
     * @return
     */
    int insert(OperateLog operateLog);

    /**
     * 获取某操盘手被驳回的操盘
     *
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listRejectedByRestrictWithAdminId(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);


    /**
     * 获取新的待处理数据
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listRefreshByRestrictWithAdminId(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);


    /**
     * 股市操盘更新数据
     * @param operateLog
     * @return
     */
    int operateStock(OperateLog operateLog);

    /**
     * 提交接口单后更新状态，避免重复提交
     *
     * @param logId
     * @return
     */
    int updateStockOperateStatus(Long logId);


    /**
     * 更新数据前 检测回单号是否存在
     * @param operateLog
     * @return
     */
    int countBeforeUpdateByOperateStock(OperateLog operateLog);



    /**
     * 获取所有待审查的操盘处理数据
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listAllWaitingReviewByRestrict(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);


    /**
     * 审核更新数据
     * @param operateLog
     * @return
     */
    int updateByReviewOperateStock(OperateLog operateLog);


    /**
     * 获取所有被驳回还未处理的数据
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listAllRejectedByRestrict(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);


    /**
     * 确认回单信息
     * @param operateLog
     * @return
     */
    int confirmOperateStock(OperateLog operateLog);

    /**
     * 根据回单号获取操作记录
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> selectByStockOperateId(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);

    /**
     * 用户提交撤单申请
     * @param operateLog
     * @return
     */
    int updateByCustomerCancel(OperateLog operateLog);

    /**
     * 确认用户撤单申请
     * @param operateLog
     * @return
     */
    int confirmCustomerCancelApply(OperateLog operateLog);


    /**
     * 获取某操盘手下 客户撤单数据列表
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listCustomerCancelByRestrictWithAdminId(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);


    /**
     * 分页获取客户撤单数据列表
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listCustomerCancelByRestrict(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);


    /**
     * 根据用户交易申请号 获取交易记录
     * @param operateId
     * @return
     */
    OperateLog selectByOperateId(Long operateId);


    /**
     * 根据参数获取当天交易记录
     *
     * @param operateLog
     * @return
     */
    List<OperateLog> selectCurrentDayDatasByOperateLog(OperateLog operateLog);


    /**
     * 获取某券商账户下新的待处理数据
     *
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listRefreshByRestrictWithSecuritiesAccountId(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);


    /**
     * 获取券商下所有操盘记录
     *
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listByRestrictWithSecuritiesAccountId(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);

    /**
     * 获取某券商账户下当天撤销的操作请求
     *
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listCustomerCancelByRestrictWithSecuritiesAccountId(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);

    /**
     * 获取某券商账户下当天需要确认的操作请求
     *
     * @param operateLogRestrict
     * @param page
     * @return
     */
    List<OperateLog> listWaitingConfirmByRestrictWithSecuritiesAccountId(OperateLogRestrict operateLogRestrict, Page<OperateLog> page);


    /**
     * 统计超时的操盘申请数量
     *
     * @param maxDelaySeconds
     * @return
     */
    int countOverTimeStockOperateApply(Long maxDelaySeconds);


    /**
     * @param maxDelaySeconds
     * @return
     */
    int countOverTimeStockConfirm(Long maxDelaySeconds);

}