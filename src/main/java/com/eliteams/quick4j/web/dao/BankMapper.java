package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.Bank;
import com.eliteams.quick4j.web.model.ForbidStock;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface BankMapper extends GenericDao<Bank, Long> {


    /**
     * 添加
     * @param bank
     * @return
     */
   int insert(Bank bank);


    /**
     * 获取所有启用的银行信息
     * @return
     */
    List<Bank> listActivity();




}