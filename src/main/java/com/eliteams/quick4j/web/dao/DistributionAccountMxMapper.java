package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.DistributionAccountMx;
import com.eliteams.quick4j.web.model.pagerestrict.DistributionAccountMxRestrict;

import java.util.List;

/*
 * 佣金明细
 */
public interface DistributionAccountMxMapper extends GenericDao<DistributionAccountMx, Long> {


    /**
     * 添加
     *
     * @param distributionAccountMx
     * @return
     */
    int insert(DistributionAccountMx distributionAccountMx);


    /**
     * 根据请求参数获取分页数据
     *
     * @param distributionAccountMxRestrict
     * @param page
     * @return
     */
    List<DistributionAccountMx> listByRestrict(DistributionAccountMxRestrict distributionAccountMxRestrict, Page<DistributionAccountMx> page);

}
