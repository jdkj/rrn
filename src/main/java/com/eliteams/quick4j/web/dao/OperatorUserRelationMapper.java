package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.OperatorUserRelation;
import com.eliteams.quick4j.web.model.pagerestrict.OperatorUserRelationRestrict;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface OperatorUserRelationMapper extends GenericDao<OperatorUserRelation, Long> {


    /**
     * 添加
     *
     * @param operatorUserRelation
     * @return
     */
    int insert(OperatorUserRelation operatorUserRelation);


    /**
     * 插入前检测是否有相同数据
     *
     * @param operatorUserRelation
     * @return
     */
    int countBeforeInsert(OperatorUserRelation operatorUserRelation);


    /**
     * 更新数据
     * @param operatorUserRelation
     * @return
     */
    int updateItem(OperatorUserRelation operatorUserRelation);


    /**
     * 根据操盘员编号获取其绑定的关系
     * @param operatorUserRelationRestrict
     * @return
     */
    List<OperatorUserRelation> listByOperatorId(OperatorUserRelationRestrict operatorUserRelationRestrict, Page<OperatorUserRelation> page);


    /**
     * 根据操盘员编号获取其所有绑定关系
     * @param adminId
     * @return
     */
    List<OperatorUserRelation> listAllByOperatorId(Long adminId);


    /**
     * 根据用户编号获取操盘关系
     * @param userId
     * @return
     */
    OperatorUserRelation selectByUserId(Long userId);


}