package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetailStocks;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailStocksRestrict;

import java.util.List;

/*
 * 券商账户持有股票信息dao
 */
public interface SecuritiesCompanyMoneyAccountDetailStocksMapper extends GenericDao<SecuritiesCompanyMoneyAccountDetailStocks, Long> {


    /**
     * 添加
     *
     * @param securitiesCompanyMoneyAccountDetailStocks
     * @return
     */
    int insert(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks);


    /**
     * 添加之前检测是否已经持有该股票
     *
     * @param securitiesCompanyMoneyAccountDetailStocks
     * @return
     */
    int countBeforeInsert(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks);


    /**
     * 根据券商账户编号与股票编码获取持仓数据
     *
     * @param securitiesCompanyMoneyAccountDetailStocks
     * @return
     */
    SecuritiesCompanyMoneyAccountDetailStocks selectBySecuritiesAccountIdAndStockCode(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks);


    /**
     * 根据持有股票编码获取股票信息
     *
     * @param stockCode
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetailStocks> listAllByStockCode(String stockCode);


    /**
     * 获取所有持有股票
     *
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetailStocks> listAllByGroupStockCode();


    /**
     * 根据请求参数获取分页数据
     *
     * @param securitiesCompanyMoneyAccountDetailStocksRestrict
     * @param page
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetailStocks> listByRestrict(SecuritiesCompanyMoneyAccountDetailStocksRestrict securitiesCompanyMoneyAccountDetailStocksRestrict, Page<SecuritiesCompanyMoneyAccountDetailStocks> page);


    /**
     * 根据券商账户获取所持有的股票数据
     *
     * @param securitiesAccountId
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetailStocks> listAllBySecuritiesAccountId(Long securitiesAccountId);


}
