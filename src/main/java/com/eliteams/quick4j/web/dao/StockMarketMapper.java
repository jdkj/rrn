package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.StockMarket;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface StockMarketMapper extends GenericDao<StockMarket, Long> {


    /**
     * 添加
     * @param stockMarket
     * @return
     */
   int insert(StockMarket stockMarket);


    /**
     * 更新股票信息
     * @param stockMarket
     * @return
     */
    int updateStockInfo(StockMarket stockMarket);


    /**
     * 获取所有存储的股票信息
     * @return
     */
    List<StockMarket> listAll();


    /**
     * 根据股票编码获取股票信息
     * @param code
     * @return
     */
    StockMarket selectByStockCode(String code);



}