package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.AgreementAiOperateStockLog;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementAiOperateStockLogRestrict;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface AgreementAiOperateStockLogMapper extends GenericDao<AgreementAiOperateStockLog, Long> {


    /**
     * 添加
     *
     * @param agreementAiOperateStockLog
     * @return
     */
    int insert(AgreementAiOperateStockLog agreementAiOperateStockLog);

    /**
     * 通过合约编号与股票编码查询记录是否存在
     *
     * @param agreementAiOperateStockLog
     * @return
     */
    AgreementAiOperateStockLog selectByAgreementIdAndStockCode(AgreementAiOperateStockLog agreementAiOperateStockLog);


    /**
     * 根据请求参数获取分页列表数据
     *
     * @param agreementAiOperateStockLogRestrict
     * @param page
     * @return
     */
    List<AgreementAiOperateStockLog> listByRestrict(AgreementAiOperateStockLogRestrict agreementAiOperateStockLogRestrict, Page<AgreementAiOperateStockLog> page);


}