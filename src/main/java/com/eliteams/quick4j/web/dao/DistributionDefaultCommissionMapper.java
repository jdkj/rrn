package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.DistributionDefaultCommission;

import java.util.List;

/*
 * 分销等级处理
 */
public interface DistributionDefaultCommissionMapper extends GenericDao<DistributionDefaultCommission, Long> {


    /**
     * 添加
     *
     * @param distributionDefaultCommission
     * @return
     */
    int insert(DistributionDefaultCommission distributionDefaultCommission);


    /**
     * 获取所有等级
     *
     * @return
     */
    List<DistributionDefaultCommission> listAllActivity();


    /**
     * 获取下属层级列表
     *
     * @param level
     * @return
     */
    List<DistributionDefaultCommission> listUnderLineLevel(Integer level);

    /**
     * 根据层级获取默认属性信息
     *
     * @param level
     * @return
     */
    DistributionDefaultCommission selectByLevel(Integer level);


}
