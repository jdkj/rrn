package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.ForbidStock;
import com.eliteams.quick4j.web.model.User;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface ForbidStockMapper extends GenericDao<ForbidStock, Long> {


    /**
     * 添加
     * @param forbidStock
     * @return
     */
   int insert(ForbidStock forbidStock);


    /**
     * 获取禁止购买股票列表
     * @return
     */
    List<ForbidStock> list();




}