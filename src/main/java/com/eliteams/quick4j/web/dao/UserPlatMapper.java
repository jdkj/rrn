 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eliteams.quick4j.web.dao;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.UserPlat;


/**
 * @description
 * @version 
 * @author Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 
 */
public interface UserPlatMapper extends GenericDao<UserPlat,Long>{
    
    
    int insert(UserPlat userPlat);
    
    /**
     * 更新后期绑定的用户编号
     * @param userPlat
     * @return 
     */
    int updateUserId(UserPlat userPlat);
    
    
    /**
     * 通过openId获取用户信息
     * @param openId
     * @return 
     */
    UserPlat getUserPlatByOpenId(String openId);
    
    
    /**
     *根据用户编号获取微信用户信息
     * @param userId
     * @return 
     */
    UserPlat getWxUserPlatByUserId(Long userId);
    
}
