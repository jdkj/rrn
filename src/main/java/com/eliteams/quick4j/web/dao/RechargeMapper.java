package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.ForbidStock;
import com.eliteams.quick4j.web.model.Recharge;

import java.util.List;


/**
 * 充值记录Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface RechargeMapper extends GenericDao<Recharge, Long> {


    /**
     * 添加
     * @param recharge
     * @return
     */
   int insert(Recharge recharge);

    /**
     * 订单支付成功更新
     * 主要用于推送未抵达
     * 通过主动查询获取支付结果
     * @param rechargeId
     * @return
     */
    int updateAfterPaySuc(Long rechargeId);


    /**
     * 订单支付校验成功更新
     * 主要用于推送检测成功更新
     * @param recharge
     * @return
     */
    int updateAfterCheckSuc(Recharge recharge);


    /**
     * 订单支付校验失败更新
     * @param recharge
     * @return
     */
    int updateAfterCheckFail(Recharge recharge);

    /**
     * 支付超时批量更新
     * @return
     */
    int updateAfterExpired();



}