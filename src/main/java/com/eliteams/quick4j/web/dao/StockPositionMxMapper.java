package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.StockPositionMx;
import com.eliteams.quick4j.web.model.pagerestrict.StockPositionMxRestrict;

import java.util.List;


/**
 * 用户Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface StockPositionMxMapper extends GenericDao<StockPositionMx, Long> {


    /**
     * @param stockPositionMx
     * @return
     */
    int insert(StockPositionMx stockPositionMx);


    /**
     * 获取某用户下某股票的详细持仓数据
     *
     * @param stockPositionMxRestrict
     * @return
     */
    List<StockPositionMx> listByRestrict(StockPositionMxRestrict stockPositionMxRestrict);


    /**
     * 获取新购股票  用于改变持仓数量
     *
     * @return
     */
    List<StockPositionMx> listFreshStocks();


    /**
     * 释放新购股票
     *
     * @param stockPositionMx
     * @return
     */
    int updateStockPositionMxByUnlock(StockPositionMx stockPositionMx);


}