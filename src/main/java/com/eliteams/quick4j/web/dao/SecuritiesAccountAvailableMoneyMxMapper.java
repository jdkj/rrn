package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.SecuritiesAccountAvailableMoneyMx;
import com.eliteams.quick4j.web.model.SecuritiesAccountTotalMoneyMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountAvailableMoneyMxRestrict;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface SecuritiesAccountAvailableMoneyMxMapper extends GenericDao<SecuritiesAccountAvailableMoneyMx, Long> {


    /**
     * 添加
     *
     * @param securitiesAccountAvailableMoneyMx
     * @return
     */
    int insert(SecuritiesAccountAvailableMoneyMx securitiesAccountAvailableMoneyMx);


    /**
     * 根据请求参数获取券商总资金变动明细
     *
     * @param securitiesAccountAvailableMoneyMxRestrict
     * @param page
     * @return
     */
    List<SecuritiesAccountTotalMoneyMx> listByRestrict(SecuritiesAccountAvailableMoneyMxRestrict securitiesAccountAvailableMoneyMxRestrict, Page<SecuritiesAccountAvailableMoneyMx> page);


}