package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.AgreementAiOperateStrategy;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface AgreementAiOperateStrategyMapper extends GenericDao<AgreementAiOperateStrategy, Long> {


    /**
     * 添加
     *
     * @param AgreementAiOperateStrategy
     * @return
     */
    int insert(AgreementAiOperateStrategy AgreementAiOperateStrategy);


    /**
     * 查询是否已存在
     *
     * @param agreementAiOperateStrategy
     * @return
     */
    AgreementAiOperateStrategy isExists(AgreementAiOperateStrategy agreementAiOperateStrategy);


}