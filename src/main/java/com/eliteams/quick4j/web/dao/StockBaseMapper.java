package com.eliteams.quick4j.web.dao;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.StockBase;
import com.eliteams.quick4j.web.model.pagerestrict.StockBaseRestrict;

import java.util.List;

/**
 * Created by Dsmart on 2017/2/7.
 */
public interface StockBaseMapper extends GenericDao<StockBase, Long> {

    /**
     * 添加
     *
     * @param stockBase
     * @return
     */
    int insert(StockBase stockBase);


    /**
     * 添加前查询是否已经存在该数据
     *
     * @param stockBase
     * @return
     */
    int countBeforeInsert(StockBase stockBase);


    /**
     * 根据股票编码获取股票数据
     *
     * @param stockCode
     * @return
     */
    StockBase selectByStockCode(String stockCode);


    /**
     * 根据股票编码获取股票基本信息
     *
     * @param stockCode
     * @return
     */
    List<StockBase> queryByStockCode(String stockCode);


    /**
     * 根据股票名称获取股票数据
     *
     * @param stockName
     * @return
     */
    List<StockBase> queryByStockName(String stockName);


    /**
     * 根据股票名称拼音获取股票数据
     *
     * @param stockNamePy
     * @return
     */
    List<StockBase> queryByStockNamePY(String stockNamePy);


    /**
     * 获取所有股票数据信息
     *
     * @return
     */
    List<StockBase> listAll();


    /**
     * 根据流通市值与适应力筛选合适的股票
     *
     * @param stockBaseRestrict
     * @return
     */
    List<StockBase> queryByFlowValueAndPe(StockBaseRestrict stockBaseRestrict);


    /**
     * 模糊查询
     *
     * @param stockBaseRestrict
     * @return
     */
    List<StockBase> fuzzyQuery(StockBaseRestrict stockBaseRestrict, Page<StockBase> page);


    StockBase query(String code);

}
