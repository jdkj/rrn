package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.CouponTickets;



/**
 * 禁止购买股票Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface CouponTicketsMapper extends GenericDao<CouponTickets, Long> {


    /**
     * 添加
     * @param couponTickets
     * @return
     */
   int insert(CouponTickets couponTickets);


    /**
     * 通过激活码激活优惠券
     * @param couponTickets
     * @return
     */
   int activiteCouponTickets(CouponTickets couponTickets);




}