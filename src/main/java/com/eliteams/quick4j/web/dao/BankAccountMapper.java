package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.Bank;
import com.eliteams.quick4j.web.model.BankAccount;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface BankAccountMapper extends GenericDao<BankAccount, Long> {


    /**
     * 添加
     * @param bankAccount
     * @return
     */
   int insert(BankAccount bankAccount);


    /**
     * 根据用户编号获取银行卡号信息
     * @param userId
     * @return
     */
    BankAccount selectByUserId(Long userId);



}