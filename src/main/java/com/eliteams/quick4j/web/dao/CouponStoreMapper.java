package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.Bank;
import com.eliteams.quick4j.web.model.CouponStore;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface CouponStoreMapper extends GenericDao<CouponStore, Long> {


    /**
     * 添加
     * @param couponStore
     * @return
     */
   int insert(CouponStore couponStore);


    /**
     * 获取所有优惠券信息
     * @return
     */
    List<CouponStore> listAll();


    /**
     * 获取所有可兑换优惠券
     * @return
     */
    List<CouponStore> listAllExchangeableCouponStore();




}