package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.DistributionUser;
import com.eliteams.quick4j.web.model.pagerestrict.DistributionUserRestrict;

import java.util.List;

/*
 * 登陆数据处理
 */
public interface DistributionUserMapper extends GenericDao<DistributionUser, Long> {

    /**
     * 登录验证
     *
     * @param distributionUserRestrict
     * @return
     */
    DistributionUser authentication(DistributionUserRestrict distributionUserRestrict);

    /**
     * 通过账号号获取用户信息
     *
     * @param accountNum
     * @return
     */
    DistributionUser selectByAccountNum(Long accountNum);

    /**
     * 密码修改操作
     *
     * @param distributionUser
     * @return
     */
    int upAccountPasswordById(DistributionUser distributionUser);

    /**
     * 根据用户id更新用户信息
     *
     * @param distributionUser
     * @return
     */
    int upAccountInfoById(DistributionUser distributionUser);

    /**
     * 根据id查询用户
     *
     * @param distributionUserId
     * @return
     */
    DistributionUser selectById(Long distributionUserId);

    /**
     * 根据用户系统用户编号获取对应的分销帐号信息
     *
     * @param userId
     * @return
     */
    DistributionUser selectByUserId(Long userId);

    /**
     * 添加
     *
     * @param distributionUser
     * @return
     */
    int insert(DistributionUser distributionUser);


    /**
     * 根据请求参数获取列表
     *
     * @param distributionUserRestrict
     * @param page
     * @return
     */
    List<DistributionUser> listByRestrict(DistributionUserRestrict distributionUserRestrict, Page<DistributionUser> page);


}
