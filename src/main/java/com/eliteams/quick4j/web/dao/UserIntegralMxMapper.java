package com.eliteams.quick4j.web.dao;



import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.UserIntegralMx;
import com.eliteams.quick4j.web.model.pagerestrict.UserIntegralMxRestrict;

import java.util.List;


/**
 * 用户Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface UserIntegralMxMapper extends GenericDao<UserIntegralMx, Long> {

    /**
     * 添加账户信息
     * @param userIntegralMx
     * @return
     */
    int insert(UserIntegralMx userIntegralMx);

    /**
     * 列表显示积分流水记录
     * @param userIntegralMxRestrict
     * @return
     */
    List<UserIntegralMx> listByRestrict(UserIntegralMxRestrict userIntegralMxRestrict,Page<UserIntegralMx> page);


}