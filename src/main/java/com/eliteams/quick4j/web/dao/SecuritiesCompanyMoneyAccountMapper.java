package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccount;

/*
 * 券商总资金账户dao
 */
public interface SecuritiesCompanyMoneyAccountMapper extends GenericDao<SecuritiesCompanyMoneyAccount, Long> {


    /**
     * 添加
     *
     * @param
     * @return
     */
    int insert(SecuritiesCompanyMoneyAccount securitiesCompanyMoneyAccount);


    /**
     * 获取账户信息
     *
     * @return
     */
    SecuritiesCompanyMoneyAccount selectOne();


}
