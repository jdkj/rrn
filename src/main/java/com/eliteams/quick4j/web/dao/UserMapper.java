package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.User;


/**
 * 用户Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface UserMapper extends GenericDao<User, Long> {


    /**
     * 检测手机号是否已经注册
     *
     * @param phone
     * @return
     */
    int isPhoneRegisted(String phone);


    /**
     * 通过手机号注册
     *
     * @param user
     * @return
     */
    int registeByPhone(User user);

    /**
     * 通过手机号注册
     *
     * @param user
     * @return
     */
    int registeByPhoneAndPassword(User user);


    /**
     * 更新用户手机号
     * @param user
     * @return
     */
    int updatePhone(User user);


    /**
     * 更新用户登录密码
     *
     * @param user
     * @return
     */
    int updatePassword(User user);


    /**
     * 更新用户提现密码
     *
     * @param user
     * @return
     */
    int updateAccountPassword(User user);


    /**
     * 用户通过手机号密码登录检验
     *
     * @param user
     * @return
     */
    User loginByPhoneAndPassword(User user);


    /**
     * 通过手机号获取用户信息
     *
     * @param phone
     * @return
     */
    User selectByPhone(String phone);


    /**
     * 验证用户密码是否正确
     * 用于修改密码权限校验
     * @param user
     * @return
     */
    int validateUserIdAndPassword(User user);


    /**
     * 验证用户账户操作密码是否正确
     * 用户修改账户操作密码权限校验
     * @param user
     * @return
     */
    int validateUserIdAndAccountPassword(User user);

    /**
     * 更新用户标识信息
     * @param user
     * @return
     */
    int updateParamInfo(User user);


    /**
     * 通过第三方平台授权登录
     * @param user
     * @return
     */
    int registeByPlat(User user);


    /**
     * 更新用户头像信息
     * @param user
     * @return
     */
    int updateUserImage(User user);


}