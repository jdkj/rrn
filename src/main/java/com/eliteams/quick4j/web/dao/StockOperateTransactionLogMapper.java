package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.StockOperateTransactionLog;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateTransactionLogRestrict;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface StockOperateTransactionLogMapper extends GenericDao<StockOperateTransactionLog, Long> {


    /**
     * 添加数据
     *
     * @param stockOperateTransactionLog
     * @return
     */
    int insert(StockOperateTransactionLog stockOperateTransactionLog);


    /**
     * 交易成交插入数据
     *
     * @param stockOperateTransactionLog
     * @return
     */
    int insertByFinishOperate(StockOperateTransactionLog stockOperateTransactionLog);


    /**
     * 交易取消插入数据
     *
     * @param stockOperateTransactionLog
     * @return
     */
    int insertByCancelOperate(StockOperateTransactionLog stockOperateTransactionLog);


    /**
     * 根据合约编号获取当日成交记录
     *
     * @param stockOperateTransactionLogRestrict
     * @param page
     * @return
     */
    List<StockOperateTransactionLog> listCurrentDayDealsByRestrictWithAgreementId(StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict, Page<StockOperateTransactionLog> page);


    /**
     * 根据合约编号获取历史成交记录
     *
     * @param stockOperateTransactionLogRestrict
     * @param page
     * @return
     */
    List<StockOperateTransactionLog> listHistoryDealsByRestrictWithAgreementId(StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict, Page<StockOperateTransactionLog> page);

    /**
     * 插入前检测是否存在
     *
     * @param stockOperateTransactionLog
     * @return
     */
    int countBeforeAddByFinish(StockOperateTransactionLog stockOperateTransactionLog);


    /**
     * 插入前检测是否存在
     *
     * @param stockOperateTransactionLog
     * @return
     */
    int countBeforeAddByCancel(StockOperateTransactionLog stockOperateTransactionLog);


}