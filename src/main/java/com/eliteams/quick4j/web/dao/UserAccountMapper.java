package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;

import com.eliteams.quick4j.web.model.UserAccount;


/**
 * 用户Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface UserAccountMapper extends GenericDao<UserAccount, Long> {

    /**
     * 添加账户信息
     * @param userAccount
     * @return
     */
    int insert(UserAccount userAccount);


    /**
     * 根据用户编号获取用户账户信息
     * @param userId
     * @return
     */
    UserAccount selectByUserId(Long userId);


    /**
     * 更新用户账户资金
     * @param userAccount
     * @return
     */
    int updateAccountMoney(UserAccount userAccount);

    /**
     * 更新用户账户积分
     * @param userAccount
     * @return
     */
    int updateAccountIntegral(UserAccount userAccount);


}