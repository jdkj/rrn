package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.SystemParam;

import java.util.List;


/**
 * 禁止购买股票Dao接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface SystemParamMapper extends GenericDao<SystemParam, Long> {


    /**
     * 添加
     * @param systemParam
     * @return
     */
   int insert(SystemParam systemParam);


    /**
     * 根据参数名称获取参数值
     * @param param
     * @return
     */
    SystemParam selectByParam(String param);


    /**
     * 更新系统参数
     * @param systemParam
     * @return
     */
    int update(SystemParam systemParam);


    /**
     * 获取所有系统参数的值
     * @return
     */
    List<SystemParam> listAll();




}