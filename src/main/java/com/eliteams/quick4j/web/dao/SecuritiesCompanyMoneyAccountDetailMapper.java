package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetail;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailRestrict;

import java.util.List;

/*
 * 券商总资金账户dao
 */
public interface SecuritiesCompanyMoneyAccountDetailMapper extends GenericDao<SecuritiesCompanyMoneyAccountDetail, Long> {


    /**
     * 添加
     *
     * @param
     * @return
     */
    int insert(SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail);


    /**
     * 获取可用余额最大的账户
     *
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getMaxAvailableMoneyObject();


    /**
     * 从所有券商账户中获取当前可用余额最大的账户
     *
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getMaxAvailableMoneyFromAll();

    /**
     * 从虚拟账户中获取当前可用余额最大的账户
     *
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getMaxAvailableMoneyFromVisual();


    /**
     * 获取申请合约可用余额最大的账户
     *
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getMaxAgreementAvailableMoneyFromReal();


    /**
     * 从所有券商账户中获取申请合约可用余额最大的账户
     *
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getMaxAgreementAvailableMoneyFromAll();

    /**
     * 从虚拟账户中获取申请合约可用余额最大的账户
     *
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getMaxAgreementAvailableMoneyFromVisual();


    /**
     * 获取所有账户信息
     *
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetail> listAll();


    /**
     * 根据请求参数获取分页列表数据
     *
     * @param securitiesCompanyMoneyAccountDetailRestrict
     * @param page
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetail> listByRestrict(SecuritiesCompanyMoneyAccountDetailRestrict securitiesCompanyMoneyAccountDetailRestrict, Page<SecuritiesCompanyMoneyAccountDetail> page);


}
