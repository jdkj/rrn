package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.UserAccountMx;
import com.eliteams.quick4j.web.model.pagerestrict.UserAccountMxRestrict;

import java.util.List;


/**
 * 用户Dao接口
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:49:57
 **/
public interface UserAccountMxMapper extends GenericDao<UserAccountMx, Long> {

    /**
     * 添加账户信息
     * @param userAccountMx
     * @return
     */
    int insert(UserAccountMx userAccountMx);


    /**
     * 根据请求参数分页获取用户资金明细列表
     * @param userAccountMxRestrict 用户编号、资金变动类型
     * @param page
     * @return
     */
    List<UserAccountMx> listByRestrict(UserAccountMxRestrict userAccountMxRestrict,Page<UserAccountMx> page);

}