package com.eliteams.quick4j.web.dao;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetailMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailMxRestrict;

import java.util.List;

/*
 * 券商总资金账户dao
 */
public interface SecuritiesCompanyMoneyAccountDetailMxMapper extends GenericDao<SecuritiesCompanyMoneyAccountDetailMx, Long> {


    /**
     * 添加
     *
     * @param
     * @return
     */
    int insert(SecuritiesCompanyMoneyAccountDetailMx securitiesCompanyMoneyAccountDetailMx);


    /**
     * 根据请求参数获取列表数据
     *
     * @param securitiesCompanyMoneyAccountDetailMxRestrict
     * @param page
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetailMx> listByRestrict(SecuritiesCompanyMoneyAccountDetailMxRestrict securitiesCompanyMoneyAccountDetailMxRestrict, Page<SecuritiesCompanyMoneyAccountDetailMx> page);


}
