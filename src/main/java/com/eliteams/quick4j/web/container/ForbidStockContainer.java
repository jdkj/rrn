package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.web.model.ForbidStock;
import com.eliteams.quick4j.web.service.ForbidStockService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 禁止购买股票容器
 * Created by Dsmart on 2016/11/22.
 */
@Component
public class ForbidStockContainer {
    @Resource
    private ForbidStockService stockService;

    private static ForbidStockService forbidStockService;
    /**
     * 内存永久缓存禁止购买股票
     */
    private static ConcurrentHashMap<String, ForbidStock> container;


    /**
     * 加载本地固化数据
     */
    public static void loadLocalData() {
        //获取禁止购买股票数据
        List<ForbidStock> forbidStockList = forbidStockService.getForbidStocks();
        //填充进缓存
        if (forbidStockList != null && !forbidStockList.isEmpty()) {
            for (ForbidStock forbidStock : forbidStockList) {
                ForbidStockContainerHandler.getContainer().put(forbidStock.getStockCode(), forbidStock);
            }
        }
    }

    /**
     * 判断股票是否为禁止购买股票
     *
     * @param forbidStock
     * @return
     */
    public static boolean isForbiden(ForbidStock forbidStock) {
        if (forbidStock != null) {
            return isForbiden(forbidStock.getStockCode());
        }
        return false;
    }

    /**
     * @param stockCode
     * @return
     */
    public static boolean isForbiden(String stockCode) {
        if (StringUtils.isNotEmpty(stockCode)) {
            return ForbidStockContainerHandler.getContainer().containsKey(stockCode);
        }
        return false;
    }

    @PostConstruct
    public void init() {
        this.forbidStockService = this.stockService;
    }

    /**
     * 内部静态类  初始化容器
     */
    private static class ForbidStockContainerHandler {

        private static ConcurrentHashMap<String, ForbidStock> getContainer() {
            if (container == null) {
                container = new ConcurrentHashMap<>();
                //加载固态存储数据
                loadLocalData();
            }

            return container;
        }
    }


}
