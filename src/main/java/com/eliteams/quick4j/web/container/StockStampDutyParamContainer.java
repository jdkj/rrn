package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.SystemParam;

/**
 * 印花税参数容器
 * Created by Dsmart on 2016/11/2.
 */
public class StockStampDutyParamContainer {


    public static Double getStockStampDutyRate(){
        //印花税最低费率
        SystemParamEnums systemParamEnums = SystemParamEnums.STOCK_STAMP_DUTY_PARAM;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseFee = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        if(systemParam!=null){
            baseFee = systemParam.getParamValue();
        }

        //系统设置交易委托最低收费
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        //交易委托最低收费不可低于预设定
        base = base.compareTo(paramBase)>0?base:paramBase;

        return base;
    }

}
