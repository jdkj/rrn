package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.SystemParam;

/**
 * Created by Dsmart on 2016/12/10.
 */
public class IntegralMoneyRateParamContainer {

    public static Double getIntegralMoneyRate(){
        //积分兑换汇率
        SystemParamEnums systemParamEnums = SystemParamEnums.INTEGRAL_MONEY_RATE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseRate = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseRate);

        if(systemParam!=null){
            baseRate = systemParam.getParamValue();
        }

        //系统设置积分兑换汇率
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseRate);

        //积分兑换汇率不可小于100
        base = paramBase.compareTo(100d)<0?base:paramBase;

        return base;
    }

}
