package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.web.model.StockMarket;
import com.eliteams.quick4j.web.service.StockMarketService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 股票信息容器
 * Created by Dsmart on 2016/11/23.
 */
@Component
public class StockMarketContainer {

    @Resource
    private  StockMarketService marketService;

    private static StockMarketService stockMarketService;

    private static ConcurrentHashMap<String, StockMarket> container;

    /**
     * 载入本地固化数据
     */
    public static void loadLocalData() {
        List<StockMarket> stockMarketList = stockMarketService.listAll();
        if (stockMarketList != null && !stockMarketList.isEmpty()) {
            for (StockMarket stockMarket : stockMarketList) {
                container.put(stockMarket.getCode(), stockMarket);
            }
        }
    }

    /**
     * 根据股票编码获取股票数据
     *
     * @param code
     * @return
     */
    public static StockMarket get(String code) {
        if (StringUtils.isNotEmpty(code)) {
            return StockMarketContainerHandler.getContainer().get(code);
        }
        return null;
    }

    /**
     * 向容器中添加股票信息
     *
     * @param stockMarket
     */
    public static void put(StockMarket stockMarket) {
        if (stockMarket != null && StringUtils.isNotEmpty(stockMarket.getCode())) {
            StockMarketContainerHandler.getContainer().put(stockMarket.getCode(), stockMarket);
        }
    }

    /**
     * 获取容器
     *
     * @return
     */
    public static ConcurrentHashMap<String, StockMarket> getInstance() {
        return StockMarketContainerHandler.getContainer();
    }

    /**
     * 批量更新
     *
     * @param stockMarketList
     */
    public static void putList(List<StockMarket> stockMarketList) {
        if (stockMarketList != null && !stockMarketList.isEmpty()) {
            for (int i = 0; i < stockMarketList.size(); i++) {
                StockMarket stockMarket = stockMarketList.get(i);
                StockMarket old = get(stockMarket.getCode());
                if(old!=null){
                    stockMarket.setStockMarketId(old.getStockMarketId());
                }
                put(stockMarket);
            }
        }
    }



    @PostConstruct
    public void init(){
        this.stockMarketService = this.marketService;
    }

    private static class StockMarketContainerHandler {

        private static ConcurrentHashMap<String, StockMarket> getContainer() {
            if (container == null) {
                container = new ConcurrentHashMap<>();
                loadLocalData();
            }

            return container;
        }

    }


}
