package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.SystemParam;

/**
 * Created by Dsmart on 2017/3/20.
 */
public class AgreementBorrowMoneyCostRateContainer {

    private AgreementBorrowMoneyCostRateContainer() {
    }

    /**
     * 获取资金成本费率
     *
     * @return
     */
    public static Double getAgreementBorrowMoneyCostRate() {

        SystemParamEnums systemParamEnums = SystemParamEnums.AGREEMENT_BORROW_MONEY_COST_RATE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseFee = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        if (systemParam != null) {
            baseFee = systemParam.getParamValue();
        }

        //系统设置交易委托最低收费费率
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        //交易委托最低收费费率不可低于预设定
        base = base.compareTo(paramBase) > 0 ? base : paramBase;

        return base;


    }


}
