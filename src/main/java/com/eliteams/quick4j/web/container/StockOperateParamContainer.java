package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.SystemParam;

/**
 * 股票操盘参数容器
 * Created by Dsmart on 2016/11/2.
 */
public class StockOperateParamContainer {

    /**
     * 获取股票交易最低收费
     *
     * @return
     */
    public static Double getOperateBaseFee() {
        //交易委托最低收费
        SystemParamEnums systemParamEnums = SystemParamEnums.STOCK_OPERATE_BASE_FEE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseFee = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        if(systemParam!=null){
            baseFee = systemParam.getParamValue();
        }

        //系统设置交易委托最低收费
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        //交易委托最低收费不可低于预设定
        base = base.compareTo(paramBase)>0?base:paramBase;

        return base;
    }

    /**
     * 获取股票交易最低费率
     * @return
     */
    public static Double getOperateBaseRate() {

        //交易委托最低收费费率
        SystemParamEnums systemParamEnums = SystemParamEnums.STOCK_OPERATE_BASE_RATE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseFee = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        if (systemParam != null) {
            baseFee = systemParam.getParamValue();
        }

        //系统设置交易委托最低收费费率
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        //交易委托最低收费费率不可低于预设定
        base = base.compareTo(paramBase) > 0 ? base : paramBase;

        return base;
    }


    public static Double getOperateCostBaseFee() {
        //交易委托最低收费
        SystemParamEnums systemParamEnums = SystemParamEnums.STOCK_OPERATE_COST_BASE_FEE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseFee = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        if (systemParam != null) {
            baseFee = systemParam.getParamValue();
        }

        //系统设置交易委托最低收费
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        //交易委托最低收费不可低于预设定
        base = base.compareTo(paramBase) > 0 ? base : paramBase;

        return base;
    }

    public static Double getOperateCostBaseRate() {

        //交易委托最低收费费率
        SystemParamEnums systemParamEnums = SystemParamEnums.STOCK_OPERATE_COST_BASE_RATE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseFee = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        if(systemParam!=null){
            baseFee = systemParam.getParamValue();
        }

        //系统设置交易委托最低收费费率
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        //交易委托最低收费费率不可低于预设定
        base = base.compareTo(paramBase)>0?base:paramBase;

        return base;
    }
}
