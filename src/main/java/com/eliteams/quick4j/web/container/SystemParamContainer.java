package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.core.threadpool.GlobalFixedThreadPool;
import com.eliteams.quick4j.web.model.SystemParam;
import com.eliteams.quick4j.web.service.SystemParamService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Dsmart on 2016/11/29.
 */
@Component
public class SystemParamContainer {

    @Resource
    private SystemParamService paramService;


    private static SystemParamService systemParamService;


    private static ConcurrentHashMap<String,SystemParam> container;

    @PostConstruct
    public void init(){
        this.systemParamService = paramService;
    }


    private static class SystemParamContainerHandler{
        private static ConcurrentHashMap<String,SystemParam> getContainer(){
            if(container==null){
                container = new ConcurrentHashMap<>();
                loadLocalData();
            }
            return container;
        }
    }

    /**
     * 加载本地数据
     */
    private static void loadLocalData(){
        List<SystemParam> list = systemParamService.listAll();
        if(list!=null&&!list.isEmpty()){
            for(SystemParam systemParam:list){
                container.put(systemParam.getParam(),systemParam);
            }
        }
    }


    /**
     * 获取系统参数配置
     * @param param
     * @return
     */
    public static SystemParam get(String param){
       if(StringUtils.isNotEmpty(param)){
           return SystemParamContainerHandler.getContainer().get(param);
       }
        return null;
    }


    /**
     * 设置系统参数数据
     * @param systemParam
     */
    public static void put(SystemParam systemParam){
            put(systemParam,false);
    }




    /**
     * 设置系统参数
     * @param systemParam
     * @param  forceUpdateLocal 是否强制更新本地数据
     */
    public synchronized static void put(final SystemParam systemParam,Boolean forceUpdateLocal){
        if(systemParam!=null&&StringUtils.isNotEmpty(systemParam.getParam())){
            SystemParamContainerHandler.getContainer().put(systemParam.getParam(),systemParam);
            if(forceUpdateLocal){
                /**
                 * 异步更新本地数据
                 */
                GlobalFixedThreadPool.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {
                        systemParamService.updateItem(systemParam);
                    }
                });
            }

        }
    }

}
