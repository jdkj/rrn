package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.SystemParam;

/**
 * 过户费 参数容器
 * Created by Dsmart on 2016/11/2.
 */
public class StockTransferParamContainer {




    public static Double getStockTransferRate() {
        //过户费最低费率
        SystemParamEnums systemParamEnums = SystemParamEnums.STOCK_TRANSFER_RATE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseFee = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        if(systemParam!=null){
            baseFee = systemParam.getParamValue();
        }

        //系统设置的最低过户费费率
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        //过户费费率不可低于预设定
        base = base.compareTo(paramBase)>0?base:paramBase;

        return base;

    }

    public static Double getStockTransferBaseFee() {
        //过户费最低收费
        SystemParamEnums systemParamEnums = SystemParamEnums.STOCK_TRANSFER_BASE_FEE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        String baseFee = systemParamEnums.getParamDefaultValue();

        Double base = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        if(systemParam!=null){
            baseFee = systemParam.getParamValue();
        }

        //系统设置的最低过户费
        Double paramBase = CurrencyUtils.parseMoenyRoundHalfUp(baseFee);

        //过户最低收费不可低于预设定
        base = base.compareTo(paramBase)>0?base:paramBase;

        return base;
    }
}
