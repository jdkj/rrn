package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.SystemParam;

public class SecuritiesAccountCategoryTypeParamContainer {


    public static Integer getCategoryType() {

        int type = 0;

        SystemParamEnums systemParamEnums = SystemParamEnums.SECURITIES_ACCOUNT_CATEGORY_TYPE;

        SystemParam systemParam = SystemParamContainer.get(systemParamEnums.getParam());

        type = Integer.parseInt(systemParamEnums.getParamDefaultValue());

        if (systemParam != null) {
            type = Integer.parseInt(systemParam.getParamValue());
        }

        return type;

    }


}
