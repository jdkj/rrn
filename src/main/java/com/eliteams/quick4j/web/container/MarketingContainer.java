package com.eliteams.quick4j.web.container;

import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.enums.SystemParamEnums;
import com.eliteams.quick4j.web.model.SystemParam;

/**
 * 记录当前是否操盘日
 * Created by Dsmart on 2016/10/27.
 */
public class MarketingContainer {


    /**
     * 当前是否操盘日
     * @return
     */
    public static Boolean isMarketingOn(){
        Boolean marketingOn = false;
        //获取系统设置参数
        SystemParam systemParam = SystemParamContainer.get(SystemParamEnums.STOCK_MARKET_ON.getParam());

        //判断当天是否开市
        if(systemParam!=null&&systemParam.getParamValue().equals("1")){
            marketingOn = true;
        }
        return marketingOn;
    }



    /**
     * 当前时间段是否为开市时间
     * @param fixMills  修正参数时间    将时间区间左右扩大
     * @return
     */
    public static Boolean isMarketingOnTime(Long fixMills){
        if(isMarketingOn()){
            return StockMarketingUtils.isMarketingTime(fixMills);
        }
        return false;
    }


    /**
     * 当前时间段是否为开市时间
     * @return
     */
    public static Boolean isMarketingOnTime(){
//        if(isMarketingOn()){
//            return StockMarketingUtils.isMarketingTime();
//        }
        return isMarketingOnTime(0l);
    }


    /**
     * 是否在开市时间段内
     * @return
     */
    public static Boolean isDuringMarketingTime(){
        return StockMarketingUtils.isDuringMarketingTime();
    }


}
