package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.AgreementAiOperateStrategyMapper;
import com.eliteams.quick4j.web.model.AgreementAiOperateStrategy;
import com.eliteams.quick4j.web.service.AgreementAiOperateStrategyService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AgreementAiOperateStrategyServiceImpl extends GenericServiceImpl implements AgreementAiOperateStrategyService {

    @Resource
    private AgreementAiOperateStrategyMapper agreementAiOperateStrategyMapper;


    @Override
    public GenericDao getDao() {
        return agreementAiOperateStrategyMapper;
    }

    @Override
    public Result addItem(AgreementAiOperateStrategy agreementAiOperateStrategy) {
        Result result = new Result("添加失败");
        AgreementAiOperateStrategy original = isExists(agreementAiOperateStrategy);
        if (original != null) {
            result.setResultFlag(false);
            result.setResultMsg("已绑定策略，请勿重复绑定");
        } else {
            if (agreementAiOperateStrategyMapper.insert(agreementAiOperateStrategy) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("添加成功");
            }

        }
        return result;
    }

    @Override
    public Result reStartStrategy(AgreementAiOperateStrategy agreementAiOperateStrategy) {
        Result result = new Result("重启失败");
        AgreementAiOperateStrategy original = isExists(agreementAiOperateStrategy);
        if (original != null) {
            original.setStatus(1);
            update(original);
            result.setResultFlag(true);
            result.setResultMsg("重启成功");
        } else {
            result.setResultFlag(false);
            result.setResultMsg("还未绑定策略，重启失败");
        }

        return result;
    }

    @Override
    public Result stopStrategy(AgreementAiOperateStrategy agreementAiOperateStrategy) {
        Result result = new Result("停用失败");
        AgreementAiOperateStrategy original = isExists(agreementAiOperateStrategy);
        if (original != null) {
            original.setStatus(0);
            update(original);
            result.setResultFlag(true);
            result.setResultMsg("停用成功");
        } else {
            result.setResultFlag(false);
            result.setResultMsg("还未绑定策略,停用失败");
        }
        return result;
    }


    AgreementAiOperateStrategy isExists(AgreementAiOperateStrategy agreementAiOperateStrategy) {
        return agreementAiOperateStrategyMapper.isExists(agreementAiOperateStrategy);
    }


}
