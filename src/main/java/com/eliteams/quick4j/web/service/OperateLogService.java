package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.OperateLog;
import com.eliteams.quick4j.web.model.StockOperate;
import com.eliteams.quick4j.web.model.pagerestrict.OperateLogRestrict;

/**
 * Created by Dsmart on 2017/1/9.
 */
public interface OperateLogService extends GenericService<OperateLog,Long> {




    /**
     * 添加数据
     * @param operateLog
     * @return
     */
    Result addItem(OperateLog operateLog);

    /**
     * 用户提交操盘申请
     * @param stockOperate
     * @return
     */
    Result addItemByUserOperateStockApply(StockOperate stockOperate);

    /**
     * 获取需要股市中处理的操盘委托
     * @param operateLogRestrict
     * @return
     */
    Page<OperateLog> listOperateLogByRestrictWithAdminId(OperateLogRestrict operateLogRestrict);

    /**
     * 获取操盘手被驳回的数据
     * @param operateLogRestrict
     * @return
     */
    Page<OperateLog> listRejectedByRestrictWithAdminId(OperateLogRestrict operateLogRestrict);


    /**
     * 获取新的待处理数据
     * @param operateLogRestrict
     * @return
     */
    Page<OperateLog> listRefreshByRestrictWithAdminId(OperateLogRestrict operateLogRestrict);


    /**
     * 股市操盘数据更新
     * @param operateLog
     * @return
     */
    Result updateByOperateStock(OperateLog operateLog);


    /**
     * 获取所有待审查的操盘处理数据
     * @param operateLogRestrict
     * @return
     */
    Page<OperateLog> listAllWaitingReviewByRestrict(OperateLogRestrict operateLogRestrict);


    /**
     * 审核通过股票交易操作
     * @param operateLog
     * @return
     */
    Result reviewAgreeOperateStock(OperateLog operateLog);


    /**
     * 审核驳回股票交易操作
     * @param operateLog
     * @return
     */
    Result reviewRejectOperateStock(OperateLog operateLog);


    /**
     * 获取所有被驳回还未处理的数据
     * @param operateLogRestrict
     * @return
     */
    Page<OperateLog> listAllRejectedByRestrict(OperateLogRestrict operateLogRestrict);


    /**
     * 确认回单信息
     * @param operateLog
     * @return
     */
    Result confirmOperateStock(OperateLog operateLog);


    /**
     * 根据回单号获取操作记录
     * @param operateLogRestrict
     * @return
     */
    Page<OperateLog> selectByStockOperateId(OperateLogRestrict operateLogRestrict);


    /**
     * 用户提交撤单申请
     * @param operateLog
     * @return
     */
    Boolean updateByCustomerCancel(OperateLog operateLog);


    /**
     * 确认用户撤单申请
     * @param operateLog
     * @return
     */
    Result confirmCustomerCancelApply(OperateLog operateLog);

    /**
     * 获取某操盘手下撤单的数据
     * @param operateLogRestrict
     * @return
     */
    Page<OperateLog> listCustomerCancelByRestrictWithAdminId(OperateLogRestrict operateLogRestrict);

    /**
     * 分页获取客户撤单数据列表
     * @param operateLogRestrict
     * @return
     */
    Page<OperateLog> listCustomerCancelByRestrict(OperateLogRestrict operateLogRestrict);


    /**
     * 根据交易号获取交易记录
     * @param operateId
     * @return
     */
    OperateLog selectByOperateId(Long operateId);


    /**
     * 是否存在超时的未报操盘申请
     *
     * @param maxDelaySeconds
     * @return
     */
    Boolean isExistsOverTimeStockOperateApply(Long maxDelaySeconds);


    /**
     * 是否存在超时的未确认交易
     *
     * @param maxDelaySeconds
     * @return
     */
    Boolean isExistsOverTimeStockConfirm(Long maxDelaySeconds);

}
