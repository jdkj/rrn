package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.UserIntegralMx;
import com.eliteams.quick4j.web.model.pagerestrict.UserIntegralMxRestrict;

/**
 * Created by Dsmart on 2016/12/7.
 */
public interface UserIntegralMxService extends GenericService<UserIntegralMx,Long> {


    /**
     * 系统奖励
     * @param userIntegralMx
     * @return
     */
    public Boolean systemGift(UserIntegralMx userIntegralMx);


    /**
     * 合约奖励
     * @param userIntegralMx
     * @return
     */
    public Boolean agreementReward(UserIntegralMx userIntegralMx);


    /**
     * 卡券兑换
     * @param userIntegralMx
     * @return
     */
    public Boolean couponExchange(UserIntegralMx userIntegralMx);


    /**
     * 根据请求参数获取积分列表
     * @param userIntegralMxRestrict
     * @return
     */
    public Page<UserIntegralMx> listByRestrict(UserIntegralMxRestrict userIntegralMxRestrict);




}
