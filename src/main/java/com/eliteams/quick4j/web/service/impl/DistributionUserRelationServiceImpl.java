package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.DistributionUserRelationMapper;
import com.eliteams.quick4j.web.model.DistributionUserRelation;
import com.eliteams.quick4j.web.service.DistributionUserRelationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2017/3/11.
 */
@Service
public class DistributionUserRelationServiceImpl extends GenericServiceImpl<DistributionUserRelation, Long> implements DistributionUserRelationService {

    @Resource
    private DistributionUserRelationMapper distributionUserRelationMapper;

    @Override
    public Result addItem(DistributionUserRelation distributionUserRelation) {
        Result result = new Result("数据添加失败");
        if (distributionUserRelationMapper.insert(distributionUserRelation) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("数据添加成功");
        }
        return result;
    }

    @Override
    public GenericDao<DistributionUserRelation, Long> getDao() {
        return distributionUserRelationMapper;
    }

    @Override
    public List<DistributionUserRelation> listAllByCustomerId(Long customerId) {
        return distributionUserRelationMapper.listAllByCustomerId(customerId);
    }
}
