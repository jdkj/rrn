package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.feature.cache.redis.RedisCache;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.StockDataLoadUtils;
import com.eliteams.quick4j.web.dao.StockMarketMapper;
import com.eliteams.quick4j.web.model.StockBase;
import com.eliteams.quick4j.web.model.StockMarket;
import com.eliteams.quick4j.web.service.StockBaseService;
import com.eliteams.quick4j.web.service.StockMarketService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/11/23.
 */
@Service
public class StockMarketServiceImpl extends GenericServiceImpl<StockMarket, Long> implements StockMarketService {

    @Resource
    private StockMarketMapper stockMarketMapper;

    @Resource
    private StockBaseService stockBaseService;


    @Override
    public GenericDao<StockMarket, Long> getDao() {
        return stockMarketMapper;
    }


    @Override
    public StockMarket stockInfo(String stockCode) {
        StockMarket stockMarket = null;
        //从缓存中获取
        Object obj = RedisCache.getObject(stockCode);
        if (obj != null) {
            stockMarket = (StockMarket) obj;
        }
        if (stockMarket == null) {
            StockBase stockBase = stockBaseService.query(stockCode);
            if (stockBase != null) {
                stockMarket = StockDataLoadUtils.loadSingleStock(stockBase.getStockMarket() + stockCode);
                if (stockMarket != null) {
                    //更新固化股票信息
//                        updateStockInfo(stockMarket);
                    //将股票信息添加到缓存
//                        StockMarketContainer.put(stockMarket);
                    RedisCache.putObject(stockMarket.getCode(), stockMarket);
                }
            }
        }
        return stockMarket;
    }


    @Override
    public List<StockMarket> listAll() {
        return stockMarketMapper.listAll();
    }


    public void updateStockInfo(StockMarket stockMarket) {
        StockMarket orgianlStockMarket = stockMarketMapper.selectByStockCode(stockMarket.getCode());
        if (orgianlStockMarket == null) {//全新的股票
            stockMarketMapper.insert(stockMarket);
        } else {//更新股票信息
            stockMarketMapper.updateStockInfo(stockMarket);
        }
    }


}
