package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.SMSUtils;
import com.eliteams.quick4j.web.dao.PhoneMsgMapper;
import com.eliteams.quick4j.web.model.PhoneMsg;
import com.eliteams.quick4j.web.service.PhoneMsgService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2017/3/30.
 */
@Service
public class PhoneMsgServiceImpl extends GenericServiceImpl<PhoneMsg, Long> implements PhoneMsgService {

    @Resource
    private PhoneMsgMapper phoneMsgMapper;


    @Override
    public Result addWarningLineMsg(Long userId, Long agreementId, String phone, String content) {
        Result result = new Result("发送失败");
        PhoneMsg phoneMsg = new PhoneMsg();
        phoneMsg.setAgreementId(agreementId);
        phoneMsg.setUserId(userId);
        phoneMsg.setPhone(phone);
        phoneMsg.setMsgLimit(1);
        phoneMsg.setMsgType(0);
        phoneMsg.setMsgContent(content);
        if (phoneMsgMapper.countBeforeInsert(phoneMsg) == 0) {
            phoneMsgMapper.insert(phoneMsg);
            SMSUtils.post(phone, content);
        } else {
            result.setResultFlag(false);
            result.setResultMsg("当天已经发送");
        }
        return result;
    }

    @Override
    public GenericDao<PhoneMsg, Long> getDao() {
        return phoneMsgMapper;
    }

    @Override
    public Result addBecomingEndOfAgreement(Long userId, Long agreementId, String phone, String content) {
        Result result = new Result("发送失败");
        PhoneMsg phoneMsg = new PhoneMsg();
        phoneMsg.setAgreementId(agreementId);
        phoneMsg.setUserId(userId);
        phoneMsg.setPhone(phone);
        phoneMsg.setMsgLimit(1);
        phoneMsg.setMsgType(1);
        phoneMsg.setMsgContent(content);
        if (phoneMsgMapper.countBeforeInsert(phoneMsg) == 0) {
            phoneMsgMapper.insert(phoneMsg);
            SMSUtils.post(phone, content);
        } else {
            result.setResultFlag(false);
            result.setResultMsg("当天已发送");
        }
        return result;
    }


    @Override
    public Result addLackOfInterestMsg(Long userId, String phone, String content) {
        Result result = new Result("发送失败");
        PhoneMsg phoneMsg = new PhoneMsg();
        phoneMsg.setMsgLimit(1);
        phoneMsg.setMsgContent(content);
        phoneMsg.setMsgType(4);
        phoneMsg.setUserId(userId);
        phoneMsg.setPhone(phone);
        phoneMsgMapper.insert(phoneMsg);
        SMSUtils.post(phone, content);
        result.setResultFlag(true);
        result.setResultMsg("发送成功");
        return result;
    }

    @Override
    public Result addStockOperateApplyDelayWarningMsg(String phone, String content) {
        Result result = new Result("发送失败");
        PhoneMsg phoneMsg = new PhoneMsg();

        phoneMsg.setMsgLimit(0);
        phoneMsg.setMsgType(3);
        phoneMsg.setMsgContent(content);
        phoneMsg.setPhone(phone);
        phoneMsgMapper.insert(phoneMsg);
        SMSUtils.post(phone, content);
        result.setResultFlag(true);
        result.setResultMsg("发送成功");

        return result;
    }

    @Override
    public Result addStockConfirmDelayWarningMsg(String phone, String content) {
        Result result = new Result("发送失败");
        PhoneMsg phoneMsg = new PhoneMsg();
        phoneMsg.setPhone(phone);
        phoneMsg.setMsgLimit(0);
        phoneMsg.setMsgType(4);
        phoneMsg.setMsgContent(content);

        phoneMsgMapper.insert(phoneMsg);
        SMSUtils.post(phone, content);
        result.setResultFlag(true);
        result.setResultMsg("发送成功");

        return result;
    }


    @Override
    public Result addAgreementOverMaxPeriod(Long userId, Long agreementId, String phone, String content) {
        Result result = new Result("发送失败");
        PhoneMsg phoneMsg = new PhoneMsg();
        phoneMsg.setMsgLimit(1);
        phoneMsg.setMsgType(5);
        phoneMsg.setUserId(userId);
        phoneMsg.setAgreementId(agreementId);
        phoneMsg.setMsgContent(content);
        phoneMsg.setPhone(phone);
        phoneMsgMapper.insert(phoneMsg);
        SMSUtils.post(phone, content);
        result.setResultFlag(true);
        result.setResultMsg("发送成功");


        return result;
    }


    @Override
    public Result addWithdrawalsApplySuccessMsg(Long userId, String phone, String content) {
        Result result = new Result("发送失败");
        PhoneMsg phoneMsg = new PhoneMsg();
        phoneMsg.setUserId(userId);
        phoneMsg.setMsgLimit(0);
        phoneMsg.setMsgType(6);
        phoneMsg.setMsgContent(content);
        phoneMsg.setPhone(phone);
        phoneMsgMapper.insert(phoneMsg);
        SMSUtils.post(phone, content);
        result.setResultFlag(true);
        result.setResultMsg("发送成功");

        return result;
    }


    @Override
    public Result addConfirmWithdrawalsApplySuccessMsg(Long userId, String phone, String content) {
        Result result = new Result("发送失败");
        PhoneMsg phoneMsg = new PhoneMsg();
        phoneMsg.setUserId(userId);
        phoneMsg.setMsgLimit(0);
        phoneMsg.setMsgType(7);
        phoneMsg.setMsgContent(content);
        phoneMsg.setPhone(phone);
        phoneMsgMapper.insert(phoneMsg);
        SMSUtils.post(phone, content);
        result.setResultFlag(true);
        result.setResultMsg("发送成功");
        return result;
    }
}
