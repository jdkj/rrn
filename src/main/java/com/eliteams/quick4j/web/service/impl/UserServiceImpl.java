package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.JSONResult;
import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.web.dao.UserMapper;
import com.eliteams.quick4j.web.model.*;
import com.eliteams.quick4j.web.service.*;
import com.thinkersoft.wechat.pay.common.WxLoadUserTools;
import com.thinkersoft.wechat.pay.common.WxOpenIdUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


/**
 * 用户Service实现类
 *
 * @author StarZou
 * @since 2014年7月5日 上午11:54:24
 */
@Service
public class UserServiceImpl extends GenericServiceImpl<User, Long> implements UserService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private UserAccountService userAccountService;

    @Resource
    private RechargeService rechargeService;

    @Resource
    private BankService bankService;


    @Resource
    private BankAccountService bankAccountService;

    @Resource
    private UserPlatService userPlatService;

    @Override
    public User authenticationByPhoneAndPassword(User user) {
        return userMapper.loginByPhoneAndPassword(user);
    }

    @Override
    public GenericDao<User, Long> getDao() {
        return userMapper;
    }

    @Override
    public User selectByPhone(String phone) {
        return userMapper.selectByPhone(phone);
    }


    @Override
    public Boolean registeByPhone(User user) {
        UserParamInfoTag userParamInfoTag = new UserParamInfoTag();
        userParamInfoTag.setPhoneTag(true);
        user.setUserParamInfoTags(userParamInfoTag);
        Boolean flag = userMapper.registeByPhone(user) > 0;
        UserAccount userAccount = new UserAccount();
        userAccount.setUserId(user.getUserId());
        userAccountService.addUserAccount(userAccount);


//        //测试阶段   注册后给予5000资金
//        Recharge recharge = new Recharge();
//        recharge.setUserId(user.getUserId());
//        recharge.setMoney(10000d);
//        rechargeService.recharge(recharge);
        return flag;
    }

    @Override
    public Boolean registeByPhoneAndPassword(User user) {
        UserParamInfoTag userParamInfoTag = new UserParamInfoTag();
        userParamInfoTag.setPhoneTag(true);
        userParamInfoTag.setPasswordTag(true);
        user.setUserParamInfoTags(userParamInfoTag);
        return userMapper.registeByPhoneAndPassword(user) > 0;
    }

    @Override
    public Boolean isPhoneRegisted(String phone) {
        return userMapper.isPhoneRegisted(phone) > 0;
    }

    @Override
    public Boolean updateUserPhone(User user) {
        UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();
        userParamInfoTag.setPhoneTag(true);
        user.setUserParamInfoTags(userParamInfoTag);
        return userMapper.updatePhone(user) > 0;
    }

    @Override
    public Boolean updatePassword(User user) {
        UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();
        userParamInfoTag.setPasswordTag(true);//手机号有设置
        user.setUserParamInfoTags(userParamInfoTag);
        return userMapper.updatePassword(user) > 0;
    }

    @Override
    public Boolean updateAccountPassword(User user) {
        UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();
        userParamInfoTag.setAccountPasswordTag(true);
        user.setUserParamInfoTags(userParamInfoTag);
        return userMapper.updateAccountPassword(user) > 0;
    }

    @Override
    public Boolean validatePhone(String phone) {
        return ApplicationUtils.validPhone(phone);
    }


    @Override
    public Boolean validateUserIdAndPassword(User user) {
        return userMapper.validateUserIdAndPassword(user) > 0;
    }

    @Override
    public Boolean validateUserIdAndAccountPassword(User user) {
        return userMapper.validateUserIdAndAccountPassword(user) > 0;
    }

    @Override
    public Boolean updateByIDAuthcated(Long userId) {
        User user = this.selectById(userId);
        UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();
        userParamInfoTag.setIdTag(true);
        user.setUserParamInfoTags(userParamInfoTag);
        return userMapper.updateParamInfo(user) > 0;
    }


    @Override
    public Result updateUserBankAccount(Long bankId, BankAccount bankAccount) {
        Result result = new Result("绑定失败");
        bankAccount.setBankName(bankService.selectById(bankId).getBankName());
        bankAccountService.addItem(bankAccount);
        User user = this.selectById(bankAccount.getUserId());
        UserParamInfoTag userParamInfoTag = user.getUserParamInfoTags();
        userParamInfoTag.setBankTag(true);
        user.setUserParamInfoTags(userParamInfoTag);
        userMapper.updateParamInfo(user);
        result.setResultFlag(true);
        result.setResultMsg("绑定成功");
        return result;
    }


    @Override
    public JSONResult<User> registedByWxJsChannel(String code) {
        JSONResult<User> result = new JSONResult<>();

        //获取微信accessToken
        WxJsAccessToken wxJsAccessToken = WxOpenIdUtils.jsAccessToken(code);

        UserPlat userPlat = WxLoadUserTools.loadWxUserInfoByJsChannel(wxJsAccessToken);
        UserPlat orginal = userPlatService.getWxUserPlatByOpenId(wxJsAccessToken.getOpenId());

        if (orginal == null) {

            if (userPlat != null) {

                User user = new User();
                user.setNickName(userPlat.getNickName());
                user.setUserImage(userPlat.getImgUrl());
                userMapper.registeByPlat(user);
                userPlat.setUserId(user.getUserId());
                userPlatService.addUserPlat(userPlat);
                user = this.selectById(user.getUserId());
                UserAccount userAccount = new UserAccount();
                userAccount.setUserId(user.getUserId());
                userAccountService.addUserAccount(userAccount);
                result.setResultFlag(true);
                result.setResultMsg("信息拉取成功");
                result.setData(user);
//
//            //测试阶段   注册后给予5000资金
//            Recharge recharge = new Recharge();
//            recharge.setUserId(user.getUserId());
//            recharge.setMoney(10000d);
//            rechargeService.recharge(recharge);


            } else {
                result.setResultFlag(false);
                result.setResultMsg("信息拉取失败");

            }

        } else {
            User user = userMapper.selectByPrimaryKey(orginal.getUserId());
            result.setResultFlag(true);
            result.setResultMsg("登录成功");
            result.setData(user);
        }

        return result;
    }

    @Override
    public User authenticationByWx(String code) {
        User user = null;
        //获取微信accessToken
        WxJsAccessToken wxJsAccessToken = WxOpenIdUtils.jsAccessToken(code);
        UserPlat userPlat = userPlatService.getWxUserPlatByOpenId(wxJsAccessToken.getOpenId());
        if (userPlat != null) {
            user = this.selectById(userPlat.getUserId());
        }
        return user;
    }

    @Override
    public Boolean mergeWxUserToOrginalUser(User wxUser, User orginalUser) {

        //更新用户平台信息
        UserPlat userPlat = userPlatService.getWxUserPlatByUserId(wxUser.getUserId());
        userPlat.setUserId(orginalUser.getUserId());
        userPlatService.updateUserPlatUserId(userPlat);

        //更新用户信息
        orginalUser.setUserImage(wxUser.getUserImage());
        userMapper.updateUserImage(orginalUser);

        //删除微信授权时创建的帐号
        this.delete(wxUser.getUserId());

        return false;

    }

}
