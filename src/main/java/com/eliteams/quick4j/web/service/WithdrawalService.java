package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.Withdrawal;
import com.eliteams.quick4j.web.model.pagerestrict.WithdrawalRestrict;

/**
 * 提现申请记录处理接口
 * Created by Dsmart on 2017/3/6.
 */
public interface WithdrawalService extends GenericService<Withdrawal, Long> {


    /**
     * 添加申请记录
     *
     * @param withdrawal
     * @return
     */
    Result addItem(Withdrawal withdrawal);


    /**
     * 根据用户编号获取提现申请列表数据
     *
     * @param withdrawalRestrict
     * @return
     */
    Page<Withdrawal> listByUserId(WithdrawalRestrict withdrawalRestrict);


}
