package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.web.dao.SecuritiesCompanyMoneyAccountDetailStocksMapper;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetailStocks;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailStocksRestrict;
import com.eliteams.quick4j.web.service.SecuritiesCompanyMoneyAccountDetailStocksService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2017/4/11.
 */
@Service
public class SecuritiesCompanyMoneyAccountDetailStocksServiceImpl extends GenericServiceImpl<SecuritiesCompanyMoneyAccountDetailStocks, Long> implements SecuritiesCompanyMoneyAccountDetailStocksService {

    @Resource
    private SecuritiesCompanyMoneyAccountDetailStocksMapper securitiesCompanyMoneyAccountDetailStocksMapper;

    @Override
    public GenericDao<SecuritiesCompanyMoneyAccountDetailStocks, Long> getDao() {
        return securitiesCompanyMoneyAccountDetailStocksMapper;
    }

    @Override
    public List<SecuritiesCompanyMoneyAccountDetailStocks> listByStockCode(String stockCode) {
        return securitiesCompanyMoneyAccountDetailStocksMapper.listAllByStockCode(stockCode);
    }

    @Override
    public List<SecuritiesCompanyMoneyAccountDetailStocks> listByGroupStockCode() {
        return securitiesCompanyMoneyAccountDetailStocksMapper.listAllByGroupStockCode();
    }

    @Override
    public Page<SecuritiesCompanyMoneyAccountDetailStocks> listByRestrict(SecuritiesCompanyMoneyAccountDetailStocksRestrict securitiesCompanyMoneyAccountDetailStocksRestrict) {
        Page<SecuritiesCompanyMoneyAccountDetailStocks> page = new Page<>(securitiesCompanyMoneyAccountDetailStocksRestrict.getPageno(), securitiesCompanyMoneyAccountDetailStocksRestrict.getPagesize());
        securitiesCompanyMoneyAccountDetailStocksMapper.listByRestrict(securitiesCompanyMoneyAccountDetailStocksRestrict, page);
        return page;
    }

    @Override
    public Result addOrUpdate(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks) {
        Result result = new Result("更新失败");

        //已持有该股票
        if (securitiesCompanyMoneyAccountDetailStocksMapper.countBeforeInsert(securitiesCompanyMoneyAccountDetailStocks) > 0) {
            //获取原持有股票数据
            SecuritiesCompanyMoneyAccountDetailStocks original = selectBySecuritiesAccountIdAndStockCode(securitiesCompanyMoneyAccountDetailStocks.getSecuritiesAccountId(), securitiesCompanyMoneyAccountDetailStocks.getStockCode());
            //重新计算购入价
            //更新总股数
            //更新总费用

            Long totalStockNum = original.getStockNum() + securitiesCompanyMoneyAccountDetailStocks.getStockNum();//总持股股数
            Double totalExpenses = original.getBuyTotalExpenses() + securitiesCompanyMoneyAccountDetailStocks.getBuyTotalExpenses();//购买股票总消耗
            Double buyPrice = CurrencyUtils.parseMoenyRoundHalfUp((totalExpenses / totalStockNum) + "");//重新计算后的

            original.setStockName(securitiesCompanyMoneyAccountDetailStocks.getStockName());
            original.setStockNum(totalStockNum);
            original.setBuyTotalExpenses(totalExpenses);
            original.setBuyPrice(buyPrice);
            securitiesCompanyMoneyAccountDetailStocksMapper.updateByPrimaryKeySelective(original);
            result.setResultFlag(true);
            result.setResultMsg("更新成功");

        } else {//未持有该股票

            securitiesCompanyMoneyAccountDetailStocksMapper.insert(securitiesCompanyMoneyAccountDetailStocks);
            result.setResultFlag(true);
            result.setResultMsg("更新成功");
        }

        //更新成功  需要同步更新券商账户中可用于购买股票的资金额度


        return result;
    }


    @Override
    public Result removeOrDelete(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks) {
        Result result = new Result("更新失败");
        //获取原持有股票数据
        SecuritiesCompanyMoneyAccountDetailStocks original = selectBySecuritiesAccountIdAndStockCode(securitiesCompanyMoneyAccountDetailStocks.getSecuritiesAccountId(), securitiesCompanyMoneyAccountDetailStocks.getStockCode());
        if (original != null) {

        } else {
            result.setResultFlag(false);
            result.setResultMsg("还未持有该股票");
        }
        return result;
    }

    @Override
    public Boolean updateByDividend(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks) {
        return securitiesCompanyMoneyAccountDetailStocksMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccountDetailStocks) > 0;
    }

    @Override
    public Boolean updateByRefresh(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks) {
        return securitiesCompanyMoneyAccountDetailStocksMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccountDetailStocks) > 0;
    }

    @Override
    public List<SecuritiesCompanyMoneyAccountDetailStocks> listAllBySecuritiesAccountId(Long securitiesAccountId) {
        return securitiesCompanyMoneyAccountDetailStocksMapper.listAllBySecuritiesAccountId(securitiesAccountId);
    }


    @Override
    public SecuritiesCompanyMoneyAccountDetailStocks selectBySecuritiesAccountIdAndStockCode(Long securitiesAccountId, String stockCode) {
        SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks = new SecuritiesCompanyMoneyAccountDetailStocks();
        securitiesCompanyMoneyAccountDetailStocks.setSecuritiesAccountId(securitiesAccountId);
        securitiesCompanyMoneyAccountDetailStocks.setStockCode(stockCode);
        return securitiesCompanyMoneyAccountDetailStocksMapper.selectBySecuritiesAccountIdAndStockCode(securitiesCompanyMoneyAccountDetailStocks);
    }
}
