package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.CouponStore;

import java.util.List;

/**
 * 优惠券接口
 * Created by Dsmart on 2016/12/7.
 */
public interface CouponStoreService extends GenericService<CouponStore,Long> {

    /**
     * 获取所有可兑换优惠券列表
     * @return
     */
    List<CouponStore> listAllExchangeableCouponStore();


}
