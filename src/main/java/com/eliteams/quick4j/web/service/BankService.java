package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.Bank;

import java.util.List;

/**
 * Created by Dsmart on 2016/11/5.
 */
public interface BankService extends GenericService<Bank,Long> {

    /**
     * 获取当前可以使用的银行列表
     * @return
     */
    List<Bank> listActivity();

}
