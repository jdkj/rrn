package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetailMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailMxRestrict;

/**
 * Created by Dsmart on 2017/3/22.
 */
public interface SecuritiesCompanyMoneyAccountDetailMxService extends GenericService<SecuritiesCompanyMoneyAccountDetailMx, Long> {

    /**
     * 合约生成锁定资金
     *
     * @param securitiesAccountId
     * @param orderId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Boolean addBySignAgreement(Long securitiesAccountId, Long orderId, Double totalMoney, Double changeMoney);


    /**
     * 合约解除释放资金
     *
     * @param securitiesAccountId
     * @param orderId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Boolean addByFreeAgreement(Long securitiesAccountId, Long orderId, Double totalMoney, Double changeMoney);


    /**
     * 撤出资金
     *
     * @param securitiesAccountId
     * @param orderId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Boolean addByWithdrawal(Long securitiesAccountId, Long orderId, Double totalMoney, Double changeMoney);


    /**
     * 追加资金
     *
     * @param securitiesAccountId
     * @param orderId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Boolean addByRecharge(Long securitiesAccountId, Long orderId, Double totalMoney, Double changeMoney);


    /**
     * 根据请求参数获取分页列表数据
     *
     * @param securitiesCompanyMoneyAccountDetailMxRestrict
     * @return
     */
    Page<SecuritiesCompanyMoneyAccountDetailMx> listByRestrict(SecuritiesCompanyMoneyAccountDetailMxRestrict securitiesCompanyMoneyAccountDetailMxRestrict);


}
