package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.AgreementAiOperateStrategy;

public interface AgreementAiOperateStrategyService extends GenericService {

    /**
     * 绑定策略
     *
     * @param agreementAiOperateStrategy
     * @return
     */
    Result addItem(AgreementAiOperateStrategy agreementAiOperateStrategy);


    /**
     * 重启策略
     *
     * @param agreementAiOperateStrategy
     * @return
     */
    Result reStartStrategy(AgreementAiOperateStrategy agreementAiOperateStrategy);


    /**
     * 停止策略
     *
     * @param agreementAiOperateStrategy
     * @return
     */
    Result stopStrategy(AgreementAiOperateStrategy agreementAiOperateStrategy);


}
