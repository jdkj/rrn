package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.DistributionDefaultCommission;

import java.util.List;
import java.util.Map;

/**
 * Created by Dsmart on 2017/3/11.
 */
public interface DistributionDefaultCommissionService extends GenericService<DistributionDefaultCommission, Long> {


    /**
     * 添加数据
     *
     * @param distributionDefaultCommission
     * @return
     */
    Result addItem(DistributionDefaultCommission distributionDefaultCommission);


    /**
     * 获取所有激活的销售等级数据
     *
     * @return
     */
    List<DistributionDefaultCommission> listAllActivity();


    /**
     * 获取所有激活的销售等级数据
     *
     * @return
     */
    Map<Integer, DistributionDefaultCommission> mapAllActivity();


    /**
     * 获取下属层级列表
     *
     * @param level
     * @return
     */
    List<DistributionDefaultCommission> listUnderLineLevel(Integer level);


    DistributionDefaultCommission getByLevel(Integer level);


}
