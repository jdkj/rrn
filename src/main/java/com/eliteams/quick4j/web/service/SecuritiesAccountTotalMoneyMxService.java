package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SecuritiesAccountTotalMoneyMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountTotalMoneyMxRestrict;

public interface SecuritiesAccountTotalMoneyMxService extends GenericService<SecuritiesAccountTotalMoneyMx, Long> {


    /**
     * 追加总资金
     *
     * @param securitiesAccountId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Result increaseTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney);


    /**
     * 撤出资金
     *
     * @param securitiesAccountId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Result reduceTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney);


    /**
     * 根据请求参数获取总资金变动列表数据
     *
     * @param securitiesAccountTotalMoneyMxRestrict
     * @return
     */
    Page<SecuritiesAccountTotalMoneyMx> listByRestrict(SecuritiesAccountTotalMoneyMxRestrict securitiesAccountTotalMoneyMxRestrict);


}
