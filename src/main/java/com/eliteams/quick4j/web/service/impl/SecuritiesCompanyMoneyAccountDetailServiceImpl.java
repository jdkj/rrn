package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.SecuritiesCompanyMoneyAccountDetailMapper;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetail;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailRestrict;
import com.eliteams.quick4j.web.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2017/3/22.
 */
@Service
public class SecuritiesCompanyMoneyAccountDetailServiceImpl extends GenericServiceImpl<SecuritiesCompanyMoneyAccountDetail, Long> implements SecuritiesCompanyMoneyAccountDetailService {

    @Resource
    private SecuritiesCompanyMoneyAccountDetailMapper securitiesCompanyMoneyAccountDetailMapper;

    @Resource
    private SecuritiesCompanyMoneyAccountService securitiesCompanyMoneyAccountService;

    @Resource
    private SecuritiesAccountTotalMoneyMxService securitiesAccountTotalMoneyMxService;

    @Resource
    private SecuritiesCompanyMoneyAccountDetailMxService securitiesCompanyMoneyAccountDetailMxService;

    @Resource
    private SecuritiesAccountAgreementMoneyMxService securitiesAccountAgreementMoneyMxService;

    @Resource
    private SecuritiesAccountGrossProfitService securitiesAccountGrossProfitService;

    @Resource
    private SecuritiesAccountAvailableMoneyMxService securitiesAccountAvailableMoneyMxService;

    @Override
    public GenericDao<SecuritiesCompanyMoneyAccountDetail, Long> getDao() {
        return securitiesCompanyMoneyAccountDetailMapper;
    }

    @Override
    public Result addItem(SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail) {
        Result result = new Result("添加失败");
        if (securitiesCompanyMoneyAccountDetailMapper.insert(securitiesCompanyMoneyAccountDetail) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("添加成功");
        }
        return result;
    }


    @Override
    public SecuritiesCompanyMoneyAccountDetail getMaxAgreementAvailableMoneyObject() {
        //目前暂时定位从实际券商账户中获取
        return securitiesCompanyMoneyAccountDetailMapper.getMaxAgreementAvailableMoneyFromReal();
    }

    @Override
    public SecuritiesCompanyMoneyAccountDetail getByPrimaryKey(Long securitiesAccountId) {
        return securitiesCompanyMoneyAccountDetailMapper.selectByPrimaryKey(securitiesAccountId);
    }

    @Override
    public Result changeAccountStatus(SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail) {
        Result result = new Result("更新失败");
        Integer status = securitiesCompanyMoneyAccountDetail.getStatus();
        //状态更新成功
        if (securitiesCompanyMoneyAccountDetailMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccountDetail) > 0) {
            securitiesCompanyMoneyAccountDetail = getByPrimaryKey(securitiesCompanyMoneyAccountDetail.getSecuritiesAccountId());
            if (status.compareTo(1) == 0) {//启用账户 总账户可用资金
                result = securitiesCompanyMoneyAccountService.increaseTotalMoney(securitiesCompanyMoneyAccountDetail.getAvailableMoney());
            } else if (status.compareTo(0) == 0) {//停用账户
                result = securitiesCompanyMoneyAccountService.reduceTotalMoney(securitiesCompanyMoneyAccountDetail.getAvailableMoney());
            }
        }

        return result;
    }

    @Override
    public Result increaseTotalMoney(Long securitiesAccountId, Double money) {
        Result result = new Result("追加资金失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = getByPrimaryKey(securitiesAccountId);
        securitiesCompanyMoneyAccountDetail.setTotalMoney(securitiesCompanyMoneyAccountDetail.getTotalMoney() + money);
        securitiesCompanyMoneyAccountDetail.setAvailableMoney(securitiesCompanyMoneyAccountDetail.getAvailableMoney() + money);
        //记录资金日志
        result = securitiesAccountTotalMoneyMxService.increaseTotalMoney(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getTotalMoney(), money);
        if (result.getResultFlag()) {
            if (securitiesCompanyMoneyAccountDetailMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccountDetail) > 0) {//更新账户数据
                securitiesCompanyMoneyAccountService.increaseTotalMoney(money);//更新总账户数据
                result.setResultFlag(true);
                result.setResultMsg("资金追加成功");
            }
        }


        return result;
    }

    @Override
    public Result reduceTotalMoney(Long securitiesAccountId, Double money) {
        Result result = new Result("撤出资金失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = getByPrimaryKey(securitiesAccountId);
        Double totalMoney = securitiesCompanyMoneyAccountDetail.getTotalMoney();
        Double availableMoney = securitiesCompanyMoneyAccountDetail.getAvailableMoney();
        if (totalMoney.compareTo(money) >= 0 && availableMoney.compareTo(money) >= 0) {
            securitiesCompanyMoneyAccountDetail.setTotalMoney(totalMoney - money);
            securitiesCompanyMoneyAccountDetail.setAvailableMoney(availableMoney - money);
            //记录资金日志
            result = securitiesAccountTotalMoneyMxService.reduceTotalMoney(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getTotalMoney(), money);
//            if (securitiesCompanyMoneyAccountDetailMxService.addByWithdrawal(securitiesAccountId, null, securitiesCompanyMoneyAccountDetail.getAvailableMoney(), money)) {
            if (result.getResultFlag()) {
                if (securitiesCompanyMoneyAccountDetailMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccountDetail) > 0) {//更新账户数据
                    securitiesCompanyMoneyAccountService.reduceTotalMoney(money);//更新总账户数据
                    result.setResultFlag(true);
                    result.setResultMsg("撤出资金成功");
                }
            }
        } else {
            result.setResultFlag(false);
            result.setResultMsg("可用资金不足,撤出失败");
        }
        return result;
    }

    @Override
    public Result increaseAgreementAvailableMoney(Long securitiesAccountId, Double money, Long agreementId) {
        Result result = new Result("资金释放失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = getByPrimaryKey(securitiesAccountId);
        securitiesCompanyMoneyAccountDetail.setAgreementAvailableMoney(securitiesCompanyMoneyAccountDetail.getAgreementAvailableMoney() + money);
        //记录资金日志
        result = securitiesAccountAgreementMoneyMxService.addBySignAgreement(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getAgreementAvailableMoney(), money, agreementId);
//        if (securitiesCompanyMoneyAccountDetailMxService.addByFreeAgreement(securitiesAccountId, agreementId, securitiesCompanyMoneyAccountDetail.getAvailableMoney(), money)) {
        if (result.getResultFlag()) {
            if (securitiesCompanyMoneyAccountDetailMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccountDetail) > 0) {//更新账户数据
                if (securitiesCompanyMoneyAccountDetail.getStatus().compareTo(1) == 0) {//该账户 为启用状态 则资金返回
                    securitiesCompanyMoneyAccountService.increaseAvailableMoney(money);//更新总账户数据
                }
                result.setResultFlag(true);
                result.setResultMsg("释放资金成功");
            }
        }

        return result;
    }

    @Override
    public Result reduceAgreementAvailableMoney(Long securitiesAccountId, Double money, Long agreementId) {
        Result result = new Result("资金锁定失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = getByPrimaryKey(securitiesAccountId);
        Double availableMoney = securitiesCompanyMoneyAccountDetail.getAgreementAvailableMoney();
        if (availableMoney.compareTo(money) >= 0) {
            securitiesCompanyMoneyAccountDetail.setAgreementAvailableMoney(securitiesCompanyMoneyAccountDetail.getAvailableMoney() - money);
            //记录资金日志
            result = securitiesAccountAgreementMoneyMxService.addByReleaseAgreement(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getAgreementAvailableMoney(), money, agreementId);
//            if (securitiesCompanyMoneyAccountDetailMxService.addBySignAgreement(securitiesAccountId, agreementId, securitiesCompanyMoneyAccountDetail.getAvailableMoney(), money)) {
            if (result.getResultFlag()) {
                if (securitiesCompanyMoneyAccountDetailMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccountDetail) > 0) {//更新账户数据
                    securitiesCompanyMoneyAccountService.reduceAvailableMoney(money);//更新总账户数据
                    result.setResultFlag(true);
                    result.setResultMsg("资金锁定成功");
                }
            }

        } else {
            result.setResultFlag(false);
            result.setResultMsg("可用资金不足");
        }
        return result;
    }


    @Override
    public Page<SecuritiesCompanyMoneyAccountDetail> listByRestrict(SecuritiesCompanyMoneyAccountDetailRestrict securitiesCompanyMoneyAccountDetailRestrict) {
        Page<SecuritiesCompanyMoneyAccountDetail> page = new Page<>(securitiesCompanyMoneyAccountDetailRestrict.getPageno(), securitiesCompanyMoneyAccountDetailRestrict.getPagesize());
        securitiesCompanyMoneyAccountDetailMapper.listByRestrict(securitiesCompanyMoneyAccountDetailRestrict, page);
        return page;
    }


    @Override
    public List<SecuritiesCompanyMoneyAccountDetail> listAll() {
        return securitiesCompanyMoneyAccountDetailMapper.listAll();
    }


    @Override
    public Result refreshStockValue(Long securitiesAccountId, Double stockValue) {
        Result result = new Result("更新失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = this.getByPrimaryKey(securitiesAccountId);
        securitiesCompanyMoneyAccountDetail.setStockValue(stockValue);
        if (this.update(securitiesCompanyMoneyAccountDetail) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("更新成功");
        }
        return result;
    }

    @Override
    public Result increaseProfitMoneyByStockOperate(Long securitiesAccountId, Double profitMoney, Long operateId, Long userId) {
        Result result = new Result("更新失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = this.getByPrimaryKey(securitiesAccountId);
        securitiesCompanyMoneyAccountDetail.setProfitMoney(securitiesCompanyMoneyAccountDetail.getProfitMoney() + profitMoney);
        securitiesCompanyMoneyAccountDetail.setHistoryProfitMoney(securitiesCompanyMoneyAccountDetail.getHistoryProfitMoney() + profitMoney);
        result = securitiesAccountGrossProfitService.addByOperateFeeIncrease(securitiesAccountId, userId, securitiesCompanyMoneyAccountDetail.getProfitMoney(), profitMoney, operateId);
        if (result.getResultFlag()) {
            if (update(securitiesCompanyMoneyAccountDetail) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("更新成功");
            }
        }
        return result;
    }

    @Override
    public Result increaseProfitMoneyByInterest(Long securitiesAccountId, Double profitMoney, Long agreementId, Long userId) {
        Result result = new Result("更新失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = this.getByPrimaryKey(securitiesAccountId);
        securitiesCompanyMoneyAccountDetail.setProfitMoney(securitiesCompanyMoneyAccountDetail.getProfitMoney() + profitMoney);
        securitiesCompanyMoneyAccountDetail.setHistoryProfitMoney(securitiesCompanyMoneyAccountDetail.getHistoryProfitMoney() + profitMoney);
        result = securitiesAccountGrossProfitService.addByManagementFeeIncrease(securitiesAccountId, userId, securitiesCompanyMoneyAccountDetail.getProfitMoney(), profitMoney, agreementId);
        if (result.getResultFlag()) {
            if (update(securitiesCompanyMoneyAccountDetail) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("更新成功");
            }
        }
        return result;
    }

    @Override
    public Result reduceProfitMoneyByWithdrawal(Long securitiesAccountId, Double withdrawalMoney) {
        Result result = new Result("提取利润失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = this.getByPrimaryKey(securitiesAccountId);
        Double profitMoney = securitiesCompanyMoneyAccountDetail.getProfitMoney();
        if (profitMoney.compareTo(withdrawalMoney) >= 0) {
            securitiesCompanyMoneyAccountDetail.setProfitMoney(profitMoney - withdrawalMoney);
            result = securitiesAccountGrossProfitService.addByWithdrawal(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getProfitMoney(), withdrawalMoney);
            if (result.getResultFlag()) {
                if (update(securitiesCompanyMoneyAccountDetail) > 0) {
                    result.setResultFlag(true);
                    result.setResultMsg("提取利润成功");
                }
            }
        } else {
            result.setResultFlag(false);
            result.setResultMsg("利润余额不足,最多可提取" + profitMoney + "元");
        }
        return result;
    }

    @Override
    public Result reduceAvailableMoneyByBuyStock(Long securitiesAccountId, Double money, Long operateId, Long userId) {
        Result result = new Result("记录失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = this.getByPrimaryKey(securitiesAccountId);
        securitiesCompanyMoneyAccountDetail.setAvailableMoney(securitiesCompanyMoneyAccountDetail.getAvailableMoney() - money);
        result = securitiesAccountAvailableMoneyMxService.addByBuyStock(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getAvailableMoney(), money, operateId, userId);
        if (result.getResultFlag()) {
            if (update(securitiesCompanyMoneyAccountDetail) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("记录成功");
            }
        }
        return result;
    }

    @Override
    public Result increaseAvailableMoneyBySellStock(Long securitiesAccountId, Double money, Long operateId, Long userId) {
        Result result = new Result("记录失败");
        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = this.getByPrimaryKey(securitiesAccountId);
        securitiesCompanyMoneyAccountDetail.setAvailableMoney(securitiesCompanyMoneyAccountDetail.getAvailableMoney() + money);
        result = securitiesAccountAvailableMoneyMxService.addBySellStock(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getAvailableMoney(), money, operateId, userId);
        if (result.getResultFlag()) {
            if (update(securitiesCompanyMoneyAccountDetail) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("记录成功");
            }
        }
        return result;
    }


    @Override
    public SecuritiesCompanyMoneyAccountDetail getMaxAvailableItemFromVisual() {
        return securitiesCompanyMoneyAccountDetailMapper.getMaxAvailableMoneyFromVisual();
    }
}
