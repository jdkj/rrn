package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetail;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailRestrict;

import java.util.List;

/**
 * Created by Dsmart on 2017/3/22.
 */
public interface SecuritiesCompanyMoneyAccountDetailService extends GenericService<SecuritiesCompanyMoneyAccountDetail, Long> {

    /**
     * 添加
     *
     * @param securitiesCompanyMoneyAccountDetail
     * @return
     */
    Result addItem(SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail);


    /**
     * 获取可用余额最多的资金账户
     *
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getMaxAgreementAvailableMoneyObject();


    /**
     * 获取账户信息
     *
     * @param securitiesAccountId
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getByPrimaryKey(Long securitiesAccountId);


    /**
     * 变更账户状态
     *
     * @param securitiesCompanyMoneyAccountDetail
     * @return
     */
    Result changeAccountStatus(SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail);


    /**
     * 追加资金
     *
     * @param securitiesAccountId
     * @param money
     * @return
     */
    Result increaseTotalMoney(Long securitiesAccountId, Double money);


    /**
     * 撤出资金
     *
     * @param securitiesAccountId
     * @param money
     * @return
     */
    Result reduceTotalMoney(Long securitiesAccountId, Double money);


    /**
     * 释放合约
     *
     * @param securitiesAccountId
     * @param money
     * @param agreementId
     * @return
     */
    Result increaseAgreementAvailableMoney(Long securitiesAccountId, Double money, Long agreementId);


    /**
     * 生成合约
     *
     * @param securitiesAccountId
     * @param money
     * @param agreementId
     * @return
     */
    Result reduceAgreementAvailableMoney(Long securitiesAccountId, Double money, Long agreementId);


    /**
     * 根据请求参数获取证券交易账户
     *
     * @param securitiesCompanyMoneyAccountDetailRestrict
     * @return
     */
    Page<SecuritiesCompanyMoneyAccountDetail> listByRestrict(SecuritiesCompanyMoneyAccountDetailRestrict securitiesCompanyMoneyAccountDetailRestrict);


    List<SecuritiesCompanyMoneyAccountDetail> listAll();


    /**
     * 更新券商账户持仓股票市值
     *
     * @param securitiesAccountId
     * @param stockValue
     * @return
     */
    Result refreshStockValue(Long securitiesAccountId, Double stockValue);


    /**
     * 更新 因股票交易带来的利润
     *
     * @param securitiesAccountId
     * @param profitMoney
     * @param operateId
     * @return
     */
    Result increaseProfitMoneyByStockOperate(Long securitiesAccountId, Double profitMoney, Long operateId, Long userId);


    /**
     * 更新 因缴付合约管理费带来的利润
     *
     * @param securitiesAccountId
     * @param profitMoney
     * @param agreementId
     * @return
     */
    Result increaseProfitMoneyByInterest(Long securitiesAccountId, Double profitMoney, Long agreementId, Long userId);


    /**
     * 利润提取
     *
     * @param securitiesAccountId
     * @param withdrawalMoney
     * @return
     */
    Result reduceProfitMoneyByWithdrawal(Long securitiesAccountId, Double withdrawalMoney);


    /**
     * 买入股票 减少券商可用余额
     *
     * @param securitiesAccountId
     * @param money
     * @param operateId
     * @param userId
     * @return
     */
    Result reduceAvailableMoneyByBuyStock(Long securitiesAccountId, Double money, Long operateId, Long userId);


    /**
     * 卖出股票增加券商可用余额
     *
     * @param securitiesAccountId
     * @param money
     * @param operateId
     * @param userId
     * @return
     */
    Result increaseAvailableMoneyBySellStock(Long securitiesAccountId, Double money, Long operateId, Long userId);


    /**
     * 从虚拟账户中获取一个用于购买股票资金最多的账户
     *
     * @return
     */
    SecuritiesCompanyMoneyAccountDetail getMaxAvailableItemFromVisual();


}
