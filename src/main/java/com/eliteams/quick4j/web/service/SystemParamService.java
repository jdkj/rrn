package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SystemParam;

import java.util.List;

/**
 * Created by Dsmart on 2016/11/29.
 */
public interface SystemParamService extends GenericService<SystemParam,Long> {

    /**
     * 添加系统参数
     * @param systemParam
     * @return
     */
    Result addItem(SystemParam systemParam);


    /**
     * 根据参数获取参数值
     * @param param
     * @return
     */
    SystemParam getByParam(String param);


    /**
     * 获取所有参数
     * @return
     */
    List<SystemParam> listAll();


    /**
     * 更新参数数据
     * @param systemParam
     * @return
     */
    Boolean updateItem(SystemParam systemParam);




}
