package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.SecuritiesAccountGrossProfitMapper;
import com.eliteams.quick4j.web.model.SecuritiesAccountGrossProfit;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountGrossProfitRestrict;
import com.eliteams.quick4j.web.service.SecuritiesAccountGrossProfitService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SecuritiesAccountGrossProfitServiceImpl extends GenericServiceImpl<SecuritiesAccountGrossProfit, Long> implements SecuritiesAccountGrossProfitService {

    @Resource
    private SecuritiesAccountGrossProfitMapper securitiesAccountGrossProfitMapper;

    @Override
    public GenericDao<SecuritiesAccountGrossProfit, Long> getDao() {
        return securitiesAccountGrossProfitMapper;
    }

    @Override
    public Result addByOperateFeeIncrease(Long securitiesAccountId, Long userId, Double totalProfitMoney, Double profitMoney, Long profitIssueId) {
        Result result = new Result("记录失败");
        SecuritiesAccountGrossProfit securitiesAccountGrossProfit = new SecuritiesAccountGrossProfit();
        securitiesAccountGrossProfit.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountGrossProfit.setUserId(userId);
        securitiesAccountGrossProfit.setProfitChangeType(1);
        securitiesAccountGrossProfit.setProfitType(1);
        securitiesAccountGrossProfit.setProfitIssueId(profitIssueId);
        securitiesAccountGrossProfit.setProfitMoney(profitMoney);
        securitiesAccountGrossProfit.setTotalProfitMoney(totalProfitMoney);
        if (securitiesAccountGrossProfitMapper.insert(securitiesAccountGrossProfit) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Result addByManagementFeeIncrease(Long securitiesAccountId, Long userId, Double totalProfitMoney, Double profitMoney, Long profitIssueId) {
        Result result = new Result("记录失败");
        SecuritiesAccountGrossProfit securitiesAccountGrossProfit = new SecuritiesAccountGrossProfit();
        securitiesAccountGrossProfit.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountGrossProfit.setUserId(userId);
        securitiesAccountGrossProfit.setProfitChangeType(1);
        securitiesAccountGrossProfit.setProfitType(0);
        securitiesAccountGrossProfit.setProfitIssueId(profitIssueId);
        securitiesAccountGrossProfit.setProfitMoney(profitMoney);
        securitiesAccountGrossProfit.setTotalProfitMoney(totalProfitMoney);
        if (securitiesAccountGrossProfitMapper.insert(securitiesAccountGrossProfit) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Result addByWithdrawal(Long securitiesAccountId, Double totalProfitMoney, Double profitMoney) {
        Result result = new Result("记录失败");
        SecuritiesAccountGrossProfit securitiesAccountGrossProfit = new SecuritiesAccountGrossProfit();
        securitiesAccountGrossProfit.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountGrossProfit.setTotalProfitMoney(totalProfitMoney);
        securitiesAccountGrossProfit.setProfitMoney(profitMoney);
        securitiesAccountGrossProfit.setProfitChangeType(0);
        if (securitiesAccountGrossProfitMapper.insert(securitiesAccountGrossProfit) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Page<SecuritiesAccountGrossProfit> listByRestrict(SecuritiesAccountGrossProfitRestrict securitiesAccountGrossProfitRestrict) {
        Page<SecuritiesAccountGrossProfit> page = new Page<>(securitiesAccountGrossProfitRestrict.getPageno(), securitiesAccountGrossProfitRestrict.getPagesize());
        securitiesAccountGrossProfitMapper.listByRestrict(securitiesAccountGrossProfitRestrict, page);
        return page;
    }
}
