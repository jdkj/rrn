package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.DistributionUserMapper;
import com.eliteams.quick4j.web.model.DistributionAccount;
import com.eliteams.quick4j.web.model.DistributionDefaultCommission;
import com.eliteams.quick4j.web.model.DistributionUser;
import com.eliteams.quick4j.web.model.DistributionUserRelation;
import com.eliteams.quick4j.web.model.pagerestrict.DistributionUserRestrict;
import com.eliteams.quick4j.web.service.DistributionAccountService;
import com.eliteams.quick4j.web.service.DistributionDefaultCommissionService;
import com.eliteams.quick4j.web.service.DistributionUserRelationService;
import com.eliteams.quick4j.web.service.DistributionUserService;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class DistributionUserServiceImpl extends GenericServiceImpl<DistributionUser, Long> implements DistributionUserService {

    @Resource
    private DistributionUserMapper distributionUserMapper;

    @Resource
    private DistributionAccountService distributionAccountService;

    @Resource
    private DistributionUserRelationService distributionUserRelationService;

    @Resource
    private DistributionDefaultCommissionService distributionDefaultCommissionService;

    @Override
    public DistributionUser authentication(DistributionUserRestrict distributionUserRestrict) {

        return distributionUserMapper.authentication(distributionUserRestrict);

    }

    @Override
    public DistributionUser authentication(DistributionUser distributionUser) {
        DistributionUserRestrict distributionUserRestrict = new DistributionUserRestrict();
        distributionUserRestrict.setAccountNum(distributionUser.getAccountNum());
        distributionUserRestrict.setAccountPassword(distributionUser.getAccountPassword());
        return authentication(distributionUserRestrict);
    }

    @Override
    public DistributionUser selectDistributionUserByAccountNum(Long accountNum) {

        return distributionUserMapper.selectByAccountNum(accountNum);
    }

    @Override
    public GenericDao<DistributionUser, Long> getDao() {

        return distributionUserMapper;
    }

    @Override
    public DistributionUser upAccountPasswordById(DistributionUserRestrict distributionUserRestrict) {

        DistributionUser resultDistributionUser = null;
        //验证原始密码是否正确
        DistributionUser distributionUser = authentication(distributionUserRestrict);
        if (distributionUser != null) {
            //不为空旧密码正确
            //修改密码
            //传入新密码
            String newAccountPassword = distributionUserRestrict.getNewAccountPassword();
            distributionUser.setAccountPassword(newAccountPassword);
            if (distributionUserMapper.upAccountPasswordById(distributionUser) > 0) {
                resultDistributionUser = distributionUser;
            }

        }

        return resultDistributionUser;
    }

    @Override
    public Boolean updateByPrimaryKey(DistributionUser distributionUser) {
        return distributionUserMapper.upAccountInfoById(distributionUser) > 0;
    }

    @Override
    public DistributionUser selectDistributionUserById(Long id) {

        return distributionUserMapper.selectById(id);
    }

    @Override
    public DistributionUser selectByUserId(Long userId) {
        return distributionUserMapper.selectByUserId(userId);
    }

    @Override
    public Result addItem(DistributionUser distributionUser) {
        Result result = new Result("添加失败");

        //获取
        Long parentId = distributionUser.getParentId();

        DistributionUser parentUser = selectDistributionUserById(parentId);
        if (parentUser != null) {
            //非分销体系内成员推广发展的关系  全部数据总公司
            if (parentUser.getAccountLevel().compareTo(-1) == 0) {
                distributionUser.setParentId(1l);
                distributionUser.setRelationPath("0_1");
            } else {
                distributionUser.setRelationPath(parentUser.getRelationPath());
            }
        } else {//自由散客 属于总公司
            distributionUser.setParentId(1l);
            distributionUser.setRelationPath("0_1");
        }
        distributionUserMapper.insert(distributionUser);
        distributionUser.setAccountNum(distributionUser.getDistributionUserId() + 10000000l);
        distributionUser.setRelationPath(distributionUser.getRelationPath() + "_" + distributionUser.getDistributionUserId());
        distributionUserMapper.upAccountInfoById(distributionUser);

        //创建分销体系账户
        DistributionAccount distributionAccount = new DistributionAccount();
        distributionAccount.setDistributionUserId(distributionUser.getDistributionUserId());
        distributionAccountService.addItem(distributionAccount);


        //添加分销分佣比率关系
        String relationPath = distributionUser.getRelationPath();
        String ids[] = relationPath.split("_");

        Long keys[] = new Long[ids.length - 2];
        for (int i = 1; i < ids.length - 1; i++) {
            keys[i - 1] = Long.parseLong(ids[i]);
        }
        ArrayUtils.reverse(keys);
        Double tagCommissionProportion = 0d;
        DistributionUserRelation distributionUserRelation = null;
        //每一个上级的分销比率为其分销比率减去其下属一级分销比率的值
        for (int j = 0; j < keys.length; j++) {
            parentUser = selectDistributionUserById(keys[j]);
            Double commissionProportion = parentUser.getCommissionProportion();
            distributionUserRelation = new DistributionUserRelation();
            distributionUserRelation.setDistributionUserId(parentUser.getDistributionUserId());
            distributionUserRelation.setCustomerId(distributionUser.getDistributionUserId());
            distributionUserRelation.setCommissionProportion(parentUser.getCommissionProportion() - tagCommissionProportion);
            distributionUserRelationService.addItem(distributionUserRelation);
            tagCommissionProportion = commissionProportion;
        }

        result.setResultFlag(true);
        result.setResultMsg("添加成功");
        return result;
    }


    @Override
    public Result addDistributionRelation(Long userId, Long distributionId) {
        DistributionUser distributionUser = new DistributionUser();
        distributionUser.setUserId(userId);
        distributionUser.setParentId(distributionId);
        return addItem(distributionUser);
    }


    @Override
    public Page<DistributionUser> listByRestrict(DistributionUserRestrict distributionUserRestrict) {
        Page<DistributionUser> page = new Page<>(distributionUserRestrict.getPageno(), distributionUserRestrict.getPagesize());
        distributionUserMapper.listByRestrict(distributionUserRestrict, page);
        return page;
    }

    @Override
    public Result changeUserCommissionLevel(Long distributionUserId, Integer commissionLevel) {
        Result result = new Result();
        DistributionUser distributionUser = selectDistributionUserById(distributionUserId);
        Integer orginalLevel = distributionUser.getAccountLevel();
        if (orginalLevel.compareTo(commissionLevel) == 0) {
            result.setResultFlag(true);
            result.setResultMsg("修改成功");
            return result;
        }
        //升级为代理
        if (commissionLevel.compareTo(1) == 0) {
            distributionUser.setRelationPath("0_1_" + distributionUser.getDistributionUserId());
        }


        distributionUser.setAccountLevel(commissionLevel);

        DistributionDefaultCommission distributionDefaultCommission = distributionDefaultCommissionService.getByLevel(commissionLevel);
        distributionUser.setCommissionProportion(distributionDefaultCommission.getCommissionProportion());

        if (updateByPrimaryKey(distributionUser)) {
            result.setResultFlag(true);
            result.setResultMsg("修改成功");
        }
        return result;
    }
}
