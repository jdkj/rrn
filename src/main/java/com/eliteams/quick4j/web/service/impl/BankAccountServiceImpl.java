package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.BankAccountMapper;
import com.eliteams.quick4j.web.model.BankAccount;
import com.eliteams.quick4j.web.service.BankAccountService;
import com.eliteams.quick4j.web.service.BankService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/11/5.
 */
@Service
public class BankAccountServiceImpl extends GenericServiceImpl<BankAccount,Long> implements BankAccountService {

    @Resource
    private BankAccountMapper bankAccountMapper;

    @Override
    public Boolean addItem(BankAccount bankAccount) {
        return bankAccountMapper.insert(bankAccount)>0;
    }

    @Override
    public GenericDao<BankAccount, Long> getDao() {
        return bankAccountMapper;
    }

    @Override
    public BankAccount selectByUserId(Long userId) {
        return bankAccountMapper.selectByUserId(userId);
    }
}
