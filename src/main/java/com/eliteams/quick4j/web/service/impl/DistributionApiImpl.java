package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.web.model.DistributionUser;
import com.eliteams.quick4j.web.model.DistributionUserRelation;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2017/3/13.
 */
@Service
public class DistributionApiImpl implements DistributionApi {
    @Resource
    private DistributionUserService distributionUserService;

    @Resource
    private DistributionUserRelationService distributionUserRelationService;

    @Resource
    private DistributionCommissionMxService distributionCommissionMxService;

    @Resource
    private UserService userService;


    @Override
    public Result bindDistributionRelation(Long userId, Long distributionId) {
        return distributionUserService.addDistributionRelation(userId, distributionId);
    }

    @Override
    public Result shareCommissionByAgreementInterest(Long userId, Long agreementId, Double money) {
        Result result = new Result();
        DistributionUser distributionUser = distributionUserService.selectByUserId(userId);
        if (distributionUser != null) {
            DistributionUser parentUser = distributionUserService.selectDistributionUserById(distributionUser.getParentId());
            List<DistributionUserRelation> distributionUserRelationList = distributionUserRelationService.listAllByCustomerId(distributionUser.getDistributionUserId());
            for (DistributionUserRelation distributionUserRelation : distributionUserRelationList) {
                distributionCommissionMxService.addByInterestCommission(distributionUserRelation.getDistributionUserId(), agreementId, money, distributionUserRelation.getCommissionProportion(), distributionUserRelation.getCustomerId(), parentUser.getDistributionUserId(), parentUser.getAccountLevel());
            }
            distributionCommissionMxService.increaseContributionCommissionMoney(distributionUser.getDistributionUserId(), money, agreementId);

            result.setResultFlag(true);
            result.setResultMsg("分佣成功");
        } else {
            result.setResultFlag(false);
            result.setResultMsg("不存在于分销体系中");
        }
        return result;
    }

    @Override
    public Result shareCommissionByStockOperate(Long userId, Long operateId, Double money) {
        Result result = new Result();
        DistributionUser distributionUser = distributionUserService.selectByUserId(userId);
        if (distributionUser != null) {


            DistributionUser parentUser = distributionUserService.selectDistributionUserById(distributionUser.getParentId());
            List<DistributionUserRelation> distributionUserRelationList = distributionUserRelationService.listAllByCustomerId(distributionUser.getDistributionUserId());
            for (DistributionUserRelation distributionUserRelation : distributionUserRelationList) {
                distributionCommissionMxService.addByOperateFeeCommission(distributionUserRelation.getDistributionUserId(), operateId, money, distributionUserRelation.getCommissionProportion(), distributionUserRelation.getCustomerId(), parentUser.getDistributionUserId(), parentUser.getAccountLevel());
            }
            distributionCommissionMxService.increaseContributionCommissionMoney(distributionUser.getDistributionUserId(), money, operateId);
            result.setResultFlag(true);
            result.setResultMsg("分佣成功");
        } else {
            result.setResultFlag(false);
            result.setResultMsg("不存在于分销体系中");
        }
        return result;
    }

    @Override
    public DistributionUser getByUserId(Long userId) {
        return distributionUserService.selectByUserId(userId);
    }

    @Override
    public Boolean updateByUser(User user) {
        user = userService.selectById(user.getUserId());
        DistributionUser distributionUser = getByUserId(user.getUserId());
        if (user.getNickName() != null && (distributionUser.getAccountName() == null || distributionUser.getAccountName().isEmpty())) {
            distributionUser.setAccountName(user.getNickName());
        }
        if (user.getPhone() != null) {
            distributionUser.setAccountPhone(user.getPhone());
        }
        return distributionUserService.updateByPrimaryKey(distributionUser);
    }
}
