package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.UserAccountMx;
import com.eliteams.quick4j.web.model.pagerestrict.UserAccountMxRestrict;

/**
 * Created by Dsmart on 2016/10/26.
 */
public interface UserAccountMxService extends GenericService<UserAccountMx,Long> {

    /**
     * 充值存入
     * @param userAccountMx
     * @return
     */
    public Boolean recharge(UserAccountMx userAccountMx);


    /**
     * 提款取出
     * @param userAccountMx
     * @return
     */
    public Boolean withdrawals(UserAccountMx userAccountMx);


    /**
     * 操盘支出
     * 签订合约锁定资金
     * @param userAccountMx
     * @return
     */
    public Boolean signAgreementLockMoney(UserAccountMx userAccountMx);


    /**
     * 退保证金
     * @param userAccountMx
     * @return
     */
    public Boolean relaseLockMoney(UserAccountMx userAccountMx);


    /**
     * 加保证金
     * @param userAccountMx
     * @return
     */
    public Boolean increaseLockMoney(UserAccountMx userAccountMx);


    /**
     * 利息支出
     * @param userAccountMx
     * @return
     */
    public Boolean interestPayment(UserAccountMx userAccountMx);


    /**
     * 利息支出---优惠券抵扣
     * @param userAccountMx
     * @return
     */
    public Boolean interestPaymentByCoupon(UserAccountMx userAccountMx);


    /**
     * 利润提取
     * @param userAccountMx
     * @return
     */
    public Boolean profitWithdrawals(UserAccountMx userAccountMx);







    /**
     * 根据请求参数获取资金明细列表
     * @param userAccountMxRestrict
     * @return
     */
    public Page<UserAccountMx> listByRestrict(UserAccountMxRestrict userAccountMxRestrict);

}
