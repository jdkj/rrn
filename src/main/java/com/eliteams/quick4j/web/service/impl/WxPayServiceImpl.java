/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.JSONResult;
import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.util.HttpRequestClient;
import com.eliteams.quick4j.core.util.IPUtils;
import com.eliteams.quick4j.core.util.PrimaryKeyModifyUtils;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.Recharge;
import com.eliteams.quick4j.web.model.User;
import com.eliteams.quick4j.web.model.UserPlat;
import com.eliteams.quick4j.web.model.WxPayPo;
import com.eliteams.quick4j.web.model.pagerestrict.WxPayRestrict;
import com.eliteams.quick4j.web.service.*;
import com.thinkersoft.wechat.pay.common.RandomStringGenerator;
import com.thinkersoft.wechat.pay.common.Signature;
import com.thinkersoft.wechat.pay.common.WxPayXMLParser;
import com.thinkersoft.wechat.pay.configure.ServerConfigure;
import com.thinkersoft.wechat.pay.configure.WxPayConfigurePo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-7-15, 15:17:12
 */
@Service
public class WxPayServiceImpl implements WxPayService {

    @Resource
    private UserService userService;

    @Resource
    private UserPlatService userPlatService;

    @Resource
    private RechargeService rechargeService;

    @Resource
    private UserAccountService userAccountService;


    @Override
    public JSONResult jsAPIWxPay(WxPayRestrict wxPayRestrict) {

        return jsAPIWxPayPo(wxPayRestrict);

    }

    /**
     * 获取调取微信页面端支付的数据
     *
     * @param wxPayRestrict
     * @return
     */
    public JSONResult jsAPIWxPayPo(WxPayRestrict wxPayRestrict) {

        JSONResult json = new JSONResult();
        json.setResultFlag(false);
        json.setResultMsg("获取失败");
        Result feedbackPo = new Result();

        if (wxPayRestrict != null) {

            feedbackPo = getJSAPIPrepayId(wxPayRestrict);

            if (feedbackPo.getResultFlag()) {

                String orderId = wxPayRestrict.getOrderId();

                Map<String, Object> map = new HashMap<String, Object>();

                String appid = WxPayConfigurePo.APPID;

                map.put("appId", appid);

                map.put("timeStamp", System.currentTimeMillis() / 1000 + "");

                map.put("nonceStr", RandomStringGenerator.getRandomStringByLength(32));

                map.put("package", "prepay_id=" + feedbackPo.getResultMsg());

                map.put("signType", "MD5");

                String sign = Signature.getSign(map, WxPayConfigurePo.KEY);

                map.put("paySign", sign);

                //订单号传送到页面端
                map.put("id", orderId);

                json.setResultFlag(true);

                json.setResultMsg("获取成功");

                json.setData(map);

            } else {
                json.setResultFlag(false);
                json.setResultMsg("获取失败");
            }

        }

        return json;

    }

    /**
     * 获取微信页面jsapi支付使用的prepayid
     *
     * @param wxPayRestrict
     * @return
     */
    protected Result getJSAPIPrepayId(WxPayRestrict wxPayRestrict) {

        Result feedbackPo = new Result();

        User user = wxPayRestrict.getUser();

        //微信授权登录账户信息
        UserPlat userPlat = userPlatService.getWxUserPlatByUserId(user.getUserId());

        String openId =userPlat.getOpenId();

        //外显订单号
        String showOrderId = wxPayRestrict.getOrderId();

        Long orderId = PrimaryKeyModifyUtils.decodePrimaryKey(showOrderId);

        Recharge recharge = rechargeService.selectById(orderId);

        //该支付订单存在
        if(recharge!=null){
            //该订单未完成支付
            if(recharge.getStatus()==0){
                int total_fee = (int)(recharge.getMoney()*100);
                String body = recharge.getRechargeDesc();
                //微信支付对象
                WxPayPo wxPayPo = new WxPayPo();

                wxPayPo.setOpenId(openId);

                wxPayPo.setAppId(WxPayConfigurePo.APPID);

                wxPayPo.setOut_trade_no(wxPayRestrict.getOrderId());

                wxPayPo.setMch_id(WxPayConfigurePo.MERCHANT_ID);

                wxPayPo.setPrivateKey(WxPayConfigurePo.KEY);

                wxPayPo.setAttach(body);

                wxPayPo.setBody(body);

                wxPayPo.setSpbill_create_ip(IPUtils.getIpAddr(wxPayRestrict.getRequest()));

                wxPayPo.setTotal_fee(total_fee);

                wxPayPo.setSign(Signature.getSign(wxPayPo.createUnifiedOrderReqOriginalDataForSign(), WxPayConfigurePo.KEY));

                //返回的数据
                String res = HttpRequestClient.sendPost(ServerConfigure.UNIFIED_ORDER, wxPayPo.createUnifiedOrderReqData());

                feedbackPo = WxPayXMLParser.getPrepayIdFromXml(res);
            }else{
                feedbackPo.setResponseStatusEnums(ResponseStatusEnums.ORDER_STATUS_ILLEGAL);
            }
        }else{
            feedbackPo.setResponseStatusEnums(ResponseStatusEnums.ORDER_IS_NOT_EXIST);
        }
        return feedbackPo;

    }


    @Override
    public Result scanWxPay(WxPayRestrict wxPayRestrict) {
        return getNATIVECodeUrl(wxPayRestrict);
    }

    /**
     * 获取扫码支付需要的二维码地址
     * @param wxPayRestrict
     * @return
     */
    protected Result getNATIVECodeUrl(WxPayRestrict wxPayRestrict) {

        Result feedbackPo = new Result();

        //外显订单号
        String showOrderId = wxPayRestrict.getOrderId();

        Long orderId = PrimaryKeyModifyUtils.decodePrimaryKey(showOrderId);

        Recharge recharge = rechargeService.selectById(orderId);

        //该支付订单存在
        if(recharge!=null){
            //该订单未完成支付
            if(recharge.getStatus()==0){
                int total_fee = (int)(recharge.getMoney()*100);
                String body = recharge.getRechargeDesc();
                //微信支付对象
                WxPayPo wxPayPo = new WxPayPo();

                wxPayPo.setTrade_type("NATIVE");

                wxPayPo.setAppId(WxPayConfigurePo.APPID);

                wxPayPo.setOut_trade_no(wxPayRestrict.getOrderId());

                wxPayPo.setMch_id(WxPayConfigurePo.MERCHANT_ID);

                wxPayPo.setPrivateKey(WxPayConfigurePo.KEY);

                wxPayPo.setAttach(body);

                wxPayPo.setBody(body);
//                wxPayPo.setSpbill_create_ip("110.81.169.253");
                wxPayPo.setSpbill_create_ip(IPUtils.getIpAddr(wxPayRestrict.getRequest()));

                wxPayPo.setTotal_fee(total_fee);

                wxPayPo.setSign(Signature.getSign(wxPayPo.createUnifiedOrderReqOriginalDataForSign(), WxPayConfigurePo.KEY));

                //返回的数据
                String res = HttpRequestClient.sendPost(ServerConfigure.UNIFIED_ORDER, wxPayPo.createUnifiedOrderReqData());

                feedbackPo = WxPayXMLParser.getCodeUrlFromXml(res);
            }else{
                feedbackPo.setResponseStatusEnums(ResponseStatusEnums.ORDER_STATUS_ILLEGAL);
            }
        }else{
            feedbackPo.setResponseStatusEnums(ResponseStatusEnums.ORDER_IS_NOT_EXIST);
        }
        return feedbackPo;

    }

    @Override
    public JSONResult orderQuery(WxPayRestrict wxPayRestrict) {

        return orderQueryPo(wxPayRestrict);

    }

    public JSONResult orderQueryPo(WxPayRestrict wxPayRestrict) {

        JSONResult feedbackPo = new JSONResult();

        if (wxPayRestrict != null) {

            //外显订单号
            String showOrderId = wxPayRestrict.getOrderId();

            Long orderId = PrimaryKeyModifyUtils.decodePrimaryKey(showOrderId);

            Recharge recharge = rechargeService.selectById(orderId);

            //该支付订单存在
            if(recharge!=null){
                //该订单未完成支付
                if(recharge.getStatus()==0){//查询检验
                    WxPayPo wxPayPo = new WxPayPo();

                    wxPayPo.setAppId(WxPayConfigurePo.APPID);

                    wxPayPo.setMch_id(WxPayConfigurePo.MERCHANT_ID);

                    wxPayPo.setOut_trade_no(showOrderId + "");

                    wxPayPo.setSign(Signature.getSign(wxPayPo.createOrderQueryReqOrginalDataForSign(), WxPayConfigurePo.KEY));

                    String res = HttpRequestClient.sendPost(ServerConfigure.ORDER_QUERY, wxPayPo.createOrderQueryReqData());

                    Result result = WxPayXMLParser.getPayStatusFromQueryOrderRspXml(res);

                    //订单交易成功
                    if (result.getResultFlag()) {
                        //查询返回的交易金额
                        int rmoney = (int) (Double.parseDouble(result.getResultMsg()));

                        int money = (int)(recharge.getMoney()*100);

                        //支付金额无误
                        if (money == rmoney) {
                            //订单状态更新
                            rechargeService.updateAfterPaySuc(recharge.getRechargeId());
                            //资金到达用户账户
                            userAccountService.recharge(recharge.getUserId(), recharge.getMoney(), orderId);
                            feedbackPo.setResultFlag(true);
                            feedbackPo.setResultMsg(showOrderId);
                        } else {
                            feedbackPo.setResultFlag(false);
                            feedbackPo.setResultMsg("交易金额不一致");
                        }

                    }else{
                        feedbackPo.setResultFlag(false);
                        feedbackPo.setResultMsg(result.getResultMsg());
                    }
                }else if(recharge.getStatus()==1){//订单已经支付
                    feedbackPo.setResultFlag(true);
                    feedbackPo.setResultMsg(showOrderId);
                }else{
                    feedbackPo.setResponseStatusEnums(ResponseStatusEnums.ORDER_STATUS_ILLEGAL);
                }
            }else{
                feedbackPo.setResponseStatusEnums(ResponseStatusEnums.ORDER_IS_NOT_EXIST);
            }
        }
        return feedbackPo;
    }

}
