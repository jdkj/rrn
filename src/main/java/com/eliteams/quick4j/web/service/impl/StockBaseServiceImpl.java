package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.StockBaseMapper;
import com.eliteams.quick4j.web.model.StockBase;
import com.eliteams.quick4j.web.model.pagerestrict.StockBaseRestrict;
import com.eliteams.quick4j.web.service.StockBaseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2017/2/7.
 */
@Service
public class StockBaseServiceImpl extends GenericServiceImpl<StockBase, Long> implements StockBaseService {


    @Resource
    private StockBaseMapper stockBaseMapper;

    @Override
    public Result addItem(StockBase stockBase) {
        Result result = new Result("添加失败");
        if (!countBeforeInsert(stockBase)) {
            if (stockBaseMapper.insert(stockBase) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("添加成功");
//                StockBaseContainer.put(stockBase);
            }
        } else {
            this.update(stockBase);
            result.setResultFlag(false);
            result.setResultMsg("该数据已经存在，请勿重复添加");
        }
        return result;
    }

    @Override
    public GenericDao<StockBase, Long> getDao() {
        return stockBaseMapper;
    }

    /**
     * 插入前检测是否已经存在
     *
     * @param stockBase
     * @return
     */
    Boolean countBeforeInsert(StockBase stockBase) {
        return stockBaseMapper.countBeforeInsert(stockBase) > 0;
    }


    @Override
    public List<StockBase> listAll() {
        List<StockBase> list = null;
        //缓存获取
//        list = StockBaseContainer.getAll();
        if (list == null || list.isEmpty()) {
            list = stockBaseMapper.listAll();
        }
        return list;
    }


    @Override
    public List<StockBase> queryByFlowValueAndPe(Double minFlowValue, Double maxPe) {
        StockBaseRestrict stockBaseRestrict = new StockBaseRestrict();
        stockBaseRestrict.setFlowValue(minFlowValue);
        stockBaseRestrict.setPe(maxPe);
        return stockBaseMapper.queryByFlowValueAndPe(stockBaseRestrict);
    }

    @Override
    public Page<StockBase> fuzzyQuery(StockBaseRestrict stockBaseRestrict) {
        Page<StockBase> page = new Page<>(stockBaseRestrict.getPageno(), stockBaseRestrict.getPagesize());
        stockBaseMapper.fuzzyQuery(stockBaseRestrict, page);
        return page;
    }


    @Override
    public StockBase query(String code) {
        return stockBaseMapper.query(code);
    }
}
