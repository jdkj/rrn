package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.CurrencyUtils;
import com.eliteams.quick4j.core.util.FestivalDayUtils;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.AgreementBorrowMoneyCostRateContainer;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.container.SecuritiesAccountCategoryTypeParamContainer;
import com.eliteams.quick4j.web.dao.AgreementMapper;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.*;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementRestrict;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateRestrict;
import com.eliteams.quick4j.web.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dsmart on 2016/10/24.
 */
@Service
public class AgreementServiceImpl extends GenericServiceImpl<Agreement, Long> implements AgreementService {

    @Resource
    private AgreementMapper agreementMapper;


    @Resource
    private UserService userService;

    @Resource
    private BorrowAgreementService borrowAgreementService;

    @Resource
    private UserAccountService userAccountService;


    @Resource
    private UserCouponService userCouponService;

    @Resource
    private StockPositionService stockPositionService;

    @Resource
    private StockOperateService stockOperateService;

    @Resource
    private DistributionApi distributionApi;

    @Resource
    private SecuritiesCompanyMoneyAccountDetailService securitiesCompanyMoneyAccountDetailService;

    @Resource
    private PhoneMsgService phoneMsgService;


    @Override
    public Agreement getAgreementByUserIdAndAgreementId(Long userId, Long agreementId) {
        AgreementRestrict agreementRestrict = new AgreementRestrict();
        agreementRestrict.setUserId(userId);
        agreementRestrict.setAgreementId(agreementId);
        return agreementMapper.selectByUserIdAndId(agreementRestrict);
    }

    @Override
    public List<Agreement> listAllActivityAgreementByUserId(Long userId) {
        return agreementMapper.listAllActivityAgreementByUserId(userId);
    }

    @Override
    public Page<Agreement> listAgreement(AgreementRestrict agreementRestrict) {
        Page<Agreement> page = new Page<>(agreementRestrict.getPageno(), agreementRestrict.getPagesize());
        agreementMapper.listActiveAgreementsByUserId(agreementRestrict, page);
        return page;
    }

    @Override
    public Page<Agreement> listAgreementBySignType(AgreementRestrict agreementRestrict) {
        Page<Agreement> page = new Page<>(agreementRestrict.getPageno(), agreementRestrict.getPagesize());
        agreementMapper.listActiveAgreementsByUserIdAndSignType(agreementRestrict, page);
        return page;
    }

    @Override
    public GenericDao<Agreement, Long> getDao() {
        return agreementMapper;
    }

    @Override
    public Result signAgreement(AgreementRestrict agreementRestrict) {
        Result result = new Result("合约生成失败");


        //借款金额
        Double borrowMoney = agreementRestrict.getBorrowMoney();
        BorrowAgreement borrowAgreement = borrowAgreementService.selectById(agreementRestrict.getBorrowAgreementId());

        //当前最大持有量
        Integer maxOwnNumber = borrowAgreement.getMaxOwnNumber();

        Integer owningNumber = this.countActiveAgreementByTypeAndUserId(agreementRestrict.getSignAgreementType(), agreementRestrict.getUserId());

        if (maxOwnNumber.compareTo(0) > 0 && owningNumber.compareTo(maxOwnNumber) >= 0) {
            result.setResultFlag(false);
            result.setResultMsg("您已达到[" + borrowAgreement.getBorrowTypeDesc() + "]最大可持有量,请选择其它投顾类型!");
            return result;
        }



        //判断申请资金范围
        if (borrowMoney < borrowAgreement.getBorrowMinMoney()) {
            result.setResponseStatusEnums(ResponseStatusEnums.BORROW_NOT_ENOUGH_MONEY);
            result.setResultMsg("申请资金不能少于" + borrowAgreement.getBorrowMinMoney());
            return result;
        } else if (borrowMoney > borrowAgreement.getBorrowMaxMoney()) {
            result.setResponseStatusEnums(ResponseStatusEnums.BORROW_TOO_MUCH_MONEY);
            result.setResultMsg("申请资金不能多于" + borrowAgreement.getBorrowMaxMoney());
            return result;
        }


        //这里先判断当前系统操盘账户策略

        SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = null;
        if (SecuritiesAccountCategoryTypeParamContainer.getCategoryType().compareTo(0) == 1) {
            securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getMaxAgreementAvailableMoneyObject();
        } else {
            securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getMaxAvailableItemFromVisual();
        }

        if (securitiesCompanyMoneyAccountDetail == null || securitiesCompanyMoneyAccountDetail.getAvailableMoney().compareTo(borrowMoney) < 0) {
            result.setResultFlag(false);
            result.setResultMsg("当前可申请资金最大为" + securitiesCompanyMoneyAccountDetail.getAvailableMoney());
            return result;
        }

        Agreement agreement = new Agreement();
        agreement.setSignAgreementType(agreementRestrict.getSignAgreementType());
        agreement.setSignAgreementTypeDesc(borrowAgreement.getBorrowTypeDesc());

        User user = userService.selectById(agreementRestrict.getUserId());


        //借款年利率   单位%
        Double multipleRate = borrowAgreement.getMultipleRate();

        //借款倍数
        Integer multiple = borrowAgreement.getMultiple();

        //保证金
        //取整
        Double lockMoney = CurrencyUtils.parseMoenyRoundHalfUpWithoutDigits(borrowMoney / multiple + "");

        //警戒线
        Double warningLineMoney = CurrencyUtils.parseMoenyRoundHalfUpWithoutDigits(borrowMoney + lockMoney * 0.5 + "");

        //平仓线
        Double closeLineMoney = CurrencyUtils.parseMoenyRoundHalfUpWithoutDigits(borrowMoney + lockMoney * 0.3 + "");

        //管理费
        //保留2为小数
        Double accountManagementFee = 0.0d;

        //资金成本
        Double agreementBorrowMoneyFee = 0.0d;


        int baseManagementFeeRate = 1;

        switch (agreement.getSignAgreementType()) {
            case 0://短期借款需保证账户中有两日的资金管理费
                baseManagementFeeRate = 2;
                accountManagementFee = borrowMoney * multipleRate / 100 / 365;
                agreementBorrowMoneyFee = borrowMoney * AgreementBorrowMoneyCostRateContainer.getAgreementBorrowMoneyCostRate() / 100 / 365;
                break;
            case 1:
            case 2:
                accountManagementFee = borrowMoney * multipleRate / 100 / 12;
                agreementBorrowMoneyFee = borrowMoney * AgreementBorrowMoneyCostRateContainer.getAgreementBorrowMoneyCostRate() / 100 / 12;
                break;
            default:
                break;
        }
        agreement.setAccountManagementFee(accountManagementFee);
        agreement.setManagementCostFee(agreementBorrowMoneyFee);

        UserAccount userAccount = userAccountService.getUserAccountByUserId(user.getUserId());

        if (user.getUserParamInfoTags().isPhoneTag()) {

            //账户可用资金不少于合约生成需要资金
            if (userAccount.getMoney() >= (lockMoney + agreement.getAccountManagementFee() * baseManagementFeeRate)) {

                agreement.setUserId(agreementRestrict.getUserId());
                agreement.setBorrowMoney(borrowMoney);//借款
                agreement.setLockMoney(lockMoney);//锁定资金
                agreement.setAvailableCredit(borrowMoney + lockMoney);//可用余额
                agreement.setWarningLineMoney(warningLineMoney);//警戒线
                agreement.setCloseLineMoney(closeLineMoney);//平仓线
                agreement.setSignAgreementMultipleId(borrowAgreement.getBorrowAgreementId());//借款倍数编号
                agreement.setSignAgreementType(borrowAgreement.getBorrowType());//借款类型
                agreement.setAccountManagementFee(accountManagementFee);//资金管理费用(利息)
                agreement.setOperateFee(user.getOperateFee());
                agreement.setOperateRate(user.getOperateRate());
                agreement.setContinueOperatePeriod(0);//
                agreement.setOperateMaxPeriod(borrowAgreement.getMaxPeriod());//最久操盘期限
                agreement.setSecuritiesAccountId(securitiesCompanyMoneyAccountDetail.getSecuritiesAccountId());//合约所属证券账户
                agreementMapper.insert(agreement);//生成合约
                securitiesCompanyMoneyAccountDetailService.reduceAgreementAvailableMoney(securitiesCompanyMoneyAccountDetail.getSecuritiesAccountId(), agreement.getBorrowMoney(), agreement.getAgreementId());

                Long userCouponId = agreementRestrict.getUserCouponId();
                Double userCouponAvailableValue = 0.0d;
                if (userCouponId.compareTo(0l) > 0) {
                    //绑定优惠券成功
                    if (userCouponService.updateByAttachAgreement(user.getUserId(), userCouponId, agreement.getAgreementId()).getResultFlag()) {
                        UserCoupon userCoupon = userCouponService.getUserCouponByUserIdAndUserCouponId(user.getUserId(), userCouponId);
                        userCouponAvailableValue = userCoupon.getCouponAvailableValue();
                        agreement.setUserCouponId(userCouponId);
                        agreement.setCouponAvailableValue(userCouponAvailableValue);
                        agreementMapper.updateAgreement(agreement);
                    }
                }


                //账户资金变动
                //将几种资金变动的接口封装到UserAccountService中
                //锁定保证金
                userAccountService.signAgreementLockMoney(user.getUserId(), lockMoney, agreement.getAgreementId());

                //合约奖励积分
                userAccountService.agreementRewardIntegral(user.getUserId(), CurrencyUtils.parseMoenyRoundHalfUpWithoutDigits(accountManagementFee * baseManagementFeeRate + ""), agreement.getAgreementId());

                interestPaymentBySignAgreement(agreement.getAgreementId());

//            agreementInterestPayment(agreement.getAgreementId());
                result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
                result.setResultMsg("合约已生成");

//            //缴纳资金管理费用 延迟执行
//            final Long fAgreementId = agreement.getAgreementId();
//            GlobalFixedThreadPool.getInstance().execute(new Runnable() {
//                @Override
//                public void run() {
//                    try {
//                        Thread.sleep(10000l);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                    agreementInterestPayment(fAgreementId);
//                }
//            });


            } else {
                result.setResponseStatusEnums(ResponseStatusEnums.ACCOUNT_MONEY_NOT_ENOUGH);
                return result;
            }
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.USER_PHONE_IS_REQUIRED);
            return result;
        }
        return result;
    }


    @Override
    public Result interestPaymentBySignAgreement(Long agreementId) {
        Result result = new Result("资金管理费扣除失败");

        Agreement agreement = this.selectById(agreementId);

        Integer agreementType = agreement.getSignAgreementType();
        Boolean isShort = false;
        int feeRate = 1;
        if (agreementType.compareTo(0) == 0) {//短期合约
            feeRate = 2;
            isShort = true;
        }


        //利息结算  先扣除优惠券  再扣除用户账户资金
        //优惠券扣除方式  预充值到用户账户

        //判断当前合约是否使用优惠券 且优惠券余额不为0
        if (agreement.getUserCouponId().compareTo(0l) > 0 && agreement.getCouponAvailableValue().compareTo(0.0d) > 0) {
            result = userAccountService.interestPaymentByCoupon(agreement.getUserId(), agreement.getAccountManagementFee() * feeRate, agreement.getAgreementId(), agreement.getUserCouponId());
            if (result.getResultFlag()) {
                Double couponAvailableValue = agreement.getCouponAvailableValue() - agreement.getAccountManagementFee() * feeRate;

                //如果优惠券不足支付管理费用
                if (couponAvailableValue.compareTo(0.00d) < 0) {

                    //记录该合约总贡献管理费
                    this.increaseTotalManagementFee(agreementId, agreement.getAccountManagementFee() * feeRate - agreement.getCouponAvailableValue());

                    //利息扣除资金成本
                    Double profitMoney = agreement.getAccountManagementFee() * feeRate - agreement.getCouponAvailableValue() - agreement.getManagementCostFee() * feeRate;
                    if (profitMoney.compareTo(0.00d) > 0) {

                        //记录管理费贡献利润
                        securitiesCompanyMoneyAccountDetailService.increaseProfitMoneyByInterest(agreement.getSecuritiesAccountId(), profitMoney, agreementId, agreement.getUserId());

                        //利息分成
                        distributionApi.shareCommissionByAgreementInterest(agreement.getUserId(), agreement.getAgreementId(), profitMoney);

                    }
                }

                couponAvailableValue = couponAvailableValue.compareTo(0.00d) > 0 ? couponAvailableValue : 0;
                agreement.setCouponAvailableValue(couponAvailableValue);
                agreementMapper.updateAgreement(agreement);
            }
        } else {

            //结算利息
            result = userAccountService.interestPayment(agreement.getUserId(), agreement.getAccountManagementFee() * feeRate, agreement.getAgreementId());

            if (result.getResultFlag()) {

                //记录合约总消耗管理费用
                this.increaseTotalManagementFee(agreementId, agreement.getAccountManagementFee() * feeRate);

                //利息扣除资金成本
                Double profitMoney = (agreement.getAccountManagementFee() - agreement.getManagementCostFee()) * feeRate;
                if (profitMoney.compareTo(0.00d) > 0) {

                    //记录管理费贡献利润
                    securitiesCompanyMoneyAccountDetailService.increaseProfitMoneyByInterest(agreement.getSecuritiesAccountId(), profitMoney, agreementId, agreement.getUserId());

                    //利息分成
                    distributionApi.shareCommissionByAgreementInterest(agreement.getUserId(), agreement.getAgreementId(), profitMoney);
                }

            }
        }


        //利息结算成功
        if (result.getResultFlag()) {
            //更新合约已操盘日期和操盘最大周期
            switch (agreement.getSignAgreementType()) {
                case 0:
                default:
                    agreement.setOperateDeadline(FestivalDayUtils.getSeveralWorkDayBaseOnToday(2));
                    agreementMapper.updateDayAgreementByPayInterestWhenSigned(agreement);
                    // agreementMapper.updateDayAgreementByPayInterest(agreementId);
                    break;
                case 1://中期
                case 2://特惠
                    agreementMapper.updateMonthAgreementByPayInterest(agreementId);
                    break;
            }
            if (MarketingContainer.isMarketingOn()) {//如果今天是操盘日
                //更新合约持续操盘天数
                this.updateAgreementContinueOperateDay(agreementId);
            }
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
            result.setResultMsg("资金管理费缴纳成功");
        } else {//利息结算失败
            //更新合约状态为  结算中   操盘资金管理费不足关闭
            //直接从合约中扣除需要的操盘资金管理费?
            agreement.setAvailableCredit(agreement.getAvailableCredit() - agreement.getAccountManagementFee());
            agreementMapper.updateAgreementByLackOfInterest(agreement);

        }


        return result;
    }

    @Override
    public Result agreementInterestPayment(Long agreementId) {

        Result result = new Result("资金管理费扣除失败");

        Agreement agreement = this.selectById(agreementId);

        //当前连续操盘周期不大于最大操盘周期
        if (agreement.getContinueOperatePeriod() < agreement.getOperateMaxPeriod()) {

            //当前是开盘日
            if (MarketingContainer.isMarketingOn()) {

                //利息结算  先扣除优惠券  再扣除用户账户资金
                //优惠券扣除方式  预充值到用户账户

                //判断当前合约是否使用优惠券 且优惠券余额不为0
                if (agreement.getUserCouponId().compareTo(0l) > 0 && agreement.getCouponAvailableValue().compareTo(0.0d) > 0) {

                    result = userAccountService.interestPaymentByCoupon(agreement.getUserId(), agreement.getAccountManagementFee(), agreement.getAgreementId(), agreement.getUserCouponId());
                    if (result.getResultFlag()) {
                        Double couponAvailableValue = agreement.getCouponAvailableValue() - agreement.getAccountManagementFee();

                        //如果优惠券不足支付管理费用
                        if (couponAvailableValue.compareTo(0.00d) < 0) {

                            //记录该合约总贡献管理费
                            this.increaseTotalManagementFee(agreementId, agreement.getAccountManagementFee() - agreement.getCouponAvailableValue());

                            //利息扣除资金成本
                            Double profitMoney = agreement.getAccountManagementFee() - agreement.getCouponAvailableValue() - agreement.getManagementCostFee();
                            if (profitMoney.compareTo(0.00d) > 0) {
                                //利息分成
                                distributionApi.shareCommissionByAgreementInterest(agreement.getUserId(), agreement.getAgreementId(), profitMoney);

                                //记录管理费贡献利润
                                securitiesCompanyMoneyAccountDetailService.increaseProfitMoneyByInterest(agreement.getSecuritiesAccountId(), profitMoney, agreementId, agreement.getUserId());

                            }
                        }

                        couponAvailableValue = couponAvailableValue.compareTo(0.00d) > 0 ? couponAvailableValue : 0;
                        agreement.setCouponAvailableValue(couponAvailableValue);
                        agreementMapper.updateAgreement(agreement);


                    }

                } else {
                    //结算利息
                    result = userAccountService.interestPayment(agreement.getUserId(), agreement.getAccountManagementFee(), agreement.getAgreementId());

                    if (result.getResultFlag()) {

                        //记录该合约总贡献管理费
                        this.increaseTotalManagementFee(agreementId, agreement.getAccountManagementFee());

                        //利息扣除资金成本
                        Double profitMoney = agreement.getAccountManagementFee() - agreement.getManagementCostFee();
                        if (profitMoney.compareTo(0.00d) > 0) {
                            //利息分成
                            distributionApi.shareCommissionByAgreementInterest(agreement.getUserId(), agreement.getAgreementId(), profitMoney);

                            //记录管理费贡献利润
                            securitiesCompanyMoneyAccountDetailService.increaseProfitMoneyByInterest(agreement.getSecuritiesAccountId(), profitMoney, agreementId, agreement.getUserId());
                        }
                    }
                }


                //利息结算成功
                if (result.getResultFlag()) {
                    //更新合约已操盘日期和操盘最大周期
                    switch (agreement.getSignAgreementType()) {
                        case 0:
                        default:
                            agreementMapper.updateDayAgreementByPayInterest(agreementId);
                            break;
                        case 1://中期
                        case 2://特惠
                            agreementMapper.updateMonthAgreementByPayInterest(agreementId);
                            break;
                    }
                    //更新合约持续操盘天数
                    this.updateAgreementContinueOperateDay(agreementId);
                    result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
                    result.setResultMsg("资金管理费缴纳成功");

                } else {//利息结算失败
                    //更新合约状态为  结算中   操盘资金管理费不足关闭
                    //直接从合约中扣除需要的操盘资金管理费?
//                    agreement.setAvailableCredit(agreement.getAvailableCredit() - agreement.getAccountManagementFee());
                    agreement.setForbidBuy(1);//禁止买入
                    agreement.setForceClose(1);//强制平仓
                    agreement.setOperateStatus(2);//管理费不足
                    agreement.setSettlementType(1);//管理费不足结算
                    agreement.setStatus(1);//合约待结算   欠管理费进入结算周期的合约   将由系统来执行卖出操作
                    agreementMapper.updateAgreementByLackOfInterest(agreement);
                }

            } else {
                result.setResultFlag(true);
                result.setResultMsg("非操盘日无需缴纳资金管理费");
            }

        } else {//操盘期限超过最大周期
            User user = userService.selectById(agreement.getUserId());

            phoneMsgService.addAgreementOverMaxPeriod(user.getUserId(), agreementId, user.getPhone(), "尊敬的用户您好,您的合约" + agreement.getShowAgreementId() + "已达到最大操盘期限，" +
                    "请您于今日下午14点前卖出所有持仓股票，并终止合约。系统将在14:30自动卖出该合约所有股票并进行结算。");

            agreement.setForbidBuy(1);//禁止买入
            agreement.setForceClose(1);//强制平仓
            agreement.setOperateStatus(4);//管理费不足
            agreement.setSettlementType(2);//管理费不足结算
//            agreement.setStatus(0);//达到最大操盘期限用户是可以自己进行卖出操作的
            agreementMapper.updateAgreementByOverMaxPeriod(agreement);
        }
        return result;
    }

    @Override
    public Boolean updateAgreementContinueOperateDay(Long agreementId) {
        return agreementMapper.updateAgreementContinueOperateDay(agreementId) > 0;
    }

    @Override
    public List<Agreement> listCaculateAgreements() {
        return agreementMapper.listCaculateAgreements();
    }


    @Override
    public List<Agreement> listNeedPayInterestAgreements() {
        return agreementMapper.listNeedPayInterestAgreements();
    }

    @Override
    public Boolean updateAgreementByOperateStock(Agreement agreement) {
        return agreementMapper.updateAgreementByOperateStock(agreement) > 0;
    }

    @Override
    public List<Agreement> listAll() {
        return agreementMapper.listAll();
    }

    @Override
    public Boolean updateByFreshAgreement(Agreement agreement) {
        return agreementMapper.updateByFreshAgreement(agreement) > 0;
    }


    @Override
    public Result settlementAgreement(Long agreementId, Long userId) {
        Result result = new Result();
        Agreement agreement = this.getAgreementByUserIdAndAgreementId(userId, agreementId);

        if (agreement != null) {

            //合约状态正常 或合约正在结算中
            if (agreement.getStatus() <= 2) {
                List<StockPosition> list = stockPositionService.listAllByAgreementId(agreementId);
                //当前合约没有持仓
                if (list == null || list.size() == 0) {
                    //没有未完成的交易委托
                    if (stockOperateService.countWaitingStockOperateOfAgreement(agreementId) == 0) {
                        //结算
                        //可用余额-配资资金
                        Double money = agreement.getAvailableCredit() - agreement.getBorrowMoney();
                        result = userAccountService.relaseLockMoney(agreement.getUserId(), money, agreementId);

                        //统计用户操盘合约盈利金额
                        Double profitMoney = money - agreement.getLockMoney();
                        userAccountService.increaseTotalProfit(agreement.getUserId(), profitMoney);


                        //合约释放同步更新对应券商账户数据
                        securitiesCompanyMoneyAccountDetailService.increaseAgreementAvailableMoney(agreement.getSecuritiesAccountId(), agreement.getBorrowMoney(), agreement.getAgreementId());
                        agreement.setStatus(2);
                        agreementMapper.updateAgreementStatus(agreement);
                    } else {
                        result.setResultFlag(false);
                        result.setResultMsg("当前仍有交易正在进行中");
                    }
                } else {
                    result.setResultFlag(false);
                    result.setResultMsg("请在卖出所有股票后在申请结算");
                }
            }
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.DATA_ILLEGAL_ERROR);
        }
        return result;
    }


    @Override
    public Result additionalLockMoney(AgreementRestrict agreementRestrict) {
        Result result = new Result();
        Agreement agreement = this.getAgreementByUserIdAndAgreementId(agreementRestrict.getUserId(), agreementRestrict.getAgreementId());

        if (agreement != null) {//合约存在
            Double additionalLockMoney = agreementRestrict.getAdditionalLockMoney();
            //追加的保证金不能少于1%
            if (additionalLockMoney.compareTo((agreement.getLockMoney() + agreement.getBorrowMoney()) * 0.01) >= 0) {

                result = userAccountService.increaseLockMoney(agreementRestrict.getUserId(), additionalLockMoney, agreementRestrict.getAgreementId());
                if (result.getResultFlag()) {
                    agreement.setLockMoney(agreement.getLockMoney() + additionalLockMoney);
                    agreement.setAvailableCredit(agreement.getAvailableCredit() + additionalLockMoney);


                    Double currentTotalValue = agreement.getStockCurrentPrice() + agreement.getAvailableCredit() + agreement.getPrebuyLockMoney();
                    //追加金额后大于平仓线
                    if (currentTotalValue.compareTo(agreement.getCloseLineMoney()) > 0) {
                        agreement.setForceClose(0);
                        agreement.setOperateStatus(0);
                    }
                    //追加金额后大于警戒线
                    if (currentTotalValue.compareTo(agreement.getWarningLineMoney()) > 0) {
                        agreement.setForbidBuy(0);
                    }

                    agreementMapper.updateAgreement(agreement);

                }
            } else {
                result.setResultFlag(false);
                result.setResultMsg("最低追加" + CurrencyUtils.parseMoenyRoundHalfUp((agreement.getLockMoney() + agreement.getBorrowMoney()) * 0.01 + ""));
            }
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.DATA_ILLEGAL_ERROR);
        }
        return result;
    }

    @Override
    public Boolean refreshAgreementInfo(Long agreementId) {
        Double stockCurrentPrice = 0.0d;
        List<StockPosition> stockPositionList = stockPositionService.listAllByAgreementId(agreementId);
        Agreement agreement = this.selectById(agreementId);
        if (stockPositionList != null && stockPositionList.size() > 0) {
            for (StockPosition stockPosition : stockPositionList) {
                stockCurrentPrice += stockPosition.getCurrentMarketValue();//
            }
        }
        agreement.setStockCurrentPrice(stockCurrentPrice);
        agreement.setDiffMoney(stockCurrentPrice + agreement.getAvailableCredit() + agreement.getPrebuyLockMoney() - agreement.getLockMoney() - agreement.getBorrowMoney());

        //当前可用额度
        Double avaiableCredit = agreement.getAvailableCredit();

        //警戒线
        Double warningLineMoney = agreement.getWarningLineMoney();
        //平仓线
        Double closeLineMoney = agreement.getCloseLineMoney();

        Double currentTotalValue = stockCurrentPrice + agreement.getAvailableCredit() + agreement.getPrebuyLockMoney();

        if (currentTotalValue.compareTo(warningLineMoney) > 0) {//当前额度大于警戒线
            this.updateAgreementByOperateStock(agreement);
        } else {//低于警戒线的时候

            updateAgreementForbidBuy(agreement);
            //发送短信
            User user = userService.selectById(agreement.getUserId());
            String msg = user.getPhone() + ",您的投顾合约(" + agreement.getShowAgreementId() + ")盘中已触及警戒线，请密切关注并及时追加保证金，以免影响您的后续操作!";
            phoneMsgService.addWarningLineMsg(user.getUserId(), agreement.getAgreementId(), user.getPhone(), msg);


            if (currentTotalValue.compareTo(closeLineMoney) <= 0) {//当前额度不大于平仓线

                //如果当前是自由报价时间段  那么则进入强制平仓流程
                if (StockMarketingUtils.isDuringStreamingPricesTime()) {

                    //强制平仓
                    //1、撤销所有未完结的买入操作(未报以及挂单)
                    List<StockOperate> unFinishedList = stockOperateService.listAllWaitingStockBuyOperateByAgreementId(agreementId);
                    if (unFinishedList != null && !unFinishedList.isEmpty()) {
                        for (StockOperate stockOperate : unFinishedList) {
                            stockOperateService.updateExpiredOperate(stockOperate);
                        }
                    }

                    //2、市价卖出当前所有可售
                    StockOperateRestrict stockOperateRestrict = null;
                    if (stockPositionList != null && stockPositionList.size() > 0) {
                        for (StockPosition stockPosition : stockPositionList) {
                            if (stockPosition.getStockAvailableNum().compareTo(0l) > 0) {
                                stockOperateRestrict = new StockOperateRestrict();
                                stockOperateRestrict.setAgreementId(agreementId);
                                stockOperateRestrict.setUserId(agreement.getUserId());
                                stockOperateRestrict.setCommissionType(1);//市价卖出
                                stockOperateRestrict.setCommissionNum(stockPosition.getStockAvailableNum());//当前可售数量
                                stockOperateRestrict.setStockCode(stockPosition.getStockCode());
                                stockOperateService.operateSell(stockOperateRestrict);
                            }
                        }
                    }
                    updateAgreementForceClose(agreement);//更新合约信息
                }//否则暂时不处理
            }
        }
        return true;
    }

    /**
     * 更新合约禁止买入
     *
     * @param agreement
     * @return
     */
    Boolean updateAgreementForbidBuy(Agreement agreement) {
        agreement.setForbidBuy(1);

        return agreementMapper.updateAgreement(agreement) > 0;
    }

    /**
     * 更新合约强制平仓
     *
     * @param agreement
     * @return
     */
    Boolean updateAgreementForceClose(Agreement agreement) {
        agreement.setForbidBuy(1);
        agreement.setForceClose(1);
//        agreement.setStatus(1);
        agreement.setOperateStatus(3);
        return agreementMapper.updateAgreement(agreement) > 0;
    }


    @Override
    public Map<Long, List<Agreement>> listAlmostOverdueAgreements() {
        Map<Long, List<Agreement>> map = null;
        List<Agreement> agreementList = agreementMapper.listAlmostOverdueAgreements();
        if (agreementList != null && agreementList.size() > 0) {
            map = new HashMap<Long, List<Agreement>>();
            for (Agreement agreement : agreementList) {
                int continueOperatePeriod = agreement.getContinueOperatePeriod();
                int operateMaxPeriod = agreement.getOperateMaxPeriod();
                if (continueOperatePeriod < operateMaxPeriod) {//还未超过最大操盘周期
                    List<Agreement> unitList = null;
                    if (map.get(agreement.getUserId()) != null) {
                        unitList = map.get(agreement.getUserId());
                    } else {
                        unitList = new ArrayList<>();
                    }
                    unitList.add(agreement);
                    map.put(agreement.getUserId(), unitList);
                }
            }
        }
        return map;
    }

    @Override
    public Result forceSettlementAgreement(Long agreementId) {
        Result result = new Result("强制结算失败");
        List<StockPosition> stockPositionList = stockPositionService.listAllByAgreementId(agreementId);
        Agreement agreement = this.selectById(agreementId);
        //如果当前是自由报价时间段  那么则进入强制平仓流程
        if (StockMarketingUtils.isDuringStreamingPricesTime()) {
            //强制平仓
            //1、撤销所有未完结的买入操作(未报以及挂单)
            List<StockOperate> unFinishedList = stockOperateService.listAllWaitingStockBuyOperateByAgreementId(agreementId);
            if (unFinishedList != null && !unFinishedList.isEmpty()) {
                for (StockOperate stockOperate : unFinishedList) {
                    stockOperateService.updateExpiredOperate(stockOperate);
                }
            }


            int sellFailCount = 0;

            //2、市价卖出当前所有可售
            StockOperateRestrict stockOperateRestrict = null;
            if (stockPositionList != null && stockPositionList.size() > 0) {
                for (StockPosition stockPosition : stockPositionList) {
                    if (stockPosition.getStockAvailableNum().compareTo(0l) > 0) {
                        stockOperateRestrict = new StockOperateRestrict();
                        stockOperateRestrict.setAgreementId(agreementId);
                        stockOperateRestrict.setUserId(agreement.getUserId());
                        stockOperateRestrict.setCommissionType(1);//市价卖出
                        stockOperateRestrict.setCommissionNum(stockPosition.getStockAvailableNum());//当前可售数量
                        stockOperateRestrict.setStockCode(stockPosition.getStockCode());
                        Result flagResult = stockOperateService.operateSellByForceSettlement(stockOperateRestrict);
                        if (!flagResult.getResultFlag()) {
                            sellFailCount++;
                        }
                    }
                }
            }

            if (sellFailCount == 0) {
                settlementAgreement(agreement.getAgreementId(), agreement.getUserId());//进入结算状态
            } else {
                agreement.setStatus(4);
                agreement.setSettlementType(3);
                updateBySettlementFailed(agreement);
            }

            result.setResultFlag(true);
            result.setResultMsg("强制结算成功");
        }//否则暂时不处理

        return result;
    }


    @Override
    public List<Agreement> listLackOfManagementFeeAgreements() {
        return agreementMapper.listLackOfManagementFeeAgreements();
    }


    @Override
    public List<Agreement> listOverMaxPeriodAgreements() {
        return agreementMapper.listOverMaxPeriodAgreements();
    }


    @Override
    public Boolean updateBySettlementFailed(Agreement agreement) {
        return agreementMapper.updateBySettlementFailed(agreement) > 0;
    }


    @Override
    public Result increaseTotalManagementFee(Long agreementId, Double managementFee) {
        Result result = new Result("总管理费用更新失败");
        Agreement agreement = this.selectById(agreementId);
        agreement.setTotalManagementFee(agreement.getTotalManagementFee() + managementFee);
        if (agreementMapper.updateAgreement(agreement) > 0) {
            result = userAccountService.increaseTotalManagementFee(agreement.getUserId(), managementFee);
        }
        return result;
    }

    @Override
    public Result increaseTotalOperateFee(Long agreementId, Double operateFee) {
        Result result = new Result("总操盘费用更新失败");
        Agreement agreement = this.selectById(agreementId);
        agreement.setTotalOperateFee(agreement.getTotalOperateFee() + operateFee);
        if (agreementMapper.updateAgreement(agreement) > 0) {
            result = userAccountService.increaseTotalOperateFee(agreement.getUserId(), operateFee);
        }
        return result;
    }


    @Override
    public int countActiveAgreementByTypeAndUserId(Integer type, Long userId) {
        AgreementRestrict agreementRestrict = new AgreementRestrict();
        agreementRestrict.setUserId(userId);
        agreementRestrict.setSignAgreementType(type);
        return agreementMapper.countActiveAgreementByTypeAndUserId(agreementRestrict);
    }
}
