package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.UserIdAuth;

/**
 * Created by Dsmart on 2016/10/23.
 */
public interface UserIdAuthService extends GenericService<UserIdAuth,Long> {

    /**
     * 添加身份信息
     * @param userIdAuth
     * @return
     */
    public Boolean addItem(UserIdAuth userIdAuth);


    /**
     * 根据用户编号获取身份认证信息
     * @param userId
     * @return
     */
    public UserIdAuth getAuthInfoByUserId(Long userId);


}
