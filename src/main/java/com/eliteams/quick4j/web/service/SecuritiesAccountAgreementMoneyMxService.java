package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SecuritiesAccountAgreementMoneyMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountAgreementMoneyMxRestrict;

public interface SecuritiesAccountAgreementMoneyMxService extends GenericService<SecuritiesAccountAgreementMoneyMx, Long> {


    /**
     * 追加总资金 引起合约总可用资金变动
     *
     * @param securitiesAccountId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Result addByIncreaseTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney);


    /**
     * 撤出总资金 引起合约总可用资金变动
     *
     * @param securitiesAccountId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Result addByReduceTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney);


    /**
     * 生成合约 引起合约总可用资金变动
     *
     * @param securitiesAccountId
     * @param totalMoney
     * @param changeMoney
     * @param agreementId
     * @return
     */
    Result addBySignAgreement(Long securitiesAccountId, Double totalMoney, Double changeMoney, Long agreementId);


    /**
     * 释放合约 引起合约总可用资金变动
     *
     * @param securitiesAccountId
     * @param totalMoney
     * @param changeMoney
     * @param agreementId
     * @return
     */
    Result addByReleaseAgreement(Long securitiesAccountId, Double totalMoney, Double changeMoney, Long agreementId);


    /**
     * 根据请求参数获取申请合约资金变动明细列表
     *
     * @param securitiesAccountAgreementMoneyMxRestrict
     * @return
     */
    Page<SecuritiesAccountAgreementMoneyMx> listByRestrict(SecuritiesAccountAgreementMoneyMxRestrict securitiesAccountAgreementMoneyMxRestrict);


}
