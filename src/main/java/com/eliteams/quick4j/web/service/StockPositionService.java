package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.StockPosition;
import com.eliteams.quick4j.web.model.pagerestrict.StockPositionRestrict;


import java.util.List;

/**
 * Created by Dsmart on 2016/10/31.
 */
public interface StockPositionService extends GenericService<StockPosition,Long> {


    /**
     * 增持新股票
     * @param stockPosition
     * @return
     */
    public  Boolean addItem(StockPosition stockPosition);

    /**
     * 获取用户某合约下的持仓列表详情
     * @param stockPositionRestrict
     * @return
     */
    public Page<StockPosition> listByAgreementId(StockPositionRestrict stockPositionRestrict);


    /**
     * 获取某合约下所有的持仓数据
     * @param agreementId
     * @return
     */
    List<StockPosition> listAllByAgreementId(Long agreementId);


    /**
     * 根据股票代码获取持仓列表
     * @param stockCode
     * @return
     */
    public List<StockPosition> listByStockCode(String stockCode);


    /**
     * 根据用户编号和股票代码获取用户持仓数据
     * @param userId
     * @param stockCode
     * @return
     */
    public StockPosition getStockPositionByAgreementIdAndUserIdAndStockCode(Long agreementId,Long userId,String stockCode);


    /**
     * 更新用户持仓数据信息(增持)
     * @param agreementId 合约编号
     * @param userId 用户编号
     * @param stockCode 股票代码
     * @param stockName 股票名称
     * @param stockNum
     * @param buyPrice
     * @return
     */
    public Boolean addStockPosition(Long agreementId,Long userId,String stockCode,String stockName,Long stockNum,Double buyPrice);


    /**
     * 更新用户持仓数据信息(减持)  减持成功
     * @param agreementId 合约编号
     * @param userId 用户编号
     * @param stockCode 股票编码
     * @param stockNum 股票数量
     * @param sellPrice 卖出价格
     * @return
     */
     public Boolean reduceStockPosition(Long agreementId,Long userId,String stockCode,Long stockNum,Double sellPrice);


    /**
     * 预减持
     * @param agreementId
     * @param userId
     * @param stockCode
     * @param stockNum
     * @return
     */
    public Result preReduceStockPosition(Long agreementId, Long userId, String stockCode, Long stockNum);


    /**
     * 更新可售股票数据
     * @param stockPosition
     * @return
     */
    public Boolean updateStockPositionAvailableNum(StockPosition stockPosition);


    /**
     * 获取所有的持仓数据
     * @return
     */
    public List<StockPosition> listAll();



    /**
     * 根据股票编码分组获取所有的持仓数据
     * @return
     */
    List<StockPosition> listAllGroupByStockCode();


    /**
     * 根据股票编码获取所有的持仓数据
     * @param stockCode
     * @return
     */
    List<StockPosition> listAllByStockCode(String stockCode);


}
