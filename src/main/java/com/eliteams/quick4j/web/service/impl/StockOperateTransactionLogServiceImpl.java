package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.StockOperateTransactionLogMapper;
import com.eliteams.quick4j.web.model.StockOperateTransactionLog;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateTransactionLogRestrict;
import com.eliteams.quick4j.web.service.StockOperateTransactionLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2017/5/31.
 */
@Service
public class StockOperateTransactionLogServiceImpl extends GenericServiceImpl<StockOperateTransactionLog, Long> implements StockOperateTransactionLogService {


    @Resource
    private StockOperateTransactionLogMapper stockOperateTransactionLogMapper;


    @Override
    public Result addByFinishOperate(StockOperateTransactionLog stockOperateTransactionLog) {
        Result result = new Result("记录已存在");
        if (!isExistBeforeAddByFinish(stockOperateTransactionLog)) {
            if (stockOperateTransactionLogMapper.insertByFinishOperate(stockOperateTransactionLog) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("成交记录添加成功");
            }
        } else {
            result.setResultFlag(false);
            result.setResultMsg("记录已存在");
        }
        return result;
    }

    @Override
    public GenericDao<StockOperateTransactionLog, Long> getDao() {
        return stockOperateTransactionLogMapper;
    }

    @Override
    public Result addByCancelOperate(StockOperateTransactionLog stockOperateTransactionLog) {
        Result result = new Result("记录已存在");
        if (!isExistBeforeAddByCancel(stockOperateTransactionLog)) {
            //添加记录
            if (stockOperateTransactionLogMapper.insertByCancelOperate(stockOperateTransactionLog) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("撤单记录添加成功");
            }
        } else {
            result.setResultFlag(false);
            result.setResultMsg("记录已存在");
        }
        return result;
    }


    /**
     * 交易完成插入前的检测
     *
     * @param stockOperateTransactionLog
     * @return
     */
    Boolean isExistBeforeAddByFinish(StockOperateTransactionLog stockOperateTransactionLog) {
        return stockOperateTransactionLogMapper.countBeforeAddByFinish(stockOperateTransactionLog) > 0;
    }

    /**
     * 交易取消插入前的检测
     *
     * @param stockOperateTransactionLog
     * @return
     */
    Boolean isExistBeforeAddByCancel(StockOperateTransactionLog stockOperateTransactionLog) {
        return stockOperateTransactionLogMapper.countBeforeAddByCancel(stockOperateTransactionLog) > 0;
    }


    @Override
    public Page<StockOperateTransactionLog> listCurrentDayDealsByRestrictWithAgreementId(StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict) {
        Page<StockOperateTransactionLog> page = new Page<>(stockOperateTransactionLogRestrict.getPageno(), stockOperateTransactionLogRestrict.getPagesize());
        stockOperateTransactionLogMapper.listCurrentDayDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict, page);
        return page;
    }


    @Override
    public Page<StockOperateTransactionLog> listHistoryDealsByRestrictWithAgreementId(StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict) {
        Page<StockOperateTransactionLog> page = new Page<>(stockOperateTransactionLogRestrict.getPageno(), stockOperateTransactionLogRestrict.getPagesize());
        stockOperateTransactionLogMapper.listHistoryDealsByRestrictWithAgreementId(stockOperateTransactionLogRestrict, page);
        return page;
    }
}
