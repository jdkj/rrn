/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.UserPlatMapper;
import com.eliteams.quick4j.web.model.UserPlat;
import com.eliteams.quick4j.web.service.UserPlatService;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-9-3, 21:49:37
 */
@Service
public class UserPlatServiceImpl extends GenericServiceImpl<UserPlat, Long> implements UserPlatService {

    @Resource
    private UserPlatMapper userPlatMapper;

    @Override
    public GenericDao<UserPlat, Long> getDao() {
        return userPlatMapper;
    }

    @Override
    public Boolean addUserPlat(UserPlat userPlat) {
        return userPlatMapper.insert(userPlat) > 0;
    }

    @Override
    public Boolean updateUserPlatUserId(UserPlat userPlat) {
        return userPlatMapper.updateUserId(userPlat) > 0;
    }

    @Override
    public UserPlat getWxUserPlatByOpenId(String openId) {
        return userPlatMapper.getUserPlatByOpenId(openId);
    }

    @Override
    public UserPlat getWxUserPlatByUserId(Long userId) {
        return userPlatMapper.getWxUserPlatByUserId(userId);
    }

}
