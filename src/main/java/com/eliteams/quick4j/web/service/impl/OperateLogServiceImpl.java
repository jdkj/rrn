package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.OperateLogMapper;
import com.eliteams.quick4j.web.model.OperateLog;
import com.eliteams.quick4j.web.model.OperatorUserRelation;
import com.eliteams.quick4j.web.model.StockOperate;
import com.eliteams.quick4j.web.model.pagerestrict.OperateLogRestrict;
import com.eliteams.quick4j.web.service.OperateLogService;
import com.eliteams.quick4j.web.service.OperatorUserRelationService;
import com.eliteams.quick4j.web.service.StockOperateService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2017/1/9.
 */
@Service
public class OperateLogServiceImpl extends GenericServiceImpl<OperateLog, Long> implements OperateLogService {

    @Resource
    private OperateLogMapper operateLogMapper;

    @Resource
    private StockOperateService stockOperateService;

    @Resource
    private OperatorUserRelationService operatorUserRelationService;


    @Override
    public Result addItem(OperateLog operateLog) {
        Result result = new Result("添加失败");
        if (operateLogMapper.countBeforeInsert(operateLog) == 0) {
            if (operateLogMapper.insert(operateLog) > 0) {
                if (operateLog.getAccountType().compareTo(0) == 0) {
                    operateLogMapper.operateStock(operateLog);
                }
                result.setResultFlag(true);
                result.setResultMsg("添加成功");
            }
        } else {
            result.setResultFlag(false);
            result.setResultMsg("该数据已存在，不可重复添加");
        }

        return result;
    }

    @Override
    public Result addItemByUserOperateStockApply(StockOperate stockOperate) {
        Result result = new Result("添加失败");
        OperateLog operateLog = new OperateLog();
        operateLog.setUserId(stockOperate.getUserId());
        operateLog.setOperateId(stockOperate.getOperateId());
        operateLog.setCommissionNum(stockOperate.getCommissionNum());
        operateLog.setCommissionPrice(stockOperate.getCommissionPrice());
        operateLog.setCommissionPriceType(stockOperate.getCommissionPriceType());
        operateLog.setCommissionType(stockOperate.getOperateType());
        operateLog.setStockCode(stockOperate.getStockCode());
        operateLog.setStockMarket(stockOperate.getStockMarket());
        operateLog.setStockName(stockOperate.getStockName());
        operateLog.setSecuritiesAccountId(stockOperate.getSecuritiesAccountId());
        operateLog.setAccountType(stockOperate.getAccountType());
        if (operateLog.getAccountType().compareTo(0) == 0) {
            operateLog.setStockOperatePrice(stockOperate.getCommissionPrice());
            operateLog.setStockOperateNum(stockOperate.getCommissionNum());
        }
//        OperatorUserRelation operatorUserRelation = operatorUserRelationService.selectByUserId(stockOperate.getUserId());
//        if (operatorUserRelation != null && operatorUserRelation.getAdminId() != null) {
//
//            operateLog.setAdminId(operatorUserRelation.getAdminId());
//
//
//        }
        result = addItem(operateLog);

        return result;
    }

    @Override
    public Page<OperateLog> listOperateLogByRestrictWithAdminId(OperateLogRestrict operateLogRestrict) {
        Page<OperateLog> page = new Page<>();

        //获取驳回的
        page = this.listRejectedByRestrictWithAdminId(operateLogRestrict);


        if (page == null || page.getResult() == null || page.getResult().isEmpty()) {

            //获取撤单的
            page = this.listCustomerCancelByRestrictWithAdminId(operateLogRestrict);

            if (page == null || page.getResult() == null || page.getResult().isEmpty()) {

                //获取待处理的
                page = this.listRefreshByRestrictWithAdminId(operateLogRestrict);


                if (page == null || page.getResult() == null || page.getResult().isEmpty()) {
                    //拉新
                    Long userId = stockOperateService.getUnBindOperatorUserId();

                    //存在新的
                    if (userId != null) {
                        //绑定关系
                        OperatorUserRelation operatorUserRelation = new OperatorUserRelation();
                        operatorUserRelation.setAdminId(operateLogRestrict.getAdminId());
                        operatorUserRelation.setUserId(userId);
                        //关系插入成功
                        if (operatorUserRelationService.addItem(operatorUserRelation).getResultFlag()) {

                            //将该用户当前所有未处理的交易委托全部提交到操作员交易委托列表中
                            List<StockOperate> stockOperateList = stockOperateService.listFreshWithUserId(userId);
                            OperateLog operateLog = null;
                            for (StockOperate stockOperate : stockOperateList) {
                                operateLog = new OperateLog();
                                operateLog.setAdminId(operateLogRestrict.getAdminId());
                                operateLog.setUserId(userId);
                                operateLog.setOperateId(stockOperate.getOperateId());
                                operateLog.setCommissionNum(stockOperate.getCommissionNum());
                                operateLog.setCommissionPrice(stockOperate.getCommissionPrice());
                                operateLog.setCommissionPriceType(stockOperate.getCommissionPriceType());
                                operateLog.setCommissionType(stockOperate.getOperateType());
                                operateLog.setStockCode(stockOperate.getStockCode());
                                operateLog.setStockMarket(stockOperate.getStockMarket());
                                operateLog.setStockName(stockOperate.getStockName());
                                this.addItem(operateLog);
                            }

                            //获取当前待处理的数据
                            page = this.listRefreshByRestrictWithAdminId(operateLogRestrict);

                        }
                    }

                }
            }
        }
        return page;
    }

    @Override
    public GenericDao<OperateLog, Long> getDao() {
        return operateLogMapper;
    }

    @Override
    public Page<OperateLog> listRejectedByRestrictWithAdminId(OperateLogRestrict operateLogRestrict) {
        Page<OperateLog> page = new Page<>(operateLogRestrict.getPageno(), operateLogRestrict.getPagesize());
        operateLogMapper.listRejectedByRestrictWithAdminId(operateLogRestrict, page);
        return page;
    }

    @Override
    public Page<OperateLog> listRefreshByRestrictWithAdminId(OperateLogRestrict operateLogRestrict) {
        Page<OperateLog> page = new Page<>(operateLogRestrict.getPageno(), operateLogRestrict.getPagesize());
        operateLogMapper.listRefreshByRestrictWithAdminId(operateLogRestrict, page);
        return page;
    }

    @Override
    public Result updateByOperateStock(OperateLog operateLog) {
        Result result = new Result("挂单失败");

        OperateLog orginal = this.selectById(operateLog.getLogId());

        Integer commissionType = orginal.getCommissionType();

        Double commissionPrice = orginal.getCommissionPrice();

        Long commissionNum = orginal.getCommissionNum();

        Double stockOperatePrice = operateLog.getStockOperatePrice();

        Long stockOperateNum = operateLog.getStockOperateNum();

        switch (commissionType) {
            case 0://卖出
                if (commissionPrice.compareTo(stockOperatePrice) > 0) {
                    result.setResultFlag(false);
                    result.setResultMsg("卖出价格不得低于委托价格");
                    return result;
                }
                break;
            case 1://买入
                if (commissionPrice.compareTo(stockOperatePrice) < 0) {
                    result.setResultFlag(false);
                    result.setResultMsg("买入价格不的高于委托价格");
                    return result;
                }
                break;
        }

        if (stockOperateNum.compareTo(commissionNum) > 0) {
            result.setResultFlag(false);
            result.setResultMsg("交易数量不得大于委托数量");
            return result;
        }


        if (operateLogMapper.countBeforeUpdateByOperateStock(operateLog) == 0) {


            //股市数据提交成功
            if (operateLogMapper.operateStock(operateLog) > 0) {
                operateLog = this.selectById(operateLog.getLogId());
                //用户交易申请变更为挂单
                Long operateId = operateLog.getOperateId();
                StockOperate stockOperate = stockOperateService.selectById(operateId);
                stockOperate.setOperateStatus(3);
                stockOperateService.updateStockOperate(stockOperate);
                result.setResultFlag(true);
                result.setResultMsg("挂单成功");
            }

        } else {
            result.setResultFlag(false);
            result.setResultMsg("回单号异常");
        }

        return result;
    }


    @Override
    public Page<OperateLog> listAllWaitingReviewByRestrict(OperateLogRestrict operateLogRestrict) {
        Page<OperateLog> page = new Page<>(operateLogRestrict.getPageno(), operateLogRestrict.getPagesize());
        operateLogMapper.listAllWaitingReviewByRestrict(operateLogRestrict, page);
        return page;
    }

    @Override
    public Result reviewAgreeOperateStock(OperateLog operateLog) {
        Result result = new Result("更新失败");
        operateLog.setReviewStatus(1);
        if (operateLogMapper.updateByReviewOperateStock(operateLog) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("审核通过");
        }

        return result;
    }

    @Override
    public Result reviewRejectOperateStock(OperateLog operateLog) {
        Result result = new Result("更新失败");
        operateLog.setReviewStatus(2);
        if (operateLogMapper.updateByReviewOperateStock(operateLog) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("审核驳回");
        }
        return result;
    }

    @Override
    public Page<OperateLog> listAllRejectedByRestrict(OperateLogRestrict operateLogRestrict) {
        Page<OperateLog> page = new Page<>(operateLogRestrict.getPageno(), operateLogRestrict.getPagesize());
        operateLogMapper.listAllRejectedByRestrict(operateLogRestrict, page);
        return page;
    }

    @Override
    public Result confirmOperateStock(OperateLog operateLog) {
        Result result = new Result("回单确认失败");

        OperateLog orginal = this.selectById(operateLog.getLogId());

        Integer commissionType = orginal.getCommissionType();


        Double stockOperatePrice = orginal.getStockOperatePrice();

        Long stockOperateNum = orginal.getStockOperateNum();

        Double transactionPrice = operateLog.getTransactionPrice();

        Long transactionNum = operateLog.getTransactionNum();

        switch (commissionType) {
            case 0://卖出
                if (transactionPrice.compareTo(stockOperatePrice) < 0) {
                    result.setResultFlag(false);
                    result.setResultMsg("卖出价格不得低于提交价格");
                    return result;
                }
                break;
            case 1://买入
                if (transactionPrice.compareTo(stockOperatePrice) > 0) {
                    result.setResultFlag(false);
                    result.setResultMsg("买入价格不的高于提交价格");
                    return result;
                }
                break;
        }

        if (stockOperateNum.compareTo(transactionNum) < 0) {
            result.setResultFlag(false);
            result.setResultMsg("成交数量不得大于提交数量");
            return result;
        }

        if (operateLogMapper.confirmOperateStock(operateLog) > 0) {
            operateLog = this.selectById(operateLog.getLogId());
            StockOperate stockOperate = stockOperateService.selectById(operateLog.getOperateId());
            stockOperate.setTransactionPrice(operateLog.getTransactionPrice());
            stockOperate.setTransactionNum(operateLog.getTransactionNum());
            if (operateLog.getTransactionNum().compareTo(stockOperate.getCommissionNum()) == 0) {//全成交
                stockOperate.setOperateStatus(1);
            } else {//部分成交
                stockOperate.setOperateStatus(2);
            }
            if (commissionType == 1) {//买入
                if (stockOperateService.buySuccess(stockOperate)) {
                    result.setResultFlag(true);
                    result.setResultMsg("回单确认成功");
                }
            } else if (commissionType == 0) {//卖出
                if (stockOperateService.sellSuccess(stockOperate)) {
                    result.setResultFlag(true);
                    result.setResultMsg("回单确认成功");
                }
            }


        }
        return result;
    }

    @Override
    public Page<OperateLog> selectByStockOperateId(OperateLogRestrict operateLogRestrict) {
        Page<OperateLog> page = new Page<>(operateLogRestrict.getPageno(), operateLogRestrict.getPagesize());
        operateLogMapper.selectByStockOperateId(operateLogRestrict, page);
        return page;
    }

    @Override
    public Boolean updateByCustomerCancel(OperateLog operateLog) {
        return operateLogMapper.updateByCustomerCancel(operateLog) > 0;
    }

    @Override
    public Result confirmCustomerCancelApply(OperateLog operateLog) {
        Result result = new Result("撤单确认失败");
        //后台撤单成功
        if (operateLogMapper.confirmCustomerCancelApply(operateLog) > 0) {
            operateLog = this.selectById(operateLog.getLogId());
            //用户撤单数据更新
            result = stockOperateService.cancelStockOperate(operateLog.getOperateId());

        }
        return result;
    }

    @Override
    public Page<OperateLog> listCustomerCancelByRestrictWithAdminId(OperateLogRestrict operateLogRestrict) {
        Page<OperateLog> page = new Page<>(operateLogRestrict.getPageno(), operateLogRestrict.getPagesize());
        operateLogMapper.listCustomerCancelByRestrictWithAdminId(operateLogRestrict, page);
        return page;
    }

    @Override
    public Page<OperateLog> listCustomerCancelByRestrict(OperateLogRestrict operateLogRestrict) {
        Page<OperateLog> page = new Page<>(operateLogRestrict.getPageno(), operateLogRestrict.getPagesize());
        operateLogMapper.listCustomerCancelByRestrict(operateLogRestrict, page);
        return page;
    }


    @Override
    public OperateLog selectByOperateId(Long operateId) {
        return operateLogMapper.selectByOperateId(operateId);
    }


    @Override
    public Boolean isExistsOverTimeStockOperateApply(Long maxDelaySeconds) {
        return operateLogMapper.countOverTimeStockOperateApply(maxDelaySeconds) > 0;
    }


    @Override
    public Boolean isExistsOverTimeStockConfirm(Long maxDelaySeconds) {
        return operateLogMapper.countOverTimeStockConfirm(maxDelaySeconds) > 0;
    }
}
