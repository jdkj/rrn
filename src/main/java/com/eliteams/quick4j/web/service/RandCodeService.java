package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.RandCode;

/**
 * Created by Dsmart on 2016/6/29.
 */
public interface RandCodeService extends GenericService<RandCode,Long> {


    /**
     * 验证请求的有效性
     * @param randCode
     * @return
     */
    ResponseStatusEnums validRequest(RandCode randCode);


    /**
     * 获取新的短信验证码
     * @param randCode
     * @return
     */
    RandCode reqPhoneCode(RandCode randCode);

    /**
     * 验证短信验证码是否正确
     * @param randCode
     * @return
     */
    Boolean validPhoneCode(RandCode randCode);


}
