package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.StockPositionMxMapper;
import com.eliteams.quick4j.web.model.StockPosition;
import com.eliteams.quick4j.web.model.StockPositionMx;
import com.eliteams.quick4j.web.model.pagerestrict.StockPositionMxRestrict;
import com.eliteams.quick4j.web.service.StockPositionMxService;
import com.eliteams.quick4j.web.service.StockPositionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/10/31.
 */
@Service
public class StockPositionMxServiceImpl extends GenericServiceImpl<StockPositionMx,Long> implements StockPositionMxService {


    @Resource
    private StockPositionMxMapper stockPositionMxMapper;

    @Resource
    private StockPositionService stockPositionService;


    @Override
    public List<StockPositionMx> listByPositionIdAndUserIdAndStockCode(Long positionId,Long userId, String stockCode) {
        StockPositionMxRestrict stockPositionMxRestrict = new StockPositionMxRestrict();
        stockPositionMxRestrict.setUserId(userId);
        stockPositionMxRestrict.setStockCode(stockCode);
        stockPositionMxRestrict.setPositionId(positionId);
        return stockPositionMxMapper.listByRestrict(stockPositionMxRestrict);
    }

    @Override
    public GenericDao<StockPositionMx, Long> getDao() {
        return stockPositionMxMapper;
    }

    @Override
    public List<StockPositionMx> listFreshStocks() {
        return stockPositionMxMapper.listFreshStocks();
    }


    @Override
    public Boolean addItem(Long positionId, Long userId, String stockCode, String stockName, Long stockNum, Double buyPrice) {
        StockPositionMx stockPositionMx = new StockPositionMx();
        stockPositionMx.setPositionId(positionId);
        stockPositionMx.setUserId(userId);
        stockPositionMx.setStockCode(stockCode);
        stockPositionMx.setStockName(stockName);
        stockPositionMx.setStockNum(stockNum);
        stockPositionMx.setBuyPrice(buyPrice);
        return stockPositionMxMapper.insert(stockPositionMx)>0;
    }


    @Override
    public Boolean updateStockPositionMxByUnlock(StockPositionMx stockPositionMx) {
        StockPosition stockPosition = stockPositionService.selectById(stockPositionMx.getPositionId());
        stockPosition.setStockAvailableNum(stockPosition.getStockAvailableNum()+stockPositionMx.getStockNum());
        stockPositionMxMapper.updateStockPositionMxByUnlock(stockPositionMx);
        stockPositionService.updateStockPositionAvailableNum(stockPosition);
        return true;
    }
}
