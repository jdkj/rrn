package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.AgreementAiOperateStockLog;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementAiOperateStockLogRestrict;

public interface AgreementAiOperateStockLogService extends GenericService {


    /**
     * 批量添加
     *
     * @param agreementAiOperateStockLogRestrict
     * @return
     */
    Result batchAdd(AgreementAiOperateStockLogRestrict agreementAiOperateStockLogRestrict);


    /**
     * 添加
     *
     * @param agreementAiOperateStockLog
     * @return
     */
    Result addItem(AgreementAiOperateStockLog agreementAiOperateStockLog);


    /**
     * 根据请求参数获取分页列表数据
     *
     * @param agreementAiOperateStockLogRestrict
     * @return
     */
    Page<AgreementAiOperateStockLog> listByRestrict(AgreementAiOperateStockLogRestrict agreementAiOperateStockLogRestrict);


}
