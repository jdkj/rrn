package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;

/**
 * 优惠券api接口
 * Created by Dsmart on 2016/12/10.
 */
public interface CouponApiService {

    /**
     * 通过积分兑换优惠券
     * @param couponId
     * @param userId
     * @return
     */
    Result addIntegralExchangeCoupon(Long couponId, Long userId);


    /**
     * 用户注册赠送优惠券
     * @param userId
     * @return
     */
    Result addSystemGiftCouponByRegister(Long userId);

}
