package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.SMSUtils;
import com.eliteams.quick4j.web.dao.UserAccountMapper;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.*;
import com.eliteams.quick4j.web.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/10/26.
 */
@Service
public class UserAccountServiceImpl extends GenericServiceImpl<UserAccount, Long> implements UserAccountService {

    @Resource
    private UserAccountMapper userAccountMapper;

    @Resource
    private UserAccountMxService userAccountMxService;

    @Resource
    private UserIntegralMxService userIntegralMxService;

    @Resource
    private UserCouponService userCouponService;

    @Resource
    private CouponApiService couponApiService;

    @Resource
    private UserService userService;


    @Resource
    private PhoneMsgService phoneMsgService;


    @Override
    public Boolean addUserAccount(UserAccount userAccount) {
        userAccountMapper.insert(userAccount);

        //赠送优惠券
        couponApiService.addSystemGiftCouponByRegister(userAccount.getUserId());

        return true;
    }

    @Override
    public GenericDao<UserAccount, Long> getDao() {
        return userAccountMapper;
    }

    @Override
    public UserAccount getUserAccountByUserId(Long userId) {
        return userAccountMapper.selectByUserId(userId);
    }


    @Override
    public Boolean updateAccountMoney(UserAccount userAcccount) {
        return userAccountMapper.updateAccountMoney(userAcccount) > 0;
    }

    @Override
    public Boolean updateAccountIntegral(UserAccount userAccount) {
        return userAccountMapper.updateAccountIntegral(userAccount) > 0;
    }

    @Override
    public Result recharge(Long userId, Double diffMoney, Long orderId) {
        Result result = new Result("充值失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setMoney(userAccount.getMoney() + diffMoney);
        this.updateAccountMoney(userAccount);
        UserAccountMx userAccountMx = new UserAccountMx();
        userAccountMx.setChangeMoney(diffMoney);
        userAccountMx.setTotalMoney(userAccount.getMoney());
        userAccountMx.setUserId(userId);
        userAccountMx.setOrderId(orderId);
        userAccountMxService.recharge(userAccountMx);
        result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
        result.setResultMsg("充值成功");
        User user = userService.selectById(userId);
        SMSUtils.post("18659032066", user.getNickName() + "(" + user.getPhone() + ")充值" + diffMoney + "元");
        SMSUtils.post("13959702028", user.getNickName() + "(" + user.getPhone() + ")充值" + diffMoney + "元");
        return result;
    }

    @Override
    public Result withdrawals(Long userId, Double diffMoney, Long orderId) {
        Result result = new Result("提现失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        if (userAccount.getMoney() >= diffMoney) {
            userAccount.setMoney(userAccount.getMoney() - diffMoney);
            this.updateAccountMoney(userAccount);
            UserAccountMx userAccountMx = new UserAccountMx();
            userAccountMx.setChangeMoney(diffMoney);
            userAccountMx.setTotalMoney(userAccount.getMoney());
            userAccountMx.setUserId(userId);
            userAccountMx.setOrderId(orderId);
            userAccountMxService.withdrawals(userAccountMx);
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
            result.setResultMsg("提现申请成功");

            User user = userService.selectById(userId);

            phoneMsgService.addWithdrawalsApplySuccessMsg(userId, user.getPhone(), "您的提款申请" + diffMoney + "元已受理,具体到账时间以银行为准。当前账户余额" + userAccount.getMoney() + "元,请确保余额能支付您近期的合约管理费用!");
            SMSUtils.post("18659032066", user.getNickName() + "(" + user.getPhone() + ")提现" + diffMoney + "元");
            SMSUtils.post("13959702028", user.getNickName() + "(" + user.getPhone() + ")提现" + diffMoney + "元");

        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.ACCOUNT_MONEY_NOT_ENOUGH);
        }

        return result;
    }

    @Override
    public Result signAgreementLockMoney(Long userId, Double diffMoney, Long orderId) {
        Result result = new Result("操盘支出扣款失败");

        UserAccount userAccount = this.getUserAccountByUserId(userId);
        if (userAccount.getMoney() >= diffMoney) {
            userAccount.setMoney(userAccount.getMoney() - diffMoney);
            this.updateAccountMoney(userAccount);
            UserAccountMx userAccountMx = new UserAccountMx();
            userAccountMx.setChangeMoney(diffMoney);
            userAccountMx.setTotalMoney(userAccount.getMoney());
            userAccountMx.setUserId(userId);
            userAccountMx.setOrderId(orderId);
            userAccountMxService.signAgreementLockMoney(userAccountMx);
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
            result.setResultMsg("操盘支付扣款成功");
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.ACCOUNT_MONEY_NOT_ENOUGH);
        }

        return result;
    }

    @Override
    public Result relaseLockMoney(Long userId, Double diffMoney, Long orderId) {
        Result result = new Result("保证金退回失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setMoney(userAccount.getMoney() + diffMoney);
        this.updateAccountMoney(userAccount);
        UserAccountMx userAccountMx = new UserAccountMx();
        userAccountMx.setChangeMoney(diffMoney);
        userAccountMx.setTotalMoney(userAccount.getMoney());
        userAccountMx.setUserId(userId);
        userAccountMx.setOrderId(orderId);
        userAccountMxService.relaseLockMoney(userAccountMx);
        result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
        result.setResultMsg("保证金退回成功");
        return result;
    }

    @Override
    public Result increaseLockMoney(Long userId, Double diffMoney, Long orderId) {
        Result result = new Result("追加保证金失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        if (userAccount.getMoney() >= diffMoney) {
            userAccount.setMoney(userAccount.getMoney() - diffMoney);
            this.updateAccountMoney(userAccount);
            UserAccountMx userAccountMx = new UserAccountMx();
            userAccountMx.setChangeMoney(diffMoney);
            userAccountMx.setTotalMoney(userAccount.getMoney());
            userAccountMx.setUserId(userId);
            userAccountMx.setOrderId(orderId);
            userAccountMxService.increaseLockMoney(userAccountMx);
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
            result.setResultMsg("追加保证金成功");
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.ACCOUNT_MONEY_NOT_ENOUGH);
        }

        return result;
    }

    @Override
    public Result interestPayment(Long userId, Double diffMoney, Long orderId) {
        Result result = new Result("利息支出扣款失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        if (userAccount.getMoney().compareTo(diffMoney) >= 0) {
            userAccount.setMoney(userAccount.getMoney() - diffMoney);
            this.updateAccountMoney(userAccount);
            UserAccountMx userAccountMx = new UserAccountMx();
            userAccountMx.setChangeMoney(diffMoney);
            userAccountMx.setTotalMoney(userAccount.getMoney());
            userAccountMx.setUserId(userId);
            userAccountMx.setOrderId(orderId);
            userAccountMxService.interestPayment(userAccountMx);

            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
            result.setResultMsg("利息支出扣款成功");
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.ACCOUNT_MONEY_NOT_ENOUGH);
        }
        return result;
    }

    @Override
    public Result interestPaymentByCoupon(Long userId, Double diffMoney, Long agreementId, Long userCouponId) {
        Result result = new Result("利息支出扣款失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        UserCoupon userCoupon = userCouponService.getUserCouponByUserIdAndUserCouponId(userId, userCouponId);
        if (userCoupon != null && userCoupon.getCouponAvailableValue().compareTo(0.0d) > 0) {
            Double availableValue = userCoupon.getCouponAvailableValue();


            //优惠券余额够支付
            if (availableValue.compareTo(diffMoney) >= 0) {
                UserAccountMx userAccountMx = new UserAccountMx();
                userAccountMx.setChangeMoney(diffMoney);
                userAccountMx.setTotalMoney(userAccount.getMoney());
                userAccountMx.setUserId(userId);
                userAccountMx.setOrderId(agreementId);
                userAccountMxService.interestPaymentByCoupon(userAccountMx);

                userCoupon.setCouponAvailableValue(availableValue - diffMoney);
                userCouponService.updateByReduceCouponAvailableValue(userCoupon);

                result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
                result.setResultMsg("利息支出扣款成功");
            } else {

                UserAccountMx userAccountMx = new UserAccountMx();
                userAccountMx.setChangeMoney(availableValue);
                userAccountMx.setTotalMoney(userAccount.getMoney());
                userAccountMx.setUserId(userId);
                userAccountMx.setOrderId(agreementId);
                userAccountMxService.interestPaymentByCoupon(userAccountMx);

                userCoupon.setCouponAvailableValue(0d);
                userCouponService.updateByReduceCouponAvailableValue(userCoupon);

                //抵扣不足的用账户余额支付
                result = this.interestPayment(userId, diffMoney - availableValue, agreementId);


            }


        } else {
            result = this.interestPayment(userId, diffMoney, agreementId);
        }
        return result;
    }

    @Override
    public Result profitWithdrawals(Long userId, Double diffMoney, Long orderId) {
        Result result = new Result("利润提取失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setMoney(userAccount.getMoney() + diffMoney);
        this.updateAccountMoney(userAccount);
        UserAccountMx userAccountMx = new UserAccountMx();
        userAccountMx.setChangeMoney(diffMoney);
        userAccountMx.setTotalMoney(userAccount.getMoney());
        userAccountMx.setUserId(userId);
        userAccountMx.setOrderId(orderId);
        userAccountMxService.profitWithdrawals(userAccountMx);
        result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
        result.setResultMsg("利润提取成功");
        return result;
    }


    @Override
    public Result systemGiftIntegral(Long userId, Double diffIntegral, Long orderId) {
        Result result = new Result("积分赠送失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setIntegral(userAccount.getIntegral() + diffIntegral);
        this.updateAccountIntegral(userAccount);
        UserIntegralMx userIntegralMx = new UserIntegralMx();
        userIntegralMx.setChangeIntegral(diffIntegral);
        userIntegralMx.setTotalIntegral(userAccount.getIntegral());
        userIntegralMx.setUserId(userId);
        userIntegralMx.setOrderId(orderId);
        userIntegralMxService.systemGift(userIntegralMx);
        result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
        result.setResultMsg("积分赠送成功！");
        return result;
    }

    @Override
    public Result agreementRewardIntegral(Long userId, Double diffIntegral, Long orderId) {
        Result result = new Result("合约奖励失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setIntegral(userAccount.getIntegral() + diffIntegral);
        this.updateAccountIntegral(userAccount);
        UserIntegralMx userIntegralMx = new UserIntegralMx();
        userIntegralMx.setChangeIntegral(diffIntegral);
        userIntegralMx.setTotalIntegral(userAccount.getIntegral());
        userIntegralMx.setUserId(userId);
        userIntegralMx.setOrderId(orderId);
        userIntegralMxService.agreementReward(userIntegralMx);
        result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
        result.setResultMsg("合约奖励成功");
        return result;
    }

    @Override
    public Result couponExchangeIntegral(Long userId, Double diffIntegral, Long orderId) {
        Result result = new Result("优惠券兑换积分扣除失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        if (userAccount.getIntegral().compareTo(diffIntegral) >= 0) {
            userAccount.setIntegral(userAccount.getIntegral() - diffIntegral);
            this.updateAccountIntegral(userAccount);
            UserIntegralMx userIntegralMx = new UserIntegralMx();
            userIntegralMx.setChangeIntegral(diffIntegral);
            userIntegralMx.setTotalIntegral(userAccount.getIntegral());
            userIntegralMx.setUserId(userId);
            userIntegralMx.setOrderId(orderId);
            userIntegralMxService.couponExchange(userIntegralMx);
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
            result.setResultMsg("优惠券兑换积分扣除成功");
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.ACCOUNT_INTEGRAL_NOT_ENOUGH);
        }
        return result;
    }


    @Override
    public Result increaseTotalOperateFee(Long userId, Double operateFee) {
        Result result = new Result("总操盘手续费更新失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setTotalOperateFee(userAccount.getTotalOperateFee() + operateFee);
        userAccount.setTotalContributionFee(userAccount.getTotalContributionFee() + operateFee);
        userAccountMapper.updateByPrimaryKeySelective(userAccount);
        result.setResultFlag(true);
        result.setResultMsg("总操盘手续费更新成功");
        return result;
    }

    @Override
    public Result increaseTotalManagementFee(Long userId, Double managementFee) {
        Result result = new Result("总管理费用更新失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setTotalManagementFee(userAccount.getTotalManagementFee() + managementFee);
        userAccount.setTotalContributionFee(userAccount.getTotalContributionFee() + managementFee);
        userAccountMapper.updateByPrimaryKeySelective(userAccount);
        result.setResultFlag(true);
        result.setResultMsg("总管理费用更新成功");
        return result;
    }

    @Override
    public Result increaseTotalProfit(Long userId, Double profit) {
        Result result = new Result("总盈利更新失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setTotalProfit(userAccount.getTotalProfit() + profit);
        userAccountMapper.updateByPrimaryKeySelective(userAccount);
        result.setResultFlag(true);
        result.setResultMsg("总盈利更新成功");
        return result;
    }

    @Override
    public Result increaseTotalAgreementFund(Long userId, Double agreementMoney) {
        Result result = new Result("总操盘资金更新失败");
        UserAccount userAccount = this.getUserAccountByUserId(userId);
        userAccount.setTotalAgreementFund(userAccount.getTotalAgreementFund() + agreementMoney);
        userAccountMapper.updateByPrimaryKeySelective(userAccount);
        result.setResultFlag(true);
        result.setResultMsg("总操盘资金更新成功");
        return result;
    }
}
