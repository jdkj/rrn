package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.DistributionAccountMxMapper;
import com.eliteams.quick4j.web.model.DistributionAccountMx;
import com.eliteams.quick4j.web.service.DistributionAccountMxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2017/3/12.
 */
@Service
public class DistributionAccountMxServiceImpl extends GenericServiceImpl<DistributionAccountMx, Long> implements DistributionAccountMxService {

    @Resource
    private DistributionAccountMxMapper distributionAccountMxMapper;

    @Override
    public Boolean addItem(DistributionAccountMx distributionAccountMx) {
        return distributionAccountMxMapper.insert(distributionAccountMx) > 0;
    }

    @Override
    public GenericDao<DistributionAccountMx, Long> getDao() {
        return distributionAccountMxMapper;
    }

    @Override
    public Boolean addByIncreaseCommissionMoney(Long distributionUserId, Long orderId, Double totalMoney, Double changeMoney) {
        DistributionAccountMx distributionAccountMx = new DistributionAccountMx();
        distributionAccountMx.setDistributionUserId(distributionUserId);
        distributionAccountMx.setOrderId(orderId);
        distributionAccountMx.setOrderType(1);
        distributionAccountMx.setTotalMoney(totalMoney);
        distributionAccountMx.setChangeMoney(changeMoney);
        return addItem(distributionAccountMx);
    }

    @Override
    public Boolean addByWithdrawalCommissionMoney(Long distributionUserId, Long orderId, Double totalMoney, Double changeMoney) {
        DistributionAccountMx distributionAccountMx = new DistributionAccountMx();
        distributionAccountMx.setDistributionUserId(distributionUserId);
        distributionAccountMx.setOrderId(orderId);
        distributionAccountMx.setOrderType(0);
        distributionAccountMx.setTotalMoney(totalMoney);
        distributionAccountMx.setChangeMoney(changeMoney);
        return addItem(distributionAccountMx);
    }
}
