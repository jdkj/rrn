package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.DistributionUser;
import com.eliteams.quick4j.web.model.pagerestrict.DistributionUserRestrict;

public interface DistributionUserService extends GenericService<DistributionUser, Long> {

    /**
     * 用户登陆
     *
     * @param distributionUserRestrict
     * @return
     */
    DistributionUser authentication(DistributionUserRestrict distributionUserRestrict);

    /**
     * @param distributionUser
     * @return
     */
    DistributionUser authentication(DistributionUser distributionUser);

    /**
     * 根据账户号查找用户
     *
     * @param accountNum
     * @return
     */
    DistributionUser selectDistributionUserByAccountNum(Long accountNum);

    /**
     * 根据id查找用户
     *
     * @param distributionUserId
     * @return
     */
    DistributionUser selectDistributionUserById(Long distributionUserId);


    /**
     * 根据用户系统用户编号获取对应的分销帐号信息
     *
     * @param userId
     * @return
     */
    DistributionUser selectByUserId(Long userId);


    /**
     * 更新用户密码
     *
     * @param distributionUserRestrict
     * @return
     */
    DistributionUser upAccountPasswordById(DistributionUserRestrict distributionUserRestrict);


    /**
     * @param distributionUser
     * @return
     */
    Boolean updateByPrimaryKey(DistributionUser distributionUser);


    /**
     * 添加账户信息
     *
     * @param distributionUser
     * @return
     */
    Result addItem(DistributionUser distributionUser);

    /**
     * 绑定分销关系
     *
     * @param userId
     * @param distributionId
     * @return
     */
    Result addDistributionRelation(Long userId, Long distributionId);


    /**
     * 根据请求参数获取
     *
     * @param distributionUserRestrict
     * @return
     */
    Page<DistributionUser> listByRestrict(DistributionUserRestrict distributionUserRestrict);


    /**
     * 变更分销所属层级
     *
     * @param distributionUserId
     * @param commissionLevel
     * @return
     */
    Result changeUserCommissionLevel(Long distributionUserId, Integer commissionLevel);

}
