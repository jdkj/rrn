package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.SystemParamMapper;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.SystemParam;
import com.eliteams.quick4j.web.service.SystemParamService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/11/29.
 */
@Service
public class SystemParamServiceImpl extends GenericServiceImpl<SystemParam,Long> implements SystemParamService {

    @Resource
    private SystemParamMapper systemParamMapper;

    @Override
    public Result addItem(SystemParam systemParam) {
        Result result = new Result("添加失败");
        //已经存在该参数  提示添加失败 而不是默认转更新
        if(getByParam(systemParam.getParam())!=null){
            result.setResultFlag(false);
            result.setResultMsg("参数已经存在");
        }else{
            systemParamMapper.insert(systemParam);
            result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_SUCCESS);
        }
        return result;
    }

    @Override
    public GenericDao<SystemParam, Long> getDao() {
        return systemParamMapper;
    }

    @Override
    public SystemParam getByParam(String param) {
        return systemParamMapper.selectByParam(param);
    }

    @Override
    public List<SystemParam> listAll() {
        return systemParamMapper.listAll();
    }

    @Override
    public Boolean updateItem(SystemParam systemParam) {
        return systemParamMapper.update(systemParam)>0;
    }
}
