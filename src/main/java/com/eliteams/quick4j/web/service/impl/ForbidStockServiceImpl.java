package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.ForbidStockMapper;
import com.eliteams.quick4j.web.model.ForbidStock;
import com.eliteams.quick4j.web.service.ForbidStockService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/10/17.
 */
@Service
public class ForbidStockServiceImpl extends GenericServiceImpl<ForbidStock,Long> implements ForbidStockService {


    @Resource
    private ForbidStockMapper forbidStockMapper;


    @Override
    public List<ForbidStock> getForbidStocks() {
        return forbidStockMapper.list();
    }

    @Override
    public GenericDao<ForbidStock, Long> getDao() {
        return forbidStockMapper;
    }
}
