/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.JSONResult;
import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.web.model.pagerestrict.WxPayRestrict;

/**
 * @description
 * @version 
 * @author Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 
 */
public interface WxPayService {
    /**
     * 微信jsapi支付调起插件支付需要返回的数据
     *
     * @param wxPayRestrict
     * @return
     */
    public JSONResult jsAPIWxPay(WxPayRestrict wxPayRestrict);


    /**
     * 微信扫码支付
     * @param wxPayRestrict
     * @return
     */
    public Result scanWxPay(WxPayRestrict wxPayRestrict);


    /**
     * 查询订单支付结果
     *
     * @param wxPayRestrict
     * @return
     */
    public JSONResult orderQuery(WxPayRestrict wxPayRestrict);

    
}
