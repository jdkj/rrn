package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetailStocks;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailStocksRestrict;

import java.util.List;

/**
 * Created by Dsmart on 2017/4/11.
 */
public interface SecuritiesCompanyMoneyAccountDetailStocksService extends GenericService<SecuritiesCompanyMoneyAccountDetailStocks, Long> {


    /**
     * 根据券商账户编号与股票编码获取持仓数据
     *
     * @param securitiesAccountId
     * @param stockCode
     * @return
     */
    SecuritiesCompanyMoneyAccountDetailStocks selectBySecuritiesAccountIdAndStockCode(Long securitiesAccountId, String stockCode);


    /**
     * 根据股票编码获取持仓数据
     *
     * @param stockCode
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetailStocks> listByStockCode(String stockCode);


    /**
     * 根据股票编码分组获取所有持有股票
     *
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetailStocks> listByGroupStockCode();


    /**
     * 根据请求参数获取分页数据
     *
     * @param securitiesCompanyMoneyAccountDetailStocksRestrict
     * @return
     */
    Page<SecuritiesCompanyMoneyAccountDetailStocks> listByRestrict(SecuritiesCompanyMoneyAccountDetailStocksRestrict securitiesCompanyMoneyAccountDetailStocksRestrict);


    /**
     * 添加或更新数据
     *
     * @param securitiesCompanyMoneyAccountDetailStocks
     * @return
     */
    Result addOrUpdate(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks);

    /**
     * 减少或清除数据
     *
     * @param securitiesCompanyMoneyAccountDetailStocks
     * @return
     */
    Result removeOrDelete(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks);


    /**
     * 股票分红派股跟新数据
     *
     * @param securitiesCompanyMoneyAccountDetailStocks
     * @return
     */
    Boolean updateByDividend(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks);


    /**
     * 股票数据更新
     *
     * @param securitiesCompanyMoneyAccountDetailStocks
     * @return
     */
    Boolean updateByRefresh(SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks);


    /**
     * 根据券商账户编号获取所持有股票
     *
     * @param securitiesAccountId
     * @return
     */
    List<SecuritiesCompanyMoneyAccountDetailStocks> listAllBySecuritiesAccountId(Long securitiesAccountId);


}
