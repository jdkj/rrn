package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.StockOperateTransactionLog;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateTransactionLogRestrict;

/**
 * Created by Dsmart on 2017/5/31.
 */
public interface StockOperateTransactionLogService extends GenericService<StockOperateTransactionLog, Long> {


    /**
     * 交易完成插入记录
     *
     * @param stockOperateTransactionLog
     * @return
     */
    Result addByFinishOperate(StockOperateTransactionLog stockOperateTransactionLog);


    /**
     * 交易取消插入记录
     *
     * @param stockOperateTransactionLog
     * @return
     */
    Result addByCancelOperate(StockOperateTransactionLog stockOperateTransactionLog);


    /**
     * 根据合约编号获取当日的成交记录
     *
     * @param stockOperateTransactionLogRestrict
     * @return
     */
    Page<StockOperateTransactionLog> listCurrentDayDealsByRestrictWithAgreementId(StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict);


    /**
     * 根据合约编号获取历史成交记录
     *
     * @param stockOperateTransactionLogRestrict
     * @return
     */
    Page<StockOperateTransactionLog> listHistoryDealsByRestrictWithAgreementId(StockOperateTransactionLogRestrict stockOperateTransactionLogRestrict);


}
