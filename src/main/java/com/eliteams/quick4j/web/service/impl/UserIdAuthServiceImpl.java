package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.UserIdAuthMapper;
import com.eliteams.quick4j.web.model.UserIdAuth;
import com.eliteams.quick4j.web.service.UserIdAuthService;
import com.eliteams.quick4j.web.service.UserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/10/23.
 */
@Service
public class UserIdAuthServiceImpl extends GenericServiceImpl<UserIdAuth, Long> implements UserIdAuthService {

    @Resource
    private UserIdAuthMapper userIdAuthMapper;

    @Resource
    private UserService userService;

    @Override
    public Boolean addItem(UserIdAuth userIdAuth) {
        userIdAuthMapper.insert(userIdAuth);
        userService.updateByIDAuthcated(userIdAuth.getUserId());
        return true;
    }

    @Override
    public GenericDao<UserIdAuth, Long> getDao() {
        return userIdAuthMapper;
    }

    @Override
    public UserIdAuth getAuthInfoByUserId(Long userId) {
        return userIdAuthMapper.selectByUserId(userId);
    }
}
