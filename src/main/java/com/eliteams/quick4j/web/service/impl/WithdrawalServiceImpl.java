package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.WithdrawalMapper;
import com.eliteams.quick4j.web.model.Withdrawal;
import com.eliteams.quick4j.web.model.pagerestrict.WithdrawalRestrict;
import com.eliteams.quick4j.web.service.UserAccountService;
import com.eliteams.quick4j.web.service.WithdrawalService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2017/3/6.
 */
@Service
public class WithdrawalServiceImpl extends GenericServiceImpl<Withdrawal, Long> implements WithdrawalService {

    @Resource
    private WithdrawalMapper withdrawalMapper;

    @Resource
    private UserAccountService userAccountService;

    @Override
    public GenericDao<Withdrawal, Long> getDao() {
        return withdrawalMapper;
    }

    @Override
    public Result addItem(Withdrawal withdrawal) {
        Result result = new Result("提现申请失败");
        if (withdrawalMapper.insert(withdrawal) > 0) {
            result = userAccountService.withdrawals(withdrawal.getUserId(), withdrawal.getMoney(), withdrawal.getWithdrawalId());
        }
        return result;
    }

    @Override
    public Page<Withdrawal> listByUserId(WithdrawalRestrict withdrawalRestrict) {
        Page<Withdrawal> page = new Page<>(withdrawalRestrict.getPageno(), withdrawalRestrict.getPagesize());
        withdrawalMapper.listByUserId(withdrawalRestrict, page);
        return page;
    }
}
