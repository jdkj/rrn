package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.CouponStoreMapper;
import com.eliteams.quick4j.web.model.CouponStore;
import com.eliteams.quick4j.web.service.CouponStoreService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/12/7.
 */
@Service
public class CouponStoreServiceImpl extends GenericServiceImpl<CouponStore,Long> implements CouponStoreService {


    @Resource
    private CouponStoreMapper couponStoreMapper;


    @Override
    public List<CouponStore> listAllExchangeableCouponStore() {
        return couponStoreMapper.listAllExchangeableCouponStore();
    }

    @Override
    public GenericDao<CouponStore, Long> getDao() {
        return couponStoreMapper;
    }
}
