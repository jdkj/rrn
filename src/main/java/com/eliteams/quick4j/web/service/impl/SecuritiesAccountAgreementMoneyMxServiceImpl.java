package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.SecuritiesAccountAgreementMoneyMxMapper;
import com.eliteams.quick4j.web.model.SecuritiesAccountAgreementMoneyMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountAgreementMoneyMxRestrict;
import com.eliteams.quick4j.web.service.SecuritiesAccountAgreementMoneyMxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SecuritiesAccountAgreementMoneyMxServiceImpl extends GenericServiceImpl<SecuritiesAccountAgreementMoneyMx, Long> implements SecuritiesAccountAgreementMoneyMxService {

    @Resource
    private SecuritiesAccountAgreementMoneyMxMapper securitiesAccountAgreementMoneyMxMapper;


    @Override
    public GenericDao<SecuritiesAccountAgreementMoneyMx, Long> getDao() {
        return securitiesAccountAgreementMoneyMxMapper;
    }


    @Override
    public Result addByIncreaseTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney) {
        Result result = new Result("记录失败");
        SecuritiesAccountAgreementMoneyMx securitiesAccountAgreementMoneyMx = new SecuritiesAccountAgreementMoneyMx();
        securitiesAccountAgreementMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountAgreementMoneyMx.setOrderType(4);
        securitiesAccountAgreementMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountAgreementMoneyMx.setChangeMoney(changeMoney);
        if (securitiesAccountAgreementMoneyMxMapper.insert(securitiesAccountAgreementMoneyMx) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Result addByReduceTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney) {
        Result result = new Result("记录失败");
        SecuritiesAccountAgreementMoneyMx securitiesAccountAgreementMoneyMx = new SecuritiesAccountAgreementMoneyMx();
        securitiesAccountAgreementMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountAgreementMoneyMx.setOrderType(3);
        securitiesAccountAgreementMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountAgreementMoneyMx.setChangeMoney(changeMoney);
        if (securitiesAccountAgreementMoneyMxMapper.insert(securitiesAccountAgreementMoneyMx) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Result addBySignAgreement(Long securitiesAccountId, Double totalMoney, Double changeMoney, Long agreementId) {
        Result result = new Result("记录失败");
        SecuritiesAccountAgreementMoneyMx securitiesAccountAgreementMoneyMx = new SecuritiesAccountAgreementMoneyMx();
        securitiesAccountAgreementMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountAgreementMoneyMx.setOrderType(0);
        securitiesAccountAgreementMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountAgreementMoneyMx.setChangeMoney(changeMoney);
        securitiesAccountAgreementMoneyMx.setAgreementId(agreementId);
        if (securitiesAccountAgreementMoneyMxMapper.insert(securitiesAccountAgreementMoneyMx) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Result addByReleaseAgreement(Long securitiesAccountId, Double totalMoney, Double changeMoney, Long agreementId) {
        Result result = new Result("记录失败");
        SecuritiesAccountAgreementMoneyMx securitiesAccountAgreementMoneyMx = new SecuritiesAccountAgreementMoneyMx();
        securitiesAccountAgreementMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountAgreementMoneyMx.setOrderType(1);
        securitiesAccountAgreementMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountAgreementMoneyMx.setChangeMoney(changeMoney);
        securitiesAccountAgreementMoneyMx.setAgreementId(agreementId);
        if (securitiesAccountAgreementMoneyMxMapper.insert(securitiesAccountAgreementMoneyMx) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }


    @Override
    public Page<SecuritiesAccountAgreementMoneyMx> listByRestrict(SecuritiesAccountAgreementMoneyMxRestrict securitiesAccountAgreementMoneyMxRestrict) {
        Page<SecuritiesAccountAgreementMoneyMx> page = new Page<>(securitiesAccountAgreementMoneyMxRestrict.getPageno(), securitiesAccountAgreementMoneyMxRestrict.getPagesize());
        securitiesAccountAgreementMoneyMxMapper.listByRestrict(securitiesAccountAgreementMoneyMxRestrict, page);
        return page;
    }
}
