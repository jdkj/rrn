package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.DistributionAccountMx;

/**
 * Created by Dsmart on 2017/3/12.
 */
public interface DistributionAccountMxService extends GenericService<DistributionAccountMx, Long> {

    /**
     * 添加
     *
     * @param distributionAccountMx
     * @return
     */
    Boolean addItem(DistributionAccountMx distributionAccountMx);


    /**
     * 佣金增加添加记录
     *
     * @param distributionUserId
     * @param orderId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Boolean addByIncreaseCommissionMoney(Long distributionUserId, Long orderId, Double totalMoney, Double changeMoney);

    /**
     * 佣金提现增加记录
     *
     * @param distributionUserId
     * @param orderId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Boolean addByWithdrawalCommissionMoney(Long distributionUserId, Long orderId, Double totalMoney, Double changeMoney);

}
