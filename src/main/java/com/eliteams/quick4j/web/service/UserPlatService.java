/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.UserPlat;

/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time
 */
public interface UserPlatService extends GenericService<UserPlat, Long> {

    /**
     * 添加平台用户信息
     *
     * @param userPlat
     * @return
     */
    public Boolean addUserPlat(UserPlat userPlat);

    /**
     * 更新平台用户信息中的用户编号
     *
     * @param userPlat
     * @return
     */
    public Boolean updateUserPlatUserId(UserPlat userPlat);

    /**
     * 根据openId获取用户平台信息
     *
     * @param openId
     * @return
     */
    public UserPlat getWxUserPlatByOpenId(String openId);

    /**
     * 根据用户编号获取用户平台信息
     *
     * @param userId
     * @return
     */
    public UserPlat getWxUserPlatByUserId(Long userId);

}
