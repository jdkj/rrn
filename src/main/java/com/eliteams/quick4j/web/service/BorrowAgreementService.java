package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.BorrowAgreement;

import java.util.List;

/**
 * Created by Dsmart on 2016/10/18.
 */
public interface BorrowAgreementService extends GenericService<BorrowAgreement,Long> {

    /**
     * 添加
     * @param borrowAgreement
     * @return
     */
    public Boolean addBorrowAgreement(BorrowAgreement borrowAgreement);


    /**
     * 根据合约类型获取合同的倍数以及对应的费率
     * @param borrowType  合约类型
     * @return
     */
    public List<BorrowAgreement> listBorrowAgreement(Integer borrowType);


    /**
     * 获取当前存在的合约类型
     * @return
     */
    public List<BorrowAgreement> listActivityBorrowType();

    /**
     * 根据合约类别编号获取合约信息
     * @param borrowType
     * @return
     */
    BorrowAgreement getBorrowTypeByType(Integer borrowType);

}
