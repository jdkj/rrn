package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.StockMarketingUtils;
import com.eliteams.quick4j.web.container.ForbidStockContainer;
import com.eliteams.quick4j.web.container.MarketingContainer;
import com.eliteams.quick4j.web.dao.StockOperateMapper;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.*;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateRestrict;
import com.eliteams.quick4j.web.service.*;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/11/3.
 */
@Service
public class StockOperateServiceImpl extends GenericServiceImpl<StockOperate, Long> implements StockOperateService {


    @Resource
    private StockOperateMapper stockOperateMapper;

    @Resource
    private AgreementService agreementService;

    @Resource
    private StockPositionService stockPositionService;

    @Resource
    private StockMarketService stockMarketService;

    @Resource
    private OperateLogService operateLogService;

    @Resource
    private BorrowAgreementService borrowAgreementService;

    @Resource
    private SecuritiesCompanyMoneyAccountDetailService securitiesCompanyMoneyAccountDetailService;

    @Resource
    private SecuritiesCompanyMoneyAccountDetailStocksService securitiesCompanyMoneyAccountDetailStocksService;


    @Override
    public GenericDao<StockOperate, Long> getDao() {
        return stockOperateMapper;
    }


    @Override
    public Page<StockOperate> listAllWaitingStockOperateOfAgreement(StockOperateRestrict stockOperateRestrict) {
        Page<StockOperate> page = new Page<>(stockOperateRestrict.getPageno(), stockOperateRestrict.getPagesize());
        stockOperateMapper.listAllWaitingStockOperateOfAgreement(stockOperateRestrict, page);
        return page;
    }


    @Override
    public int countWaitingStockOperateOfAgreement(Long agreementId) {
        return stockOperateMapper.countWaitingStockOperateOfAgreement(agreementId);
    }

    @Override
    public Page<StockOperate> listCurrentDayDeals(StockOperateRestrict stockOperateRestrict) {
        Page<StockOperate> page = new Page<>(stockOperateRestrict.getPageno(), stockOperateRestrict.getPagesize());
        stockOperateMapper.listCurrentDayDeals(stockOperateRestrict, page);
        return page;
    }

    @Override
    public Page<StockOperate> listCurrentDayCommissions(StockOperateRestrict stockOperateRestrict) {
        Page<StockOperate> page = new Page<>(stockOperateRestrict.getPageno(), stockOperateRestrict.getPagesize());
        stockOperateMapper.listCurrentDayCommissions(stockOperateRestrict, page);
        return page;
    }

    @Override
    public Page<StockOperate> listHistoryDeals(StockOperateRestrict stockOperateRestrict) {
        Page<StockOperate> page = new Page<>(stockOperateRestrict.getPageno(), stockOperateRestrict.getPagesize());
        stockOperateMapper.listHistoryDeals(stockOperateRestrict, page);
        return page;
    }

    @Override
    public Page<StockOperate> listHistoryCommissions(StockOperateRestrict stockOperateRestrict) {
        Page<StockOperate> page = new Page<>(stockOperateRestrict.getPageno(), stockOperateRestrict.getPagesize());
        stockOperateMapper.listHistoryCommissions(stockOperateRestrict, page);
        return page;
    }

    @Override
    public Result cancelStockOperateApply(Long operateId) {
        Result result = new Result("撤单失败");
        if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringStockEntrustTime()) {
            //撤单后需要将数据逻辑回滚
            StockOperate stockOperate = this.selectById(operateId);
            String stockMarket = stockOperate.getStockMarket();

            if (!StockMarketingUtils.isDuringUncancelableTime(stockMarket)) {
                if (stockOperate.getOperateStatus().compareTo(3) == 0) {
                    //先查看是否进入操盘员列表
                    OperateLog operateLog = operateLogService.selectByOperateId(operateId);
                    operateLogService.updateByCustomerCancel(operateLog);
                    stockOperateMapper.cancelStockOperateApply(operateId);//状态变更为撤单中
                    result.setResultFlag(true);
                    result.setResultMsg("撤单请求提交成功");
                } else {
                    result.setResultFlag(false);
                    result.setResultMsg("只有挂单时才可撤单");
                }

            } else {
                result.setResultFlag(false);
                result.setResultMsg("集合竞价撮合阶段，不可撤单");
            }


        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.STOCK_MARKET_IS_NOT_OPENING);
        }
        return result;
    }

    @Override
    public Result cancelStockOperate(Long operateId) {
        StockOperate stockOperate = this.selectById(operateId);
        return cancelOperateRollBackData(stockOperate);
    }

    @Override
    public Result operateBuy(StockOperateRestrict stockOperateRestrict) {
        Result result = new Result("买入失败");

        if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringStockEntrustTime()) {

            //
            String stockCode = stockOperateRestrict.getStockCode();

            //首先判断该购买的股票是否是禁止购买的
            if (!ForbidStockContainer.isForbiden(stockCode)) {//非限购股票
                //用户信息
                Long userId = stockOperateRestrict.getUserId();

                Long agreementId = stockOperateRestrict.getAgreementId();

                Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(userId, agreementId);


                if (agreement == null || agreement.getStatus() != 0) {
                    result.setResultMsg("合约异常，无法买入");
                    return result;
                }

                //当前订单是否已经禁止买入
                if (agreement.getForbidBuy() == 1) {
                    //触发预警   禁止买入
                    result.setResponseStatusEnums(ResponseStatusEnums.AGREEMENT_IS_FORBID_BUY);
                    return result;
                }

                StockMarket stockMarket = stockMarketService.stockInfo(stockCode);

                if (stockMarket == null) {
                    result.setResultMsg("股票信息异常");
                    return result;
                }

                if (stockMarket.getName().contains("S")) {
                    result.setResultFlag(false);
                    result.setResultMsg("不可交易基金、S、ST、*ST、S*ST及SST类股票");
                    return result;
                }

                if (stockMarket.getName().contains("N")) {
                    result.setResultFlag(false);
                    result.setResultMsg("不得购买首日上市新股（或复牌首日股票）等当日不设涨跌停板限制的股票");
                    return result;
                }


                if (stockMarket.getOpenPrice().compareTo(0d) == 0) {
                    result.setResultFlag(false);
                    result.setResultMsg("股票停牌，不可交易！");
                    return result;
                }


                if (stockMarket.getCode().startsWith("3")) {
                    result.setResultFlag(false);
                    result.setResultMsg("当前不可交易创业板！");
                    return result;
                }

                /**********************************
                 * 单票持仓限制
                 *start
                 ************************************/
                //单票持仓限制
                BorrowAgreement borrowAgreement = borrowAgreementService.getBorrowTypeByType(agreement.getSignAgreementType());

                if (stockMarket.getCode().startsWith("3")) {//创业板

                } else {//主板
                    Double mainBoard = borrowAgreement.getMainBoard();
                    if (mainBoard.compareTo(100d) < 0) {//最大单票持仓小于100
                        //合约申请时净总资产
                        Double total = agreement.getBorrowMoney() + agreement.getLockMoney();

                        //先查看当前申请购买股票所占比率是否超限
                        Double totalExpenses = stockMarket.getNowPrice() * stockOperateRestrict.getCommissionNum();

                        if (totalExpenses.compareTo(total * mainBoard / 100) > 0) {
                            result.setResultMsg("买入失败!风控提示:主板单票持仓最大" + mainBoard + "%");
                            result.setResultFlag(false);
                            return result;
                        }


                        //查看未完成的该股票与预购股票占比
                        List<StockOperate> stockOperateList = getUnFinishedStockOperateByAgreementIdAndStockCode(agreementId, stockCode);

                        if (stockOperateList != null && !stockOperateList.isEmpty()) {
                            for (StockOperate stockOperate : stockOperateList) {

                                if (stockOperate != null) {

                                    totalExpenses = totalExpenses + stockOperate.getCommissionNum() * stockOperate.getCommissionPrice();
                                }
                            }

                            if (totalExpenses.compareTo(total * mainBoard / 100) > 0) {
                                result.setResultMsg("买入失败!风控提示:主板单票持仓最大" + mainBoard + "%");
                                result.setResultFlag(false);
                                return result;
                            }
                        }


                        //再查看计算当前所持有该股票与预购股票占比
                        StockPosition stockPosition = stockPositionService.getStockPositionByAgreementIdAndUserIdAndStockCode(agreementId, agreement.getUserId(), stockCode);

                        if (stockPosition != null) {
                            totalExpenses = totalExpenses + stockPosition.getBuyTotalExpenses();


                            if (totalExpenses.compareTo(total * mainBoard / 100) > 0) {
                                result.setResultMsg("买入失败!风控提示:主板单票持仓最大" + mainBoard + "%");
                                result.setResultFlag(false);
                                return result;
                            }

                        }


                    }

                }
                /**********************************
                 * 单票持仓限制
                 *end
                 ************************************/


                //市价委托买入  则将委托价格设置为涨停价
                if (stockOperateRestrict.getCommissionType() == 1) {
                    stockOperateRestrict.setCommissionPrice(stockMarket.getMaxPrice());
                }

                //委托价格
                Double commissionPrice = stockOperateRestrict.getCommissionPrice();


                //委托数量
                Long commissionNum = stockOperateRestrict.getCommissionNum();
                //校验委托价格
                if (commissionPrice < stockMarket.getMinPrice() || commissionPrice > stockMarket.getMaxPrice()) {
                    result.setResultMsg("委托价格必须介于涨停与跌停之间");
                    return result;
                }


                StockOperate stockOperate = new StockOperate(agreementId, userId, stockCode, stockMarket.getName(), stockMarket.getMarket(), stockOperateRestrict.getCommissionPrice(), stockOperateRestrict.getCommissionNum(), 1, agreement.getOperateRate());

                //判断合约余额是否可以支付此次的股票交易操作
                if (agreement.getAvailableCredit() < stockOperate.getLockMoney()) {
                    result.setResultMsg("可用余额不足,还需" + (stockOperate.getLockMoney() - agreement.getAvailableCredit()));
                    return result;
                }

                //市价委托
                if (stockOperateRestrict.getCommissionType() == 1) {
                    stockOperate.setCommissionPriceType(2);
                }
                stockOperate.setSecuritiesAccountId(agreement.getSecuritiesAccountId());
                //判断券商账户余额是否可以支付此次的股票交易操作
                SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getByPrimaryKey(agreement.getSecuritiesAccountId());
                Double securitiesAccountAvailableMoney = securitiesCompanyMoneyAccountDetail.getAvailableMoney();
                //当前券商账户无法支付此次股票交易
                if (securitiesAccountAvailableMoney.compareTo(stockOperate.getLockMoney()) < 0) {
                    //将该笔交易转到虚盘
                    securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getMaxAvailableItemFromVisual();
                    stockOperate.setSecuritiesAccountId(securitiesCompanyMoneyAccountDetail.getSecuritiesAccountId());
                    //设置交易方式为虚盘
                    stockOperate.setAccountType(0);
                    stockOperate.setOperateStatus(3);
                    stockOperate.setProgressStatus(1);
                } else {
                    stockOperate.setAccountType(securitiesCompanyMoneyAccountDetail.getAccountType());
                    if (securitiesCompanyMoneyAccountDetail.getAccountType().compareTo(0) == 0) {
                        stockOperate.setOperateStatus(3);
                        stockOperate.setProgressStatus(1);
                    }
                }
                securitiesCompanyMoneyAccountDetail.setPreBuyLockMoney(securitiesCompanyMoneyAccountDetail.getPreBuyLockMoney() + stockOperate.getLockMoney());//锁定该笔交易资金
                securitiesCompanyMoneyAccountDetail.setAvailableMoney(securitiesCompanyMoneyAccountDetail.getAvailableMoney() - stockOperate.getLockMoney());//同步减少该账户下可用于股票交易资金


                stockOperateMapper.insert(stockOperate);
                if (stockOperate.getAccountType().compareTo(0) == 0) {//如果挂虚盘 状态立刻更新
                    this.updateStockOperate(stockOperate);
                }
                agreement.setAvailableCredit(agreement.getAvailableCredit() - stockOperate.getLockMoney());//可用额度减少
                agreement.setPrebuyLockMoney(agreement.getPrebuyLockMoney() + stockOperate.getLockMoney());//预购资金锁定
                agreementService.updateAgreementByOperateStock(agreement);

                //写入股市委托记录
                operateLogService.addItemByUserOperateStockApply(stockOperate);
                result.setResultFlag(true);
                securitiesCompanyMoneyAccountDetailService.update(securitiesCompanyMoneyAccountDetail);
                result.setResultMsg("委托成功");
            } else {
                //限购股票
                result.setResponseStatusEnums(ResponseStatusEnums.STOCK_IS_FORBID);
            }

        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.STOCK_MARKET_IS_NOT_OPENING);//休市无法交易
        }


        return result;
    }

    @Override
    public Result operateSell(StockOperateRestrict stockOperateRestrict) {

        Result result = new Result("卖出失败");

        //是否开市
        if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringStockEntrustTime()) {


            //用户编号
            Long userId = stockOperateRestrict.getUserId();

            //合约编号
            Long agreementId = stockOperateRestrict.getAgreementId();


            //委托股数
            Long commissionNum = stockOperateRestrict.getCommissionNum();

            //股票代码
            String stockCode = stockOperateRestrict.getStockCode();


            Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(userId, agreementId);

            if (commissionNum.compareTo(0l) == 0) {
                result.setResultMsg("委托股数不能为0");
                return result;
            }


            if (agreement == null || agreement.getStatus() != 0) {
                result.setResultMsg("合约异常，无法交易");
                return result;
            }

            StockPosition stockPosition = stockPositionService.getStockPositionByAgreementIdAndUserIdAndStockCode(agreementId, userId, stockCode);

            //用户该合约下是否持有该股票
            if (stockPosition == null) {
                result.setResultMsg("未持有该股票,无法进行卖出操作");
                return result;
            }

            if (stockPosition.getStockAvailableNum() < commissionNum) {
                result.setResultMsg("最多可售股票" + stockPosition.getStockAvailableNum());
                return result;
            }

            StockMarket stockMarket = stockMarketService.stockInfo(stockCode);

            if (stockMarket.getOpenPrice().compareTo(0d) == 0) {
                result.setResultFlag(false);
                result.setResultMsg("股票停牌，不可交易！");
                return result;
            }


            //市价委托卖出  则将委托价格设置为跌停价
            if (stockOperateRestrict.getCommissionType() == 1) {
                stockOperateRestrict.setCommissionPrice(stockMarket.getMinPrice());
            }

            //委托价格
            Double commissionPrice = stockOperateRestrict.getCommissionPrice();

            //校验委托价格
            if (commissionPrice < stockMarket.getMinPrice() || commissionPrice > stockMarket.getMaxPrice()) {
                result.setResultMsg("委托价格必须介于涨停与跌停之间");
                return result;
            }

            StockOperate stockOperate = new StockOperate(agreementId, userId, stockCode, stockMarket.getName(), stockMarket.getMarket(), stockOperateRestrict.getCommissionPrice(), stockOperateRestrict.getCommissionNum(), 0, agreement.getOperateRate());

            //市价委托卖出
            if (stockOperateRestrict.getCommissionType() == 1) {
                stockOperate.setCommissionPriceType(2);//市价委托
            }

            stockPosition.setStockAvailableNum(stockPosition.getStockAvailableNum() - commissionNum);
            stockPositionService.updateStockPositionAvailableNum(stockPosition);
            stockOperate.setSecuritiesAccountId(agreement.getSecuritiesAccountId());

            SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getByPrimaryKey(stockOperate.getSecuritiesAccountId());
            //判断该券商账户下是否有这些股票
            SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks = securitiesCompanyMoneyAccountDetailStocksService.selectBySecuritiesAccountIdAndStockCode(agreement.getSecuritiesAccountId(), stockCode);
            //非虚盘券商账户下未持有足额的该股票
            //将该笔交易绑定到虚拟账户下
            if (securitiesCompanyMoneyAccountDetail.getAccountType().compareTo(0) != 0 && (securitiesCompanyMoneyAccountDetailStocks == null || securitiesCompanyMoneyAccountDetailStocks.getStockAvailableNum().compareTo(commissionNum) < 0)) {
                securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getMaxAvailableItemFromVisual();
                stockOperate.setSecuritiesAccountId(securitiesCompanyMoneyAccountDetail.getSecuritiesAccountId());
                stockOperate.setAccountType(0);
                stockOperate.setOperateStatus(3);
                stockOperate.setProgressStatus(1);
            } else {
                stockOperate.setAccountType(securitiesCompanyMoneyAccountDetail.getAccountType());
                if (securitiesCompanyMoneyAccountDetail.getAccountType().compareTo(0) == 0) {
                    stockOperate.setOperateStatus(3);
                    stockOperate.setProgressStatus(1);
                }
            }

            //同步锁定对应券商账户可售股票余额
            securitiesCompanyMoneyAccountDetailStocks = securitiesCompanyMoneyAccountDetailStocksService.selectBySecuritiesAccountIdAndStockCode(stockOperate.getSecuritiesAccountId(), stockCode);
            if (securitiesCompanyMoneyAccountDetailStocks != null) {
                securitiesCompanyMoneyAccountDetailStocks.setStockAvailableNum(securitiesCompanyMoneyAccountDetailStocks.getStockAvailableNum() - commissionNum);
                securitiesCompanyMoneyAccountDetailStocksService.update(securitiesCompanyMoneyAccountDetailStocks);
            }


            stockOperateMapper.insert(stockOperate);

            //写入股市委托记录
            operateLogService.addItemByUserOperateStockApply(stockOperate);
            result.setResultMsg("委托成功");
            result.setResultFlag(true);

        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.STOCK_MARKET_IS_NOT_OPENING);//当前休市
        }

        return result;
    }

    @Override
    public Result operateSellByForceSettlement(StockOperateRestrict stockOperateRestrict) {
        Result result = new Result("卖出失败");

        //是否开市
        if (MarketingContainer.isMarketingOn() && StockMarketingUtils.isDuringStockEntrustTime()) {


            //用户编号
            Long userId = stockOperateRestrict.getUserId();

            //合约编号
            Long agreementId = stockOperateRestrict.getAgreementId();


            //委托股数
            Long commissionNum = stockOperateRestrict.getCommissionNum();

            //股票代码
            String stockCode = stockOperateRestrict.getStockCode();


            Agreement agreement = agreementService.getAgreementByUserIdAndAgreementId(userId, agreementId);

            if (agreement == null || agreement.getSettlementType() == 0) {
                result.setResultMsg("非强制结算合约，请通过正常渠道卖出");
                return result;
            }

            StockPosition stockPosition = stockPositionService.getStockPositionByAgreementIdAndUserIdAndStockCode(agreementId, userId, stockCode);

            if (commissionNum.compareTo(0l) == 0) {
                result.setResultMsg("委托股数不能为0");
                return result;
            }

            //用户该合约下是否持有该股票
            if (stockPosition == null) {
                result.setResultMsg("未持有该股票,无法进行卖出操作");
                return result;
            }

            if (stockPosition.getStockAvailableNum() < commissionNum) {
                result.setResultMsg("最多可售股票" + stockPosition.getStockAvailableNum());
                return result;
            }

            StockMarket stockMarket = stockMarketService.stockInfo(stockCode);

            if (stockMarket.getOpenPrice().compareTo(0d) == 0) {
                result.setResultFlag(false);
                result.setResultMsg("股票停牌，不可交易！");
                return result;
            }


            //市价委托卖出  则将委托价格设置为跌停价
            if (stockOperateRestrict.getCommissionType() == 1) {
                stockOperateRestrict.setCommissionPrice(stockMarket.getMinPrice());
            }

            //委托价格
            Double commissionPrice = stockOperateRestrict.getCommissionPrice();

            //校验委托价格
            if (commissionPrice < stockMarket.getMinPrice() || commissionPrice > stockMarket.getMaxPrice()) {
                result.setResultMsg("委托价格必须介于涨停与跌停之间");
                return result;
            }

            StockOperate stockOperate = new StockOperate(agreementId, userId, stockCode, stockMarket.getName(), stockMarket.getMarket(), stockOperateRestrict.getCommissionPrice(), stockOperateRestrict.getCommissionNum(), 0, agreement.getOperateRate());

            //市价委托卖出
            if (stockOperateRestrict.getCommissionType() == 1) {
                stockOperate.setCommissionPriceType(2);//市价委托
            }

            stockPosition.setStockAvailableNum(stockPosition.getStockAvailableNum() - commissionNum);
            stockPositionService.updateStockPositionAvailableNum(stockPosition);
            stockOperate.setSecuritiesAccountId(agreement.getSecuritiesAccountId());

            SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getByPrimaryKey(stockOperate.getSecuritiesAccountId());
            SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks = securitiesCompanyMoneyAccountDetailStocksService.selectBySecuritiesAccountIdAndStockCode(agreement.getSecuritiesAccountId(), stockCode);
            if (securitiesCompanyMoneyAccountDetailStocks == null || securitiesCompanyMoneyAccountDetailStocks.getStockAvailableNum().compareTo(commissionNum) < 0) {
                securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getMaxAvailableItemFromVisual();
                stockOperate.setSecuritiesAccountId(securitiesCompanyMoneyAccountDetail.getSecuritiesAccountId());
                stockOperate.setAccountType(0);
                stockOperate.setOperateStatus(3);
                stockOperate.setProgressStatus(1);
            } else {
                stockOperate.setAccountType(securitiesCompanyMoneyAccountDetail.getAccountType());
                if (securitiesCompanyMoneyAccountDetail.getAccountType().compareTo(0) == 0) {
                    stockOperate.setOperateStatus(3);
                    stockOperate.setProgressStatus(1);
                }
            }


            //同步锁定对应券商账户可售股票余额
            securitiesCompanyMoneyAccountDetailStocks = securitiesCompanyMoneyAccountDetailStocksService.selectBySecuritiesAccountIdAndStockCode(stockOperate.getSecuritiesAccountId(), stockCode);
            if (securitiesCompanyMoneyAccountDetailStocks != null) {
                securitiesCompanyMoneyAccountDetailStocks.setStockAvailableNum(securitiesCompanyMoneyAccountDetailStocks.getStockAvailableNum() - commissionNum);
                securitiesCompanyMoneyAccountDetailStocksService.update(securitiesCompanyMoneyAccountDetailStocks);
            }

            stockOperateMapper.insert(stockOperate);

            //写入股市委托记录
            operateLogService.addItemByUserOperateStockApply(stockOperate);
            result.setResultMsg("委托成功");
            result.setResultFlag(true);

        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.STOCK_MARKET_IS_NOT_OPENING);//当前休市
        }

        return result;
    }

    @Override
    public List<StockOperate> listFresh() {
        return stockOperateMapper.listFresh();
    }

    @Override
    public List<StockOperate> listFreshWithUserId(Long userId) {
        return stockOperateMapper.listFreshWithUserId(userId);
    }

    @Override
    public Boolean updateStockOperate(StockOperate stockOperate) {
        return stockOperateMapper.updateStockOperate(stockOperate) > 0;
    }

    @Override
    public Boolean buySuccess(StockOperate stockOperate) {
        Double transactionValue = stockOperate.caculatedByTransaction();
        //操盘所需费用 多退少补
        Agreement agreement = agreementService.selectById(stockOperate.getAgreementId());
        agreement.setPrebuyLockMoney(agreement.getPrebuyLockMoney() - stockOperate.getLockMoney());//交易成功后 释放预购资金
        agreement.setStockCurrentPrice(agreement.getStockCurrentPrice() + stockOperate.getTransactionNum() * stockOperate.getTransactionPrice());//当前持仓价值变更
        //成交数据与委托数据不一致时
        if (transactionValue.compareTo(stockOperate.getLockMoney()) != 0) {
            agreement.setAvailableCredit(agreement.getAvailableCredit() + stockOperate.getLockMoney() - transactionValue);
            stockOperate.setLockMoney(transactionValue);
        }
        //当前盈利
        //持仓市值+可用余额+锁定预支付金额-配资金额-锁定资金
        agreement.setDiffMoney(agreement.getStockCurrentPrice() + agreement.getAvailableCredit() + agreement.getPrebuyLockMoney() - agreement.getBorrowMoney() - agreement.getLockMoney());
        agreementService.updateAgreementByOperateStock(agreement);
        updateStockOperate(stockOperate);
        //增仓
        stockPositionService.addStockPosition(stockOperate.getAgreementId(), stockOperate.getUserId(), stockOperate.getStockCode(), stockOperate.getStockName(), stockOperate.getTransactionNum(), stockOperate.getTransactionPrice());


        //测试更新可售股票
//        StockPosition stockPosition = stockPositionService.getStockPositionByAgreementIdAndUserIdAndStockCode(stockOperate.getAgreementId(), stockOperate.getUserId(), stockOperate.getStockCode());
//        stockPosition.setStockAvailableNum(stockPosition.getStockAvailableNum() + stockOperate.getTransactionNum());
//        stockPositionService.updateStockPositionAvailableNum(stockPosition);

        return true;
    }

    @Override
    public Boolean sellSuccess(StockOperate stockOperate) {
        Double transactionValue = stockOperate.caculatedByTransaction();
        updateStockOperate(stockOperate);
        Agreement agreement = agreementService.selectById(stockOperate.getAgreementId());
        agreement.setAvailableCredit(agreement.getAvailableCredit() + transactionValue);
        agreement.setStockCurrentPrice(agreement.getStockCurrentPrice() - stockOperate.getTransactionPrice() * stockOperate.getTransactionNum());
        //当前盈利
        //持仓市值+可用余额+锁定预支付金额-配资金额-锁定资金
        agreement.setDiffMoney(agreement.getStockCurrentPrice() + agreement.getAvailableCredit() + agreement.getPrebuyLockMoney() - agreement.getBorrowMoney() - agreement.getLockMoney());

        agreementService.updateAgreementByOperateStock(agreement);
        //减仓
        stockPositionService.reduceStockPosition(stockOperate.getAgreementId(), stockOperate.getUserId(), stockOperate.getStockCode(), stockOperate.getTransactionNum(), stockOperate.getTransactionPrice());

        return true;
    }


    @Override
    public Page<StockOperate> listAllStockOperatesOfAgreement(StockOperateRestrict stockOperateRestrict) {
        Page<StockOperate> page = new Page<>(stockOperateRestrict.getPageno(), stockOperateRestrict.getPagesize());
        stockOperateMapper.listAllStockOperatesOfAgreement(stockOperateRestrict, page);
        return page;
    }


    @Override
    public List<StockOperate> listAllUnFinishedGroupByStockCode() {
        return stockOperateMapper.listAllUnFinishedGroupByStockCode();
    }

    @Override
    public List<StockOperate> listAllUnFinishedByStockCode(String stockCode) {
        return stockOperateMapper.listAllUnFinishedByStockCode(stockCode);
    }

    @Override
    public List<StockOperate> listAllWaitingStockBuyOperateByAgreementId(Long agreementId) {
        return stockOperateMapper.listAllWaitingStockBuyOperateByAgreementId(agreementId);
    }

    @Override
    public List<StockOperate> listAllExpiredUnFinished() {
        return stockOperateMapper.listAllExpiredUnFinished();
    }

    @Override
    public Result updateExpiredOperate(StockOperate stockOperate) {
        Result result = new Result("设置失败");
        //委托过期后需要将数据逻辑回滚
        if (stockOperate.getOperateStatus() == 0) {
            cancelOperateRollBackData(stockOperate);
        } else {
            expiredOperateRollBackData(stockOperate);
        }
        return result;
    }


    protected Result cancelOperateRollBackData(StockOperate stockOperate) {
        Result result = new Result("回滚失败");
        Long operateId = stockOperate.getOperateId();
        switch (stockOperate.getOperateType()) {
            case 0: //卖出过期则需要将锁定可售股数撤回
            default:
                StockPosition stockPosition = stockPositionService.getStockPositionByAgreementIdAndUserIdAndStockCode(stockOperate.getAgreementId(), stockOperate.getUserId(), stockOperate.getStockCode());
                if (stockPosition != null) {
                    stockPosition.setStockAvailableNum(stockPosition.getStockAvailableNum() + stockOperate.getCommissionNum());
                    stockPositionService.updateStockPositionAvailableNum(stockPosition);
                }
                break;
            case 1: //买入过期则需要将锁定资金撤回
                Agreement agreement = agreementService.selectById(stockOperate.getAgreementId());
                agreement.setAvailableCredit(agreement.getAvailableCredit() + stockOperate.getLockMoney());
                agreement.setPrebuyLockMoney(agreement.getPrebuyLockMoney() - stockOperate.getLockMoney());
                agreementService.updateAgreementByOperateStock(agreement);
                break;
        }
        stockOperateMapper.cancelStockOperate(operateId);
        result.setResultFlag(true);
        result.setResultMsg("撤单成功");

        return result;
    }


    /**
     * 闭市后委托过期回滚数据
     *
     * @param stockOperate
     * @return
     */
    protected Result expiredOperateRollBackData(StockOperate stockOperate) {

        Result result = new Result("回滚失败");

        //未完结
        if (stockOperate.getProgressStatus() < 2) {
            //委托类型
            int operateType = stockOperate.getOperateType();
            //已处理的数量
            Long progressNum = stockOperate.getProgressNum();
            //用户提交总数量
            Long commissionNum = stockOperate.getCommissionNum();

            Long unTransactionNum = commissionNum - progressNum;

            stockOperate.setProgressStatus(2);
            stockOperate.setProgressNum(commissionNum);

            switch (operateType) {
                case 0: //卖出过期则需要将锁定可售股数撤回
                default:
                    StockPosition stockPosition = stockPositionService.getStockPositionByAgreementIdAndUserIdAndStockCode(stockOperate.getAgreementId(), stockOperate.getUserId(), stockOperate.getStockCode());
                    stockPosition.setStockAvailableNum(stockPosition.getStockAvailableNum() + unTransactionNum);
                    stockPositionService.updateStockPositionAvailableNum(stockPosition);
                    //处理完毕
//                if (stockOperate.getOperateStatus() == 2) {
                    //如果是全部撤销
                    if (unTransactionNum.compareTo(stockOperate.getCommissionNum()) == 0) {
                        stockOperate.setOperateStatus(4);
                    } else {
                        stockOperate.setOperateStatus(2);
                    }

//                }

                    //对应券商账户下锁定可售股票撤回
                    SecuritiesCompanyMoneyAccountDetailStocks securitiesCompanyMoneyAccountDetailStocks = securitiesCompanyMoneyAccountDetailStocksService.selectBySecuritiesAccountIdAndStockCode(stockOperate.getSecuritiesAccountId(), stockOperate.getStockCode());
                    securitiesCompanyMoneyAccountDetailStocks.setStockAvailableNum(securitiesCompanyMoneyAccountDetailStocks.getStockAvailableNum() + unTransactionNum);
                    securitiesCompanyMoneyAccountDetailStocksService.update(securitiesCompanyMoneyAccountDetailStocks);

                    this.updateStockOperate(stockOperate);

                    break;
                case 1: //买入过期则需要将锁定资金撤回
                    Agreement agreement = agreementService.selectById(stockOperate.getAgreementId());
                    agreement.setAvailableCredit(agreement.getAvailableCredit() + unTransactionNum * stockOperate.getCommissionPrice());
                    agreement.setPrebuyLockMoney(agreement.getPrebuyLockMoney() - unTransactionNum * stockOperate.getCommissionPrice());
                    stockOperate.setLockMoney(stockOperate.getLockMoney() - unTransactionNum * stockOperate.getCommissionPrice());
                    if (unTransactionNum.compareTo(stockOperate.getCommissionNum()) == 0) {//全部撤销
                        stockOperate.setOperateStatus(4);
                    } else {//部分成交
                        stockOperate.setOperateStatus(2);
                    }
                    //释放合约中仍被锁定的预购金额
                    agreement.setPrebuyLockMoney(agreement.getPrebuyLockMoney() - stockOperate.getLockMoney());
                    //变更合约中用户可用余额
                    agreement.setAvailableCredit(agreement.getAvailableCredit() + stockOperate.getLockMoney());

                    agreementService.updateAgreementByOperateStock(agreement);

                    //对应券商账户的预锁定资金亦撤回
                    SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getByPrimaryKey(stockOperate.getSecuritiesAccountId());
                    securitiesCompanyMoneyAccountDetail.setPreBuyLockMoney(securitiesCompanyMoneyAccountDetail.getPreBuyLockMoney() - stockOperate.getLockMoney());
                    securitiesCompanyMoneyAccountDetail.setAvailableMoney(securitiesCompanyMoneyAccountDetail.getAvailableMoney() + stockOperate.getLockMoney());
                    securitiesCompanyMoneyAccountDetailService.update(securitiesCompanyMoneyAccountDetail);

                    this.updateStockOperate(stockOperate);
                    break;
            }
            result.setResultFlag(true);
            result.setResultMsg("撤单成功");

        } else {
            result.setResultFlag(false);
            result.setResultMsg("无回滚数据");
        }


        return result;
    }


    @Override
    public Long getUnBindOperatorUserId() {
        return stockOperateMapper.getUnBindOperatorUserId();
    }


    @Override
    public Boolean isAllOperateFinishedByAgreementId(Long agreementId) {
        return stockOperateMapper.countUnfinishedOperateByAgreementId(agreementId) == 0;
    }


    @Override
    public List<StockOperate> getUnFinishedStockOperateByAgreementIdAndStockCode(Long agreementId, String stockCode) {
        StockOperateRestrict stockOperateRestrict = new StockOperateRestrict();
        stockOperateRestrict.setAgreementId(agreementId);
        stockOperateRestrict.setStockCode(stockCode);
        return stockOperateMapper.getUnFinishedStockOperateByAgreementIdAndStockCode(stockOperateRestrict);
    }
}
