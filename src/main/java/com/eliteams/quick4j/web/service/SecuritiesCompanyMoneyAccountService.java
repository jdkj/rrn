package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccount;

/**
 * Created by Dsmart on 2017/3/21.
 */
public interface SecuritiesCompanyMoneyAccountService extends GenericService<SecuritiesCompanyMoneyAccount, Long> {


    /**
     * 获取账户信息
     *
     * @return
     */
    SecuritiesCompanyMoneyAccount getAccount();


    /**
     * 增加总额度
     *
     * @param changeMoney
     * @return
     */
    Result increaseTotalMoney(Double changeMoney);


    /**
     * 减少总额度
     *
     * @param changeMoney
     * @return
     */
    Result reduceTotalMoney(Double changeMoney);


    /**
     * 增加可用额度
     *
     * @param changeMoney
     * @return
     */
    Result increaseAvailableMoney(Double changeMoney);


    /**
     * 减少可用额度
     *
     * @param changeMoney
     * @return
     */
    Result reduceAvailableMoney(Double changeMoney);


}
