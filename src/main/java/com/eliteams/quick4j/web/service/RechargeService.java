package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.Recharge;

/**
 * Created by Dsmart on 2016/10/26.
 */
public interface RechargeService extends GenericService<Recharge,Long> {

    public Boolean recharge(Recharge recharge);


    /**
     * 申请发起支付
     * @param recharge
     * @return
     */
    public Boolean applyRecharge(Recharge recharge);


    /**
     * 订单支付成功后主动查询 检验支付成功 更新订单状态
     * @param rechargeId
     * @return
     */
    Boolean updateAfterPaySuc(Long rechargeId);


}
