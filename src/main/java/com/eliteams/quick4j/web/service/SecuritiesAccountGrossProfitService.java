package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SecuritiesAccountGrossProfit;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountGrossProfitRestrict;

public interface SecuritiesAccountGrossProfitService extends GenericService<SecuritiesAccountGrossProfit, Long> {


    /**
     * 操盘手续费盈利增加
     *
     * @param securitiesAccountId 券商账户编号
     * @param userId              用户编号
     * @param totalProfitMoney    总盈利资金
     * @param profitMoney         盈利变动资金
     * @param profitIssueId       操盘编号
     * @return
     */
    Result addByOperateFeeIncrease(Long securitiesAccountId, Long userId, Double totalProfitMoney, Double profitMoney, Long profitIssueId);


    /**
     * 合约管理费 盈利增加
     *
     * @param securitiesAccountId 券商账户编号
     * @param userId              用户编号
     * @param totalProfitMoney    总盈利资金
     * @param profitMoney         盈利变动资金
     * @param profitIssueId       合约编号
     * @return
     */
    Result addByManagementFeeIncrease(Long securitiesAccountId, Long userId, Double totalProfitMoney, Double profitMoney, Long profitIssueId);


    /**
     * 利润提取
     *
     * @param securitiesAccountId 券商账户编号
     * @param totalProfitMoney    总盈利资金
     * @param profitMoney         提取金额
     * @return
     */
    Result addByWithdrawal(Long securitiesAccountId, Double totalProfitMoney, Double profitMoney);


    /**
     * 根据请求参数获取列表数据
     *
     * @param securitiesAccountGrossProfitRestrict
     * @return
     */
    Page<SecuritiesAccountGrossProfit> listByRestrict(SecuritiesAccountGrossProfitRestrict securitiesAccountGrossProfitRestrict);


}
