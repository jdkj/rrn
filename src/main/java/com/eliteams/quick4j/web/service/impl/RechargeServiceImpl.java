package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.RechargeMapper;
import com.eliteams.quick4j.web.model.Recharge;
import com.eliteams.quick4j.web.model.UserAccount;
import com.eliteams.quick4j.web.model.UserAccountMx;
import com.eliteams.quick4j.web.service.RechargeService;
import com.eliteams.quick4j.web.service.UserAccountMxService;
import com.eliteams.quick4j.web.service.UserAccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/10/26.
 */
@Service
public class RechargeServiceImpl extends GenericServiceImpl<Recharge,Long> implements RechargeService {

    @Resource
    private RechargeMapper rechargeMapper;

    @Resource
    private UserAccountService userAccountService;

    @Resource
    private UserAccountMxService userAccountMxService;


    @Override
    public Boolean recharge(Recharge recharge) {
        UserAccount userAccount = userAccountService.getUserAccountByUserId(recharge.getUserId());
        userAccount.setMoney(userAccount.getMoney()+5000);
        UserAccountMx userAccountMx = new UserAccountMx();
        userAccountMx.setUserId(userAccount.getUserId());
        userAccountMx.setChangeMoney(5000d);
        userAccountMx.setTotalMoney(userAccount.getMoney());
        userAccountMx.setOrderId(0l);
        userAccountService.updateAccountMoney(userAccount);
        userAccountMxService.recharge(userAccountMx);
        return true;
    }

    @Override
    public GenericDao<Recharge, Long> getDao() {
        return rechargeMapper;
    }


    @Override
    public Boolean applyRecharge(Recharge recharge) {
        return rechargeMapper.insert(recharge)>0;
    }

    @Override
    public Boolean updateAfterPaySuc(Long rechargeId) {
        return rechargeMapper.updateAfterPaySuc(rechargeId)>0;
    }
}
