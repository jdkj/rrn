package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.web.model.DistributionUser;
import com.eliteams.quick4j.web.model.User;

/**
 * Created by Dsmart on 2017/3/13.
 */
public interface DistributionApi {

    /**
     * 绑定分销关系
     *
     * @param userId
     * @param distributionId
     * @return
     */
    Result bindDistributionRelation(Long userId, Long distributionId);


    /**
     * 合约利息分佣
     *
     * @param userId
     * @param agreementId
     * @param money
     * @return
     */
    Result shareCommissionByAgreementInterest(Long userId, Long agreementId, Double money);

    /**
     * 操盘手续费分佣
     *
     * @param userId
     * @param operateId
     * @param money
     * @return
     */
    Result shareCommissionByStockOperate(Long userId, Long operateId, Double money);

    /**
     * 根据用户编号获取分销体系用户信息
     *
     * @param userId
     * @return
     */
    DistributionUser getByUserId(Long userId);


    /**
     * @param user
     * @return
     */
    Boolean updateByUser(User user);


}
