package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.DistributionCommissionMxMapper;
import com.eliteams.quick4j.web.model.DistributionCommissionMx;
import com.eliteams.quick4j.web.model.pagerestrict.DistributionCommissionMxRestrict;
import com.eliteams.quick4j.web.service.DistributionAccountService;
import com.eliteams.quick4j.web.service.DistributionCommissionMxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class DistributionCommissionMxServiceImpl extends GenericServiceImpl<DistributionCommissionMx, Long> implements DistributionCommissionMxService {

    @Resource
    private DistributionCommissionMxMapper distributionCommissionMxMapper;

    @Resource
    private DistributionAccountService distributionAccountService;


    @Override
    public GenericDao<DistributionCommissionMx, Long> getDao() {
        return distributionCommissionMxMapper;
    }


    @Override
    public Result addItem(DistributionCommissionMx distributionCommissionMx) {
        return null;
    }

    @Override
    public Result addByInterestCommission(Long distributionUserId, Long orderId, Double totalCommission, Double commissionProportion, Long customerId, Long introducerId, Integer introducerLevel) {
        Result result = new Result("佣金分配失败");
        DistributionCommissionMx distributionCommissionMx = new DistributionCommissionMx();
        distributionCommissionMx.setDistributionUserId(distributionUserId);
        distributionCommissionMx.setOrderId(orderId);
        distributionCommissionMx.setOrderType(0);
        distributionCommissionMx.setTotalCommission(totalCommission);
        distributionCommissionMx.setGainCommission(totalCommission * commissionProportion / 100d);
        distributionCommissionMx.setCommissionProportion(commissionProportion);
        distributionCommissionMx.setCustomerId(customerId);
        distributionCommissionMx.setIntroducerId(introducerId);
        distributionCommissionMx.setIntroducerLevel(introducerLevel);
        if (distributionCommissionMxMapper.insert(distributionCommissionMx) > 0) {
            result = distributionAccountService.increaseCommissionMoneyAndTotalCommissionMoney(distributionUserId, distributionCommissionMx.getGainCommission(), orderId);//增加佣金获得明细

        }
        return result;
    }


    @Override
    public Result addByOperateFeeCommission(Long distributionUserId, Long orderId, Double totalCommission, Double commissionProportion, Long customerId, Long introducerId, Integer introducerLevel) {
        Result result = new Result("佣金分配成功");
        DistributionCommissionMx distributionCommissionMx = new DistributionCommissionMx();
        distributionCommissionMx.setDistributionUserId(distributionUserId);
        distributionCommissionMx.setOrderId(orderId);
        distributionCommissionMx.setOrderType(1);
        distributionCommissionMx.setTotalCommission(totalCommission);
        distributionCommissionMx.setGainCommission(totalCommission * commissionProportion / 100d);
        distributionCommissionMx.setCommissionProportion(commissionProportion);
        distributionCommissionMx.setCustomerId(customerId);
        distributionCommissionMx.setIntroducerId(introducerId);
        distributionCommissionMx.setIntroducerLevel(introducerLevel);
        if (distributionCommissionMxMapper.insert(distributionCommissionMx) > 0) {
            result = distributionAccountService.increaseCommissionMoneyAndTotalCommissionMoney(distributionUserId, distributionCommissionMx.getGainCommission(), orderId);//增加佣金获得明细
        }
        return result;
    }


    @Override
    public Result increaseContributionCommissionMoney(Long customerId, Double totalCommission, Long orderId) {
        return distributionAccountService.increaseContributionCommissionMoney(customerId, totalCommission, orderId);//修改个人贡献佣金;
    }

    @Override
    public Page<DistributionCommissionMx> listBuRestrict(DistributionCommissionMxRestrict distributionCommissionMxRestrict) {
        Page<DistributionCommissionMx> page = new Page<>(distributionCommissionMxRestrict.getPageno(), distributionCommissionMxRestrict.getPagesize());
        distributionCommissionMxMapper.listByRestrict(distributionCommissionMxRestrict, page);
        return page;
    }
}
