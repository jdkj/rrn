package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.SecuritiesCompanyMoneyAccountMapper;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccount;
import com.eliteams.quick4j.web.service.SecuritiesCompanyMoneyAccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2017/3/21.
 */
@Service
public class SecuritiesCompanyMoneyAccountServiceImpl extends GenericServiceImpl<SecuritiesCompanyMoneyAccount, Long> implements SecuritiesCompanyMoneyAccountService {

    @Resource
    private SecuritiesCompanyMoneyAccountMapper securitiesCompanyMoneyAccountMapper;


    @Override
    public SecuritiesCompanyMoneyAccount getAccount() {
        return securitiesCompanyMoneyAccountMapper.selectOne();
    }

    @Override
    public Result increaseTotalMoney(Double changeMoney) {
        Result result = new Result("更新失败");
        SecuritiesCompanyMoneyAccount securitiesCompanyMoneyAccount = getAccount();
        securitiesCompanyMoneyAccount.setAvailableMoney(securitiesCompanyMoneyAccount.getAvailableMoney() + changeMoney);
        securitiesCompanyMoneyAccount.setTotalMoney(securitiesCompanyMoneyAccount.getTotalMoney() + changeMoney);
        if (securitiesCompanyMoneyAccountMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccount) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("更新成功");
        }
        return result;
    }

    @Override
    public GenericDao<SecuritiesCompanyMoneyAccount, Long> getDao() {
        return securitiesCompanyMoneyAccountMapper;
    }

    @Override
    public Result reduceTotalMoney(Double changeMoney) {
        Result result = new Result("更新失败");
        SecuritiesCompanyMoneyAccount securitiesCompanyMoneyAccount = getAccount();
        Double totalMoney = securitiesCompanyMoneyAccount.getTotalMoney();
        Double availableMoney = securitiesCompanyMoneyAccount.getAvailableMoney();
        if (totalMoney.compareTo(changeMoney) >= 0 && availableMoney.compareTo(changeMoney) >= 0) {
            securitiesCompanyMoneyAccount.setTotalMoney(totalMoney - changeMoney);
            securitiesCompanyMoneyAccount.setAvailableMoney(availableMoney - changeMoney);
            if (securitiesCompanyMoneyAccountMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccount) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("更新成功");
            }
        } else {
            result.setResultFlag(false);
            result.setResultMsg("可用余额不足");
        }

        return result;
    }

    @Override
    public Result increaseAvailableMoney(Double changeMoney) {
        Result result = new Result("更新失败");
        SecuritiesCompanyMoneyAccount securitiesCompanyMoneyAccount = getAccount();
        securitiesCompanyMoneyAccount.setAvailableMoney(securitiesCompanyMoneyAccount.getAvailableMoney() + changeMoney);
        if (securitiesCompanyMoneyAccountMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccount) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("更新成功");
        }
        return result;
    }

    @Override
    public Result reduceAvailableMoney(Double changeMoney) {
        Result result = new Result("更新失败");
        SecuritiesCompanyMoneyAccount securitiesCompanyMoneyAccount = getAccount();
        Double availableMoney = securitiesCompanyMoneyAccount.getAvailableMoney();
        if (availableMoney.compareTo(changeMoney) >= 0) {
            securitiesCompanyMoneyAccount.setAvailableMoney(securitiesCompanyMoneyAccount.getAvailableMoney() - changeMoney);
            if (securitiesCompanyMoneyAccountMapper.updateByPrimaryKeySelective(securitiesCompanyMoneyAccount) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("更新成功");
            }
        } else {
            result.setResultFlag(false);
            result.setResultMsg("可用余额不足");
        }

        return result;
    }


}
