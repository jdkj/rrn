package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.DistributionAccount;

/**
 * Created by Dsmart on 2017/3/12.
 */
public interface DistributionAccountService extends GenericService<DistributionAccount, Long> {

    /**
     * 添加对象
     *
     * @param distributionAccount
     * @return
     */
    Result addItem(DistributionAccount distributionAccount);


    /**
     * 根据分销体系编号获取用户分销体系账户对象
     *
     * @param distributionUserId
     * @return
     */
    DistributionAccount getByDistributionUserId(Long distributionUserId);


    /**
     * 增加获得佣金与累计获得佣金数据
     *
     * @param distributionUserId
     * @param money
     * @return
     */
    Result increaseCommissionMoneyAndTotalCommissionMoney(Long distributionUserId, Double money, Long orderId);


    /**
     * 减少可用佣金
     *
     * @param distributionUserId
     * @param money
     * @return
     */
    Result reduceCommissionMoney(Long distributionUserId, Double money);


    /**
     * 增加贡献佣金数据
     *
     * @param distributionUserId
     * @param money
     * @return
     */
    Result increaseContributionCommissionMoney(Long distributionUserId, Double money, Long orderId);


}
