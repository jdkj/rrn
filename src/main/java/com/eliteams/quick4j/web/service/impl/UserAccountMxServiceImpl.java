package com.eliteams.quick4j.web.service.impl;


import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.UserAccountMxMapper;
import com.eliteams.quick4j.web.model.UserAccountMx;
import com.eliteams.quick4j.web.model.pagerestrict.UserAccountMxRestrict;
import com.eliteams.quick4j.web.service.UserAccountMxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/10/26.
 */
@Service
public class UserAccountMxServiceImpl extends GenericServiceImpl<UserAccountMx,Long> implements UserAccountMxService {

    @Resource
    private UserAccountMxMapper userAccountMxMapper;

    @Override
    public GenericDao<UserAccountMx, Long> getDao() {
        return userAccountMxMapper;
    }

    @Override
    public Boolean recharge(UserAccountMx userAccountMx) {
        userAccountMx.setChangeDesc("充值存入");
        userAccountMx.setOrderType(0);
        userAccountMx.setChangeType(0);
        return userAccountMxMapper.insert(userAccountMx)>0;
    }

    @Override
    public Boolean withdrawals(UserAccountMx userAccountMx) {
        userAccountMx.setChangeDesc("提款取出");
        userAccountMx.setOrderType(1);
        userAccountMx.setChangeType(0);
        return userAccountMxMapper.insert(userAccountMx)>0;
    }

    @Override
    public Boolean signAgreementLockMoney(UserAccountMx userAccountMx) {
        userAccountMx.setChangeDesc("操盘支出");
        userAccountMx.setOrderType(2);
        userAccountMx.setChangeType(1);
        return userAccountMxMapper.insert(userAccountMx)>0;
    }

    @Override
    public Boolean relaseLockMoney(UserAccountMx userAccountMx) {
        userAccountMx.setChangeDesc("退保证金");
        userAccountMx.setOrderType(3);
        userAccountMx.setChangeType(1);
        return userAccountMxMapper.insert(userAccountMx)>0;
    }

    @Override
    public Boolean increaseLockMoney(UserAccountMx userAccountMx) {
        userAccountMx.setChangeDesc("加保证金");
        userAccountMx.setOrderType(4);
        userAccountMx.setChangeType(1);
        return userAccountMxMapper.insert(userAccountMx)>0;
    }

    @Override
    public Boolean interestPayment(UserAccountMx userAccountMx) {
        userAccountMx.setChangeDesc("利息支出");
        userAccountMx.setOrderType(5);
        userAccountMx.setChangeType(2);
        return userAccountMxMapper.insert(userAccountMx)>0;
    }

    @Override
    public Boolean interestPaymentByCoupon(UserAccountMx userAccountMx) {
        userAccountMx.setChangeDesc("优惠券抵扣");
        userAccountMx.setOrderType(5);
        userAccountMx.setChangeType(2);
        return userAccountMxMapper.insert(userAccountMx)>0;
    }

    @Override
    public Boolean profitWithdrawals(UserAccountMx userAccountMx) {
        userAccountMx.setChangeDesc("利润提取");
        userAccountMx.setOrderType(6);
        userAccountMx.setChangeType(4);
        return userAccountMxMapper.insert(userAccountMx)>0;
    }

    @Override
    public Page<UserAccountMx> listByRestrict(UserAccountMxRestrict userAccountMxRestrict) {
        Page<UserAccountMx> page = new Page<>(userAccountMxRestrict.getPageno(),userAccountMxRestrict.getPagesize());
        userAccountMxMapper.listByRestrict(userAccountMxRestrict,page);
        return page;
    }
}
