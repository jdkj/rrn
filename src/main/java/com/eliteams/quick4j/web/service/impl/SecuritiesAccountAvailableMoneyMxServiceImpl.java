package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.SecuritiesAccountAvailableMoneyMxMapper;
import com.eliteams.quick4j.web.model.SecuritiesAccountAvailableMoneyMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountAvailableMoneyMxRestrict;
import com.eliteams.quick4j.web.service.SecuritiesAccountAvailableMoneyMxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SecuritiesAccountAvailableMoneyMxServiceImpl extends GenericServiceImpl<SecuritiesAccountAvailableMoneyMx, Long> implements SecuritiesAccountAvailableMoneyMxService {

    @Resource
    private SecuritiesAccountAvailableMoneyMxMapper securitiesAccountAvailableMoneyMxMapper;


    @Override
    public GenericDao<SecuritiesAccountAvailableMoneyMx, Long> getDao() {
        return securitiesAccountAvailableMoneyMxMapper;
    }


    @Override
    public Result addByIncreaseTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney) {
        Result result = new Result("记录失败");
        SecuritiesAccountAvailableMoneyMx securitiesAccountAvailableMoneyMx = new SecuritiesAccountAvailableMoneyMx();
        securitiesAccountAvailableMoneyMx.setOrderType(3);
        securitiesAccountAvailableMoneyMx.setChangeMoney(changeMoney);
        securitiesAccountAvailableMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountAvailableMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        if (securitiesAccountAvailableMoneyMxMapper.insert(securitiesAccountAvailableMoneyMx) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Result addByReduceTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney) {
        Result result = new Result("记录失败");
        SecuritiesAccountAvailableMoneyMx securitiesAccountAvailableMoneyMx = new SecuritiesAccountAvailableMoneyMx();
        securitiesAccountAvailableMoneyMx.setOrderType(2);
        securitiesAccountAvailableMoneyMx.setChangeMoney(changeMoney);
        securitiesAccountAvailableMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountAvailableMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        if (securitiesAccountAvailableMoneyMxMapper.insert(securitiesAccountAvailableMoneyMx) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Result addByBuyStock(Long securitiesAccountId, Double totalMoney, Double changeMoney, Long operateId, Long userId) {
        Result result = new Result("记录失败");
        SecuritiesAccountAvailableMoneyMx securitiesAccountAvailableMoneyMx = new SecuritiesAccountAvailableMoneyMx();
        securitiesAccountAvailableMoneyMx.setOrderType(0);
        securitiesAccountAvailableMoneyMx.setChangeMoney(changeMoney);
        securitiesAccountAvailableMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountAvailableMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountAvailableMoneyMx.setOperateId(operateId);
        securitiesAccountAvailableMoneyMx.setUserId(userId);
        if (securitiesAccountAvailableMoneyMxMapper.insert(securitiesAccountAvailableMoneyMx) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }

    @Override
    public Result addBySellStock(Long securitiesAccountId, Double totalMoney, Double changeMoney, Long operateId, Long userId) {
        Result result = new Result("记录失败");
        SecuritiesAccountAvailableMoneyMx securitiesAccountAvailableMoneyMx = new SecuritiesAccountAvailableMoneyMx();
        securitiesAccountAvailableMoneyMx.setOrderType(1);
        securitiesAccountAvailableMoneyMx.setChangeMoney(changeMoney);
        securitiesAccountAvailableMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountAvailableMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesAccountAvailableMoneyMx.setOperateId(operateId);
        securitiesAccountAvailableMoneyMx.setUserId(userId);
        if (securitiesAccountAvailableMoneyMxMapper.insert(securitiesAccountAvailableMoneyMx) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("记录成功");
        }
        return result;
    }


    @Override
    public Page<SecuritiesAccountAvailableMoneyMx> listByRestrict(SecuritiesAccountAvailableMoneyMxRestrict securitiesAccountAvailableMoneyMxRestrict) {
        Page<SecuritiesAccountAvailableMoneyMx> page = new Page<>(securitiesAccountAvailableMoneyMxRestrict.getPageno(), securitiesAccountAvailableMoneyMxRestrict.getPagesize());
        securitiesAccountAvailableMoneyMxMapper.listByRestrict(securitiesAccountAvailableMoneyMxRestrict, page);
        return page;
    }
}
