package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.DistributionUserRelation;

import java.util.List;

/**
 * Created by Dsmart on 2017/3/11.
 */
public interface DistributionUserRelationService extends GenericService<DistributionUserRelation, Long> {


    /**
     * 添加关系
     *
     * @param distributionUserRelation
     * @return
     */
    Result addItem(DistributionUserRelation distributionUserRelation);


    /**
     * 根据提供佣金用户编号获取需要参与分佣的销售关系数据
     *
     * @param customerId
     * @return
     */
    List<DistributionUserRelation> listAllByCustomerId(Long customerId);


}
