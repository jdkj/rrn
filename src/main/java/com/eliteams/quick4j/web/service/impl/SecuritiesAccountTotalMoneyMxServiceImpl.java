package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.SecuritiesAccountTotalMoneyMxMapper;
import com.eliteams.quick4j.web.model.SecuritiesAccountTotalMoneyMx;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetail;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountTotalMoneyMxRestrict;
import com.eliteams.quick4j.web.service.SecuritiesAccountAgreementMoneyMxService;
import com.eliteams.quick4j.web.service.SecuritiesAccountAvailableMoneyMxService;
import com.eliteams.quick4j.web.service.SecuritiesAccountTotalMoneyMxService;
import com.eliteams.quick4j.web.service.SecuritiesCompanyMoneyAccountDetailService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SecuritiesAccountTotalMoneyMxServiceImpl extends GenericServiceImpl<SecuritiesAccountTotalMoneyMx, Long> implements SecuritiesAccountTotalMoneyMxService {

    @Resource
    private SecuritiesAccountTotalMoneyMxMapper securitiesAccountTotalMoneyMxMapper;

    @Resource
    private SecuritiesCompanyMoneyAccountDetailService securitiesCompanyMoneyAccountDetailService;

    @Resource
    private SecuritiesAccountAvailableMoneyMxService securitiesAccountAvailableMoneyMxService;

    @Resource
    private SecuritiesAccountAgreementMoneyMxService securitiesAccountAgreementMoneyMxService;


    @Override
    public GenericDao<SecuritiesAccountTotalMoneyMx, Long> getDao() {
        return securitiesAccountTotalMoneyMxMapper;
    }

    @Override
    public Result increaseTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney) {
        Result result = new Result("记录失败");
        SecuritiesAccountTotalMoneyMx securitiesAccountTotalMoneyMx = new SecuritiesAccountTotalMoneyMx();
        securitiesAccountTotalMoneyMx.setOrderType(1);
        securitiesAccountTotalMoneyMx.setChangeMoney(changeMoney);
        securitiesAccountTotalMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountTotalMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        if (securitiesAccountTotalMoneyMxMapper.insert(securitiesAccountTotalMoneyMx) > 0) {
            SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getByPrimaryKey(securitiesAccountId);
            securitiesAccountAgreementMoneyMxService.addByIncreaseTotalMoney(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getAgreementAvailableMoney() + changeMoney, changeMoney);
            result = securitiesAccountAvailableMoneyMxService.addByIncreaseTotalMoney(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getAvailableMoney() + changeMoney, changeMoney);
        }
        return result;
    }

    @Override
    public Result reduceTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney) {
        Result result = new Result("记录失败");
        SecuritiesAccountTotalMoneyMx securitiesAccountTotalMoneyMx = new SecuritiesAccountTotalMoneyMx();
        securitiesAccountTotalMoneyMx.setOrderType(0);
        securitiesAccountTotalMoneyMx.setChangeMoney(changeMoney);
        securitiesAccountTotalMoneyMx.setTotalMoney(totalMoney);
        securitiesAccountTotalMoneyMx.setSecuritiesAccountId(securitiesAccountId);
        if (securitiesAccountTotalMoneyMxMapper.insert(securitiesAccountTotalMoneyMx) > 0) {
            SecuritiesCompanyMoneyAccountDetail securitiesCompanyMoneyAccountDetail = securitiesCompanyMoneyAccountDetailService.getByPrimaryKey(securitiesAccountId);
            securitiesAccountAgreementMoneyMxService.addByReduceTotalMoney(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getAgreementAvailableMoney() - changeMoney, changeMoney);
            result = securitiesAccountAvailableMoneyMxService.addByReduceTotalMoney(securitiesAccountId, securitiesCompanyMoneyAccountDetail.getAvailableMoney() - changeMoney, changeMoney);
        }
        return result;
    }

    @Override
    public Page<SecuritiesAccountTotalMoneyMx> listByRestrict(SecuritiesAccountTotalMoneyMxRestrict securitiesAccountTotalMoneyMxRestrict) {
        Page<SecuritiesAccountTotalMoneyMx> page = new Page<>(securitiesAccountTotalMoneyMxRestrict.getPageno(), securitiesAccountTotalMoneyMxRestrict.getPagesize());
        securitiesAccountTotalMoneyMxMapper.listByRestrict(securitiesAccountTotalMoneyMxRestrict, page);
        return page;
    }
}
