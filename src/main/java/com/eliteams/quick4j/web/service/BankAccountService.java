package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.BankAccount;

/**
 * Created by Dsmart on 2016/11/5.
 */
public interface BankAccountService extends GenericService<BankAccount,Long> {

    /**
     * 添加银行卡信息
     * @param bankAccount
     * @return
     */
    Boolean addItem(BankAccount bankAccount);


    /**
     * 根据用户编号获取银行卡信息
     * @param userId
     * @return
     */
    BankAccount selectByUserId(Long userId);



}
