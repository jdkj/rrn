package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.web.dao.UserCouponMapper;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.UserCoupon;
import com.eliteams.quick4j.web.model.enums.usercoupon.UserCouponStatusEnums;
import com.eliteams.quick4j.web.model.enums.usercoupon.UserCouponTypeAndDescEnums;
import com.eliteams.quick4j.web.service.UserCouponService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/12/8.
 */
@Service
public class UserCouponServiceImpl extends GenericServiceImpl<UserCoupon,Long> implements UserCouponService {

    @Resource
    private UserCouponMapper userCouponMapper;

    @Override
    public List<UserCoupon> listAvailableUserCouponByUserId(Long userId) {
        return userCouponMapper.listAvailableUserCouponByUserId(userId);
    }

    @Override
    public GenericDao<UserCoupon, Long> getDao() {
        return userCouponMapper;
    }

    @Override
    public Boolean addSystemGiftCoupon(UserCoupon userCoupon,UserCouponTypeAndDescEnums userCouponTypeAndDescEnums) {
       userCoupon.setUserCouponTypeAndDescEnums(userCouponTypeAndDescEnums);
        return userCouponMapper.insert(userCoupon)>0;
    }

    @Override
    public Boolean addTicketsExchangeCoupon(UserCoupon userCoupon) {
        userCoupon.setUserCouponTypeAndDescEnums(UserCouponTypeAndDescEnums.COUPON_TICKETS_EXCHANGE);
        return userCouponMapper.insert(userCoupon)>0;
    }

    @Override
    public Boolean addIntegralExchangeCoupon(UserCoupon userCoupon) {
        userCoupon.setUserCouponTypeAndDescEnums(UserCouponTypeAndDescEnums.USER_INTEGRAL_EXCHANGE);
        return userCouponMapper.insert(userCoupon)>0;
    }

    @Override
    public Result updateByAttachAgreement(Long userId,Long userCouponId, Long agreementId) {
        Result result = new Result();
        //该合约还未绑定优惠券
        if(!isAgreementAttached(agreementId)){
            UserCoupon userCoupon = this.getUserCouponByUserIdAndUserCouponId(userId,userCouponId);
            if(userCoupon!=null){
                UserCouponStatusEnums userCouponStatusEnums = userCoupon.getUserCouponStatusEnums();
                //当前优惠券仍是可用的
                if(userCouponStatusEnums.equals(UserCouponStatusEnums.USER_COUPON_AVAILABLE)){
                    userCoupon.setAgreementId(agreementId);
                    userCoupon.setUserCouponStatus(UserCouponStatusEnums.USER_COUPON_USED.getUserCouponStatus());
                    userCoupon.setActiviteTime(ApplicationUtils.formatDate(ApplicationUtils.getDateWithMills(System.currentTimeMillis()),"yyyy-MM-dd HH:mm:ss:SSS","yyyy-MM-dd HH:mm:ss"));
                    userCouponMapper.update(userCoupon);
                    result.setResultFlag(true);
                }else{//非可用状态
                    result.setResponseStatusEnums(ResponseStatusEnums.OPERATE_FAIL);
                    result.setResultMsg(userCouponStatusEnums.getUserCouponStatusDesc());
                }
            }else{
                //用户优惠券不存在
                result.setResponseStatusEnums(ResponseStatusEnums.USER_COUPON_IS_NOT_EXIST);
            }
        }else{//该合约已经绑定优惠券
                result.setResponseStatusEnums(ResponseStatusEnums.AGREEMENT_ATTACHED_COUPON);
        }
        return result;
    }



    @Override
    public Boolean updateByReduceCouponAvailableValue(UserCoupon userCoupon) {
        return userCouponMapper.update(userCoupon)>0;
    }

    @Override
    public UserCoupon getUserCouponByUserIdAndUserCouponId(Long userId, Long userCouponId) {
        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setUserId(userId);
        userCoupon.setUserCouponId(userCouponId);
        return userCouponMapper.selectByUserIdAndUserCouponId(userCoupon);
    }


    public Boolean isAgreementAttached(Long agreementId){
        return userCouponMapper.isAgreementAttached(agreementId)>0;
    }

    @Override
    public Integer countUserCouponByCouponTicketId(Long couponTicketId) {
        return userCouponMapper.countUserCouponByCouponTicketId(couponTicketId);
    }
}
