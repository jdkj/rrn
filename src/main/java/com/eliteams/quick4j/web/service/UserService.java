package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.JSONResult;
import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.BankAccount;
import com.eliteams.quick4j.web.model.User;

/**
 * 用户 业务 接口
 * 
 * @author StarZou
 * @since 2014年7月5日 上午11:53:33
 **/
public interface UserService extends GenericService<User, Long> {

    /**
     * 验证通过手机号密码登录
     * @param user
     * @return
     */
    User authenticationByPhoneAndPassword(User user);


    /**
     * 通过手机号获取用户信息
     * @param phone
     * @return
     */
    User selectByPhone(String phone);

    /**
     * 通过手机号注册用户
     * @param user
     * @return
     */
    Boolean registeByPhone(User user);


    /**
     * 通过手机号注册用户
     * @param user
     * @return
     */
    Boolean registeByPhoneAndPassword(User user);


    /**
     * 判断手机号是否注册
     * @param phone
     * @return
     */
    Boolean isPhoneRegisted(String phone);

    /**
     * 更新用户手机号
     * @param user
     * @return
     */
    Boolean updateUserPhone(User user);

    /**
     * 更新用户登录密码
     * @param user
     * @return
     */
    Boolean updatePassword(User user);


    /**
     * 更新用户提现密码
     * @param user
     * @return
     */
    Boolean updateAccountPassword(User user);


    /**
     * 验证手机号码格式是否正确
     * @param phone
     * @return
     */
    Boolean validatePhone(String phone);

    /**
     * 校验用户密码是否正确
     * @param user
     * @return
     */
    Boolean validateUserIdAndPassword(User user);


    /**
     * 校验用户账户操作密码是否正确
     * @param user
     * @return
     */
    Boolean validateUserIdAndAccountPassword(User user);

    /**
     * 身份认证通过后更新信息
     * @param userId
     * @return
     */
    Boolean updateByIDAuthcated(Long userId);


    /**
     * 更新用户银行卡信息
     * @param bankId
     * @param bankAccount
     * @return
     */
    Result updateUserBankAccount(Long bankId, BankAccount bankAccount);


    /**
     * 通过微信js 渠道注册用户信息
     * @param code
     * @return
     */
    JSONResult<User> registedByWxJsChannel(String code);


    /**
     * 通过微信js 渠道登录
     * @param code
     * @return
     */
    User authenticationByWx(String code);


    /**
     * 将通过微信注册的用户信息与原有通过手机号注册的用户信息合并
     * @param wxUser
     * @param orginalUser
     * @return
     */
    Boolean mergeWxUserToOrginalUser(User wxUser,User orginalUser);





}
