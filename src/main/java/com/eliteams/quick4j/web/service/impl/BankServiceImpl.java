package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.BankMapper;
import com.eliteams.quick4j.web.model.Bank;
import com.eliteams.quick4j.web.service.BankService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/11/5.
 */
@Service
public class BankServiceImpl extends GenericServiceImpl<Bank,Long> implements BankService {


    @Resource
    private BankMapper bankMapper;

    @Override
    public List<Bank> listActivity() {
        return bankMapper.listActivity();
    }

    @Override
    public GenericDao<Bank, Long> getDao() {
        return bankMapper;
    }
}
