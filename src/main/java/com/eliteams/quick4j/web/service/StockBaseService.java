package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.StockBase;
import com.eliteams.quick4j.web.model.pagerestrict.StockBaseRestrict;

import java.util.List;

/**
 * Created by Dsmart on 2017/2/7.
 */
public interface StockBaseService extends GenericService<StockBase, Long> {


    /**
     * 添加
     *
     * @param stockBase
     * @return
     */
    public Result addItem(StockBase stockBase);


    /**
     * 获取所有股票基本信息
     *
     * @return
     */
    List<StockBase> listAll();


    /**
     * 根据流通市值与适应力筛选合适的股票
     *
     * @param minFlowValue 最低流通市值
     * @param maxPe        最高市盈率
     * @return
     */
    List<StockBase> queryByFlowValueAndPe(Double minFlowValue, Double maxPe);


    /**
     * 根据股票编码  名称  拼音简写 查询
     *
     * @param stockBaseRestrict
     * @return
     */
    Page<StockBase> fuzzyQuery(StockBaseRestrict stockBaseRestrict);


    /**
     * 根据股票编码获取基本信息
     *
     * @param code
     * @return
     */
    StockBase query(String code);


}
