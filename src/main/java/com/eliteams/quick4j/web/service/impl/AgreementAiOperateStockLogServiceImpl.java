package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.AgreementAiOperateStockLogMapper;
import com.eliteams.quick4j.web.model.AgreementAiOperateStockLog;
import com.eliteams.quick4j.web.model.StockMarket;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementAiOperateStockLogRestrict;
import com.eliteams.quick4j.web.service.AgreementAiOperateStockLogService;
import com.eliteams.quick4j.web.service.StockMarketService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class AgreementAiOperateStockLogServiceImpl extends GenericServiceImpl implements AgreementAiOperateStockLogService {

    @Resource
    private AgreementAiOperateStockLogMapper agreementAiOperateStockLogMapper;

    @Resource
    private StockMarketService stockMarketService;


    @Override
    public GenericDao getDao() {
        return agreementAiOperateStockLogMapper;
    }

    @Override
    public Result addItem(AgreementAiOperateStockLog agreementAiOperateStockLog) {
        Result result = new Result("数据已存在");
        AgreementAiOperateStockLog original = selectByAgreementIdAndStockCode(agreementAiOperateStockLog);
        if (original == null) {
            StockMarket stockMarket = stockMarketService.stockInfo(agreementAiOperateStockLog.getStockCode());
            agreementAiOperateStockLog.setStockMarket(stockMarket.getMarket());
            agreementAiOperateStockLog.setStockName(stockMarket.getName());
            agreementAiOperateStockLog.setStockPrice(stockMarket.getNowPrice());
            agreementAiOperateStockLog.setCurrentPrice(stockMarket.getNowPrice());
            if (agreementAiOperateStockLogMapper.insert(agreementAiOperateStockLog) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("添加成功");
            } else {
                result.setResultFlag(false);
                result.setResultMsg("添加失败");
            }

        }
        return result;
    }


    AgreementAiOperateStockLog selectByAgreementIdAndStockCode(AgreementAiOperateStockLog agreementAiOperateStockLog) {
        return agreementAiOperateStockLogMapper.selectByAgreementIdAndStockCode(agreementAiOperateStockLog);
    }


    @Override
    public Page<AgreementAiOperateStockLog> listByRestrict(AgreementAiOperateStockLogRestrict agreementAiOperateStockLogRestrict) {
        Page<AgreementAiOperateStockLog> page = new Page<>(agreementAiOperateStockLogRestrict.getPageno(), agreementAiOperateStockLogRestrict.getPagesize());
        agreementAiOperateStockLogMapper.listByRestrict(agreementAiOperateStockLogRestrict, page);
        return page;
    }

    @Override
    public Result batchAdd(AgreementAiOperateStockLogRestrict agreementAiOperateStockLogRestrict) {
        Result result = new Result("添加失败");
        Long agreementId = agreementAiOperateStockLogRestrict.getAgreementId();
        if (agreementId != null) {
            String stockCodeArr[] = agreementAiOperateStockLogRestrict.getStockCodeArr();
            if (stockCodeArr != null && stockCodeArr.length > 0) {

                for (String stockCode : stockCodeArr) {
                    AgreementAiOperateStockLog agreementAiOperateStockLog = new AgreementAiOperateStockLog();
                    agreementAiOperateStockLog.setStockCode(stockCode);
                    agreementAiOperateStockLog.setAgreementId(agreementId);
                    addItem(agreementAiOperateStockLog);
                }

                result.setResultFlag(true);
                result.setResultMsg("添加成功");

            }

        }
        return result;
    }
}
