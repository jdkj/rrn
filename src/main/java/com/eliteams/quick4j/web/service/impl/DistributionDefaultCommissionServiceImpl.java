package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.DistributionDefaultCommissionMapper;
import com.eliteams.quick4j.web.model.DistributionDefaultCommission;
import com.eliteams.quick4j.web.service.DistributionDefaultCommissionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dsmart on 2017/3/11.
 */
@Service
public class DistributionDefaultCommissionServiceImpl extends GenericServiceImpl<DistributionDefaultCommission, Long> implements DistributionDefaultCommissionService {

    @Resource
    private DistributionDefaultCommissionMapper distributionDefaultCommissionMapper;


    @Override
    public GenericDao<DistributionDefaultCommission, Long> getDao() {
        return distributionDefaultCommissionMapper;
    }

    @Override
    public Result addItem(DistributionDefaultCommission distributionDefaultCommission) {
        Result result = new Result("数据添加失败");
        if (distributionDefaultCommissionMapper.insert(distributionDefaultCommission) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("数据添加成功");
        }
        return result;
    }

    @Override
    public List<DistributionDefaultCommission> listAllActivity() {
        return distributionDefaultCommissionMapper.listAllActivity();
    }

    @Override
    public Map<Integer, DistributionDefaultCommission> mapAllActivity() {
        Map<Integer, DistributionDefaultCommission> map = new HashMap<>();
        List<DistributionDefaultCommission> list = listAllActivity();
        if (list != null && !list.isEmpty()) {
            for (DistributionDefaultCommission distributionDefaultCommission : list) {
                map.put(distributionDefaultCommission.getCommissionLevel(), distributionDefaultCommission);
            }
        }
        return map;
    }

    @Override
    public List<DistributionDefaultCommission> listUnderLineLevel(Integer level) {
        return distributionDefaultCommissionMapper.listUnderLineLevel(level);
    }

    @Override
    public DistributionDefaultCommission getByLevel(Integer level) {
        return distributionDefaultCommissionMapper.selectByLevel(level);
    }
}
