package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.OperatorUserRelation;
import com.eliteams.quick4j.web.model.pagerestrict.OperatorUserRelationRestrict;

import java.util.List;

/**
 * Created by Dsmart on 2017/1/6.
 */
public interface OperatorUserRelationService extends GenericService<OperatorUserRelation,Long> {


    /**
     * 添加绑定关系
     * @param operatorUserRelation
     * @return
     */
    Result addItem(OperatorUserRelation operatorUserRelation);


    /**
     * 更新绑定关系
     * @param operatorUserRelation
     * @return
     */
    Result updateItem(OperatorUserRelation operatorUserRelation);


    /**
     * 根据请求参数获取分页列表数据
     * @param operatorUserRelationRestrict
     * @return
     */
    Page<OperatorUserRelation> listByOperateId(OperatorUserRelationRestrict operatorUserRelationRestrict);


    /**
     * 根据管理员编号获取所有关系列表
     * @param adminId
     * @return
     */
    List<OperatorUserRelation> listAllByOperateId(Long adminId);


    /**
     * 根据用户编号获取 操盘关系
     * @param userId
     * @return
     */
    OperatorUserRelation selectByUserId(Long userId);

}
