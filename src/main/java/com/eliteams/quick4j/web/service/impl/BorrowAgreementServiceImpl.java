package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.BorrowAgreementMapper;
import com.eliteams.quick4j.web.model.BorrowAgreement;
import com.eliteams.quick4j.web.service.BorrowAgreementService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/10/18.
 */
@Service
public class BorrowAgreementServiceImpl extends GenericServiceImpl<BorrowAgreement,Long> implements BorrowAgreementService {

    @Resource
    private BorrowAgreementMapper borrowAgreementMapper;

    @Override
    public Boolean addBorrowAgreement(BorrowAgreement borrowAgreement) {
        return borrowAgreementMapper.insert(borrowAgreement)>0;
    }

    @Override
    public GenericDao<BorrowAgreement, Long> getDao() {
        return borrowAgreementMapper;
    }

    @Override
    public List<BorrowAgreement> listBorrowAgreement(Integer borrowType) {
        return borrowAgreementMapper.list(borrowType);
    }

    @Override
    public List<BorrowAgreement> listActivityBorrowType() {
        return borrowAgreementMapper.listActivityBorrowType();
    }

    @Override
    public BorrowAgreement getBorrowTypeByType(Integer borrowType) {
        return borrowAgreementMapper.getBorrowTypeByType(borrowType);
    }
}
