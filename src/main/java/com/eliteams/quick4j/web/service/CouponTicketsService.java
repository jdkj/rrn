package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.CouponTickets;

/**
 * Created by Dsmart on 2016/12/8.
 */
public interface CouponTicketsService extends GenericService<CouponTickets,Long> {

    /**
     * 通过激活码兑换优惠券
     * @param activiteCode 优惠券激活码 eg:A490012312316
     * @param userId 用户编号
     * @return
     */
    Result activiteCouponTickets(String activiteCode,Long userId);





}
