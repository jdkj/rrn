package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.PhoneMsg;

/**
 * Created by Dsmart on 2017/3/30.
 */
public interface PhoneMsgService extends GenericService<PhoneMsg, Long> {

    /**
     * 添加触发警戒线后发送的短消息
     *
     * @param userId
     * @param agreementId
     * @param phone
     * @param content
     * @return
     */
    Result addWarningLineMsg(Long userId, Long agreementId, String phone, String content);


    /**
     * 合约快到期是的提醒短消息
     *
     * @param userId
     * @param agreementId
     * @param phone
     * @param content
     * @return
     */
    Result addBecomingEndOfAgreement(Long userId, Long agreementId, String phone, String content);


    /**
     * @param userId
     * @param phone
     * @param content
     * @return
     */
    Result addLackOfInterestMsg(Long userId, String phone, String content);


    Result addStockOperateApplyDelayWarningMsg(String phone, String content);


    Result addStockConfirmDelayWarningMsg(String phone, String content);


    /**
     * 合约已超过最大操盘日期 发送短信通知
     *
     * @param userId
     * @param agreementId
     * @param phone
     * @param content
     * @return
     */
    Result addAgreementOverMaxPeriod(Long userId, Long agreementId, String phone, String content);


    /**
     * 提现申请成功提醒
     *
     * @param userId
     * @param phone
     * @param content
     * @return
     */
    Result addWithdrawalsApplySuccessMsg(Long userId, String phone, String content);

    /**
     * 提现确认成功短信提醒
     *
     * @param userId
     * @param phone
     * @param content
     * @return
     */
    Result addConfirmWithdrawalsApplySuccessMsg(Long userId, String phone, String content);


}
