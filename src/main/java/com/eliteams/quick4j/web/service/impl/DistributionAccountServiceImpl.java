package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.DistributionAccountMapper;
import com.eliteams.quick4j.web.model.DistributionAccount;
import com.eliteams.quick4j.web.service.DistributionAccountMxService;
import com.eliteams.quick4j.web.service.DistributionAccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2017/3/12.
 */
@Service
public class DistributionAccountServiceImpl extends GenericServiceImpl<DistributionAccount, Long> implements DistributionAccountService {

    @Resource
    private DistributionAccountMapper distributionAccountMapper;

    @Resource
    private DistributionAccountMxService distributionAccountMxService;


    @Override
    public Result addItem(DistributionAccount distributionAccount) {
        Result result = new Result("添加失败");
        if (distributionAccountMapper.insert(distributionAccount) > 0) {
            result.setResultFlag(true);
            result.setResultMsg("添加成功");
        }
        return result;
    }

    @Override
    public GenericDao<DistributionAccount, Long> getDao() {
        return distributionAccountMapper;
    }

    @Override
    public DistributionAccount getByDistributionUserId(Long distributionUserId) {
        return distributionAccountMapper.selectByDistributionUserId(distributionUserId);
    }

    @Override
    public Result increaseCommissionMoneyAndTotalCommissionMoney(Long distributionUserId, Double money, Long orderId) {
        Result result = new Result("佣金增加失败");
        DistributionAccount distributionAccount = getByDistributionUserId(distributionUserId);
        if (distributionAccount != null) {
            distributionAccount.setCommissionMoney(distributionAccount.getCommissionMoney() + money);
            distributionAccount.setTotalCommissionMoney(distributionAccount.getTotalCommissionMoney() + money);
            if (distributionAccountMapper.updateByPrimaryKeySelective(distributionAccount) > 0) {
                if (distributionAccountMxService.addByIncreaseCommissionMoney(distributionUserId, orderId, distributionAccount.getCommissionMoney(), money)) {
                    result.setResultFlag(true);
                    result.setResultMsg("增加成功");
                }
            }

        } else {
            result.setResultFlag(false);
            result.setResultMsg("佣金账户不存在");
        }
        return result;
    }

    @Override
    public Result reduceCommissionMoney(Long distributionUserId, Double money) {
        return null;
    }

    @Override
    public Result increaseContributionCommissionMoney(Long distributionUserId, Double money, Long orderId) {
        Result result = new Result("贡献佣金增加失败");
        DistributionAccount distributionAccount = getByDistributionUserId(distributionUserId);
        if (distributionAccount != null) {
            distributionAccount.setContributionCommissionMoney(distributionAccount.getContributionCommissionMoney() + money);
            if (distributionAccountMapper.updateByPrimaryKeySelective(distributionAccount) > 0) {
                result.setResultFlag(true);
                result.setResultMsg("贡献增加成功");
            }
        } else {
            result.setResultFlag(false);
            result.setResultMsg("佣金账户不存在");
        }
        return result;
    }
}

