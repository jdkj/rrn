package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.PrimaryKeyModifyUtils;
import com.eliteams.quick4j.web.dao.CouponTicketsMapper;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.CouponStore;
import com.eliteams.quick4j.web.model.CouponTickets;
import com.eliteams.quick4j.web.model.UserCoupon;
import com.eliteams.quick4j.web.model.enums.usercoupon.UserCouponTypeAndDescEnums;
import com.eliteams.quick4j.web.service.CouponStoreService;
import com.eliteams.quick4j.web.service.CouponTicketsService;
import com.eliteams.quick4j.web.service.UserCouponService;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/12/8.
 */
@Service
public class CouponTicketsServiceImpl extends GenericServiceImpl<CouponTickets, Long> implements CouponTicketsService {

    @Resource
    private CouponTicketsMapper couponTicketsMapper;

    @Resource
    private CouponStoreService couponStoreService;

    @Resource
    private UserCouponService userCouponService;


    @Override
    public Result activiteCouponTickets(String activiteCode, Long userId) {
        Result result = new Result();

        if (StringUtils.isNotEmpty(activiteCode)) {

            Long couponTicketId = PrimaryKeyModifyUtils.decodePrimaryKey(activiteCode.substring(1));
            CouponTickets couponTickets = this.selectById(couponTicketId);

            if (couponTickets != null) {

                if (activiteCode.toLowerCase().startsWith("a")) {//可重复兑换

                    //优惠券兑换码是可重复使用的
                    if (couponTickets.getCouponTicketType() == 1) {

                        //优惠券已被停用
                        if (couponTickets.getCouponTicketStatus() == 2) {
                            result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_IS_NOT_AVAILABLE);
                            return result;
                        }

                        //判断当前优惠券是否限制每个用户拥有的数量
                        int limitNo = couponTickets.getLimitNo();

                        if (limitNo > 0) {//限制拥有数量

                            //查询当前用户已经拥有的数量
                            int haveExchangeNo = userCouponService.countUserCouponByCouponTicketId(couponTickets.getCouponTicketId());

                            if (haveExchangeNo >= limitNo) {//已超额
                                result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_IS_LIMITED);
                                result.setResultMsg(String.format(result.getResultMsg(), limitNo));
                                return result;
                            }

                        }

                        //优惠券数据
                        CouponStore couponStore = couponStoreService.selectById(couponTickets.getCouponId());


                        UserCoupon userCoupon = new UserCoupon();
                        userCoupon.setCouponId(couponTickets.getCouponId());
                        userCoupon.setUserId(userId);
                        userCoupon.setUserCouponTypeAndDescEnums(UserCouponTypeAndDescEnums.COUPON_TICKETS_EXCHANGE);
                        userCoupon.setCouponMarketValue(couponStore.getCouponMarketValue());
                        userCoupon.setCouponAvailableValue(couponStore.getCouponMarketValue());
                        userCoupon.setExpiredTimeSql(couponStore.getAvailablePeriod(), couponStore.getCouponStoreAvailablePeriodUnitEnums());
                        userCoupon.setCouponTicketId(couponTickets.getCouponTicketId());
                        if (userCouponService.addTicketsExchangeCoupon(userCoupon)) {//兑换成功
                            result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_EXCHANGE_SUCCESS);
                        } else {
                            result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_EXCHANGE_FAILED);
                        }


                    } else {
                        result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_IS_NOT_CORRECT);
                    }


                } else if (activiteCode.toLowerCase().startsWith("b")) {//不可重复兑换

                    //优惠券兑换码是一次性的
                    if (couponTickets.getCouponTicketType() == 0) {

                        //优惠券已被停用
                        if (couponTickets.getCouponTicketStatus() == 2) {
                            result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_IS_NOT_AVAILABLE);
                            return result;
                        }

                        //优惠券激活码已被使用
                        if(couponTickets.getCouponTicketStatus()==1){
                            result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_IS_USED);
                            return result;
                        }


                        //优惠券数据
                        CouponStore couponStore = couponStoreService.selectById(couponTickets.getCouponId());


                        UserCoupon userCoupon = new UserCoupon();
                        userCoupon.setCouponId(couponTickets.getCouponId());
                        userCoupon.setUserId(userId);
                        userCoupon.setUserCouponTypeAndDescEnums(UserCouponTypeAndDescEnums.COUPON_TICKETS_EXCHANGE);
                        userCoupon.setCouponMarketValue(couponStore.getCouponMarketValue());
                        userCoupon.setCouponAvailableValue(couponStore.getCouponMarketValue());
                        userCoupon.setExpiredTimeSql(couponStore.getAvailablePeriod(), couponStore.getCouponStoreAvailablePeriodUnitEnums());
                        userCoupon.setCouponTicketId(couponTickets.getCouponTicketId());
                        if(userCouponService.addTicketsExchangeCoupon(userCoupon)){
                            couponTickets.setUserId(userId);
                            couponTickets.setCouponTicketStatus(1);
                            if(activiteCouponTickets(couponTickets)){
                                result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_EXCHANGE_SUCCESS);
                            }else{
                                result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_EXCHANGE_FAILED);
                            }
                        }else{
                            result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_EXCHANGE_FAILED);
                        }

                    }else{
                        result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_IS_NOT_CORRECT);
                    }
                }

            } else {
                result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_IS_NOT_CORRECT);
            }

        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.COUPON_TICKETS_IS_NOT_EXIST);
        }

        return result;
    }

    @Override
    public GenericDao<CouponTickets, Long> getDao() {
        return couponTicketsMapper;
    }



    Boolean activiteCouponTickets(CouponTickets couponTickets){
        return couponTicketsMapper.activiteCouponTickets(couponTickets)>0;
    }
}
