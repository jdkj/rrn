package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.UserIntegralMxMapper;
import com.eliteams.quick4j.web.model.UserIntegralMx;
import com.eliteams.quick4j.web.model.pagerestrict.UserIntegralMxRestrict;
import com.eliteams.quick4j.web.service.UserIntegralMxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/12/7.
 */
@Service
public class UserIntegralMxServiceImpl extends GenericServiceImpl<UserIntegralMx,Long> implements UserIntegralMxService {

    @Resource
    private UserIntegralMxMapper userIntegralMxMapper;

    @Override
    public Boolean systemGift(UserIntegralMx userIntegralMx) {
        userIntegralMx.setChangeType(1);
        userIntegralMx.setChangeDesc("系统赠送");

        return userIntegralMxMapper.insert(userIntegralMx)>0;
    }

    @Override
    public GenericDao<UserIntegralMx, Long> getDao() {
        return userIntegralMxMapper;
    }

    @Override
    public Boolean agreementReward(UserIntegralMx userIntegralMx) {
        userIntegralMx.setChangeType(0);
        userIntegralMx.setChangeDesc("合约奖励");
        return userIntegralMxMapper.insert(userIntegralMx)>0;
    }

    @Override
    public Boolean couponExchange(UserIntegralMx userIntegralMx) {
        userIntegralMx.setChangeType(3);
        userIntegralMx.setChangeDesc("卡券兑换");
        return userIntegralMxMapper.insert(userIntegralMx)>0;
    }


    @Override
    public Page<UserIntegralMx> listByRestrict(UserIntegralMxRestrict userIntegralMxRestrict) {
        Page<UserIntegralMx> page = new Page<UserIntegralMx>(userIntegralMxRestrict.getPageno(),userIntegralMxRestrict.getPagesize());
        userIntegralMxMapper.listByRestrict(userIntegralMxRestrict,page);
        return page;
    }
}
