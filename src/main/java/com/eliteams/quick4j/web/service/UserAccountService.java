package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.UserAccount;

/**
 * Created by Dsmart on 2016/10/25.
 */
public interface UserAccountService extends GenericService<UserAccount,Long> {

    /**
     * 添加用户账户信息
     * @param userAccount
     * @return
     */
    public Boolean addUserAccount(UserAccount userAccount);


    /**
     * 通过用户编号获取用户账户信息
     * @param userId
     * @return
     */
    public UserAccount getUserAccountByUserId(Long userId);


    /**
     * 更新用户账户资金信息
     * @param userAcccount
     * @return
     */
    public Boolean updateAccountMoney(UserAccount userAcccount);

    /**
     * 更新用户账户积分信息
     * @param userAccount
     * @return
     */
    Boolean updateAccountIntegral(UserAccount userAccount);


    /**
     * 充值存入
     * @param userId
     * @param diffMoney
     * @param orderId
     * @return
     */
    public Result recharge(Long userId,Double diffMoney,Long orderId);


    /**
     * 提款取出
     * @param userId
     * @param diffMoney
     * @param orderId
     * @return
     */
    public Result withdrawals(Long userId,Double diffMoney,Long orderId);


    /**
     * 操盘支出
     * 签订合约锁定资金
     * @param userId
     * @param diffMoney
     * @param orderId
     * @return
     */
    public Result signAgreementLockMoney(Long userId,Double diffMoney,Long orderId);


    /**
     * 退保证金
     * @param userId
     * @param diffMoney
     * @param orderId
     * @return
     */
    public Result relaseLockMoney(Long userId,Double diffMoney,Long orderId);


    /**
     * 加保证金
     * @param userId
     * @param diffMoney
     * @param orderId
     * @return
     */
    public Result increaseLockMoney(Long userId,Double diffMoney,Long orderId);


    /**
     * 利息支出
     * @param userId
     * @param diffMoney
     * @param orderId
     * @return
     */
    public Result interestPayment(Long userId,Double diffMoney,Long orderId);


    /**
     * 通过优惠券进行利息支出
     * @param userId
     * @param diffMoney
     * @param agreementId
     * @param userCouponId
     * @return
     */
    public Result interestPaymentByCoupon(Long userId,Double diffMoney,Long agreementId,Long userCouponId);



    /**
     * 利润提取
     * @param userId
     * @param diffMoney
     * @param orderId
     * @return
     */
    public Result profitWithdrawals(Long userId,Double diffMoney,Long orderId);


    /**
     * 系统赠送积分
     * @param userId
     * @param diffIntegral
     * @param orderId
     * @return
     */
    Result systemGiftIntegral(Long userId,Double diffIntegral,Long orderId);


    /**
     * 合约奖励积分
     * @param userId
     * @param diffIntegral
     * @param orderId
     * @return
     */
    Result agreementRewardIntegral(Long userId,Double diffIntegral,Long orderId);


    /**
     * 优惠券兑换使用积分
     * @param userId
     * @param diffIntegral
     * @param orderId
     * @return
     */
    Result couponExchangeIntegral(Long userId,Double diffIntegral,Long orderId);


    /**
     * 统计用户总操盘手续费(剔除成本)
     *
     * @param userId
     * @param operateFee
     * @return
     */
    Result increaseTotalOperateFee(Long userId, Double operateFee);


    /**
     * 统计合约管理费用
     *
     * @param userId
     * @param managementFee
     * @return
     */
    Result increaseTotalManagementFee(Long userId, Double managementFee);


    /**
     * 统计用户总盈利(剔除成本)
     *
     * @param userId
     * @param profit
     * @return
     */
    Result increaseTotalProfit(Long userId, Double profit);


    /**
     * 统计用户总申请合约价值
     *
     * @param userId
     * @param agreementMoney
     * @return
     */
    Result increaseTotalAgreementFund(Long userId, Double agreementMoney);







}
