package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.ForbidStock;

import java.util.List;

/**
 * 禁止操作股票接口
 * Created by Dsmart on 2016/10/17.
 */
public interface ForbidStockService extends GenericService<ForbidStock,Long> {


    /**
     * 获取禁止操作股票列表
     * @return
     */
    public List<ForbidStock> getForbidStocks();


}
