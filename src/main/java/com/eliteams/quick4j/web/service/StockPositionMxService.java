package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.StockPositionMx;

import java.util.List;

/**
 * Created by Dsmart on 2016/10/31.
 */
public interface StockPositionMxService extends GenericService<StockPositionMx,Long> {

    /**
     * 添加记录
     * @param positionId  持仓编号
     * @param userId 用户编号
     * @param stockCode 股票代码
     * @param stockName 股票名称
     * @param stockNum 购买股数
     * @param buyPrice 买入价格
     * @return
     */
    public Boolean addItem(Long positionId,Long userId,String stockCode,String stockName,Long stockNum,Double buyPrice);




    /**
     * 根据用户编号和股票编码获取用户的持仓详情
     * @param positionId
     * @param userId
     * @param stockCode
     * @return
     */
    public List<StockPositionMx> listByPositionIdAndUserIdAndStockCode(Long positionId,Long userId,String stockCode);


    /**
     * 获取新购股票列表数据  用于改变可售数量
     * @return
     */
    public List<StockPositionMx> listFreshStocks();


    /**
     * 释放新购股票
     * @param stockPositionMx
     * @return
     */
    public Boolean updateStockPositionMxByUnlock(StockPositionMx stockPositionMx);





}
