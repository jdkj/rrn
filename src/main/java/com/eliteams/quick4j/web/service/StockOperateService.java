package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.StockOperate;
import com.eliteams.quick4j.web.model.pagerestrict.StockOperateRestrict;

import java.util.List;

/**
 * Created by Dsmart on 2016/11/3.
 */
public interface StockOperateService extends GenericService<StockOperate, Long> {

    /**
     * 获取挂单状态的操作列表
     *
     * @param stockOperateRestrict
     * @return
     */
    Page<StockOperate> listAllWaitingStockOperateOfAgreement(StockOperateRestrict stockOperateRestrict);


    /**
     * 获取当前未完成的交易委托数量
     *
     * @param agreementId
     * @return
     */
    int countWaitingStockOperateOfAgreement(Long agreementId);


    /**
     * 获取当日成交记录
     *
     * @param stockOperateRestrict
     * @return
     */
    Page<StockOperate> listCurrentDayDeals(StockOperateRestrict stockOperateRestrict);


    /**
     * 获取当日委托记录
     *
     * @param stockOperateRestrict
     * @return
     */
    Page<StockOperate> listCurrentDayCommissions(StockOperateRestrict stockOperateRestrict);


    /**
     * 获取历史成交记录
     *
     * @param stockOperateRestrict
     * @return
     */
    Page<StockOperate> listHistoryDeals(StockOperateRestrict stockOperateRestrict);


    /**
     * 获取历史委托记录
     *
     * @param stockOperateRestrict
     * @return
     */
    Page<StockOperate> listHistoryCommissions(StockOperateRestrict stockOperateRestrict);


    /**
     * 申请撤单
     *
     * @param operateId
     * @return
     */
    Result cancelStockOperateApply(Long operateId);


    /**
     * 撤单
     * @param operateId
     * @return
     */
    Result cancelStockOperate(Long operateId);


    /**
     * 买入
     *
     * @param stockOperateRestrict
     * @return
     */
    public Result operateBuy(StockOperateRestrict stockOperateRestrict);


    /**
     * 卖出
     *
     * @param stockOperateRestrict
     * @return
     */
    public Result operateSell(StockOperateRestrict stockOperateRestrict);


    /**
     * 强制结算合约操盘卖出
     *
     * @param stockOperateRestrict
     * @return
     */
    Result operateSellByForceSettlement(StockOperateRestrict stockOperateRestrict);






    /**
     * 获取未报的委托
     *
     * @return
     */
    List<StockOperate> listFresh();


    /**
     * 获取某用户下所有未报委托
     * @param userId
     * @return
     */
    List<StockOperate> listFreshWithUserId(Long userId);


    /**
     * 买入成功
     *
     * @param stockOperate
     * @return
     */
    Boolean buySuccess(StockOperate stockOperate);


    /**
     * 卖出成功
     *
     * @param stockOperate
     * @return
     */
    Boolean sellSuccess(StockOperate stockOperate);


    /**
     * 更新委托信息
     * @param stockOperate
     * @return
     */
    Boolean updateStockOperate(StockOperate stockOperate);


    Page<StockOperate> listAllStockOperatesOfAgreement(StockOperateRestrict stockOperateRestrict);


    /**
     * 根据股票编码分组获取当前所有未完结委托
     *
     * @return
     */
    List<StockOperate> listAllUnFinishedGroupByStockCode();

    /**
     * 根据股票编码获取当前所有未完结委托
     *
     * @param stockCode
     * @return
     */
    List<StockOperate> listAllUnFinishedByStockCode(String stockCode);




    /**
     * 获取某合约下所有未完结的交易委托
     * @param agreementId
     * @return
     */
    List<StockOperate> listAllWaitingStockBuyOperateByAgreementId(Long agreementId);


    /**
     * 获取所有过期的未完结交易委托
     * @return
     */
    List<StockOperate> listAllExpiredUnFinished();



    /**
     * 更新已经过期的交易委托
     */
    Result updateExpiredOperate(StockOperate stockOperate);


    /**
     * 获取一个未绑定操作员的用户编号
     * @return
     */
    Long getUnBindOperatorUserId();


    /**
     * 根据合约编号判断是否还有未完成的操盘委托
     *
     * @param agreementId
     * @return
     */
    Boolean isAllOperateFinishedByAgreementId(Long agreementId);


    /**
     * 获取某合约下当前还未完成的某股票申购
     *
     * @param agreementId
     * @param stockCode
     * @return
     */
    List<StockOperate> getUnFinishedStockOperateByAgreementIdAndStockCode(Long agreementId, String stockCode);


}
