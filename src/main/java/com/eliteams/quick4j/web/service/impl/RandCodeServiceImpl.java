package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.core.util.ApplicationUtils;
import com.eliteams.quick4j.core.util.SMSUtils;
import com.eliteams.quick4j.web.dao.RandCodeMapper;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.RandCode;
import com.eliteams.quick4j.web.service.RandCodeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/6/29.
 */
@Service
public class RandCodeServiceImpl extends GenericServiceImpl<RandCode, Long> implements RandCodeService {

    @Resource
    private RandCodeMapper randCodeMapper;

    @Override
    public GenericDao<RandCode, Long> getDao() {
        return randCodeMapper;
    }

    @Override
    public ResponseStatusEnums validRequest(RandCode randCode) {

        if (randCodeMapper.countByIp(randCode) <= 20) {

            if (randCodeMapper.countByClient(randCode) <= 6) {

                if (randCodeMapper.countByPhone(randCode) <= 3) {
                    return ResponseStatusEnums.OPERATE_SUCCESS;

                } else {
                    return ResponseStatusEnums.PHNOE_CODE_LIMITTED;
                }

            } else {
                return ResponseStatusEnums.CLIENT_CODE_LIMITTED;
            }

        } else {
            return ResponseStatusEnums.IP_CODE_LIMITTED;
        }
    }

    @Override
    public RandCode reqPhoneCode(RandCode randCode) {

        if (validRequest(randCode) == ResponseStatusEnums.OPERATE_SUCCESS) {

            randCode.setCodes(ApplicationUtils.limitedNumsStr(5));

            if (randCodeMapper.insert(randCode) <= 0) {
                randCode = null;
            }

            SMSUtils.post(randCode.getPhone(), randCode.getCodes() + "(人人牛股票平台动态验证码，请勿泄漏!)");

        } else {
            randCode = null;
        }

        return randCode;
    }

    @Override
    public Boolean validPhoneCode(RandCode randCode) {
        randCode = randCodeMapper.authPhoneCodes(randCode);
        Boolean flag = randCode != null;
        if (flag) {//更新验证后数据的状态
            randCodeMapper.updateByPrimaryKey(randCode);
        }
        return flag;
    }
}
