package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.model.pagerestrict.AgreementRestrict;

import java.util.List;
import java.util.Map;

/**
 * Created by Dsmart on 2016/10/24.
 */
public interface AgreementService extends GenericService<Agreement,Long> {


    /**
     * 根据用户编号和合约编号获取合约
     * @param userId
     * @param agreementId
     * @return
     */
    Agreement getAgreementByUserIdAndAgreementId(Long userId,Long agreementId);

    /**
     * 获取用户当前活跃的合约
     * @param userId
     * @return
     */
    List<Agreement> listAllActivityAgreementByUserId(Long userId);

    /**
     *根据合约类型获取合约列表
     * @param agreementRestrict
     * @return
     */
    public Page<Agreement> listAgreementBySignType(AgreementRestrict agreementRestrict);


    /**
     * 获取合约列表
     * @param agreementRestrict
     * @return
     */
    public Page<Agreement> listAgreement(AgreementRestrict agreementRestrict);


    /**
     * 申请签约
     * @param agreementRestrict
     * @return
     */
    public Result signAgreement(AgreementRestrict agreementRestrict);


    /**
     * 生成合约的时候扣除利息
     *
     * @param agreementId
     * @return
     */
    Result interestPaymentBySignAgreement(Long agreementId);

    /**
     * 合约利息支出
     * 利息扣除后 更新合约操盘截止日期与已操盘日信息
     * @param agreementId
     * @return
     */
    public Result agreementInterestPayment(Long agreementId);


    /**
     *更新长期操盘合约的连续操盘天数
     * @param agreementId
     * @return
     */
    Boolean updateAgreementContinueOperateDay(Long agreementId);


    /**
     * 合约结算
     * @param agreementId
     * @return
     */
    Result settlementAgreement(Long agreementId,Long userId);


    /**
     * 追加保证金
     * @param agreementRestrict
     * @return
     */
    Result additionalLockMoney(AgreementRestrict agreementRestrict);


    /**
     * 获取需要结算的合约列表
     * @return
     */
    public List<Agreement> listCaculateAgreements();


    /**
     * 获取需要缴纳资金管理费的合约列表
     * @return
     */
    public List<Agreement> listNeedPayInterestAgreements();


    /**
     * 更新因股票交易导致的合约余额变化
     * @param agreement
     * @return
     */
    Boolean updateAgreementByOperateStock(Agreement agreement);


    /**
     * 更新合约数据
     * @param agreementId
     * @return
     */
    Boolean refreshAgreementInfo(Long agreementId);





    /**
     * 获取所有合约列表
     * @return
     */
    List<Agreement> listAll();


    Boolean updateByFreshAgreement(Agreement agreement);


    /**
     * 获取快逾期的合约列表
     *
     * @return
     */
    Map<Long, List<Agreement>> listAlmostOverdueAgreements();

    /**
     * 强制结算合约
     * 适用于合约未缴付利息或合约已经到最大期限
     *
     * @param agreementId
     * @return
     */
    Result forceSettlementAgreement(Long agreementId);


    /**
     * 获取由于缺少管理费而导致关闭的合约列表
     *
     * @return
     */
    List<Agreement> listLackOfManagementFeeAgreements();


    /**
     * 获取超过最大操盘期限的合约列表
     *
     * @return
     */
    List<Agreement> listOverMaxPeriodAgreements();


    /**
     * 合约结算失败 更新合约状态
     *
     * @param agreement
     * @return
     */
    Boolean updateBySettlementFailed(Agreement agreement);


    /**
     * 统计总管理费
     *
     * @param agreementId
     * @param managementFee
     * @return
     */
    Result increaseTotalManagementFee(Long agreementId, Double managementFee);


    /**
     * 统计总操盘费用
     *
     * @param agreementId
     * @param operateFee
     * @return
     */
    Result increaseTotalOperateFee(Long agreementId, Double operateFee);


    /**
     * 统计某用户当前持有的某类型合约的数量
     *
     * @param type
     * @param userId
     * @return
     */
    int countActiveAgreementByTypeAndUserId(Integer type, Long userId);


}
