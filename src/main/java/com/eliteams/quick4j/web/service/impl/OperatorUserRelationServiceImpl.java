package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.OperatorUserRelationMapper;
import com.eliteams.quick4j.web.model.OperatorUserRelation;
import com.eliteams.quick4j.web.model.pagerestrict.OperatorUserRelationRestrict;
import com.eliteams.quick4j.web.service.OperatorUserRelationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2017/1/6.
 */
@Service
public class OperatorUserRelationServiceImpl extends GenericServiceImpl<OperatorUserRelation, Long> implements OperatorUserRelationService {

    @Resource
    private OperatorUserRelationMapper operatorUserRelationMapper;

    @Override
    public GenericDao<OperatorUserRelation, Long> getDao() {
        return operatorUserRelationMapper;
    }

    @Override
    public Result addItem(OperatorUserRelation operatorUserRelation) {
        Result result = new Result("添加失败");
        if (operatorUserRelationMapper.countBeforeInsert(operatorUserRelation) ==0) {
            operatorUserRelationMapper.insert(operatorUserRelation);
            result.setResultFlag(true);
            result.setResultMsg("添加成功");
        } else {
            result.setResultFlag(false);
            result.setResultMsg("该数据已存在，不能重复添加");
        }
        return result;
    }

    @Override
    public Result updateItem(OperatorUserRelation operatorUserRelation) {
        Result result = new Result("更新失败");
        if(operatorUserRelationMapper.updateItem(operatorUserRelation)>0){
            result.setResultFlag(true);
            result.setResultMsg("更新成功");
        }
        return result;
    }

    @Override
    public Page<OperatorUserRelation> listByOperateId(OperatorUserRelationRestrict operatorUserRelationRestrict) {
        Page<OperatorUserRelation> page = new Page<>(operatorUserRelationRestrict.getPageno(),operatorUserRelationRestrict.getPagesize());
        operatorUserRelationMapper.listByOperatorId(operatorUserRelationRestrict,page);

        return page;
    }

    @Override
    public List<OperatorUserRelation> listAllByOperateId(Long adminId) {
        return operatorUserRelationMapper.listAllByOperatorId(adminId);
    }


    @Override
    public OperatorUserRelation selectByUserId(Long userId) {
        return operatorUserRelationMapper.selectByUserId(userId);
    }
}
