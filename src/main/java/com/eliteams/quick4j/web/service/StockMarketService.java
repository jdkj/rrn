package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.StockMarket;

import java.util.List;

/**
 * Created by Dsmart on 2016/11/23.
 */
public interface StockMarketService  extends GenericService<StockMarket,Long>{


    /**
     * 根据股票编码获取股票信息
     * @param stockCode
     * @return
     */
    StockMarket stockInfo(String stockCode);


    /**
     * 获取所有本地存储的股票信息
     * @return
     */
    List<StockMarket> listAll();


    /**
     * 固化股票信息
     * @param stockMarket
     */
    void updateStockInfo(StockMarket stockMarket);


}
