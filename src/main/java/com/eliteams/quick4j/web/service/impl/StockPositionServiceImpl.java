package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.StockPositionMapper;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.Agreement;
import com.eliteams.quick4j.web.model.StockPosition;
import com.eliteams.quick4j.web.model.pagerestrict.StockPositionRestrict;
import com.eliteams.quick4j.web.service.AgreementService;
import com.eliteams.quick4j.web.service.StockPositionMxService;
import com.eliteams.quick4j.web.service.StockPositionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Dsmart on 2016/10/31.
 */
@Service
public class StockPositionServiceImpl extends GenericServiceImpl<StockPosition, Long> implements StockPositionService {

    @Resource
    private StockPositionMapper stockPositionMapper;

    @Resource
    private AgreementService agreementService;

    @Resource
    private StockPositionMxService stockPositionMxService;


    @Override
    public GenericDao<StockPosition, Long> getDao() {
        return stockPositionMapper;
    }

    @Override
    public Boolean addItem(StockPosition stockPosition) {
        return stockPositionMapper.insert(stockPosition) > 0;
    }

    @Override
    public Page<StockPosition> listByAgreementId(StockPositionRestrict stockPositionRestrict) {
        Page<StockPosition> page = new Page<>(stockPositionRestrict.getPageno(), stockPositionRestrict.getPagesize());
        stockPositionMapper.listByRestrict(stockPositionRestrict, page);
        return page;
    }


    @Override
    public List<StockPosition> listByStockCode(String stockCode) {
        return stockPositionMapper.listByStockCode(stockCode);
    }

    @Override
    public StockPosition getStockPositionByAgreementIdAndUserIdAndStockCode(Long agreementId, Long userId, String stockCode) {
        StockPosition stockPosition = new StockPosition();
        stockPosition.setUserId(userId);
        stockPosition.setStockCode(stockCode);
        stockPosition.setAgreementId(agreementId);
        return stockPositionMapper.getStockPositionByAgreementIdAndUserIdAndStockCode(stockPosition);
    }

    @Override
    public Boolean addStockPosition(Long agreementId, Long userId, String stockCode, String stockName, Long stockNum, Double buyPrice) {
        //查询用户是否持有该股票
        StockPosition stockPosition = this.getStockPositionByAgreementIdAndUserIdAndStockCode(agreementId, userId, stockCode);

        if (stockPosition != null) {//用户已持有该股票
            //写入持仓明细
            stockPositionMxService.addItem(stockPosition.getPositionId(), userId, stockCode, stockName, stockNum, buyPrice);

            stockPosition.setBuyTotalExpenses(stockPosition.getBuyTotalExpenses() + buyPrice * stockNum);//总投入
            stockPosition.setStockNum(stockPosition.getStockNum() + stockNum);//总持股
            stockPosition.setBuyPrice(stockPosition.getBuyTotalExpenses() / stockPosition.getStockNum());//多次增投平均价
            stockPosition.setCurrentPrice(buyPrice);
            stockPosition.setCurrentMarketValue(stockPosition.getStockNum() * stockPosition.getCurrentPrice());//当前总价值
            stockPositionMapper.updateByPositionId(stockPosition);//更新持仓数据


        } else {//用户还未持有该股票

            stockPosition = new StockPosition();
            stockPosition.setAgreementId(agreementId);
            stockPosition.setUserId(userId);
            stockPosition.setStockCode(stockCode);
            stockPosition.setStockName(stockName);
            stockPosition.setStockNum(stockNum);
            stockPosition.setBuyPrice(buyPrice);
            stockPosition.setStockAvailableNum(0l);
            stockPosition.setBuyTotalExpenses(buyPrice * stockNum);
            stockPosition.setCurrentPrice(buyPrice);
            stockPosition.setCurrentMarketValue(stockPosition.getStockNum()*stockPosition.getCurrentPrice());
            stockPositionMapper.insert(stockPosition);
            //写入持仓明细
            stockPositionMxService.addItem(stockPosition.getPositionId(), userId, stockCode, stockName, stockNum, buyPrice);
        }

        return true;
    }

    @Override
    public Boolean reduceStockPosition(Long agreementId, Long userId, String stockCode, Long stockNum, Double sellPrice) {
        StockPosition stockPosition = this.getStockPositionByAgreementIdAndUserIdAndStockCode(agreementId, userId, stockCode);
        stockPosition.setStockNum(stockPosition.getStockNum() - stockNum);
        stockPosition.setCurrentMarketValue(stockPosition.getStockNum() * stockPosition.getCurrentPrice());//当前市场价值
        stockPosition.setBuyTotalExpenses(stockPosition.getBuyTotalExpenses() - stockPosition.getBuyPrice() * stockNum);//购买股票消耗
        if (stockPosition.getStockNum().equals(0l)) {
            stockPositionMapper.deleteByPrimaryKey(stockPosition.getPositionId());

        } else {
            stockPositionMapper.updateByPositionId(stockPosition);//更新持仓数据
        }
        agreementService.refreshAgreementInfo(agreementId);
        return true;
    }


    @Override
    public Result preReduceStockPosition(Long agreementId, Long userId, String stockCode, Long stockNum) {
        Result result = new Result("预减持失败");
        StockPosition stockPosition = this.getStockPositionByAgreementIdAndUserIdAndStockCode(agreementId, userId, stockCode);
        if (stockPosition.getStockAvailableNum() > stockNum) {
            stockPosition.setStockAvailableNum(stockPosition.getStockAvailableNum() - stockNum);
            stockPosition.setCurrentMarketValue(stockPosition.getStockNum() * stockPosition.getCurrentPrice());//当前市场价值
            stockPosition.setBuyTotalExpenses(stockPosition.getBuyTotalExpenses() - stockPosition.getBuyPrice() * stockNum);//购买股票消耗
            stockPositionMapper.updateByPositionId(stockPosition);//更新持仓数据
            result.setResultFlag(true);
            result.setResultMsg("预减持成功");
        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.AVAILABLE_STOCK_NUM_NOT_ENOUGH);
        }
        return result;
    }

    @Override
    public Boolean updateStockPositionAvailableNum(StockPosition stockPosition) {
        return stockPositionMapper.updateByPositionId(stockPosition) > 0;
    }


    @Override
    public List<StockPosition> listAll() {
        return stockPositionMapper.listAll();
    }


    @Override
    public List<StockPosition> listAllByAgreementId(Long agreementId) {
        return stockPositionMapper.listAllByAgreementId(agreementId);
    }


    @Override
    public List<StockPosition> listAllGroupByStockCode() {
        return stockPositionMapper.listAllGroupByStockCode();
    }

    @Override
    public List<StockPosition> listAllByStockCode(String stockCode) {
        return stockPositionMapper.listAllByStockCode(stockCode);
    }
}
