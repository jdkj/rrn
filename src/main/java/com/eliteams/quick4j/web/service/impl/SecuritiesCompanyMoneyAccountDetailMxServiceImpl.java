package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericDao;
import com.eliteams.quick4j.core.generic.GenericServiceImpl;
import com.eliteams.quick4j.web.dao.SecuritiesCompanyMoneyAccountDetailMxMapper;
import com.eliteams.quick4j.web.model.SecuritiesCompanyMoneyAccountDetailMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesCompanyMoneyAccountDetailMxRestrict;
import com.eliteams.quick4j.web.service.SecuritiesCompanyMoneyAccountDetailMxService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2017/3/22.
 */
@Service
public class SecuritiesCompanyMoneyAccountDetailMxServiceImpl extends GenericServiceImpl<SecuritiesCompanyMoneyAccountDetailMx, Long> implements SecuritiesCompanyMoneyAccountDetailMxService {


    @Resource
    private SecuritiesCompanyMoneyAccountDetailMxMapper securitiesCompanyMoneyAccountDetailMxMapper;

    @Override
    public GenericDao<SecuritiesCompanyMoneyAccountDetailMx, Long> getDao() {
        return securitiesCompanyMoneyAccountDetailMxMapper;
    }

    @Override
    public Boolean addBySignAgreement(Long securitiesAccountId, Long orderId, Double totalMoney, Double changeMoney) {
        SecuritiesCompanyMoneyAccountDetailMx securitiesCompanyMoneyAccountDetailMx = new SecuritiesCompanyMoneyAccountDetailMx();
        securitiesCompanyMoneyAccountDetailMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesCompanyMoneyAccountDetailMx.setOrderId(orderId);
        securitiesCompanyMoneyAccountDetailMx.setOrderType(0);
        securitiesCompanyMoneyAccountDetailMx.setTotalMoney(totalMoney);
        securitiesCompanyMoneyAccountDetailMx.setChangeMoney(changeMoney);
        return securitiesCompanyMoneyAccountDetailMxMapper.insert(securitiesCompanyMoneyAccountDetailMx) > 0;
    }

    @Override
    public Boolean addByFreeAgreement(Long securitiesAccountId, Long orderId, Double totalMoney, Double changeMoney) {
        SecuritiesCompanyMoneyAccountDetailMx securitiesCompanyMoneyAccountDetailMx = new SecuritiesCompanyMoneyAccountDetailMx();
        securitiesCompanyMoneyAccountDetailMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesCompanyMoneyAccountDetailMx.setOrderId(orderId);
        securitiesCompanyMoneyAccountDetailMx.setOrderType(1);
        securitiesCompanyMoneyAccountDetailMx.setTotalMoney(totalMoney);
        securitiesCompanyMoneyAccountDetailMx.setChangeMoney(changeMoney);
        return securitiesCompanyMoneyAccountDetailMxMapper.insert(securitiesCompanyMoneyAccountDetailMx) > 0;
    }

    @Override
    public Boolean addByWithdrawal(Long securitiesAccountId, Long orderId, Double totalMoney, Double changeMoney) {
        SecuritiesCompanyMoneyAccountDetailMx securitiesCompanyMoneyAccountDetailMx = new SecuritiesCompanyMoneyAccountDetailMx();
        securitiesCompanyMoneyAccountDetailMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesCompanyMoneyAccountDetailMx.setOrderId(-3l);
        securitiesCompanyMoneyAccountDetailMx.setOrderType(3);
        securitiesCompanyMoneyAccountDetailMx.setTotalMoney(totalMoney);
        securitiesCompanyMoneyAccountDetailMx.setChangeMoney(changeMoney);
        return securitiesCompanyMoneyAccountDetailMxMapper.insert(securitiesCompanyMoneyAccountDetailMx) > 0;
    }

    @Override
    public Boolean addByRecharge(Long securitiesAccountId, Long orderId, Double totalMoney, Double changeMoney) {
        SecuritiesCompanyMoneyAccountDetailMx securitiesCompanyMoneyAccountDetailMx = new SecuritiesCompanyMoneyAccountDetailMx();
        securitiesCompanyMoneyAccountDetailMx.setSecuritiesAccountId(securitiesAccountId);
        securitiesCompanyMoneyAccountDetailMx.setOrderId(-4l);
        securitiesCompanyMoneyAccountDetailMx.setOrderType(4);
        securitiesCompanyMoneyAccountDetailMx.setTotalMoney(totalMoney);
        securitiesCompanyMoneyAccountDetailMx.setChangeMoney(changeMoney);
        return securitiesCompanyMoneyAccountDetailMxMapper.insert(securitiesCompanyMoneyAccountDetailMx) > 0;
    }

    @Override
    public Page<SecuritiesCompanyMoneyAccountDetailMx> listByRestrict(SecuritiesCompanyMoneyAccountDetailMxRestrict securitiesCompanyMoneyAccountDetailMxRestrict) {
        Page<SecuritiesCompanyMoneyAccountDetailMx> page = new Page<>(securitiesCompanyMoneyAccountDetailMxRestrict.getPageno(), securitiesCompanyMoneyAccountDetailMxRestrict.getPagesize());
        securitiesCompanyMoneyAccountDetailMxMapper.listByRestrict(securitiesCompanyMoneyAccountDetailMxRestrict, page);
        return page;
    }
}
