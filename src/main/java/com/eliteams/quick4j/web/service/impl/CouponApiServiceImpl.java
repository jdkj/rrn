package com.eliteams.quick4j.web.service.impl;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.web.container.IntegralMoneyRateParamContainer;
import com.eliteams.quick4j.web.enums.ResponseStatusEnums;
import com.eliteams.quick4j.web.model.CouponStore;
import com.eliteams.quick4j.web.model.UserCoupon;
import com.eliteams.quick4j.web.model.enums.usercoupon.UserCouponTypeAndDescEnums;
import com.eliteams.quick4j.web.service.CouponApiService;
import com.eliteams.quick4j.web.service.CouponStoreService;
import com.eliteams.quick4j.web.service.UserAccountService;
import com.eliteams.quick4j.web.service.UserCouponService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Dsmart on 2016/12/10.
 */
@Service
public class CouponApiServiceImpl implements CouponApiService {


    @Resource
    private CouponStoreService couponStoreService;

    @Resource
    private UserAccountService userAccountService;


    @Resource
    private UserCouponService userCouponService;


    @Override
    public Result addIntegralExchangeCoupon(Long couponId, Long userId) {
        Result result = new Result("兑换失败");
        CouponStore couponStore = couponStoreService.selectById(couponId);
        if (couponStore != null && couponStore.getCouponStatus() == 1) {
            //当前优惠券是可兑换的
            if (couponStore.getIsExchangeable() == 1) {
                UserCoupon userCoupon = new UserCoupon();
                userCoupon.setCouponId(couponId);
                userCoupon.setUserId(userId);
                userCoupon.setUserCouponTypeAndDescEnums(UserCouponTypeAndDescEnums.USER_INTEGRAL_EXCHANGE);
                userCoupon.setCouponMarketValue(couponStore.getCouponMarketValue());
                userCoupon.setCouponAvailableValue(couponStore.getCouponMarketValue());
                userCoupon.setExpiredTimeSql(couponStore.getAvailablePeriod(), couponStore.getCouponStoreAvailablePeriodUnitEnums());
                userCouponService.addIntegralExchangeCoupon(userCoupon);
                result = userAccountService.couponExchangeIntegral(userId, couponStore.getCouponMarketValue()* IntegralMoneyRateParamContainer.getIntegralMoneyRate(), userCoupon.getUserCouponId());
            } else {
                result.setResponseStatusEnums(ResponseStatusEnums.COUPON_STORE_IS_NOT_EXCHANGEABLE);
            }

        } else {
            result.setResponseStatusEnums(ResponseStatusEnums.COUPON_STORE_IS_NOT_EXIST);
        }

        return result;
    }

    @Override
    public Result addSystemGiftCouponByRegister(Long userId) {
        Result result = new Result("赠送失败");
        //
        Long couponId = 1l;
        CouponStore couponStore = couponStoreService.selectById(couponId);
        UserCoupon userCoupon = new UserCoupon();
        userCoupon.setCouponId(couponId);
        userCoupon.setUserId(userId);
        userCoupon.setUserCouponTypeAndDescEnums(UserCouponTypeAndDescEnums.USER_REGISTER_GIFT);
        userCoupon.setCouponMarketValue(couponStore.getCouponMarketValue());
        userCoupon.setCouponAvailableValue(couponStore.getCouponMarketValue());
        userCoupon.setExpiredTimeSql(couponStore.getAvailablePeriod(), couponStore.getCouponStoreAvailablePeriodUnitEnums());
        userCouponService.addSystemGiftCoupon(userCoupon, UserCouponTypeAndDescEnums.USER_REGISTER_GIFT);
        result.setResultFlag(true);
        result.setResultMsg("赠送成功");
        return result;
    }
}
