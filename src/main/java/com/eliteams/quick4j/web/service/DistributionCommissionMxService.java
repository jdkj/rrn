package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.web.model.DistributionCommissionMx;
import com.eliteams.quick4j.web.model.pagerestrict.DistributionCommissionMxRestrict;

public interface DistributionCommissionMxService {


    /**
     * 添加记录
     *
     * @param distributionCommissionMx
     * @return
     */
    Result addItem(DistributionCommissionMx distributionCommissionMx);

    /**
     * 合约利息分佣
     *
     * @param distributionUserId
     * @param orderId
     * @param totalCommission
     * @param commissionProportion
     * @param customerId
     * @param introducerId
     * @param introducerLevel
     * @return
     */
    Result addByInterestCommission(Long distributionUserId, Long orderId, Double totalCommission, Double commissionProportion, Long customerId, Long introducerId, Integer introducerLevel);

    /**
     * 操作手续费分佣
     *
     * @param distributionUserId
     * @param orderId
     * @param totalCommission
     * @param commissionProportion
     * @param customerId
     * @param introducerId
     * @param introducerLevel
     * @return
     */
    Result addByOperateFeeCommission(Long distributionUserId, Long orderId, Double totalCommission, Double commissionProportion, Long customerId, Long introducerId, Integer introducerLevel);

    /**
     * 贡献佣金
     *
     * @param customerId
     * @param totalCommission
     * @param orderId
     * @return
     */
    Result increaseContributionCommissionMoney(Long customerId, Double totalCommission, Long orderId);


    /**
     * 根据请求参数获取列表数据
     *
     * @param distributionCommissionMxRestrict
     * @return
     */
    Page<DistributionCommissionMx> listBuRestrict(DistributionCommissionMxRestrict distributionCommissionMxRestrict);

}
