package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.UserCoupon;
import com.eliteams.quick4j.web.model.enums.usercoupon.UserCouponTypeAndDescEnums;


import java.util.List;

/**
 * Created by Dsmart on 2016/12/8.
 */
public interface UserCouponService extends GenericService<UserCoupon,Long> {

    /**
     * 根据用户编号获取用户所有的优惠券
     * @param userId
     * @return
     */
    List<UserCoupon> listAvailableUserCouponByUserId(Long userId);

    /**
     * 添加系统赠送优惠券
     * @param userCoupon 优惠券对象
     *@param userCouponTypeAndDescEnums  赠送来源类型
     * @return
     */
    Boolean addSystemGiftCoupon(UserCoupon userCoupon,UserCouponTypeAndDescEnums userCouponTypeAndDescEnums);


    /**
     * 添加激活码兑换优惠券
     * @param userCoupon
     * @return
     */
    Boolean addTicketsExchangeCoupon(UserCoupon userCoupon);


    /**
     * 添加用户积分购买优惠券
     * @param userCoupon
     * @return
     */
    Boolean addIntegralExchangeCoupon(UserCoupon userCoupon);


    /**
     * 在合约中使用优惠券
     * @param userId
     * @param userCouponId
     * @param agreementId
     * @return
     */
    Result updateByAttachAgreement(Long userId,Long userCouponId, Long agreementId);



    /**
     * 合约利息扣除  首先使用优惠券
     * @param userCoupon
     *
     *
     * @return
     */
    Boolean updateByReduceCouponAvailableValue(UserCoupon userCoupon);

    /**
     * 通过用户编号与用户优惠券编号 获取用户优惠券信息
     * @param userId
     * @param userCouponId
     * @return
     */
    UserCoupon getUserCouponByUserIdAndUserCouponId(Long userId,Long userCouponId);


    /**
     * 当前合约是否已经绑定了优惠券
     * @param agreement
     * @return true:已绑定 false:未绑定
     */
    Boolean isAgreementAttached(Long agreement);


    /**
     * 查询某一用户某优惠券激活码兑换的激活码数量
     * @param couponTicketId
     * @return
     */
    Integer countUserCouponByCouponTicketId(Long couponTicketId);


}
