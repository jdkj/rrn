package com.eliteams.quick4j.web.service;

import com.eliteams.quick4j.core.entity.Result;
import com.eliteams.quick4j.core.feature.orm.mybatis.Page;
import com.eliteams.quick4j.core.generic.GenericService;
import com.eliteams.quick4j.web.model.SecuritiesAccountAvailableMoneyMx;
import com.eliteams.quick4j.web.model.pagerestrict.SecuritiesAccountAvailableMoneyMxRestrict;

public interface SecuritiesAccountAvailableMoneyMxService extends GenericService<SecuritiesAccountAvailableMoneyMx, Long> {


    /**
     * 追加总资金 引起买卖股票可用资金变动
     *
     * @param securitiesAccountId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Result addByIncreaseTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney);


    /**
     * 撤出总资金 引起买卖股票可用资金变动
     *
     * @param securitiesAccountId
     * @param totalMoney
     * @param changeMoney
     * @return
     */
    Result addByReduceTotalMoney(Long securitiesAccountId, Double totalMoney, Double changeMoney);


    /**
     * 买入股票 引起买卖股票可用资金变动
     *
     * @param securitiesAccountId 券商资金编号
     * @param totalMoney          可用资金
     * @param changeMoney         变动资金
     * @param operateId           操盘交易编号
     * @param userId              用户编号
     * @return
     */
    Result addByBuyStock(Long securitiesAccountId, Double totalMoney, Double changeMoney, Long operateId, Long userId);


    /**
     * 卖出股票  引起买卖股票可用资金变动
     *
     * @param securitiesAccountId 券商资金编号
     * @param totalMoney          可用资金
     * @param changeMoney         变动资金
     * @param operateId           操盘交易编号
     * @param userId              用户编号
     * @return
     */
    Result addBySellStock(Long securitiesAccountId, Double totalMoney, Double changeMoney, Long operateId, Long userId);


    /**
     * 根据请求参数获取券商可用资金变动明细列表
     *
     * @param securitiesAccountAvailableMoneyMxRestrict
     * @return
     */
    Page<SecuritiesAccountAvailableMoneyMx> listByRestrict(SecuritiesAccountAvailableMoneyMxRestrict securitiesAccountAvailableMoneyMxRestrict);


}
