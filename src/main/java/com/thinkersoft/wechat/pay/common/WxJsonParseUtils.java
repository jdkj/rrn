/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thinkersoft.wechat.pay.common;

import java.util.HashMap;
import java.util.Map;

/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-9-13, 17:30:02
 */
public class WxJsonParseUtils {

    public static Map parseJsAccessToken(String res) {
        Map map = null;
        if (res.contains("access_token")) {
            map = new HashMap();
            res = res.replaceAll("\\}", "").replaceAll("\\{", "").replaceAll(" +", "");
            String[] pvs = res.split(",");
            for (String item : pvs) {
                item = item.replaceAll("\"", "");
                String[] pv = item.split(":");
                map.put(pv[0], pv[1]);
            }

        }

        return map;
    }

    public static Map parseJsChannelUserInfo(String res) {

        Map map = null;
        if (res.contains("openid")) {
            map = new HashMap();
            res = res.replaceAll("\\}", "").replaceAll("\\{", "").replaceAll(" +", "");
            String[] pvs = res.split(",");
            for (String item : pvs) {
                item = item.replaceAll("\"", "");
                String[] pv = item.split(":");

                if (item.contains("headimgurl")) {
                    map.put(pv[0], (pv[1] + ":" + pv[2]).replaceAll("\\\\", ""));
                } else {
                    if (pv.length > 1) {
                        map.put(pv[0], pv[1]);
                    } else {
                        map.put(pv[0], "");
                    }


                }
            }

        }

        return map;
    }

    public static void main(String[] as) {

//        String res = "{\"access_token\":\"W-IlvXXCMQjeObK94lKE2-fmrt63J6ZqSZveMQR1tx21Nopa-J5d_TaYdcbhRRZcWwvtgZ7G_kR2js42Izduo9OJSYjvb2ge0k1wNv3vsBc\",\"expires_in\":7200,\"refresh_token\":\"zWcPU27ez7iDgkhIlt_9O5V6y70q1sr3E0ITFRDG0Byy1LLPk3OzMS5-HyZAA_jrrEjpFJ-g85fVXeJgXCdg8spWgyRHLEGG96RA9DfwfDQ\",\"openid\":\"oifpMxCbzfc0mnWmDNgBsGeLA2-k\",\"scope\":\"snsapi_base\"}";
//
//        SystemParam.out.println(WxJsonParseUtils.parseJsAccessToken(res));
        String img = "http:\\/\\/wx.qlogo.cn\\/mmopen\\/1gvL9ficRs1GZqKHcPibD6ZxkHxW2JfQxBCRF8z8P8gXJYN37sicAGFX9ibvFibsFiaicSCCEWlYbKt1hNJgNNx2u9duw\\/0";

        img = img.replaceAll("\\\\", "");
        System.out.println(img);


        String res = "{\"openid\":\"ohlL6vqxcrQtrDQ9ybJCdcdE_9oI\",\"nickname\":\"随枫\",\"sex\":1,\"language\":\"zh_CN\",\"city\":\"\",\"province\":\"\",\"country\":\"不丹\",\"headimgurl\":\"http:\\/\\/wx.qlogo.cn\\/mmopen\\/ajNVdqHZLLD3W6x1yXIw22et6iaibahTN4Q9xAzahOytKWU1Jx1LUfhKQryI2vTtkE1XNlpzPGaaE00MSdyarNJg\\/0\",\"privilege\":[],\"unionid\":\"otLX0wZ233F65_9XD5ecT4gsTTyg\"}";


        System.out.println(parseJsChannelUserInfo(res));


    }

}
