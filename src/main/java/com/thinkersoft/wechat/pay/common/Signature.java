/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thinkersoft.wechat.pay.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * @description 微信支付加密相关类
 * @version 1.0
 * @author Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2015-11-10, 10:27:52
 */
public class Signature {

    /**
     * 获取加密数据
     *
     * @param map 需要加密的数据集合
     * @param key 附加加密字段
     * @return 加密后的数据
     */
    public static String getSign(Map<String, Object> map, String key) {
        ArrayList<String> list = new ArrayList<String>();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (entry.getValue() != "") {
                list.add(entry.getKey() + "=" + entry.getValue() + "&");
            }
        }
        int size = list.size();
        String[] arrayToSort = list.toArray(new String[size]);
        Arrays.sort(arrayToSort, String.CASE_INSENSITIVE_ORDER);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(arrayToSort[i]);
        }
        String result = sb.toString();
        result += "key=" + key;

        result = MD5.MD5Encode(result).toUpperCase();

        return result;
    }

}
