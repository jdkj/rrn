/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thinkersoft.wechat.pay.common;

import com.alibaba.fastjson.JSONObject;
import com.eliteams.quick4j.core.util.HttpRequestClient;
import com.eliteams.quick4j.web.model.WxJsAccessToken;
import com.thinkersoft.wechat.pay.configure.WxPayConfigurePo;
import java.util.Map;


/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-7-15, 16:22:29
 */
public class WxOpenIdUtils {

    private final static String JS_CHANNEL_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

    public static String getUserOpenId(String code) {

        return jsAccessToken(code).getOpenId();

    }

    public static WxJsAccessToken jsAccessToken(String code) {
        long start = System.currentTimeMillis();
        WxJsAccessToken wxJsAccessToken = new WxJsAccessToken();

        String res = HttpRequestClient.sendGet(String.format(JS_CHANNEL_URL, WxPayConfigurePo.APPID, WxPayConfigurePo.APPSECRET, code), "");
        JSONObject json = JSONObject.parseObject(res);
        if (json.containsKey("access_token")) {

            wxJsAccessToken.setAccessToken(json.getString("access_token"));

            wxJsAccessToken.setCreateTime(System.currentTimeMillis());

            wxJsAccessToken.setExpiresIn(Integer.parseInt(json.getString("expires_in")));

            wxJsAccessToken.setOpenId(json.getString("openid"));

        }
        long delay = System.currentTimeMillis()-start;

        System.err.println("获取微信AccessToken消耗时间:"+delay);

        return wxJsAccessToken;

    }

    public static void main(String[] as) {

        String code = "0212H0Pv0kob0o15GiNv00n7Pv02H0Pu";

        System.out.println(getUserOpenId(code));

    }

}
