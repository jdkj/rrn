/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thinkersoft.wechat.pay.common;

import com.thinkersoft.wechat.pay.configure.WxPayCommonConfigure;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2015-11-23, 2:05:54
 */
public class Log {

    /**
     * 日志记录类
     */
    private static final Logger log = Logger.getLogger(Log.class.getName());

    /**
     * 写日志，方便测试（看网站需求，也可以改成把记录存入数据库）
     *
     * @param sWord 要写入日志里的文本内容
     */
    public static void logResult(String sWord) {

        FileWriter writer = null;

        try {

            writer = new FileWriter(WxPayCommonConfigure.LOG_PATH + "wxpay_log_" + System.currentTimeMillis() + ".txt");

            writer.write(sWord);

        } catch (Exception ex) {

            log.log(Level.SEVERE, sWord, ex);

        } finally {

            if (writer != null) {

                try {

                    writer.close();

                } catch (IOException ex) {

                    log.log(Level.SEVERE, sWord, ex);

                }

            }
        }
    }

}
