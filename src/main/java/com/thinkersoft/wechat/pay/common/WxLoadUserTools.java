/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thinkersoft.wechat.pay.common;

import com.eliteams.quick4j.core.util.HttpRequestClient;
import com.eliteams.quick4j.web.model.UserPlat;
import com.eliteams.quick4j.web.model.WxJsAccessToken;

import java.util.Map;


/**
 * @description @version @author
 * Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2016-9-5, 12:06:14
 */
public class WxLoadUserTools {

    /**
     * 网页授权端获取用户信息地址
     */
    private static final String JS_CHANNEL_URL = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";

    /**
     * 通过wx js 渠道获取用户信息
     *
     * @param wxJsAccessToken
     * @return
     */
    public static UserPlat loadWxUserInfoByJsChannel(WxJsAccessToken wxJsAccessToken) {

        UserPlat userPlat = null;

        String res = HttpRequestClient.sendGet(String.format(JS_CHANNEL_URL, wxJsAccessToken.getAccessToken(), wxJsAccessToken.getOpenId()), "");
//
//        res = res.replaceAll("\\[","(").replaceAll("\\]",")");
//
//
//        JSONObject json = JSONObject.parseObject(res);
//        if (json.containsKey("openid")) {
//
//            userPlat = new UserPlat();
//
//            userPlat.setOpenId(json.getString("openid"));
//            userPlat.setImgUrl(json.getString("headimgurl"));
//
//            userPlat.setNickName(json.getString("nickname"));
//
//            if (json.containsKey("unionid")) {
//
//                userPlat.setUnionId(json.getString("unionid"));
//
//            }
//
//        }
        Map map = WxJsonParseUtils.parseJsChannelUserInfo(res);

        if (map != null) {
            userPlat = new UserPlat();

            userPlat.setOpenId(map.get("openid").toString());
            userPlat.setImgUrl(map.get("headimgurl").toString());

            userPlat.setNickName(map.get("nickname").toString());

            if (map.containsKey("unionid")) {

                userPlat.setUnionId(map.get("unionid").toString());

            }
        }

        return userPlat;
    }

}
