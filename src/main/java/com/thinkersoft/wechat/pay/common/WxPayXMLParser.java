/**
 * ΢��֧��xmL�ĵ�������
 */
package com.thinkersoft.wechat.pay.common;


import com.eliteams.quick4j.core.entity.Result;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.springframework.util.StringUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


/**
 * @description 微信支付xml文档解析工具类
 * @version 1.0
 * @author Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2015-11-17, 17:55:36
 */
public class WxPayXMLParser {

    /**
     * 日志记录类
     */
    private static Logger log = Logger.getLogger(WxPayXMLParser.class.getName());

    /**
     * 从统一下单接口返回的数据中解析出prepay_id
     *
     * @param xml 统一下单返回数据
     * <xml>
     * <return_code><![CDATA[SUCCESS]]></return_code>
     * <return_msg><![CDATA[OK]]></return_msg>
     * <appid><![CDATA[wx3ad7ae3de3bc10fc]]></appid>
     * <mch_id><![CDATA[1265942501]]></mch_id>
     * <nonce_str><![CDATA[9sSv0SB7bZXN5oeF]]></nonce_str>
     * <sign><![CDATA[E1E90D0A0C0119C60530A53D0EEB637B]]></sign>
     * <result_code><![CDATA[SUCCESS]]></result_code>
     * <prepay_id><![CDATA[wx20151117200540327d383d110887722209]]></prepay_id>
     * <trade_type><![CDATA[JSAPI]]></trade_type>
     * </xml>
     * @return
     */
    public static Result getPrepayIdFromXml(String xml) {

        Result feedbackPo = new Result();

        DocumentBuilder documentBuilder = null;

        StringReader stringReader = null;

        InputSource inputSource = null;

        Document document = null;

        if (!StringUtils.isEmpty(xml)) {

            try {

                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                stringReader = new StringReader(xml);

                inputSource = new InputSource(stringReader);

                document = documentBuilder.parse(inputSource);

                Element root = document.getDocumentElement();

                //有返回结果
                if (root.getElementsByTagName("return_code").getLength() > 0) {

                    //数据提交成功
                    if (root.getElementsByTagName("return_code").item(0).getTextContent().equalsIgnoreCase("SUCCESS")) {

                        //成功返回微信预支付标识
                        if (root.getElementsByTagName("result_code").item(0).getTextContent().equalsIgnoreCase("SUCCESS")) {

                            feedbackPo.setResultFlag(true);

                            feedbackPo.setResultMsg(root.getElementsByTagName("prepay_id").item(0).getTextContent());

                        } else {//未成功返回微信预支付标识

                            feedbackPo.setResultFlag(false);

                            feedbackPo.setResultMsg(root.getElementsByTagName("result_code").item(0).getTextContent());

                        }

                    } else {//提交数据验签不通过

                        feedbackPo.setResultFlag(false);

                        feedbackPo.setResultMsg(root.getElementsByTagName("return_msg").item(0).getTextContent());

                    }

                }

            } catch (ParserConfigurationException ex) {

                log.log(Level.SEVERE, null, ex);

            } catch (SAXException ex) {

                log.log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                log.log(Level.SEVERE, null, ex);

            }

        }

        return feedbackPo;

    }


    /**
     * 从统一下单接口返回的数据中解析出扫码支付二维码地址code_url
     *
     * @param xml 统一下单返回数据
     * <xml>
     * <return_code><![CDATA[SUCCESS]]></return_code>
     * <return_msg><![CDATA[OK]]></return_msg>
     * <appid><![CDATA[wx3ad7ae3de3bc10fc]]></appid>
     * <mch_id><![CDATA[1265942501]]></mch_id>
     * <nonce_str><![CDATA[9sSv0SB7bZXN5oeF]]></nonce_str>
     * <sign><![CDATA[E1E90D0A0C0119C60530A53D0EEB637B]]></sign>
     * <result_code><![CDATA[SUCCESS]]></result_code>
     * <prepay_id><![CDATA[wx20151117200540327d383d110887722209]]></prepay_id>
     * <trade_type><![CDATA[NATIVE]]></trade_type>
     * <code_url><![CDATA[NATIVE]]></code_url>
     * </xml>
     * @return
     */
    public static Result getCodeUrlFromXml(String xml) {

        Result feedbackPo = new Result();

        DocumentBuilder documentBuilder = null;

        StringReader stringReader = null;

        InputSource inputSource = null;

        Document document = null;

        if (!StringUtils.isEmpty(xml)) {

            try {

                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                stringReader = new StringReader(xml);

                inputSource = new InputSource(stringReader);

                document = documentBuilder.parse(inputSource);

                Element root = document.getDocumentElement();

                //有返回结果
                if (root.getElementsByTagName("return_code").getLength() > 0) {

                    //数据提交成功
                    if (root.getElementsByTagName("return_code").item(0).getTextContent().equalsIgnoreCase("SUCCESS")) {

                        //成功返回微信预支付标识
                        if (root.getElementsByTagName("result_code").item(0).getTextContent().equalsIgnoreCase("SUCCESS")) {

                            feedbackPo.setResultFlag(true);

                            feedbackPo.setResultMsg(root.getElementsByTagName("code_url").item(0).getTextContent());

                        } else {//未成功返回微信预支付标识

                            feedbackPo.setResultFlag(false);

                            feedbackPo.setResultMsg(root.getElementsByTagName("result_code").item(0).getTextContent());

                        }

                    } else {//提交数据验签不通过

                        feedbackPo.setResultFlag(false);

                        feedbackPo.setResultMsg(root.getElementsByTagName("return_msg").item(0).getTextContent());

                    }

                }

            } catch (ParserConfigurationException ex) {

                log.log(Level.SEVERE, null, ex);

            } catch (SAXException ex) {

                log.log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                log.log(Level.SEVERE, null, ex);

            }

        }

        return feedbackPo;

    }

    /**
     * 从订单查询返回的数据中获取支付状态
     *
     * @param xml
     * <xml>
     * <return_code><![CDATA[SUCCESS]]></return_code>
     * <return_msg><![CDATA[OK]]></return_msg>
     * <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
     * <mch_id><![CDATA[10000100]]></mch_id>
     * <device_info><![CDATA[1000]]></device_info>
     * <nonce_str><![CDATA[TN55wO9Pba5yENl8]]></nonce_str>
     * <sign><![CDATA[BDF0099C15FF7BC6B1585FBB110AB635]]></sign>
     * <result_code><![CDATA[SUCCESS]]></result_code>
     * <openid><![CDATA[oUpF8uN95-Ptaags6E_roPHg7AG0]]></openid>
     * <is_subscribe><![CDATA[Y]]></is_subscribe>
     * <trade_type><![CDATA[MICROPAY]]></trade_type>
     * <bank_type><![CDATA[CCB_DEBIT]]></bank_type>
     * <total_fee>1</total_fee>
     * <fee_type><![CDATA[CNY]]></fee_type>
     * <transaction_id><![CDATA[1008450740201411110005820873]]></transaction_id>
     * <out_trade_no><![CDATA[1415757673]]></out_trade_no>
     * <attach><![CDATA[订单额外描述]]></attach>
     * <time_end><![CDATA[20141111170043]]></time_end>
     * <trade_state><![CDATA[SUCCESS]]></trade_state>
     * </xml>
     * @return
     */
    public static Result getPayStatusFromQueryOrderRspXml(String xml) {

        Result feedbackPo = new Result();

        DocumentBuilder documentBuilder = null;

        StringReader stringReader = null;

        InputSource inputSource = null;

        Document document = null;

        if (!StringUtils.isEmpty(xml)) {

            try {

                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                stringReader = new StringReader(xml);

                inputSource = new InputSource(stringReader);

                document = documentBuilder.parse(inputSource);

                Element root = document.getDocumentElement();

                //有返回结果
                if (root.getElementsByTagName("return_code").getLength() > 0) {

                    //数据提交成功
                    if (root.getElementsByTagName("return_code").item(0).getTextContent().equalsIgnoreCase("SUCCESS")) {

                        //成功返回查询结果信息
                        if (root.getElementsByTagName("result_code").item(0).getTextContent().equalsIgnoreCase("SUCCESS")) {

                            feedbackPo.setResultFlag(true);

                            //交易金额
                            feedbackPo.setResultMsg(root.getElementsByTagName("total_fee").item(0).getTextContent());

                        } else {//未成功返回查询结果信息

                            feedbackPo.setResultFlag(false);

                            feedbackPo.setResultMsg(root.getElementsByTagName("result_code").item(0).getTextContent());

                        }

                    } else {//提交数据验签不通过

                        feedbackPo.setResultFlag(false);

                        feedbackPo.setResultMsg(root.getElementsByTagName("return_msg").item(0).getTextContent());

                    }

                }

            } catch (ParserConfigurationException ex) {

                log.log(Level.SEVERE, null, ex);

            } catch (SAXException ex) {

                log.log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                log.log(Level.SEVERE, null, ex);

            }

        }

        return feedbackPo;

    }

    /**
     * 从订单交易通知返回数据中获取交易状态
     *
     * @param xml
     * <xml>
     * <appid><![CDATA[wx2421b1c4370ec43b]]></appid>
     * <attach><![CDATA[支付测试]]></attach>
     * <bank_type><![CDATA[CFT]]></bank_type>
     * <fee_type><![CDATA[CNY]]></fee_type>
     * <is_subscribe><![CDATA[Y]]></is_subscribe>
     * <mch_id><![CDATA[10000100]]></mch_id>
     * <nonce_str><![CDATA[5d2b6c2a8db53831f7eda20af46e531c]]></nonce_str>
     * <openid><![CDATA[oUpF8uMEb4qRXf22hE3X68TekukE]]></openid>
     * <out_trade_no><![CDATA[1409811653]]></out_trade_no>
     * <result_code><![CDATA[SUCCESS]]></result_code>
     * <return_code><![CDATA[SUCCESS]]></return_code>
     * <sign><![CDATA[B552ED6B279343CB493C5DD0D78AB241]]></sign>
     * <sub_mch_id><![CDATA[10000100]]></sub_mch_id>
     * <time_end><![CDATA[20140903131540]]></time_end>
     * <total_fee>1</total_fee>
     * <trade_type><![CDATA[JSAPI]]></trade_type>
     * <transaction_id><![CDATA[1004400740201409030005092168]]></transaction_id>
     * </xml>
     * @return
     */
    public static Result getPayStatusFromNotifyRspXml(String xml) {

        Result feedbackPo = new Result();

        DocumentBuilder documentBuilder = null;

        StringReader stringReader = null;

        InputSource inputSource = null;

        Document document = null;

        if (!StringUtils.isEmpty(xml)) {

            try {

                documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();

                stringReader = new StringReader(xml);

                inputSource = new InputSource(stringReader);

                document = documentBuilder.parse(inputSource);

                Element root = document.getDocumentElement();

                //有返回结果
                if (root.getElementsByTagName("return_code").getLength() > 0) {

                    //数据提交成功
                    if (root.getElementsByTagName("return_code").item(0).getTextContent().equalsIgnoreCase("SUCCESS")) {

                        //成功返回查询结果信息
                        if (root.getElementsByTagName("result_code").item(0).getTextContent().equalsIgnoreCase("SUCCESS")) {

                            feedbackPo.setResultFlag(true);

                            //商户订单号与交易金额
                            feedbackPo.setResultMsg(root.getElementsByTagName("out_trade_no").item(0).getTextContent() + ";" + root.getElementsByTagName("cash_fee").item(0).getTextContent());

                        } else {//未成功返回查询结果信息

                            feedbackPo.setResultFlag(false);

                            feedbackPo.setResultMsg(root.getElementsByTagName("result_code").item(0).getTextContent());

                        }

                    } else {//提交数据验签不通过

                        feedbackPo.setResultFlag(false);

                        feedbackPo.setResultMsg(root.getElementsByTagName("return_msg").item(0).getTextContent());

                    }

                }

            } catch (ParserConfigurationException ex) {

                log.log(Level.SEVERE, null, ex);

            } catch (SAXException ex) {

                log.log(Level.SEVERE, null, ex);

            } catch (IOException ex) {

                log.log(Level.SEVERE, null, ex);

            }

        }

        return feedbackPo;

    }

}
