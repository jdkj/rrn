/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thinkersoft.wechat.pay.configure;

/**
 * @description 微信请求接口集合
 * @version 1.0
 * @author Dsmart<Email:yuliujian@30buy.com   QQ:476851641    Phone:18659032066>
 * @time 2015-11-9, 17:40:09
 */
public class ServerConfigure {

    //统一下单api
    public static String UNIFIED_ORDER = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    //查询订单api
    public static String ORDER_QUERY = "https://api.mch.weixin.qq.com/pay/orderquery";

    //关闭订单api
    public static String CLOSE_ORDER = "https://api.mch.weixin.qq.com/pay/closeorder";

    //申请退款
    public static String REFUND = "https://api.mch.weixin.qq.com/secapi/pay/refund";

}
