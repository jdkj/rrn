<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/12
  Time: 16:27
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css" />

    <title>人人牛-股市配置平台</title>
    <link rel="alternate" media="only screen and (max-width: 640px)" href="http://m.rrrrrrrrrrrrrrrrrrr.com/">
</head>
<body>
<div class="banner realtive">
    <!--导航-->
    <div class="w1200">
        <div class="top">
            <div class="fl"><img src="<%=basePath%>/rrn/1.0/images-assets/logo.png" alt="" /></div>
            <div class="links fr">
                <a href="/rest/page/login">登录</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/rest/page/registe">注册</a>
            </div>
           <div class="cl"></div>
        </div>
    </div>
    <!--内容-->
    <div class="center" style="margin-top:120px;"><img src="<%=basePath%>/rrn/1.0/images-assets/banner_title1.png" alt="" /></div>
    <div class="center" style="margin-top:120px;"><a href="#"><img src="<%=basePath%>/rrn/1.0/images-assets/baoming.png" alt="" /></a></div>
</div>
<!--人人牛做什么-->
<!--人人牛做什么-->
<!--人人牛做什么-->
<div class="row1">
    <div class="w1200">
        <div class="ml30" style="width:650px;margin-top:100px;">
            <div><span class="row1Font1">人人牛做什么?</span><span class="row1Font2">WHAT ARE WE DOING?</span></div>
            <div class="row1Font3" style="margin-top:30px;">让用户实现更多投资成就</div>
            <div class="row1Font4">人人牛为用户提供更快捷的投资咨询服务。以创新的形式，使智慧对接财富，依托强大的金融信息咨询平台和投资人关系网络，以专业的新闻视角和丰富的数据资源，为业内人士提供客观、时效、高品质、全覆盖的资讯服务。助力用户实现更多投资成就。</div>
        </div>
    </div>
</div>
<!--人人牛优势-->
<!--人人牛优势-->
<!--人人牛优势-->
<div class="row2">
    <div class="w1200">
        <div class="row2Title center mt60"><p>人人牛优势</p><span>OUR ADVANTAGES</span></div>
        <div class="center mt20"><img src="<%=basePath%>/rrn/1.0/images-assets/black_lline1.png" alt="" /></div>
        <div class="row2ItemGroup">
            <div class="items fl">
                <img src="<%=basePath%>/rrn/1.0/images-assets/ys_icon1.png" alt="" />
                <h1>快捷开启盈利之门</h1>
                <p>快速注册   便捷体验<br>一键撬动财富梦想</p>
            </div>
            <div class="items fl">
                <img src="<%=basePath%>/rrn/1.0/images-assets/ys_icon1.png" alt="" />
                <h1>随时抓住投资机遇</h1>
                <p>多屏适应   实时覆盖<br> 即刻掌握财富先机</p>
            </div>
            <div class="items fl">
                <img src="<%=basePath%>/rrn/1.0/images-assets/ys_icon1.png" alt="" />
                <h1>用心服务创造价值</h1>
                <p>专业风控   高效客服<br> 助力更多投资成就</p>
            </div>
            <div class="cl"></div>
        </div>
        <div class="center"><img src="<%=basePath%>/rrn/1.0/images-assets/black_lline1.png" alt="" /></div>
    </div>
</div>
<!--选择人人牛-->
<!--选择人人牛-->
<!--选择人人牛-->
<div class="row3">
    <div class="w1200">
        <div class="row3Title">
            <b>选择人人牛</b>
            <span>SAFETY AND SECURITY</span>
        </div>
        <div class="row3ItemsGroup" style="">
            <div class="item">
                <img src="<%=basePath%>/rrn/1.0/images-assets/xz_icon1.png" alt="" class="fl"/>
                <div class="wrap">
                    <h1>实力深厚</h1>
                    <p>拥有强大的金融背景，以不凡的专业能力为投资者保驾护航。</p>
                </div>
                <div class="cl"></div>
            </div>
            <div class="item mt20">
                <img src="<%=basePath%>/rrn/1.0/images-assets/xz_icon2.png" alt="" class="fl"/>
                <div class="wrap">
                    <h1>信息安全</h1>
                    <p>拥有资深IT团队、世界级云服务器、信息加密处理，悉心保护用户信息安全。</p>
                </div>
                <div class="cl"></div>
            </div>
        </div>
    </div>
</div>

<!--愿景-->
<!--愿景-->
<!--愿景-->
<div class="row4">
    <div class="w1200 center">
        <div class="row4Title mt60"><span>OUR GOAL</span><br><b>人人牛愿景</b></div>
        <h1>让用户实现更多投资成就</h1>
        <h2>跨越投资藩篱，开拓财富人生新通道</h2>
        <p>在日夜更迭的每一天，卓越不凡的我们将尽心尽力，以创新的互联网技术及投资信息服务，帮助用户打破投资壁垒，锻造财富增值的阶梯<br>脚踏实地一步一个脚印，为用户实现更多投资成就，让财富自由的梦想不再遥远。</p>
    </div>
</div>

<!--foot-->
<!--foot-->
<!--foot-->
<div class="foot">
    <div class="w1200">
        <!--左边的-->
        <div class="fl">
            <div class="navFoot mt10">
                <a href="#">关于我们</a> |
                <a href="#">加入人人牛</a> |
                <a href="#">联系试</a>
            </div>
            <div class="color999 mt10">风险提示: 保护投资者利益是中国证监会工作的重中之重。<br>我们提醒您：股市有风险，投资需谨慎！市场风险莫测，务请谨慎行事！</div>
            <div class="colorfff mt10">版权所有 © 人人牛 闽ICP备10006454号-4</div>
        </div>
        <!--右边的-->
        <div class="qrcode fr">
            <img src="<%=basePath%>/rrn/1.0/images-assets/qrcode.png" alt="" /><div class="colorfff">关注微信公众号</div>
        </div>
        <div class="fr mt20" style="margin-right: 70px;;">
            <div class="fs16 color999">客服电话</div>
            <div class="fs30 fontbold colorfff">0592-5917777</div>
            <div class="fs16 color999">周一~周日 9:00 ~ 18:00</div>
        </div>
        <div class="cl"></div>
    </div>
</div>
</body>
</html>

