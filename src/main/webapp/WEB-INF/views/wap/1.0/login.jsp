<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 9:12
  Desc: 移动端网页登录
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>登录</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/register.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/login.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap">
    <div class="module-noborder">
        <ul class="list-haveline clearfix">
            <li>
                <span class="fb font-navline">登录人人牛&nbsp;</span>
            </li>
            <li></li>
        </ul>

        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-yonghu"><i class="icon-yonghu" style=""></i></span>
                <div class="line-down">
                    <input class="longbox" id="username" type="tel" placeholder="用户名/手机号码" style="">
                </div>
            </li>
        </ul>
        <ul class="list-anotherline clearfix" id="login-telcode">
            <li>
                <span class="pic-yanzheng"><i class="icon-duanxinyanzheng" style=""></i></span>
                <div class="line-no">
                    <input class="longbox" id="verifyCode" type="number" placeholder="手机验证码" style="">
                    <button class="getcode duanxin font-blue ng-binding" disabled="true">获取短信验证码</button>
                </div>
            </li>
        </ul>
        <ul class="list-anotherline clearfix" id="login-pass" style="display:none;">
            <li>
                <span class="pic-lock"><i class="icon-mima" style=""></i></span>
                <div class="line-no">
                    <input class="longbox" id="password" type="password" placeholder="登录密码" style="">
                    <a href="#" id="JS-chageLoginMethod" class="duanxin font-blue">忘记密码?</a>
                </div>
            </li>
        </ul>
    </div>
    <div class="blank-85"></div>
    <div>
        <button class="btn-full btn-orange bt-submit" disabled="true">一键登录注册</button>
    </div>
</div>
</body>
</html>
