<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/3/14
  Time: 10:29
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>关于</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/helpCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap">
    <div class="module-noborder">
        <div class="introduction fb font-size14 font-navline">赚外快</div>
        <div class="list-rightspace">
            <div class="bg-blue">引荐码</div>

            <p class="artical font-size12 font-gray6">
                <img src="/rest//page/qrcode/?content=http://www.rrniu.cn/rest/page/s/${distributionUser.distributionUserId}"
                     width="200px" alt="">
            </p>
            <div class="bg-blue">引荐人奖励</div>
            <p class="artical font-size12 font-gray6">被邀请人发起操盘合约并支付管理费，引荐人可获得一定比率的返现奖励</p>
            <p class="artical font-size12 font-gray6"> 后台(<a href="http://fx.rrniu.cn">http://fx.rrniu.cn</a>)帐号可查询当前返现数据。登录帐号:${distributionUser.accountNum},初始密码:123456
            </p>
        </div>
    </div>
</div>
</body>
</html>
