<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/12
  Time: 17:02
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>
<html>
<head>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <title>人人牛</title>
        <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
        <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
        <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css?20171101">
        <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
        <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/swiper.min.css">
        <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
        <script src="<%=basePath%>/rrn/wap.1.0/js/swiper.min.js"></script>
        <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
        <base target="_self" >
        <style type="text/css">

            .swiper-container {
                margin: 0 auto;
            }

            .swiper-slide {
                text-align: center;
                font-size: 18px;
                background: #fff;
            }
        </style>
    </head>
<body>
<div class="swiper-container" id="swiper">
    <div class="swiper-wrapper">
        <div class="swiper-slide" style="background:green;"><img src="/rrn/wap.1.0/images-assets/intro_1.jpg" alt=""
                                                                 width="100%"></div>
        <div class="swiper-slide" style="background:yellow;"><img src="/rrn/wap.1.0/images-assets/intro_2.jpg" alt=""
                                                                  width="100%"></div>
        <div class="swiper-slide" style="background:orange;"><img src="/rrn/wap.1.0/images-assets/intro_4.jpg" alt=""
                                                                  width="100%"></div>
    </div>
    <div class="swiper-pagination"></div>
</div>

<%--<jsp:include page="common/dashboard.bottom.nav.jsp" flush="false"></jsp:include>--%>
<div class="footnavwrap">
    <nav class="bottom-nav-w clearfix">
        <a class="cell active" href="javascript:void(0)"> <i class="icon-financ font-size24"></i> <br>活动</a>
        <a class="cell " href="/rest/wap/trade/type/0/"> <i class="icon-caopan font-size24"></i> <br>操盘</a>
        <a class="cell" href="/rest/wap/personalcenter/"><i class="icon-wo1 font-size24"></i><br>我的</a>
    </nav>
</div>
<script>
    window.onload = function () {
        var swiper = document.getElementById("swiper");
        var scale = window.screen.height / window.screen.width;
        swiper.style.height = (document.body.clientHeight - 55) + 'px';
    }
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 0,
        centeredSlides: true,
        autoplay: 300000,
        autoplayDisableOnInteraction: false
    });
</script>

<script>
    <c:choose>
    <c:when test="${empty userInfo &&isWeChat}">
    $(function(){
        window.location.href= "/rest/wap/page/login";
    });
    </c:when>
    </c:choose>

</script>
</body>
</html>
