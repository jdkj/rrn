<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 22:10
  Desc: 用户银行卡信息页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>用户中心- 银行卡绑定</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/personCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self" >
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div   class="wrap bg-gray  "  >
    <div class="common-list">
        <!-- ngRepeat: b in bankList --><ul class="list-haveline clearfix  "  >
        <a>
            <li class="font-gray3">
                ${bankAccount.bankName}
            </li>
            <li><span class="card-number  "  >${bankAccount.modifiedCardNo}</span>
                <i class="font-dorange icon-binding" ></i></li>
        </a>
    </ul>

    </div>
</div>
</body>
</html>
