<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/17
  Time: 20:40
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>当日委托</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/tradeClient.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div   class="wrap bg-gray  ">

    <c:choose>
        <c:when test="${pageResult!=null&&pageResult.result!=null&&fn:length(pageResult.result)>0}">
            <div class="deal-module">
                <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <tr class="none-topline">
                        <th width="25%">名称/代码</th>
                        <th width="28%">价格/数量</th>
                        <th width="23%">状态/类型</th>
                        <th width="24%" class="t-right">委托时间</th>
                    </tr>
                    </tbody>
                    <c:forEach var="stockOperate" items="${pageResult.result}">
                        <tr class="" style="">
                            <td class="first-column "><span class="">${stockOperate.stockName}</span><span class="">${stockOperate.stockCode}</span></td>
                            <td>

                                <span class=""><fmt:formatNumber pattern="#0.00" type="number" value="${stockOperate.commissionPrice}"></fmt:formatNumber> </span>
                                <span  class="">${stockOperate.commissionNum}</span>

                            </td>
                            <td><span class="">${stockOperate.operateStatusDesc}</span><span class="<c:choose><c:when test="${stockOperate.operateType==1}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">${stockOperate.operateTypeDesc}</span></td>
                            <td><span class="font-gray3"><fmt:parseDate pattern="yyyy-MM-dd" value="${stockOperate.createTime}" var="yDate"/> <fmt:formatDate pattern="yyyy-MM-dd" value="${yDate}"></fmt:formatDate></span><span class=""><fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${stockOperate.createTime}" var="tDate"/> <fmt:formatDate pattern="HH:mm:ss" value="${tDate}"></fmt:formatDate></span></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <div class="page-middel clearfix">
                <ul id="pagination-flowing" class="pagination-sm pagination">
                    <c:choose>
                        <c:when test="${pageResult.totalPages>1}">
                            <c:choose>
                                <c:when test="${pageResult.pageNo>1}">
                                    <li class="first"><a href="/rest/wap/trade/${agreement.showAgreementId}/query/current/commission/1">首页</a></li>
                                    <li class="prev "><a href="/rest/wap/trade/${agreement.showAgreementId}/query/current/commission/${pageResult.pageNo-1}">上一页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                    <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                <c:choose>
                                    <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                        <li class="page">
                                            <a href="/rest/wap/trade/${agreement.showAgreementId}/query/current/commission/${pageIndex}">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                    <c:when test="${pageResult.pageNo==pageIndex}">
                                        <li class="page active">
                                            <a href="javascript:void(0)">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                    <li class="next "><a href="/rest/wap/trade/${agreement.showAgreementId}/query/current/commission/${pageResult.pageNo+1}">下一页</a></li>
                                    <li class="last "><a href="/rest/wap/trade/${agreement.showAgreementId}/query/current/commission/${pageResult.totalPages}">尾页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                    <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                            <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                            <li class="page active"><a href="javascript:void(0)">1</a></li>
                            <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                            <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                        </c:otherwise>
                    </c:choose>

                </ul>
            </div>
        </c:when>
        <c:otherwise>
            <div class="deal-module">
                <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr class="none-topline">
                        <th width="25%">名称/代码</th>
                        <th width="28%">价格/数量</th>
                        <th width="23%">状态/类型</th>
                        <th width="24%" class="t-right">委托时间</th>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="mt_pic"></div>
        </c:otherwise>
    </c:choose>

</div>

</body>
</html>
