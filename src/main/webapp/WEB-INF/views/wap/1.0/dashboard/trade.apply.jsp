<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/15
  Time: 22:04
  Desc: 操盘配置申请页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>我要操盘</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css?20171101">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/applyTrade.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/swiper.min.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/applytrade.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/swiper.min.js"></script>
    <base target="_self">
    <style>
        #interest-wrap {
            height: 70px;
            padding-top: 8px;
            font-size: 16px;
            line-height: 10px;
            position: relative;
        }

        #interest-wrap .fx-value {
            position: absolute;
            bottom: 10px;
            left: 0;
            right: 0;
            font-size: 14px;
        }

        .trade-module {
            margin-left: 15px;
            padding-top: 5px;
            border-bottom: 1px solid #E5E5E5;
        }

        .block-noright, .block-padright {
            margin-bottom: 5px;
        }

        .title-public {
            margin-bottom: 5px;
            margin-right: 15px;
            font-size: 14px;
            color: #666;
        }

    </style>
</head>
<body>
<%--<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>--%>
<article class="">

    <!-- 菜单 -->
    <nav class="head-nav clearfix">
        <a class="width50 select"> <i class="icon-trade-selected"></i> <br>我要操盘</a>
        <a class="width50" href="/rest/wap/trade/?_t=<%=mills%>"><i class="icon-mytrade"></i><br>我的操盘</a>
    </nav>

    <c:choose>
        <%--当前存在可申请的配资类型--%>
        <c:when test="${borrowAgreementTypeList!=null&&fn:length(borrowAgreementTypeList)>0}">
            <!-- tab -->
            <nav class="head-subnav trade-nav">
                <c:forEach var="borrowAgreementType" items="${borrowAgreementTypeList}">
                    <a href="/rest/wap/trade/type/${borrowAgreementType.borrowType}/?_t=<%=mills%>"
                            <c:choose>
                                <c:when test="${borrowAgreementType.borrowType==tradeType}">class="selected"</c:when>
                            </c:choose>>${borrowAgreementType.borrowTypeDesc}</a>
                </c:forEach>
            </nav>

            <div class="wrap" style="margin-bottom: 44px">
                <!-- 申请额度 -->
                <div class="trade-module">
                    <div class="title-public clearfix">
                        <div class="fl">申请额度</div>
                        <span class="title-tip fl" style="">100%交易盈利</span>
                            <%--<div class="fr font-blue" id="JS-todayLimitBuy">今日限买股</div>--%>
                        <div class="fr font-blue" id=""><i class="icon-bangzhuzhongxin font-size20 JS-showhelp"
                                                           data-index=1></i> <span id="JS-todayLimitBuy">今日限买股</span>
                        </div>
                    </div>
                    <div class="block-padright" id="applyAgreementContainer">
                        <input class="input-box" id="input-money" type="tel"
                               data-min="<fmt:formatNumber pattern="#0" type="number" value="${currnetBorrowType.borrowMinMoney}"></fmt:formatNumber>"
                               data-max="<fmt:formatNumber pattern="#0" type="number" value="${currnetBorrowType.borrowMaxMoney}"></fmt:formatNumber>"
                               placeholder="最少<fmt:formatNumber pattern="#0" type="number" value="${currnetBorrowType.borrowMinMoney}"></fmt:formatNumber>，最多<fmt:formatNumber pattern="#0" type="number" value="${currnetBorrowType.borrowMaxMoney/10000}"></fmt:formatNumber>万，千的整数倍"
                               style="">
                    </div>
                </div>

                <!-- 风险保证金 -->
                <div class="trade-module" id="domsFXBZJ" style="display:none">
                    <div class="title-public">风险保证金</div>
                    <ul class="listbox-public clearfix JS-fxbzj">
                        <c:set var="indexCount" value="1"></c:set>
                        <c:forEach var="borrowAgreement" items="${borrowAgreementList}">
                            <li id="interest-wrap"
                                class="JS-trade-investAmount  <c:choose><c:when test="${indexCount==1}">selected</c:when></c:choose>">
                                <div data-agreement-id="${borrowAgreement.borrowAgreementId}"
                                     data-multiple="${borrowAgreement.multiple}"
                                     data-multiple-rate="${borrowAgreement.multipleRate}"
                                     data-borrow-agreement-type="${borrowAgreement.borrowType}"
                                     class="">${borrowAgreement.multiple}倍
                                </div>
                                <c:choose>
                                    <c:when test="${borrowAgreement.borrowType>0}">
                                        <div class="month-value">月利率<fmt:formatNumber type="number" pattern="#0.00"
                                                                                      value="${borrowAgreement.multipleRate/12}"/>%
                                        </div>
                                    </c:when>
                                </c:choose>
                                <div class="fx-value JS-invest-val">元</div>
                            </li>
                            <c:set var="indexCount" value="${indexCount+1}"></c:set>
                        </c:forEach>
                    </ul>
                </div>

                <!-- 说明1 -->
                <div class="module-spaceleft">
                    <ul class="list-haveline clearfix">
                        <li>单票持仓 <i class="icon-bangzhuzhongxin font-size20 JS-showhelp" data-index=2></i></li>
                        <li>
                            <span class="">主板${currnetBorrowType.mainBoard}%，创业板${currnetBorrowType.gem}%</span>
                        </li>
                    </ul>
                    <ul class="list-noline clearfix">
                        <li>持仓时间</li>
                        <li class="pr15">自动延期，最长${currnetBorrowType.maxPeriod}个<c:choose><c:when
                                test="${currnetBorrowType.borrowType==0}">交易日</c:when><c:otherwise>月</c:otherwise></c:choose></li>
                    </ul>
                </div>

                <!-- 数据 -->
                <div class="module-spaceleft statics-info">
                    <ul class="list-haveline clearfix">
                        <li>总操盘资金</li>
                        <li><span class="">0</span> <span>元</span></li>
                    </ul>
                    <ul class="list-haveline clearfix">
                        <li>亏损警戒线<i class="icon-bangzhuzhongxin font-size20 JS-showhelp" data-index=3></i></li>
                        <li><span class="font-orange">0</span> <span class="font-orange">元</span></li>
                    </ul>
                    <ul class="list-haveline clearfix">
                        <li>亏损平仓线<i class="icon-bangzhuzhongxin font-size20 JS-showhelp" data-index=4></i></li>
                        <li><span class="font-green">0</span> <span class="font-green">元</span></li>
                    </ul>
                    <ul class="list-haveline clearfix">
                        <li>风险保证金 <i class="icon-bangzhuzhongxin font-size20 JS-showhelp" data-index=5></i></li>
                        <li><span class="">0</span> <span>元</span></li>
                    </ul>
                    <ul class="list-haveline clearfix">
                        <li>预存管理费&nbsp;<span class="font-size12 font-gray9">非交易日不收取</span></li>
                        <li><span class="">0</span> <span class="">元<span class="">/<c:choose><c:when
                                test="${currnetBorrowType.borrowType==0}">交易日</c:when><c:otherwise>月</c:otherwise></c:choose></span></span>
                        </li>
                    </ul>
                        <%--<ul class="list-haveline clearfix">--%>
                        <%--<li>抵用券</li>--%>
                        <%--<li ><span class=""><span class="font-red">0</span> 张可用<i class="icon-more1" style=""></i></span> </li>--%>
                        <%--</ul>--%>
                </div>

                <!-- 协议 -->
                <div class="trade-xy pay-money-padding" style="">
                    申请即表示已阅读并同意<a id="JS-tradeProtocol" class="font-blue" href="javascript:void(0)">《操盘协议》</a>
                </div>

                <!-- 结算，按钮 -->
                <div class="trade-pay">
                    <div class="pay-money" style="padding-top:5px;">
                        <span class="font-size16 font-gray3 fb">共计应支付：</span>
                        <span class="font-size16 font-orange fb">0</span>
                        <br>
                            <%--<span class="font-red">*</span> 您预期还将获得 <span class="font-red">0</span> 积分--%>
                        <div class="pay-info" style="padding:0 0 5px 0;">准备资金 = 风险保证金 + <c:choose><c:when
                                test="${currnetBorrowType.borrowType==0}">日管理费 x 天数</c:when><c:otherwise>月管理费 x 月数</c:otherwise></c:choose></div>
                    </div>
                    <div class="cancel-btn">
                        <a class="btn-orange btn-full" href="javascript:void(0)" id="JS-instant-apply">立即申请</a>
                    </div>
                </div>
            </div>

            <!-- *********************************************************隐藏的层********************************************************* -->
            <!-- 今日限买肌股 -->
            <div id="popup-todayLimit" class="trade-popup-container" style="display:none;">
                <div class="trade-popup">
                    <div class="trade-popup-head">今日限买股</div>
                    <div class="trade-popup-body trade-limit-wrap">
                        <c:choose>
                            <c:when test="${forbidStockList!=null&&fn:length(forbidStockList)>0}">
                                <c:forEach var="forbidStock" items="${forbidStockList}">
                                    <div class="trade-limit"><span
                                            class="trade-stockcode">${forbidStock.stockCode}</span> <span
                                            class="trade-stockname">${forbidStock.stockName}</span></div>
                                </c:forEach>
                            </c:when>
                        </c:choose>
                    </div>
                    <div class="trade-tip"><i class="icon-jinggao"></i> <span>平台不可交易基金、S、ST、*ST、S*ST及SST类股票。出于风控需求，对些部分股票也会限制买入。</span>
                    </div>
                    <div class="trade-popup-buttons">
                        <button>确定</button>
                    </div>
                </div>
            </div>
        </c:when>
        <c:otherwise>
            <div class="mt_pic"></div>
        </c:otherwise>
    </c:choose>
</article>


<%--<jsp:include page="../common/dashboard.bottom.nav.jsp" flush="false"></jsp:include>--%>
<div class="footnavwrap">
    <nav class="bottom-nav-w clearfix">
        <a class="cell" href="/rest/wap/page/index?_t=<%=mills%>"> <i class="icon-financ font-size24"></i> <br>活动</a>
        <a class="cell active"> <i class="icon-caopan font-size24"></i> <br>操盘</a>
        <a class="cell" href="/rest/wap/personalcenter/?_t=<%=mills%>"><i class="icon-wo1 font-size24"></i><br>我的</a>
    </nav>
</div>
<!-- Swiper -->
<div id="popup-tips" class="popup-container" style="opacity:0;z-index:-1">
    <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide helpeInfo">
                <div class="title"><s class="left"></s><em>今日限买股</em><s class="right"></s></div>

                <div class="description">平台不可交易基金、S、ST、*ST、S*ST及SST类股票。出于风控需求，对些部分股票也会限制买入。</div>
            </div>
            <div class="swiper-slide helpeInfo">
                <div class="title"><s class="left"></s><em>单票持仓</em><s class="right"></s></div>
                <div class="description">根据不同合约出于风控需求会设定一个股票在当前合约可购买比率。</div>
            </div>
            <div class="swiper-slide helpeInfo">
                <div class="title"><s class="left"></s><em>亏损警戒线</em><s class="right"></s></div>
                <div class="description">警戒线就是平仓线之上的一个额度，在客户资金首先触及警戒线的时候一般就应该注意操作风险了，及时减仓或者追加保证金到警戒线之上。</div>
            </div>
            <div class="swiper-slide helpeInfo">
                <div class="title"><s class="left"></s><em>亏损平仓线</em><s class="right"></s></div>
                <div class="description">所谓的平仓线就是客户保证金亏损到一定点位，配资公司有权对股票或期货账户进行强行平仓</div>
            </div>
            <div class="swiper-slide helpeInfo">
                <div class="title"><s class="left"></s><em>风险保证金</em><s class="right"></s></div>
                <div class="description">交易者在持仓过程中，会因行情不断变化而产生浮盈亏，因而保证金实际可用来弥补亏损和提供担保的资金</div>
            </div>

        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</div>
<script>
    //给标题加上一个下一个的标题
    $(".helpeInfo").each(function () {
        var $this = $(this);
        var _lt = $this.prev(".helpeInfo").find('em').text();
        var _rt = $this.next(".helpeInfo").find('em').text();
        _lt = _lt == '' && ' ' || '←' + _lt;
        _rt = _rt == '' && ' ' || _rt + '→';
        $this.find(".left").text(_lt);
        $this.find(".right").text(_rt);
    });
    //初始化轮播
    var mySwiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true
    });
    //setTimeout(function(){mySwiper.slideTo(3, 100, false);},2000);

    //打开说明轮播
    $('.JS-showhelp').on('click', function (event) {
        console.log($(this).data('index'));
        mySwiper.slideTo($(this).data('index') - 1, 10, false)
        $("#popup-tips").css({"opacity": 1, "z-index": 1}).show();

    });

</script>


<script>
    $(function () {
        // 今日限买股弹出
        $('#JS-todayLimitBuy').on('click', this, function (event) {
            action_showPopup('#popup-todayLimit');
        });
        //今日限买股关闭
        $('.trade-popup-container').on('click', this, function (event) {
            (event.target == this || event.target == $('.trade-popup-buttons>button')[0]) && $(this).hide();
        });


        //输入配资申请资金
        $("#input-money").on("keyup", this, function () {

            var item = $(this);
            var min = item.data("min");//最少操盘申请资金
            var max = item.data("max");//最多操盘申请资金
            var borrowMoney = item.val();
            //申请配资金额必须为千的整数倍且必须在可申请资金的区间内
            if (borrowMoney >= min && borrowMoney <= max && borrowMoney / 1000 > 0 && borrowMoney % 1000 == 0) {
                //显示当前可选倍数
                caculateLockMoneys();
                staticsticsAgreementInfo();
            } else {
                $("#domsFXBZJ").hide();
            }
            return false;
        });
        $("#applyAgreementContainer").on("click", this, function () {
//          $.scrollTo('#applyAgreementContainer',500);

//            $.mobile.silentScroll(100);
//            $('html').animate({ scrollTop: 100}, 500);
            $("body").height() == $(window).height() && $("body").css({"height": "auto"});
            $('body').scrollTop(150);
        });

        $(".JS-trade-investAmount").on("click", this, function () {
            var item = $(this);
            item.siblings().removeClass("selected");
            item.addClass("selected");
            staticsticsAgreementInfo();
        });


        $("#JS-instant-apply").click(function () {
            if (validInputMoney()) {
                //申请资金
                var borrowMoney = $("#input-money").val();

                var multipleItem = $(".JS-trade-investAmount.selected").find("div");
                var borrowAgreementId = multipleItem.data("agreement-id");
                var signAgreementType = multipleItem.data("borrow-agreement-type");
                var data = {
                    "borrowMoney": borrowMoney,
                    "borrowAgreementId": borrowAgreementId,
                    "signAgreementType": signAgreementType
                };
                $.post("/rest/trade/sign", data, function (res) {
                    if (res.resultFlag) {
                        alert("合约生成成功");
                        window.location.href = "/rest/wap/trade/";
                    } else {
                        if (res.statusCode == 1011) {//用户未完善手机号
                            showDialog('为了保障您的权益，请先绑定手机号!', '确定', function () {
                                window.location.href = '/rest/wap/personalcenter/setting/phone/?_t=' + new Date().getTime();
                            }, '取消');
                        } else if (res.statusCode == 8002) {//账户资金不足
                            showDialog('账户资金不足,是否前往充值!', '确定', function () {
                                window.location.href = '/rest/wap/fund/recharge/?_t=' + new Date().getTime();
                            }, '取消')
                        } else {
                            toast(res.resultMsg);
                        }

                    }
                }, "json");
            } else {
                var item = $("#input-money");
                var min = item.data("min");//最少操盘申请资金
                var max = item.data("max");//最多操盘申请资金
                toast("请输入大于" + min + "的整千数，最高" + max / 10000 + "万的投入资金");
            }
        });


    });

    /**
     * 验证输入的金额是否在允许范围内
     */
    function validInputMoney() {
        var item = $("#input-money");
        var min = item.data("min");//最少操盘申请资金
        var max = item.data("max");//最多操盘申请资金
        var borrowMoney = item.val();
        //申请配资金额必须为千的整数倍且必须在可申请资金的区间内
        if (borrowMoney >= min && borrowMoney <= max && borrowMoney / 1000 > 0 && borrowMoney % 1000 == 0) {
            return true;
        }
        return false;
    }

    /**
     * 计算需要投资的金额
     */
    function caculateLockMoneys() {
        //借款金额
        var borrowMoney = +$("#input-money").val();

        $(".JS-trade-investAmount").each(function (index) {
            var item = $(this);
            var multiple = item.find("div").eq(0).data("multiple");
            //需要锁定的资金
            var lockMoney = parseInt((borrowMoney / multiple).toFixed(0));
            item.find(".JS-invest-val").html(lockMoney + "元");
            item.data("lock-money", lockMoney);
        });
        $("#domsFXBZJ").show();
    }


    /**
     * 统计配资合约信息
     */
    function staticsticsAgreementInfo() {

        //申请资金
        var borrowMoney = +$("#input-money").val();

        var multipleItem = $(".JS-trade-investAmount.selected").find("div").eq(0);
        //配资倍数
        var multiple = multipleItem.data("multiple");
        //投入资金
        var lockMoney = parseInt((borrowMoney / multiple).toFixed(0));
        //利率(年)
        var multipleRate = multipleItem.data("multiple-rate");
        //管理费
        var managementFee;
        //管理费预交倍数
        var managementFeeRate = 1;
        //操盘周期单位
        var operatePeriod = "天";
        //配资类型
        var agreementType = multipleItem.data("borrow-agreement-type");
        switch (agreementType) {
            case 0:
                managementFee = (borrowMoney * multipleRate / 365 / 100).toFixed(2);
                managementFeeRate = 2;
                break;
            default:
                managementFee = (borrowMoney * multipleRate / 12 / 100).toFixed(2);
                operatePeriod = "月";
                break;
        }
        var resultItem = $(".statics-info").find("ul");
        //总操盘资金
        resultItem.eq(0).find("li").eq(1).find("span").eq(0).html(parseInt(borrowMoney + lockMoney).toFixed(2));
        //警戒线
        resultItem.eq(1).find("li").eq(1).find("span").eq(0).html((parseInt(borrowMoney + lockMoney * 0.5)).toFixed(2));
        //止损线
        resultItem.eq(2).find("li").eq(1).find("span").eq(0).html((parseInt(borrowMoney + lockMoney * 0.3)).toFixed(2));
        //投资本金
        resultItem.eq(3).find("li").eq(1).find("span").eq(0).html((parseInt(lockMoney)).toFixed(2));
        //管理费
        resultItem.eq(4).find("li").eq(1).find("span").eq(0).html((parseInt(managementFee)).toFixed(2));

        //共计应支付
        $(".pay-money").find("span").eq(1).html((lockMoney + managementFee * managementFeeRate).toFixed(2) + "元");

        $("#JS-instant-apply").prop("disabled", false);

    }


    $("#JS-tradeProtocol").on("click", this, function () {
        var _url = "/rest/page/protocol/trade";

        //申请资金
        var borrowMoney = +$("#input-money").val();

        var multipleItem = $(".JS-trade-investAmount.selected").find("div").eq(0);
        //配资倍数
        var multiple = multipleItem.data("multiple");
        //投入资金
        var lockMoney = parseInt((borrowMoney / multiple).toFixed(0));

        //配资类型
        var agreementType = multipleItem.data("borrow-agreement-type");

        if (borrowMoney < 2000) {
            borrowMoney = 5000;
            lockMoney = 1667;
        }

        window.location.href = _url + "?borrowMoney=" + borrowMoney + "&lockMoney=" + lockMoney + "&agreementType=" + agreementType;

    });


</script>


</body>
</html>
