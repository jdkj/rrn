<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/15
  Time: 9:38
  Desc: 密码设置页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>用户中心-密码设置</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/update.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self" >
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div   class="wrap  "  >
    <div class="title-name font-navline">
        <c:choose><c:when test="${isModify}">修改</c:when><c:otherwise>设置</c:otherwise></c:choose>登录密码
    </div>
    <div class="module-fullspace">
       <c:choose>
           <c:when test="${isModify}">
               <ul class="list-otherline clearfix">
                   <li>
                       <span class="pic-lock"><i class="icon-mima1"  ></i></span>
                       <div class="line-down">
                           <input class="longbox  " id="oldPwd" type="password" placeholder="请输入原登录密码"      >
                       </div>
                   </li>
               </ul>
           </c:when>
       </c:choose>

        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-mima1"  ></i></span>
                <div class="line-down">
                    <input class="longbox      " id="pwd"  type="password" placeholder="请输入新登录密码"      >
                </div>
            </li>
        </ul>
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-mima1"  ></i></span>
                <div class="line-no">
                    <input class="longbox " id="pwd2" type="password" placeholder="再次输入新登录密码"      >
                </div>
            </li>
        </ul>
    </div>
    <div class="module-white">
		<span class="pic-jinggao">
		  <i class="icon-jinggao"></i></span>
        <div class="tianxie font-gray9 font-size12">登录密码由6-16位数字和字母组成。</div>
    </div>
    <div class="btn-full btn-orange JSbtnsubmit"  >确认</div>
</div>

<script>

    $(function(){
        $(".JSbtnsubmit").click(function(){
            if(validPassword()){
                var oldPassword = $("#oldPwd").val();
                var pwd = $("#pwd").val();
                var data = {"orginalPassword":oldPassword,"password":pwd};
                $.post("/rest/user/modify/password",data,function(res){
                    if(res.resultFlag){
                        alert(res.resultMsg);
                        window.location.href = "/rest/wap/personalcenter/setting/";
                    }else{
                        alert(res.resultMsg);
                        return false;
                    }
                },"json");
            }

        });
    });

    /**
     * 验证密码修改/设置 信息
     * @returns {boolean}
     */
    function validPassword(){
        var settingType = +$("#settingType").val();
        var oldPassword = $("#oldPwd").val();
        var pwd = $("#pwd").val();
        var pwd2 = $("#pwd2").val();

       <c:choose>
        <c:when test="${isModify}">
        if(oldPassword===""){
            alert("请输入原密码");
            return false;
        }
        </c:when>
        </c:choose>


        if(pwd===""){
            alert("请输入新密码");
            return false;
        }

        if(pwd.length<6){
            alert("密码长度不能少于6位")
            return false;
        }

        if(pwd!=pwd2){
            alert("两次密码输入不一致");
            return false;
        }

        return true;
    }
</script>
</body>
</html>
