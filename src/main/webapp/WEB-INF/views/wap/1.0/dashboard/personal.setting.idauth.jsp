<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 23:30
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>用户中心-身份认证</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/update.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">

</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap  ">
    <div class="module-fullspace">
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-yonghu"></i></span>
                <div class="line-down">
                    <div class="line-down">
                        <input class="longbox" id="realName" type="text" placeholder="请输入真实姓名">
                    </div>
                </div>
            </li>
        </ul>
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-shimingrenzheng"></i></span>
                <div class="line-down">
                    <input class="longbox" id="idCard" type="text" placeholder="请输入您的18位身份证号">
                </div>
            </li>
        </ul>
    </div>
    <div class="module-white">
		<span class="pic-jinggao">
		  <i class="icon-jinggao"></i></span>
        <div class="tianxie font-gray9 font-size12">请填写有效的证件信息</div>
    </div>
    <div class="btn-full btn-orange JSaddBanksubmit">确认</div>
</div>
<script>
    $(function () {
        <c:choose>
        <c:when test="${userInfo.userParamInfoTags.idTag}">
        alert("您已通过实名认证");
        window.location.href = "/rest/wap/personalcenter/setting/";
        </c:when>

        <c:otherwise>

        var isClick = false;
        $(".JSaddBanksubmit").click(function () {
            if (!isClick) {
                isClick = true;
                if (validIdentification()) {
                    var realName = $("#realName").val();
                    var idCard = $.trim($("#idCard").val());
                    var data = {"idNo": idCard, "idName": realName};
                    $.ajax({
                        type: "post",
                        dataType: "json",
                        url: "/rest/user/authId",
                        data: data
                    }).done(function (res) {
                        if (res.resultFlag) {
                            alert("认证成功");
                            window.location.href = "/rest/wap/personalcenter/setting/";
                        } else {
                            alert(res.resultMsg);
                            return false;
                        }
                    }).always(function () {
                        isClick = false;
                    });
                }

            }
        });


        /**
         * 验证身份信息
         * @returns {boolean}
         */
        function validIdentification() {
            var realName = $("#realName").val();
            var idCard = $.trim($("#idCard").val());
            if (realName != '' && idCard != '' && idCard.length === 18) {
                return true;
            }
            alert("请输入正确的身份信息");
            return false;
        }


        </c:otherwise>
        </c:choose>
    })
</script>

</body>
</html>
