<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/16
  Time: 22:37
  Desc: 追加保证金页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>追加保证金</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/deposit.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self" >
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div   class="wrap">
    <div class="module-pink">
        <ul class="list-haveline clearfix">
            <li>合约编号</li>
            <li>
                <span class=" ">${agreement.showAgreementId}</span>
            </li>
        </ul>
    </div>
    <div class="blank-10"></div>
    <div class="module-noborder">
        <ul class="list-line clearfix">
            <li>账户余额</li>
            <li>
                <span>${userAccount.money}元</span>
            </li>
        </ul>
        <ul class="list-line clearfix">
            <li>追加保证金</li>
            <li>
                <input class="input-shortbox input-additional" type="number" placeholder="请输入追加金额"  >
                <span>元</span>
            </li>
        </ul>
        <div class="module-shortborder font-size12" style="border-top:1px solid #E5E5E5;padding:10px 0 20px 0px;margin-left: 60px;">
            <i class="icon-jinggao font-orange"></i><span class="font-gray9">&nbsp;&nbsp;最低追加金额 </span>
            <span class="font-orange   ng-isolate-scope"  ><fmt:formatNumber pattern="#0.00" type="number" value="${(agreement.borrowMoney+agreement.lockMoney)*0.01}"></fmt:formatNumber> </span><span class="font-gray9"> 元</span>
        </div>
    </div>
    <div class="blank-60"></div>
    <div>
        <a class="btn-orange btn-full JS-btnsubmit"  >立即追加</a>
    </div>
</div>

<script>
$(function(){
    $(".JS-btnsubmit").on("click",this,function(){
        //账户余额
        var money = ${userAccount.money};
        //最低追加金额
        var minAdditional =<fmt:formatNumber pattern="#0.00" type="number" value="${(agreement.borrowMoney+agreement.lockMoney)*0.01}"></fmt:formatNumber>;
        if(money<minAdditional){
            toast("还需"+(minAdditional-money)+"元,请先充值!");
            return false;
        }

        var inputMoney = +$(".input-additional").val();
        if(inputMoney<minAdditional){
            toast("最低追加金额"+minAdditional+"元");
            return false;
        }

        var additionalLockMoney = inputMoney;
        var data = {"agreementId":${agreement.agreementId},"additionalLockMoney":additionalLockMoney};
        $.post("/rest/agreement/additional",data,function(res){

            if(res.resultFlag){
                alert("追加成功");
                window.location.href= "/rest/wap/trade/tradeDetail/${agreement.showAgreementId}/";
            }else{
                alert(res.resultMsg);
                return false;
            }
        },"json");
    });
})
</script>

</body>
</html>
