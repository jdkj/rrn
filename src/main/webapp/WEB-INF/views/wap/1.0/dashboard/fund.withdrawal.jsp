<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/9/11
  Time: 14:16
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>提现</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/personCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray  ">
    <div class="contract-cont">
        <div class="his-codetitle clearfix">
            <div class="fl">当前余额</div>
            <div class="font-size14 fr">
                <span class="font-red"><span class="  ng-isolate-scope">${userAccount.money}</span></span> 元
            </div>
        </div>
    </div>
    <div class="common-list">
        <ul class="list-haveline clearfix">
            <li class="font-gray3">提现金额</li>
            <li>
                <input class="input-whit" type="number" id="withdrawalMoney" placeholder="请输入金额"
                       data-money-max="${userAccount.money}">元
            </li>
        </ul>
    </div>

    <div class="drawing-tips"><i class="icon-jinggao"></i>每日提款额度累计<em>超过10万元</em>，T+1到账</div>
    <div class="common-list no-borderbot">
        <ul class="list-haveline clearfix">
            <li class="font-gray3">选择提现银行</li>
            <li>
                <!-- <span class="font-blue">我的银行卡<i class="icon-more1"></i></span> -->
            </li>
        </ul>
    </div>
    <c:choose>
        <c:when test="${bankAccount!=null}">
            <div class="common-list mt0" style="border-bottom: 0">

                <ul class="list-haveline clearfix  ">
                    <a>
                        <li class="font-gray3">
                                ${bankAccount.bankName}

                        </li>
                        <li><span class="card-number  ">${bankAccount.modifiedCardNo}</span><i
                                class="font-dorange icon-yixuanze"></i>
                            <input type="hidden" id="bankAccountId" value="${bankAccount.bankAccountId}">
                        </li>
                    </a>
                </ul>
            </div>
        </c:when>
        <c:otherwise>
            <div class="add-card  " style=""><a href="/rest/wap/personalcenter//setting/userbank/"><i
                    class="icon-tianjia1"></i>&nbsp;添加银行卡</a></div>
        </c:otherwise>
    </c:choose>

    <!-- <div class="add-card  "  ="bankList &amp;&amp; bankList.length>0" style=""><a  ><i class="icon-tianjia1"></i>&nbsp;添加银行卡</a></div> -->
    <div class="drawing-tip">
        <h1>温馨提示</h1>
        <p>禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确认，将终止该账户的使用。 </p>
    </div>
    <div class="cancel-btn">
        <a class="btn-full btn-red" id="confirmBtn">立即提现</a>
    </div>
    <div class="popup-container ng-isolate-scope  " style="display:none;">
        <div class="popup">
            <div class="popup-head">提示</div>
            <div class="popup-body">
                <input class="popup-input      " placeholder="" type="">
            </div>
            <div class="forget clearfix  ">
                <a href="#" class="forget2" vbyzc-go="/personcenter/forgetDrawPassword.html">忘记提款密码？</a>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $("#confirmBtn").click(function () {
            var _this = $(this);
            _this.prop("disabled", true);
            if ($("#bankAccountId").length > 0) {
                var maxMoney = parseFloat(${userAccount.money});
                var applyMoney = +$("#withdrawalMoney").val();
                if (applyMoney > 0 && applyMoney <= maxMoney) {
                    var data = {"bankAccountId": $("#bankAccountId").val(), "money": applyMoney};
                    $.ajax({
                        type: "post",
                        data: data,
                        dataType: "json",
                        url: "/rest/fund/withdrawals/apply/"
                    }).done(function (res) {
                        alert(res.resultMsg);
                        if (res.resultFlag) {
                            window.location.href = "/rest/wap/personalcenter/"
                        }
                    }).always(function () {
                        _this.prop("disabled", false);
                    })


                } else {
                    _this.prop("disabled", false);
                    alert("请输入正确的提款金额");
                }

            } else {
                _this.prop("disabled", false);
                alert("请选绑定银行卡");
            }
        });
    });
</script>
</body>
</html>

