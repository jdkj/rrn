<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/17
  Time: 20:16
  Desc: 查询页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>操盘-持仓</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/tradeClient.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray ">
    <!-- 余额 -->
    <div class="contract-cont">
        <div class="his-codetitle clearfix">
            <div class="fl">现金余额</div>
            <div class="font-size14 fr font-red"><span class=" "> <fmt:formatNumber pattern="#0.00" type="number"
                                                                                    value="${agreement.availableCredit}"></fmt:formatNumber> </span>
                元
            </div>
        </div>
    </div>

    <!-- 数据 -->
    <div class="taking-module">
        <div class="taking-tab clearfix ">
            <a href="/rest/wap/trade/${agreement.showAgreementId}/position/?t=<%=mills%>" >持仓</a>
            <a href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/buy/?t=<%=mills%>">买入</a>
            <a href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/sell/?t=<%=mills%>">卖出</a>
            <a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/?t=<%=mills%>" >撤单</a>
            <a href="/rest/wap/trade/${agreement.showAgreementId}/query/?t=<%=mills%>" class="selected">查询</a>
        </div>
    </div>

    <!-- 菜单 -->
    <div class="common-list">
        <a href="/rest/wap/trade/${agreement.showAgreementId}/query/current/transaction/?t=<%=mills%>">
        <ul class="list-haveline clearfix" >
            <li class="font-gray3">当日成交</li>
            <li><i class="icon-more1 font-grayD"></i></li>
        </ul>
        </a>
        <a href="/rest/wap/trade/${agreement.showAgreementId}/query/current/commission/?t=<%=mills%>">
        <ul class="list-haveline clearfix">
            <li class="font-gray3">当日委托</li>
            <li><i class="icon-more1 font-grayD"></i></li>
        </ul>
        </a>
    </div>
    <div class="common-list">
        <a href="/rest/wap/trade/${agreement.showAgreementId}/query/history/transaction/?t=<%=mills%>">
        <ul class="list-haveline clearfix" >
            <li class="font-gray3">历史成交</li>
            <li><i class="icon-more1 font-grayD"></i></li>
        </ul>
        </a>
        <a href="/rest/wap/trade/${agreement.showAgreementId}/query/history/commission/?t=<%=mills%>">
        <ul class="list-haveline clearfix" >
            <li class="font-gray3">历史委托</li>
            <li><i class="icon-more1 font-grayD"></i></li>
        </ul>
        </a>
    </div>

</div>
</body>
</html>
