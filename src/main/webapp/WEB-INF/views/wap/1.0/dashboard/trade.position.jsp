<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/17
  Time: 19:05
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>操盘-持仓</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/tradeClient.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray ">
    <!-- 余额 -->
    <div class="contract-cont">
        <div class="his-codetitle clearfix">
            <div class="fl">现金余额</div>
            <div class="font-size14 fr font-red"><span class=" "> <fmt:formatNumber pattern="#0.00" type="number"
                                                                                    value="${agreement.availableCredit}"></fmt:formatNumber> </span>
                元
            </div>
        </div>
    </div>

    <!-- 数据 -->
    <div class="taking-module">
        <div class="taking-tab clearfix ">
            <a href="/rest/wap/trade/${agreement.showAgreementId}/position/?t=<%=mills%>" class="selected">持仓</a>
            <a href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/buy/?t=<%=mills%>">买入</a>
            <a href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/sell/?t=<%=mills%>">卖出</a>
            <a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/?t=<%=mills%>">撤单</a>
            <a href="/rest/wap/trade/${agreement.showAgreementId}/query/?t=<%=mills%>">查询</a>
        </div>
    </div>
    <c:choose>
        <c:when test="${pageResult!=null&&pageResult.result!=null&&fn:length(pageResult.result)>0}">
            <!-- 列表 -->
            <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr class="none-topline">
                    <th width="">名称/代码</th>
                    <th width="">市值/盈亏</th>
                    <th width="">持仓/可用</th>
                    <th width="">成本/现价</th>
                </tr>
                <c:forEach var="stockPosition" items="${pageResult.result}">
                    <tr class="" style="">
                        <td class="first-column "><span class="">${stockPosition.stockName}</span><span class="">${stockPosition.stockCode}</span></td>
                        <td>
                            <em class="<c:choose><c:when test="${stockPosition.currentMarketValue-stockPosition.buyTotalExpenses>=0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">
                                <span class=""><fmt:formatNumber pattern="#0.00" type="number" value="${stockPosition.currentMarketValue}"></fmt:formatNumber> </span>
                                <span style="display: inline;" class=""><fmt:formatNumber pattern="#0.00" type="number"
                                                                                          value="${stockPosition.currentMarketValue-stockPosition.buyTotalExpenses}"></fmt:formatNumber></span>(<span
                                    style="display: inline;" class=""><fmt:formatNumber pattern="#0.00" type="number"
                                                                                        value="${(stockPosition.currentMarketValue-stockPosition.buyTotalExpenses)*100/stockPosition.buyTotalExpenses}"></fmt:formatNumber></span>%)
                            </em>
                        </td>
                        <td><span class="">${stockPosition.stockNum}</span><span class="">${stockPosition.stockAvailableNum}</span></td>
                        <td><span class="font-gray3">${stockPosition.buyPrice}</span><span class="<c:choose><c:when test="${stockPosition.currentMarketValue-stockPosition.buyTotalExpenses>=0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">${stockPosition.currentPrice}</span></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="page-middel clearfix">
                <ul id="pagination-flowing" class="pagination-sm pagination">
                    <c:choose>
                        <c:when test="${pageResult.totalPages>1}">
                            <c:choose>
                                <c:when test="${pageResult.pageNo>1}">
                                    <li class="first"><a href="/rest/wap/trade/${agreement.showAgreementId}/position/1">首页</a></li>
                                    <li class="prev "><a href="/rest/wap/trade/${agreement.showAgreementId}/position/${pageResult.pageNo-1}">上一页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                    <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                <c:choose>
                                    <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                        <li class="page">
                                            <a href="/rest/wap/trade/${agreement.showAgreementId}/position/${pageIndex}">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                    <c:when test="${pageResult.pageNo==pageIndex}">
                                        <li class="page active">
                                            <a href="javascript:void(0)">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                    <li class="next "><a href="/rest/wap/trade/${agreement.showAgreementId}/position/${pageResult.pageNo+1}">下一页</a></li>
                                    <li class="last "><a href="/rest/wap/trade/${agreement.showAgreementId}/position/${pageResult.totalPages}">尾页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                    <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                            <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                            <li class="page active"><a href="javascript:void(0)">1</a></li>
                            <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                            <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                        </c:otherwise>
                    </c:choose>

                </ul>
            </div>
        </c:when>
        <c:otherwise>
            <!-- 列表 -->
            <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr class="none-topline">
                    <th width="25%">名称/代码</th>
                    <th width="28%">市值/盈亏</th>
                    <th width="23%">持仓/可用</th>
                    <th width="24%">成本/现价</th>
                </tr>

                </tbody>
            </table>
            <div class="mt_pic"></div>
        </c:otherwise>
    </c:choose>

</div>

</body>
</html>
