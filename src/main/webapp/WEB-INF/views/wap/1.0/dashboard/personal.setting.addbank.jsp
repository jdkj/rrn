<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 22:48
  Desc: 添加银行卡信息
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>用户中心-绑定银行卡</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/update.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self" >

</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div   class="wrap  "  >
    <div class="module-fullspace">
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-yonghu"  ></i> </span>
                <div class="line-down"><span class="font-red">${userIdAuth.idName}</span></div>
            </li>
        </ul>
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-yinxingqia"  ></i></span>
                <div class="line-down">
                   <select class="JSBankId">
                       <option value="0">请选择开户行</option>
                       <c:choose>
                           <c:when test="${bankList!=null&&fn:length(bankList)>0}">
                               <c:forEach var="bank" items="${bankList}">
                                   <option value="${bank.bankId}">${bank.bankName}</option>
                               </c:forEach>
                           </c:when>
                       </c:choose>
                   </select>
                </div>
            </li>
        </ul>
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-yinxingleizhifu"  ></i></span>
                <div class="line-down">
                    <input class="longbox" name="cardNo" type="number" placeholder="请输入银行卡卡号">
                </div>
            </li>
        </ul>
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-yinxingleizhifu"  ></i></span>
                <div class="line-down">
                    <input class="longbox" name="cardNo2" type="number" placeholder="请确认银行卡卡号">
                </div>
            </li>
        </ul>
    </div>
    <div class="module-white">
		<span class="pic-jinggao">
		  <i class="icon-jinggao"></i></span>
        <div class="tianxie font-gray9 font-size12">请使用[${userIdAuth.idName}]名下常用的银行卡，绑定成功后提现只能在该银行卡上进行！</div>
    </div>
    <div class="btn-full btn-orange JSaddBanksubmit">确认</div>
</div>
<script>
    $(function () {
        <c:choose>
        <c:when test="${!userInfo.userParamInfoTags.idTag}">
        alert("请先进行实名认证");
        window.location.href = "/rest/wap/personalcenter/setting/idauth/";
        </c:when>
        <c:when test="${userInfo.userParamInfoTags.bankTag}">
        alert("您已绑定了银行卡信息，无需重新绑定");
        window.location.href = "/rest/wap/personalcenter/setting/";
        </c:when>
        <c:otherwise>

        $(".JSaddBanksubmit").click(function(){
            if(validAddBankValue()){
                var item = $(this);
                item.prop("disabled",true);
                var bankId = +$(".JSBankId").val();
                var cardNo = $("input[name=cardNo]").val();
                var data = {"bankId":bankId,"cardNo":cardNo};
                $.post("/rest/user/addBank",data,function(res){
                    item.prop("disabled",false);
                    if(res.resultFlag){
                        alert("添加成功");
                        window.location.href="/rest/wap/personalcenter/setting/";
                    }else{
                        alert("添加失败");
                    }
                },"json");
            }
        });



        function validAddBankValue(){
            var bankId = +$(".JSBankId").val();
            var cardNo = $("input[name=cardNo]").val();
            var cardNo2 = $("input[name=cardNo2]").val();
            if(bankId===0){
                alert("请选择开户银行");
                return false;
            }
            if(cardNo===""){
                alert("请输入银行卡号");
                return false;
            }
            if(cardNo!=cardNo2){
                alert("两次卡号填写不一致");
                return false;
            }
            return true;
        }


        </c:otherwise>
        </c:choose>
    })
</script>

</body>
</html>
