<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/24
  Time: 15:43
  Desc: 历史交易详细信息
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>历史操盘 - 详细数据</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/applyTrade.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self" >

</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div   class="wrap bg-gray  "  >
    <!-- 主要内容 -->
    <div class="contract-cont">
        <div class="his-codetitle clearfix">
            <div class="fl">合约编号</div>
            <div class="font-size14 fr  ">${agreement.showAgreementId}</div>
        </div>
        <div class="detail-module his-detail-module clearfix">
            <ul>
                <li>总操盘资金
                    <br>
                    <span class="font-gray3   ng-isolate-scope"  ><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.borrowMoney+agreement.lockMoney}"></fmt:formatNumber> </span>
                </li>
                <li class="d-line"></li>
                <li>
                    申请额度
                    <br>
                    <span class="font-gray3   ng-isolate-scope"  ><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.borrowMoney}"></fmt:formatNumber></span>
                </li>
                <li class="d-line"></li>
                <li>
                    风险保证金
                    <br>
                    <span class="font-gray3   ng-isolate-scope"  ><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.lockMoney}"></fmt:formatNumber></span>
                </li>
                <li>收益分成
                    <br>
                    <span class="font-gray3   ng-isolate-scope"  >100.00</span>%
                </li>
                <li class="d-line"></li>
                <li>
                    亏损警戒线
                    <br>
                    <span class="font-gray3   ng-isolate-scope"  ><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.warningLineMoney}"></fmt:formatNumber></span>
                </li>
                <li class="d-line"></li>
                <li>
                    亏损平仓线
                    <br>
                    <span class="font-gray3   ng-isolate-scope"  ><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.closeLineMoney}"></fmt:formatNumber></span>
                </li>
            </ul>
        </div>
        <div class="title-graybg">交易盈亏分配</div>
        <div class="common-list mt0">
            <ul class="list-haveline clearfix">
                <li>盈利分配</li>
                <li>
                    <span class="font-red   ng-isolate-scope"  ><c:choose><c:when test="${agreement.diffMoney>0}"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney}"></fmt:formatNumber></c:when><c:otherwise>0.00</c:otherwise></c:choose></span> <span class="font-red">元</span></li>
            </ul>
            <ul class="list-haveline clearfix">
                <li>亏损赔付</li>
                <li>
                    <span class="font-green   ng-isolate-scope"  ><c:choose><c:when test="${agreement.diffMoney>0}">0.00</c:when><c:otherwise><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.diffMoney}"></fmt:formatNumber></c:otherwise></c:choose></span> <span class="font-green">元</span></li>
            </ul>
        </div>
        <div class="title-graybg">保证金结算</div>
        <div class="common-list mt0  no-botline">
            <ul class="list-haveline clearfix">
                <li>冻结</li>
                <li>
                    <span class="font-orange   ng-isolate-scope"  ><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.lockMoney}"></fmt:formatNumber></span> <span class="font-orange">元</span></li>
            </ul>
            <ul class="list-haveline clearfix">
                <li>扣减</li>
                <li>
                    <span class="font-green   ng-isolate-scope"  ><c:choose><c:when test="${agreement.diffMoney>0}">0.00</c:when><c:otherwise><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.diffMoney}"></fmt:formatNumber></c:otherwise></c:choose></span> <span class="font-green">元</span></li>
            </ul>
            <ul class="list-haveline clearfix">
                <li>解冻</li>
                <li>
                    <span class="font-gray3   ng-isolate-scope"  ><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.lockMoney+agreement.diffMoney}"></fmt:formatNumber> </span> <span class="font-gray3">元</span></li>
            </ul>
        </div>
    </div>

    <!-- 流水 -->
    <div class="title-graybg">交易流水</div>
<c:choose>
    <c:when test="${pageResult!=null&&pageResult.result!=null&&fn:length(pageResult.result)>0}">
    <div>
        <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
                <th width="25%">名称/代码</th>
                <th width="25%">价格/数量</th>
                <th width="25%">状态/类型</th>
                <th width="25%" class="t-right">成交时间</th>
            </tr>
        <c:forEach var="stockOperate" items="${pageResult.result}">
            <tr class="" style="">
                <td class="first-column "><span class="">${stockOperate.stockName}</span><span class="">${stockOperate.stockCode}</span></td>
                <td>
                    <span class=""><fmt:formatNumber pattern="#0.00" type="number" value="${stockOperate.transactionPrice}"></fmt:formatNumber> </span>
                    <span  class="">${stockOperate.transactionNum}</span>
                </td>
                <td><span class="">${stockOperate.operateStatusDesc}</span><span class="<c:choose><c:when test="${stockOperate.operateType==1}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">${stockOperate.operateTypeDesc}</span></td>
                <td><span class="font-gray3"><fmt:parseDate pattern="yyyy-MM-dd" value="${stockOperate.updateTime}" var="yDate"/> <fmt:formatDate pattern="yyyy-MM-dd" value="${yDate}"></fmt:formatDate></span><span class=""><fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${stockOperate.updateTime}" var="tDate"/><fmt:formatDate pattern="HH:mm:ss" value="${tDate}"></fmt:formatDate></span></td>
            </tr>
        </c:forEach>
            </tbody>
        </table>
        <div class="page-middel clearfix">
        <ul id="pagination-flowing" class="pagination-sm pagination">
        <c:choose>
            <c:when test="${pageResult.totalPages>1}">
                <c:choose>
                    <c:when test="${pageResult.pageNo>1}">
                        <li class="first"><a href="/rest/wap/trade/tradeHistory/detail/${agreement.showAgreementId}/1">首页</a></li>
                        <li class="prev "><a href="/rest/wap/trade/tradeHistory/detail/${agreement.showAgreementId}/${pageResult.pageNo-1}">上一页</a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                        <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                    </c:otherwise>
                </c:choose>
                <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                    <c:choose>
                        <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                            <li class="page">
                                <a href="/rest/wap/trade/tradeHistory/detail/${agreement.showAgreementId}/${pageIndex}">${pageIndex}</a>
                            </li>
                        </c:when>
                        <c:when test="${pageResult.pageNo==pageIndex}">
                            <li class="page active">
                                <a href="javascript:void(0)">${pageIndex}</a>
                            </li>
                        </c:when>
                    </c:choose>
                </c:forEach>
                <c:choose>
                    <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                        <li class="next "><a href="/rest/wap/trade/tradeHistory/detail/${agreement.showAgreementId}/${pageResult.pageNo+1}">下一页</a></li>
                        <li class="last "><a href="/rest/wap/trade/tradeHistory/detail/${agreement.showAgreementId}/${pageResult.totalPages}">尾页</a></li>
                    </c:when>
                    <c:otherwise>
                        <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                        <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                <li class="page active"><a href="javascript:void(0)">1</a></li>
                <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
            </c:otherwise>
        </c:choose>

        </ul>
        </div>
        </c:when>
        <c:otherwise>
            <div class="deal-module">
                <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
                    <tbody><tr class="none-topline">
                        <th width="25%">名称/代码</th>
                        <th width="25%">价格/数量</th>
                        <th width="25%">状态/类型</th>
                        <th width="25%" class="t-right">成交时间</th>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="mt_pic"></div>
        </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>
