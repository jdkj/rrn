<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/17
  Time: 19:23
  Desc: 撤单页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>操盘-持仓</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/tradeClient.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray ">
    <!-- 余额 -->
    <div class="contract-cont">
        <div class="his-codetitle clearfix">
            <div class="fl">现金余额</div>
            <div class="font-size14 fr font-red"><span class=" "> <fmt:formatNumber pattern="#0.00" type="number"
                                                                                    value="${agreement.availableCredit}"></fmt:formatNumber> </span>
                元
            </div>
        </div>
    </div>

    <!-- 数据 -->
    <div class="taking-module">
        <div class="taking-tab clearfix ">
            <a href="/rest/wap/trade/${agreement.showAgreementId}/position/?t=<%=mills%>" >持仓</a>
            <a href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/buy/?t=<%=mills%>">买入</a>
            <a href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/sell/?t=<%=mills%>">卖出</a>
            <a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/?t=<%=mills%>" class="selected">撤单</a>
            <a href="/rest/wap/trade/${agreement.showAgreementId}/query/?t=<%=mills%>">查询</a>
        </div>
    </div>
    <c:choose>
        <c:when test="${pageResult!=null&&pageResult.result!=null&&fn:length(pageResult.result)>0}">
            <!-- 列表 -->
            <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr class="none-topline">
                    <th>&nbsp;</th>
                    <th width="25%">名称/代码</th>
                    <th width="28%">价格/数量</th>
                    <th width="23%">状态/类型</th>
                    <th width="24%" class="t-right">委托时间</th>
                </tr>
                <c:forEach var="stockOperate" items="${pageResult.result}">
                    <tr class="" style="">
                        <td><c:choose><c:when test="${stockOperate.operateStatus==3&&agreement.forceClose==0}"><input
                                type="radio"
                                value="${stockOperate.operateId}"/></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose>
                        </td>
                        <td class="first-column "><span class="">${stockOperate.stockName}</span><span class="">${stockOperate.stockCode}</span></td>
                        <td>

                                <span class=""><fmt:formatNumber pattern="#0.00" type="number" value="${stockOperate.commissionPrice}"></fmt:formatNumber> </span>
                                <span  class="">${stockOperate.commissionNum}</span>

                        </td>
                        <td><span class="">${stockOperate.operateStatusDesc}</span><span class="">${stockOperate.operateTypeDesc}</span></td>
                        <td><span class="font-gray3"><fmt:parseDate pattern="yyyy-MM-dd" value="${stockOperate.createTime}" var="yDate"/> <fmt:formatDate pattern="yyyy-MM-dd" value="${yDate}"></fmt:formatDate></span><span class=""><fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${stockOperate.createTime}" var="tDate"/> <fmt:formatDate pattern="HH:mm:ss" value="${tDate}"></fmt:formatDate></span></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div class="page-middel clearfix">
                <ul id="pagination-flowing" class="pagination-sm pagination">
                    <c:choose>
                        <c:when test="${pageResult.totalPages>1}">
                            <c:choose>
                                <c:when test="${pageResult.pageNo>1}">
                                    <li class="first"><a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/1">首页</a></li>
                                    <li class="prev "><a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/${pageResult.pageNo-1}">上一页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                    <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                <c:choose>
                                    <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                        <li class="page">
                                            <a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/${pageIndex}">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                    <c:when test="${pageResult.pageNo==pageIndex}">
                                        <li class="page active">
                                            <a href="javascript:void(0)">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                    <li class="next "><a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/${pageResult.pageNo+1}">下一页</a></li>
                                    <li class="last "><a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/${pageResult.totalPages}">尾页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                    <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                            <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                            <li class="page active"><a href="javascript:void(0)">1</a></li>
                            <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                            <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                        </c:otherwise>
                    </c:choose>

                </ul>
            </div>
            <div style="position:fixed;bottom:25px;left:0px;right:0px;"><input id="JS-submit-cancel" class="btn-grayborder" type="button" value="撤单"></div>
        </c:when>
        <c:otherwise>
            <!-- 列表 -->
            <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
                <tbody>
                <tr class="none-topline">
                    <th>&nbsp;</th>
                    <th width="25%">名称/代码</th>
                    <th width="28%">价格/数量</th>
                    <th width="23%">状态/类型</th>
                    <th width="24%" class="t-right">委托时间</th>
                </tr>

                </tbody>
            </table>
            <div class="mt_pic"></div>
        </c:otherwise>
    </c:choose>

</div>
<script>
    $(function(){

        $("input[type='radio']").each(function(){
            var _this = $(this);
            var item = $(this).parent().parent();

            item.on("click",this,function(){
                var flag = !_this.prop("checked");
                $("input[type='radio']").prop("checked",false);
                _this.prop("checked",flag);
            })
        });

       $("#JS-submit-cancel").on("click",this,function(){
           var item = $("input[type='radio']:checked");
           if(item.length>0){
               var id = item.eq(0).val();
               // 请求
               $.ajax({
                   url: '/rest/trade/operate/cancel',
                   type: 'post',
                   data: {"operateId": id},
               }).done(function(res) {
                   if(res.resultFlag){
                       window.location.href = window.location.href;
                   }else{
                       toast(res.resultMsg);
                       return false;
                   }
               }).fail(function(textStatus) {
                   toast("请求出错，请稍后重试!");
               })
           }else{
               toast("请先选择要撤单的股票");
           }
       }) ;
    });
</script>
</body>
</html>
