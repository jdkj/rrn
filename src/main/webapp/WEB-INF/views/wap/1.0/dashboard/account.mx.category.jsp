<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 17:13
  Desc: 资金明细---列表
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>资金明细</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/personCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self" >
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div   class="wrap bg-gray  "  >
    <!-- 标签 -->
    <nav class="head-subnav fund-nav">
        <div>
            <a  href="/rest/wap/personalcenter/flowing/?_t=<%=mills%>" class="" style="">全部</a>
            <a  href="/rest//wap/personalcenter/flowing/c0/?_t=<%=mills%>"  class="<c:choose><c:when test="${changeType==0}">selected</c:when></c:choose>" style="">充值提款</a>
            <a  href="/rest//wap/personalcenter/flowing/c1/?_t=<%=mills%>"  class="<c:choose><c:when test="${changeType==1}">selected</c:when></c:choose>" style="">借款明细</a>
            <a  href="/rest//wap/personalcenter/flowing/c2/?_t=<%=mills%>"  class="<c:choose><c:when test="${changeType==2}">selected</c:when></c:choose>" style="">服务费明细</a>
            <a  href="/rest//wap/personalcenter/flowing/c3/?_t=<%=mills%>"  class="<c:choose><c:when test="${changeType==3}">selected</c:when></c:choose>" style="">理财明细</a>
            <a  href="/rest//wap/personalcenter/flowing/c4/?_t=<%=mills%>"  class="<c:choose><c:when test="${changeType==4}">selected</c:when></c:choose>" style="">利润提取</a>

        </div>
    </nav>
    <!-- 内容 -->
    <div id="loadmore"  class="ng-isolate-scope">
        <c:choose>
            <c:when test="${pageResult!=null&&pageResult.result!=null&&fn:length(pageResult.result)>0}">
                <c:forEach var="accountMx" items="${pageResult.result}">
                    <div class="detail-list clearfix  "  style="">
                        <ul class="clearfix">
                            <li  class=" ">${accountMx.changeDesc}</li>
                            <c:choose>
                            <c:when test="${accountMx.orderType==1||accountMx.orderType==2||accountMx.orderType==4||accountMx.orderType==5}">
                            <li class="font-green  ">-
                                </c:when>
                                <c:otherwise>
                            <li class="font-red  ">
                                </c:otherwise>
                                </c:choose>

                                <span   class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number" value="${accountMx.changeMoney}"></fmt:formatNumber> </span>
                            </li>
                        </ul>
                        <ul>
                            <li class="font-gray3">余额：
                                <span   class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number" value="${accountMx.totalMoney}"></fmt:formatNumber> </span>元</li>
                            <li class="font-gray9  ">${accountMx.updateTime}</li>
                        </ul>
                    </div>
                </c:forEach>
                <div class="page-middel clearfix">
                    <ul id="pagination-flowing" class="pagination-sm pagination">
                        <c:choose>
                            <c:when test="${pageResult.totalPages>1}">
                                <c:choose>
                                    <c:when test="${pageResult.pageNo>1}">
                                        <li class="first"><a href="/rest/wap/personalcenter/flowing/c${changeType}/1">首页</a></li>
                                        <li class="prev "><a href="/rest/wap/personalcenter/flowing/c${changeType}/${pageResult.pageNo-1}">上一页</a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                        <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                    </c:otherwise>
                                </c:choose>
                                <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                    <c:choose>
                                        <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                            <li class="page">
                                                <a href="/rest/wap/personalcenter/flowing/c${changeType}/${pageIndex}">${pageIndex}</a>
                                            </li>
                                        </c:when>
                                        <c:when test="${pageResult.pageNo==pageIndex}">
                                            <li class="page active">
                                                <a href="javascript:void(0)">${pageIndex}</a>
                                            </li>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                                <c:choose>
                                    <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                        <li class="next "><a href="/rest/wap/personalcenter/flowing/c${changeType}/${pageResult.pageNo+1}">下一页</a>
                                        </li>
                                        <li class="last "><a href="/rest/wap/personalcenter/flowing/c${changeType}/${pageResult.totalPages}">尾页</a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                        <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                <li class="page active"><a href="javascript:void(0)">1</a></li>
                                <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </c:when>
            <c:otherwise>
              <div class="mt_pic"></div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>
