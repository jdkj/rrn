<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/15
  Time: 11:17
  Desc: 我的操盘
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>我的操盘</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/images/favicon.ico"
          type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css?20171101">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/applyTrade.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">

</head>
<body>
<%--<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>--%>
<div class="wrap bg-gray  ">
    <!-- 菜单 -->
    <nav class="head-nav clearfix">
        <a class="width50   " href="/rest/wap/trade/type/0/?_t=<%=mills%>"><i class="icon-trade"></i><br>我要操盘</a>
        <a class="width50 select" href="/rest/wap/trade/?_t=<%=mills%>"><i class="icon-mytrade-selected"></i><br>我的操盘</a>
    </nav>


    <c:choose>
        <c:when test="${pageResult!=null&&pageResult.result!=null&&fn:length(pageResult.result)>0}">
            <div class="title-graybg">当前操盘</div>
            <c:forEach var="agreement" items="${pageResult.result}">
                <c:choose>
                    <c:when test="${agreement.status<2}">
                        <!-- 中期投顾 -->
                        <a href="/rest/wap/trade/tradeDetail/${agreement.showAgreementId}?_t=<%=mills%>">
                        <div class="contract-list "  style="">
                            <div class="mytrade-title  ">${agreement.signAgreementTypeDesc}</div>
                            <div class="mytrade-data  ">
                                <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate>
                            </div>
                            <div class="mytrade-cont">
                                <label class="<c:choose><c:when test="${agreement.diffMoney>=0}">current-data-rise</c:when><c:otherwise>current-data-fall</c:otherwise></c:choose>">
                            <span>当前额度<span class="  ng-isolate-scope">:<fmt:formatNumber pattern="#0.00"
                                                                                          value="${agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney}"
                                                                                          type="number"></fmt:formatNumber></span></span>
                                    <div class="current-quota">
                            <span class="fl">
                                <span class=""><c:choose><c:when
                                        test="${agreement.diffMoney>=0}">+</c:when><c:otherwise></c:otherwise></c:choose><fmt:formatNumber
                                        pattern="#0.00" type="number"
                                        value="${agreement.diffMoney}"></fmt:formatNumber></span>
                            </span>
                                        <span class="fr">
                                            <span class=""><c:choose><c:when test="${agreement.diffMoney>=0}">+</c:when><c:otherwise></c:otherwise></c:choose><fmt:formatNumber
                                                    type="number"
                                                    value="${agreement.diffMoney*100/(agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney)}"
                                                    pattern="#0.00"/></span>%<span class=""><c:choose><c:when test="${agreement.diffMoney>=0}">↑</c:when><c:otherwise>↓</c:otherwise></c:choose></span>
                                        </span>
                                    </div>
                                </label>
                                <label class="early-warn">亏损警戒线
                                    <br>
                                    <span class="font-orange fb"><span class="  ng-isolate-scope"><fmt:formatNumber
                                            type="number"
                                            value="${agreement.warningLineMoney}"
                                            pattern="#0.00"/></span></span></label>
                                <label class="stop-soss">亏损平仓线
                                    <br>
                                    <span class="font-red fb"><span class="  ng-isolate-scope"><fmt:formatNumber type="number"
                                                                                                                 value="${agreement.closeLineMoney}"
                                                                                                                 pattern="#0.00"/></span></span></label>
                                <label class="blocked-fund">风险保证金
                                    <br>
                                    <span class="font-blue fb"><span class="  ng-isolate-scope"><fmt:formatNumber type="number"
                                                                                                                  pattern="#0.00"
                                                                                                                  value="${agreement.lockMoney}"></fmt:formatNumber></span></span></label>
                            </div>
                        </div>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a href="/rest/wap/trade/tradeHistory/${agreement.showAgreementId}?_t=<%=mills%>">
                        <div class="history-list  <c:choose><c:when test="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney>0}">history-rise</c:when><c:otherwise>history-fall</c:otherwise></c:choose>  " style="">
                            <div class="mytrade-title  ">${agreement.signAgreementTypeDesc}</div>
                            <div class="mytrade-data  ">    <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate>
                            </div>
                            <ul class="clearfix">
                                <li>总操盘资金：
                                    <span class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number" value="${agreement.borrowMoney+agreement.lockMoney}"></fmt:formatNumber> </span>
                                </li>
                                <li>风险保证金：
                                    <span class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number" value="${agreement.lockMoney}"></fmt:formatNumber> </span>
                                </li>
                                <li class="<c:choose><c:when test="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney>0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">
                                    <span class="font-gray3">累计盈亏：</span>
                                    <span class="<c:choose><c:when test="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney>0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>"><c:choose><c:when test="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney>0}">+</c:when><c:otherwise></c:otherwise></c:choose></span>
                                    <span class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number" value="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney}"></fmt:formatNumber> </span>（<span class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number" value="${(agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney)*100/agreement.lockMoney}"></fmt:formatNumber></span>%）
                                </li>
                            </ul>
                        </div>
                        </a>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <!-- 翻页 -->
            <div class="page-middel clearfix">
                <ul id="pagination-flowing" class="pagination-sm pagination">
                    <c:choose>
                        <c:when test="${pageResult.totalPages>1}">
                            <c:choose>
                                <c:when test="${pageResult.pageNo>1}">
                                    <li class="first"><a href="/rest/wap/trade/1">首页</a></li>
                                    <li class="prev "><a href="/rest/wap/trade/${pageResult.pageNo-1}">上一页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                    <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                <c:choose>
                                    <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                        <li class="page">
                                            <a href="/rest/wap/trade/${pageIndex}">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                    <c:when test="${pageResult.pageNo==pageIndex}">
                                        <li class="page active">
                                            <a href="javascript:void(0)">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                    <li class="next "><a href="/rest/wap/trade/${pageResult.pageNo+1}">下一页</a></li>
                                    <li class="last "><a href="/rest/wap/trade/${pageResult.totalPages}">尾页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                    <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                            <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                            <li class="page active"><a href="javascript:void(0)">1</a></li>
                            <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                            <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                        </c:otherwise>
                    </c:choose>

                </ul>
            </div>
        </c:when>
        <c:otherwise>
            <div class="mt_pic"></div>
        </c:otherwise>
    </c:choose>


    <div class="footnavwrap">
        <nav class="bottom-nav-w clearfix">
            <a class="cell" href="/rest/wap/page/index?_t=<%=mills%>"> <i class="icon-financ font-size24"></i>
                <br>活动</a>
            <a class="cell active"> <i class="icon-caopan font-size24"></i> <br>操盘</a>
            <a class="cell" href="/rest/wap/personalcenter/?_t=<%=mills%>"><i
                    class="icon-wo1 font-size24"></i><br>我的</a>
        </nav>
    </div>
</div>
</body>
</html>
