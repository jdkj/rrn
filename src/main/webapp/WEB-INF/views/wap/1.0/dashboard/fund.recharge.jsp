<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/17
  Time: 23:26
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>充值</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/personCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray">
    <form action="/rest//wap/fund/recharge/confirm/" method="post">
        <div class="contract-cont">
            <div class="his-codetitle clearfix">
                <div class="fl">当前余额</div>
                <div class="font-size14 fr"><span class="font-red"><span class=""><fmt:formatNumber pattern="#0.00"
                                                                                                    type="number"
                                                                                                    value="${userAccount.money}"></fmt:formatNumber> </span></span>
                    元
                </div>
            </div>
        </div>
        <div class="common-list">
            <ul class="list-haveline clearfix">
                <a>
                    <li class="font-gray3">充值金额</li>
                    <li><input class="input-white" name="money" type="number" placeholder="请输入金额">元</li>
                </a>
            </ul>
        </div>
        <div class="recharge-tip">
            <h1>温馨提示</h1>
            <p>1、为了您的资金安全，您的账户资金将有第三方银行托管； </p>
            <p>2、充值前请注意您的银行卡充值限额。以免造成不便； </p>
            <p>3、禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确认，将终止该账户的使用； </p>
            <p>4、为了您的资金安全，建议充值前进行实名认证、手机绑定、设置提现密码； </p>
            <p>5、如果充值遇到任何问题，请联系客服：0592-5917777</p>
        </div>
        <div class="cancel-btn">
            <a class="btn-full btn-red JS-submitbtn">立即充值</a>
            <input type="hidden" name="payChannel" value="0"/>
        </div>
    </form>
</div>

<script>
    $(function(){
       $(".JS-submitbtn").on("click",this,function(){
            if(+$("input[name='money']").val()>0){
                $("form").submit();
            }else{
                toast("请输入充值金额");
            }
       });
    });
</script>
</body>
</html>
