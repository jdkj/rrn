<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/16
  Time: 16:37
  Desc: 合约详细
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>操盘详细</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css?20171101">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/applyTrade.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/tradeDetail.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp"></jsp:include>
<div class="wrap bg-gray">
    <!-- 详细数据 -->
    <div class="contract-cont">
        <div class="detail-title clearfix">
            <div class="mytrade-title fl">合约编号</div>
            <div class="mytrade-title fr">${agreement.showAgreementId}</div>
        </div>
        <div class="detail-module clearfix">
            <div class="mytrade-title fl">${agreement.signAgreementTypeDesc}</div>
            <div class="mytrade-data fr">
                <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/> <fmt:formatDate
                    pattern="yyyy-MM-dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate pattern="yyyy-MM-dd"
                                                                                             value="${agreement.operateDeadline}"
                                                                                             var="eDate"/>
                <fmt:formatDate pattern="yyyy-MM-dd" value="${eDate}"></fmt:formatDate>
            </div>
        </div>
        <div class="detail-module clearfix">
            合约总资产（元）
            <div class="total-assets"> <span class=""><fmt:formatNumber pattern="#0.00"
                                                                        value="${agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney}"
                                                                        type="number"></fmt:formatNumber></span></div>
            <ul>
                <li>持仓市值（元） <br> <span class="font-gray3"><fmt:formatNumber pattern="#0.00"
                                                                            value="${agreement.stockCurrentPrice}"
                                                                            type="number"></fmt:formatNumber></span>
                </li>
                <li class="d-line"></li>
                <li> 持仓比例 <br> <span class="font-gray3"><fmt:formatNumber pattern="#0.00"
                                                                          value="${agreement.stockCurrentPrice*100/(agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney)}"
                                                                          type="number"></fmt:formatNumber></span>%
                </li>
                <li class="d-line"></li>
                <li> 可用额度（元） <br> <span class="font-gray3"><fmt:formatNumber pattern="#0.00"
                                                                             value="${agreement.availableCredit}"
                                                                             type="number"></fmt:formatNumber></span>
                </li>
            </ul>
        </div>
        <div class="trade-noborder">
            当前盈亏（元）
            <br>
            <span class="<c:choose><c:when test="${agreement.diffMoney>=0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">
                <fmt:formatNumber pattern="#0.00" value="${agreement.diffMoney}"
                                  type="number"></fmt:formatNumber></span>
            <span class="<c:choose><c:when test="${agreement.diffMoney>=0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>"><fmt:formatNumber
                    pattern="#0.00"
                    value="${agreement.diffMoney*100/(agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney)}"
                    type="number"></fmt:formatNumber></span>
            <span class="<c:choose><c:when test="${agreement.diffMoney>=0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">%</span>
        </div>
        <div class="warn-line">
            <img height="32px" width="auto" src="<%=basePath%>/rrn/wap.1.0/images/icon_niu_smile.fw.png" style="
            <c:choose>
            <c:when test="${(agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney)<agreement.warningLineMoney}">left:50%</c:when>
                <c:when test="${(agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney)<agreement.closeLineMoney}">left:15%</c:when></c:choose>">
        </div>
        <!-- 隐藏显示合约 -->
        <div class="detail-module no-botline clearfix">
            <div class="contract-no">合约编号：<span class="gray3 font-size14">${agreement.showAgreementId}</span></div>
            <ul>
                <li>总操盘资金 <br> <span class="font-gray3"><fmt:formatNumber pattern="#0.00"
                                                                          value="${agreement.borrowMoney+agreement.lockMoney}"
                                                                          type="number"></fmt:formatNumber></span></li>
                <li class="d-line"></li>
                <li> 申请额度 <br> <span class="font-gray3"><fmt:formatNumber pattern="#0.00"
                                                                          value="${agreement.borrowMoney}"
                                                                          type="number"></fmt:formatNumber></span></li>
                <li class="d-line"></li>
                <li> 风险保证金 <br> <span class="font-gray3"><fmt:formatNumber pattern="#0.00"
                                                                           value="${agreement.lockMoney}"
                                                                           type="number"></fmt:formatNumber></span></li>
                <li>收益分成 <br> <span class="font-gray3">100.00</span>%</li>
                <li class="d-line"></li>
                <li> 亏损警戒线 <br> <span class="font-gray3"><fmt:formatNumber pattern="#0.00"
                                                                           value="${agreement.warningLineMoney}"
                                                                           type="number"></fmt:formatNumber></span></li>
                <li class="d-line"></li>
                <li> 亏损平仓线 <br> <span class="font-gray3"><fmt:formatNumber pattern="#0.00"
                                                                           value="${agreement.closeLineMoney}"
                                                                           type="number"></fmt:formatNumber></span></li>
            </ul>
        </div>
    </div>

    <!-- 信息2 -->
    <div class="common-list" style="margin-bottom: 58px;">
        <ul class="list-haveline clearfix">
            <li>账户管理费</li>
            <li class="" style=""><span class=""><fmt:formatNumber pattern="#0.00"
                                                                   value="${agreement.accountManagementFee}"
                                                                   type="number"></fmt:formatNumber></span>元 /
                <c:choose><c:when
                        test="${agreement.signAgreementType==0}">日</c:when><c:otherwise>月</c:otherwise></c:choose></li>
        </ul>
        <ul class="list-haveline clearfix">
            <li>已操盘天数</li>
            <li><span class="">${agreement.continueOperateDay}</span> 个交易日</li>
        </ul>
    </div>

    <!-- 底部命令 -->
    <div class="bottom-operate">
        <a class="btn-grayborder JS-moreMenu"><i class="icon-caidan"></i>更多操作</a>
        <a class="btn-grayborder JS-settlement">申请结算</a>
        <a class="btn-orange btn-full"
           href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/buy/?_t=<%=mills%>">交易委托</a>
    </div>

    <!-- 隐藏菜单 -->
    <div class="moreOper-wrap" style="display:none;">
        <a href="/rest/wap/trade/${agreement.showAgreementId}/additional/?_t=<%=mills%>">
            <div class="moreOper-each" style="">追加保证金</div>
        </a>
        <!-- 可用额度<0 不显示 -->
        <%--<div class="moreOper-each"vbyzc-go="fetchprofit.html"style="">利润提取</div>--%>
    </div>

    <!-- 结算对话框 -->
    <div class="popup-container popup-jieshuan" style="display:none;">
        <div class="popup">
            <div class="popup-head">提示</div>
            <div class="popup-body">确认结算当前合约？</div>
            <div class="popup-buttons">
                <button class="JS-popup-cancle">取消</button>
                <button class="JS-action-js">确定</button>
            </div>
        </div>
    </div>

</div>

<script>
    jQuery(document).ready(function ($) {
        /////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////

        //结算过程
        ;
        function settlementSubmit() {
            var data = {"agreementId":${agreement.agreementId}};
            $.post("/rest/agreement/settlement", data, function (res) {

                if (res.resultFlag) {
                    alert("结算成功");
                    window.location.href = "/rest/wap/trade/";
                } else {
                    alert(res.resultMsg);
                    return false;
                }

            }, "json");
        }


        /////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////

        // 小菜单的隐藏显示
        $(document).on('click', '.JS-moreMenu', function (event) {
            $('.moreOper-wrap').is(':visible') && $('.moreOper-wrap').hide() || $('.moreOper-wrap').show();
        });

        // 结算对话框
        $(document).on('click', '.JS-settlement', function (event) {
            $('.popup-jieshuan').show();
        });
        // 结算确定
        $(document).on('click', '.JS-action-js', function (event) {
            settlementSubmit();
        });

    });
</script>
</body>
</html>
