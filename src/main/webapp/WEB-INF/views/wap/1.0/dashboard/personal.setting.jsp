<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 20:19
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>个人设置中心</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/personCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray  ">
    <div class="common-list">
        <ul class="list-haveline clearfix">
            <a>
                <li class="font-gray3">
                    <i class="user-icon icon-wo"></i>昵称
                </li>
                <li class=" ">${userInfo.nickName}<i class="font-white icon-more1"></i></li>
            </a>
        </ul>

        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.idTag}">
                        <ul class="list-haveline clearfix">
                            <li class="font-gray3">
                                <i class="user-icon icon-shimingrenzheng"></i>实名认证
                            </li>
                            <li>
                                <span class="" style="">${userIdAuth.idName}</span>
                                <i class="icon-more1 font-white" style=""></i></li>
                        </ul>
                    </c:when>
                    <c:otherwise>
                        <a href="/rest/wap/personalcenter/setting/idauth/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-shimingrenzheng"></i>实名认证
                                </li>
                                <li><span class="font-red  " style="">未认证</span>
                                    <i class="icon-more1 font-grayD" style=""></i></li>
                            </ul>
                        </a>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <a href="javascript:showDialog('为了保障您的权益，请先绑定手机号!','确认',function(){window.location.href='/rest/wap/personalcenter/setting/phone/?_t='+new Date().getTime();},'取消')">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-shimingrenzheng"></i>实名认证
                        </li>
                        <li><span class="font-red  " style="">未认证</span>
                            <i class="icon-more1 font-grayD" style=""></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>


        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.bankTag}">
                        <a href="/rest/wap/personalcenter/setting/userbank/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-yinxingleizhifu"></i>银行卡绑定
                                </li>
                                <li>
                                    <span class="" style="">已绑定</span>
                                    <i class="font-grayD icon-more1"></i>
                                </li>
                            </ul>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${userInfo.userParamInfoTags.idTag}">
                                <a href="/rest/wap/personalcenter/setting/addbank/">
                                    <ul class="list-haveline clearfix">
                                        <li class="font-gray3">
                                            <i class="user-icon icon-yinxingleizhifu"></i>银行卡绑定
                                        </li>
                                        <li><span class="font-red  " style="">未绑定</span>
                                            <i class="font-grayD icon-more1"></i></li>
                                    </ul>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:showDialog('请先进行身份认证','确认',function(){window.location.href='/rest/wap/personalcenter/setting/idauth/?_t='+new Date().getTime();},'取消')">
                                    <ul class="list-haveline clearfix">
                                        <li class="font-gray3">
                                            <i class="user-icon icon-yinxingleizhifu"></i>银行卡绑定
                                        </li>
                                        <li><span class="font-red  " style="">未绑定</span>
                                            <i class="font-grayD icon-more1"></i></li>
                                    </ul>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <a href="javascript:showDialog('为了保障您的权益，请先绑定手机号!','确认',function(){window.location.href='/rest/wap/personalcenter/setting/phone/?_t='+new Date().getTime();},'取消')">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-yinxingleizhifu"></i>银行卡绑定
                        </li>
                        <li><span class="font-red  " style="">未绑定</span>
                            <i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>


        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <a href="/rest/wap/personalcenter/setting/phone/modify/" style="display:block">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-shouji1"></i>手机绑定
                        </li>
                        <li class=" ">${userInfo.phone}<i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:when>
            <c:otherwise>
                <a href="/rest/wap/personalcenter/setting/phone/" style="display:block">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-shouji1"></i>手机绑定
                        </li>
                        <li class=" "><span class="font-red  " style="">未绑定</span><i class="font-grayD icon-more1"></i>
                        </li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>

    </div>
    <div class="common-list">

        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.passwordTag}">
                        <a href="/rest/wap/personalcenter/setting/password/modify/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-mima1"></i>登录密码
                                </li>
                                <li>已设置<i class="font-grayD icon-more1"></i></li>
                            </ul>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a href="/rest/wap/personalcenter/setting/password/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-mima1"></i>登录密码
                                </li>
                                <li><span class="font-red  " style="">未设置</span><i class="font-grayD icon-more1"></i>
                                </li>
                            </ul>
                        </a>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <a href="javascript:toast('为了保障您的权益，请先绑定手机号!')">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-mima1"></i>登录密码
                        </li>
                        <li><span class="font-red  " style="">未设置</span><i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>


        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.accountPasswordTag}">
                        <a href="/rest/wap/personalcenter/setting/accountpassword/modify/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-zhifumima"></i>提款密码
                                </li>
                                <li>
                                    <span class="" style="">已设置</span><i class="font-grayD icon-more1"></i></li>
                            </ul>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a href="/rest/wap/personalcenter/setting/accountpassword/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-zhifumima"></i>提款密码
                                </li>
                                <li><span class="font-red  " style="">未设置</span><i class="font-grayD icon-more1"></i>
                                </li>
                            </ul>
                        </a>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <a href="javascript:showDialog('为了保障您的权益，请先绑定手机号!','确认',function(){window.location.href='/rest/wap/personalcenter/setting/phone/?_t='+new Date().getTime();},'取消')">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-zhifumima"></i>提款密码
                        </li>
                        <li><span class="font-red  " style="">未设置</span><i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>


    </div>


    <div class="common-list">
        <c:choose>
            <c:when test="${distributionUser!=null&&distributionUser.accountLevel>-1}">
                <a href="/rest/wap/page/share/">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3"><i class="user-icon icon-xinxi1 font-size18"></i>赚外快</li>
                        <li><i class="icon-more1 font-grayD"></i></li>
                    </ul>
                </a>
            </c:when>
        </c:choose>

        <a href="/rest/wap/page/about/?_t=<%=mills%>">
            <ul class="list-haveline clearfix">
                <li class="font-gray3"><i class="user-icon icon-xinxi font-size18"></i>关于人人牛</li>
                <li><i class="icon-more1 font-grayD"></i></li>
            </ul>
        </a>
    </div>

    <%--<div class="save-btn">--%>
    <%--<a class="btn-full btn-orange" href="/rest/user/logout" >安全退出</a>--%>
    <%--</div>--%>

</div>
</body>
</html>
