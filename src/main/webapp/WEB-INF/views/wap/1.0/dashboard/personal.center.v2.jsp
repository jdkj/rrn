<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/1/23
  Time: 16:03
  Desc: 新版用户中心
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>用户中心</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="/rrn/wap/2.0/css/applyTrade.css">
    <link rel="stylesheet" href="/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="/rrn/wap/2.0/css/main.css">
    <link rel="stylesheet" href="/rrn/wap/2.0/css/main9n.css">
    <link rel="stylesheet" href="/rrn/wap/2.0/css/personCenter.css">
    <script src="/rrn/wap/2.0/js/183min.js"></script>
    <script src="/rrn/wap/2.0/js/vbyzc.js"></script>
</head>
<body>
<%--<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>--%>
<div class="wrap bg-gray  " style="margin-bottom:60px;">
    <div class="head-box-FFFFFF">
        <div class="head-box clearfix">
            <div class="head-line1 clearfix">
                <a href="/rest/wap/personalcenter/setting/?_t=<%=mills%>">
                    <ul>
                        <li><img class="head-phone45"
                                 src="<c:choose><c:when test="${not empty userInfo.userImage }">${userInfo.userImage}</c:when><c:otherwise>/rrn/1.0/imgs/login_after.fw.png</c:otherwise></c:choose>"/>

                        </li>
                        <li class="font-size16" style="line-height:2rem;margin-top:1rem">${userInfo.nickName}<br><span
                                class="font-size12">设置个人信息</span></li>
                        <li style="line-height:6.4rem;"><i class=" icon-more1 font-gray6"></i>
                        </li>
                    </ul>
                </a>
                <div class="cl"></div>
                <ul class="user_tabinfo">
                    <li>我的积分：${userAccount.integral}</li>
                    <li>
                        <%--优惠券 ${userCouponTicketsNo} 张--%>
                        &nbsp;
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- 总资产 -->
    <div class="head-box-FFFFFF">
        <div class="head-box clearfix">
            <div class="head-line2 clearfix">
                <ul>
                    <li>总资产</li>
                    <li class=" font-size21">
                        <span class="font-red"><fmt:formatNumber type="number" pattern="#0.00"
                                                                 value="${userAccount.money+agreementMoney}"/></span>
                    </li>
                </ul>
                <div class="btn-recharge">
                    <c:choose>
                        <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                            <a class="btn-small btn-red" href="/rest/wap/fund/recharge/?_t=<%=mills%>">充值</a>
                        </c:when>
                        <c:otherwise>
                            <a class="btn-small btn-red"
                               href="javascript:showDialog('为了保障您的权益，请先绑定手机号!','确定',function(){window.location.href='/rest/wap/personalcenter/setting/phone/?_t='+new Date().getTime();},'取消')"
                               title="人人牛网在线充值">充值</a>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${userInfo.userParamInfoTags.phoneTag&&userInfo.userParamInfoTags.idTag&&userInfo.userParamInfoTags.bankTag&&userInfo.userParamInfoTags.accountPasswordTag}">
                            <a class="btn-small btn-blue" href="/rest/wap/fund/withdrawals/?_t=<%=mills%>">提现</a>
                        </c:when>
                        <c:otherwise>
                            <a class="btn-small btn-blue"
                               href="javascript:showDialog('请先进行手机绑定、实名认证、绑定银行卡、设置提现密码','确定',function(){window.location.href='/rest/wap/personalcenter/setting/?_t='+new Date().getTime();},'取消')"
                               title="人人牛网在线提现">提现</a>
                        </c:otherwise>
                    </c:choose>

                </div>
            </div>
        </div>
        <!-- 资产分布 2列 -->
        <div class="infobox">

                <div class="w50p">
                    <a href="/rest/wap/trade/?_t=<%=mills%>">
                        <div class="wraps left">
                            <p class="infotitle font-size12" style="color:rgb(102,102,102)">证券</p>
                            <p class="font-gray3 font-size20">￥<fmt:formatNumber pattern="#0.00" type="number"
                                                                                 value="${agreementMoney}"/></p>
                        </div>
                        <div class="wraps right"><i class=" icon-more1 font-gray9"></i></div>
                    </a>
                </div>


            <div class="w50p">
                <a href="/rest/wap/personalcenter/flowing/?_t=<%=mills%>">
                    <div class="wraps left">
                        <p class="infotitle font-size12" style="color:rgb(102,102,102)">余额</p>
                        <p class="font-orange font-size20">￥<fmt:formatNumber type="number" pattern="#0.00"
                                                                              value="${userAccount.money}"/></p>
                    </div>
                    <div class="wraps right"><i class=" icon-more1 font-gray9"></i></div>
                </a>
            </div>

        </div>
    </div>


    <!--***************************合约************************* -->
    <!--***************************合约************************* -->
    <!--***************************合约************************* -->
    <c:choose>
        <c:when test="${agreementList!=null&&fn:length(agreementList)>0}">
            <c:forEach var="agreement" items="${agreementList}">
                <a href="/rest/wap/trade/tradeDetail/${agreement.showAgreementId}?_t=<%=mills%>">
                    <div class="contract-list " style="">
                        <div class="mytrade-title  ">${agreement.signAgreementTypeDesc}</div>
                        <div class="mytrade-data  ">
                            <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/>
                            <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate
                                pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate
                                pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate>
                        </div>
                        <div class="mytrade-cont">
                            <label class="<c:choose><c:when test="${agreement.diffMoney>=0}">current-data-rise</c:when><c:otherwise>current-data-fall</c:otherwise></c:choose>">
                            <span>当前额度：<span class="  ng-isolate-scope">：<fmt:formatNumber pattern="#0.00"
                                                                                           value="${agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney}"
                                                                                           type="number"></fmt:formatNumber></span></span>
                                <div class="current-quota">
                            <span class="fl">
                                <span class=""><c:choose><c:when
                                        test="${agreement.diffMoney>=0}">+</c:when><c:otherwise></c:otherwise></c:choose></span>
                                <span class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number"
                                                                                   value="${agreement.diffMoney}"></fmt:formatNumber></span>
                            </span>
                                    <span class="fr">
                                            <span class=""><c:choose><c:when test="${agreement.diffMoney>=0}">+</c:when><c:otherwise></c:otherwise></c:choose></span>
                                            <span class="  ng-isolate-scope"><fmt:formatNumber type="number"
                                                                                               value="${agreement.diffMoney*100/(agreement.stockCurrentPrice+agreement.availableCredit)}"
                                                                                               pattern="#0.00"/></span>%<span
                                            class=""><c:choose><c:when
                                            test="${agreement.diffMoney>=0}">↑</c:when><c:otherwise>↓</c:otherwise></c:choose></span>
                                        </span>
                                </div>
                            </label>
                            <label class="early-warn">亏损警戒线
                                <br>
                                <span class="font-orange fb"><span class="  ng-isolate-scope"><fmt:formatNumber
                                        type="number"
                                        value="${agreement.warningLineMoney}"
                                        pattern="#0.00"/></span></span></label>
                            <label class="stop-soss">亏损平仓线
                                <br>
                                <span class="font-red fb"><span class="  ng-isolate-scope"><fmt:formatNumber
                                        type="number"
                                        value="${agreement.closeLineMoney}"
                                        pattern="#0.00"/></span></span></label>
                            <label class="blocked-fund">风险保证金
                                <br>
                                <span class="font-blue fb"><span class="  ng-isolate-scope"><fmt:formatNumber
                                        type="number"
                                        pattern="#0.00"
                                        value="${agreement.lockMoney}"></fmt:formatNumber></span></span></label>
                        </div>
                    </div>
                </a>
            </c:forEach>
        </c:when>
        <c:otherwise>
            <a href="/rest/wap/trade/type/0/?_t=<%=mills%>">
                <div class="contract-list " style="">
                    <div class="mytrade-title  " style="text-align:center"> 暂无合约，点击前往我要操盘</div>
                </div>
            </a>
        </c:otherwise>
    </c:choose>
</div>
<%--<div class="cancel-btn" style="--%>
<%--position: fixed;--%>
<%--bottom: 3px;--%>
<%--">--%>
<%--<a href="/rest/wap/trade/type/0/?_t=<%=mills%>" class="btn-full btn-red" id="confirmBtn">我要操盘</a>--%>
<%--</div>--%>

<div class="footnavwrap">
    <nav class="bottom-nav-w clearfix">
        <a class="cell " href="/rest/wap/page/index?_t=<%=mills%>"> <i class="icon-financ font-size24"></i> <br>活动</a>
        <a class="cell " href="/rest/wap/trade/type/0/?_t=<%=mills%>"> <i class="icon-caopan font-size24"></i>
            <br>操盘</a>
        <a class="cell active" href="javascript:void(0)"><i class="icon-wo1 font-size24"></i><br>我的</a>
    </nav>
</div>

</body>
</html>
