<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 10:18
  Desc: 用户中心
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>用户中心</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/personCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray  ">
    <div class="head-box clearfix">

        <div class="head-line1 clearfix">
            <a href="/rest/wap/personalcenter/setting/?_t=<%=mills%>" class="fl">
                <ul>
                    <li>
                        <img class="head-phone45"
                             src="<c:choose><c:when test="${not empty userInfo.userImage }">${userInfo.userImage}</c:when><c:otherwise><%=basePath%>/rrn/1.0/imgs/login_after.fw.png</c:otherwise></c:choose>">
                    </li>
                    <li class=" ">${userInfo.nickName}</li>
                    <li>

                        <%--<div class="font-white"><span class="font-red  "  >4529</span> 积分</div>--%>
                        <%--<div class="font-white"><span class="font-red  "  >0</span> 张卡券</div>--%>

                        <i class="person-set icon-more1 font-grayD"></i>
                    </li>
                </ul>
            </a>
        </div>

        <div class="head-line2 clearfix">
            <ul>
                <li>总资产</li>
                <li class="font-orange font-size21">
                    <span class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number"
                                                                       value="${agreementMoney+availableMoney}"></fmt:formatNumber></span>
                </li>
            </ul>
            <div class="btn-recharge">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                        <a class="btn-small btn-red" href="/rest/wap/fund/recharge/?_t=<%=mills%>">充值</a>
                    </c:when>
                    <c:otherwise>
                        <a class="btn-small btn-red" href="javascript:toast('为了保障您的权益，请先绑定手机号!')"
                           title="人人牛网在线充值">充值</a>
                    </c:otherwise>
                </c:choose>
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.phoneTag&&userInfo.userParamInfoTags.idTag&&userInfo.userParamInfoTags.bankTag&&userInfo.userParamInfoTags.accountPasswordTag}">
                        <a class="btn-small btn-blue" href="/rest/wap/fund/withdrawals/?_t=<%=mills%>">提现</a>
                    </c:when>
                    <c:otherwise>
                        <a class="btn-small btn-blue" href="javascript:alert('请先进行手机绑定、实名认证、绑定银行卡、设置提现密码')"
                           title="人人牛网在线提现">提现</a>
                    </c:otherwise>
                </c:choose>

            </div>
        </div>
        <!-- 资产分布 -->
        <div class="head-line3">
            <div class="icon-block btn-red"></div>
            <div class="line3-cont"><span class="font-gray9">证券</span>
                <span>
						<span class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number"
                                                                           value="${agreementMoney}"></fmt:formatNumber> </span>
					</span>
            </div>
        </div>
        <div class="head-line3">
            <div class="icon-block btn-orange"></div>
            <div class="line3-cont"><span class="font-gray9">投资VIP</span>
                <span>
						<span class=" ng-isolate-scope">0.00</span>
					</span>
            </div>
        </div>
        <div class="head-line3">
            <div class="icon-block btn-blue"></div>
            <div class="line3-cont"><span class="font-gray9">现金</span>
                <span>
						<span class=" ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number"
                                                                          value="${availableMoney}"></fmt:formatNumber></span>
					</span>
            </div>
        </div>
    </div>
    <!-- 三条线 -->
    <div class="head-allline">
        <div class="trade-line" style="width:<fmt:formatNumber pattern="#0.00" type="number"
                                                               value="${agreementMoney*100/(availableMoney+agreementMoney)}"></fmt:formatNumber>%; background-color: #f00;"></div>
        <div class="finance-line" style="width: 0%; background-color: rgb(252, 174, 82);"></div>
        <div class="cash-line" style="width: <fmt:formatNumber pattern="#0.00" type="number"
                                                               value="${availableMoney*100/(availableMoney+agreementMoney)}"></fmt:formatNumber>%; background-color:#464f63;"></div>
    </div>

    <div class="common-list">
        <a class=" " href="/rest/wap/trade/type/0/?_t=<%=mills%>">
            <ul class="list-haveline clearfix">
                <li class="font-gray3"><i class="user-icon icon-mytrade font-size18"></i>我要操盘</li>
                <li><i class="icon-more1 font-grayD"></i></li>
            </ul>
        </a>
        <a class=" " href="/rest/wap/trade/?_t=<%=mills%>">
            <ul class="list-haveline clearfix">
                <li class="font-gray3"><i class="user-icon icon-mytrade font-size18"></i>我的操盘</li>
                <li><i class="icon-more1 font-grayD"></i></li>
            </ul>
        </a>
        <%--<ul class="list-haveline clearfix" vbyzc-go="http://m.9niu.com/mycompet.html">--%>
        <%--<a>--%>
        <%--<li class="font-gray3"><i class="user-icon icon-mycompet font-size18"></i>我的竞技</li>--%>
        <%--<li><i class="icon-more1 font-grayD"></i></li>--%>
        <%--</a>--%>
        <%--</ul>--%>
    </div>
    <%--<div class="common-list">--%>
    <%--<ul class="list-haveline clearfix" vbyzc-go="extend.html">--%>
    <%--<a>--%>
    <%--<li class="font-red"><i class="user-icon icon-tuiguangzhuanqianxian font-size18"></i>赚外快</li>--%>
    <%--<li><i class="icon-more1 font-grayD"></i></li>--%>
    <%--</a>--%>
    <%--</ul>--%>
    <%--</div>--%>
    <div class="common-list">
        <a href="/rest/wap/personalcenter/setting/?_t=<%=mills%>">
            <ul class="list-haveline clearfix">

                <li class="font-gray3"><i class="user-icon icon-zijinmingxi font-size18"></i>个人中心</li>
                <li><i class="icon-more1 font-grayD"></i></li>

            </ul>
        </a>
        <%--<ul class="list-haveline clearfix" vbyzc-go="mytrack.html">--%>
        <%--<a>--%>
        <%--<li class="font-gray3"><i class="user-icon icon-zhuizong font-size18"></i>我的追踪</li>--%>
        <%--<li><i class="icon-more1 font-grayD"></i></li>--%>
        <%--</a>--%>
        <%--</ul>--%>
        <%--<ul class="list-haveline clearfix" vbyzc-go="coupon.html">--%>
        <%--<a>--%>
        <%--<li class="font-gray3"><i class="user-icon icon-jifen1 font-size18"></i>积分卡券</li>--%>
        <%--<li><i class="icon-more1 font-grayD"></i></li>--%>
        <%--</a>--%>
        <%--</ul>--%>
        <a href="/rest/wap/personalcenter/flowing/?_t=<%=mills%>">
            <ul class="list-haveline clearfix">

                <li class="font-gray3"><i class="user-icon icon-zijinmingxi font-size18"></i>资金明细</li>
                <li><i class="icon-more1 font-grayD"></i></li>

            </ul>
        </a>
    </div>
    <div class="common-list">
        <c:choose>
            <c:when test="${distributionUser!=null&&distributionUser.accountLevel>-1}">


                <a href="/rest/wap/page/share/">
                    <ul class="list-haveline clearfix">

                        <li class="font-gray3"><i class="user-icon icon-xinxi1 font-size18"></i>赚外快</li>
                        <li><i class="icon-more1 font-grayD"></i></li>

                    </ul>
                </a>
            </c:when>
        </c:choose>


        <a href="/rest/wap/page/about/?_t=<%=mills%>">
        <ul class="list-haveline clearfix" >
            <li class="font-gray3"><i class="user-icon icon-xinxi font-size18"></i>关于人人牛</li>
            <li><i class="icon-more1 font-grayD"></i></li>
        </ul>
        </a>

    </div>

</div>

</body>
</html>
