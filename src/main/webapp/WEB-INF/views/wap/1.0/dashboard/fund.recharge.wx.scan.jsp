<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/19
  Time: 15:28
  Desc: 微信扫码支付
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>支付确认</title>

    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/personCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self" >
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div   class="wrap bg-gray  ">
    <div class="contract-cont">
        <div class="his-codetitle clearfix">
            <div class="fl">当前余额</div>
            <div class="font-size14 fr"><span class="font-red"><span class=""><fmt:formatNumber pattern="#0.00"
                                                                                                type="number"
                                                                                                value="${userAccount.money}"></fmt:formatNumber> </span></span>
                元
            </div>
        </div>
    </div>
    <div class="paycheck">
        <div class="row">
            <div class="title">交易类型</div>
            <div class="con">${recharge.rechargeDesc}</div>
        </div>
        <div class="row">
            <div class="title">金额</div>
            <div class="con" style="color:#f20;font-size:18px;">￥<fmt:formatNumber pattern="#0.00" type="number" value="${recharge.money}"></fmt:formatNumber> </div>
        </div>

        <div class="row">
            <div class="title">时间</div>
            <div class="con"><fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${recharge.createTime}" var="rDate"/><fmt:formatDate value="${rDate}" pattern="yyyy-MM-dd HH:mm:ss"/> </div>
        </div>

        <div class="row">
            <div class="title">订单号</div>
            <div class="con bold">${recharge.showRechargeId}</div>
        </div>

        <div class="row">
            <div class="title">过期时间</div>
            <div class="con"><fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${recharge.expiredTime}" var="rDate"/><fmt:formatDate value="${rDate}" pattern="yyyy-MM-dd HH:mm:ss"/></div>
        </div>
        <div class="row center p10">
            <img src="/rest/fund/${recharge.showRechargeId}"  width="200px" alt="">
            <p>扫码支付</p>
        </div>
    </div>
    <div class="recharge-tip">
        <h1>温馨提示</h1>
        <p>1、为了您的资金安全，您的账户资金将有第三方银行托管 </p>
        <p>2、账户充值只能通过储蓄卡或微信零钱支付 </p>
        <p>3、为了您的资金安全，建议充值前进行实名认证、手机绑定、设置提现密码</p>
        <p>4、如果充值遇到任何问题，请联系客服：0592-5917777</p>
    </div>
    <div>&nbsp;</div>
</div>
<script>
    $(function(){
        var count = 0;
        var interval = setInterval(function(){
//            if(count>6){
//                console("还没充值完成，那就给个弹窗吧！");
//                clearInterval(interval);
//            }
            var data ={"orderId":${recharge.showRechargeId}};
            $.post("/rest/fund/recharge/query/wx/",data,function(res){
                if(res.resultFlag){
                    alert("充值成功",function(){
                        window.location.href="/rest/assets/";
                    });

                }
            },"json")
            count++;
        },10000);
    })
</script>
</body>
</html>
