<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/17
  Time: 17:39
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>操盘-买入</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/tradeClient.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/tradeClient.js?20170710"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray">
    <!-- 余额 -->
    <div class="contract-cont">
        <div class="his-codetitle clearfix">
            <div class="fl">现金余额</div>
            <div class="font-size14 fr font-red">
                <span class="JS-balance-money">
                <fmt:formatNumber pattern="#0.00" type="number" value="${agreement.availableCredit}"></fmt:formatNumber>
                </span>
                元
            </div>
        </div>
    </div>

    <!-- 数据 -->
    <div class="taking-module">
        <div class="taking-tab clearfix">
            <a href="/rest/wap/trade/${agreement.showAgreementId}/position/?_t=<%=mills%>">持仓</a>
            <a href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/buy/<c:choose><c:when test="${not empty stockCode}">${stockCode}/</c:when></c:choose>?_t=<%=mills%>" >买入</a>
            <a href="/rest/wap/trade/tradeClient/${agreement.showAgreementId}/sell/<c:choose><c:when test="${not empty stockCode}">${stockCode}/</c:when></c:choose>?_t=<%=mills%>" class="selected">卖出</a>
            <a href="/rest/wap/trade/${agreement.showAgreementId}/waiting/?_t=<%=mills%>">撤单</a>
            <a href="/rest/wap/trade/${agreement.showAgreementId}/query/?_t=<%=mills%>">查询</a>
        </div>

        <div class="shop-module clearfix">
            <div class="shop-left">
                <ul>
                    <li class="trade-code"><input class="input-box" id="JS-input-stockcode" type="tel" value="<c:choose><c:when test="${not empty stockCode}">${stockCode}</c:when></c:choose>" placeholder="股票代码"><div class="trade-name" id="stockname"></div></li>
                    <!-- 加减按钮 -->
                    <li class="trade-line">
                        <i class="icon-sub icon-jianshao JS-minPrice"></i>
                        <label><input class="input-trading" id="JS-entrust-price" type="text" value="0.00"></label>
                        <i class="icon-add icon-tianjia JS-addPrice"></i>
                    </li>
                    <li class="clearfix">
                        <div class="fl">跌停&nbsp; <span class="font-green lowerLimitPrice">0</span></div>
                        <div class="fr">涨停&nbsp; <span class="font-red upperLimitPrice">0</span></div>
                    </li>
                    <!-- 股数输入 -->
                    <li class="trade-code">
                        <input class="input-box" id="JS-entrust-num" type="tel" placeholder="卖出股数">
                        <div class="trade-name font-gray9">最大可卖 <span class="JS-weituo-max">0</span> 股</div>
                    </li>
                    <!-- <li class="trade-code">
                        <input class="input-box " id="JS-entrust-num" type="tel" placeholder="卖出股数">
                        <div class="trade-name font-gray9">最大可卖 0 股</div>
                    </li> -->
                    <!-- 买入买出按钮 -->
                    <li class="clearfix">
                        <div class="menu-buy"><a id="JS-submit-order" class="btn-trade btn-green" type="button">卖出</a></div>
                        <%--<div class="menu-market"><input id="JS-submit-commission" class="btn-greenborder" type="button" value="市价卖出"></div>--%>
                    </li>
                    <!-- <li class="clearfix">
                        <div class="menu-buy"><a id="JS-submit-order" class="btn-trade btn-green" type="button">卖出</a></div>
                        <div class="menu-market"><input class="btn-greenborder" type="button" value="市价卖出"></div>
                    </li> -->
                </ul>
            </div>
            <div class="shop-right">
                <div class="shop-shelves clearfix">
                    <ul class="clearfix"><li>卖5</li><li class="font-red"><span class="sellPrice5">0</span></li><li class="sellQty5">0</li></ul>
                    <ul class="clearfix"><li>卖4</li><li class="font-red"><span class="sellPrice4">0</span></li><li class="sellQty4">0</li></ul>
                    <ul class="clearfix"><li>卖3</li><li class="font-red"><span class="sellPrice3">0</span></li><li class="sellQty3">0</li></ul>
                    <ul class="clearfix"><li>卖2</li><li class="font-red"><span class="sellPrice2">0</span></li><li class="sellQty2">0</li></ul>
                    <ul class="clearfix"><li>卖1</li><li class="font-red"><span class="sellPrice1">0</span></li><li class="sellQty1">0</li></ul>
                </div>
                <div class="shop-shelves clearfix">
                    <ul class="clearfix"><li>买1</li><li class="font-red"><span class="buyPrice1">0</span></li><li class="buyQty1">0</li></ul>
                    <ul class="clearfix"><li>买2</li><li class="font-red"><span class="buyPrice2">0</span></li><li class="buyQty2">0</li></ul>
                    <ul class="clearfix"><li>买3</li><li class="font-red"><span class="buyPrice3">0</span></li><li class="buyQty3">0</li></ul>
                    <ul class="clearfix"><li>买4</li><li class="font-red"><span class="buyPrice4">0</span></li><li class="buyQty4">0</li></ul>
                    <ul class="clearfix"><li>买5</li><li class="font-red"><span class="buyPrice5">0</span></li><li class="buyQty5">0</li></ul>
                </div>
            </div>
        </div>
    </div>

    <!-- 股票持仓详情 -->
    <c:choose>
        <c:when test="${stockPositionList!=null&&fn:length(stockPositionList)>0}">
            <div class="taking-module">
                <table class="table-list" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <tr class="none-topline">
                        <th width="">名称/代码</th>
                        <th width="">市值/盈亏</th>
                        <th width="">持仓/可用</th>
                        <th width="">成本/现价</th>
                    </tr>
                    <c:forEach var="stockPosition" items="${stockPositionList}">
                        <tr class="JS-loadStockInfo" data-stockno="${stockPosition.stockCode}" data-available="${stockPosition.stockAvailableNum}">
                            <td class="first-column "><span class="">${stockPosition.stockName}</span><span class="">${stockPosition.stockCode}</span></td>
                            <td>
                                <em class="<c:choose><c:when test="${stockPosition.currentMarketValue-stockPosition.buyTotalExpenses>=0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">
                                    <span class=""><fmt:formatNumber pattern="#0.00" type="number" value="${stockPosition.currentMarketValue}"></fmt:formatNumber> </span>
                                    <span style="display: inline;" class=""><fmt:formatNumber pattern="#0.00"
                                                                                              type="number"
                                                                                              value="${stockPosition.currentMarketValue-stockPosition.buyTotalExpenses}"></fmt:formatNumber></span>(<span
                                        style="display: inline;" class=""><fmt:formatNumber pattern="#0.00"
                                                                                            type="number"
                                                                                            value="${(stockPosition.currentMarketValue-stockPosition.buyTotalExpenses)*100/stockPosition.buyTotalExpenses}"></fmt:formatNumber></span>%)
                                </em>
                            </td>
                            <td><span class="">${stockPosition.stockNum}</span><span class="">${stockPosition.stockAvailableNum}</span></td>
                            <td><span class="font-gray3">${stockPosition.buyPrice}</span><span class="<c:choose><c:when test="${stockPosition.currentMarketValue-stockPosition.buyTotalExpenses>=0}">font-red</c:when><c:otherwise>font-green</c:otherwise></c:choose>">${stockPosition.currentPrice}</span></td>
                        </tr>
                    </c:forEach>

                    </tbody>
                </table>
            </div>
        </c:when>
    </c:choose>
</div>
<!-- ***********************买入确认************************************** -->
<div class="popup-container trade-popup-container" style="display:none;">
    <div class="trade-popup">
        <div class="trade-popup-head">委托卖出确认</div>
        <!-- <div class="trade-popup-head">委托卖出确认</div> -->
        <div class="trade-popup-body">
            <div class="trade-popup-wrap"><span class="popup-text">股票代码</span><span class="popup-value popup-stock-code"></span></div>
            <div class="trade-popup-wrap"><span class="popup-text">股票名称</span><span class="popup-value popup-stock-name"></span></div>
            <div class="line"></div>
            <div class="trade-popup-wrap"><span class="popup-text">委托价格</span><span class="popup-value popup-stock-commission-price"></span></div>
            <div class="trade-popup-wrap"><span class="popup-text">委托数量</span><span class="popup-value popup-stock-commission-amount"></span></div>
        </div>
        <div class="trade-popup-buttons">
            <button class="JS-action-buyin">确定卖出</button>
            <!-- <button class="">确定卖出</button> -->
        </div>
    </div>
</div>


<!-- **********************************隐藏内容*********************** -->
<input type="hidden" name="" id="JS-value-hyNo" value="${agreement.agreementId}"><!--合约编号 -->
<input type="hidden" name="" id="v-post-action" value="2" data-description='提交的动作,设定死的，和代码关联，1买，2卖'>
<input type="hidden" name="" id="commissionType" value="0">
<script>
    <c:choose><c:when test="${not empty stockCode}">
    $(function(){
        Load_stock_info(${stockCode});
    });
    </c:when></c:choose>
</script>
</body>
</html>
