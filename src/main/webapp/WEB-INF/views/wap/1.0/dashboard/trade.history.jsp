<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/24
  Time: 14:57
  Desc: 操盘历史信息
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>历史操盘</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/applyTrade.css">
    <link href="<%=basePath%>/rrn/wap.1.0/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/jquery.circliful.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/hisTrade.js"></script>
    <base target="_self" >
    <script>
        $( document ).ready(function() {
            $('.myStat2').circliful();
        });
    </script>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray  ">
    <!-- 主体 -->
    <div class="contract-cont">
        <div class="his-codetitle clearfix">
            <div class="fl">合约编号</div>
            <div class="font-size14 fr  ">${agreement.showAgreementId}</div>
        </div>
        <!--盈利时显示-->
        <!-- contract-fall -->
        <div class="contract-chart">
            <div class="mytrade-title  ">${agreement.signAgreementTypeDesc}</div>
            <div class="mytrade-data  ">
                <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate>

            </div>
            <div class="chart-img" id="hisTradeChart" _echarts_instance_="1478503271992" style="height: 160px; width: 100%; -webkit-tap-highlight-color: transparent; -webkit-user-select: none; background-color: rgba(0, 0, 0, 0);">
                <div style="position: relative; overflow: hidden; width: 100%; height: 160px;">
                    <div class="myStat2" data-dimension="150" data-text="<fmt:formatNumber pattern="#0.00" type="number" value="${(agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney)*100/agreement.lockMoney}"></fmt:formatNumber>%" data-info="盈亏比例" data-width="10" data-fontsize="28" data-percent="<fmt:formatNumber pattern="#0.00" type="number" value="${(agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney)*100/agreement.lockMoney}"></fmt:formatNumber>" data-fgcolor="<c:choose><c:when test="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney>0}">#f00</c:when><c:otherwise>#0f0</c:otherwise></c:choose>" data-bgcolor="#eee"></div>
                </div>
            </div>
            <div class="font-gray3">交易盈亏</div>
            <div class="chart-value" style="color:<c:choose><c:when test="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney>0}">#f00</c:when><c:otherwise>#0f0</c:otherwise></c:choose>">
                <span class=""><fmt:formatNumber pattern="#0.00" type="number" value="${(agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney)}"></fmt:formatNumber></span>
            </div>
            <%--<div class="sharebtn" style=""><span>炫耀一下</span><img src="images/shareicon.png" width="18px"></div>--%>
        </div>
        <div class="common-list no-botline">
            <ul class="list-haveline clearfix">
                <li>总操盘资金</li>
                <li>
                    <span class=""><fmt:formatNumber pattern="#0.00" type="number" value="${agreement.borrowMoney+agreement.lockMoney}"></fmt:formatNumber> </span>元</li>
            </ul>
            <ul class="list-haveline clearfix">
                <li>管理费</li>
                <li>
                    <span class=""><fmt:formatNumber pattern="#0.00" type="number" value="${agreement.accountManagementFee*agreement.continueOperatePeriod}"></fmt:formatNumber> </span>元</li>
            </ul>
            <ul class="list-haveline clearfix">
                <li>交易品种</li>
                <li>A股</li>
            </ul>
        </div>
    </div>
    <!-- 按钮 -->
    <div class="menu-topspace">
        <a class="btn-grayborder" vbyzc-go="/rest/wap/trade/tradeHistory/detail/${agreement.showAgreementId}?_t=${mills}">查看交易详情</a>
    </div>
</div>
</body>
</html>
