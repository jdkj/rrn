
<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/12
  Time: 16:35
  Desc: 移动版通用头
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<c:choose>
    <c:when test="${userInfo==null}">
        <nav class="global-nav">
            <header class="">
                <a href="/"><img src="<%=basePath%>/rrn/wap.1.0/images-assets/logo.png" height=35 class="fl"></a>
                <span class="login-btn"> <a href="/rest/wap/page/login">登录</a> </span>
            </header>
        </nav>
    </c:when>
    <c:otherwise>
        <style>
            .fix-menu {
                width: 22px;
                height: 22px;
                display: block;
                position: absolute;
                right: 12px;
                top: 0;
                top: 12px;
                z-index: 2;
                background: url(/rrn/wap.1.0/icons-assets/icon_list.png) no-repeat center center;
                background-size: contain;
            }
        </style>
        <nav class="global-nav">
            <header class="">
                <a href="/"><img src="<%=basePath%>/rrn/wap.1.0/images-assets/logo.png" height=35 class="fl"></a>
                <a href="/rest/wap/personalcenter/" class="fix-menu"></a>
            </header>
                <%--<!-- 用户登录后的系统菜单 -->--%>
                <%--<aside class="m_animate" id="topHideMenulist">--%>
                <%--<div class="user-info ">--%>
                <%--<p class="">--%>
                <%--<a href="/rest/wap/personalcenter/"><img alt="用户头像" src="<c:choose><c:when test="${not empty userInfo.userImage }">${userInfo.userImage}</c:when><c:otherwise><%=basePath%>/rrn/1.0/imgs/login_after.fw.png</c:otherwise></c:choose>"></a>--%>
                <%--</p>--%>
                <%--</div>--%>
                <%--<nav class="nav-wrap wrap">--%>
                <%--<p>--%>
                <%--<span class="clearfix" style="display: inline-block;width:159px;">--%>

                <%--<c:choose>--%>
                <%--<c:when test="${userInfo.userParamInfoTags.phoneTag&&userInfo.userParamInfoTags.idTag&&userInfo.userParamInfoTags.bankTag&&userInfo.userParamInfoTags.accountPasswordTag}">--%>
                <%--<a href="/rest/wap/fund/withdrawals/" title="人人牛网在线提现">提现</a>--%>
                <%--</c:when>--%>
                <%--<c:otherwise>--%>
                <%--<a href="javascript:alert('请先进行手机绑定、实名认证、绑定银行卡、设置提现密码')" title="人人牛网在线提现">提现</a>--%>
                <%--</c:otherwise>--%>
                <%--</c:choose>--%>
                <%--<c:choose>--%>
                <%--<c:when test="${userInfo.userParamInfoTags.phoneTag}">--%>
                <%--<a href="/rest/wap/fund/recharge/" title="人人牛网在线充值">充值</a>--%>
                <%--</c:when>--%>
                <%--<c:otherwise>--%>
                <%--<a href="javascript:alert('为了保障您的权益，请先绑定手机号!')" title="人人牛网在线充值">充值</a>--%>
                <%--</c:otherwise>--%>
                <%--</c:choose>--%>

                <%--</span>--%>
                <%--</p>--%>
                <%--<a href="/rest/wap/personalcenter/"><p class=" "><i class="niu-icon icon-yonghu"></i>个人中心</p></a>--%>
                <%--<a href="/rest/wap/trade/type/0/"><p class=" "><i class="niu-icon icon-chaopan"></i>我要操盘</p></a>--%>
                <%--&lt;%&ndash;<a href="compet.html"><p class=""><i class="niu-icon icon-jingji"></i>我要竞技</p></a>&ndash;%&gt;--%>
                <%--&lt;%&ndash;<a href="news.html"><p class=" "><i class="niu-icon icon-zixun"></i>资讯中心</p></a>&ndash;%&gt;--%>
                <%--&lt;%&ndash;<a href="help.html"><p class=" "><i class="niu-icon icon-bangzhuzhongxin"></i>帮助中心</p></a>&ndash;%&gt;--%>
                <%--&lt;%&ndash;<a href="suggest.html"><p><i class="niu-icon icon-kefu"></i>联系客服</p></a>&ndash;%&gt;--%>
                <%--</nav>--%>
                <%--</aside>--%>
        </nav>
    </c:otherwise>
</c:choose>


