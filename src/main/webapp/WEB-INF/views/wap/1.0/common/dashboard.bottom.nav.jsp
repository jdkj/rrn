<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/10/31
  Time: 11:04
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<style>

    .bottom-nav-w {
        width: 100%;
        background: #fff;
        position: fixed;
        left: 0px;
        bottom: 0px;
        z-index: 999999;
        display: -webkit-flex;
        display: flex;
        border-top: 1px solid #ddd;
    }

    .bottom-nav-w .cell {
        -webkit-flex: 1;
        flex: 1;
    }

    .bottom-nav-w a {
        float: left;
        display: block;
        text-align: center;
        width: 33.3%;
        color: #666;
        font-size: 12px;
        padding: 5px 0px;
        transition: all .3s;
        -webkit-transition: all .3s;
    }

    .bottom-nav-w a.active {
        color: #f50;
    }

    .bottom-nav-w a i {
        display: inline-block;
        width: 100%;
        margin-bottom: 2px
    }

    .bottom-nav-w a.select {
        color: #FFF
    }
</style>
<!-- ************底部选项卡************ -->
<div class="footnavwrap">
    <nav class="bottom-nav-w clearfix">
        <a class="cell" vbyzc-go="#"> <i class="icon-financ font-size24"></i> <br>活动</a>
        <a class="cell active" vbyzc-go="#"> <i class="icon-caopan font-size24"></i> <br>操盘</a>
        <a class="cell" vbyzc-go="#"><i class="icon-wo1 font-size24"></i><br>我的</a>
    </nav>
</div>
