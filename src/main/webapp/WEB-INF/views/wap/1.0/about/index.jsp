<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 10:58
  Desc: 关于首页
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>关于</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/helpCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self">
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray  ">
    <div class="zhuye" style="background-color: rgb(180,29,34)"></div>
    <div class="module-bottomborder bg-white topline">

        <ul class="list-line clearfix">
            <a href="/rest/wap/page/about/introduction/">
                <li>
                    <i class="icon-about font-grayCB"></i>
                    <span class="font-gray6">&nbsp;&nbsp;公司简介</span>
                    <i class="icon-more1 font-grayCB"></i>
                </li>
            </a>
        </ul>

        <!-- <ul class="list-line clearfix  " vbyzc-href="http://m.9niu.com/about/media.html"  >
            <li>
                <i class="icon-media font-grayCB"></i>
                <a href="#" class="font-gray6">&nbsp;&nbsp;媒体报道</a>
                <i class="icon-more1 font-grayCB"></i>
            </li>
        </ul>
        <ul class="list-line clearfix  " vbyzc-href="http://m.9niu.com/about/safety.html"  >
            <li>
                <i class="icon-safe font-grayCB"></i>
                <a href="#" class="lianjie">&nbsp;&nbsp;安全保障</a>
                <i class="icon-more1 font-grayCB"></i>
            </li>
        </ul> -->
        <!-- <ul class="list-lineno clearfix" vbyzc-href="http://rrniu.30buy.com/agent/index">
        <li>
        <i class="icon-join font-grayCB"></i>
        <a href="#" class="lianjie">&nbsp;&nbsp;机构合作</a>
        <i class="icon-more1 font-grayCB"></i>
        </li>
        </ul> -->
    </div>
</div>
</body>
</html>
