<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/14
  Time: 11:05
  Desc: 公司简介
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <title>公司简介</title>
    <link rel="apple-touch-icon-precomposed" href="logo_icon.png">
    <link rel="icon" href="favicon.ico" mce_href="<%=basePath%>/rrn/wap.1.0/images/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/main9n.css">
    <link rel="stylesheet" href="<%=basePath%>/rrn/wap.1.0/css/helpCenter.css">
    <script src="<%=basePath%>/rrn/wap.1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/wap.1.0/js/vbyzc.js"></script>
    <base target="_self" >
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap">
    <div class="module-noborder">
        <div class="introduction fb font-size14 font-navline">公司简介</div>
        <div class="list-rightspace">
            <div class="bg-blue">关于人人牛</div>
            <p class="artical font-size12 font-gray6">
                人人牛网（www.rrniu.cn）隶属竞东(厦门)科技有限公司，致力于为广大股民提供方便、快捷、安全的互联网信息服务。人人牛网由多位金融、证券、私募和互联网领域的资深专家共同组建而成，以方便、安全、专业、创新的特征赢得了良好的用户口碑，已成为行业内极具影响力的品牌。</p>
            <div class="bg-blue">企业愿景</div>
            <p class="artical font-size12 font-gray6">让用户实现更多投资成就。</p>
            <div class="bg-blue">我们的优势</div>
            <p class="artical font-size12 font-gray6">
                <span class="fb">1.实力强劲</span>
                <br> 强大的金融背景，资深IT研发团队，技术保障能力强
                <br>
                <br>
                <span class="fb">2.使用便捷</span>
                <br> 多屏适应，实时覆盖，掌握市场最新动态
                <br>
                <br>
                <span class="fb">3.模式创新</span>
                <br>展现自我，交流切磋，以操盘技能论英雄
            </p>
        </div>
    </div>
</div>
</body>
</html>
