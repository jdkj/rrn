<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/1/23
  Time: 15:02
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <jsp:include page="../../common/head.jsp"/>
    <title>个人设置中心</title>
    <link rel="stylesheet" href="/rrn/wap/2.0/css/personCenter.2.0.css">
</head>
<body>
<jsp:include page="../../common/top.jsp"/>
<div class="wrap bg-gray  ">
    <div class="common-list">
        <ul class="list-haveline clearfix">
            <a>
                <li class="font-gray3">
                    <i class="user-icon icon-wo"></i>昵称
                </li>
                <li class=" ">${userInfo.nickName}<i class="font-white icon-more1"></i></li>
            </a>
        </ul>

        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.idTag}">
                        <ul class="list-haveline clearfix">
                            <li class="font-gray3">
                                <i class="user-icon icon-shimingrenzheng"></i>实名认证
                            </li>
                            <li>
                                <span class="" style="">${userIdAuth.idName}</span>
                                <i class="icon-more1 font-white" style=""></i></li>
                        </ul>
                    </c:when>
                    <c:otherwise>
                        <a href="/rest/wap/personalcenter/setting/idauth/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-shimingrenzheng"></i>实名认证
                                </li>
                                <li><span class="font-red  " style="">未认证</span>
                                    <i class="icon-more1 font-grayD" style=""></i></li>
                            </ul>
                        </a>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <a  href="javascript:toast('为了保障您的权益，请先绑定手机号!')">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-shimingrenzheng"></i>实名认证
                        </li>
                        <li><span class="font-red  " style="">未认证</span>
                            <i class="icon-more1 font-grayD" style=""></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>





        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.bankTag}">
                        <a href="/rest/wap/personalcenter/setting/userbank/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-yinxingleizhifu"></i>银行卡绑定
                                </li>
                                <li>
                                    <span class="" style="">已绑定</span>
                                    <i class="font-grayD icon-more1"></i>
                                </li>
                            </ul>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <c:choose>
                            <c:when test="${userInfo.userParamInfoTags.idTag}">
                                <a href="/rest/wap/personalcenter/setting/addbank/">
                                    <ul class="list-haveline clearfix">
                                        <li class="font-gray3">
                                            <i class="user-icon icon-yinxingleizhifu"></i>银行卡绑定
                                        </li>
                                        <li><span class="font-red  " style="">未绑定</span>
                                            <i class="font-grayD icon-more1"></i></li>
                                    </ul>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <a href="javascript:alert('请先进行身份认证')">
                                    <ul class="list-haveline clearfix">
                                        <li class="font-gray3">
                                            <i class="user-icon icon-yinxingleizhifu"></i>银行卡绑定
                                        </li>
                                        <li><span class="font-red  " style="">未绑定</span>
                                            <i class="font-grayD icon-more1"></i></li>
                                    </ul>
                                </a>
                            </c:otherwise>
                        </c:choose>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <a  href="javascript:toast('为了保障您的权益，请先绑定手机号!')" >
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-yinxingleizhifu"></i>银行卡绑定
                        </li>
                        <li><span class="font-red  " style="">未绑定</span>
                            <i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>



        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <a href="/rest/wap/personalcenter/setting/phone/modify/" style="display:block">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-shouji1"></i>手机绑定
                        </li>
                        <li class=" ">${userInfo.phone}<i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:when>
            <c:otherwise>
                <a href="/rest/wap/personalcenter/setting/phone/" style="display:block">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-shouji1"></i>手机绑定
                        </li>
                        <li class=" "><span class="font-red  " style="">未绑定</span><i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>

    </div>
    <div class="common-list">

        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.passwordTag}">
                        <a href="/rest/wap/personalcenter/setting/password/modify/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-mima1"></i>登录密码
                                </li>
                                <li>已设置<i class="font-grayD icon-more1"></i></li>
                            </ul>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a href="/rest/wap/personalcenter/setting/password/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-mima1"></i>登录密码
                                </li>
                                <li><span class="font-red  " style="">未设置</span><i class="font-grayD icon-more1"></i></li>
                            </ul>
                        </a>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <a href="javascript:toast('为了保障您的权益，请先绑定手机号!')">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-mima1"></i>登录密码
                        </li>
                        <li><span class="font-red  " style="">未设置</span><i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>




        <c:choose>
            <c:when test="${userInfo.userParamInfoTags.phoneTag}">
                <c:choose>
                    <c:when test="${userInfo.userParamInfoTags.accountPasswordTag}">
                        <a href="/rest/wap/personalcenter/setting/accountpassword/modify/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-zhifumima"></i>提款密码
                                </li>
                                <li>
                                    <span class="" style="">已设置</span><i class="font-grayD icon-more1"></i></li>
                            </ul>
                        </a>
                    </c:when>
                    <c:otherwise>
                        <a href="/rest/wap/personalcenter/setting/accountpassword/">
                            <ul class="list-haveline clearfix">
                                <li class="font-gray3">
                                    <i class="user-icon icon-zhifumima"></i>提款密码
                                </li>
                                <li><span class="font-red  " style="">未设置</span><i class="font-grayD icon-more1"></i></li>
                            </ul>
                        </a>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <a href="javascript:toast('为了保障您的权益，请先绑定手机号!')">
                    <ul class="list-haveline clearfix">
                        <li class="font-gray3">
                            <i class="user-icon icon-zhifumima"></i>提款密码
                        </li>
                        <li><span class="font-red  " style="">未设置</span><i class="font-grayD icon-more1"></i></li>
                    </ul>
                </a>
            </c:otherwise>
        </c:choose>



    </div>
    <%--<div class="save-btn">--%>
    <%--<a class="btn-full btn-orange">安全退出</a>--%>
    <%--</div>--%>
    <jsp:include page="../../common/bottom_nav.jsp"/>
</div>
</body>
</html>

