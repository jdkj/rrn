<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/1/23
  Time: 15:20
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <jsp:include page="../../common/head.jsp"/>
    <title>用户中心-身份认证</title>
    <link rel="stylesheet" href="/rrn/wap.1.0/css/update.css">
</head>
<body>
<jsp:include page="../../common/top.jsp"/>
<div   class="wrap  "  >
    <div class="module-fullspace">
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-yonghu"  ></i></span>
                <div class="line-down">
                    <div class="line-down">
                        <input class="longbox" id="realName"  type="text" placeholder="请输入真实姓名">
                    </div>
                </div>
            </li>
        </ul>
        <ul class="list-otherline clearfix">
            <li>
                <span class="pic-lock"><i class="icon-shimingrenzheng"  ></i></span>
                <div class="line-down">
                    <input class="longbox" id="idCard" type="number" placeholder="请输入您的18位身份证号">
                </div>
            </li>
        </ul>
    </div>
    <div class="module-white">
		<span class="pic-jinggao">
		  <i class="icon-jinggao"></i></span>
        <div class="tianxie font-gray9 font-size12">请填写有效的证件信息</div>
    </div>
    <div class="btn-full btn-orange JSaddBanksubmit">确认</div>
</div>
<jsp:include page="../../common/bottom_nav.jsp"/>
<script>
    $(function () {
        <c:choose>
        <c:when test="${userInfo.userParamInfoTags.idTag}">
        alert("您已通过实名认证");
        window.location.href = "/rest/wap/personalcenter/setting/";
        </c:when>

        <c:otherwise>

        $(".JSaddBanksubmit").click(function(){
            if(validIdentification()){
                var realName = $("#realName").val();
                var idCard = $("#idCard").val();
                var data = {"idNo":idCard,"idName":realName};
                $.post("/rest/user/authId",data,function(res){
                    if(res.resultFlag){
                        alert("认证成功");
                        window.location.href =  "/rest/wap/personalcenter/setting/";
                    }else{
                        alert(res.resultMsg);
                        return false;
                    }
                },"json");
            }
        });


        /**
         * 验证身份信息
         * @returns {boolean}
         */
        function validIdentification(){
            var realName = $("#realName").val();
            var idCard = $("#idCard").val();
            if(realName!=''&&idCard!=''&&idCard.length===18){
                return true;
            }
            alert("请输入正确的身份信息");
            return false;
        }


        </c:otherwise>
        </c:choose>
    })
</script>

</body>
</html>
