<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/1/20
  Time: 15:21
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
    long mills = System.currentTimeMillis();
%>
<html>
<head>
    <title>个人中心</title>
    <jsp:include page="../../common/head.jsp"/>
    <link rel="stylesheet" href="rrn/wap/2.0/css/applyTrade.css">
    <link rel="stylesheet" href="rrn/wap/2.0/css/personCenter.css">
</head>
<body>
<jsp:include page="../../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="wrap bg-gray  ">
    <!-- 头像 -->
    <div class="head-box-FFFFFF">
        <div class="head-box clearfix">
            <div class="head-line1 clearfix">
                <a href="/rest/wap/personalcenter/setting/?_t=<%=mills%>">
                    <ul>
                        <li><img class="head-phone45"
                                 src="<c:choose><c:when test="${not empty userInfo.userImage }">${userInfo.userImage}</c:when><c:otherwise>rrn/wap/2.0/images-assets/login_after.fw.png</c:otherwise></c:choose>"/>

                        </li>
                        <li class="font-size16" style="line-height:2rem;margin-top:1rem">${userInfo.nickName}<br><span
                                class="font-size12">设置个人信息</span></li>
                        <li style="line-height:6.4rem;"><i class=" icon-more1 font-gray6"></i>
                        </li>
                    </ul>
                </a>
                <div class="cl"></div>
                <ul class="user_tabinfo">
                    <li>我的积分：${userAccount.integral}</li>
                    <li>优惠券 ${userCouponTicketsNo} 张</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- 总资产 -->
    <div class="head-box-FFFFFF">
        <div class="head-box clearfix">
            <div class="head-line2 clearfix">
                <ul>
                    <li>总资产</li>
                    <li class=" font-size21">
                        <span class="font-red"><fmt:formatNumber type="number" pattern="#0.00"
                                                                 value="${userAccount.money+agreementMoney}"/></span>
                    </li>
                </ul>
                <div class="btn-recharge">
                    <a class="btn-small btn-red" href="recharge.html">充值</a>
                    <a class="btn-small btn-blue" href="drawing.html">提现</a>
                </div>
            </div>
        </div>
        <!-- 资产分布 2列 -->
        <div class="infobox">

            <div class="w50p">
                <div class="wraps left">
                    <p class="infotitle font-size12">证券</p>
                    <p class="font-gray3 font-size20">￥<fmt:formatNumber pattern="#0.00" type="number"
                                                                         value="${agreementMoney}"/></p>
                </div>
                <a class=" " href="/rest/wap/trade/?_t=<%=mills%>">
                    <div class="wraps right"><i class=" icon-more1 font-gray9"></i></div>
                </a>
            </div>


            <div class="w50p">
                <div class="wraps left">
                    <p class="infotitle font-size12">余额</p>
                    <p class="font-orange font-size20">￥<fmt:formatNumber type="number" pattern="#0.00"
                                                                          value="${userAccount.money}"/></p>
                </div>
                <a href="/rest/wap/personalcenter/flowing/?_t=<%=mills%>">
                    <div class="wraps right"><i class=" icon-more1 font-gray9"></i></div>
                </a>
            </div>

        </div>
    </div>

    <!--***************************合约************************* -->
    <!--***************************合约************************* -->
    <!--***************************合约************************* -->
    <c:choose>
        <c:when test="${agreementList!=null&&fn:length(agreementList)>0}">
            <c:forEach var="agreement" items="${agreementList}">
                <a href="/rest/wap/trade/tradeDetail/${agreement.showAgreementId}?_t=<%=mills%>">
                    <div class="contract-list " style="">
                        <div class="mytrade-title  ">${agreement.signAgreementTypeDesc}</div>
                        <div class="mytrade-data  ">
                            <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/>
                            <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate
                                pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate
                                pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate>
                        </div>
                        <div class="mytrade-cont">
                            <label class="<c:choose><c:when test="${agreement.diffMoney>=0}">current-data-rise</c:when><c:otherwise>current-data-fall</c:otherwise></c:choose>">
                            <span>当前额度：<span class="  ng-isolate-scope">：<fmt:formatNumber pattern="#0.00"
                                                                                           value="${agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney}"
                                                                                           type="number"></fmt:formatNumber></span></span>
                                <div class="current-quota">
                            <span class="fl">
                                <span class=""><c:choose><c:when
                                        test="${agreement.diffMoney>=0}">+</c:when><c:otherwise></c:otherwise></c:choose></span>
                                <span class="  ng-isolate-scope"><fmt:formatNumber pattern="#0.00" type="number"
                                                                                   value="${agreement.diffMoney}"></fmt:formatNumber></span>
                            </span>
                                    <span class="fr">
                                            <span class=""><c:choose><c:when test="${agreement.diffMoney>=0}">+</c:when><c:otherwise></c:otherwise></c:choose></span>
                                            <span class="  ng-isolate-scope"><fmt:formatNumber type="number"
                                                                                               value="${agreement.diffMoney*100/(agreement.stockCurrentPrice+agreement.availableCredit)}"
                                                                                               pattern="#0.00"/></span>%<span
                                            class=""><c:choose><c:when
                                            test="${agreement.diffMoney>=0}">↑</c:when><c:otherwise>↓</c:otherwise></c:choose></span>
                                        </span>
                                </div>
                            </label>
                            <label class="early-warn">亏损警戒线
                                <br>
                                <span class="font-orange fb"><span class="  ng-isolate-scope"><fmt:formatNumber
                                        type="number"
                                        value="${agreement.warningLineMoney}"
                                        pattern="#0.00"/></span></span></label>
                            <label class="stop-soss">亏损平仓线
                                <br>
                                <span class="font-red fb"><span class="  ng-isolate-scope"><fmt:formatNumber
                                        type="number"
                                        value="${agreement.closeLineMoney}"
                                        pattern="#0.00"/></span></span></label>
                            <label class="blocked-fund">风险保证金
                                <br>
                                <span class="font-blue fb"><span class="  ng-isolate-scope"><fmt:formatNumber
                                        type="number"
                                        pattern="#0.00"
                                        value="${agreement.lockMoney}"></fmt:formatNumber></span></span></label>
                        </div>
                    </div>
                </a>
            </c:forEach>
        </c:when>
    </c:choose>
</div>

<%--<jsp:include page="../../common/bottom_nav.jsp"/>--%>
</body>
</html>
