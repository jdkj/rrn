<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/1/23
  Time: 15:23
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<jsp:include page="../../common/head.jsp"/>
<title>用户中心-修改手机绑定</title>
<link rel="stylesheet" href="/rrn/wap.1.0/css/update.css">
<body>
<jsp:include page="../../common/top.jsp"/>

<section class=" ">
    <c:choose>
        <c:when test="${isModify}">
            <!-- 第一步 -->
            <div class="wrap step-one">
                <div class="module-spaceleft">
                    <ul class="list-haveline clearfix">
                        <li>
                            <span class="fb font-size14 font-navline">修改绑定手机</span>
                            <span class="fb font-size10 font-navline">1/2</span>
                        </li>
                        <li></li>
                    </ul>
                    <ul class="list-noline clearfix">
                        <li>
                            <span>原手机号码</span>
                        </li>
                        <li>
                            <span class="dianhua  ">${userInfo.phone}</span>
                            <input type="hidden" class="phone" value="${userInfo.phone}"/>
                        </li>
                    </ul>

                    <ul class="list-otherline clearfix">
                        <li>
				<span class="pic-yanzheng spaceleft">
				<i class="icon-duanxinyanzheng"></i>
			 </span>
                            <div class="line-top setting-input">
                                <input class="longbox     code " type="tel" placeholder="请输入手机验证码">
                                <button class="fr  req-code-btn">获取短信验证码</button>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="btn-full btn-orange next">下一步</div>
            </div>
        </c:when>
    </c:choose>

    <!-- 第二步？ -->
    <div class="wrap  step-two" <c:choose><c:when test="${isModify}">style="display:none"</c:when></c:choose>>
        <div class="module-noborder">
            <ul class="list-haveline clearfix">
                <c:choose>
                    <c:when test="${isModify}">
                        <li>
                            <span class="fb font-size14 font-navline">修改绑定手机</span>
                            <span class="fb font-size10 font-navline">2/2</span>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li>
                            <span class="fb font-size14 font-navline">绑定手机</span>
                            <span class="fb font-size10 font-navline">&nbsp;</span>
                        </li>
                    </c:otherwise>
                </c:choose>

                <li></li>
            </ul>
            <ul class="list-otherline clearfix" style="color: #CBCBD1;">
                <li>
				<span class="pic-yanzheng spaceleft">
				<i class="icon-shouji1"></i>
			 </span>
                    <div class="setting-input">
                        <input class="longbox     phone " type="tel" placeholder="请输入11位手机号码">
                    </div>
                </li>
            </ul>

            <ul class="bottomline list-otherline clearfix">
                <li>
				<span class="pic-yanzheng spaceleft">
				<i class="icon-duanxinyanzheng"></i>
			 </span>
                    <div class="line-top setting-input">
                        <input class="longbox  code    " type="tel" placeholder="请输入手机验证码">
                        <button class="fr  req-code-btn">获取短信验证码</button>
                    </div>
                </li>
            </ul>
        </div>
        <div class="btn-full btn-orange confirm">确认</div>
    </div>
</section>
<jsp:include page="../../common/bottom_nav.jsp"/>
<script>

    $(function(){
        //点击请求验证码
        $(".req-code-btn").on('click',this,function(){
            var phone = $(this).parent().parent().parent().parent().find(".phone").val();
            clickReqCodeBtn(phone,$(this));
        });
        //点击下一步
        $(".next").on("click",this,function(){
            var container = $(this).parent();
            var phone = container.find(".phone").val();
            var codes = container.find(".code").val();
            if(validPhoneNum(phone)&&validCode(codes)){
                validPermission(phone,codes);
            }
        });
        $(".confirm").on('click',this,function(){
            var container = $(this).parent();
            var phone = container.find(".phone").val();
            var codes = container.find(".code").val();
            if(validPhoneNum(phone)&&validCode(codes)){
                postModifyReq(phone,codes);
            }
        });
    })


    /**
     * 获取短信验证码
     * @param phone
     */
    function reqCode(phone){
        var data = {"phone":phone,"reqType":2};
        $.ajax({
            url:"/rest/user/reqCode",
            type:"post",
            data:data,
            dataType:"json"
        }).done(function(res){
            if(res.resultFlag){
                alert(res.data.codes);
            }
        }).fail(function(){
            toast("请求出错,稍后再试");
        })
    };

    /**
     * 验证权限 用于修改手机号
     * @param phone
     * @param code
     */
    function validPermission(phone,code){
        var data = {"phone":phone,"codes":code};
        $.ajax({
            url:"/rest/user/modify/phone/codeValidate",
            type:"post",
            dataType:"json",
            data:data
        }).done(function(res){
            if(res.resultFlag){
                $(".step-one").hide();
                $(".step-two").show();
            }else{
                toast(res.resultMsg);
            }
        }).fail(function(){
            toast("请求出错,稍后再试");
        })
    };
    /**
     * 点击获取短信验证码
     * @param phone
     * @param _this
     * @returns {boolean}
     */
    function clickReqCodeBtn(phone,_this){
        _this.prop('disabled',true);
        if(!validPhoneNum(phone)){
            _this.prop('disabled',false);
            return false;
        }
        var _count = 60;
        function countdown(){
            _count--;
            _this.text(_count+'秒后再重新获取...');
            if(_count===0){
                clearInterval(_timeHandler);
                _this.text('获取短信验证码');
                _this.prop('disabled',false);
            }
        }
        var _timeHandler = setInterval(countdown,1000);
        reqCode(phone);

    };
    /**
     * 验证手机号格式
     * @param phoneNo
     * @returns {boolean}
     */
    function validPhoneNum(phoneNo){
        if(/^1\d{10}$/gi.test(phoneNo)){
            return true;
        }else{
            toast("请输入正确的手机号");
            return false;
        }
    };
    /**
     * 校验短信随机码
     * @returns {boolean}
     */
    function validCode(code){
        if(!/^[\d]{5}$/gi.test(code)){
            toast("请输入正确的验证码");
            return false;
        }
        return true;
    };
    /**
     * 提交修改请求
     * @param phone
     * @param codes
     */
    function postModifyReq(phone,codes){
        var data={"phone":phone,"codes":codes};
        $.ajax({
            url:"/rest/user/modify/phone",
            type:"post",
            dataType:"json",
            data:data
        }).done(function(res){
            if(res.resultFlag){
                window.location.href = "/rest/wap/personalcenter/setting/";
            }else{
                toast(res.resultMsg);
            }
        }).fail(function(){
            toast("请求出错,请稍候再试");
        })
    }
</script>
</body>
</html>

