<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/1/20
  Time: 15:29
  Desc: head中通用参数与文件
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%

    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<meta charset="utf-8">
<base href="<%=basePath%>">
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="icon" href="favicon.ico" mce_href="favicon.ico" type="image/x-icon" />
<link rel="stylesheet" href="rrn/wap/2.0/css/main.css">
<link rel="stylesheet" href="rrn/wap/2.0/css/main9n.css">
<script src="rrn/wap/2.0/js/183min.js"></script>
<script src="rrn/wap/2.0/js/vbyzc.js"></script>
