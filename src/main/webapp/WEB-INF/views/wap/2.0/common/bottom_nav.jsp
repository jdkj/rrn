<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/1/20
  Time: 15:26
  Desc:底部导航
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    long mills = System.currentTimeMillis();
%>
<div class="footnavwrap">
    <nav class="bottom-nav clearfix">
        <a class="width50" > <i class="icon-caopan font-size24"></i> <br>我要操盘</a>
        <a href="/rest/wap/v2/personalcenter/?_t=<%=mills%>" class="width50"><i class="icon-geren font-size24"></i><br>我的中心</a>
    </nav>
</div>
