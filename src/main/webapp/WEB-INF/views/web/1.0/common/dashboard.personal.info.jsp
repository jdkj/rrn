<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/22
  Time: 23:14
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div class="person-info clearfix">
    <img src="<c:choose><c:when test="${not empty userInfo.userImage }">${userInfo.userImage}</c:when><c:otherwise><%=basePath%>/rrn/1.0/imgs/login_after.fw.png</c:otherwise></c:choose>" /><span class="account-num">${userInfo.nickName}</span>
    <div class="btn">
        <a href="/rest/fund/recharge/" class="button button1">充值</a><a style=" border: none;width: 10px;" class="button button2"></a>
        <c:choose>
            <c:when test="${bankAccount!=null}">
                <a href="/rest/fund/withdrawals/" class="button">提现</a>
            </c:when>
            <c:otherwise>
                <a href="javascript:alert('请先绑定银行卡')" class="button">提现</a>
            </c:otherwise>
        </c:choose>

    </div>
</div>
