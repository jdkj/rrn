<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/3
  Time: 23:52
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="mt_nav JS-tradeclient-nav">
    <!-- 要显示的对应的内容div的id,写在data-*属性里面 -->
    <a id="JS-keep-select" href="/rest/agreement/${agreementId}/position"  class="JS-nav">持仓</a>
    <a id="JS-buy-in"  href="/rest/trade/tradeClient/${agreementId}/buy" class="JS-nav">买入</a>
    <a id="JS-sold-out" href="/rest/trade/tradeClient/${agreementId}/sell" class="JS-nav">卖出</a>
    <a id="JS-cancel-menu" href="/rest/agreement/${agreementId}/operate/waiting"  class="JS-nav">撤单</a>
    <a id="JS-search" href="/rest/agreement/${agreementId}/query/currentdeals"  class="JS-nav">查询  </a>
</div>
