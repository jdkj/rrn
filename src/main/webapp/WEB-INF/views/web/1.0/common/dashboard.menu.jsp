<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/22
  Time: 21:24
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div data-type="0" class="sibar-nav">
    <a href="/rest/assets/" data-type="0"><i class="icon-yonghuyilan"></i><span>收益统计</span></a>
    <a href="/rest/trade/" data-type="1"><i class="icon-wodecaopan"></i><span>我的操盘</span></a>
    <%--<a href="/member/compet" data-type="3" class="no4"><i class="icon-jingji"></i><span>我的竞技</span></a>--%>
    <a href="/rest/coupon/" data-type="4"><i class="icon-jifen"></i><span>积分卡券</span></a>
    <a href="/rest/user/setting" data-type="5"><i class="icon-shezhi"></i><span>个人设置</span></a>
    <a href="/rest//assets/flowing/" data-type="6"><i class="icon-zijinmingxi"></i><span>资金明细</span></a>
    <c:choose>
        <c:when test="${distributionUser!=null&&distributionUser.accountLevel>-1}">
            <a href="/rest//page/share/" data-type="8" class="nav-extend"><i
                    class="icon-tuiguangzhuanqianxian"></i><span>赚外快</span></a>
        </c:when>
    </c:choose>

</div>
