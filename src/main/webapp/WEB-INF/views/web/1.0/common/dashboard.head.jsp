<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/22
  Time: 21:16
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<div class="common-head">
    <div class="head-nav-wrap clearfix"><a href="/" class="logo">
        <img src="<%=basePath%>/rrn/1.0/images-assets/logo.png" height="60" alt=""></a>
        <div class="nav-wrap">
            <c:choose>
                <c:when test="${userInfo==null}">
                    <a id="JS-head" href="/rest/page/login" class="nav">登录</a>
                    <a id="JS-head-operate" href="/rest//page/login"    class="nav">注册</a>
                </c:when>
                <c:otherwise>
                    <a id="JS-head" href="/" class="nav">首页</a>
                    <a id="JS-head-operate" href="/rest/trade/type/0"    class="nav">我要操盘</a>
                    <%--<a id="JS-head-compete"  href="/compet"  class="nav">我要竞技</a>--%>
                    <!--a.nav(href='/news') 资讯中心-->
                    <div class="user-photo dropdown clearfix"><img src="<c:choose><c:when test="${not empty userInfo.userImage }">${userInfo.userImage}</c:when><c:otherwise><%=basePath%>/rrn/1.0/imgs/login_after.fw.png</c:otherwise></c:choose>"
                                                                   style="cursor:pointer">
                        <ul class="niu-dropdown-menu">
                            <li><a href="/rest/assets/" rel="nofollow"><i class="icon-yonghuyilan"></i>账户一览</a></li>
                                <%--<li><a href="/member/coupon.html" rel="nofollow"><i class="icon-jifen"></i>积分卡券</a></li>--%>
                            <li><a href="/rest/user/setting" rel="nofollow"><i class="icon-shezhi"></i>个人设置</a></li>
                            <li><a href="/rest/assets/flowing/" rel="nofollow"><i class="icon-zijinmingxi"></i>资金明细</a></li>
                            <li><a href="/rest/user/logout" rel="nofollow" id="JSlogout"><i class="icon-yonghu"></i>退出</a></li>
                        </ul>
                    </div>
                </c:otherwise>
            </c:choose>

        </div>
    </div>
</div>
