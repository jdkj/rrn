<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/22
  Time: 19:26
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!--foot-->
<div class="foot">
    <div class="w1200">
        <!--左边的-->
        <div class="fl">
            <div class="navFoot mt10">
                <a href="#">关于我们</a> |
                <a href="#">加入人人牛</a> |
                <a href="#">联系我们</a>
            </div>
            <div class="color999 mt10">风险提示: 保护投资者利益是中国证监会工作的重中之重。<br>我们提醒您：股市有风险，投资需谨慎！市场风险莫测，务请谨慎行事！</div>
            <div class="colorfff mt10">版权所有 © 人人牛 闽ICP备10006454号-4</div>
        </div>
        <!--右边的-->
        <div class="qrcode fr">
            <img src="<%=basePath%>/rrn/1.0/images-assets/qrcode4rrn.jpg" alt="" style="width:100px;height:100px;"/>
            <div class="colorfff">关注微信公众号</div>
        </div>
        <div class="fr mt20" style="margin-right: 70px;;">
            <div class="fs16 color999">客服电话</div>
            <div class="fs30 fontbold colorfff">0592-5917777</div>
            <div class="fs16 color999">周一~周日 9:00 ~ 18:00</div>
        </div>
        <div class="cl"></div>
    </div>
</div>

<div id="myAlert" class="modal-body" style="display:none">
    <div class="content-wrap content-box3 context-align">
        <h3>提示</h3>
        <div id="JS-Alert-resultMsg" class="cont-word"></div>
        <button type="button" class="JSbtnclose" id="JS-Alert-btnclose">确认</button>
    </div>
</div>

<!--foot end-->
