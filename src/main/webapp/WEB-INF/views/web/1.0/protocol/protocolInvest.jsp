<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/11
  Time: 16:02
  Desc: 合格投资人申明
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/protocol.css"/>

    <title>合格投资人申明</title>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<div class="inner-page-content-wrap">
    <div class="contract_wrap"><h1>合格投资人申明</h1>
        <p class="para_blank">本人自愿申报成为合格投资人，并申明如下：
            本人申明在人人牛网注册并进行投资交易之前，本人已认真阅读并理解所有产品文件，本人不属于中国证监会认定的市场禁入者，包括上市公司董事、监事等高级管理人员及证券经营机构（包括分支机构）高级管理人员及内设业务部门负责人，证券登记、托管、清算机构高级管理人员及内设业务部门负责人，从事证券业务的律师，注册会计师以及资产评估人员，投资基金管理机构、投资基金托管机构的高级管理人员及其内设业务部门负责人，证券投资咨询机构的高级管理人员及其投资咨询人员以及中国证监会认定的其他人员。本人能够识别、判断和承担相关投资风险，并愿意依法承担相应的投资风险。</p>
        <p class="para_blank">本人申明成为合格投资人后，对交易涉及的事项保密，并依法缴纳投资活动所产生的各项税费。</p></div>
</div>
<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
</body>
</html>
