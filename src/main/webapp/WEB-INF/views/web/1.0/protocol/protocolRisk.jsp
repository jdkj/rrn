<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/11
  Time: 16:03
  Desc: 风险揭示书
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/protocol.css"/>

    <title>风险揭示书</title>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<div class="inner-page-content-wrap">
    <div class="contract_wrap"><h1>风险揭示书</h1>
        <h2>尊敬的投资顾问</h2>
        <p class="para_blank">
            为保护投资顾问的合法权益，本公司敬告各位投资顾问，应充分详细阅读本公司发布的与投顾操盘相关的协议和规则，根据自身的经济实力和心理承受能力，理性选择操盘方案，谨慎进行股票交易。</p>
        <p>
            一、MOM操盘是指私募基金委托投资顾问对本协议涉及的委托资产提供投资顾问服务。投资顾问承诺在遵守中国相关法律、法规的前提下，对私募基金提供的委托资金额度，在私募基金账户中进行投资顾问服务。私募基金向投资顾问提供本协议涉及委托资产的授权，授权投资顾问对这些资产在授权范围内进行操作，投资顾问按照一定比例提供相应的保证金，并向私募基金缴纳约定的综合管理费。投资顾问仅对分配的资金额度享有使用权，而不享有资金额度的最终控制权。</p>
        <p>二、投资顾问应当知晓，利用分成制或缴费制等合作方式进行股票交易可能会获得投资收益，但同时也存在着保证金损失的风险。除了会放大您的收益外，也会大幅放大您的交易风险。</p>
        <p>三、投资顾问操盘过程包括网上注册、网上签约、网上交易等互联网操作行为，可能会导致投资顾问发生损失,这些损失都是由投资顾问自己承担的。</p>
        <p>上述风险揭示，并不能揭示从事MOM操盘业务的全部风险。您务必对此有清醒的认识，认真考虑是否进行MOM操盘业务。</p>
        <p>以上《风险揭示书》本人已阅读并完全理解，愿意承担MOM操盘过程中的各种风险。</p></div>
</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
</body>
</html>
