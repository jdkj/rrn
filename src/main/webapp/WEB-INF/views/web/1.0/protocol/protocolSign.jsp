<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/11
  Time: 16:03
  Desc: 服务协议
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/protocol.css"/>

    <title>风险揭示书</title>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<div class="inner-page-content-wrap">
    <div class="contract_wrap"><h1>服务协议<br></h1>
        <p class="para_blank">为保障投资者权益，注册会员（投资者）在自愿注册使用人人牛网投资服务前，必须仔细阅读并充分理解知悉下述协议所有条款。</p>
        <h2>第一条：释义</h2>
        <p class="para_blank">1.1
            人人牛网（http://www.rrniu.cn）：由竞东(厦门)科技有限公司开发并管理的提供专业投资理财服务的网站平台。</p>
        <p class="para_blank">1.2 注册会员：在人人牛网实名注册，具有完全民事权利能力和行为能力的自然人并获得唯一网站账户信息的投资者。</p>
        <p class="para_blank">1.3 会员服务：通过人人牛网平台向注册会员提供的服务，具体服务内容包括：证券交易信息发布、交易管理服务、客户服务等。</p>
        <h2>第二条：会员须知</h2>
        <p class="para_blank">
            2.1
            你在人人牛网平台进行注册即可成为人人牛网注册会员，同时网站为你生成注册会员账户，该账户将记载注册会员在人人牛网平台发生的一切交易活动。注册会员应妥善保管好账户名称及密码等信息，该账户为注册会员在人人牛网平台进行交易的唯一有效的身份识别。会员注册成功后，不得将本网站的账户转让给第三方或者授权给第三方使用。人人牛网站通过会员注册的账户及密码来识别会员的指令。你确认，使用你的会员账户和密码登录本网站后在本网站的一切行为均代表你本人意志。使用你的会员账户和密码登陆操作所产生的电子信息记录均为你行为的有效凭据，并由你承担相应的法律后果。</p>
        <p class="para_blank">
            你有义务在注册时提供自己的真实、最新、完整的资料，并保证诸如电子邮件地址、联系电话、联系地址、邮政编码等内容的有效性及安全性。你有义务维持并立即更新你的注册资料，确保其为真实、最新、有效及完整。若你提供任何错误、虚假、过时或不完整的资料，或者本网站有合理的理由怀疑资料为错误、虚假、过时或不完整，本网站有权暂停或终止你的账户，并拒绝你使用本网站服务的部份或全部功能。在此情况下，本网站不承担任何责任，并且你同意自行负担因此所产生的直接或间接的任何支出或损失。</p>
        <p class="para_blank">2.2
            本服务协议的内容包括本协议的现有所有条款及将来发布的修改或变更条款。人人牛网有权根据实际情况不定时修改本服务协议及网站的相关内容并在网站以公告的形式发布，无需另行单独通知各会员。各会员需及时关注网站公告，各会员在服务协议修改或变更后若继续使用网站服务的，则表明会员已阅读、理解并接受修改或变更后的协议内容；若会员不接受修改或变更后的协议内容，则应立即停止使用网站服务，否则视为同意并接受。</p>
        <p class="para_blank">2.3
            注册会员理解并同意，在注册时有义务提供真实、有效、完整的资料，并保证联系方式等内容的有效性、安全性。上述信息发生任何变化，应及时在账户中予以更新，以便人人牛网为注册会员提供良好的服务并与注册会员进行即时有效的联系。若因上述联系方式错误而发生无法与注册会员取得联系而导致注册会员在使用本服务过程中遭受的任何损失由注册会员自行承担。</p>
        <p class="para_blank">2.4 注册会员承诺，其通过人人牛网平台进行交易的资金来源合法，否则人人牛网有权暂停或终止服务，并且由此产生的损失由注册会员自行承担。</p>
        <p class="para_blank">2.5
            注册会员确认，注册会员在人人牛网平台上按人人牛网服务流程所确认的交易结果，将成为人人牛网为注册会员进行相关交易或操作（包括但不限于支付或收取款项、冻结资金、订立合同等）的不可撤销的指令。注册会员同意，相关交易结果的执行时间以人人牛网系统中注册会员实际操作的时间为准。注册会员同意，人人牛网有权依据本协议及人人牛网其余相关协议规则，单方对相关事项进行操作。</p>
        <p class="para_blank">2.6
            注册会员理解并同意，人人牛网并非银行或金融机构，根据中华人民共和国相关法律规定，人人牛网不提供“即时”资金转账服务，下述3.3.2及3.3.3中充值、取现等服务均涉及人人牛网与银行、第三方支付机构等第三方机构合作，资金到账的时间因发卡行不同而存在差异，人人牛网对资金到帐的延迟不承担任何责任，包括但不限于由此产生的利息、货币贬值等损失。</p>
        <p class="para_blank">2.7 注册会员理解并同意，注册会员所有认证银行卡相关信息的服务由该银行卡发卡行提供，有关银行卡及账户的所有信息需向发卡行查证。</p>
        <p class="para_blank">2.8 在本网站提供的交易活动中,你无权要求本网站提供其他用户的个人资料,除非符合以下条件：</p>
        <p class="para_blank">（1）司法机关或政府部门根据法律法规要求本网站提供；</p>
        <p class="para_blank">（2）接受你借款的借款人逾期未归还借款本息，且本网站根据自己的判断同意披露的；</p>
        <p class="para_blank">2.9
            注册会员欲终止与人人牛网的服务协议时，应向人人牛网提出注销账户的申请，清偿所有保证金、利息、服务费、管理费等应付款项并经本网站审核同意后方可注销。在服务期间内，注册会员丧失全部或部分民事权利能力或行为能力的，人人牛网有权根据有效的司法文书或其监护人的指示处置其账户；注册会员死亡或被宣告死亡、被宣告失踪的，其权利义务由其继承人承担。</p>
        <p class="para_blank">2.10
            注册会员理解并同意，一经注册及使用人人牛网服务即视为对本协议所有条款的充分理解和接受，注册会员应完全履行本协议项下确定的所有义务，如有违反并导致任何的任何法律后果或人人牛网、第三方的任何损失由注册会员承担。人人牛网保留向注册会员追究违约责任的权利。</p>
        <h2>第三条：服务内容</h2>
        <p class="para_blank">3.1
            交易信息发布：人人牛网为注册会员提供交易信息。注册会员充分了解，人人牛网提供的该等信息仅供参考，并非代表人人牛网的任何推荐及指导，不作为相关操作或交易的证据或依据，注册会员据该等信息所进行的一切交易，产生的盈亏风险由注册会员自行承担。</p>
        <p class="para_blank">3.2 交易管理服务：人人牛网将为注册会员提供以下交易管理服务：
            如：支付或收取款项、冻结资金、订立合同等</p>
        <p class="para_blank">3.3 人人牛网将为注册会员提供以下客户服务：</p>
        <p class="para_blank">• 3.3.1
            银行卡认证：注册会员应按照人人牛网平台规定的流程（建议将该流程作为附件附后，或做个网页链接在后面）提交注册会员本人名下有效银行借记卡等信息，经由人人牛网审核通过后，人人牛网将注册会员的账户与前述银行账户进行绑定。如注册会员未按照人人牛网规定按时提交相关信息或提交的信息不完整的或人人牛网有合理理由怀疑提交的信息可能存在错误、虚假情况的，人人牛网有权拒绝为该注册会员提供银行卡认证服务并要求注册会员于规定时间内补正，若注册会员未及时补正而造成的未能使用充值、取现等其他服务，由此造成的一切损失由注册会员自行承担。</p>
        <p class="para_blank">• 3.3.2 充值：注册会员可以使用人人牛网指定的方式向会员账户充入资金，用于人人牛网平台内账户交易。</p>
        <p class="para_blank">• 3.3.3
            取现：注册会员可以通过人人牛网平台的取现功能将注册会员账户中的资金转入经过3.3.1中经过认证的银行卡中。人人牛网将于注册会员的前述操作后，在一个工作日内通过第三方机构将相应的款项汇入3.3.1中注册会员经过认证的银行卡账户。</p>
        <p class="para_blank">• 3.3.4 咨询：人人牛网对注册会员在人人牛网平台所有操作进行记录，注册会员可以通过会员账户实时查询会员账户名下的交易记录。</p>
        <h2>第四条：免责声明</h2>
        <p class="para_blank">
            4.1
            人人牛网对因注册会员的过错（包括但不限于：决策失误、操作不当、遗忘或泄露密码、密码被他人破解、注册会员使用的计算机系统被第三方侵入、注册会员委托他人代理交易时、他人恶意或不当操作等）导致的任何损失，不承担任何法律责任。</p>
        <p class="para_blank">4.2
            若人人牛网发现因系统故障或其他任何原因导致的系统处理错误，无论该处理错误导致的结果有利于人人牛网还是有利于注册会员，人人牛网都有权立即单方纠正错误。注册会员理解并同意，若该等错误导致注册会员实际收到的款项多于应获得的金额，则无论错误的性质和原因为何，注册会员应根据人人牛网向注册会员发生的有关纠正错误的通知的具体要求返还多收的款项或进行其他操作。</p>
        <p class="para_blank">4.3
            注册会员理解并同意，发生下述情形时，人人牛网有权基于单方独立判断而不经过通知而单方先行暂停、中断或终止向注册会员提供本协议下的全部或部分会员服务，并将会员账户移除或注销且无需对注册会员或任何第三方承担任何责任。前述情形包括但不限于：</p>
        <p class="para_blank">• 1、注册会员提供的个人资料存在不真实、不完整或无效的情形；</p>
        <p class="para_blank">• 2、人人牛网发现会员账户存在异常交易或违法交易的情形；</p>
        <p class="para_blank">• 3、人人牛网有合理理由怀疑注册会员账户涉嫌洗钱、套现、传销、被冒用或其他人人牛网认为账户存在风险的情形；</p>
        <p class="para_blank">• 4、注册会员账户已连续三年未实际使用且账户中余额为零；</p>
        <p class="para_blank">• 5、其他人人牛网基于交易安全等原因，根据其单独判断需先行暂停、中断或终止向注册会员提供本协议的全部或部分会员服务的情形。</p>
        <p class="para_blank">4.4
            由于互联网的特殊性，在系统无法正常运作（包括但不限于因台风、地震、停电等不可抗力，黑客攻击、网络故障，电信设备故障导致信息传递延误，网站维护等原因）致使用户不能使用人人牛网服务或服务受到影响，人人牛网不承担责任。</p>
        <p class="para_blank">4.5
            注册会员理解并同意，因网络系统、第三方支付服务，银行转账服务等发生异常，无法正常运作（包括但不限于因台风、地震、停电等不可抗力，黑客攻击、网络故障，电信设备故障导致信息传递延误，网站维护等原因）导致充值、转账、取现等操作受到影响，人人牛网不承担责任。</p>
        <p class="para_blank">4.6 注册会员理解并同意，因宏观经济调控、法律法规政策变化、市场利率引起的价格波动致使会员遭受损失，人人牛网不承担损失赔偿责任。</p>
        <h2>第五条： 通知</h2>
        <p class="para_blank">
            本协议条款及任何其他的协议、告示或其他关于注册会员使用本服务账户及服务的通知，你同意本网站使用电子方式通知你。电子方式包括但不限于以电子邮件方式、或于本网站或者合作网站上公布、或短信等无线通讯装置通知等方式。你同意，本网站以电子方式发出前述通知之日视为通知已送达。因信息传输等原因导致你未在前述通知发出当日收到该等通知的，本网站不承担责任。</p>
        <h2>第六条： 法律适用及争议解决</h2>
        <p class="para_blank">6.1 本协议适用中华人民共和国法律。</p>
        <p class="para_blank">6.2 因履行本协议发生的一切争议，双方应协商解决；协商不成的，任一方均可向竞东(厦门)科技有限公司住所地人民法院提起诉讼。</p>
        <h2>第七条： 其他</h2>
        <p class="para_blank">7.1 本协议是由注册会员与人人牛网共同签订的，适用于注册会员在人人牛网的全部操作。</p>
        <p class="para_blank">7.2 人人牛网、竞东(厦门)科技有限公司对本服务协议拥有最终解释权。</p></div>
</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
</body>
</html>

