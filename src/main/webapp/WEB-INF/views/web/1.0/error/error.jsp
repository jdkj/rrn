<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/17
  Time: 11:13
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>

<!--[if !IE]><!-->
<html>

<head>
    <base href="<%=basePath%>">
    <meta charset="utf-8" />
    <title>出错啦</title>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body >
<jsp:include page="../common/dashboard.head.jsp" flush="false"></jsp:include>
<div class="ui-view-wrap ">

    <div class="inner-page-content-wrap clearfix">
        <div class="error-left"> </div>
        <div class="error-right">
            <div class="title">出错啦！</div>程序猿们在加急处理中
            <br>我们正在鞭打他 ...
            <div class="cont">您可以通过以下方式继续访问</div><a href="/">返回首页</a></div>
    </div>

</div>
<jsp:include page="../common/foot.jsp" flush="false"></jsp:include>
</body>
<!-- END BODY -->
</html>
