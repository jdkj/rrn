<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/5
  Time: 21:44
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/recharge.css">
    <title>添加银行卡</title>

</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<!--主要内容-->


<div class="ui-view-wrap">
    <div ng-controller="AddbankController" class="inner-page-content-wrap">
        <ul class="tab_public clearfix">
            <li>添加银行卡</li>
        </ul>
        <div class="card_admin">
            <div class="cont-box addbank-wrap">
                <div class="cont-left open-name"></div>
                <div class="cont-right">请选择常用的银行卡，绑定成功后提现只能在该银行卡上进行！</div>
            </div>
            <div class="cont-box">
                <div class="cont-left open-name">开户名</div>
                <div class="cont-right"><span class="color-black6 font-size-16 font-bold"><c:choose><c:when
                        test="${userInfo.userParamInfoTags.idTag}">${userIdAuth.idName}</c:when></c:choose></span></div>
            </div>
            <div class="cont-box">
                <div class="cont-left open-bank">开户银行</div>
                <div class="cont-right">
                    <div class="add_rno1">
                        <select class="recharge_input JSBankId">
                            <option value="0">请选择开户银行</option>
                            <c:choose>
                                <c:when test="${bankList!=null&&fn:length(bankList)>0}">
                                    <c:forEach var="bank" items="${bankList}">
                                        <option value="${bank.bankId}">${bank.bankName}</option>
                                    </c:forEach>
                                </c:when>
                            </c:choose>

                        </select>
                    </div>
                </div>
            </div>
            <div class="cont-box">
                <div class="cont-box">
                    <div class="cont-left">银行卡号</div>
                    <div class="cont-right">
                        <div class="add_rno">
                            <input name="cardNo" type="text" placeholder="银行卡号" class="recharge_text">
                        </div>
                    </div>
                </div>
                <div class="cont-box">
                    <div class="cont-left">确认卡号</div>
                    <div class="cont-right">
                        <div class="add_rno">
                            <input name="cardNo2" type="text" placeholder="确认银行卡号" class="recharge_text">
                        </div>
                        <div class="add-box">
                            <button type="button" class="btn-standard btn-blue JSaddBanksubmit">确认添加</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        <c:choose>
        <c:when test="${!userInfo.userParamInfoTags.idTag}">
        alert("请先进行实名认证");
        window.location.href = "/rest/user/setting";
        </c:when>
        <c:when test="${userInfo.userParamInfoTags.bankTag}">
        alert("您已绑定了银行卡信息，无需重新绑定");
        window.location.href = "/rest/user/setting";
        </c:when>
        <c:otherwise>

                $(".JSaddBanksubmit").click(function(){
                    if(validAddBankValue()){
                        var item = $(this);
                        item.attr("disabled","disable");
                        var bankId = +$(".JSBankId").val();
                        var cardNo = $("input[name=cardNo]").val();
                        var data = {"bankId":bankId,"cardNo":cardNo};
                        $.post("/rest/user/addBank",data,function(res){
                            item.removeAttr("disabled");
                            if(res.resultFlag){
                                alert("添加成功");
                                window.location.href="/rest/user/setting";
                            }else{
                                alert("添加失败");
                            }
                        },"json");
                    }
                });



                function validAddBankValue(){
                    var bankId = +$(".JSBankId").val();
                    var cardNo = $("input[name=cardNo]").val();
                    var cardNo2 = $("input[name=cardNo2]").val();
                    if(bankId===0){
                        alert("请选择开户银行");
                        return false;
                    }
                    if(cardNo===""){
                        alert("请输入银行卡号");
                        return false;
                    }
                    if(cardNo!=cardNo2){
                        alert("两次卡号填写不一致");
                        return false;
                    }
                    return true;
                }


        </c:otherwise>
        </c:choose>
    })
</script>

</body>
</html>

