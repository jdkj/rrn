<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/5
  Time: 11:51
  Desc: 资金明细---具体类目
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/personCenter.css"/>

    <title>资金明细</title>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp"></jsp:include>

<div class="ui-view-wrap">
    <div class="inner-page-content-wrap clearfix">
        <jsp:include page="../common/dashboard.menu.jsp"></jsp:include>
        <div class="person-content">
            <jsp:include page="../common/dashboard.personal.info.jsp" flush="true"></jsp:include>
            <div class="assest-wrapper">
                <div class="ass-title">资金明细</div>
                <div class="ass-index">
                    <ul class="ass-type clearfix">
                        <li class="first-title">类型：</li>
                        <li id="all" class=""><a href="/rest/assets/flowing/">全部</a></li>
                        <li id="2" data-iotype="all" class="<c:choose><c:when test="${changeType==0}">ass-check</c:when></c:choose>"><a href="/rest/assets/flowing/c0/">充值提款</a></li>
                        <li id="3" data-iotype="all" class="<c:choose><c:when test="${changeType==1}">ass-check</c:when></c:choose>"><a href="/rest/assets/flowing/c1/">借款明细</a></li>
                        <li id="4" data-iotype="all" class="<c:choose><c:when test="${changeType==2}">ass-check</c:when></c:choose>"><a href="/rest/assets/flowing/c2/">服务费明细</a></li>
                        <li id="5" data-iotype="all" class="<c:choose><c:when test="${changeType==3}">ass-check</c:when></c:choose>"><a href="/rest/assets/flowing/c3/">理财明细</a></li>
                        <li id="1" data-iotype="201009" class="<c:choose><c:when test="${changeType==4}">ass-check</c:when></c:choose>"><a href="/rest/assets/flowing/c4/">利润提取</a></li>
                    </ul>
                </div>

                <c:choose>
                    <c:when test="${pageResult!=null&&fn:length(pageResult.result)>0}">
                        <table class="JS-iolist-page ass-table">
                            <tr>
                                <th class="t-time">时间</th>
                                <th class="t-id">交易号</th>
                                <th class="t-type">类型</th>
                                <th class="t-money">金额</th>
                                <th class="t-balance">余额</th>
                            </tr>
                            <tbody>
                            <c:forEach var="accountMx" items="${pageResult.result}">
                                <tr>
                                    <td class="td-time">${accountMx.updateTime}</td>
                                    <td class="td-id">${accountMx.orderId}</td>
                                    <td class="td-type">${accountMx.changeDesc}</td>
                                    <td class="td-money"><fmt:formatNumber type="number" pattern="#0.00" value="${accountMx.changeMoney}"></fmt:formatNumber></td>
                                    <td class="td-balance"><fmt:formatNumber type="number" pattern="#0.00" value="${accountMx.totalMoney}"></fmt:formatNumber></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <div class="page-middel clearfix">
                            <ul id="pagination-flowing" class="pagination-sm pagination">
                                <c:choose>
                                    <c:when test="${pageResult.totalPages>1}">
                                        <c:choose>
                                            <c:when test="${pageResult.pageNo>1}">
                                                <li class="first"><a href="/rest/assets/flowing/c${changeType}/1">首页</a></li>
                                                <li class="prev "><a href="/rest/assets/flowing/c${changeType}/${pageResult.pageNo-1}">上一页</a>
                                                </li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                                <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                            <c:choose>
                                                <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                                    <li class="page">
                                                        <a href="/rest/assets/flowing/c${changeType}/${pageIndex}">${pageIndex}</a>
                                                    </li>
                                                </c:when>
                                                <c:when test="${pageResult.pageNo==pageIndex}">
                                                    <li class="page active">
                                                        <a href="javascript:void(0)">${pageIndex}</a>
                                                    </li>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                        <c:choose>
                                            <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                                <li class="next "><a href="/rest/assets/flowing/c${changeType}/${pageResult.pageNo+1}">下一页</a>
                                                </li>
                                                <li class="last "><a href="/rest/assets/flowing/c${changeType}/${pageResult.totalPages}">尾页</a>
                                                </li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                                <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                        <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                        <li class="page active"><a href="javascript:void(0)">1</a></li>
                                        <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                        <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                    </c:otherwise>
                                </c:choose>

                            </ul>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <table class="JS-iolist-page ass-table">
                            <tr>
                                <th class="t-time">时间</th>
                                <th class="t-id">交易号</th>
                                <th class="t-type">类型</th>
                                <th class="t-money">金额</th>
                                <th class="t-balance">余额</th>
                            </tr>
                        </table>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
</html>
