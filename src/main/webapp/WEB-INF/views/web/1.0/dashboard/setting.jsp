<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/22
  Time: 23:08
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/setting-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/personCenter.css"/>
    <style>
        /*Dsmar 工具类样式*/
        #ds-tool-toast{
            width:100%;
            min-height:30px;
            padding:3px;
            background:rgba(255,0,0,.6);
            position:absolute;
            left:0;
            bottom:25px;
            z-index:666666;
            text-align:center;
            color:#fff;
            border-radius:20px;
            font-size:13px;
            line-height:30px
        }

    </style>
    <title>个人设置</title>
</head>
<body>

<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>

<!--主要内容-->
<div class="ui-view-wrap">
    <div class="inner-page-content-wrap clearfix">
        <jsp:include page="../common/dashboard.menu.jsp" flush="true"></jsp:include>
        <div class="person-content">
            <jsp:include page="../common/dashboard.personal.info.jsp" flush="true"></jsp:include>
            <div class="setting">
                <div class="set-title">安全设置</div>
                <table class="set-table">
                    <tbody>
                    <tr>
                        <td class="set-name"><i class="icon-yonghu"> </i>昵称</td>
                        <td class="set-value">${userInfo.nickName}</td>
                        <td class="set-update"></td>
                    </tr>
                    <tr>
                        <td class="set-name"><i class="icon-shimingrenzheng"> </i>实名认证</td>
                        <c:choose>
                            <c:when test="${userInfo.userParamInfoTags.idTag}">
                                <td class="set-value">${userIdAuth.idName}</td>
                                <td class="set update">&nbsp;</td>
                            </c:when>
                            <c:otherwise>
                                <td class="set-value">未认证</td>
                                <td class="set update"><a class="JSidentification">认证</a></td>
                            </c:otherwise>
                        </c:choose>

                    </tr>
                    <tr>
                        <td class="set-name"><i class="icon-yinxingleizhifu"> </i>银行卡绑定</td>
                        <c:choose>
                            <c:when test="${userInfo.userParamInfoTags.bankTag}">
                                <td class="set-value">${bankAccount.bankName}(${bankAccount.modifiedCardNo})</td>
                                <td class="set-update">&nbsp;</td>
                            </c:when>
                            <c:otherwise>
                                <td class="set-value">未绑定</td>
                                <td class="set-update">
                                    <c:choose>
                                        <c:when test="${userInfo.userParamInfoTags.idTag}">
                                            <a href="/rest/user/addbank" data-realname="false">绑定</a>
                                        </c:when>
                                        <c:otherwise>
                                            <a href="javascript:void(0)" data-realname="false" class="JSaddBank">绑定</a>
                                        </c:otherwise>
                                    </c:choose>

                                </td>
                            </c:otherwise>
                        </c:choose>


                    </tr>
                    <tr>
                        <td class="set-name"><i class="icon-shouji1"> </i>手机绑定</td>
                        <td class="set-value">${userInfo.phone}</td>
                        <td class="set-update"><a class="JSmodifyTel">修改</a></td>
                    </tr>
                    <tr>
                        <td class="set-name"><i class="icon-mima1"> </i>登录密码</td>
                        <c:choose>
                            <c:when test="${userInfo.userParamInfoTags.passwordTag}">
                                <td class="set-value">已设置</td>
                                <td class="set-update"><a class="JSmodifyPwd">修改</a></td>
                            </c:when>
                            <c:otherwise>
                                <td class="set-value">未设置</td>
                                <td class="set-update"><a class="JSsetPwd">设置</a></td>
                            </c:otherwise>
                        </c:choose>

                    </tr>
                    <tr>
                        <td class="set-name"><i class="icon-zhifumima"> </i>提款密码</td>
                        <c:choose>
                            <c:when test="${userInfo.userParamInfoTags.accountPasswordTag}">
                                <td class="set-value">已设置</td>
                                <td class="set-update"><a class="JSmodifyDrawPwd">修改</a></td>
                            </c:when>
                            <c:otherwise>
                                <td class="set-value">未设置</td>
                                <td class="set-update"><a class="JSsetDrawPwd">设置</a></td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div id="modifytelphoneDialog" style="display:none" class="modal-body">
    <div class="content-wrap content-box3">
        <h3>修改绑定手机</h3>
        <form>
            <p data-tel="13959795667" class="old-tel">原手机号码<span>139****5667</span></p>
            <div style="display:none" class="modal-error phone-error"><i class="icon-jinggao"></i><span></span></div>
            <span class="clearfix"></span>
            <label for="verifyCode" class="has-ivc"><i class="icon-duanxinyanzheng"></i>
                <input name="verifyCode" id="verifyCode" placeholder="手机验证码">
            </label>
            <button type="button" class="button-ivc JSgetSMSCode">获取短信验证码</button><span class="clearfix"></span>
            <button type="button" class="JScheckSMSCode">下一步</button>
        </form>
    </div>
</div>

<c:choose>
    <c:when test="${userInfo.userParamInfoTags.passwordTag}">
        <div id="modifyPwdDialog" style="display:none" class="modal-body">
            <div class="content-wrap content-box3">
                <h3>修改登录密码</h3>
                <form>
                    <label for="oldPwd" class="have-ts"><i class="icon-mima1"></i>
                        <input id="oldPwd" type="password" name="oldPwd" placeholder="原登录密码">
                    </label>
                    <label for="pwd"><i class="icon-mima1"></i>
                        <input id="pwd" type="password" name="pwd" placeholder="新登录密码">
                    </label>
                    <label for="pwd2"><i class="icon-mima1"></i>
                        <input id="pwd2" type="password" name="pwd2" placeholder="确认新登录密码">
                    </label>
                    <button type="button" class="JSbtnsubmit">确认</button>
                    <div style="display:none" class="modal-error"><i class="icon-jinggao"></i><span></span></div>
                </form>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div id="setPwdDialog" style="display:none" class="modal-body">
            <div class="content-wrap content-box3">
                <h3>设置登录密码</h3>
                <form>
                    <label for="pwd"><i class="icon-mima1"></i>
                        <input id="pwd" type="password" name="pwd" placeholder="登录密码">
                    </label>
                    <label for="pwd2"><i class="icon-mima1"></i>
                        <input id="pwd2" type="password" name="pwd2" placeholder="确认登录密码">
                    </label>
                    <button type="button" class="JSbtnsubmit">确认</button>
                    <div style="display:none" class="modal-error"><i class="icon-jinggao"></i><span></span></div>
                </form>
            </div>
        </div>
    </c:otherwise>
</c:choose>


<c:choose>
    <c:when test="${userInfo.userParamInfoTags.accountPasswordTag}">
        <div id="modifyDrawPwdDialog" style="display:none" class="modal-body">
            <div class="content-wrap content-box3">
                <h3>修改提款密码</h3>
                <form>
                    <label for="oldDrawPwd" class="have-ts"><i class="icon-mima1"></i>
                        <input id="oldDrawPwd" type="password" name="oldDrawPwd" placeholder="原提款密码">
                    </label>
                    <label for="drawpwd"><i class="icon-mima1"></i>
                        <input id="drawpwd" type="password" name="drawpwd" placeholder="新提款密码">
                    </label>
                    <label for="drawpwd2"><i class="icon-mima1"></i>
                        <input id="drawpwd2" type="password" name="drawpwd2" placeholder="确认新提款密码">
                    </label>
                    <button type="button" class="JSbtnsubmit">确认</button>
                    <div style="display:none" class="modal-error"><i class="icon-jinggao"></i><span></span></div>
                </form>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <div id="setDrawPwdDialog" style="display:none" class="modal-body">
            <div class="content-wrap content-box3">
                <h3>设置提款密码</h3>
                <form>
                    <label for="drawpwd"><i class="icon-mima1"></i>
                        <input id="drawpwd" type="password" name="drawpwd" placeholder="新提款密码">
                    </label>
                    <label for="drawpwd2"><i class="icon-mima1"></i>
                        <input id="drawpwd2" type="password" name="drawpwd2" placeholder="确认新提款密码">
                    </label>
                    <button type="button" class="JSbtnsubmit">确认</button>
                    <div style="display:none" class="modal-error"><i class="icon-jinggao"></i><span></span></div>
                </form>
            </div>
        </div>
    </c:otherwise>
</c:choose>

<c:choose>
    <c:when test="${userInfo.userParamInfoTags.idTag}">
    </c:when>
    <c:otherwise>
        <div id="authenticationDialog" style="display:none" class="modal-body">
            <div class="content-wrap content-box3">
                <h3>实名认证</h3>
                <div>
                    <label for="realName" class="have-ts"><i class="icon-yonghu"></i>
                        <input id="realName" type="text" name="realName" placeholder="请输入您的真实姓名">
                    </label>
                    <label for="idCard"><i class="icon-shimingrenzheng"></i>
                        <input id="idCard" type="text" name="idCard" placeholder="请输入您的18位身份证号">
                    </label>
                    <button type="button" class="JSbtnsubmit">确认</button>
                    <div style="display:none" class="modal-error"><i class="icon-jinggao"></i><span></span></div>
                </div>
            </div>
        </div>
    </c:otherwise>
</c:choose>



<div id="addBankDialog" style="display:none" class="modal-body">
    <div class="content-wrap content-box3">
        <h3>提示</h3>
        <div class="cont-word">为了您的资金安全，请先进行实名认证</div>
        <button type="button" class="JSbtnsubmit">确认</button>
    </div>
</div>

<input type="hidden" id="settingType" value="0"/>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
<script>


    /**
     * toast
     * @param msg
     */
    function toast(msg){
        if($("#ds-tool-toast").length>0){
            $("#ds-tool-toast").remove();
        }
        var toastHtml = "<div id='ds-tool-toast'>"+msg+"</div>";
        $("body").append(toastHtml);
        setTimeout(function(){
            $("#ds-tool-toast").remove();
        },2000);
    };


    function alert(msg){
       toast(msg);
    }

    $(function(){

        $(".JSbtnsubmit").click(function(){
            var settingType = +$("#settingType").val();
            switch(settingType){
                case 0:
                default:
                    vbyzctools.hide($("#addBankDialog"));
                    break;
                case 1://身份认证
                     if(validIdentification()){
                         var realName = $("#realName").val();
                         var idCard = $("#idCard").val();
                         var data = {"idNo":idCard,"idName":realName};
                         $.post("/rest/user/authId",data,function(res){
                             if(res.resultFlag){
                                 alert("认证成功");
                                 window.location.href = window.location.href;
                             }else{
                                 alert(res.resultMsg);
                                 return false;
                             }
                         },"json");
                     }
                    break;
                case 2://绑定手机号
                    break;
                case 3://修改手机号
                    break;
                case 4://设置或修改密码
                case 5:
                    if(validPassword()){
                        var oldPassword = $("#oldPwd").val();
                        var pwd = $("#pwd").val();
                        var data = {"orginalPassword":oldPassword,"password":pwd};
                        $.post("/rest/user/modify/password",data,function(res){
                            if(res.resultFlag){
                                alert(res.resultMsg);
                                window.location.href = window.location.href;
                            }else{
                                alert(res.resultMsg);
                                return false;
                            }
                        },"json");
                    }
                    break;
                case 6://设置或修改提款密码
                case 7:
                    if(validAccountPassword()){
                        var oldDrawPwd = $("#oldDrawPwd").val();
                        var drawPwd = $("#drawpwd").val();
                        var data = {"orginalAccountPassword":oldDrawPwd,"accountPassword":drawPwd};
                        $.post("/rest/user/modify/accountpassword",data,function(res){
                            if(res.resultFlag){
                                alert(res.resultMsg);
                                window.location.href = window.location.href;
                            }else{
                                alert(res.resultMsg);
                                return false;
                            }
                        },"json");
                    }
                    break;



            }
        });

    });

    /**
     * 验证身份信息
     * @returns {boolean}
     */
    function validIdentification(){
        var realName = $("#realName").val();
        var idCard = $("#idCard").val();
        if(realName!=''&&idCard!=''&&idCard.length===18){
            return true;
        }
        alert("请输入正确的身份信息");
        return false;
    }

    /**
     * 验证密码修改/设置 信息
     * @returns {boolean}
     */
    function validPassword(){
        var settingType = +$("#settingType").val();
        var oldPassword = $("#oldPwd").val();
        var pwd = $("#pwd").val();
        var pwd2 = $("#pwd2").val();

         if(settingType===5){
             if(oldPassword===""){
                 alert("请输入原密码");
                 return false;
             }
         }

         if(pwd===""){
             alert("请输入新密码");
             return false;
         }

        if(pwd.length<6){
            alert("密码长度不能少于6位")
            return false;
        }

         if(pwd!=pwd2){
             alert("两次密码输入不一致");
             return false;
         }

        return true;
    }

    /**
     * 验证交易密码设置/修改 信息
     * @returns {boolean}
     */
    function validAccountPassword(){
        var settingType = +$("#settingType").val();
        var oldDrawPwd = $("#oldDrawPwd").val();
        var drawPwd = $("#drawpwd").val();
        var drawPwd2 = $("#drawpwd2").val();
        if(settingType===7){
            if(oldDrawPwd===""){
                alert("请输入原密码");
                return false;
            }
        }

        if(drawPwd===""){
            alert("请输入新密码");
            return false;
        }
        if(drawPwd.length<6){
            alert("提款密码不能少于6位");
            return false;
        }
        if(drawPwd!=drawPwd2){
            alert("两次密码输入不一致");
            return false;
        }
        return true;
    }




</script>
</body>
</html>
