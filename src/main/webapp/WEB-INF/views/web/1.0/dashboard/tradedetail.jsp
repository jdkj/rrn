<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/3
  Time: 15:26
  Desc: 操盘信息
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/tradeDetail.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/trade.css">


    <title>我的操盘</title>
</head>
<body class="ui-view-wrap">
<jsp:include page="../common/dashboard.head.jsp"></jsp:include>
<!--*************************************************主要内容**************************************************************-->
<!-- 操盘tab，但是是静态的 -->
<div class="inner-page-nav-wrap">
    <ul class="inner-page-nav clearfix">
        <a href="/rest/trade/type/0"><li id="JS-operate" class="JS-dotrade nav "><i class="icon-chaopan"></i>我要操盘
        </li></a>
        <a href="/rest/trade/"><li id="JS-my-operate" class="JS-mytrade JS-mytrade nav active"><i class="icon-wodecaopan"></i>我的操盘
        </li></a>
    </ul>
</div>
<!-- 主要内容 -->
<div class="inner-page-content-wrap">
    <div class="history_contract">合约单：${agreement.showAgreementId}</div>
    <div class="trade-detail null">
        <div class="detail-wrap1 clearfix">
            <div class="detail-title"><span class="title-name">${agreement.signAgreementTypeDesc}</span><span
                    class="title-time">     <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}"
                                                           var="sDate"/> <fmt:formatDate pattern="yyyy.MM.dd"
                                                                                         value="${sDate}"></fmt:formatDate> - <fmt:parseDate
                    pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate
                    pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate>
           </span>
                <div class="wrap1-btn">
                    <a id="JS-trade-entrust" class="JS-tradeclient btn_operate btn_s_orange" href="/rest/agreement/${agreement.showAgreementId}/position">交易委托</a>
                    <%--<a id="JS-draw-profit" class="JS-fetchProfit btn_operate btn_s_width">提取利润</a>--%>
                    <a id="JS-plus-money" class="JS-addBound btn_operate btn_s_width">追加保证金</a>
                    <a id="JS-apply-settlement" class="JS-quittrans btn_operate btn_s_width">申请结算</a>
                </div>
            </div>
            <div class="detail-content"><span class="content-text">账户管理费：<fmt:formatNumber type="number" pattern="#0.00" value="${agreement.accountManagementFee}"></fmt:formatNumber>元 /  <c:choose><c:when test="${agreement.signAgreementType==0}">交易日</c:when><c:otherwise>月</c:otherwise></c:choose></span><span
                    class="content-text">已操盘天数：${agreement.continueOperateDay}个交易日</span>
            </div>
        </div>
        <div class="detail-wrap2 clearfix">
            <div class="wrap2-left">
                <div class="font-bold">合约总资产（元）</div>
                <div class="font-bold wrap2-value"><fmt:formatNumber type="number" pattern="#0.00"
                                                                     value="${agreement.availableCredit+agreement.stockCurrentPrice+agreement.prebuyLockMoney}"></fmt:formatNumber></div>
                <div class="font-bold">当前盈亏（元）<span class="trade-profit">100%利益分配</span></div>

                <div class="font-bold wrap2-value2  <c:choose>
                    <c:when test="${agreement.diffMoney>=0}">color-red</c:when><c:otherwise>color-green</c:otherwise>
                </c:choose>"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.diffMoney}"></fmt:formatNumber><span
                        class="font-normal font-size-14"><fmt:formatNumber type="number" pattern="#0.00"
                                                                           value="${agreement.diffMoney*100/(agreement.availableCredit+agreement.stockCurrentPrice+agreement.prebuyLockMoney)}"></fmt:formatNumber>%</span>
                </div>
            </div>
            <div class="wrap2-right">
                <div class="detail-each-wrap clearfix"><span class="detail-each">持仓市值</span><span class="detail-each">持仓比例</span><span
                        class="detail-each">可用额度</span></div>
                <div class="detail-each-wrap mb-40 clearfix"><span
                        class="detail-each color-black3 font-bold"> <fmt:formatNumber type="number" pattern="#0.00" value="${agreement.stockCurrentPrice}"></fmt:formatNumber></span><span
                        class="detail-each color-black3 font-bold"> <fmt:formatNumber type="number" pattern="#0.00"
                                                                                      value="${agreement.stockCurrentPrice*100/(agreement.availableCredit+agreement.stockCurrentPrice+agreement.prebuyLockMoney)}"></fmt:formatNumber>%</span><span
                        class="detail-each color-black3 font-bold"><fmt:formatNumber type="number" pattern="#0.00" value=" ${agreement.availableCredit}"></fmt:formatNumber></span></div>
                <div class="detail-each-wrap clearfix"><span class="detail-each">总操盘资金</span><span class="detail-each">申请额度</span><span
                        class="detail-each">风险保证金</span></div>
                <div class="detail-each-wrap clearfix"><span
                        class="detail-each color-black3 font-bold"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.borrowMoney+agreement.lockMoney}"></fmt:formatNumber> </span><span
                        class="detail-each color-black3 font-bold"> <fmt:formatNumber type="number" pattern="#0.00" value="${agreement.borrowMoney}"></fmt:formatNumber></span><span
                        class="detail-each color-black3 font-bold"><fmt:formatNumber type="number" pattern="#0.00" value=" ${agreement.lockMoney}"></fmt:formatNumber></span></div>
            </div>
        </div>
    </div>
    <div class="tradedetail-niu">
        <div style="margin-left:770px;" class="d_cont_niusmile"></div>
    </div>
    <ul class="detail-line-wrap clearfix">
        <li class="d-blue"></li>
        <li class="d-orange"></li>
        <li class="d-red"></li>
    </ul>
    <ul class="detail-line-value clearfix">
        <li class="d-value1">
            <div><i class="icon-less"></i></div>
            <span>亏损平仓线：<em class="font-bold"><fmt:formatNumber type="number" pattern="#0.00"
                                                                value="${agreement.closeLineMoney}"></fmt:formatNumber></em></span>
        </li>
        <li class="d-value2">
            <div><i class="icon-less"></i></div>
            <span> 亏损警戒线：<em class="font-bold"><fmt:formatNumber type="number" pattern="#0.00"
                                                                 value="${agreement.warningLineMoney}"></fmt:formatNumber></em></span>
        </li>
    </ul>
    <ul class="tab_public clearfix">
        <li data-type="position" class="selected JS-query">持仓详情</li>
    </ul>
    <div class="JS-list detail_list">
        <c:choose>
            <c:when test="${pageResult!=null&&fn:length(pageResult.result)>0}">
                <table width="100%" cellpadding="0" cellspacing="0" class="list-table">
                    <tbody>
                    <tr>
                        <th class="first-column">股票名称</th>
                        <th>持有股数</th>
                        <th>可卖股数</th>
                        <th>成本价</th>
                        <th>当前价</th>
                        <th>当前市值（占比）</th>
                        <th>浮动收益</th>
                        <th>操作</th>
                    </tr>

                    <c:forEach var="stockPosition" items="${pageResult.result}">
                        <tr>
                            <td class="first-column" width="235"><span class="color-black3 font-bold">${stockPosition.stockName}</span><span class="color-black6">(${stockPosition.stockCode})</span></td>
                            <td width="110">${stockPosition.stockNum}</td>
                            <td width="110">${stockPosition.stockAvailableNum}</td>
                            <td width="110">¥  ${stockPosition.buyPrice}</td>
                            <c:choose>
                                <c:when test="${stockPosition.diffMoney>=0}">
                                    <td width="110">¥  <span class="font-bold color-red">${stockPosition.currentPrice}</span></td>
                                    <td width="200"><fmt:formatNumber type="number" pattern="#0.00"
                                                                      value="${stockPosition.currentMarketValue}"></fmt:formatNumber>
                                        <span class="font-bold ">(<fmt:formatNumber type="number" pattern="#0.00"
                                                                                    value="${stockPosition.currentMarketValue*100/(agreement.availableCredit+agreement.stockCurrentPrice+agreement.prebuyLockMoney)}"></fmt:formatNumber>%)</span>
                                    </td>
                                    <td width="200"><span class="font-bold color-red">+<fmt:formatNumber type="number" pattern="#0.00" value="${stockPosition.diffMoney}"></fmt:formatNumber></span>(<span class="font-bold color-red">+${stockPosition.diffRate}%</span>)</td>
                                </c:when>
                                <c:otherwise>
                                    <td width="110">¥  <span class="font-bold color-green">${stockPosition.currentPrice}</span></td>
                                    <td width="200"><fmt:formatNumber type="number" pattern="#0.00"
                                                                      value="${stockPosition.currentMarketValue}"></fmt:formatNumber><span
                                            class="font-bold ">(<fmt:formatNumber type="number" pattern="#0.00"
                                                                                  value="${stockPosition.currentMarketValue*100/(agreement.availableCredit+agreement.stockCurrentPrice+agreement.prebuyLockMoney)}"></fmt:formatNumber>%)</span>
                                    </td>
                                    <td width="200"><span class="font-bold color-green"><fmt:formatNumber type="number" pattern="#0.00" value="${stockPosition.diffMoney}"></fmt:formatNumber></span>(<span class="font-bold color-green">${stockPosition.diffRate}%</span>)</td>
                                </c:otherwise>
                            </c:choose>
                            <td class="operate_column">
                                <a href="/rest//trade/tradeClient/${agreement.showAgreementId}/buy/${stockPosition.stockCode}"><i
                                        data-type="buy" class="icon-tianjia"></i></a>
                                <a href="/rest/trade/tradeClient/${agreement.showAgreementId}/sell/${stockPosition.stockCode}"><i
                                        data-type="buy" class="icon-jianshao"></i></a>
                            </td>
                        </tr>
                    </c:forEach>


                    </tbody>
                </table>
                <div class="page-middel clearfix">
                    <ul id="pagination-flowing" class="pagination-sm pagination">
                        <c:choose>
                            <c:when test="${pageResult.totalPages>1}">
                                <c:choose>
                                    <c:when test="${pageResult.pageNo>1}">
                                        <li class="first"><a href="/rest/trade/tradeDetail/${agreement.showAgreementId}/1">首页</a></li>
                                        <li class="prev "><a href="/rest/trade/tradeDetail/${agreement.showAgreementId}/${pageResult.pageNo-1}">上一页</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                        <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                    </c:otherwise>
                                </c:choose>
                                <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                    <c:choose>
                                        <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                            <li class="page">
                                                <a href="/rest/trade/tradeDetail/${agreement.showAgreementId}/${pageIndex}">${pageIndex}</a>
                                            </li>
                                        </c:when>
                                        <c:when test="${pageResult.pageNo==pageIndex}">
                                            <li class="page active">
                                                <a href="javascript:void(0)">${pageIndex}</a>
                                            </li>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                                <c:choose>
                                    <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                        <li class="next "><a href="/rest/trade/tradeDetail/${agreement.showAgreementId}/${pageResult.pageNo+1}">下一页</a></li>
                                        <li class="last "><a href="/rest/trade/tradeDetail/${agreement.showAgreementId}/${pageResult.totalPages}">尾页</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                        <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                <li class="page active"><a href="javascript:void(0)">1</a></li>
                                <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </c:when>
            <c:otherwise>
                <table width="100%" cellpadding="0" cellspacing="0" class="list-table">
                    <tbody>
                    <tr>
                        <th class="first-column">股票名称</th>
                        <th>持有股数</th>
                        <th>可卖股数</th>
                        <th>成本价</th>
                        <th>当前价</th>
                        <th>当前市值（占比）</th>
                        <th>浮动收益</th>
                        <th>操作</th>
                    </tr>
                    </tbody>
                </table>
                <div class="mt_pic"></div>
            </c:otherwise>
        </c:choose>


    </div>
</div>


<!-- **********************************浮动的隐藏内容******************************** -->
<!-- **********************************浮动的隐藏内容******************************** -->
<!-- **********************************浮动的隐藏内容******************************** -->

<!-- 利润提取 -->
<div id="fetchProfitDialog" style="display:none;margin: 0px;" class="modal-body">
    <div id="onestep" class="content-wrap content-box3 context-align">
        <h3>利润提取</h3>
        <div class="addBound-balance">当前盈利：<span class="data-profit">167.07</span>元</div>
        <div style="display:none" class="modal-error fxAmount-error"><i class="icon-jinggao"></i><span>123</span></div>
        <span class="clearfix"></span>
        <label for="fxAmount"> <i class="icon-shouru"></i>
            <input id="fxAmount" type="text" name="inputvalue" placeholder="请输入提取金额" data-inputvalue="">
        </label>
        <div class="addBound-tip">最高可提利润 <span class="data-cashprofit">167.07</span>元</div>
        <input hidden="" data-id="">
        <button type="button" class="JSbtnsubmit">确认</button>
    </div>
    <div id="twostep" style="display:none" class="content-wrap content-box3 context-align">
        <h3>提示</h3>
        <div id="JS-resultMsg" class="cont-word">利润成功</div>
        <button type="button" class="JSbtnclose">确认</button>
    </div>
</div>

<!-- 追加保证金 -->
<div id="addBondDialog" style="display: none; margin: 0px;" class="modal-body">
    <div id="onestep" class="content-wrap content-box3 context-align">
        <h3>追加保证金</h3>
        <div class="addBound-balance">当前余额：<span class="data-balance"><fmt:formatNumber type="number" pattern="#0.00" value="${userAccount.money}"></fmt:formatNumber> </span>元</div>
        <div style="display:none" class="modal-error fxAmount-error"><i class="icon-jinggao"></i><span>123</span></div>
        <span class="clearfix"></span>
        <label for="fxAmount"> <i class="icon-shouru"></i>
            <input id="fxAmount" type="text" name="fxAmount" placeholder="请输入追加金额" data-fxamount="">
        </label>
        <div class="addBound-tip">
            最低追加<span class="data-minAmount"><fmt:formatNumber pattern="#0.00" type="number" value="${(agreement.lockMoney+agreement.borrowMoney)*0.01}"></fmt:formatNumber></span>元
        <c:choose>
            <c:when test="${userAccount.money<(agreement.lockMoney+agreement.borrowMoney)*0.01}">
                <span class="data-minAmount">当前余额不足</span>
            </c:when>
        </c:choose>
        </div>
        <input hidden="" data-id="">
        <button type="button" class="JSbtnsubmit" <c:choose>
        <c:when test="${userAccount.money<(agreement.lockMoney+agreement.borrowMoney)*0.01}">
            disabled="disabled" style="cursor:not-allowed"
        </c:when>
    </c:choose>>确认</button>
    </div>
    <div id="twostep" style="display:none" class="content-wrap content-box3 context-align">
        <h3>提示</h3>
        <div id="JS-resultMsg" class="cont-word"></div>
        <button type="button" class="JSbtnclose">确认</button>
    </div>
</div>

<!-- 申请结算 -->
<div id="quittransDialog" style="display: none; margin: 0px;" class="modal-body">
    <div id="onestep" class="content-wrap content-box3 context-align">
        <h3>结算确认</h3>
        <div class="cont-word">确认结算当前合约？</div>
        <input hidden="" data-id="">
        <button type="button" class="JSbtnsubmit">确认</button>
    </div>
    <div id="twostep" style="display:none" class="content-wrap content-box3 context-align">
        <h3>提示</h3>
        <div id="JS-resultMsg" class="cont-word"></div>
        <button type="button" class="JSbtnclose">确认</button>
    </div>
</div>


<!--***********************************foot***********************************-->
<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>


<!-- **********************************隐藏内容*********************** -->
<input type="hidden" name="" id="JS-value-hyNo" value="${agreement.agreementId}"><!--合约编号 -->

<script>
    $(function(){
        //合约编号
        var agreementId = $("#JS-value-hyNo").val();

        $(".JSbtnsubmit").click(function(){
            $(this).prop("disabled",true);
            var container = $(this).parent().parent();
            //申请结算
            if(container.attr("id")==="quittransDialog"){
                var data = {"agreementId":agreementId};
                $.post("/rest/agreement/settlement",data,function(res){
                    container.find("#JS-resultMsg").html(res.resultMsg);
                    container.find("#onestep").hide();
                    container.find("#twostep").show();
                    if(res.resultFlag){
                        container.find(".JSbtnclose").on("click",this,function(){
                            window.location.href = "/rest/trade/";
                        });
                    }else{
                        container.find(".JSbtnclose").on("click",this,function(){
                            vbyzctools.hide(container);
                            container.find("#onestep").show();
                            container.find("#twostep").hide();
                        });
                    }
                    $(this).prop("disabled",false);
                },"json");
            }else if(container.attr("id")==="addBondDialog"){
                var additionalLockMoney = container.find("#fxAmount").val();
                var data = {"agreementId":agreementId,"additionalLockMoney":additionalLockMoney};
                $.post("/rest/agreement/additional",data,function(res){
                    container.find("#JS-resultMsg").html(res.resultMsg);
                    container.find("#onestep").hide();
                    container.find("#twostep").show();
                    if(res.resultFlag){
                        container.find(".JSbtnclose").on("click",this,function(){
                            window.location.href = window.location.href;
                        });
                    }else{
                        container.find(".JSbtnclose").on("click",this,function(){
                            vbyzctools.hide(container);
                            container.find("#onestep").show();
                            container.find("#twostep").hide();
                        });
                    }
                    $(this).prop("disabled",false);
                },"json");
            }
        });



    });
</script>

</body>
</html>

