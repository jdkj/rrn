<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/13
  Time: 16:03
  Desc: 提现申请页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/drawings-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/recharge.css" />
    <style>
        a[disabled]{
            background:#999 !important;
        }
    </style>
    <title>账户充值</title>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<div class="ui-view-wrap">
    <div class="inner-page-content-wrap">
        <div class="recharge_title">
            账户：<span class="user_name">139****5667</span>余额：<span class="color-orange">￥<fmt:formatNumber pattern="#0.00" type="number" value="${userAccount.money}"></fmt:formatNumber> </span>元
        </div>
        <ul class="tab_public clearfix">
            <a href="/rest/fund/withdrawals/"><li data-class="drawing_box" >我要提款</li></a>
            <a href="/rest/fund/withdrawals/record/"><li data-class="drawings_record" class="selected">提款记录</li></a>
        </ul>
        <div class="vbyzcbox">
            <div class="drawings_record">
                <table cellpadding="0" cellspacing="0" width="100%" class="list-table">
                    <tbody>
                    <tr>
                        <th class="first-column"> 时间 </th>
                        <th> 提款银行卡 </th>
                        <th> 金额 </th>
                        <th> 状态 </th>
                    </tr>
                    </tbody>
                </table>
                <div class="mypage">
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
</body>
</html>
