<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2017/3/14
  Time: 11:52
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/jquery.circliful.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/coupon-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/personCenter.css"/>
    <title>我的推荐码</title>
    <style>
        /*Dsmar 工具类样式*/
        #ds-tool-toast {
            width: 100%;
            min-height: 30px;
            padding: 3px;
            background: rgba(255, 0, 0, .6);
            position: absolute;
            left: 0;
            bottom: 25px;
            z-index: 666666;
            text-align: center;
            color: #fff;
            border-radius: 20px;
            font-size: 13px;
            line-height: 30px
        }

    </style>
</head>
<body>

<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>

<div class="ui-view-wrap">
    <div class="inner-page-content-wrap clearfix">
        <jsp:include page="../common/dashboard.menu.jsp" flush="true"></jsp:include>
        <div class="person-content">
            <jsp:include page="../common/dashboard.personal.info.jsp" flush="true"></jsp:include>
            <div class="coupon">
                <div class="my-coupon-wrap clearfix">
                    <p class="my_coupon_title">
                        我的引荐码
                    </p>
                    <p>
                        <img src="/rest//page/qrcode/?content=http://www.rrniu.cn/rest/page/s/${distributionUser.distributionUserId}"
                             width="200px" alt="">
                    </p>
                    <p>
                        或发送地址http://www.rrniu.cn/rest/page/s/${distributionUser.distributionUserId}给朋友
                    </p>
                </div>


                <div class="use-tips">
                    <h3>引荐人奖励：</h3>
                    <p>
                        被邀请人发起操盘合约并支付管理费，引荐人可获得一定比率的返现奖励
                    </p>
                    <p>
                        后台(<a
                            href="http://fx.rrniu.cn">http://fx.rrniu.cn</a>)帐号可查询当前返现数据。登录帐号${distributionUser.accountNum}
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>


<script>
    /**
     * toast
     * @param msg
     * @param callback
     */
    function toast(msg, callback) {
        if ($("#ds-tool-toast").length > 0) {
            $("#ds-tool-toast").remove();
        }
        var toastHtml = "<div id='ds-tool-toast'>" + msg + "</div>";
        $("body").append(toastHtml);
        setTimeout(function () {
            $("#ds-tool-toast").remove();
            if (typeof callback === "function") callback();
        }, 2000);
    };
</script>
</body>
</html>
