<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/12
  Time: 18:40
  Desc: 充值页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/recharge.css" />
    <style>
        a[disabled]{
            background:#999 !important;
        }
    </style>
    <title>账户充值</title>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<div class="ui-view-wrap">
    <div class="inner-page-content-wrap">
        <div class="recharge_title">
            账户：<span class="user_name">${userInfo.nickName}</span>余额：<span class="color-orange">¥ <fmt:formatNumber type="number" pattern="#0.00" value="${userAccount.money}"/></span>元
        </div>
        <div class="vbyzcbox">
            <div class="recharge_box">
                <div class="cont-box">
                    <div class="cont-left">
                        充值金额
                    </div>
                    <div class="cont-right">
                        <form action="/rest/fund/recharge/confirm/" method="post">
                        <div class="recharge_money">
                            <input type="text" name="money" class="JSdeltaAmount recharge_text">&nbsp;元
                        </div>

                        <div class="recharge_btn">
                            <a class="btn-standard btn-red JS-submitbtn" >确认充值</a>
                        </div>
                        </form>
                        <div class="recharge_tips">
                            <h1>温馨提示</h1>
                            <p>
                                1、为了您的资金安全，您的账户资金将由第三方银行托管；
                            </p>
                            <p>
                                2、充值前请注意您的银行卡充值限额，以免造成不便；
                            </p>
                            <p>
                                3、禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确认，将终止该账户的使用；
                            </p>
                            <p>
                                4、为了您的资金安全，建议充值前进行实名认证、手机绑定、设置提现密码；
                            </p>
                            <p>
                                5、如果充值遇到任何问题，请联系客服：<b>0592-5917777</b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
<script>
    $(function(){
        $(".JS-submitbtn").on("click",this,function(){
            if(+$("input[name='money']").val()>0){
                $("form").submit();
            }else{
                alert("请输入充值金额");
            }
        });
    });
</script>
</body>
</html>
