<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/3
  Time: 21:09
  Desc: 委托交易---持仓
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>

    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/trade.css">
    <title>操盘控制中心</title>
</head>
<body class="ui-view-wrap">
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<!--*************************************************主要内容**************************************************************-->
<!-- 操盘tab，但是是静态的 -->
<div class="inner-page-nav-wrap">
    <ul class="inner-page-nav clearfix">
        <a href="/rest/trade/type/0"><li id="JS-operate" class="JS-dotrade nav "><i class="icon-chaopan"></i>我要操盘
        </li></a>
        <a href="/rest/trade/"><li id="JS-my-operate" class="JS-mytrade JS-mytrade nav active"><i class="icon-wodecaopan"></i>我的操盘
        </li></a>
    </ul>
</div>
<!-- 主要内容 -->
<div class="inner-page-content-wrap">
    <!-- ************************************************选项卡************************************************ -->
    <jsp:include page="../common/agreement.tradeclient.menu.jsp"></jsp:include>
    <!-- ************************************************内容块************************************************* -->
    <div class="vbyzcbox">
        <!--  持仓-->
        <div id="JS-list" >


            <c:choose>
                <c:when test="${pageResult!=null&&fn:length(pageResult.result)>0}">
                    <table width="150%" cellpadding="0" cellspacing="0" class="list-table" id="TABLE-cicang">
                        <tr> <th class="first-column">股票名称</th> <th>持有股数</th> <th>可卖股数</th> <th>成本价</th> <th>当前价</th> <th>当前市值（占比）</th> <th>浮动收益</th> <th>操作</th> </tr>
                        <tbody>
                        <c:forEach var="stockPosition" items="${pageResult.result}">
                            <tr>
                                <td class="first-column" width="235"><span class="color-black3 font-bold">${stockPosition.stockName}</span><span class="color-black6">(${stockPosition.stockCode})</span></td>
                                <td width="110">${stockPosition.stockNum}</td>
                                <td width="110">${stockPosition.stockAvailableNum}</td>
                                <td width="110">¥  ${stockPosition.buyPrice}</td>
                                <c:choose>
                                    <c:when test="${stockPosition.diffMoney>=0}">
                                        <td width="110">¥  <span class="font-bold color-red">${stockPosition.currentPrice}</span></td>
                                        <td width="200"><fmt:formatNumber type="number" pattern="#0.00"
                                                                          value="${stockPosition.currentMarketValue}"></fmt:formatNumber>
                                            (<span class="font-bold color-red"><fmt:formatNumber type="number"
                                                                                                 pattern="#0.00"
                                                                                                 value="${stockPosition.currentMarketValue*100/(agreement.availableCredit+agreement.stockCurrentPrice+agreement.prebuyLockMoney)}"></fmt:formatNumber>%</span>)
                                        </td>
                                        <td width="200"><span class="font-bold color-red">+<fmt:formatNumber type="number" pattern="#0.00" value="${stockPosition.diffMoney}"></fmt:formatNumber></span>(<span class="font-bold color-red">+${stockPosition.diffRate}%</span>)</td>
                                    </c:when>
                                    <c:otherwise>
                                        <td width="110">¥  <span class="font-bold color-green">${stockPosition.currentPrice}</span></td>
                                        <td width="200"><fmt:formatNumber type="number" pattern="#0.00"
                                                                          value="${stockPosition.currentMarketValue}"></fmt:formatNumber>(<span
                                                class="font-bold color-green"><fmt:formatNumber type="number"
                                                                                                pattern="#0.00"
                                                                                                value="${stockPosition.currentMarketValue*100/(agreement.availableCredit+agreement.stockCurrentPrice+agreement.prebuyLockMoney)}"></fmt:formatNumber>%</span>)
                                        </td>
                                        <td width="200"><span class="font-bold color-green"><fmt:formatNumber type="number" pattern="#0.00" value="${stockPosition.diffMoney}"></fmt:formatNumber></span>(<span class="font-bold color-green">${stockPosition.diffRate}%</span>)</td>
                                    </c:otherwise>
                                </c:choose>
                                <td class="operate_column">
                                    <a href="/rest//trade/tradeClient/${agreementId}/buy/${stockPosition.stockCode}"><i data-type="buy" class="icon-tianjia" ></i></a>
                                    <a href="/rest/trade/tradeClient/${agreementId}/sell/${stockPosition.stockCode}"><i data-type="buy" class="icon-jianshao"></i></a>
                                </td>
                            </tr>
                        </c:forEach>


                        </tbody>
                    </table>
                    <div class="page-middel clearfix">
                        <ul id="pagination-flowing" class="pagination-sm pagination">
                            <c:choose>
                                <c:when test="${pageResult.totalPages>1}">
                                    <c:choose>
                                        <c:when test="${pageResult.pageNo>1}">
                                            <li class="first"><a href="/rest/agreement/${agreementId}/position/1">首页</a></li>
                                            <li class="prev "><a href="/rest/agreement/${agreementId}/position/${pageResult.pageNo-1}">上一页</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                            <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                    <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                        <c:choose>
                                            <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                                <li class="page">
                                                    <a href="/rest/agreement/${agreementId}/position/${pageIndex}">${pageIndex}</a>
                                                </li>
                                            </c:when>
                                            <c:when test="${pageResult.pageNo==pageIndex}">
                                                <li class="page active">
                                                    <a href="javascript:void(0)">${pageIndex}</a>
                                                </li>
                                            </c:when>
                                        </c:choose>
                                    </c:forEach>
                                    <c:choose>
                                        <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                            <li class="next "><a href="/rest/agreement/${agreementId}/position/${pageResult.pageNo+1}">下一页</a></li>
                                            <li class="last "><a href="/rest/agreement/${agreementId}/position/${pageResult.totalPages}">尾页</a></li>
                                        </c:when>
                                        <c:otherwise>
                                            <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                            <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                    <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                    <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                    <li class="page active"><a href="javascript:void(0)">1</a></li>
                                    <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                    <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                </c:otherwise>
                            </c:choose>

                        </ul>
                    </div>
                </c:when>
                <c:otherwise>
                    <table width="150%" cellpadding="0" cellspacing="0" class="list-table" id="TABLE-cicang">
                        <tr> <th class="first-column">股票名称</th> <th>持有股数</th> <th>可卖股数</th> <th>成本价</th> <th>当前价</th> <th>当前市值（占比）</th> <th>浮动收益</th> <th>操作</th> </tr>
                    </table>
                    <div class="mt_pic"></div>
                </c:otherwise>
            </c:choose>


        </div>
    </div>
</div>



<!--***********************************foot***********************************-->
<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
<script>
    $(function(){
        $(".JS-tradeclient-nav").find("a").eq(0).addClass("nav_select");
    })
</script>
</body>
</html>
