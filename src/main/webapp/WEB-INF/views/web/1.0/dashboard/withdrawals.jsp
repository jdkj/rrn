<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/13
  Time: 16:03
  Desc: 提现申请页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/drawings-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/recharge.css" />
    <style>
        a[disabled]{
            background:#999 !important;
        }
    </style>
    <title>账户充值</title>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<div class="ui-view-wrap">
    <div class="inner-page-content-wrap">
        <div class="recharge_title">
            账户：<span class="user_name">139****5667</span>余额：<span class="color-orange">￥<fmt:formatNumber pattern="#0.00" type="number" value="${userAccount.money}"></fmt:formatNumber> </span>元
        </div>
        <ul class="tab_public clearfix">
            <a href="/rest/fund/withdrawals/"><li data-class="drawing_box" class="selected">我要提款</li></a>
            <a href="/rest/fund/withdrawals/record/"><li data-class="drawings_record">提款记录</li></a>
        </ul>
        <div class="vbyzcbox">
            <div class="drawing_box">
                <div class="drawings_left">
                    <span class="draw_no1">账户余额</span>
                    <span class="draw_no2">提款金额</span>
                    <span class="draw_no3">提款银行卡号</span>
                    <span class="draw_no3">提款密码</span>
                </div>
                <div class="drawings_right">
                    <div class="drawings_word">
                        首次提款，请先实名认证、设置提款密码，添加提款银行卡。
                    </div>
                    <span class="color-black3 font-bold font-size-14">您的实名认证名为<span class="color-red font-size-16"></span>，请务必用此名字的银行卡提款！</span>
                    <div class="draw_r_no1">
                        <span>￥<fmt:formatNumber pattern="#0.00" type="number" value="${userAccount.money}"></fmt:formatNumber></span>&nbsp;元
                    </div>

                    <form id="drawId" name="drawForm">
                        <input type="text" name="" style="visibility: hidden"><input type="password" name="" style="visibility: hidden">
                        <div class="draw_r_no2">
                            <input type="text" name="drawAmount" class="recharge_text JSdrawAmount" data-max="<fmt:formatNumber pattern="#0.00" type="number" value="${userAccount.money}"></fmt:formatNumber>">&nbsp;&nbsp;元
                            <div class="draw-info">
                                <i class="icon-jinggao color-orange"></i>&nbsp;每日提款额度累计<span class="color-orange">超过10万元</span>，T+1到账
                            </div>
                        </div>
                        <div class="draw_r_no3">
                            <select class="recharge_input JSdrawCard">
                                <option>请选择银行卡</option>
                            </select>
                            <a data-realname="false" class="blue JSaddBank">绑定银行卡</a>
                        </div>
                        <div class="draw_r_no4">
                            <input type="password" name="drawPwd" class="recharge_text JSdrawPassword"><a href="/member/setting.html" class="blue">设置提款密码</a>
                        </div>
                        <div class="recharge_btn">
                            <button type="button" class="btn-standard btn-red JSdraw-btn">提交</button>
                        </div>
                    </form>
                    <div class="recharge_tips">
                        <h1>温馨提示</h1>
                        <div class="drawing_explain">
                            <ul>
                                <li> <h1>最迟4小时到账</h1> </li>
                                <li>工作日<b>10:00-15:00</b>提款当日结算</li>
                                <li><b>15:00</b>以后提款第<b>2</b>日结算</li>
                                <li>具体到账时间以银行为准</li>
                            </ul>
                            <label></label>
                            <ul>
                                <li> <h1>提款0手续费</h1> </li>
                                <li>提款产生的银行手续费全免</li>
                            </ul>
                            <label></label>
                            <ul>
                                <li> <h1>支持10多家银行</h1> </li>
                                <li>推荐您使用</li>
                                <li>工商银行&nbsp;农业银行&nbsp;建设银行&nbsp;交通银行</li>
                                <li>提款到账最快</li>
                            </ul>
                            <ul>
                            </ul>
                        </div>
                        <div>
                        </div>
                        <p> 禁止洗钱、信用卡套现、虚假交易等行为，一经发现并确认，将终止该账户的使用。 </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
</body>
</html>
