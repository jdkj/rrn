<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/9
  Time: 23:08
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/tradeDetail.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/trade.css">


    <title>我的操盘</title>
</head>
<body class="ui-view-wrap">
<jsp:include page="../common/dashboard.head.jsp"></jsp:include>
<!--*************************************************主要内容**************************************************************-->
<!-- 操盘tab，但是是静态的 -->
<div class="inner-page-nav-wrap">
    <ul class="inner-page-nav clearfix">
        <a href="/rest/trade/type/0"><li id="JS-operate" class="JS-dotrade nav "><i class="icon-chaopan"></i>我要操盘
        </li></a>
        <a href="/rest/trade/"><li id="JS-my-operate" class="JS-mytrade JS-mytrade nav active"><i class="icon-wodecaopan"></i>我的操盘
        </li></a>
    </ul>
</div>
<!-- 主要内容 -->
<div class="inner-page-content-wrap">
    <div class="history_contract">合约单：${agreement.showAgreementId}</div>
    <div class="history_cont">
        <div class="history-title-wrap">
            <ul class="history-title-ul mb-40 clearfix">
                <li>
                    <div class="history-title-name">总操盘资金</div>
                    <div class="history-title-value"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.lockMoney+agreement.borrowMoney}"></fmt:formatNumber></div>
                </li>
                <li>
                    <div class="history-title-name">申请额度</div>
                    <div class="history-title-value"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.borrowMoney}"></fmt:formatNumber></div>
                </li>
                <li>
                    <div class="history-title-name">风险保证金</div>
                    <div class="history-title-value"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.lockMoney}"></fmt:formatNumber></div>
                </li>
            </ul>
            <ul class="history-title-ul clearfix">
                <li>
                    <div class="history-title-name">利益分配</div>
                    <div class="history-title-value">100%</div>
                </li>
                <li>
                    <div class="history-title-name">亏损警戒线</div>
                    <div class="history-title-value"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.warningLineMoney}"></fmt:formatNumber></div>
                </li>
                <li>
                    <div class="history-title-name">亏损平仓线</div>
                    <div class="history-title-value"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.closeLineMoney}"></fmt:formatNumber></div>
                </li>
            </ul>
        </div>


        <div class="history_title">合约明细</div>
        <ul class="history-cont-ul clearfix">
            <li><span class="history_cont_t">操盘资金：</span><span class="color-black3 font-bold font-size-16"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.lockMoney+agreement.borrowMoney}"></fmt:formatNumber></span></li>
            <li><span class="history_cont_t">交易品种：</span><span class="color-blue font-bold font-size-16">A</span><span class="color-black3 font-bold font-size-16">股</span></li>
            <li><span class="history_cont_t">交易盈亏：</span><span class="<c:choose><c:when test="${(agreement.availableCredit-agreement.lockMoney-agreement.borrowMoney)>0}">color-red</c:when><c:otherwise>color-green</c:otherwise></c:choose> font-bold font-size-16"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.availableCredit-agreement.lockMoney-agreement.borrowMoney}"></fmt:formatNumber></span></li>
            <li><span class="history_cont_t">开始时间：</span><span class="color-black3 font-size-14"><fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate></span></li>
            <li><span class="history_cont_t">结束时间：</span><span class="color-black3 font-size-14"><fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate></span></li>
            <li><span class="history_cont_t">管理费：</span><span class="color-black3 font-size-14"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.accountManagementFee}"></fmt:formatNumber></span></li>
        </ul>
        <div class="history_title clearfix"><span class="fl">交易盈亏分配</span><span class="fr font-size-14 color-gray9">已结算</span></div>
        <ul class="history-cont-ul clearfix">
            <li><span class="history_cont_t">盈利分配：</span><span class="<c:choose><c:when test="${(agreement.availableCredit-agreement.lockMoney-agreement.borrowMoney)>0}">color-red</c:when><c:otherwise>color-green</c:otherwise></c:choose> font-bold font-size-16"><c:choose><c:when test="${agreement.diffMoney>0}"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.availableCredit-agreement.borrowMoney-agreement.lockMoney}"></fmt:formatNumber></c:when><c:otherwise>0.00</c:otherwise></c:choose></span></li>
            <li><span class="history_cont_t">亏损赔付：</span><span class="color-gray9 font-bold font-size-16"><c:choose><c:when test="${agreement.diffMoney>0}">0.00</c:when><c:otherwise><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.diffMoney}"></fmt:formatNumber></c:otherwise></c:choose></span></li>
        </ul>
        <div class="history_title clearfix"><span class="fl">保证金结算</span><span class="fr font-size-14 color-gray9">已结算</span></div>
        <ul class="history-cont-ul clearfix last-list">
            <li><span class="history_cont_t">冻结：</span><span class="color-blue font-bold font-size-14"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.lockMoney}"></fmt:formatNumber></span></li>
            <li><span class="history_cont_t">扣减：</span><span class="color-gray9 font-bold font-size-16"><c:choose><c:when test="${agreement.diffMoney>0}">0.00</c:when><c:otherwise><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.diffMoney}"></fmt:formatNumber></c:otherwise></c:choose></span></li>
            <li><span class="history_cont_t">解冻：</span><span class="color-black3 font-bold font-size-14"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.availableCredit-agreement.borrowMoney}"></fmt:formatNumber></span></li>
        </ul>
    </div>

    <div class="JS-list detail_list">
        <div class="detail_list_title">历史交易</div>
        <c:choose>
            <c:when test="${pageResult!=null&&fn:length(pageResult.result)>0}">
                <table width="100%" cellpadding="0" cellspacing="0" class="list-table">
                    <tbody>
                    <tr>
                        <th class="first-column">股票名称</th>
                        <th>买卖方向</th>
                        <th>成交股数</th>
                        <th>成交价格</th>
                        <th>成交状态</th>
                        <th>成交市值</th>
                        <th>成交时间</th>
                    </tr>

                    <c:forEach var="stockOperate" items="${pageResult.result}">
                        <tr>
                            <td width="235" class="first-column"><span class="color-black3 font-bold">${stockOperate.stockName}</span><span class="color-black6">（${stockOperate.stockMarket}${stockOperate.stockCode}）</span></td>
                            <td width="110" class="<c:choose><c:when test="${stockOperate.operateType==1}">color-red</c:when><c:otherwise>color-green</c:otherwise></c:choose>">${stockOperate.operateTypeDesc}</td>
                            <td width="110">${stockOperate.transactionNum}</td>
                            <td width="130">¥<span class="<c:choose><c:when test="${stockOperate.operateType==1}">color-red</c:when><c:otherwise>color-green</c:otherwise></c:choose> font-bold "><fmt:formatNumber pattern="#0.00" type="number"  value="${stockOperate.transactionPrice}"></fmt:formatNumber></span></td>
                            <td width="110"><span class="color-gray9">已成交</span></td>
                            <td width="210"><span class="color-black3 font-bold"><fmt:formatNumber pattern="#0.00" type="number" value="${stockOperate.transactionPrice*stockOperate.transactionNum}"></fmt:formatNumber></span></td>
                            <td class="color-gray9">${stockOperate.updateTime}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="page-middel clearfix">
                    <ul id="pagination-flowing" class="pagination-sm pagination">
                        <c:choose>
                            <c:when test="${pageResult.totalPages>1}">
                                <c:choose>
                                    <c:when test="${pageResult.pageNo>1}">
                                        <li class="first"><a href="/rest/trade/tradeHistory/${agreement.showAgreementId}/1">首页</a></li>
                                        <li class="prev "><a href="/rest/trade/tradeHistory/${agreement.showAgreementId}/${pageResult.pageNo-1}">上一页</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                        <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                    </c:otherwise>
                                </c:choose>
                                <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                    <c:choose>
                                        <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                            <li class="page">
                                                <a href="/rest/trade/tradeHistory/${agreement.showAgreementId}/${pageIndex}">${pageIndex}</a>
                                            </li>
                                        </c:when>
                                        <c:when test="${pageResult.pageNo==pageIndex}">
                                            <li class="page active">
                                                <a href="javascript:void(0)">${pageIndex}</a>
                                            </li>
                                        </c:when>
                                    </c:choose>
                                </c:forEach>
                                <c:choose>
                                    <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                        <li class="next "><a href="/rest/trade/tradeHistory/${agreement.showAgreementId}/${pageResult.pageNo+1}">下一页</a></li>
                                        <li class="last "><a href="/rest/trade/tradeHistory/${agreement.showAgreementId}/${pageResult.totalPages}">尾页</a></li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                        <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                            <c:otherwise>
                                <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                <li class="page active"><a href="javascript:void(0)">1</a></li>
                                <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </c:when>
            <c:otherwise>
                <table width="100%" cellpadding="0" cellspacing="0" class="list-table">
                    <tbody>
                    <tr>
                        <th class="first-column">股票名称</th>
                        <th>买卖方向</th>
                        <th>成交股数</th>
                        <th>成交价格</th>
                        <th>成交状态</th>
                        <th>成交市值</th>
                        <th>成交时间</th>
                    </tr>
                    </tbody>
                </table>
                <div class="mt_pic"></div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
</body>
</html>
