<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/12/10
  Time: 14:47
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/jquery.circliful.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"> </script>
    <script src="<%=basePath%>/rrn/1.0/js/coupon-vbyzc.js"> </script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/personCenter.css" />
    <title>积分卡券</title>
    <style>
        /*Dsmar 工具类样式*/
        #ds-tool-toast{
            width:100%;
            min-height:30px;
            padding:3px;
            background:rgba(255,0,0,.6);
            position:absolute;
            left:0;
            bottom:25px;
            z-index:666666;
            text-align:center;
            color:#fff;
            border-radius:20px;
            font-size:13px;
            line-height:30px
        }

    </style>
</head>
<body>

<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>

<div class="ui-view-wrap">
    <div class="inner-page-content-wrap clearfix">
        <jsp:include page="../common/dashboard.menu.jsp" flush="true"></jsp:include>
        <div class="person-content">
            <jsp:include page="../common/dashboard.personal.info.jsp" flush="true"></jsp:include>
            <div class="coupon">
                <div class="my-coupon-wrap clearfix">
                    <p class="my_coupon_title">
                        我的优惠券
                    </p>
                    <span class="add_coupon"><a><strong>+</strong>添加优惠券</a></span>
                </div>
                <c:choose>
                    <c:when test="${userCouponList!=null&&fn:length(userCouponList)>0}">
                        <ul class="JS-conpons-list my-coupon-list clearfix">
                            <c:forEach var="userCoupon" items="${userCouponList}">
                                <li style="cursor: pointer;" class="clearfix JS-conponTrade blue"><span class="s-money">¥</span><span class="num-money"><fmt:formatNumber type="number" pattern="#0" value="${userCoupon.couponStore.couponMarketValue}"/></span>
                                    <p class="ref-dict-name">
                                        管理费抵用券
                                    </p>
                                    <p class="date-range">
                                        有效期：<fmt:parseDate pattern="yyyy-MM-dd" value="${userCoupon.createTime}" var="sDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate pattern="yyyy-MM-dd" value="${userCoupon.expiredTime}" var="eDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate>
                                    </p>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:when>
                </c:choose>

                <div class="use-tips">
                    <h3>优惠券使用说明：</h3>
                    <p>
                        1、单笔操盘只可使用1张优惠券，并在发起操盘时使用
                    </p>
                    <p>
                        2、操盘过程中，该笔操盘需要支付的手续费将优先从优惠券余额中扣除
                    </p>
                    <p>
                        3、操盘结束，该笔操盘使用的优惠券余额如未使用完，将自动作废
                    </p>
                </div>
                <div class="my-coupon-wrap clearfixe">
                    <p class="my_coupon_title">
                        优惠券商城<span>（可用积分：<fmt:formatNumber type="number" pattern="#0" value="${userAccount.integral}"/>）</span>
                    </p>
                </div>
                <c:choose>
                    <c:when test="${couponStoreList!=null&&fn:length(couponStoreList)>0}">
                        <ul class="JS-conpons-list my-coupon-list clearfix store-coupon-list">
                            <c:forEach var="couponStore" items="${couponStoreList}">
                                <li class="c-des-noheight JS-coupon-exchange  <c:choose><c:when test="${couponStore.couponMarketValue<100}">blue</c:when><c:when test="${couponStore.couponMarketValue>=100&&couponStore.couponMarketValue<500}">red</c:when><c:otherwise>yellow</c:otherwise></c:choose>">
                                    <div class="c-des clearfix">
                                        <span class="s-money">¥</span><span class="num-money"><fmt:formatNumber type="number" pattern="#0" value="${couponStore.couponMarketValue}"/> </span>
                                        <p class="ref-dict-name">
                                            管理费抵用券
                                        </p>
                                        <p class="date-range">
                                            有效期：1个月
                                        </p>
                                    </div>
                                    <div class="exchange-btn">
                                        <span class="JS-coupon-score"><fmt:formatNumber type="number" pattern="#0" value="${couponStore.couponMarketValue*1000}"/> </span>积分<i>兑换</i><span style="display:none" class="JS-coupon-id">${couponStore.couponId}</span>
                                    </div>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:when>
                </c:choose>

            </div>
        </div>
    </div>
</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
<!-- 添加优惠券的浮动层 -->
<div id="darkwrap"></div>
<!--添加优惠券-->
<div id="addCouponDialog" style="display: none;"  class="modal-body">
    <div class="content-wrap content-box3">
        <h3>请输入您的优惠券码</h3>
        <div>
            <div style="display:none;" class="modal-error"><i class="icon-jinggao"></i><span></span></div>
            <label for="couponCode" class="have-ts">
                <input name="couponCode" id="couponCode" placeholder="请输入您的优惠券码">
            </label>
            <button type="button" class="JSbtnsubmit">确认</button>
        </div>
    </div>
</div>
<!--兑换-->
<div id="exchangeDialog" style="display: none;"  class="modal-body">
    <!-- 这里直接提交检测 -->
    <div id="onestep" class="content-wrap content-box3">
        <h3>兑换提示</h3>
        <form>
            <input id="couponId" name="couponId" type="hidden" value="0">
            <div class="cont-word"> 确认花费&nbsp;<span>5000</span> 积分进行兑换？</div>
            <button type="button" class="JSbtnsubmit">确认</button>
        </form>
    </div>
</div>

<script>
    /**
     * toast
     * @param msg
     * @param callback
     */
    function toast(msg,callback){
        if($("#ds-tool-toast").length>0){
            $("#ds-tool-toast").remove();
        }
        var toastHtml = "<div id='ds-tool-toast'>"+msg+"</div>";
        $("body").append(toastHtml);
        setTimeout(function(){
            $("#ds-tool-toast").remove();
            if(typeof callback ==="function")callback();
        },2000);
    };
</script>
</body>
</html>

