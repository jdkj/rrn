<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/3
  Time: 21:09
  Desc: 卖出页面
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/tradeclient.fix.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/trade.css">
    <title>我的操盘</title>
</head>
<body class="ui-view-wrap">
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<!--*************************************************主要内容**************************************************************-->
<!-- 操盘tab，但是是静态的 -->
<div class="inner-page-nav-wrap">
    <ul class="inner-page-nav clearfix">
        <a href="/rest/trade/type/0"><li id="JS-operate" class="JS-dotrade nav "><i class="icon-chaopan"></i>我要操盘
        </li></a>
        <a href="/rest/trade/"><li id="JS-my-operate" class="JS-mytrade JS-mytrade nav active"><i class="icon-wodecaopan"></i>我的操盘
        </li></a>
    </ul>
</div>
<!-- 主要内容 -->
<div class="inner-page-content-wrap">
    <!-- ************************************************选项卡************************************************ -->
    <jsp:include page="../common/agreement.tradeclient.menu.jsp"></jsp:include>
    <!-- ************************************************内容块************************************************* -->
    <div class="vbyzcbox">


        <!-- 买入和卖出 共用-->
        <div class="trading_cont JS-tradeclient" id="trading_cont1" >
            <!-- 左边标题头 -->
            <div class="trading_l_title"><span class="leftline_01">现金余额</span><span class="leftline_02">股票代码</span><span class="leftline_03">委托类型</span><span class="leftline_04">委托价格</span><span class="leftline_05 JS-left5-tip">委买股数</span><span class="JS-dj-name">冻结现金</span></div>
            <!-- 右边 -->
            <div class="trading_r_cont">
                <ul class="trading_operate">
                    <li class="line_01"><span class="JS-balance-money color-orange font-size-16 font-bold"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.availableCredit}"></fmt:formatNumber></span>元</li>
                    <li class="line_02">
                        <input id="JS-input-stockcode" type="text" placeholder="请输入股票代码" maxlength="6" class="JS-stockCode input_trading" value="${stockCode}">
                        <span class="color-gray9">&nbsp;</span>
                        <span class="name JS-stockName"></span>
                    </li>
                    <li class="line_03">
                        <label><input id="JS-limit-entrust" type="radio" name="radio1" checked="checked" value="" class="JS-orderType"><span class="tc-weituo">限价委托</span></label>
                        <%--<label><input id="JS-market-entrust" type="radio" name="radio1" value="H" class="JS-orderType"><span class="tc-weituo">市价委托</span></label>--%>
                    </li>
                    <li class="line_04 JS-weituo-wrap1">
                        <i id="JS-price-reduce1" onselectstart="return false" style="-moz-user-select:none;" class="JS-minPrice icon-jianshao menu-addsub"></i>
                        <input id="JS-entrust-price" type="text" value="0" maxlength="10" class="JS-weituo-tip fl">
                        <i id="JS-price-add1"  onselectstart="return false" style="-moz-user-select:none;" class="JS-addPrice icon-mianxi menu-addsub"></i>
                        <div class="line4_word JS-stockPrice">
                            <span class="tc-updown">涨停： <span class="color-red font-bold upperLimitPrice">0.00</span></span>
                            <span class="tc-updown">跌停：<span class="color-green font-bold lowerLimitPrice">0.00</span></span>
                        </div>
                    </li>
                    <li class="line_04 JS-weituo-wrap2">
                        <i id="JS-mount-reduce1" onselectstart="return false" style="-moz-user-select:none;" class="JS-minNum icon-jianshao menu-addsub"></i>
                        <input id="JS-entrust-num" type="text" value="0" maxlength="10" class="JS-weituo-num fl">
                        <i id="JS-mount-add1" onselectstart="return false" style="-moz-user-select:none;" class="JS-addNum icon-mianxi menu-addsub"></i>
                        <span class="line4_word">
							<span class="JS-line4-tip">最大可买 </span>
							<span class="JS-weituo-max color-red font-bold">0</span>
							<span> 股</span>
							<span class="word-tips JS-sale-tips"> <i class="icon-jinggao"></i>不支持零股卖出，如有零股请联系客服人员进行处理！</span>
						</span>
                    </li>
                    <li class="line_05 JS-dj-name"><span class="JS-dj color-blue font-size-16 font-bold">0.00</span><span>&nbsp;元</span></li>
                </ul>
                <div class="trading_market JS-stockPrice">
                    <ul class="list-business-title clearfix"><li class="selected">买卖五档</li></ul>
                    <div class="list-business-list">
                        <ul> <li class="nth-child1">卖五</li> <li class="nth-child2 color-green sellPrice5">0.00</li> <li class="nth-child3 sellQty5">0</li> </ul>
                        <ul> <li class="nth-child1">卖四</li> <li class="nth-child2 color-green sellPrice4">0.00</li><li class="nth-child3 sellQty4">0</li> </ul>
                        <ul><li class="nth-child1">卖三</li><li class="nth-child2 color-green sellPrice3">0.00</li><li class="nth-child3 sellQty3">0</li></ul>
                        <ul><li class="nth-child1">卖二</li><li class="nth-child2 color-green sellPrice2">0.00</li><li class="nth-child3 sellQty2">0</li></ul>
                        <ul><li class="nth-child1">卖一</li><li class="nth-child2 color-green sellPrice1">0.00</li><li class="nth-child3 sellQty1">0</li></ul>
                    </div>
                    <div class="list-business-list">
                        <ul><li class="nth-child1">买一</li><li class="nth-child2 color-red buyPrice1">0.00</li><li class="nth-child3 buyQty1">0</li></ul>
                        <ul><li class="nth-child1">买二</li><li class="nth-child2 color-red buyPrice2">0.00</li><li class="nth-child3 buyQty2">0</li></ul>
                        <ul><li class="nth-child1">买三</li><li class="nth-child2 color-red buyPrice3">0.00</li><li class="nth-child3 buyQty3">0</li></ul>
                        <ul><li class="nth-child1">买四</li><li class="nth-child2 color-red buyPrice4">0.00</li><li class="nth-child3 buyQty4">0</li></ul>
                        <ul><li class="nth-child1">买五</li><li class="nth-child2 color-red buyPrice5">0.00</li><li class="nth-child3 buyQty5">0</li></ul>
                    </div>
                    <div class="list-business-list list-child4">
                        <ul><li class="nth-child1">现价</li><li><span class="color-black3 lastPrice">0.00</span></li></ul>
                        <ul><li class="nth-child1">涨幅</li><li><span class="color-red zhangfu">0</span><span class="color-red">%</span></li></ul>
                    </div>
                    <div class="list-business-list list-child5">
                        <ul><li class="nth-child1">涨停</li><li><span class="color-red upperLimitPrice">0.00</span></li></ul>
                        <ul><li class="nth-child1">跌停</li><li><span class="color-green lowerLimitPrice">0.00</span></li></ul>
                    </div>
                </div>
            </div>
            <!-- 下面 -->

            <div class="trading_submit">
                <div class="">
                    <input type="hidden" name="" id="v-post-action" value="2" data-description='提交的动作，1买，2卖'>
                </div>
                <div><button id="JS-submit-order" class="btn-standard btn-orange JS-sendOrder">提交指令</button><span class="trading_tips">交易时间段为&nbsp; 9：30-15：00</span></div>
                <a id="JS-back" class="tarding_back fr hand">返回</a>
            </div>
        </div>
    </div>
</div>



<!--***********************************foot***********************************-->
<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>


<!-- **********************************隐藏内容*********************** -->

<input type="hidden" name="" id="JS-value-hyNo" value="${agreement.agreementId}"><!--合约编号 -->
<script>
    $(function(){
        $(".JS-tradeclient-nav").find("a").eq(2).addClass("nav_select");
        $('.JS-left5-tip').text('委卖股数');
        $('.JS-line4-tip').text('最大可卖');
        $('#v-post-action').val('2');
        $('.JS-weituo-max').text('0'); //切换了标签，打乱了原先的计划，就要清空最大可操作股数，以防出乱子
        $('.JS-dj-name').hide();
        $('.JS-sale-tips').css("display","inline");
        <c:choose>
        <c:when test="${stockCode!=null}">
        Load_stock_info("${stockCode}");
        </c:when>
        </c:choose>
    })
</script>
</body>
</html>
