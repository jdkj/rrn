<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/3
  Time: 21:13
  Desc: 当日成交
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>

    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/trade.css">
    <title>我的操盘</title>
</head>
<body class="ui-view-wrap">
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<!--*************************************************主要内容**************************************************************-->
<!-- 操盘tab，但是是静态的 -->
<div class="inner-page-nav-wrap">
    <ul class="inner-page-nav clearfix">
        <a href="/rest/trade/type/0"><li id="JS-operate" class="JS-dotrade nav "><i class="icon-chaopan"></i>我要操盘
        </li></a>
        <a href="/rest/trade/"><li id="JS-my-operate" class="JS-mytrade JS-mytrade nav active"><i class="icon-wodecaopan"></i>我的操盘
        </li></a>
    </ul>
</div>
<!-- 主要内容 -->
<div class="inner-page-content-wrap">
    <!-- ************************************************选项卡************************************************ -->
    <jsp:include page="../common/agreement.tradeclient.menu.jsp"></jsp:include>
    <!-- ************************************************内容块************************************************* -->
    <div class="vbyzcbox">
        <div class="querySelect" id="Query_Select" >
            <div class="queryTab-wrap">
                <ul class="queryTab">
                    <li data-nav="queryExecution" class="query-nav query-select" >
                        <a href="/rest/agreement/${agreementId}/query/currentdeals">当日成交</a>
                    </li>
                    <li data-nav="queryEntrust" class="query-nav" >
                        <a href="/rest/agreement/${agreementId}/query/currentcommission">当日委托</a>
                    </li>
                    <li data-nav="queryHistoryExecution" class="query-nav">
                    <a href="/rest/agreement/${agreementId}/query/historydeals">历史成交</a>
                    </li>
                    <li data-nav="queryHistoryEntrust" class="query-nav" >
                        <a href="/rest/agreement/${agreementId}/query/historycommission">历史委托</a>
                    </li>
                </ul>
            </div>

            <!--  持仓-->
            <div id="JS-list">
                <c:choose>
                    <c:when test="${pageResult!=null&&fn:length(pageResult.result)>0}">
                        <table width="150%" cellpadding="0" cellspacing="0" class="list-table">
                            <tr>
                                <th class="first-column">股票名称</th>
                                <th>买卖方向</th>
                                <th>成交股数</th>
                                <th>成交价格</th>
                                <th>成交状态</th>
                                <th>成交市值</th>
                                <th>成交时间</th>
                            </tr>
                            <tbody>
                            <c:forEach var="stockOperate" items="${pageResult.result}">
                                <tr>
                                    <td class="first-column" width="235">
                                        <span class="color-black3 font-bold">${stockOperate.stockName}</span><span
                                            class="color-black6">(${stockOperate.stockCode})</span>
                                    </td>
                                    <td width="110"
                                        <c:choose>
                                        <c:when test="${stockOperate.transactionType==1}">class="color-red" </c:when>
                                            <c:otherwise>class="color-green"</c:otherwise>
                                    </c:choose>>${stockOperate.transactionTypeDesc}</td>
                                    <td width="110">${stockOperate.transactionNum}</td>
                                    <td width="110"><span class="color-black3 font-bold"><fmt:formatNumber type="currency" value="${stockOperate.transactionPrice}"></fmt:formatNumber></span></td>
                                    <td width="110"><span class="color-gray9">全成交</span></td>
                                    <td>
                                        <span class="color-black3 font-bold"><fmt:formatNumber value="${stockOperate.transactionNum*stockOperate.transactionPrice}" type="currency"></fmt:formatNumber></span>
                                    </td>
                                    <td width="">${stockOperate.updateTime}</td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                        <div class="page-middel clearfix">
                            <ul id="pagination-flowing" class="pagination-sm pagination">
                                <c:choose>
                                    <c:when test="${pageResult.totalPages>1}">
                                        <c:choose>
                                            <c:when test="${pageResult.pageNo>1}">
                                                <li class="first"><a href="/rest/agreement/${agreementId}/query/currentdeals/1">首页</a></li>
                                                <li class="prev "><a href="/rest/agreement/${agreementId}/query/currentdeals/${pageResult.pageNo-1}">上一页</a>
                                                </li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                                <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                            <c:choose>
                                                <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                                    <li class="page">
                                                        <a href="/rest/agreement/${agreementId}/query/currentdeals/${pageIndex}">${pageIndex}</a>
                                                    </li>
                                                </c:when>
                                                <c:when test="${pageResult.pageNo==pageIndex}">
                                                    <li class="page active">
                                                        <a href="javascript:void(0)">${pageIndex}</a>
                                                    </li>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                        <c:choose>
                                            <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                                <li class="next "><a href="/rest/agreement/${agreementId}/query/currentdeals/${pageResult.pageNo+1}">下一页</a>
                                                </li>
                                                <li class="last "><a href="/rest/agreement/${agreementId}/query/currentdeals/${pageResult.totalPages}">尾页</a>
                                                </li>
                                            </c:when>
                                            <c:otherwise>
                                                <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                                <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                        <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                        <li class="page active"><a href="javascript:void(0)">1</a></li>
                                        <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                        <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                    </c:otherwise>
                                </c:choose>

                            </ul>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <table width="150%" cellpadding="0" cellspacing="0" class="list-table">
                            <tr>
                                <th class="first-column">股票名称</th>
                                <th>买卖方向</th>
                                <th>成交股数</th>
                                <th>成交价格</th>
                                <th>成交状态</th>
                                <th>成交市值</th>
                                <th>成交时间</th>
                            </tr>
                        </table>
                        <div class="mt_pic"></div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>


<!--***********************************foot***********************************-->
<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
<script>
    $(function(){
        $(".JS-tradeclient-nav").find("a").eq(4).addClass("nav_select");
    })
</script>
</body>
</html>

