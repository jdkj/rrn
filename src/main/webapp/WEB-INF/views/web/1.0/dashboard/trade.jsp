<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/23
  Time: 21:36
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/trade-vbyzc.js?17070801"></script>

    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/trade.css"/>

    <title> 我要操盘</title>
</head>
<body class="ui-view-wrap">

<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<!--主要内容-->
<!--主要内容-->
<!--主要内容-->
<!--主要内容-->
<!-- 选项卡 -->
<div class="inner-page-nav-wrap">
    <ul class="inner-page-nav clearfix">
        <a href="/rest/trade/type/0">
            <li id="JS-operate" class="JS-dotrade nav active"><i class="icon-chaopan"></i>我要操盘
            </li>
        </a>
        <a href="/rest/trade/">
            <li id="JS-my-operate" class="JS-mytrade JS-mytrade nav"><i class="icon-wodecaopan"></i>我的操盘
            </li>
        </a>
    </ul>
</div>

<!-- 内容主体 -->
<div class="inner-page-content-wrap">
    <div class="trade-nav-warp clearfix">
        <c:choose>
            <c:when test="${borrowAgreementTypeList!=null&&fn:length(borrowAgreementTypeList)>0}">
                <c:forEach var="borrowAgreementType" items="${borrowAgreementTypeList}">
                    <a id="JS-short-term" href="/rest/trade/type/${borrowAgreementType.borrowType}"
                       class="t-navtext <c:choose>
                    <c:when test="${borrowAgreementType.borrowType==tradeType}">selected</c:when>
                </c:choose>">${borrowAgreementType.borrowTypeDesc}</a>
                </c:forEach>
            </c:when>
        </c:choose>
        <div id="JS-limit-stock" class="t-daylimit">今日限买股</div>
    </div>
    <div class="trade-content-wrap clearfix">
        <div class="trade-form clearfix">
            <div class="select-btn-label">申请资金</div>
            <div class="select-btn-group-wrap">
                <button id="JS-5000" data-pzamount="5000" class="JS-trade-applyAmount select-btn select-btn-active">
                    5000
                </button>
                <button id="JS-10000" data-pzamount="10000" class="JS-trade-applyAmount select-btn">1万</button>
                <button id="JS-30000" data-pzamount="30000" class="JS-trade-applyAmount select-btn">3万</button>
                <button id="JS-100000" data-pzamount="100000" class="JS-trade-applyAmount select-btn">10万</button>
                <button id="JS-150000" data-pzamount="150000" class="JS-trade-applyAmount select-btn">15万</button>
                <button id="JS-200000" data-pzamount="200000" class="JS-trade-applyAmount select-btn">20万</button>
                <button id="JS-300000" data-pzamount="300000" class="JS-trade-applyAmount select-btn">30万</button>
                <button id="JS-otherApplyAmount" data-pzamount="otherApplyAmount"
                        class="JS-trade-applyAmount select-btn">其他
                </button>

                <input id="JS-input-amount" type="text"
                       data-min="<fmt:formatNumber pattern="#0" type="number" value="${currentBorrowType.borrowMinMoney}"/>"
                       data-max="<fmt:formatNumber pattern="#0" type="number" value="${currentBorrowType.borrowMaxMoney}"/>"
                       placeholder="最少<fmt:formatNumber pattern="#0" type="number" value="${currentBorrowType.borrowMinMoney}"/>，最多<fmt:formatNumber pattern="#0" type="number" value="${currentBorrowType.borrowMaxMoney/10000}"/>万，千的整数倍"
                       style="width:447px; display:none;" class="JS-other-input"><span id="errInputTip"
                                                                                       style="display:none"
                                                                                       class="select-input-error"><i
                    class="icon-jinggao"> </i>最少<fmt:formatNumber pattern="#0" type="number"
                                                                  value="${currentBorrowType.borrowMinMoney}"/>，最多<fmt:formatNumber
                    pattern="#0" type="number" value="${currentBorrowType.borrowMaxMoney/10000}"/>万，千的整数倍</span></div>
            <div class="select-btn-label">投入资金</div>
            <div class="select-btn-group-wrap last-child">


                <c:choose>
                    <c:when test="${fn:length(borrowAgreementList)>0}">
                        <c:forEach var="borrowAgreement" items="${borrowAgreementList}">
                            <div id=" " data-agreement-id="${borrowAgreement.borrowAgreementId}"
                                 data-multiple="${borrowAgreement.multiple}"
                                 data-multiple-rate="${borrowAgreement.multipleRate}"
                                 data-borrow-agreement-type="${borrowAgreement.borrowType}"
                                 class="JS-trade-investAmount tehui-btn select-btn ">
                                <div data-mutiple="3">${borrowAgreement.multiple}倍</div>
                                <c:choose><c:when test="${borrowAgreement.borrowType>0}">
                                    <div class="monthinterest">月利率<fmt:formatNumber pattern="#0.00" type="number"
                                                                                    value="${borrowAgreement.multipleRate/12}"/>%
                                    </div>
                                </c:when><c:otherwise>
                                    <div class="monthinterest"></div>
                                </c:otherwise></c:choose>
                                <div class="JS-invest-val">&nbsp;</div>
                            </div>
                        </c:forEach>
                    </c:when>
                </c:choose>
            </div>
        </div>
        <div class="JS-result trade-result">
            <div class="trade-res">
                <table>
                    <tbody>
                    <tr>
                        <td class="first-column">总操盘资金</td>
                        <td><span class="font-color-dark font-bold">6667.00</span>&nbsp;&nbsp;元<span class="tips">（申请资金&nbsp;+&nbsp;投资本金）</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="first-column">单票持仓</td>
                        <td><span>主板${currentBorrowType.mainBoard}%,创业板${currentBorrowType.gem}%</span></td>
                    </tr>
                    <tr class="line">
                        <td class="first-column">持仓时间</td>
                        <td>自动延期，最长${currentBorrowType.maxPeriod}个<c:choose><c:when
                                test="${currentBorrowType.borrowType==0}">交易日</c:when><c:otherwise>月</c:otherwise></c:choose></td>
                    </tr>
                    <tr>
                        <td class="first-column">警戒线</td>
                        <td><span class="font-color-orangr">5834.00</span>&nbsp;&nbsp;元<span class="tips">（冻结买入）</span>
                        </td>
                    </tr>
                    <tr>
                        <td class="first-column">止损线</td>
                        <td><span class="font-color-green">5500.00</span>&nbsp;&nbsp;元</td>
                    </tr>
                    <tr>
                        <td class="first-column">投资本金</td>
                        <td><span class="JS-capitalAmount">1667.00</span>&nbsp;&nbsp;元</td>
                    </tr>
                    <tr>
                        <td class="first-column">管理费</td>
                        <td style="position: relative;cursor:pointer" class="clearfix"><span
                                class="JS-trade-mgamt">7.5 </span>&nbsp;元 <span>/<c:choose><c:when
                                test="${currentBorrowType.borrowType==0}">交易日</c:when><c:otherwise>月</c:otherwise></c:choose></span><span>（非交易日不收取）</span>
                            <span class="JS-coupon-wrap trade-coupons" data-user-coupon-id="0"
                                  data-user-coupon-money="0">
                                有 <em style="color:#FF2626;font-weight:bold">
                                <c:choose><c:when
                                        test="${userCouponList!=null&&fn:length(userCouponList)>0}">${fn:length(userCouponList)}</c:when><c:otherwise>0</c:otherwise></c:choose>
                            </em> 张抵用券可用<i class="icon-moreunfold"></i></span>
                            <c:choose>
                                <c:when test="${userCouponList!=null&&fn:length(userCouponList)>0}">
                                    <ul style="display:none" class="JS-coupon-list my-coupon-list clearfix">
                                        <c:forEach var="userCoupon" items="${userCouponList}">
                                            <li class="JS-coupon-choose clearfix blue">
                                                <span style="display:none"
                                                      class="JS-coupon-code">${userCoupon.userCouponId}</span><span
                                                    class="s-money">￥</span><span
                                                    class="JS-coupon-originAmount num-money"><fmt:formatNumber
                                                    type="number" pattern="#0"
                                                    value="${userCoupon.couponStore.couponMarketValue}"/></span>
                                                <p class="ref-dict-name">管理费抵用券</p>
                                                <p class="date-range"><fmt:parseDate pattern="yyyy-MM-dd"
                                                                                     value="${userCoupon.createTime}"
                                                                                     var="sDate"/> <fmt:formatDate
                                                        pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> -
                                                    <fmt:parseDate pattern="yyyy-MM-dd"
                                                                   value="${userCoupon.expiredTime}" var="eDate"/>
                                                    <fmt:formatDate pattern="yyyy.MM.dd"
                                                                    value="${eDate}"></fmt:formatDate></p><img
                                                    src="<%=basePath%>/rrn/1.0/imgs/flag4.fw.png"
                                                    style="display:none">
                                            </li>
                                        </c:forEach>

                                    </ul>
                                </c:when>
                            </c:choose>

                        </td>
                    </tr>
                    <tr class="line">
                        <td class="first-column">利益分配</td>
                        <td><span class="font-color-red font-bold">100%</span></td>
                    </tr>
                    <tr>
                        <td class="font-bold first-column">共计应支付</td>
                        <td><span class="JS-payall font-color-red font-bold font-size-16">1682</span>&nbsp;&nbsp;元<span
                                class="tips">（准备资金 = 投资本金 + 日管理费 x 天数）</span></td>
                    </tr>
                    <tr>
                        <td class="first-column"></td>
                        <td style="padding: 0">
                            <p class="jifen-wrap">您预期还将获得 <span class="JS-score font-color-red">15</span> 积分 </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="form-submit-btn">
        <p><span class="tips">申请即表示已阅读并同意</span><a id="JS-tradeProtocol" href="javascript:void(0)">《操盘协议》</a></p>
        <p>
            <button id="JS-instant-apply" class="JS-trade-deal btn-standard btn-orange">立即申请</button>
        </p>
    </div>
</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>


<!-- 浮动隐藏的东西 -->
<div id="darkwrap"></div>
<!-- 今日限买股的浮动层 -->
<div id="dayLimitDialog" style="display: none; margin: 0px auto;" class="modal-body">
    <div class="content-wrap content-box4"><h3>今日限买股</h3>
        <div class="limit-wrap">
            <c:choose>
                <c:when test="${fn:length(forbidStockList)>0}">
                    <c:forEach var="forbid" items="${forbidStockList}">
                        <div class="limit-each">
                            <div class="stock-name">${forbid.stockName}</div>
                            <div class="stock-code">${forbid.stockCode}</div>
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div class="limit-each">
                        <div class="stock-name">暂无限买股票</div>
                        <div class="stock-code">------</div>
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="tip"><i class="icon-jinggao"></i><span>平台不可交易基金、S、ST、*ST、S*ST及SST类股票。</span></div>
        <button type="button" class="JSbtnsubmit">确认</button>
    </div>

</div>


<script>

    $(function () {
        $("#JS-instant-apply").click(function () {
            var borrowItem = $(".JS-trade-applyAmount.select-btn-active");
            //申请资金
            var borrowMoney;
            if (borrowItem.is("#JS-otherApplyAmount")) {
                borrowMoney = +$("#JS-input-amount").val();
            } else {
                borrowMoney = +borrowItem.data("pzamount");
            }
            var multipleItem = $(".JS-trade-investAmount.select-btn-active");
            var borrowAgreementId = multipleItem.data("agreement-id");
            var signAgreementType = multipleItem.data("borrow-agreement-type");
            var userCouponId = $(".JS-coupon-wrap").data("user-coupon-id");
            var data = {
                "borrowMoney": borrowMoney,
                "borrowAgreementId": borrowAgreementId,
                "signAgreementType": signAgreementType,
                "userCouponId": userCouponId
            };
            $.post("/rest/trade/sign", data, function (res) {
                if (res.resultFlag) {
                    alert("合约生成成功");
                    window.location.href = "/rest/trade/";
                } else {
                    alert(res.resultMsg);
                }
            }, "json");
        });

        $("#JS-tradeProtocol").on("click", this, function () {
            var _url = "/rest/page/protocol/trade"
            var borrowItem = $(".JS-trade-applyAmount.select-btn-active");
            //申请资金
            var borrowMoney;
            if (borrowItem.is("#JS-otherApplyAmount")) {
                borrowMoney = +$("#JS-input-amount").val();
            } else {
                borrowMoney = +borrowItem.data("pzamount");
            }

            var lockMoney = $(".JS-trade-investAmount.select-btn-active").data("lock-money");

            if (borrowMoney < 2000) {
                borrowMoney = 5000;
                lockMoney = 1667;
            }

            var multipleItem = $(".JS-trade-investAmount.select-btn-active");

            var agreementType = multipleItem.data("borrow-agreement-type");

            window.location.href = _url + "?borrowMoney=" + borrowMoney + "&lockMoney=" + lockMoney + "&agreementType=" + agreementType;

        });
    });
</script>


</body>
</html>

