<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/26
  Time: 22:12
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/trade.css">


    <title>我的操盘</title>
</head>
<body class="ui-view-wrap">
<jsp:include page="../common/dashboard.head.jsp"></jsp:include>
<!--*************************************************主要内容**************************************************************-->
<!-- 操盘tab，但是是静态的 -->
<div class="inner-page-nav-wrap">
    <ul class="inner-page-nav clearfix">
        <a href="/rest/trade/type/0"><li id="JS-operate" class="JS-dotrade nav "><i class="icon-chaopan"></i>我要操盘
        </li></a>
        <a href="/rest/trade/"><li id="JS-my-operate" class="JS-mytrade JS-mytrade nav active"><i class="icon-wodecaopan"></i>我的操盘
        </li></a>
    </ul>
</div>
<!-- 主要内容 -->
<div class="inner-page-content-wrap">
    <c:choose>
        <c:when test="${pageResult!=null&&pageResult.result!=null&&fn:length(pageResult.result)>0}">
            <c:forEach items="${pageResult.result}" var="agreement">
                <c:choose>
                    <c:when test="${agreement.status==0||agreement.status==1}">
                        <div class="mt_content">

                            <div class="cont_title"><span class="c_title_left">${agreement.signAgreementTypeDesc}</span><span
                                    class="c_title_right"><fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate></span></div>
                            <div class="cont_picture">
                                <c:choose>
                                    <c:when test="${agreement.diffMoney>=0}">
                                        <div class="current_data_wrap current_data_rise"><span
                                                class="current">当前额度：<fmt:formatNumber pattern="#0.00" value="${agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney}" type="number"></fmt:formatNumber></span>
                                            <br><span class="fl">+<fmt:formatNumber type="number" pattern="#0.00"  value="${agreement.diffMoney}"></fmt:formatNumber></span><span
                                                    class="fr">+<fmt:formatNumber type="number"
                                                                                  value="${agreement.diffMoney*100/(agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney)}"
                                                                                  pattern="#0.00"/>%</span></div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="current_data_wrap current_data_fall"><span
                                                class="current">当前额度：<fmt:formatNumber pattern="#0.00" value="${agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney}" type="number"></fmt:formatNumber></span>
                                            <br><span class="fl"><fmt:formatNumber type="number" pattern="#0.00"  value="${agreement.diffMoney}"></fmt:formatNumber></span><span class="fr"><fmt:formatNumber
                                                    type="number"
                                                    value="${agreement.diffMoney*100/(agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney)}"
                                                    pattern="#0.00"/>%</span></div>
                                    </c:otherwise>
                                </c:choose>

                                <label class="early_warn">触发预警
                                    <br><span class="color-orange font-bold"> <fmt:formatNumber type="number"
                                                                                                value="${agreement.warningLineMoney}"
                                                                                                pattern="#0.00"/></span></label>
                                <label class="stop_soss">触发止损
                                    <br><span class="color-red font-bold"><fmt:formatNumber type="number"
                                                                                            value="${agreement.closeLineMoney}"
                                                                                            pattern="#0.00"/></span></label>
                                <label class="blocked_fund">风险保证金
                                    <br><span class="color-blue font-bold"><fmt:formatNumber type="number" pattern="#0.00" value="${agreement.lockMoney}"></fmt:formatNumber></span></label>
                            </div>
                            <div class="cont_list_wrap">
                                <ul>
                                    <li>申请额度：<span class="color-black3 font-bold"><fmt:formatNumber pattern="#0.00" type="number" value="${agreement.borrowMoney}"></fmt:formatNumber></span></li>
                                    <li>触发警戒：<span class="color-orange font-bold"><fmt:formatNumber type="number"
                                                                                                    value="${agreement.warningLineMoney}"
                                                                                                    pattern="#0.00"/></span>
                                    </li>
                                    <li>触发止损：<span class="color-red font-bold"><fmt:formatNumber type="number"
                                                                                                 value="${agreement.closeLineMoney}"
                                                                                                 pattern="#0.00"/></span>
                                    </li>
                                    <li>浮动盈亏：
                                        <c:choose>
                                        <c:when test="${agreement.diffMoney>=0}">
                                        <span class="color-red font-bold">
                                    </c:when>
                                    <c:otherwise>
                                         <span class="color-green font-bold">
                                    </c:otherwise>
                                </c:choose>
                               <fmt:formatNumber pattern="#0.00" type="number" value="${agreement.diffMoney}"></fmt:formatNumber>（<fmt:formatNumber type="number"
                                                                                                                                                    value="${agreement.diffMoney*100/(agreement.stockCurrentPrice+agreement.availableCredit)}"
                                                                                                                                                    pattern="#0.00"/>%）</span></li>
                                    <li>风险保证金：<span class="color-blue font-bold"><fmt:formatNumber type="number" value="${agreement.lockMoney}" pattern="#0.00"></fmt:formatNumber> </span></li>
                                    <li>持仓比例：<span class="color-blue font-bold"><fmt:formatNumber type="number" value="${agreement.stockCurrentPrice*100/(agreement.stockCurrentPrice+agreement.availableCredit+agreement.prebuyLockMoney)}" pattern="#0.00"></fmt:formatNumber>% </span></li>
                                </ul>
                                <a id="JS-gotrade" class="go-trade JS-gotrade" href="/rest//trade/tradeDetail/${agreement.showAgreementId}">去交易</a>
                                <!-- 下面这几个不知所谓的input不知干嘛用的，我们自己搞一个按钮就行 -->
                                <!-- <input hidden="" value="47632581608709" class="JS-tradeId">
                                <input hidden="" value="1" class="JS-tradeStatus">
                                <input hidden="" value="true" class="JS-tradeAccountInit">
                                <input hidden="" value="2" class="JS-tradeType"> -->

                            </div>
                        </div>
                        <!-- 块 -->
                    </c:when>
                    <c:when test="${agreement.status==2}">
                        <div class="JS-histrade-page">
                            <a href="/rest/trade/tradeHistory/${agreement.showAgreementId}">
                            <div class="mt_nav clearfix"></div>
                            <div id="" class="mt_list JS-histrade">
                                <input hidden="" value="${agreement.agreementId}" class="JS-tradeId">
                                <div class="cont_title"><span class="c_title_left">${agreement.signAgreementTypeDesc}</span><span class="c_title_right">  <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.createTime}" var="sDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${sDate}"></fmt:formatDate> - <fmt:parseDate pattern="yyyy-MM-dd" value="${agreement.operateDeadline}" var="eDate"/> <fmt:formatDate pattern="yyyy.MM.dd" value="${eDate}"></fmt:formatDate></span>
                                </div>
                                <ul class="list1">
                                    <li>申请额度：<span class="color-black3 font-bold font-size-16"><fmt:formatNumber pattern="#0.00" type="number" value="${agreement.borrowMoney}"></fmt:formatNumber></span></li>
                                    <li data-profit="16.71" class="JS-profit">浮动盈亏： <c:choose>
                                        <c:when test="${agreement.diffMoney>=0}">
                                        <span class="color-red font-bold">
                                    </c:when>
                                    <c:otherwise>
                                         <span class="color-green font-bold">
                                    </c:otherwise>
                                </c:choose>
                               <fmt:formatNumber pattern="#0.00" type="number" value="${agreement.diffMoney}"></fmt:formatNumber>（<fmt:formatNumber type="number"
                                                                                                                                                    value="${agreement.diffMoney*100/(agreement.stockCurrentPrice+agreement.availableCredit)}"
                                                                                                                                                    pattern="#0.00"/>%）</span>
                                    </li>
                                </ul>
                                <ul class="list2">
                                    <li>触发警戒：<span class="color-orange font-bold"><fmt:formatNumber type="number"
                                                                                                    value="${agreement.warningLineMoney}"
                                                                                                    pattern="#0.00"/></span>
                                    </li>
                                    <li>触发止损：<span class="color-red font-bold"><fmt:formatNumber type="number"
                                                                                                 value="${agreement.closeLineMoney}"
                                                                                                 pattern="#0.00"/></span>
                                    </li>
                                    <li>风险保证金：<span class="color-blue font-bold"><fmt:formatNumber type="number" value="${agreement.lockMoney}" pattern="#0.00"></fmt:formatNumber></span></li>
                                </ul>
                            </div>
                            </a>
                        </div>
                    </c:when>
                </c:choose>


            </c:forEach>
            <!-- 翻页 -->
            <div class="page-middel clearfix">
                <ul id="pagination-flowing" class="pagination-sm pagination">
                    <c:choose>
                        <c:when test="${pageResult.totalPages>1}">
                            <c:choose>
                                <c:when test="${pageResult.pageNo>1}">
                                    <li class="first"><a href="/rest/trade/1">首页</a></li>
                                    <li class="prev "><a href="/rest/trade/${pageResult.pageNo-1}">上一页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                                    <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach begin="1" end="${pageResult.totalPages}" var="pageIndex">
                                <c:choose>
                                    <c:when test="${(productResult.pageNo-2<=pageIndex&&pageResult.pageNo>pageIndex)||(pageResult.pageNo+3>pageIndex&&pageIndex>pageResult.pageNo)}">
                                        <li class="page">
                                            <a href="/rest/trade/${pageIndex}">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                    <c:when test="${pageResult.pageNo==pageIndex}">
                                        <li class="page active">
                                            <a href="javascript:void(0)">${pageIndex}</a>
                                        </li>
                                    </c:when>
                                </c:choose>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${pageResult.pageNo<pageResult.totalPages}">
                                    <li class="next "><a href="/rest/trade/${pageResult.pageNo+1}">下一页</a></li>
                                    <li class="last "><a href="/rest/trade/${pageResult.totalPages}">尾页</a></li>
                                </c:when>
                                <c:otherwise>
                                    <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                                    <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                                </c:otherwise>
                            </c:choose>
                        </c:when>
                        <c:otherwise>
                            <li class="first disabled"><a href="javascript:void(0)">首页</a></li>
                            <li class="prev disabled"><a href="javascript:void(0)">上一页</a></li>
                            <li class="page active"><a href="javascript:void(0)">1</a></li>
                            <li class="next disabled"><a href="javascript:void(0)">下一页</a></li>
                            <li class="last disabled"><a href="javascript:void(0)">尾页</a></li>
                        </c:otherwise>
                    </c:choose>

                </ul>
            </div>
        </c:when>
        <c:otherwise>
            <div class="mt_pic"></div>
        </c:otherwise>
    </c:choose>


</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>
</body>
</html>

