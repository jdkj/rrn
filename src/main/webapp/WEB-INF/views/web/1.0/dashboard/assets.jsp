<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/22
  Time: 21:15
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/jquery.circliful.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"> </script>
    <script src="<%=basePath%>/rrn/1.0/js/assets-vbyzc.js"> </script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css" />
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/personCenter.css" />
    <link href="<%=basePath%>/rrn/1.0/css/jquery.circliful.css" rel="stylesheet" type="text/css" />
    <title>收益统计</title>
</head>
<body>

<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>

<div class="ui-view-wrap">
    <div class="inner-page-content-wrap clearfix">
        <jsp:include page="../common/dashboard.menu.jsp" flush="true"></jsp:include>
        <div class="person-content">
            <jsp:include page="../common/dashboard.personal.info.jsp" flush="true"></jsp:include>
            <div class="account">
                <table width="100%">
                    <tr class="header">
                        <td width="25%" style="font-size:16px;color:#666" class="font-bold">
                            我的资产
                        </td>
                        <td width="25%">
                        </td>
                        <td width="25%" style="color:#666">
                            可用资金
                        </td>
                        <td width="25%" style="color:#666">
                            冻结资金
                        </td>
                    </tr>
                    <tr class="line">
                        <td style="font-size:28px;color:#333" class="font-bold">
                            <fmt:formatNumber type="number" pattern="#0.00"  value="${availableMoney+agreementMoney}"></fmt:formatNumber>
                        </td>
                        <td width="25%">
                        </td>
                        <td style="color:#333" class="font-bold">
                            <fmt:formatNumber type="number" pattern="#0.00"  value="${availableMoney}"></fmt:formatNumber>
                        </td>
                        <td style="color:#333" class="font-bold">
                            0.00
                        </td>
                    </tr>
                    <%--<tr class="jifen">--%>
                        <%--<td colspan="1">--%>
                            <%--积分：<span class="font-bold">1010</span>--%>
                        <%--</td>--%>
                        <%--<td colspan="1">--%>
                            <%--<a href="/member/coupon.html">卡券：<span class="font-bold color-orange">0</span>&nbsp;&nbsp;张</a>--%>
                        <%--</td>--%>
                    <%--</tr>--%>
                    <tr>
                        <td style="height:35px" colspan=5>
                            <div class="tubiao">
                                <div class="cell">
                                    <div class="myStat2" data-dimension="250"
                                         data-text="<c:choose><c:when test="${(availableMoney+agreementMoney)==0}">0</c:when><c:otherwise><fmt:formatNumber type="number" pattern="#0"  value="${agreementMoney*100/(availableMoney+agreementMoney)}"></fmt:formatNumber></c:otherwise></c:choose>%"
                                         data-info="证券净值( <fmt:formatNumber type="number" pattern="#0.00"  value="${agreementMoney}"></fmt:formatNumber>)"
                                         data-width="30" data-fontsize="38"
                                         data-percent="<c:choose><c:when test="${(availableMoney+agreementMoney)==0}">0</c:when><c:otherwise><fmt:formatNumber type="number" pattern="#0"  value="${agreementMoney*100/(availableMoney+agreementMoney)}"></fmt:formatNumber></c:otherwise></c:choose>"
                                         data-fgcolor="#61a9dc" data-bgcolor="#eee"></div>
                                </div>
                                <div class="cell"><div class="myStat2" data-dimension="250" data-text="0%" data-info="投资VIP(0)" data-width="30" data-fontsize="38" data-percent="0" data-fgcolor="#fa0" data-bgcolor="#eee"></div></div>
                                <div class="cell">
                                    <div class="myStat2" data-dimension="250"
                                         data-text="<c:choose><c:when test="${(availableMoney+agreementMoney)==0}">0</c:when><c:otherwise><fmt:formatNumber type="number" pattern="#0"  value="${availableMoney*100/(availableMoney+agreementMoney)}"></fmt:formatNumber></c:otherwise></c:choose>%"
                                         data-info="现金资产( <fmt:formatNumber type="number" pattern="#0.00"  value="${availableMoney}"></fmt:formatNumber>)"
                                         data-width="30" data-fontsize="38"
                                         data-percent="<c:choose><c:when test="${(availableMoney+agreementMoney)==0}">0</c:when><c:otherwise><fmt:formatNumber type="number" pattern="#0"  value="${availableMoney*100/(availableMoney+agreementMoney)}"></fmt:formatNumber></c:otherwise></c:choose>"
                                         data-fgcolor="#f10" data-bgcolor="#eee"></div>
                                </div>
                                <div class="cl"></div>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>

</body>
</html>

