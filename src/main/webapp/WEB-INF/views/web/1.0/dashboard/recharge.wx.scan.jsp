<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/11/20
  Time: 23:41
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/recharge-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/recharge.css" />
    <style>
        a[disabled]{
            background:#999 !important;
        }
    </style>
    <title>账户充值</title>
</head>
<body>
<jsp:include page="../common/dashboard.head.jsp" flush="true"></jsp:include>
<div class="ui-view-wrap">
    <div class="inner-page-content-wrap">
        <div class="recharge_title">
            账户：<span class="user_name">${userInfo.nickName}</span>余额：<span class="color-orange">¥<fmt:formatNumber type="number" pattern="#0.00" value="${userAccount.money}"/> </span>元
        </div>
        <ul class="tab_public2 clearfix">
            <li data-class="recharge_box" class="selected">支付清单</li>
        </ul>
        <!-- 内容 -->
        <div class="vbyzcbox">
            <table width="100%" id="paytable" cellpadding="20" cellspacing="1" border="0">
                <tr>
                    <td class="left">交易类型：</td>
                    <td class="right">${recharge.rechargeDesc}</td>
                </tr>
                <tr>
                    <td class="left">金额：</td>
                    <td class="right"><span style="color:red;font-size:32px">￥<fmt:formatNumber pattern="#0.00" type="number" value="${recharge.money}"></fmt:formatNumber></span></td>
                </tr>
                <tr>
                    <td class="left">时间：</td>
                    <td class="right"><span style=""><fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${recharge.createTime}" var="rDate"/><fmt:formatDate value="${rDate}" pattern="yyyy-MM-dd HH:mm:ss"/></span></td>
                </tr>
                <tr>
                    <td class="left">订单号：</td>
                    <td class="right"><span style="">${recharge.showRechargeId}</span></td>
                </tr>
                <tr>
                    <td class="left">过期时间：</td>
                    <td class="right"><span style=""><fmt:parseDate pattern="yyyy-MM-dd HH:mm:ss" value="${recharge.expiredTime}" var="rDate"/><fmt:formatDate value="${rDate}" pattern="yyyy-MM-dd HH:mm:ss"/></span></td>
                </tr>
                <tr>
                    <td class="left">扫码支付：</td>
                    <td class="right"><img src="/rest/fund/${recharge.showRechargeId}" alt=""></td>
                </tr>
            </table>
        </div>
    </div>
</div>
<jsp:include page="../common/foot.jsp" flush="true"></jsp:include>

<script>
    $(function(){
        var count = 0;
        var interval = setInterval(function(){
//            if(count>6){
//                console("还没充值完成，那就给个弹窗吧！");
//                clearInterval(interval);
//            }
           var data ={"orderId":${recharge.showRechargeId}};
            $.post("/rest/fund/recharge/query/wx/",data,function(res){
                if(res.resultFlag){
                    alert("充值成功",function(){
                        window.location.href="/rest/assets/";
                    });

                }
            },"json")
            count++;
        },10000);
    })
</script>

</body>
</html>
