<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/22
  Time: 19:27
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/main-vbyzc.js"></script>
    <script src="<%=basePath%>/rrn/1.0/js/register-vbyzc.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/login.css"/>
    <title><c:choose><c:when test="${isRegiste}">注册</c:when><c:otherwise>登录</c:otherwise></c:choose></title>
</head>
<body>
<div class="topwrap">
    <div class="w1200">
        <div class="top">
            <div class="fl"><img src="<%=basePath%>/rrn/1.0/images-assets/logo.png" alt="人人牛" height="60"/></div>
            <div class="links fr">
                <a href="/rest/page/login">登录</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/rest/page/registe">注册</a>
            </div>
            <div class="cl"></div>
        </div>
    </div>
</div>
<!--主要内容-->
<div class="inner-page-content-wrap clearfix">
    <div class="reg-left"></div>
    <div class="login-right">
        <div class="login-title clearfix">
            <div class="title-name"><c:choose><c:when test="${isRegiste}">注册</c:when><c:otherwise>登录</c:otherwise></c:choose>人人牛</div>
            <div class="title-link">
                <!-- <a href="login.html">立即登录</a> -->
            </div>
        </div>
        <div class="login-cont">
            <div style="display:none" class="error-msg"><i class="icon-jinggao"></i><span>密码错误</span></div>
            <label><i class="icon-shouji1"></i><input id="username" type="text" name="username" placeholder="11位手机号码"
                                                      autocomplete="off"></label>
            <div id="login-imagecode" style="position:relative;display: none">
                <label class="dxyz"> <i class="icon-duanxinyanzheng"></i><input type="text" id="imageCode"
                                                                                name="imageCode" placeholder="图形验证码"
                                                                                class=" form-input" autocomplete="off">
                </label>
                <img src="/rest//ImageRandCode/" onclick="this.src='/rest//ImageRandCode/?_t='+new Date().getTime()"
                     style="cursor: pointer;position:absolute;left:210px;top:5px;"/>
            </div>
            <div id="login-telcode" style="position:relative;">
                <label class="dxyz"> <i class="icon-duanxinyanzheng"></i><input type="text" id="verifyCode"
                                                                                name="verifyCode" placeholder="手机验证码"
                                                                                class=" form-input" autocomplete="off">
                </label>
                <button class="getcode" disabled="true" style="top:0px;">获取短信验证码</button>
            </div>
            <div id="login-pass" style="display:none;">
                <label><i class="icon-duanxinyanzheng"></i><input id="password" type="password" name="password"
                                                                  placeholder="登录密码" autocomplete="off"></label>
                <div id="JS-chageLoginMethod">忘记密码，用手机验证码登录</div>
            </div>

            <div class="login-menu"><button type="button" class="bt-submit btn-red JSregister" disabled="true">一键登录注册</button></div>
            <div class="login-info"><c:choose><c:when test="${isRegiste}">注册</c:when><c:otherwise>登录</c:otherwise></c:choose>人即代表<br>我同意
                <a href="/rest/page/protocol/sign">《注册协议》</a>、
                <a href="/rest/page/protocol/invest">《合格投资人申明》</a>、
                <a href="/rest/page/protocol/risk">《风险揭示书》</a>
            </div>
        </div>
    </div>
</div>

<jsp:include page="common/foot.jsp" flush="true"/>
<script>
//    $(function () {
//
//
//        /**
//         * 获取短信验证码
//         */
//        $(".getcode").click(function () {
//            var phone = $("#username").val();
//            if(validPhoneValue(phone)){
//                var data = {"phone":phone};
//                $.post("/rest/user/reqCode",data,function(res){
//                    if(res.resultFlag){
//                        alert(res.data.codes );
//                    }else{
//                        alert(res.resultMsg);
//                    }
//                    return false;
//                },"json");
//            }
//        });
//
//
//        $(".JSregister").click(function(){
//            var phone = $("#username").val();
//            var code = $("#verifyCode").val();
//            var data = {"phone":phone,"codes":code};
//            if(validPhoneValue(phone)&&validCode(code)){
//                $.post("/rest/user/registe",data,function(res){
//                  if(res.resultFlag){
//                      window.location.href="/";
//                  }else{
//                      alert(res.resultMsg);
//                  }
//                },"json");
//            }
//        });
//
//
//    });
//
//    /**
//     * 验证手机号是否正确
//     */
//    function validPhoneValue(phone) {
//
//        if (!(/^1[34578]\d{9}$/.test(phone))) {
//            alert("手机号码有误，请重填");
//            return false;
//        }
//        return true;
//    }
//
//
//    /**
//     * 验证短信验证码
//     * @param code
//     * @returns {boolean}
//     */
//    function validCode(code){
//        if(!(/^\d{5}$/.test(code))){
//            alert("请输入验证码!");
//            return false;
//        }
//        return true;
//    }
</script>
</body>
</html>
