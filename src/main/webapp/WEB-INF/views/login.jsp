<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/13
  Time: 15:12
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/login.css"/>
    <title>登录</title>
</head>
<body>
<div class="topwrap">
    <div class="w1200">
        <div class="top">
            <div class="fl"><img src="<%=basePath%>/rrn/1.0/images-assets/logo.png" alt=""/></div>
            <div class="links fr">
                <a href="/rest/page/login">登录</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/rest/page/registe">注册</a>
            </div>
            <div class="cl"></div>
        </div>
    </div>
</div>
<!--主要内容-->
<div class="inner-page-content-wrap clearfix">
    <div class="login-left"></div>
    <div class="login-right">
        <div class="login-title clearfix">
            <div class="title-name">登录人人牛</div>
            <div class="title-link">
                <a id="JS-free-register" href="/rest/page/registe">免费注册</a>
            </div>
        </div>
        <div class="login-cont">
            <div style="display:none" class="error-msg"><i class="icon-jinggao"></i><span>密码错误</span></div>
            <label><i class="icon-shouji1"></i><input id="username" type="text" name="username"
                                                      placeholder="用户名/11位手机号码"></label><label><i
                class="icon-duanxinyanzheng"></i><input id="password" type="password" name="password"
                                                        placeholder="登录密码"></label>
            <div class="forget-code">
                <a id="JS-forget-pass" href="/rest/page/forrgotpassword">忘记密码</a>
            </div>
            <div class="login-menu">
                <button id="JS-login" type="button" class="btn-red JSlogin">登录</button>
            </div>
            <%--<div class="third-part-login-wrap">--%>
                <%--<div class="title">第三方账号登录</div>--%>
                <%--<img src="/imgs/wx_icon48.png" class="JSthirdSigninBtn"></div>--%>
        </div>
    </div>
</div>

<!--foot-->
<!--foot-->
<!--foot-->
<div class="foot">
    <div class="w1200">
        <!--左边的-->
        <div class="fl">
            <div class="navFoot mt10">
                <a href="#">关于我们</a> |
                <a href="#">加入人人牛</a> |
                <a href="#">联系试</a>
            </div>
            <div class="color999 mt10">风险提示: 保护投资者利益是中国证监会工作的重中之重。<br>我们提醒您：股市有风险，投资需谨慎！市场风险莫测，务请谨慎行事！</div>
            <div class="colorfff mt10">版权所有 © 人人牛 闽ICP备10006454号-4</div>
        </div>
        <!--右边的-->
        <div class="qrcode fr">
            <img src="<%=basePath%>/rrn/1.0/images-assets/qrcode.png" alt=""/>
            <div class="colorfff">关注微信公众号</div>
        </div>
        <div class="fr mt20" style="margin-right: 70px;;">
            <div class="fs16 color999">客服电话</div>
            <div class="fs30 fontbold colorfff">0592-5917777</div>
            <div class="fs16 color999">周一~周日 9:00 ~ 18:00</div>
        </div>
        <div class="cl"></div>
    </div>
</div>
<script>
    //- 前端发送ajax的host
    var ajaxBaseUrl = '';
</script>
<script src="<%=basePath%>/rrn/1.0/js/libs-1b5e06e759.js" type="text/javascript"></script>
<script src="<%=basePath%>/rrn/1.0/js/main-248fe7ea0c.js" type="text/javascript"></script>
<script src="<%=basePath%>/rrn/1.0/js/login-057891b68b.js" type="text/javascript"></script>
</body>
</html>

