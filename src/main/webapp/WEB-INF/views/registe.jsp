<%--
  Created by IntelliJ IDEA.
  User: Dsmart
  Date: 2016/10/14
  Time: 0:50
  Desc: 
  
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="Generator" content="">
    <meta name="Author" content="">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script src="<%=basePath%>/rrn/1.0/js/183min.js"></script>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/css.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/reset.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/main.css"/>
    <link rel="stylesheet" href="<%=basePath%>/rrn/1.0/css/login.css"/>
    <title>注册</title>
</head>
<body>
<div class="topwrap">
    <div class="w1200">
        <div class="top">
            <div class="fl"><img src="<%=basePath%>/rrn/1.0/images-assets/logo.png" alt=""/></div>
            <div class="links fr">
                <a href="/rest/page/login">登录</a>
                &nbsp;&nbsp;&nbsp;
                <a href="/rest/page/registe">注册</a>
            </div>
            <div class="cl"></div>
        </div>
    </div>
</div>
<!--主要内容-->
<div class="inner-page-content-wrap clearfix">
    <div class="reg-left"></div>
    <div class="login-right">
        <div class="login-title clearfix">
            <div class="title-name">注册人人牛</div>
            <div class="title-link">
                <a href="/rest/page/login">立即登录</a>
            </div>
        </div>
        <div class="login-cont">
            <div style="display:none" class="error-msg">
                <i class="icon-jinggao"></i>
                <span>密码错误</span>
            </div>
            <label>
                <i class="icon-shouji1"></i>
                <input id="username" type="text" name="username"
                       placeholder="11位手机号码">
            </label>
            <label class="dxyz">
                <i class="icon-duanxinyanzheng"></i>
                <input id="ivc" type="text" name="ivc" placeholder="验证码">
                <span class="imgcode">
                    <img src="/verifyCode" onclick="this.src='/verifyCode?' + new Date().getTime();"
                                         class="ivc">
                </span>
            </label>
            <label class="dxyz">
                <i
                class="icon-duanxinyanzheng"></i>
                <input
                type="text" name="verifyCode" placeholder="手机验证码" class="form-input">
            </label>
            <span class="getcode">获取短信验证码</span><label><i
                class="icon-mima1"></i><input type="password" name="pwd" placeholder="设置登录密码"
                                              class="form-input"></label>
            <div class="login-menu">
                <button type="button" class="btn-red JSregister">注册</button>
            </div>
            <div class="login-info">注册即代表<br>我同意
                <a href="/protocolSign.html">《注册协议》</a>、
                <a href="/protocolInvest.html">《合格投资人申明》</a>、
                <a href="/protocolRisk.html">《风险揭示书》</a>
            </div>
        </div>
    </div>
</div>
<!--foot-->
<!--foot-->
<!--foot-->
<div class="foot">
    <div class="w1200">
        <!--左边的-->
        <div class="fl">
            <div class="navFoot mt10">
                <a href="#">关于我们</a> |
                <a href="#">加入人人牛</a> |
                <a href="#">联系试</a>
            </div>
            <div class="color999 mt10">风险提示: 保护投资者利益是中国证监会工作的重中之重。<br>我们提醒您：股市有风险，投资需谨慎！市场风险莫测，务请谨慎行事！</div>
            <div class="colorfff mt10">版权所有 © 人人牛 闽ICP备10006454号-4</div>
        </div>
        <!--右边的-->
        <div class="qrcode fr">
            <img src="<%=basePath%>/rrn/1.0/images-assets/qrcode.png" alt=""/>
            <div class="colorfff">关注微信公众号</div>
        </div>
        <div class="fr mt20" style="margin-right: 70px;;">
            <div class="fs16 color999">客服电话</div>
            <div class="fs30 fontbold colorfff">0592-5917777</div>
            <div class="fs16 color999">周一~周日 9:00 ~ 18:00</div>
        </div>
        <div class="cl"></div>
    </div>
</div>
<script>
    //- 前端发送ajax的host
    var ajaxBaseUrl = '';
</script>
<script src="<%=basePath%>/rrn/1.0/js/libs-1b5e06e759.js" type="text/javascript"></script>
<script src="<%=basePath%>/rrn/1.0/js/main-248fe7ea0c.js" type="text/javascript"></script>
<script src="<%=basePath%>/rrn/1.0/js/register-ca37be5d1c.js" type="text/javascript"></script>
</body>
</html>

