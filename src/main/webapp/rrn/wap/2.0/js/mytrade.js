//为了省事，用全局变量
var gPageSize = 1;   //每页读取多少记录
var i = 1; //设置当前页数，全局变量  
/*********************************jquery区域 *********************************/
/*********************************jquery区域 *********************************/
/*********************************jquery区域 *********************************/
$(function () {  
	/**
	 * 远程加载数据
	 * @param  {[type]} pagenumber [description]
	 * @return {[type]}            [description]
	 */
	function getData(pagenumber) {  
		i++; 
		$.ajax({  
			type: "post",  
			url: "json_tradedata.html",  
			data: { pagesize: gPageSize, pagenumber: pagenumber },  
			dataType: "json",  
			success: function (data) {  
				$(".loaddiv").hide();  
				if (data.length > 0) {  
					var jsonObj = data;  
					insertDiv(jsonObj[0]); 
				}  
			},  
			beforeSend: function () {$(".loaddiv").show();},
			error: function () {$(".loaddiv").hide(); }
		});  
	}  
	//初始加载第一页数据
	getData(1);  
  
	/**
	 * 组织html并装载
	 * @param  {[type]} json [description]
	 * @return {[type]}      [description]
	 */
	function insertDiv(json) {  
		var $mainDiv = $(".mainDiv");  
		var text_1 = json.curp2 > 1 && 'current-data-rise' || 'current-data-fall';
		var text_2 = json.curp2 > 1 && '+' ;
		var text_3 = json.curp2 > 1 && '↑' || '↓';
		var html = '';  
		html += '<div class="contract-list" vbyzc-go="tradeDetail.html?'+json.agreement+'" >';
		html += '	<div class="mytrade-title  ">'+json.type+'</div>';
		html += '	<div class="mytrade-data  ">'+json.startdate+' - '+json.enddate+'</div>';
		html += '	<div class="mytrade-cont">';
		html += '		<label class="'+text_1+'">';
		html += '			<span>当前额度：<span >'+json.curp1+'</span></span>';
		html += '			<div class="current-quota">';
		html += '				<span class="fl">'+text_2+'<span >'+json.curp2+'</span> </span>';
		html += '				<span class="fr">'+text_2+'<span >'+json.curp3+'</span>%<span >'+text_3+'</span></span>';
		html += '			</div>';
		html += '		</label>';
		html += '		<label class="early-warn">亏损警戒线 <br> <span class="font-red fb"><span >'+json.ksjjx+'</span></span></label>';
		html += '		<label class="stop-soss">亏损平仓线 <br> <span class="font-orange fb"><span >'+json.kspcx+'</span></span></label>';
		html += '		<label class="blocked-fund">风险保证金 <br> <span class="font-blue fb"><span >'+json.fxbzj+'</span></span></label>';
		html += '	</div>';
		html += '</div>';
		$mainDiv.append(html);  
	}  

	/*************************操作过程*************************/
	var winH = $(window).height(); //页面可视区域高度   
	var scrollHandler = function () {  
		var pageH = $(document).height();  
		var scrollT = $(window).scrollTop(); //滚动条top   
		var aa = (pageH - winH - scrollT) / winH; 
		if (aa < 0.02) {//0.02是个参数  
			console.log('判断一次',Math.random())
			if (i % 10 === 0) {//每10页停一下
				getData(i);  
				$(window).unbind('scroll');  
				$("#btn_Page").show();  
			} else {  
				getData(i);  
				$("#btn_Page").hide();  
			}  
		}  
	}  

	/*****************************事件*******************************/
	//定义鼠标滚动事件  
	$(window).scroll(scrollHandler);  
  
	//继续加载按钮事件  
	$("#btn_Page").click(function () {  
		getData(i);  
		$(window).scroll(scrollHandler);  
	});  
});  