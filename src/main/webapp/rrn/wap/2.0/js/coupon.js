jQuery(document).ready(function($) {
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	
	/**
	 * [priceControl 输入价格的时候]
	 * @param  {[type]} _this [input自己]
	 * @return {[type]}       [买有！]
	 */
	;function priceControl (_this) {
		_n=+_this.val();
		if(isNaN(_n)) return false; //如果不是数字，不往下跑
		_n = parseInt(_n/1000)*1000 ; //换成1000整数
		chk(_n) && $('#domsFXBZJ').show() || $('#domsFXBZJ').hide(); //是否显示
		// 检测函数
		;function chk (num) {
			if( num < 2000 || num > 1000000) return false;//如果小于2000或大于100万
			return true;
		}
	}

	/**
	 * [choseFXBZJ 选择风险保证金的时候]
	 * @param  {[type]} $this [li元素]
	 * @return {[type]}       [description]
	 */
	;function choseFXBZJ ($this) {
		$this.ac('selected');
		$this.sbl().rc('selected');
	}
	/**
	 * 兑换优惠券
	 * @param  {[type]} argument [description]
	 * @return {[type]}          [description]
	 */
	var couponTool = (function () {
		var couponno = '00000000'; //商城的优惠券类型码
		var points   = 0 ;
		var control  = {};
		//操作过程
		function exchangeCouponOperation(){
			console.log('开始ajax请求,要兑换的优惠券类型是：',couponno);
			$.ajax({
				url: 'json_exchangeCoupon.html',
				type: 'POST',
				dataType:'json',
				data: {exCardType:couponno},
			})
			.done(function(data) {
				if(!$.isEmptyObject(data)){
					$('.my-coupon').append(organizeCouponHtml(data));
					// 重新显示积分
					$('#myPoints').fadeOut(500,function(){
						$('#myPoints').text($('#myPoints').text()-points);
						$(this).fadeIn(1000);
					});
					
				}
			})
			.fail(function() {
				console.log("远程请求失败");
			})
			.always(function() {
				console.log("complete");
			});
		}
		//设置私有数据
		control.setCno = function (cpno,pt) {
			points   = pt
			couponno = cpno ;
			console.log(couponno);
		}
		// 操行兑换操作
		control.exchangedo = function(){
			console.log('要兑换的优惠券：' ,couponno);
			exchangeCouponOperation.call(this,'');//调用
		}
		return control ;
	})()

	/**
	 * [组织优惠券的html结构]
	 * @param  {[type]} data [description]
	 * @return {[type]}      [description]
	 */
	;function organizeCouponHtml (data) {
		var outhtml= '';
		var color = '';
		// 是否失败
		if ($.isEmptyObject(data)) {return '';};
		// 判断颜色
		if(data.card_price < 100){
			color= 'card-blue';
		}else if (data.card_price >=100 && data.card_price<500){
			color= 'card-red' 
		}else if(data.card_price >= 500) {
		 	color= 'card-orange' ;
		}
		// 组织输出
		outhtml += '<div class="coupon-card clearfix '+color+'" data-couponno="'+data.card_no+'">';
		outhtml += ' <ul>';
		outhtml += '	<li> <div class="fl font-size16">￥</div> <div class="fl font-size32 ">'+data.card_price+'</div> </li>';
		outhtml += '	<li><span class="fb ">管理费抵用券</span> <br><span class="font-gray9">有效期至：<span class="">'+data.deadline+'</span></span> </li>';
		outhtml += ' </ul>';
		outhtml += ' </div>';
		return outhtml;
	}

	/**
	 * 添加优惠券过程
	 * @param {[type]} argument [description]
	 */
	;function addCoupon (argument) {
		var input_coupon_no= $('.input_coupon_no').val();
		console.log("发送的优惠券编码：",input_coupon_no);
		$.ajax({
			url: 'json_exchangeCoupon.html',
			type: 'POST',
			dataType:'json',
			data: {input_coupon_no:input_coupon_no},
		})
		.done(function(data) {
			$('.my-coupon').append(organizeCouponHtml(data));
		})
		.fail(function() {
			console.log("远程请求失败");
		})
		.always(function() {
			console.log("complete");
		});
	}

	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	//确认添加优惠券
	$('.JS-addCouponConfirm').on('click', function(event) {
		addCoupon();
	});
	// 显示添加优惠券
	$('.JS-addCoupon').on('click', this, function(event) {
		$('.coupon_add_dom').show();
	});

	//点击兑换优惠券
	$('.JS-cardItem').on('click', this, function(event) {
		var $this= $(this);
		if($('#myPoints').text() > $this.data('needpoint')){
			couponTool.setCno($this.data('couponno'),$this.data('needpoint'));
			$('.exchangeNeedPoint').text($this.data('needpoint')); //对话框上的文字
			$('.coupon_exchange_dom').show();
		}else{
			alert('积分不够！');
		}
	});

	//确定要兑换优惠券
	$('.JS-exchangeCountConfirm').on('click',  function(event) {
		couponTool.exchangedo();
	});

});