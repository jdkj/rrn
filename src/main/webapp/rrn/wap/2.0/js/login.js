// ****************************************************** 全局 ****************************************************** //
var __LOGINMETHOD__ = 1; //登录方法，1为验证码，2为密码


//****************************************************** jquery  ******************************************************//
jQuery(document).ready(function($) {
	////////////////////////【方法与变量】////////////////////////
	/**
	 * [switchLoginMethod 切换登录方式]
	 * @param  {[type]} _N [1为手机验证码，2为密码]
	 * @return {[type]}    [description]
	 */
	;function switchLoginMethod (_N) {
		switch(_N){
			case 1 :
				$('#login-telcode').show();
				$('#login-pass').hide();
				$('.getcode').prop('disabled',false);
				__LOGINMETHOD__=1;
				break;
			defautl:void(0);
		}
	}

	/**
	 * [checkCellNo 手机输入时检测]
	 * @param  {[type]} _input [description]
	 * @return {[type]}        [description]
	 */
	;function checkCellNo (_input) {
		if(/^\d{11}$/gi.test(_input.val())){
			$.log('手机号输入正确');
			// 检测是否注册过
			$.ajax({
				url: 'json_checktel.html',
				type: 'post',
				data: {cellNo:_input.val()},
			})
			.done(function(msg) {
				// alert(!!(+msg));
				if(!!(+msg)){
					$('#login-telcode').hide();
					$('#login-pass').show();
					__LOGINMETHOD__ = 2; //存在手机号就是老用户，2就是密码登录
				}else{
					$('#login-telcode').show();
					$('#login-pass').hide();
					$('.getcode').prop('disabled',false);
					__LOGINMETHOD__ = 1; //不存在手机号，新用户，用验证码
				}
			})
			.fail(function(textStatus) {
				alert('谢谢收看，请求失败，可能是网络问题，或服务器挂了');
			})
		}else{
			console.log('不正确的手机号');
			$('.getcode,.bt-submit').prop('disabled',true);
		}
	}
	/**
	 * [checkCode 输入验证码的时候]
	 * @param  {[type]} _input [验证码本身]
	 * @return {[type]}        [description]
	 */
	;function checkCode (_input) {
		if(/^\d{11}$/gi.test($('#username').val()) && /^[\w\d]{4}$/gi.test(_input.val())){
			console.log('手机正确 ，验证码正确！');
			$('.bt-submit').prop('disabled',false);
		}else{
			$('.bt-submit').prop('disabled',true);
		}
	}

	/**
	 * [checkCode 输入密码的时候]
	 * @param  {[type]} _input [本身]
	 * @return {[type]}        [description]
	 */
	;function checkPassword (_input) {
		if(/^\d{11}$/gi.test($('#username').val()) && /^\S{4}$/gi.test(_input.val())){
			console.log('手机正确 ，密码正确！');
			$('.bt-submit').prop('disabled',false);
		}else{
			$('.bt-submit').prop('disabled',true);
		}
	}

	;function sendCode (_this) {
		alert('哈哈，发送验证码的ajax可以直接写在这区域');
		_this.prop('disabled',true);
		var _count = 0;
		function countdown(){
			_count+=1;
			_this.text(_count+'秒后再重新获取...');
			if(_count>5){
				clearInterval(_timeHandler);
				_this.text('获取短信验证码');
				_this.prop('disabled',false);
			}
		}
		var _timeHandler = setInterval(countdown,1000);
	}

	////////////////////////【事件绑定】////////////////////////
	// 切换为手机验证码登录
	$(document).on('click', '#JS-chageLoginMethod', function(event) {
		switchLoginMethod(1);
	});
	// 输入手机号的时候
	$(document).on('keyup', "#username", function(event) {
		checkCellNo($(this));
	});
	// 输入验证码的时候
	$(document).on('keyup', '#verifyCode', function(event) {
		checkCode($(this));
	});
	// 输入密码的时候
	$(document).on('keyup', '#password', function(event) {
		checkPassword($(this));
	});
	// 获取验证码
	$(document).on('click', '.getcode', function(event) {
		sendCode($(this));
	});
});