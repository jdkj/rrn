jQuery(document).ready(function($) {
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////

	//////////////////////////// 加载要买入或卖出的股票实时数据 //////////////////////////////\
	/**
	 * 【Load_stock_info 当页面进入的时候，肯定是激活买入或买出，然后调用url中的股票代码参数，运行此函数】
	 * 【输入正确股票代码的时候，也调用此函数】
	 * 但要判断手工点击买入或买出标签的时候，不调用此代码
	 * @param {[type]} stockNo    [股票代码]
	 */
	;function Load_stock_info (stockNo) {
		var contractNo = $('#JS-value-hyNo').val();//合约编号，网页加载时即输出到html中
		// $('#JS-input-stockcode').val(stockNo);
		// alert('加载股票信息!合约编号：'+contractNo +'，股票代码：'+stockNo);
		$.ajax({
			url: 'json_buy_sell.html',
			type: 'post',
			dataType: 'json',
			data: {contractNo:contractNo,stockNo:stockNo},
		})
		.done(function(data) {
			//要先在这里判断是否有这个股票代码，没有就提示没有这个股票信息
			if(Object.keys(data).length>0){
				refreshHTML(data);
			} else{
				alert('没有这个股票信息');
			}
		})
		.fail(function() {console.log("error"); })
		// 填充内容
		;function refreshHTML(_JOSN_){
			var o = _JOSN_.showapi_res_body
			// console.table(o);
			$('span.JS-balance-money').text(o.anotherdata.balance); //余额，但也可能直接在HTML中输出
			// $('span.lastPrice').text(o
			// .stockMarket.nowPrice); // 现价
			// $('span.zhangfu').text(o.stockMarket.diff_rate); // 涨幅
			$('span.upperLimitPrice').text(o.stockMarket.upperLimitPrice);//涨停
			$('span.lowerLimitPrice').text(o.stockMarket.lowerLimitPrice);//跌停
			$('#stockname').text(o.stockMarket.name);// 股票名称
			$('#JS-entrust-price').val(o.stockMarket.nowPrice); //委托价格格，其实就是现价
			for (var i = 1 ; i < 6 ; i++) {
				var sellmoney='sell'+i+'_m';
				var sellnum='sell'+i+'_n';
				var buymoney='buy'+i+'_m';
				var buynum='buy'+i+'_n';
				$('.sellPrice'+i).text(o.stockMarket[sellmoney]);
				$('.sellQty'+i).text(o.stockMarket[sellnum]);
				$('.buyPrice'+i).text(o.stockMarket[buymoney]);
				$('.buyQty'+i).text(o.stockMarket[buynum]);
			};
			// 判断是是买还是卖，1买，2卖，只有买的时候，可操作股数才会根据单价的改变而改变
			if ($('#v-post-action').val()=='1') {
				canBuyAmount();
			}else{
				$('span.JS-weituo-max').text(o.anotherdata.stockAvailableNum); //最大可卖股数，获取
			}
		}
	}

	////////////////////////////////////////提交订单//////////////////////////////////////////
	;function Vcontrol_submit () {
		alert('提交！');
		var hyID     = $('#JS-value-hyNo').val();
		var gpID     = $('#JS-input-stockcode').val();
		var wtPrice  = $('#JS-entrust-price').val();
		var wtAMount = $('#JS-entrust-num').val();
		var action   = $('#v-post-action').val();
		var sendObj  = {hyID: hyID,gpID:gpID,wtPrice: wtPrice,wtAMount:wtAMount,action:action};
			console.log(sendObj);
			if(wtPrice !== '0' && wtAMount !== '0'){
				   $.ajax({
					url: '111111111.html',
					type: 'post',
					data: sendObj
				   })
				   .done(function(response) {
					console.log(response);
				   })
				   .fail(function() {
					alert("谢谢收看，请求失败，请稍后再试。");
				   })
			}
	}
	//////////////////////////////////加减价格和股数//////////////////////////////////
		;function minPrice () {
			var $input           = $('#JS-entrust-price');
			var _curPice         = $input.val();
			var _lowerLimitPrice = $('.lowerLimitPrice:first').text();
			if(_curPice>_lowerLimitPrice){
				$input.val((_curPice - 0.01).toFixed(2));
				$('#v-post-action').val()=='1' && canBuyAmount(); //改变委托价格的时候，如果是买入的时候，要重新计算可买股数，并刷新dom
				CountFrozenMoney();//计算冻结资金
			};
		}
		;function addPrice () {
			var $input           = $('#JS-entrust-price');
			var _curPice         = +$input.val();
			var _upperLimitPrice = +$('.upperLimitPrice:first').text();
			if(_curPice < _upperLimitPrice){
				var tmp=(_curPice + 0.01);
				$input.val(tmp.toFixed(2));
				$('#v-post-action').val()=='1' && canBuyAmount();//改变委托价格的时候，如果是买入的时候，要重新计算可买股数，并刷新dom
				CountFrozenMoney();//计算冻结资金
			};
		}
		

	//////////////////////////////////计算可买股数//////////////////////////////////
	;function canBuyAmount () {
		console.log('现在是买入的状态，要重新计算可买股数......');
		var amount = 0;
		var _money =$('.JS-balance-money').text();
		var _price =$('#JS-entrust-price').val();
		$('#JS-entrust-num').val('0'); //当重新计算可买股数，已填写的股数可能会超过，所以要清0
		amount = (parseInt(_money/_price/100)*100); //以100为基数
		$('span.JS-weituo-max').text(amount);
	}

	////////////////////////////////动输入委托价格的限制/////////////////////////////////////
	;function LimitInputPrice (e) {
		var $e = $(e);
		var _C = +$e.val();
		var _U = +$('.upperLimitPrice:first').text();
		var _L = +$('.lowerLimitPrice:first').text();
		( _C > _U || _C < _L || isNaN(_C)) && $e.val('0');
		CountFrozenMoney();//计算冻结资金
	}
	////////////////////////////////动输入股数的限制/////////////////////////////////////
	;function LimitInputStockAmount (e) {
		var $e = $(e);
		var _C = +$e.val();
		var _U = +$('.JS-weituo-max:first').text();
		_C     = parseInt(_C/100)*100; // 以整百的倍数
		_C = _C > _U ? _U : _C; // 不能超过最大数;
		( _C > _U || _C < 0 || isNaN(_C)) && $e.val('0') || $e.val(_C);
		CountFrozenMoney();//计算冻结资金
	}

	//////////////////////////////////////计算冻结资金//////////////////////////////////////////////
	;function CountFrozenMoney () {
		// var _curPice     =$('#JS-entrust-price').val();
		// var _curStockNum =$('#JS-entrust-num').val();
		// $('.JS-dj').text((_curPice*_curStockNum).toFixed());
	}

	//清空读取出来的股票信息，止前是没有顺序逻辑错误，但要注意观察
	;function clearStockInfo (argument) {
		$('.upperLimitPrice,.lowerLimitPrice,.lastPrice,.zhangfu').text('0');
		$('li[class*="sellPrice"],li[class*="buyPrice"],li[class*="sellQty"],li[class*="buyQty"]').text('0');
		$('#JS-entrust-price,#JS-entrust-num').val('0');
	}
	/////////////////////////////////////输入股票代码///////////////////////////////
	;function InputStockNo (e) {
		// 股票代码小于6位数的时候，清空数据，=6位的时候，就加载股票信息
		if(e.val().length<6){
			clearStockInfo();//清空数据
		}else{
			// alert('读取股票信息');
			Load_stock_info(e.val());
		}
	}

	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////

	// 手动输入股票的时候
	$('#JS-input-stockcode').on('keyup',this, function(event) {
		InputStockNo($(this));
	});
	// 买入买出提交,成功则？？ 失败停留在本页面
	
	//加减买入卖出价格，和委托股数相关的绑定
	$('.JS-minPrice').on('click',this, function(event) {minPrice(); });
	$('.JS-addPrice').on('click',this, function(event) {addPrice(); });
	//手动输入价格和股数的事件
	$('#JS-entrust-price').on('blur', this, function(event) { LimitInputPrice(this);});
	$('#JS-entrust-num').on('blur', this, function(event) {LimitInputStockAmount(this);});

	//买入动作
	$('#JS-submit-order').on('click', this, function(event) {
		$('.trade-popup-container').show();
	});
	$(document).on('click', '.JS-action-buyin', function(event) {
		Vcontrol_submit();
	});

	//点击底部所持有的股票
	$(document).on('click', '.JS-loadStockInfo', function(event) {
		Load_stock_info($(this).data('stockno'));
	});
});