jQuery(document).ready(function($) {
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	
	/**
	 * [priceControl 输入价格的时候]
	 * @param  {[type]} _this [input自己]
	 * @return {[type]}       [买有！]
	 */
	;function priceControl (_this) {
		_n=+_this.val();
		if(isNaN(_n)) return false; //如果不是数字，不往下跑
		_n = parseInt(_n/1000)*1000 ; //换成1000整数
		chk(_n) && $('#domsFXBZJ').show() || $('#domsFXBZJ').hide(); //是否显示
		// 检测函数
		;function chk (num) {
			if( num < 2000 || num > 1000000) return false;//如果小于2000或大于100万
			return true;
		}
	}

	/**
	 * [choseFXBZJ 选择风险保证金的时候]
	 * @param  {[type]} $this [li元素]
	 * @return {[type]}       [description]
	 */
	;function choseFXBZJ ($this) {
		$this.ac('selected');
		$this.sbl().rc('selected');
	}


	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	
	// 今日限买股弹出
	$('#JS-todayLimitBuy').on('click', this, function(event) {
		action_showPopup('#popup-todayLimit');
	});
	//今日限买股关闭
	$('.trade-popup-container').on('click', this, function(event) {
		(event.target==this || event.target==$('.trade-popup-buttons>button')[0]) && $(this).hide();
	});

	// 输入钱的时候,输入正确才会显示保证金位数，不做其它处理
	$('#input-money').on('keyup', this, function(event) {
		priceControl($(this));
	});

	// 选择风险保证金的时候
	$('.JS-fxbzj').on('click', 'li', function(event) {
		choseFXBZJ($(this));
	});

	//显示优惠券层
	$('.JS-showCoupon').on('click',  function(event) {
		$('#popup-coupon,.coupon-box').show();
		//$(window).scrollTop(0);
		$('#popup-coupon').height($(document).height());
		//console.log($(document).height());
		
	});

	//点击了消失透明层
	$('#popup-coupon').on('click', function(event) {
		$(this).hide();
		$('.coupon-box').hide();
	});

	// 点击选择优惠券
	$('.coupon-card').on('click',  function(event) {
		$('.iconcheck').rc('icon-yixuanze').ac('icon-xuanze');
		$(this).find(".iconcheck").ac('icon-yixuanze').rc('icon-xuanze');
		$('.popup-container,.coupon-box').fadeOut();
	});
});