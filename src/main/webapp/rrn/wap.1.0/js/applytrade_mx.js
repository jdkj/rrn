jQuery(document).ready(function($) {
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
	
	/**
	 * [priceControl 输入价格的时候]
	 * @param  {[type]} _this [input自己]
	 * @return {[type]}       [买有！]
	 */
	;function priceControl (_this) {
		_n=+_this.val();
		if(isNaN(_n)) return false; //如果不是数字，不往下跑
		_n = parseInt(_n/1000)*1000 ; //换成1000整数
		chk(_n) && $('#domsFXBZJ').show() || $('#domsFXBZJ').hide(); //是否显示
		// 检测函数
		;function chk (num) {
			if( num < 2000 || num > 300000) return false;//如果小于2000或大于100万
			return true;
		}
	}

	/**
	 * [choseFXBZJ 选择风险保证金的时候]
	 * @param  {[type]} $this [li元素]
	 * @return {[type]}       [description]
	 */
	;function choseCPLX ($this) {
		$this.ac('selected');
		$this.sbl().rc('selected');
		if($this.index()==0){$('.trade-t1').show();}else{$('.trade-t1').hide()};
	}


	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
	
	// 今日限买股弹出
	$(document).on('click', '#JS-todayLimitBuy', function(event) {
		action_showPopup('#popup-todayLimit');
	});
	//今日限买股关闭
	$(document).on('click', '.trade-popup-container', function(event) {
		(event.target==this || event.target==$('.trade-popup-buttons>button')[0]) && $(this).hide();
	});

	// 输入钱的时候
	$(document).on('keyup', '#input-money', function(event) {
		priceControl($(this));
	});

	// 选择风险保证金的时候
	$('.JS-xplx').on('click', 'li', function(event) {
		choseCPLX($(this));
	});
});