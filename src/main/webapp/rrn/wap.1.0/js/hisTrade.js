jQuery(document).ready(function($) {
	//////////////////////////////////////////////////方法与变量/////////////////////////////////////////////////
	/**
	 * [showShareDom 判断是不是微信，显示不同的分享提示]
	 * @param  {[type]} argument [description]
	 * @return {[type]}          [description]
	 */
	;function showShareDom (argument) {
		if(is_weixin()){
			$('#dom-share-weixin').show() 
		}else{
			 $('#dom-share-normal').show();
		 }
	}

	//////////////////////////////////////////////////事件绑定/////////////////////////////////////////////////
	// 分享层关闭
	$(document).on('click', '.share-tips', function(event) {
		$(this).hide();;
	});
	// 点击分享的时候
	$(document).on('click', '.sharebtn', function(event) {
		showShareDom();
	});

});