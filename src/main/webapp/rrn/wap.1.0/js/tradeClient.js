/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////
/////////////////////////////////////////////////////////方法与变量///////////////////////////////////////////////////////

//////////////////////////// 加载要买入或卖出的股票实时数据 //////////////////////////////\
/**
 * 【Load_stock_info 当页面进入的时候，肯定是激活买入或买出，然后调用url中的股票代码参数，运行此函数】
 * 【输入正确股票代码的时候，也调用此函数】
 * 但要判断手工点击买入或买出标签的时候，不调用此代码
 * @param {[type]} stockNo    [股票代码]
 */
;function Load_stock_info(stockNo) {
    var contractNo = $('#JS-value-hyNo').val();//合约编号，网页加载时即输出到html中
    // $('#JS-input-stockcode').val(stockNo);
    // alert('加载股票信息!合约编号：'+contractNo +'，股票代码：'+stockNo);
    $.ajax({
        url: '/rest/trade/queryStock',
        type: 'post',
        dataType: 'json',
        data: {"agreementId": contractNo, "stockCode": stockNo},
    })
        .done(function (data) {
            //要先在这里判断是否有这个股票代码，没有就提示没有这个股票信息
            if (Object.keys(data).length > 0 && data.resultFlag) {
                refreshHTML(data);
            } else {
                toast(data.resultMsg);
            }
        })
        .fail(function () {
            toast("股票信息查询异常,请稍后重试");
        })
    // 填充内容
    ;
    function refreshHTML(_JOSN_) {
        var o = _JOSN_.data;
        // console.table(o);
        $('span.JS-balance-money').text(o.agreementInfo.avaiableCredit); //余额，但也可能直接在HTML中输出
        // $('span.lastPrice').text(o
        // .stockMarket.nowPrice); // 现价
        // $('span.zhangfu').text(o.stockMarket.diff_rate); // 涨幅
        $('span.upperLimitPrice').text(o.stockMarket.maxPrice);//涨停
        $('span.lowerLimitPrice').text(o.stockMarket.minPrice);//跌停
        $('#stockname').text(o.stockMarket.name);// 股票名称
        $('#JS-entrust-price').val(o.stockMarket.nowPrice); //委托价格格，其实就是现价

        for (var i = 1; i < 6; i++) {
            var sellmoney = 'sell' + i + '_m';
            var sellnum = 'sell' + i + '_n';
            var buymoney = 'buy' + i + '_m';
            var buynum = 'buy' + i + '_n';
            $('.sellPrice' + i).text(o.stockMarket[sellmoney]);
            $('.sellQty' + i).text((parseInt(o.stockMarket[sellnum])/100).toFixed(0));
            $('.buyPrice' + i).text(o.stockMarket[buymoney]);
            $('.buyQty' + i).text((parseInt(o.stockMarket[buynum])/100).toFixed(0));
        }
        ;
        // 判断是是买还是卖，1买，2卖，只有买的时候，可操作股数才会根据单价的改变而改变
        if ($('#v-post-action').val() == '1') {
            canBuyAmount();
        } else {
            $('span.JS-weituo-max').text(o.agreementInfo.stockAvailableNum); //最大可卖股数，获取
        }
    }
}

////////////////////////////////////////提交订单//////////////////////////////////////////
;function Vcontrol_submit() {
    $('.trade-popup-container').hide();
    var hyID = $('#JS-value-hyNo').val();
    var gpID = $('#JS-input-stockcode').val();
    var wtPrice = $('#JS-entrust-price').val();
    var wtAMount = $('#JS-entrust-num').val();
    var action = +$('#v-post-action').val();
    var commissionType = +$("#commissionType").val();
    var _url = "";
    if (action === 1) {//买
        _url = "/rest/trade/operate/buy";
    } else {//卖
        _url = "/rest/trade/operate/sell";
    }
    var sendObj = {
        "agreementId": hyID,
        "stockCode": gpID,
        "commissionPrice": wtPrice,
        "commissionNum": wtAMount,
        "commissionType": commissionType
    };

    if (wtPrice !== '0' && wtAMount !== '0') {
        $.ajax({
            url: _url,
            type: 'post',
            data: sendObj
        })
            .done(function (response) {
                if (response.resultFlag) {
                    alert(response.resultMsg);
                    window.location.href = window.location.href;
                } else {
                    alert(response.resultMsg);
                }
            })
            .fail(function () {
                toast("请求失败，请稍后再试!");
            })
    } else {
        toast("请输入委托股数");
    }
}
//////////////////////////////////加减价格和股数//////////////////////////////////
;function minPrice() {
    var $input = $('#JS-entrust-price');
    var _curPice = +$input.val();
    var _lowerLimitPrice = +$('.lowerLimitPrice:first').text();
    if (_curPice > _lowerLimitPrice) {
        $input.val((_curPice - 0.01).toFixed(2));
        $('#v-post-action').val() == '1' && canBuyAmount(); //改变委托价格的时候，如果是买入的时候，要重新计算可买股数，并刷新dom
        CountFrozenMoney();//计算冻结资金
    }
    ;
}
;function addPrice() {
    var $input = $('#JS-entrust-price');
    var _curPice = +$input.val();
    var _upperLimitPrice = +$('.upperLimitPrice:first').text();
    if (_curPice < _upperLimitPrice) {
        var tmp = (_curPice + 0.01);
        $input.val(tmp.toFixed(2));
        $('#v-post-action').val() == '1' && canBuyAmount();//改变委托价格的时候，如果是买入的时候，要重新计算可买股数，并刷新dom
        CountFrozenMoney();//计算冻结资金
    }
    ;
}


//////////////////////////////////计算可买股数//////////////////////////////////
;function canBuyAmount() {

    var amount = 0;
    var _money = $('.JS-balance-money').text();
    var _price = $('#JS-entrust-price').val();

    $('#JS-entrust-num').val('0'); //当重新计算可买股数，已填写的股数可能会超过，所以要清0
    amount = (parseInt(_money * 0.98 / _price / 100) * 100); //以100为基数
    $('span.JS-weituo-max').text(amount);
}

////////////////////////////////动输入委托价格的限制/////////////////////////////////////
;function LimitInputPrice(e) {
    var $e = $(e);
    var _C = +$e.val();
    var _U = +$('.upperLimitPrice:first').text();
    var _L = +$('.lowerLimitPrice:first').text();
    ( _C > _U || _C < _L || isNaN(_C)) && $e.val('0');
    CountFrozenMoney();//计算冻结资金
}
////////////////////////////////动输入股数的限制/////////////////////////////////////
;function LimitInputStockAmount(e) {
    var $e = $(e);
    var _C = +$e.val();
    var _U = +$('.JS-weituo-max:first').text();
    _C = parseInt(_C / 100) * 100; // 以整百的倍数
    _C = _C > _U ? 0 : _C; // 不能超过最大数;超过这变为0
    ( _C > _U || _C < 0 || isNaN(_C)) && $e.val('0') || $e.val(_C);
    CountFrozenMoney();//计算冻结资金
}

//////////////////////////////////////计算冻结资金//////////////////////////////////////////////
;function CountFrozenMoney() {
    // var _curPice     =$('#JS-entrust-price').val();
    // var _curStockNum =$('#JS-entrust-num').val();
    // $('.JS-dj').text((_curPice*_curStockNum).toFixed());
}

//清空读取出来的股票信息，止前是没有顺序逻辑错误，但要注意观察
;function clearStockInfo(argument) {
    $('.upperLimitPrice,.lowerLimitPrice,.lastPrice,.zhangfu').text('0');
    $('li[class*="sellPrice"],li[class*="buyPrice"],li[class*="sellQty"],li[class*="buyQty"]').text('0');
    $('#JS-entrust-price,#JS-entrust-num').val('0');
    $('.JS-weituo-max').html(0);
    $("#stockname").html("");
}
/////////////////////////////////////输入股票代码///////////////////////////////
;function InputStockNo(e) {
    // 股票代码小于6位数的时候，清空数据，=6位的时候，就加载股票信息
    if (e.val().length < 6) {
        clearStockInfo();//清空数据
    } else {
        // alert('读取股票信息');
        Load_stock_info(e.val());
    }
};
//买入或卖出前进行再次确认
function confirmInfo() {
    var gpID = $('#JS-input-stockcode').val();
    var wtPrice = $('#JS-entrust-price').val();
    var wtAMount = $('#JS-entrust-num').val();
    var stockName = $("#stockname").html();
    var container = $('.trade-popup-container');
    var commissionType = $("#commissionType").val();
    if (commissionType == 1) {
        wtPrice = "市价";
    }
    //清空数据
    container.find(".popup-value").html("");
    container.find(".popup-stock-code").html(gpID);
    container.find(".popup-stock-name").html(stockName);
    container.find(".popup-stock-commission-price").html(wtPrice);
    container.find(".popup-stock-commission-amount").html(wtAMount);
    container.show();
}
jQuery(document).ready(function ($) {
    /////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////事件绑定区域///////////////////////////////////////////////////////

    // 手动输入股票的时候
    $('#JS-input-stockcode').on('keyup', this, function (event) {
        InputStockNo($(this));
    });
    // 买入买出提交,成功则？？ 失败停留在本页面

    //加减买入卖出价格，和委托股数相关的绑定
    $('.JS-minPrice').on('click', this, function (event) {
        minPrice();
    });
    $('.JS-addPrice').on('click', this, function (event) {
        addPrice();
    });
    //手动输入价格和股数的事件
    $('#JS-entrust-price').on('blur', this, function (event) {
        LimitInputPrice(this);
    });
    $('#JS-entrust-num').on('blur', this, function (event) {
        LimitInputStockAmount(this);
    }).on('focus', this, function () {
        var _val = +$(this).val();
        if (_val === 0){
            $(this).val('');
        }
    });

    //交易确认---限价
    $('#JS-submit-order').on('click', this, function (event) {
        $("#commissionType").val(0);
        if (+$('#JS-entrust-num').val() > 0) {
            confirmInfo();
        } else {
            var action = +$('#v-post-action').val();
            if (action == 1) {
                toast("请输入委托股数");
            } else {
                toast("当前无可卖股数");
            }
        }
    });
    //交易确认----市价
    $("#JS-submit-commission").on('click', this, function () {
        $("#commissionType").val(1);
        if (+$('#JS-entrust-num').val() > 0) {
            confirmInfo();
        } else {
            var action = +$('#v-post-action').val();
            if (action == 1) {
                toast("请输入委托股数");
            } else {
                toast("当前无可买股数");
            }
        }
    });


    $( '.JS-action-buyin').on('click',this, function (event) {
        Vcontrol_submit();
    });

    //点击底部所持有的股票
    $(".JS-loadStockInfo").on('click', this, function (event) {
        $("#JS-input-stockcode").val($(this).data('stockno'));
        Load_stock_info($(this).data('stockno'));
    });
});