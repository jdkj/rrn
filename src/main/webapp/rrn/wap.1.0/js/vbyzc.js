//////////////////////////////////////////////////////全局变量和函数///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////全局变量和函数///////////////////////////////////////////////////////////
//////////////////////////////////////////////////////全局变量和函数///////////////////////////////////////////////////////////

/**
 * [action_showPopup 显示浮动层]
 * @param  {[type]} idNo [带入选择器字符]
 * @return {[type]}      [description]
 */
;function action_showPopup (idNo) {
	var _target= $(idNo);
	_target.show();
}

/**
 * [GetQueryString 获取url参数]
 * @param {[type]} name [变量名]
 */
;function GetQueryString(name){
	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if(r!=null)return  unescape(r[2]); return null;
}

/**
 * [is_weixin 判断是否微信浏览器]
 * @return {Boolean} [哦哦哦]
 */
;function is_weixin(){
	var ua = navigator.userAgent.toLowerCase();
	if(ua.match(/MicroMessenger/i)=="micromessenger") {
		return true;
 	} else {
		return false;
	}
};
/**
 * 初始化对话框
 */
function showDialog(content, confirm_msg, confirm_callback, cancel_msg, cancel_callback) {

    if ($(".dsmart-dialog-container").length > 0) {
        removeDialog();
    }

    var html = "<div class='dsmart-dialog-container'>" +
        "<header class='dsmart-dialog-header'>" +
        "<div class='dsmart-dialog-close-btn'>X</div>" +
        "</header>" +
        "<div class='dsmart-dialog-content'>" +
        content +
        "</div>" +
        "<div class='dsmart-dialog-progress-container'>" +
        "<button class='dsmart-dialog-cancel-btn'>" + cancel_msg + "</button>" +
        "<button class='dsmart-dialog-confirm-btn'>" + confirm_msg + "</button></div></div>";

    $("body").append(html);

    if (typeof cancel_callback == 'function') {
        $(".dsmart-dialog-cancel-btn").on('click', this, function () {
            cancel_callback();
        });
    } else {
        $(".dsmart-dialog-cancel-btn").on('click', this, function () {
            defaulCancel();
        });
    }

    if (typeof confirm_callback == 'function') {
        $(".dsmart-dialog-confirm-btn").on('click', this, function () {
            confirm_callback();
        });
    } else {
        $(".dsmart-dialog-confirm-btn").on('click', this, function () {
            defaultConfirm();
        });
    }
    $(".dsmart-dialog-close-btn").on('click', this, function () {
        removeDialog();
    })

};

/**
 * 移除对话框
 */
function removeDialog() {
    $(".dsmart-dialog-container").remove();
};

function defaulCancel() {
    removeDialog();
};

function defaultConfirm() {
    removeDialog();
};
/**
 * toast
 * @param msg
 */
function toast(msg){
    if ($("#ds-tool-toast").length > 0) {
        $("#ds-tool-toast").remove();
    }
    var toastHtml = "<div id='ds-tool-toast'>" + msg + "</div>";
    $("body").append(toastHtml);
    setTimeout(function () {
        $("#ds-tool-toast").remove();
    }, 2000);
};

/////////////////////////////////////////////////////////////jquery/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////jquery/////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////jquery/////////////////////////////////////////////////////////////////////////
$(function(){
	// *********************************插件集合*********************************
	// *********************************插件集合*********************************
	(function($,window,document,undefined){
		// 方便操作
		$.fn.ac=function(n){return this.addClass(n);}
		$.fn.rc=function(n){return this.removeClass(n);}
		$.fn.son=function(n){return this.children(n);}
		$.fn.sbl=function(n){return this.siblings(n)};
		$.extend ({
			log : function(str){
				var t = Array.prototype.slice.call(arguments,0);
				console.log(t.join(''));
			}

		});

		//手机端基于jquery的toast



	})(jQuery,window,document);
	// *******************************事件绑定*********************************
	// *******************************事件绑定*********************************
	// 顶部菜单
	$(document).on('click', '.v-action', function(event) {
		var $this=$(this);
		$(this).toggleClass('listhover');
		$("#topHideMenulist").toggleClass('hover');
		event.preventDefault();
	});

	// 绑定单击跳转的元素
	$(document).on('click', "*[vbyzc-href],*[vbyzc-go]", function(event) {
		var hrefurl = '';
		hrefurl= $(this).attr('vbyzc-href')!==undefined ? $(this).attr('vbyzc-href') : $(this).attr('vbyzc-go');

		//清除手机端页面的缓存，不然有时候点击都不会跳转
		if(hrefurl.indexOf("?")>0){
			hrefurl +="&_t="+new Date().getTime();
		}else{
			hrefurl +="?_t="+new Date().getTime();
		}
		window.location.href=hrefurl;
	});
	
	// 统一的对话框事件 : 点击灰层
	$('.popup-container').on('click',this , function(event) {
		(event.target==this) && $(this).fadeOut(200,function(){$(this).hide();});
	});
	// 统一的对话框事件 : 点击取消
	$('.JS-popup-cancle').on('click',this , function(event) {
		$(this).closest('.popup-container').fadeOut(200,function(){
			$(this).hide();
		});
	});

	(function(){
		$('#topHideMenulist').height($(window).height());
	})();
});