// ****************************************************** 全局 ****************************************************** //
var __LOGINMETHOD__ = 1; //登录方法，1为验证码，2为密码


//****************************************************** jquery  ******************************************************//
jQuery(document).ready(function($) {
	////////////////////////【方法与变量】////////////////////////
	/**
	 * [switchLoginMethod 切换登录方式]
	 * @param  {[type]} _N [1为手机验证码，2为密码]
	 * @return {[type]}    [description]
	 */
	;function switchLoginMethod (_N) {
		switch(_N){
			case 1 :
				$('#login-telcode').show();
				$('#login-pass').hide();
				$('.getcode').prop('disabled',false);
				__LOGINMETHOD__=1;
				break;
			default:
				break;
		}
	}

	/**
	 * [checkCellNo 手机输入时检测]
	 * @param  {[type]} _input [description]
	 * @return {[type]}        [description]
	 */
	;function checkCellNo (_input) {
		if(/^1\d{10}$/gi.test(_input.val())){

			// 检测是否注册过
			$.ajax({
				url: '/rest/user/switch',
				type: 'post',
				data: {"phone":_input.val()},
			})
				.done(function(res) {

					if(res.resultFlag){
						$('#login-telcode').hide();
						$('#login-pass').show();
						__LOGINMETHOD__ = 2; //存在手机号就是老用户，2就是密码登录
					}else{
						$('#login-telcode').show();
						$('#login-pass').hide();
						$('.getcode').prop('disabled',false);
						__LOGINMETHOD__ = 1; //不存在手机号，新用户，用验证码
					}
				})
				.fail(function(textStatus) {
					alert('请求异常，请稍后重试!');
				})
		}else{
			$('.getcode,.bt-submit').prop('disabled',true);
		}
	}

	/**
	 * 检测输入的手机号格式是否基本正确
	 */
	;function validPhoneNum(phoneNo){
		if(/^1\d{10}$/gi.test(phoneNo)){
			return true;
		}else{
			alert("请输入正确的手机号");
			return false;
		}
	}
	/**
	 * [checkCode 输入验证码的时候]
	 * @param  {[type]} _input [验证码本身]
	 * @return {[type]}        [description]
	 */
	;function checkCode (_input) {
		if(/^1\d{10}$/gi.test($('#username').val()) && /^[\d]{5}$/gi.test(_input.val())){
			$('.bt-submit').prop('disabled',false);
		}else{
			$('.bt-submit').prop('disabled',true);
		}
	};
	/**
	 * 校验短信随机码
	 * @returns {boolean}
	 */
	function validCode(){
		if(!/^[\d]{5}$/gi.test($("#verifyCode").val())){
			alert("请输入正确的验证码");
			return false;
		}
		return true;
	}

	/**
	 * [checkCode 输入密码的时候]
	 * @param  {[type]} _input [本身]
	 * @return {[type]}        [description]
	 */
	;function checkPassword (_input) {
		if(/^\d{11}$/gi.test($('#username').val()) && _input.val().length>5){
			$('.bt-submit').prop('disabled',false);
		}else{
			$('.bt-submit').prop('disabled',true);
		}
	}

	/**
	 * 验证登录密码格式
	 */
	;function validPassword(password){
		if(password===""){
			alert("请输入密码");
			return false;
		}else if(password.length<6){
			alert("密码不能少于6位");
			return false;
		}
		return true;
	}

	/**
	 * 获取短信验证码
	 */
	;function sendCode (_this) {
		_this.prop('disabled',true);
		var phone = $("#username").val();
		if(!validPhoneNum(phone)){
			_this.prop('disabled',false);
			return false;
		}

		var _count = 60;
		function countdown(){
			_count--;
			_this.text(_count+'秒后再重新获取...');
			if(_count===0){
				clearInterval(_timeHandler);
				_this.text('获取短信验证码');
				_this.prop('disabled',false);
			}
		}
		var _timeHandler = setInterval(countdown,1000);
		var data = {"phone":phone,"reqType":0};

		$.post("/rest/user/reqCode",data,function(res){
			if(res.resultFlag){
				alert(res.data.codes );
			}else{
				alert(res.resultMsg);
			}
			return false;
		},"json");
	};
	function validCodeOrPassword(){
		if(__LOGINMETHOD__===1){
			return (validCode());
		}else{
			return validPassword($("#password").val());
		}
	}
	/**
	 * 登录或注册
	 */
	;function login(){
		var phone = $("#username").val();
		var code = $("#verifyCode").val();
		var password = $("#password").val();
		var data ;

		var _url = "/rest/user/registe";
		if(__LOGINMETHOD__===2){
			_url = "/rest/user/login/asyc";
			data = {"phone":phone,"userPassword":password};

		}else{
			data = {"phone":phone,"codes":code};
		}
		if(validPhoneNum(phone)&&validCodeOrPassword(code)){
			$.post(_url,data,function(res){
				if(res.resultFlag){
					window.location.href="/";
				}else{
					alert(res.resultMsg);
				}
			},"json");
		}
	}

	////////////////////////【事件绑定】////////////////////////
	// 切换为手机验证码登录
	$(document).on('click', '#JS-chageLoginMethod', function(event) {
		switchLoginMethod(1);
	});
	// 输入手机号的时候
	$(document).on('keyup', "#username", function(event) {
		checkCellNo($(this));
	});
	// 输入验证码的时候
	$(document).on('keyup', '#verifyCode', function(event) {
		checkCode($(this));
	});
	// 输入密码的时候
	$(document).on('keyup', '#password', function(event) {
		checkPassword($(this));
	});
	// 获取验证码
	$(document).on('click', '.getcode', function(event) {
		sendCode($(this));
	});
	//登录或注册
	$(document).on('click', '.bt-submit', function(event) {
		login();
	});
});