/**
 * Created by Dsmart on 2016/11/4.
 */
$(function () {
    /////////////////////////////////////////////////////////////////////事件绑定//////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////事件绑定//////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////事件绑定//////////////////////////////////////////////////////////////////////////////////////


    // ////大tab,这里如果激活第1个和第4个，会出乱子
    // var actionCode = GetQueryString('act')=='buy' ? 1 : 2;
    // $(".JS-tradeclient-nav").tab_trade_1({activeNo:actionCode,contBox:".vbyzcbox"});

    //页面一进入，加载股票信息, 这个一定要放在激活大标签的下面，因为上面会先触发标签事件清空数据
    // Load_stock_info(GetQueryString('v2'));

    //查询数据表tab
    // $(".queryTab").tabdataShow({activeNo:3,contBox:".ListqueryBox"});
    //
    // //翻页的样式绑定
    // $('.pagination-sm').turnpage();
    //
    // // 撒单
    // $('body').on('click', '.JS-cedan', function(event) {
    // 	event.preventDefault();
    // 	Vcontrol_cedan.call($(this),$(this).data('stockNo'));
    // });

    //持仓买入和买出的点击事件
    $('body').on('click', '.JS-buysell', function (event) {
        event.preventDefault();
        //alert($(this).data('type') !== 'buy' && '卖' || '买');
        Vcontrol_buy_sell.call($(this), $(this).data('type'));
    });

    // 手动输入股票的时候
    $('#JS-input-stockcode').on('keyup', this, function (event) {
        InputStockNo($(this));
    });
    // 买入买出提交,成功则？？ 失败停留在本页面

    //加减买入卖出价格，和委托股数相关的绑定
    $('.JS-minPrice').on('click', this, function (event) {
        minPrice();
    });
    $('.JS-addPrice').on('click', this, function (event) {
        addPrice();
    });
    $('.JS-minNum').on('click', this, function (event) {
        minNum();
    });
    $('.JS-addNum').on('click', this, function (event) {
        addNum();
    });
    //手动输入价格和股数的事件
    $('#JS-entrust-price').on('blur', this, function (event) {
        LimitInputPrice(this);
    });
    $('#JS-entrust-num').on('blur', this, function (event) {
        LimitInputStockAmount(this);
    });

    //提交买卖
    $('#JS-submit-order').on('click', this, function (event) {
        Vcontrol_submit();
    });


});


//清空读取出来的股票信息，止前是没有顺序逻辑错误，但要注意观察
;function clearStockInfo() {
    $('.upperLimitPrice,.lowerLimitPrice,.lastPrice,.zhangfu').text('0');
    $('li[class*="sellPrice"],li[class*="buyPrice"],li[class*="sellQty"],li[class*="buyQty"]').text('0');
    $('#JS-entrust-price,#JS-entrust-num').val('0');
}


//////////////////////////// 加载要买入或卖出的股票实时数据 //////////////////////////////\
/**
 * 【Load_stock_info 当页面进入的时候，肯定是激活买入或买出，然后调用url中的股票代码参数，运行此函数】
 * 【输入正确股票代码的时候，也调用此函数】
 * 但要判断手工点击买入或买出标签的时候，不调用此代码
 * @param {[type]} stockNo    [股票代码]
 */
;function Load_stock_info(stockNo) {
    var contractNo = $('#JS-value-hyNo').val();//合约编号，网页加载时即输出到html中
    $('#JS-input-stockcode').val(stockNo);
    // alert('加载股票信息!合约编号：'+contractNo +'，股票代码：'+stockNo);
    $.ajax({
        url: '/rest/trade/queryStock',
        type: 'post',
        dataType: 'json',
        data: {"agreementId": contractNo, "stockCode": stockNo},
    })
        .done(function (data) {
            //要先在这里判断是否有这个股票代码，没有就提示没有这个股票信息
            if (Object.keys(data).length > 0 && data.resultFlag) {
                refreshHTML(data);
            } else {
                alert(data.resultMsg);
            }
        })
        .fail(function () {
            alert("请求异常，请稍后重试!");
        })

    ;
    function refreshHTML(_JOSN_) {
        var o = _JOSN_.data;
        // console.table(o);
        // 填充内容

        $('span.JS-balance-money').text(o.agreementInfo.avaiableCredit); //余额，但也可能直接在HTML中输出
        $("span.JS-stockName").text(o.stockMarket.name);
        $('span.lastPrice').text(o.stockMarket.nowPrice); // 现价
        $('span.zhangfu').text(o.stockMarket.diff_rate); // 涨幅
        $('span.upperLimitPrice').text(o.stockMarket.maxPrice);//涨停
        $('span.lowerLimitPrice').text(o.stockMarket.minPrice);//跌停
        $('#JS-entrust-price').val(o.stockMarket.nowPrice); //委托价格格，其实就是现价
        for (var i = 1; i < 6; i++) {
            var sellmoney = 'sell' + i + '_m';
            var sellnum = 'sell' + i + '_n';
            var buymoney = 'buy' + i + '_m';
            var buynum = 'buy' + i + '_n';
            $('li.sellPrice' + i).text(o.stockMarket[sellmoney]);
            $('li.sellQty' + i).text(o.stockMarket[sellnum]);
            $('li.buyPrice' + i).text(o.stockMarket[buymoney]);
            $('li.buyQty' + i).text(o.stockMarket[buynum]);
        }
        ;
        // 判断是是买还是卖，1买，2卖，只有买的时候，可操作股数才会根据单价的改变而改变
        if ($('#v-post-action').val() == '1') {
            canBuyAmount();
        } else {
            $('span.JS-weituo-max').text(o.agreementInfo.stockAvailableNum); //最大可买股数
        }
        //单独判断涨幅是绿色还是红色
    }
}


// /////////////////////////////////// 撒单操作 ///////////////////////////////////
/**
 * [Vcontrol_cedan 撒单操作]
 * @param {[type]} _stockNo [股票代码,从按钮中获取]
 */
;function Vcontrol_cedan(_stockNo) {
    var $iconDom = this;
    var removeRow = function (e) {
        e.parent().parent('tr').fadeOut('fast', function () {
            $(this).remove();
        });
    }
    // 请求
    $.ajax({
        url: 'json_cedan.html',
        type: 'post',
        data: {stockNo: _stockNo},
    })
        .done(function (msg) {
            !!(+msg) && removeRow.call(this, $iconDom);
        })
        .fail(function (textStatus) {
            alert('谢谢收看，请求失败，可能是网络问题，或服务器挂了');
        })
}

////////////////////////////////////////提交订单//////////////////////////////////////////
;function Vcontrol_submit() {
    var hyID = $('#JS-value-hyNo').val();//合约编号
    var gpID = $('#JS-input-stockcode').val();//股票编码
    var wtPrice = $('#JS-entrust-price').val();//价格
    var wtAMount = $('#JS-entrust-num').val();//数量
    var action = +$('#v-post-action').val();//买入?卖出
    var _url = "";
    if (action === 1) {//买
        _url = "/rest/trade/operate/buy";
    } else {//卖
        _url = "/rest/trade/operate/sell";
    }
    var sendObj = {"agreementId": hyID, "stockCode": gpID, "commissionPrice": wtPrice, "commissionNum": wtAMount};

    if (wtPrice !== '0' && wtAMount !== '0') {
        $.ajax({
            url: _url,
            type: 'post',
            data: sendObj
        })
            .done(function (response) {
                if (response.resultFlag) {
                    alert(response.resultMsg, function () {
                        window.location.href = window.location.href;
                    });

                } else {
                    alert(response.resultMsg);
                }
            })
            .fail(function () {
                alert("请求失败，请稍后再试。");
            })
    }
}

/////////////////////////////////////输入股票代码///////////////////////////////
;function InputStockNo(e) {
    // 股票代码小于6位数的时候，清空数据，=6位的时候，就加载股票信息
    if (e.val().length < 6) {
        clearStockInfo();//清空数据
    } else {
        // alert('读取股票信息');
        Load_stock_info(e.val());
    }
}
//////////////////////////////////加减价格和股数//////////////////////////////////
;function minPrice() {
    var $input = $('#JS-entrust-price');
    var _curPice = $input.val();
    var _lowerLimitPrice = $('.lowerLimitPrice:first').text();
    if (_curPice > _lowerLimitPrice) {
        $input.val((_curPice - 0.01).toFixed(2));
        $('#v-post-action').val() == '1' && canBuyAmount(); //改变委托价格的时候，如果是买入的时候，要重新计算可买股数，并刷新dom
        CountFrozenMoney();//计算冻结资金
    }
    ;
}
;function addPrice() {
    var $input = $('#JS-entrust-price');
    var _curPice = +$input.val();
    var _lowerLimitPrice = +$('.lowerLimitPrice:first').text();
    var _lastPrice = +$(".lastPrice").text();
    _curPice = _curPice === 0 ? _lastPrice : _curPice;
    var _upperLimitPrice = $('.upperLimitPrice:first').text();
    if (_curPice < _upperLimitPrice) {
        var tmp = (_curPice + 0.01);
        $input.val(tmp.toFixed(2));
        $('#v-post-action').val() == '1' && canBuyAmount();//改变委托价格的时候，如果是买入的时候，要重新计算可买股数，并刷新dom
        CountFrozenMoney();//计算冻结资金
    }
    ;
}
;function minNum() {
    var $input = $('#JS-entrust-num');
    var _curNum = $input.val();
    if (_curNum > 0) {
        $input.val(_curNum - 100);
        CountFrozenMoney();//计算冻结资金
    }
    ;
}
;function addNum() {
    var $input = $('#JS-entrust-num');
    var _curNum = $input.val();
    var _JS_weituo_max = $('.JS-weituo-max:first').text();
    if (+_curNum < _JS_weituo_max) {
        $input.val(+_curNum + 100);
        CountFrozenMoney();//计算冻结资金
    }
    ;
}

//////////////////////////////////计算可买股数//////////////////////////////////
;function canBuyAmount() {
    var amount = 0;
    var _money = $('.JS-balance-money').text();
    var _price = $('#JS-entrust-price').val();
    $('#JS-entrust-num').val('0'); //当重新计算可买股数，已填写的股数可能会超过，所以要清0
    amount = (parseInt(_money / _price / 100) * 100); //以100为基数
    $('span.JS-weituo-max').text(amount);
}

////////////////////////////////动输入委托价格的限制/////////////////////////////////////
;function LimitInputPrice(e) {
    var $e = $(e);
    var _C = +$e.val();
    var _U = +$('.upperLimitPrice:first').text();
    var _L = +$('.lowerLimitPrice:first').text();
    ( _C > _U || _C < _L || isNaN(_C)) && $e.val('0');
    CountFrozenMoney();//计算冻结资金
}
////////////////////////////////动输入股数的限制/////////////////////////////////////
;function LimitInputStockAmount(e) {
    var $e = $(e);
    var _C = +$e.val();
    var _U = +$('.JS-weituo-max:first').text();
    _C = parseInt(_C / 100) * 100; // 以整百的倍数
    _C = _C > _U ? _U : _C; // 不能超过最大数;
    ( _C > _U || _C < 0 || isNaN(_C)) && $e.val('0') || $e.val(_C);
    CountFrozenMoney();//计算冻结资金
}

//////////////////////////////////////计算冻结资金//////////////////////////////////////////////
;function CountFrozenMoney() {
    var _curPice = $('#JS-entrust-price').val();
    var _curStockNum = $('#JS-entrust-num').val();
    $('.JS-dj').text((_curPice * _curStockNum).toFixed());
}

