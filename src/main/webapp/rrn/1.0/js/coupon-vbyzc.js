$(function(){

	//0:优惠券激活码兑换优惠券
	//1:积分兑换优惠券
	var type = 0;

	////////////////////////绑定事件////////////////////////////////////
	// 添加优惠券
	$(".add_coupon").on('click',this, function(event) {
		event.preventDefault();
		vbyzctools.show($("#addCouponDialog"));
		type=0;
	});
	// 兑换商城的优惠券
	$(".JS-coupon-exchange").on('click', this, function(event) {
		var _this=$(this);
		event.preventDefault();
		//先装入相关的数据到动态层的相关对象里面
		var coupon_score = _this.find(".JS-coupon-score").text();
		var coupon_id = _this.find(".JS-coupon-id").text();
		var coupon_money = _this.find(".num-money").text();
		$("#couponId").val(coupon_id);
		$(".cont-word span").text(coupon_score);
		console.log(coupon_score,coupon_id,coupon_money);
		//再显示出来
		vbyzctools.show($("#exchangeDialog"));
		type=1;
	});

	//点击确认按钮
	$(".JSbtnsubmit").on("click",this,function(event){
		var _this = $(this);
		_this.prop("disabled",true);
		//优惠券激活码兑换优惠券
		if(type===0){
			//激活码
			var code = $("#couponCode").val();
			if(code!=null&&code.length>0){
				var data={code:code};
				$.ajax({
					url: "/rest/coupon/tickets/exchange",
					type: "post",
					dataType: "json",
					data: data
				}).done(function (res) {
						if(res.resultFlag){
							toast(res.resultMsg,function(){
								window.location.href = "/rest/coupon/?_t="+new Date().getTime();
							});
						}else{
							toast(res.resultMsg);
						}
					})
					.fail(function () {
						toast("请求失败");
					})
					.always(function () {
						_this.prop("disabled",false);
					});
			}else{
				toast("请输入激活码");
			}

		}else if(type===1){//积分兑换优惠券
				var couponId = $("#couponId").val();
				var data = {couponId:couponId};
			$.ajax({
				url:"/rest/coupon/integral/exchange",
				type:"post",
				dataType:"json",
				data:data
			}).done(function(res){
				if(res.resultFlag){
					toast(res.resultMsg,function(){
						window.location.href = "/rest/coupon/?_t="+new Date().getTime();
					});

				}else{
					toast(res.resultMsg);
				}
			}).fail(function(){
				toast("请求失败，请稍后再试!");
			}).always(function(){
				_this.prop("disabled",false);
			});

		}


	});
	
});