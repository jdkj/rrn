$(function(){

	var settingType = $("#settingType");

	//实名认证
	$(".JSidentification").on('click', this, function(event) {
		vbyzctools.show($("#authenticationDialog"));
		settingType.val(1);
		event.preventDefault();

	});

	//手机绑定
	$(".JSmodifyTel").on('click', this, function(event) {
		vbyzctools.show($("#modifytelphoneDialog"));
		settingType.val(2);
		event.preventDefault();
	});
	//设置登录密码
	$(".JSsetPwd").on('click', this, function(event) {
		vbyzctools.show($("#setPwdDialog"));
		settingType.val(4);
		event.preventDefault();
	});
	//修改登录密码
	$(".JSmodifyPwd").on('click', this, function(event) {
		vbyzctools.show($("#modifyPwdDialog"));
		settingType.val(5);
		event.preventDefault();
	});
	//设置提款密码
	$(".JSsetDrawPwd").on('click', this, function(event) {
		vbyzctools.show($("#setDrawPwdDialog"));
		settingType.val(6);
		event.preventDefault();
	});
	//修改提款密码
	$(".JSmodifyDrawPwd").on('click', this, function(event) {
		vbyzctools.show($("#modifyDrawPwdDialog"));
		settingType.val(7);
		event.preventDefault();
	});
	//绑定银行卡前提示用户先进行实名认证
	$(".JSaddBank").on('click',this,function(event){
		vbyzctools.show($("#addBankDialog"));
		settingType.val(0);
		event.preventDefault();
	});



	
});


