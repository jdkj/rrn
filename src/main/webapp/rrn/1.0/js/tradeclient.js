jQuery(document).ready(function($) {
	// 封闭插件区域
	(function($,window,document,undefined){
		/****************************************
		// tab选项卡操盘中心独立使用
		****************************************/
		$.fn.tab_trade_1 = function(o) {
			// 默认一下
			var options = $.extend(
			{
				activeNo: 0,contBox : '.ddddddd' }, 
				o || {}
			);
			// console.log(options);
			// 为了保险，还是each一下
			return $(this).each(function() {
				var _a=$(this).find("a");
				var _c=$(options.contBox).children("div"); //所有内容div
				_a.click(function(){
					event.preventDefault()
					var $this = $(this);
					var _cac  = $this.data('cwp');//从标签得到要显示的容器id
					var _ids  = $this.index();//占击的序号
					FormChangebuysell($this.attr('id'));//买入和买入表单略微不同显示的控制, 先运行这步，里面要ajax加载股票数据，然后下面才显示tab的激活box
					$this.addClass("nav_select");//自己加上active
					_a.not($this).removeClass("nav_select");//清除其它active
					_c.hide();//所有子元素隐藏
					$('#'+_cac).show();//显示目标
					var _LISTFOR_ = $this.data('listfor');
					$.log('***************************************\n当前主标题的激活签页：',$this.index(),'目标数据表id:',_LISTFOR_,'*******************************************');
					Loadtabledata(_LISTFOR_);// 异步读取数据
				});
				// 默认激活
				_a.eq(options.activeNo).trigger("click");
			});
		};
		/****************************************!
		 * // [查询数据表使用的独立标签]
		 ****************************************/
		$.fn.tabdataShow = function(o) {
			var options = $.extend(
			{
				activeNo: 0,contBox : '.ddddddd' }, 
				o || {}
			);
			return $(this).each(function() {
				var _a=$(this).son("li");
				_a.click(function(){
					var _LISTFOR_ = $(this).data('listfor');
					var _item=$(options.contBox);
					$.log('///////////////////////////////////////\n查询页激活签页：',$(this).index(),'目标数据表id:',_LISTFOR_,'/////////////////////////////////');
					// 样式
					$(this).ac("query-select");
					_a.not($(this)).rc("query-select");
					_item.son().hide()
					_item.son().eq($(this).index()).show();
					// 读取数据
					Loadtabledata(_LISTFOR_);
				});
				_a.eq(options.activeNo).trigger("click");
			});
		};

		///////////////////////////////翻页的事件///////////////////////////
		$.fn.turnpage = function(opt){
			// 初始化
			var defaults = {page:1};
			var options  = $.extend(defaults,opt||{});
			// 循环
			return $(this).each(function(index, e) {
				$(this).on('click', "li", function(event) {
					event.preventDefault();
					var $this     = $(this);
					var $lis      = $this.parent().son('li'); //所有LI
					var _index    = $this.index();//当前点击LI的位置
					var _activeNo = $(this).sbl('.active').index()-2; //点击时处理前页码激活的page位置, 用于判断上一页，下一页的上下标
					var _class    = $this.attr("class"); //用于方法名称
					var _tabID    = $this.parent().data('listfor');
					console.log('点击了页码LI,序号为：',_index);
					if(turnPageMethods[_class]){ // 是否占击了当前页码
						$lis.rc('active');//先清除激活
						// turnPageMethods[_class].call($this,_activeNo); //点击了LI
						turnPageMethods[_class].apply($this,[_activeNo,_tabID]); //点击了LI
					}
				});
			});
		};

	})(jQuery,window,document);

	////////////////////////////////////////////////////////////////共用方法区域【限本页面】///////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////共用方法区域【限本页面】///////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////共用方法区域【限本页面】///////////////////////////////////////////////////////////////////////////
	 // * 【本页jquery全局变量】
	var pendingRequests = 0; // 是否在请求

	///////////////////////////点击翻页样式的方法集合///////////////////////////////
	var turnPageMethods =  {
		defautl: {},
		//首页
		first : function(n,_TABID){
			Loadtabledata(_TABID,1);
			this.sbl('.page:first').ac('active');
		},
		//尾页
		last : function(n,_TABID){
			Loadtabledata(_TABID,this.sbl('.page').length);
			this.sbl('.page:last').ac('active');
		},
		//数字
		page : function(n,_TABID){
			Loadtabledata(_TABID,(+this.text()));
			this.ac('active');
		},
		// 上一页
		prev : function(n,_TABID) {
			var $pageDOMS = $(this).sbl('.page');
			n>0 && ($pageDOMS.eq(n-1).ac('active')) || $pageDOMS.eq(n).ac('active');
			Loadtabledata(_TABID,(+this.sbl('li.active').text()));
		},
		// 下一页
		next : function(n,_TABID) {
			var $pageDOMS = $(this).sbl('.page');
			var _pageCount= $pageDOMS.length;
			n> _pageCount-2 && ($pageDOMS.eq(n).ac('active')) || $pageDOMS.eq(n+1).ac('active');
			Loadtabledata(_TABID,(+this.sbl('li.active').text()));
		}
		
	}

	//////////////////////////// 把数据拼装成表格 //////////////////////////////

	;function JsonToTable(_Data){
		var _t= Array.prototype.slice.call(_Data,0);
		var _outTable = '';
		for(var i=0; i<_t.length;i++){
			_outTable += '<tr>';
			//console.log( _t[i] );
			for (var key in _t[i]){
				_outTable +=  '</td><td>' + _t[i][key];
			}
			_outTable += '</td></tr>';
		}
		_outTable =_outTable.replace(/<tr><\/td>/gi,'<tr>');
		//console.log(_outTable);
		return _outTable;
	}

	//////////////////////////// 生成页码dom //////////////////////////////
	;function CreatPageDom(_totalpages){
		var _htmlDom = '';
		_htmlDom += '<li data-pageNo="" class="first"><a href="#">首页</a></li>';
		_htmlDom += '<li data-pageNo="" class="prev"><a href="#">上一页</a></li> ';
		for(var i=1; i<+_totalpages+1 ;i++){
			i==1 && (_htmlDom +='<li data-pageNo="" class="page active"><a href="#">1</a></li>') || (_htmlDom +='<li data-pageNo="" class="page"><a href="#">'+i+'</a></li>');
		}
		_htmlDom += '<li data-pageNo="" class="next"><a href="#">下一页</a></li><li data-pageNo="" class="last"><a href="#">尾页</a></li>';
		return _htmlDom;
	}

	//清空读取出来的股票信息，止前是没有顺序逻辑错误，但要注意观察
	;function clearStockInfo (argument) {
		$('.upperLimitPrice,.lowerLimitPrice,.lastPrice,.zhangfu').text('0');
		$('li[class*="sellPrice"],li[class*="buyPrice"],li[class*="sellQty"],li[class*="buyQty"]').text('0');
            $('#JS-entrust-price,#JS-entrust-num').val('0');
	}

	//////////////////////////// 切换的是买入和卖出标签时，表单中不同的地方 单独处理//////////////////////////////
	/**
	 * [FormChangebuysell 当激活标签的时候，会调用这个过程]
	 * @param {[type]} id [激活的是哪一个标签]
	 */
	;function FormChangebuysell (id) {	
		switch(id){
			case 'JS-buy-in' :
				//买入
				$('.JS-left5-tip').text('委买股数');
				$('.JS-line4-tip').text('最高可买');
				$('#v-post-action').val('1');
				$('.JS-weituo-max').text('0'); //切换了标签，打乱了原先的计划，就要清空最大可操作股数，以防出乱子
				$('.JS-dj-name').show();
				$('.JS-sale-tips').hide();
				$('#JS-input-stockcode').val('');//股票代码的input,切换标签的时候，才清空
				clearStockInfo();
			break;
			case 'JS-sold-out':
				//卖出
				$('.JS-left5-tip').text('委卖股数');
				$('.JS-line4-tip').text('最大可卖');
				$('#v-post-action').val('2');
				$('.JS-weituo-max').text('0'); //切换了标签，打乱了原先的计划，就要清空最大可操作股数，以防出乱子
				$('.JS-dj-name').hide();
				$('.JS-sale-tips').css("display","inline");
				$('#JS-input-stockcode').val('');//股票代码的input，切换标签的时候，才清空
				clearStockInfo();
			break;
			default: void(0);
		}
		clearStockInfo();
	}
	//////////////////////////// 加载要买入或卖出的股票实时数据 //////////////////////////////\
	/**
	 * 【Load_stock_info 当页面进入的时候，肯定是激活买入或买出，然后调用url中的股票代码参数，运行此函数】
	 * 【输入正确股票代码的时候，也调用此函数】
	 * 但要判断手工点击买入或买出标签的时候，不调用此代码
	 * @param {[type]} stockNo    [股票代码]
	 */
	;function Load_stock_info (stockNo) {
		var contractNo = $('#JS-value-hyNo').val();//合约编号，网页加载时即输出到html中
		$('#JS-input-stockcode').val(stockNo);
		// alert('加载股票信息!合约编号：'+contractNo +'，股票代码：'+stockNo);
		$.ajax({
			url: '/rest/trade/queryStock',
			type: 'post',
			dataType: 'json',
			data: {"agreementId":contractNo,"stockCode":stockNo},
		})
		.done(function(data) {
			//要先在这里判断是否有这个股票代码，没有就提示没有这个股票信息
			if(Object.keys(data).length>0&&data.resultFlag){
				refreshHTML(data);
			} else{
				alert('没有这个股票信息');
			}
		})
		.fail(function() {
			console.log("error");
		})

		;function refreshHTML(_JOSN_){
			var o = _JOSN_.data;
			// console.table(o);
			// 填充内容
			
			$('span.JS-balance-money').text(o.agreementInfo.avaiableCredit); //余额，但也可能直接在HTML中输出
			$("span.JS-stockName").text(o.stockMarket.name);
			$('span.lastPrice').text(o.stockMarket.nowPrice); // 现价
			$('span.zhangfu').text(o.stockMarket.diff_rate); // 涨幅
			$('span.upperLimitPrice').text(o.stockMarket.maxPrice);//涨停
			$('span.lowerLimitPrice').text(o.stockMarket.minPrice);//跌停
			$('#JS-entrust-price').val(o.stockMarket.nowPrice); //委托价格格，其实就是现价
			for (var i = 1 ; i < 6 ; i++) {
				var sellmoney='sell'+i+'_m';
				var sellnum='sell'+i+'_n';
				var buymoney='buy'+i+'_m';
				var buynum='buy'+i+'_n';
				$('li.sellPrice'+i).text(o.stockMarket[sellmoney]);
				$('li.sellQty'+i).text(o.stockMarket[sellnum]);
				$('li.buyPrice'+i).text(o.stockMarket[buymoney]);
				$('li.buyQty'+i).text(o.stockMarket[buynum]);
			};
			// 判断是是买还是卖，1买，2卖，只有买的时候，可操作股数才会根据单价的改变而改变
			if ($('#v-post-action').val()=='1') {
				canBuyAmount();
			}else{
				$('span.JS-weituo-max').text(o.agreementInfo.stockAvailableNum); //最大可买股数
			}
			//单独判断涨幅是绿色还是红色
		}
	}

	//////////////////////////// 读取表格数据 //////////////////////////////\
	;function Loadtabledata(_tabID,_PageNo){
		// alert('读取表格：'+_tabID);
		if(_tabID ==undefined) {
			return false; //如果没有指定数据表，就不处理
		};
		// 初始化变量
		var _URL_= ''; // 目标地址
		_PageNo=_PageNo ?  _PageNo : 1;
		//决定url
		switch(_tabID){
			case "TABLE-cicang" : 
				_URL_ = 'json_1.html';
				break;
			case "TABLE-cedan" : 
				_URL_ = 'json_2.html';
				break;
			case "TABLE-Execution" : 
				_URL_ = 'json_3.html';
				break;
			case "TABLE-Entrust" : 
				_URL_ = 'json_4.html';
				break;
			case "TABLE-HistoryExecution" : 
				_URL_ = 'json_5.html';
				break;
			case "TABLE-HistoryEntrust" : 
				_URL_ = 'json_6.html';
				break;
			defautl: void(0);
		}
		// console.log('要读取的url：',_URL_);
		if(!pendingRequests){
			console.log('判断成功，开始请求第'+_PageNo+' 页内容..........');
			pendingRequests =1 ;//正在请求中
			$.ajax({
				type: "POST",
				url: _URL_,
				async: true,
				dataType: "json", 
				data: {getPage:_PageNo},
				success: function(msg){
					$.log("请求成功,共有数据",msg.totalpage,"页,状态还原：",pendingRequests);
					//console.table((msg.datarow));
					//操作
					$('#'+_tabID+' tbody').children('tr:gt(0)').remove();//留下表头
					$('#'+_tabID).append(JsonToTable(msg.datarow));//装载表格
					//有点肿。。。
					(function(target){
						target.children('li').length<1 && target.append(CreatPageDom(msg.totalpage));
					})($('ul[data-listfor='+_tabID+']'));
					pendingRequests =!pendingRequests-0;//清除请求状态
				},
				error:function(err){}
			});
		}
	}

	// /////////////////////////////////// 撒单操作 ///////////////////////////////////
	/**
	 * [Vcontrol_cedan 撒单操作]
	 * @param {[type]} _stockNo [股票代码,从按钮中获取]
	 */
	;function Vcontrol_cedan (_stockNo) {
		var $iconDom  =this;
		var removeRow = function (e) {
			e.parent().parent('tr').fadeOut('fast', function() {
				$(this).remove();
			});
		}
		// 请求
		$.ajax({
			url: 'json_cedan.html',
			type: 'post',
			data: {stockNo: _stockNo},
		})
		.done(function(msg) {
			!!(+msg) && removeRow.call(this,$iconDom);
		})
		.fail(function(textStatus) {
			alert('谢谢收看，请求失败，可能是网络问题，或服务器挂了');
		})
	}

	////////////////////////////////////////提交订单//////////////////////////////////////////
	;function Vcontrol_submit () {
		var hyID     = $('#JS-value-hyNo').val();//合约编号
		var gpID     = $('#JS-input-stockcode').val();//股票编码
		var wtPrice  = $('#JS-entrust-price').val();//价格
		var wtAMount = $('#JS-entrust-num').val();//数量
		var action   = $('#v-post-action').val();//买入?卖出
		var sendObj  = {hyID: hyID,gpID:gpID,wtPrice: wtPrice,wtAMount:wtAMount,action:action};
            console.log(sendObj);
            if(wtPrice !== '0' && wtAMount !== '0'){
                   $.ajax({
                   	url: '111111111.html',
                   	type: 'post',
                   	data: sendObj
                   })
                   .done(function(response) {
                   	console.log(response);
                   })
                   .fail(function() {
                   	alert("谢谢收看，请求失败，请稍后再试。");
                   })
            }
	}

	/////////////////////////////////////输入股票代码///////////////////////////////
	;function InputStockNo (e) {
		// 股票代码小于6位数的时候，清空数据，=6位的时候，就加载股票信息
		if(e.val().length<6){
			clearStockInfo();//清空数据
		}else{
			// alert('读取股票信息');
			Load_stock_info(e.val());
		}
	}
	//////////////////////////////////加减价格和股数//////////////////////////////////
		;function minPrice () {
			var $input           = $('#JS-entrust-price');
			var _curPice         = $input.val();
			var _lowerLimitPrice = $('.lowerLimitPrice:first').text();
			if(_curPice>_lowerLimitPrice){
				$input.val((_curPice - 0.01).toFixed(2));
				$('#v-post-action').val()=='1' && canBuyAmount(); //改变委托价格的时候，如果是买入的时候，要重新计算可买股数，并刷新dom
				CountFrozenMoney();//计算冻结资金
			};
		}
		;function addPrice () {
			var $input           = $('#JS-entrust-price');
			var _curPice         = +$input.val();
			var _upperLimitPrice = +$('.upperLimitPrice:first').text();
			if(_curPice < _upperLimitPrice){
				var tmp=(_curPice + 0.01);
				$input.val(tmp.toFixed(2));
				$('#v-post-action').val()=='1' && canBuyAmount();//改变委托价格的时候，如果是买入的时候，要重新计算可买股数，并刷新dom
				CountFrozenMoney();//计算冻结资金
			};
		}
		;function minNum () {
			var $input  = $('#JS-entrust-num');
			var _curNum = $input.val();
			if(_curNum>0){
				$input.val(_curNum-100);
				CountFrozenMoney();//计算冻结资金
			};
		}
		;function addNum () {
			var $input         = $('#JS-entrust-num');
			var _curNum        = $input.val();
			var _JS_weituo_max = $('.JS-weituo-max:first').text();
			if(+_curNum<_JS_weituo_max){
				$input.val(+_curNum+100);
				CountFrozenMoney();//计算冻结资金
			};
		}

	//////////////////////////////////计算可买股数//////////////////////////////////
	;function canBuyAmount () {
		console.log('现在是买入的状态，要重新计算可买股数......');
		var amount = 0;
		var _money =$('.JS-balance-money').text();
		var _price =$('#JS-entrust-price').val();
		$('#JS-entrust-num').val('0'); //当重新计算可买股数，已填写的股数可能会超过，所以要清0
		amount = (parseInt(_money/_price/100)*100); //以100为基数
		$('span.JS-weituo-max').text(amount);
	}

	////////////////////////////////动输入委托价格的限制/////////////////////////////////////
	;function LimitInputPrice (e) {
		var $e = $(e);
		var _C = +$e.val();
		var _U = +$('.upperLimitPrice:first').text();
		var _L = +$('.lowerLimitPrice:first').text();
		( _C > _U || _C < _L || isNaN(_C)) && $e.val('0');
		CountFrozenMoney();//计算冻结资金
	}
	////////////////////////////////动输入股数的限制/////////////////////////////////////
	;function LimitInputStockAmount (e) {
		var $e = $(e);
		var _C = +$e.val();
		var _U = +$('.JS-weituo-max:first').text();
		_C     = parseInt(_C/100)*100; // 以整百的倍数
		_C = _C > _U ? _U : _C; // 不能超过最大数;
		( _C > _U || _C < 0 || isNaN(_C)) && $e.val('0') || $e.val(_C);
		CountFrozenMoney();//计算冻结资金
	}

	//////////////////////////////////////计算冻结资金//////////////////////////////////////////////
	;function CountFrozenMoney () {
		var _curPice     =$('#JS-entrust-price').val();
		var _curStockNum =$('#JS-entrust-num').val();
		$('.JS-dj').text((_curPice*_curStockNum).toFixed());
	}

	/////////////////////////////////////////////////////////////////////事件绑定//////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////事件绑定//////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////事件绑定//////////////////////////////////////////////////////////////////////////////////////
	

	////大tab,这里如果激活第1个和第4个，会出乱子
	var actionCode = GetQueryString('act')=='buy' ? 1 : 2;
	$(".JS-tradeclient-nav").tab_trade_1({activeNo:actionCode,contBox:".vbyzcbox"});

	//页面一进入，加载股票信息, 这个一定要放在激活大标签的下面，因为上面会先触发标签事件清空数据
	// Load_stock_info(GetQueryString('v2'));

	//查询数据表tab
	// $(".queryTab").tabdataShow({activeNo:3,contBox:".ListqueryBox"});
    //
	// //翻页的样式绑定
	// $('.pagination-sm').turnpage();
    //
	// // 撒单
	// $('body').on('click', '.JS-cedan', function(event) {
	// 	event.preventDefault();
	// 	Vcontrol_cedan.call($(this),$(this).data('stockNo'));
	// });

	//持仓买入和买出的点击事件
	$('body').on('click', '.JS-buysell', function(event) {
		event.preventDefault();
		//alert($(this).data('type') !== 'buy' && '卖' || '买');
		Vcontrol_buy_sell.call($(this),$(this).data('type'));
	});

	// 手动输入股票的时候
	$('#JS-input-stockcode').on('keyup',this, function(event) {
		InputStockNo($(this));
	});
	// 买入买出提交,成功则？？ 失败停留在本页面
	
	//加减买入卖出价格，和委托股数相关的绑定
	$('.JS-minPrice').on('click',this, function(event) {minPrice(); });
	$('.JS-addPrice').on('click',this, function(event) {addPrice(); });
	$('.JS-minNum').on('click',this, function(event) {minNum(); });
	$('.JS-addNum').on('click',this, function(event) {addNum(); });
	//手动输入价格和股数的事件
	$('#JS-entrust-price').on('blur', this, function(event) { LimitInputPrice(this);});
	$('#JS-entrust-num').on('blur', this, function(event) {LimitInputStockAmount(this);});

	//提交买卖
	$('#JS-submit-order').on('click', this, function(event) { Vcontrol_submit(); });
});


