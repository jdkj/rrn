jQuery(document).ready(function($) {
	// 提取利润
	$('.JS-fetchProfit').on('click', this, function(event) {
		vbyzctools.show($("#fetchProfitDialog"));
	});

	// 追加保证金
	$('.JS-addBound').on('click', this, function(event) {
		vbyzctools.show($("#addBondDialog"));
		event.preventDefault();
	});

	// 申请结算
	$('.JS-quittrans').on('click', this, function(event) {
		vbyzctools.show($("#quittransDialog"));
		event.preventDefault();
	});

	//持仓买入和买出
	$('body').on('click', '.JS-buysell', function(event) {
		event.preventDefault();
		//alert($(this).data('type') !== 'buy' && '卖' || '买');
		Vcontrol_buy_sell.call($(this),$(this).data('type'));
	});
	
});