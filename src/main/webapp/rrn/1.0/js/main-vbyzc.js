//******************************************************全局域***********************************************
// 常用的方法
var vbyzctools = {
	_tempObj : null,
	darkwrap  : function(){
		var obj_dw = document.getElementById('darkwrap');
		return !obj_dw? $("body").append('<div id="darkwrap"></div>'):obj_dw;
	},
	show : function(obj){
		vbyzctools.darkwrap();
		$('#darkwrap').show();
		obj.show();
		this.align(obj);
		_tempObj = obj;
	},
	hide : function(obj){
		$('#darkwrap').hide();
		_tempObj.hide();
	},
	align:function(obj){
		var _oh = obj.height();
		var _ow = obj.width();
		var _wh = $(window).height();
		var _ww = $(window).width();

		return obj.css({"left":_ww/2 - _ow/2,"top":_wh/2 - _oh/2});
	}
};
/**
 * [Vcontrol_buy_sell 拼装买卖动作跳转的url]
 * @param {[type]} action [动作]
 */
;function Vcontrol_buy_sell (action) {
	var stockno = this.data('stockno');
	var hyNo    = $('#JS-value-hyNo').val(); //从页面静态input中得到
	var jumpUrl = "tradeclient.html?act="+action+"&v1="+hyNo+"&v2="+stockno;
	window.location.href=jumpUrl;
	//alert(jumpUrl);
}

/**
 * [GetQueryString 获取url参数]
 * @param {[type]} name [变量名]
 */
;function GetQueryString(name){
	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if(r!=null)return  unescape(r[2]); return null;
}
//******************************************************jquery域***********************************************
$(function(){
	var vbyzcname= '杨志聪';
	//随便放在这里面装逼一下
	(function($,window,document,undefined){
		// 方便操作
		$.fn.ac=function(n){return this.addClass(n);}
		$.fn.rc=function(n){return this.removeClass(n);}
		$.fn.son=function(n){return this.children(n);}
		$.fn.sbl=function(n){return this.siblings(n)};
		$.extend ({
			log : function(str){
				var t = Array.prototype.slice.call(arguments,0);
				console.log(t.join(''));
			}

		});
		// 输入框激活的样式
		$.fn.inputFocus = function(){
			if($(this).length){
				$(this).each(function(index, el) {
					var _this=$(this),_e=_this.parent("label");
					_this.on({
						focus: function(){_e.toggleClass('focus'); },
						blur : function(){_e.toggleClass('focus'); }
					});
				});
			}
		};
		
		// tab选项卡 初始版
		$.fn.vbyzctab = function(o) {
			var options = $.extend({activeNo: 0 }, o || {});
			return $(this).each(function() {
				var _a=$(this).son(".tabs").son("a");
				var _c=$(this).son(".contWrap");
				_a.click(function(){
					// if($(this).attr('href')=='#'){}
					_ids=$(this).index();
					$(this).ac("active");
					_a.not($(this)).rc("active");
					_c.not(_c.eq(_ids)).hide();
					_c.eq(_ids).show();
					return false;
					
				});
				_a.eq(options.activeNo).trigger("click");
			});
		};

		// tab选项卡 贱人版
		$.fn.vbyzctab2 = function(o) {
			// 默认一下
			var options = $.extend(
			{
				activeNo: 0,contBox : '.ddddddd' }, 
				o || {}
			);
			// 返回
			return $(this).each(function() {
				var _a=$(this).find("li");
				var _c=$(options.contBox).son("div");
				_a.click(function(){
					_ids=$(this).index();
					$(this).addClass("selected");
					_a.not($(this)).removeClass("selected");
					_c.not(_c.eq(_ids)).hide();
					_c.eq(_ids).show();
					return false;
					
				});
				// 默认激活
				_a.eq(options.activeNo).trigger("click");
			});
		};
		

	})(jQuery,window,document);

	//统一省事的绑定
	$(".login-cont").find(":text,:password").inputFocus();//文本框的focus状态 

	// 点击了灰色的层，隐藏浮动层
	$("body").on('click','#darkwrap' , function(event) {
		vbyzctools.hide();
	});
});
var alert = function(msg,fuc){

	$("#JS-Alert-resultMsg").html(msg);
	if(typeof fuc ==="function"){
	 	$("#JS-Alert-btnclose").on("click",this,function(){
			fuc();
			vbyzctools.hide($("#myAlert"));
	 	});
	}else{
		$("#JS-Alert-btnclose").on("click",this,function(){
			vbyzctools.hide($("#myAlert"));
		})
	}
	vbyzctools.show($("#myAlert"));
}