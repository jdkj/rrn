$(function () {
    /////////////////////////////////////绑定事件////////////////////////////////////
    /////////////////////////////////////绑定事件////////////////////////////////////
    /////////////////////////////////////绑定事件////////////////////////////////////
    $(".JS-trade-investAmount").eq(0).addClass("select-btn-active");

    caculateLockMoneys();

    staticsticsAgreementInfo();


    // 今日限买股
    $("#JS-limit-stock").on('click', this, function (event) {
        event.preventDefault();
        vbyzctools.show($("#dayLimitDialog"));
    });

    // 点击了灰色的层
    $(".JSbtnsubmit").on('click', this, function (event) {
        vbyzctools.hide();
    });

    ////////////////////////////////////////资金操作////////////////////////////////////////
    ////////////////////////////////////////资金操作////////////////////////////////////////
    ////////////////////////////////////////资金操作////////////////////////////////////////

    // 申请资金的点击效果
    $('.select-btn-group-wrap').on('click', 'button', function (event) {
        // 设置一些东西
        var $this = $(this);


        //先处理打勾的样式
        $this.siblings('.select-btn').removeClass('select-btn-active');
        $this.addClass('select-btn-active');


        // 判断是否点击了最后那个吊东西
        if ($this.is("#JS-otherApplyAmount")) {
            $(".JS-trade-investAmount").hide();
            $("#JS-input-amount").show();
        } else {//点选固定金额时
            $(".JS-trade-investAmount").show();
            $("#JS-input-amount").hide();
            // 王八蛋，投入资金的算法不知，先不做
            caculateLockMoneys();

            staticsticsAgreementInfo();
        }

        return false;

    });

    //输入金额的时候
    $("#JS-input-amount").on("keyup",this,function(){
        var errTips = $("#errInputTip");
        var item = $(this);
        var value = +item.val();
        var min = +item.data("min");
        var max = +item.data("max");
        if(value/1000>0&&value>=min&&value<=max&&value%1000===0){
            errTips.hide();
            $(".JS-trade-investAmount").show();
            caculateLockMoneys();
            staticsticsAgreementInfo();
        }else{
            $(".JS-trade-investAmount").hide();
            errTips.show();
        }
    });

    // 投入资金的点击效果
    $('.JS-trade-investAmount').on('click', this, function (event) {
        var $this = $(this);
        $this.siblings().removeClass('select-btn-active');
        $this.addClass('select-btn-active');
        staticsticsAgreementInfo();
        return false;
    });

    ///////////////////////////////////// 显示优惠券的事///////////////////////////////
    ///////////////////////////////////// 显示优惠券的事///////////////////////////////
    ///////////////////////////////////// 显示优惠券的事///////////////////////////////

    // 方法对象
    var vbyzc_coupon = {

        // 初始化有用的数据
        defaulttext1: $('.JS-coupon-wrap').html(),
        lastChooseCoupon: -1,
        // 设置优惠券提示的初始化文字
        setChooseDefaulttext: function () {
            $('.JS-coupon-wrap').html(vbyzc_coupon.defaulttext1);
        },

        // 改变icon
        icontoggle: function (o) {
            var _this = o ? o : this;
            _this = $(_this);
            $(".JS-coupon-list").toggle();
            _this.hasClass('icon-moreunfold') ? _this.rc('icon-moreunfold').ac('icon-less') : _this.rc('icon-less').ac('icon-moreunfold');
        },
        //隐藏优惠券层
        hideCouponWrap: function () {
            //setTimeout(function(){$(".JS-coupon-list").hide();},100);
            $(".JS-coupon-list").animate({right: -200, opacity: 0}, 400, function () {/*复位*/
                $(this).css({right: 0, opacity: 1, display: 'none'});
            });
        },
        // 选中优惠券
        chooseCoupon: function (e) {
            // 正常选中优惠券
            if (this.index() != vbyzc_coupon.lastChooseCoupon) {
                this.siblings(".JS-coupon-choose").children('img').hide(), this.children('img').show();
                vbyzc_coupon.lastChooseCoupon = this.index();
                $(".JS-coupon-wrap").html('已使用<b class="font-color-orangr">' + this.find('.JS-coupon-originAmount').text() + '</b>元抵用券<i class="icon-moreunfold"></i>');
                $(".JS-coupon-wrap").data("user-coupon-id",this.find('.JS-coupon-code').text());
                $(".JS-coupon-wrap").data("user-coupon-money", this.find('.JS-coupon-originAmount').text());
                vbyzc_coupon.hideCouponWrap();
                staticsticsAgreementInfo();
                // 选中同一张就取消
            } else {
                $(".JS-coupon-choose").children('img').hide();
                $(".JS-coupon-wrap").data("user-coupon-id",0);
                $(".JS-coupon-wrap").data("user-coupon-money", 0);
                vbyzc_coupon.lastChooseCoupon = -1;
                vbyzc_coupon.setChooseDefaulttext();
                vbyzc_coupon.hideCouponWrap();
                staticsticsAgreementInfo();
            }
        }
    };

    // 点击文字的时候
    $('.JS-coupon-wrap').on('click', this, function (event) {
        vbyzc_coupon.icontoggle.call($(".JS-coupon-wrap").children("i"));
    });

    // 点击选中优惠券的时候
    $(".JS-coupon-choose").on('click', this, function (event) {
        vbyzc_coupon.chooseCoupon.call($(this));
    });


    /**
     * 计算需要投资的金额
     */
    function caculateLockMoneys() {
        var borrowItem = $(".JS-trade-applyAmount.select-btn-active");
        //借款金额
        var borrowMoney;
        if(borrowItem.is("#JS-otherApplyAmount")){
            borrowMoney =+$("#JS-input-amount").val();
        }else{
            borrowMoney =+borrowItem.data("pzamount");
        }

        $(".JS-trade-investAmount").each(function (index) {
            var item = $(this);
            var multiple = item.data("multiple");
            //需要锁定的资金
            var lockMoney = parseInt((borrowMoney / multiple).toFixed(0));
            item.find(".JS-invest-val").html(lockMoney + "元");
            item.data("lock-money",lockMoney);
        });
    }


    /**
     * 统计配资合约信息
     */
    function staticsticsAgreementInfo() {

        var borrowItem = $(".JS-trade-applyAmount.select-btn-active");
        //申请资金
        var borrowMoney;
        if(borrowItem.is("#JS-otherApplyAmount")){
            borrowMoney =+$("#JS-input-amount").val();
        }else{
            borrowMoney =+borrowItem.data("pzamount");
        }
        var multipleItem = $(".JS-trade-investAmount.select-btn-active");
        //配资倍数
        var multiple = multipleItem.data("multiple");
        //投入资金
        var lockMoney = parseInt((borrowMoney / multiple).toFixed(0));
        //利率(年)
        var multipleRate = multipleItem.data("multiple-rate");
        //管理费
        var managementFee = 0;
        //管理费预交倍数
        var managementFeeRate = 1;
        //操盘周期单位
        var operatePeriod = "天";
        //配资类型
        var agreementType = multipleItem.data("borrow-agreement-type");
        switch (agreementType) {
            case 0:
                managementFeeRate = 2;
                managementFee=(borrowMoney * multipleRate / 365 / 100).toFixed(2);
                break;
            default:
                managementFee = (borrowMoney * multipleRate / 12 / 100).toFixed(2);
                operatePeriod = "月";
                break;
        }

        var couponMoney = $(".JS-coupon-wrap").data("user-coupon-money");


        var managementTotalFee = managementFee * managementFeeRate - couponMoney;

        managementTotalFee = managementTotalFee > 0 ? managementTotalFee : 0;

        var resultItem = $(".JS-result.trade-result").find("table").find("tr");
        //总操盘资金
        resultItem.eq(0).find("td").eq(1).find("span").eq(0).html(parseInt(borrowMoney + lockMoney).toFixed(2));
        //警戒线
        resultItem.eq(3).find("td").eq(1).find("span").eq(0).html((parseInt(borrowMoney + lockMoney * 0.5)).toFixed(2));
        //止损线
        resultItem.eq(4).find("td").eq(1).find("span").eq(0).html((parseInt(borrowMoney + lockMoney * 0.3)).toFixed(2));
        //投资本金
        resultItem.eq(5).find("td").eq(1).find("span").eq(0).html((parseInt(lockMoney)).toFixed(2));
        //管理费
        resultItem.eq(6).find("td").eq(1).find("span").eq(0).html((parseInt(managementFee)).toFixed(2));
        if (managementFeeRate > 1) {
            resultItem.eq(6).find("td").eq(1).find("span").eq(1).html("x" + managementFeeRate);
        } else {
            resultItem.eq(6).find("td").eq(1).find("span").eq(1).html("/");
        }
        resultItem.eq(6).find("td").eq(2).find("span").eq(2).html(operatePeriod);
        //共计应支付
        resultItem.eq(8).find("td").eq(1).find("span").eq(0).html(lockMoney + managementTotalFee);
        //预计会获得的积分
        resultItem.eq(9).find("td").eq(1).find("span").eq(0).html(managementTotalFee)
    }
});