$(document).ready(function($) {
	(function(i) {
		// 这个蓝交对象可能是用来控制获取到的数据的，塞林木
		//至于得到的数据就用表格填充好了
		var t = {
			days: "all",
			type: "all",
			ioType: "all",
			page: 1,
			pageSize: 10
		};
		// 时间
		i(".ass-time li:not(:first-child)").click(function(n) {
			i(".ass-time li:not(:first-child)").removeClass("ass-check");
			i(this).addClass("ass-check");
			t.days = i(this).attr("id");
			t.page = 1;
			t.ioType = i(this).attr("data-ioType");
		});
		// 类型
		i(".ass-type li:not(:first-child)").click(function(n) {
			i(".ass-type li:not(:first-child)").removeClass("ass-check");
			i(this).addClass("ass-check");
			t.type = i(this).attr("id");
			t.page = 1;
			t.ioType = i(this).attr("data-ioType");
		});
	}(window.jQuery));
});